/*=====================================================================================*/
/*! 
/file		MathUtils.h
/author		jesusprod
/brief		File containing utility functions for linear-algebra, interpolation, etc.
 */
/*=====================================================================================*/

#ifndef MATH_UTILS_H
#define MATH_UTILS_H

#include <JSandbox/JSandboxPCH.h>

typedef struct VizPoint
{
	Vector3d m_p;
	float m_rad;
} VizPoint;

typedef struct VizArrow
{
	Vector3d m_p0;
	Vector3d m_p1;
	float m_rad;
} VizArrow;

// Number utilities

#define EPS_APPROX 1e-9
#define EPS_INTERS 1e-3
#define EPS_POS 1e-6
#define EPS_VEL	1e-3

bool   isApprox(double a, double val, double tol);
bool   isApprox(const dVector& va, double val, double tol);

// Mesh utilities

bool   isTriangleMesh(const dVector& vpos, const iVector& vidx);

void   computeMeshNormalsArea(const dVector& vpos, const iVector& vidx, dVector& vnor);
void   computeMeshNormalsAngle(const dVector& vpos, const iVector& vidx, dVector& vnor);

//void   toQTColors(const dVector& vcol, cVector& vQTcol);

void   transformIndex(const vector<iVector>& vIndexMap, const iVector& vidxIn, int i, iVector& vidxOut);
void   applyIndexMap(const vector<iVector>& vIndexMap, const dVector& vposIn, int D, dVector& vposOut);

void   getSubvector(int i, int nele, const dVector& vin, dVector& vout);
void   setSubvector(int i, int nele, const dVector& vin, dVector& vout);

void   getSubvector(int i, int nele, const VectorXd& vin, VectorXd& vout);
void   setSubvector(int i, int nele, const VectorXd& vin, VectorXd& vout);

// Vector related

Eigen::Matrix3d computeAxisAngleRotation(const Vector3d& axis, Real angle);

Eigen::Matrix3d   crossProductMatrix(const Eigen::Vector3d& v);

Eigen::Matrix3d   tensorProduct(const Eigen::Vector3d& u, const Eigen::Vector3d v);

double   signedAngle(const Vector3d& v0, const Vector3d& v1, const Vector3d& r, double a);

Frame3d   parallelTransport(const Frame3d& F, const Vector3d& t2);

Vector3d   parallelTransport(const Vector3d& u, const Vector3d& t1, const Vector3d& t2);

Frame3d   frameRotation(const Frame3d& vF, Real angle);

Frame3d   frameRotation(const Frame3d &frame, Matrix3d& R);

void   frameRotation(const vector<Frame3d>& vF, const dVector& va, vector<Frame3d>& vFRot);

// Eigen related

typedef Eigen::SparseMatrix<Real> MatrixSd;

void   buildSelectionMatrix(const vector<iVector>& vselMap, MatrixSd& mS, bool shrink = false);
void   buildExtendedMatrix(const MatrixSd& mSin, MatrixSd& mSout, int dim = 3);

void   toTriplets(const MatrixSd& mM, tVector& vM);

void   add3D(int i, const Eigen::Vector3d& v, dVector& vd);
void   set3D(int i, const Eigen::Vector3d& v, dVector& vd);
Eigen::Vector3d   get3D(int i, const dVector& vd);

void   add2D(int i, const Eigen::Vector2d& v, dVector& vd);
void   set2D(int i, const Eigen::Vector2d& v, dVector& vd);
Eigen::Vector2d   get2D(int i, const dVector& vd);

void   addBlock3x1(int i, const Eigen::Vector3d& v, Eigen::VectorXd& vd);
void   setBlock3x1(int i, const Eigen::Vector3d& v, Eigen::VectorXd& vd);
Eigen::Vector3d   getBlock3x1(int i, const Eigen::VectorXd& vd);

void   addBlock2x1(int i, const Eigen::Vector2d& v, Eigen::VectorXd& vd);
void   setBlock2x1(int i, const Eigen::Vector2d& v, Eigen::VectorXd& vd);
Eigen::Vector2d   getBlock2x1(int i, const Eigen::VectorXd& vd);

void   addBlock3x3(int i, int j, const Eigen::Matrix3d& M, Eigen::MatrixXd& mD, bool sym = false);
void   setBlock3x3(int i, int j, const Eigen::Matrix3d& M, Eigen::MatrixXd& mD, bool sym = false);
Eigen::Matrix3d   getBlock3x3(int i, int j, Eigen::MatrixXd& mD);

void   addBlock3x3From(int i, int j, const Eigen::Matrix3d& M, Eigen::MatrixXd& mD, bool sym = false);
void   setBlock3x3From(int i, int j, const Eigen::Matrix3d& M, Eigen::MatrixXd& mD, bool sym = false);
Eigen::Matrix3d   getBlock3x3From(int i, int j, Eigen::MatrixXd& mD);

void   addBlockNxM(int n, int m, int i, int j, const Eigen::MatrixXd& M, Eigen::MatrixXd& mD, bool sym = false);
void   setBlockNxM(int n, int m, int i, int j, const Eigen::MatrixXd& M, Eigen::MatrixXd& mD, bool sym = false);
MatrixXd   getBlockNxM(int n, int m, int i, int j, Eigen::MatrixXd& mD);

Eigen::VectorXd   toEigen(const dVector& v);
Eigen::VectorXi   toEigen(const iVector& v);

dVector   toSTL(const Eigen::VectorXd& v);
iVector   toSTL(const Eigen::VectorXi& v);

dVector   toSTL(const Eigen::Vector3d& v);

void   toEigen(const dVector& v, Eigen::VectorXd& ve);
void   toEigen(const iVector& v, Eigen::VectorXi& ve);

void   toSTL(const Eigen::VectorXd& v, dVector& vs);
void   toSTL(const Eigen::VectorXi& v, iVector& vi);

void   to3D(const vector<Eigen::Vector2d>& v2D, vector<Eigen::Vector3d>& v3D, int addedD = 2);
void   to3D(const Eigen::VectorXd& v2D, Eigen::VectorXd& v3D, int addedD = 2);
Eigen::Vector3d   to3D(const Eigen::Vector2d& p2D, int addedD = 2);
void   to3D(const dVector& v2D, dVector& v3D, int addedD = 2);

void   to2D(const vector<Eigen::Vector3d>& v3D, vector<Eigen::Vector2d>& v2D, int removedD = 2);
void   to2D(const Eigen::VectorXd& v3D, Eigen::VectorXd& v2D, int removedD = 2);
Eigen::Vector2d   to2D(const Eigen::Vector3d& p3D, int removedD = 2);
void   to2D(const dVector& v3D, dVector& v2D, int removedD = 2);

void   logSimu(const char* msg, ...);

string   vectorToString(const VectorXd& M);
//void   writeToFile(const VectorXd& v, const QString& str, bool concatenate);

string   matrixToString(const MatrixXd& M);
//void   writeToFile(const MatrixXd& M, const QString& str);

string   matrixToString(const MatrixSd& M, bool sym = false);
//void   writeToFile(const MatrixSd& M, const QString& str, bool sym = false);

//void   listDirFiles(const string& path, vector<string>& files);

Real computePolarDecomposition(const Matrix3d& M, Matrix3d& Q, Matrix3d& S, Real tol);

// This function computes the Gram-Schmidt orthogonalization
void orthogonalize_GS(const MatrixXd& mM, MatrixXd& mO);

Eigen::Matrix2d computeTranslate(const Vector2d& vt);
Eigen::Matrix2d computeRotation(double angle);

// Compute curvature binormal of the parameter vectors
Eigen::Vector3d computeKB(const Eigen::Vector3d& e0, const Eigen::Vector3d& e1);

// Compute the rotation matrix corresponding to Euler
//Eigen::Matrix3d eulerRotation(Real x, Real y, Real z);

// Eigen library functions setZero() and setIdentity() change
// the internal structure of the sparse matrix and not just
// set to zero/identity its values. This functions perform
// this operation faster and avoid reconstructing the 
// matrix for maximum performance. 

//void setSparseMatrixI(MatrixSd& mM);
void setSparseMatrix0(MatrixSd& mM);

// This function looks for the index of the specified coefficient within 
// the internal vector of values storing the content of a sparse matrix.
// This should be used to cache references to fastly update the Jacobian.
// The function assumes the sparse matrix is compressed and col-major.

//bool isOccupied(int row, int col, const MatrixSd& mH, bool sym = 0);
int getInternalDataIndex(int row, int col, const MatrixSd& mH, bool sym = false);

// Deformation related

// Green strain for the given deformation F: e = 1/2*(F + F^T)
void computeGreenStrain(const MatrixXd& mF, MatrixXd& me);

// Cauchy strain for the given deformation F: E = 1/2*(F*F^T - I)
void computeCauchyStrain(const MatrixXd& mF, MatrixXd& mE);

// Visualization related

dVector convertHSLtoRGB(const dVector& hsl);

dVector computeHeatColorRGB(double alpha);

dVector computeHeatColorHSL(double alpha);

// Geometry

Vector3d   computeCurvatureBinormal(const Vector3d& e0, const Vector3d& e1);

// Miscellanea

string   generateGUID();

// Interpolation

void   orthonormalize(Frame3d& F);

void   hermiteInterpolation(const vector<Vector3d>& vin, vector<Vector3d>& vout, bool loop, int outnPoint, const Vector3d& tanIni = Vector3d(), const Vector3d& tanEnd = Vector3d());
void   hermiteInterpolation(const vector<Vector3d>& vin, vector<Vector3d>& vout, bool loop, double outTol, const Vector3d& tanIni = Vector3d(), const Vector3d& tanEnd = Vector3d());

void   hermiteInterpolation(const vector<Vector2d>& vin, const dVector& vsin, vector<Vector2d>& vout, bool loop, int outnPoint);
void   hermiteInterpolation(const vector<Vector2d>& vin, const dVector& vsin, vector<Vector2d>& vout, bool loop, double outTol);

void   hermiteInterpolation(const vector<Frame3d>& vin, const dVector& vsin, vector<Frame3d>& vout, bool loop, int outnPoint);
void   hermiteInterpolation(const vector<Frame3d>& vin, const dVector& vsin, vector<Frame3d>& vout, bool loop, double outTol);

void   computeBezierSurfacePatchCanonCon(Real sizeu, Real sizev, MatrixXd& mP);
void   computeBezierSurfacePatchCanonInt(const MatrixXd& mPcon, int nuint, int nvint, MatrixXd& mP);
void   computeBezierSurfacePatchInterpolationMatrix(int nuint, int nvint, int coord, MatrixXd& mH);

// Curves
void   sphericalHelix(double R, int nSteps, int nLines, int spinX, int spinZ, vector<Vector3d>& voutPoints);
void   cylindricalHelix(double R, double H, int nSteps, int nLines, int spinX, int spinZ, vector<Vector3d>& voutPoints);

// Distances & Intersections

bool   pointTriIntersect(const Vector3d& p, const Vector3d& v0, const Vector3d& v1, const Vector3d& v2, double& u, double& v, double tol);
bool   lineLineIntersect(const dVector& p1, const dVector& p2, const dVector& p3, const dVector& p4, double& mua, double& mub, double tol);

double   signedDistanceVF(const Vector3d& x, const Vector3d& y0, const Vector3d& y1, const Vector3d &y2, Vector3d& n, dVector& w);

double   signedDistanceEE(const Vector3d& x0, const Vector3d& x1, const Vector3d& y0, const Vector3d& y1, Vector3d& n, dVector& w);

double   signedAngle(const Vector2d& v0, const Vector2d& v1);
double   signedAngle(const Vector3d& v0, const Vector3d& v1, const Vector3d& ref);

Real   sqrDistanceVertexTriangle(const Vector3d &v, const Vector3d &f0, const Vector3d &f1, const Vector3d &f2, Real* pfSParam, Real* pfTParam);

Real   sqrDistanceEdgeTriangle(const Vector3d &e0, const Vector3d &e1, const Vector3d &f0, const Vector3d &f1, const Vector3d &f2, Real* pfSegP, Real* pfTriP0, Real* pfTriP1);

#endif