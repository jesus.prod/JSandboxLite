/*=====================================================================================*/
/*! 
/file		JSandboxPCH.h
/author		jesusprod
/brief		Precompiled header for JSandbox core
 */
/*=====================================================================================*/

#ifndef JSANDBOX_PCH_H
#define JSANDBOX_PCH_H

#define _USE_MATH_DEFINES

//#define _CRT_SECURE_NO_WARNINGS
//
//#define _SCL_SECURE_NO_DEPRECATE
//#define _CRT_SECURE_NO_DEPRECATE

#ifndef _DEBUG
#define OMP_RESTINIT
#define OMP_ENFORJAC
#define OMP_ASSEMBLE
#define MCOMPRESSION
#endif

//#undef min
//#undef max

//#include <QRgb>

#include <vector>
#include <string>
#include <cstdio>
#include <ctime>
#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <JSandbox/JSandboxDLL.h>

#include <set>

using namespace std;
using namespace Eigen;

typedef double Real;
typedef vector<int> iVector;
typedef vector<float> fVector;
typedef vector<double> dVector;
typedef vector<bool> bVector;
//typedef vector<QRgb> cVector;

typedef vector<Triplet<Real>> tVector;

typedef struct Frame3d
{
	Frame3d()
	{
		this->tan.setZero();
		this->nor.setZero();
		this->bin.setZero();
	}

	~Frame3d()
	{
		// Nothing here...
	}

	Frame3d(const Frame3d& toCopy)
	{
		this->tan = toCopy.tan;
		this->nor = toCopy.nor;
		this->bin = toCopy.bin;

		if (abs(tan.norm() - 1.0) > 1e-9)
			cout << endl << "Non unit director (tangent)";
		if (abs(bin.norm() - 1.0) > 1e-9)
			cout << endl << "Non unit director (binormal)";
		if (abs(nor.norm() - 1.0) > 1e-9)
			cout << endl << "Non unit director (normal)";
		if (abs(tan.dot(nor)) > 1e-9)
			cout << endl << "Non orthogonal (tan dot nor)";
		if (abs(nor.dot(bin)) > 1e-9)
			cout << endl << "Non orthogonal (nor dot bin)";
		if (abs(bin.dot(tan)) > 1e-9)
			cout << endl << "Non orthogonal (bin dot tan)";
	}

	bool operator == (const Frame3d& other) const
	{
		return (tan - other.tan).squaredNorm() < 1e-9 &&
			   (nor - other.nor).squaredNorm() < 1e-9 &&
			   (bin - other.bin).squaredNorm() < 1e-9;
	}

	Frame3d reverse() const
	{
		Frame3d out = *this;
		out.tan = -out.tan;
		out.bin = -out.bin;
		return out;
	}

	Vector3d tan;
	Vector3d nor;
	Vector3d bin;
}
Frame;

typedef struct EmbeddedPoint
{
	dVector m_vwei;
	iVector m_vidx;
} EmbeddedPoint;

class ModelPoint
{
public:

	ModelPoint()
	{
		this->m_pointIdx = -1;
		this->m_numDim = -1;
		this->m_numSup = -1;
	}

	ModelPoint(int numDim, int index)
	{
		assert(numDim > 0);

		assert(index >= 0);

		this->m_numDim = numDim;
		this->m_pointIdx = index;
	
		this->m_numSup = 1;
		this->m_vweights.push_back(1.0);
		this->m_vindices.push_back(index);
	}

	ModelPoint(int numDim, const iVector& vindices, const dVector& vweights)
	{
		assert(numDim > 0);
		
		assert((int)vindices.size() == (int)vweights.size());

		this->m_pointIdx = -1;

		this->m_numDim = numDim;
		this->m_vindices = vindices;
		this->m_vweights = vweights;

		this->m_numSup = (int) vindices.size();
	}

	virtual ~ModelPoint()
	{
		// Nothing to do
	}

	ModelPoint(const ModelPoint& toCopy)
	{
		this->m_pointIdx = toCopy.m_pointIdx;
		this->m_numDim = toCopy.m_numDim;
		this->m_numSup = toCopy.m_numSup;
		this->m_vindices = toCopy.m_vindices;
		this->m_vweights = toCopy.m_vweights;
	}

	int getPointIdx() const { return this->m_pointIdx; }
	void setPointIdx(int idx) { this->m_pointIdx = idx; }

	int isExplicit() const { return this->m_pointIdx != -1; }

	int getNumDim() const { return this->m_numDim; }
	int getNumSup() const { return this->m_numSup; }
	void setNumDim(int numDim) { assert(numDim > 0); this->m_numDim = numDim; }
	void setNumSup(int numSup) { assert(numSup > 0); this->m_numSup = numSup; }

	iVector& getIndices() { return this->m_vindices; }
	dVector& getWeights() { return this->m_vweights; }
	const iVector& getIndices() const { return this->m_vindices; }
	const dVector& getWeights() const { return this->m_vweights; }

	void setIndices(const iVector& vindices)
	{
		assert((int)vindices.size() == this->m_numSup);
		this->m_vindices = vindices;
	}

	void setWeights(const dVector& vweights)
	{
		assert((int)vweights.size() == this->m_numSup);
		this->m_vweights = vweights;
	}

	bool operator < (const ModelPoint& other) const
	{
		bool boolOr = false;
		boolOr |= this->isExplicit() && !other.isExplicit();
		boolOr |= this->m_pointIdx < other.m_pointIdx;
		return boolOr;
	}

	bool operator <= (const ModelPoint& other) const
	{
		return (*this) < other || (*this) == other;
	}

	bool operator >= (const ModelPoint& other) const
	{
		return other < (*this) || (*this) == other;
	}

	bool operator == (const ModelPoint& other) const
	{
		if (this->isExplicit() != other.isExplicit())
			return false; // Impossible matching

		if (this->isExplicit() && other.isExplicit())
		{
			bool boolAnd = true;
			boolAnd &= this->m_numDim == this->m_numDim;
			boolAnd &= this->m_pointIdx == other.m_pointIdx;
		}

		bool boolAnd = true;

		boolAnd &= this->m_numDim == other.m_numDim;
		boolAnd &= this->m_numSup == other.m_numSup;
		boolAnd &= this->m_pointIdx == other.m_pointIdx;
		boolAnd &= this->m_vindices == other.m_vindices;

		for (int i = 0; i < this->m_numSup; ++i) // Check for the similarity
			boolAnd &= abs(this->m_vweights[i] - other.m_vweights[i]) < 1e-6;

		return boolAnd;
	}

	bool operator != (const ModelPoint& other) const
	{
		return !(*this == other);
	}

private:

	int m_pointIdx;
	int m_numDim;
	int m_numSup;
	iVector m_vindices;
	dVector m_vweights;

};

typedef vector<ModelPoint> pVector;

typedef struct PointPro
{
	Real m_D;
	Vector3d m_oriPoint;
	Vector3d m_proPoint;
	ModelPoint m_point;
	bool m_valid;
} PointProjection;


typedef struct EigenPair
{
	Real m_eigenVal_r;
	Real m_eigenVal_i;
	VectorXd m_veigenVec_r;
	VectorXd m_veigenVec_i;
} EigenPair;

typedef struct LT_EigenPair
{
	bool operator()(EigenPair ep0, EigenPair ep1)
	{
		return ep0.m_eigenVal_r < ep1.m_eigenVal_r || (ep0.m_eigenVal_r == ep1.m_eigenVal_r && ep0.m_eigenVal_i < ep1.m_eigenVal_i);
	}
} LT_EigenPair;

typedef struct MT_EigenPair
{
	bool operator()(EigenPair ep0, EigenPair ep1)
	{
		return ep0.m_eigenVal_r > ep1.m_eigenVal_r || (ep0.m_eigenVal_r == ep1.m_eigenVal_r && ep0.m_eigenVal_i > ep1.m_eigenVal_i);
	}
} MT_EigenPair;

typedef struct CurvePoint
{
	Vector3d m_vp;
	ModelPoint m_mp;
	Vector3d m_tanI;
	Vector3d m_tanO;
	Real m_arcLen;

	set<int> m_spointers;

	bool m_isMasterPoint;
}
CurvePoint;

typedef struct LT_CurvePoint
{
	bool operator()(const CurvePoint& bp0, CurvePoint& bp1)
	{
		return bp0.m_arcLen < bp1.m_arcLen;
	}
}
LT_CurvePoint;

typedef vector<CurvePoint> ModelCurve;

#endif