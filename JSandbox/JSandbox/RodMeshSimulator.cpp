
#include <JSandbox/RodMeshSimulator.h>

#include <JSandbox/Solver/PhysSolver.h>
#include <JSandbox/Solver/BConditionFixed.h>
#include <JSandbox/Solver/BConditionSpring.h>

#include <JSandbox/Optim/ParameterSet_Basic.h>

RodMeshSimulator::RodMeshSimulator()
{
	this->m_pModel = NULL;
	this->m_pSolver = NULL;
	this->m_pDesign = NULL;
	m_L = new MatrixSd();
	m_DpdDpn = new MatrixSd();
	m_DfnDfd = new MatrixSd();
}

RodMeshSimulator::~RodMeshSimulator()
{
	if (this->m_pModel != NULL)
		delete this->m_pModel;
	if (this->m_pSolver != NULL)
		delete this->m_pSolver;
	if (this->m_pDesign != NULL)
		delete this->m_pDesign;
	delete m_L;
	delete m_DpdDpn;
	delete m_DfnDfd;
}

void RodMeshSimulator::ComputePositionVector(const RodMesh& mesh, VectorXd& vX)
{
	dVector vXS;
	iVector vES;
	mesh.getMeshData(vXS, vES);
	toEigen(vXS, vX);
}

void RodMeshSimulator::ComputeLaplacianMatrix(const RodMesh& mesh, MatrixSd& mL)
{
	// Compute Laplacian matrix

	int numCon = mesh.getNumCon();

	tVector vlaplacian;
	vlaplacian.reserve((5 * numCon) * 3);
	for (int i = 0; i < numCon; ++i)
	{
		// Add central node coeff.

		for (int j = 0; j < 3; ++j)
		{
			vlaplacian.push_back(Triplet<Real>(3 * i + j, 3 * i + j, 1));
		}

		// Add neighbor node coeff.

		const iVector& vconNodes = mesh.getConNodes(i);
		int numNeighbor = (int) mesh.getConEdges(i).size();
		Real coeff = -1.0 / (Real)numNeighbor;
		for (int k = 0; k < numNeighbor; ++k)
		{
			iVector vconNodeSimDOF;
			iVector vconNodeRawDOF;
			vconNodeRawDOF = m_pModel->getNodeRawDOF(vconNodes[2*k+1]);
			this->m_pModel->mapRaw2Sim(vconNodeRawDOF, vconNodeSimDOF);
			for (int j = 0; j < 3; ++j)
			{
				vlaplacian.push_back(Triplet<Real>(3 * i + j, vconNodeSimDOF[j], coeff));
			}
		}
	}

	mL = MatrixSd(3*numCon, 3*numCon);
	mL.setFromTriplets(
		vlaplacian.begin(),
		vlaplacian.end());
	mL.makeCompressed();
}

void RodMeshSimulator::Transform_Default2Normalized(const VectorXd& vpDef, const VectorXd& vpMin, const VectorXd& vpRan, VectorXd& vpNor)
{
	int fullSize = (int)vpDef.size();
	int blockSize = (int)vpMin.size();
	assert(vpRan.size() == blockSize);
	assert(fullSize % blockSize == 0);

	int numBlocks = fullSize / blockSize;

	vpNor.resize(fullSize);

	for (int i = 0; i < numBlocks; ++i)
	{
		vpNor.block(i*blockSize, 0, blockSize, 1) = (vpDef.block(i*blockSize, 0, blockSize, 1) - vpMin).cwiseProduct(vpRan.cwiseInverse());
	}
}

void RodMeshSimulator::Transform_Normalized2Default(const VectorXd& vpNor, const VectorXd& vpMin, const VectorXd& vpRan, VectorXd& vpDef)
{
	int fullSize = (int)vpNor.size();
	int blockSize = (int)vpMin.size();
	assert(vpRan.size() == blockSize);
	assert(fullSize % blockSize == 0);

	int numBlocks = fullSize / blockSize;

	vpDef.resize(fullSize);

	for (int i = 0; i < numBlocks; ++i)
	{
		vpDef.block(i*blockSize, 0, blockSize, 1) = vpNor.block(i*blockSize, 0, blockSize, 1).cwiseProduct(vpRan) + vpMin;
	}
}

void RodMeshSimulator::Assemble_DNormalizedDDefault(int numBlocks, const VectorXd& vran, MatrixSd& DnDd)
{
	int numVals = vran.size();
	int numRows = numBlocks*numVals;
	int numCols = numBlocks*numVals;

	tVector vDnDd;
	vDnDd.reserve(numRows);
	for (int i = 0; i < numBlocks; ++i)
	{
		int valOffset = i*numVals;
		for (int j = 0; j < numVals; ++j)
			vDnDd.push_back(Triplet<Real>(valOffset + j, valOffset + j, 1.0 / vran(j)));
	}

	DnDd = MatrixSd(numRows, numCols);
	DnDd.setFromTriplets(
		vDnDd.begin(),
		vDnDd.end());
	DnDd.makeCompressed();

	logSimu("[INFO] DNormalizedDDefault assembled with %i triplets", (int)vDnDd.size());
}

void RodMeshSimulator::Assemble_DDefaultDNormalized(int numBlocks, const VectorXd& vran, MatrixSd& DdDn)
{
	int numVals = vran.size();
	int numRows = numBlocks*numVals;
	int numCols = numBlocks*numVals;

	tVector vDdDn;
	vDdDn.reserve(numRows);
	for (int i = 0; i < numBlocks; ++i)
	{
		int valOffset = i*numVals;
		for (int j = 0; j < numVals; ++j)
			vDdDn.push_back(Triplet<Real>(valOffset + j, valOffset + j, vran(j)));
	}

	DdDn = MatrixSd(numRows, numCols);
	DdDn.setFromTriplets(
		vDdDn.begin(),
		vDdDn.end());
	DdDn.makeCompressed();

	logSimu("[INFO] DDefaultDNormalized assembled with %i triplets", (int) vDdDn.size());
}

void RodMeshSimulator::Assemble_DParametersDFabrication(const vector<MatrixXd>& vDpDfBlock, MatrixSd& DpDf)
{
	int numBlocks = vDpDfBlock.size();
	int blockRows = vDpDfBlock[0].rows();
	int blockCols = vDpDfBlock[0].cols();
	int numRows = numBlocks*blockRows;
	int numCols = numBlocks*blockCols;

	tVector vDpDf;
	vDpDf.reserve(blockRows*blockCols*numBlocks);
	for (int i = 0; i < numBlocks; ++i)
	{
		int rowOffset = i*blockRows;
		int colOffset = i*blockCols;
		for (int j = 0; j < blockRows; ++j)
			for (int k = 0; k < blockCols; ++k)
				vDpDf.push_back(Triplet<Real>(rowOffset + j, colOffset + k, vDpDfBlock[i](j, k)));
	}

	DpDf = MatrixSd(numRows, numCols);
	DpDf.setFromTriplets(
		vDpDf.begin(),
		vDpDf.end());
	DpDf.makeCompressed();

	logSimu("[INFO] DSimulationDFabrication assembled with %i triplets", (int)vDpDf.size());
}

int RodMeshSimulator::ComputeResidual_Laplacian(const VectorXd& params, VectorXd& residual)
{
	logSimu("");
	logSimu("===================== JSandbox trace begin ====================");

	logSimu("---------------------------------------------------------------");
	logSimu("---------------------- Computing residual ---------------------");
	logSimu("---------------------------------------------------------------");

	logSimu("[INFO] Received parameters. Min: %f, Max: %f, Avg: %f", params.minCoeff(), params.maxCoeff(), params.mean());

	logSimu("[INFO] Recovering equilibrium state");
	this->m_pModel->setState_x(this->m_state);

	this->m_pDesign->setParameters(params); // Set parameters

	// Intialize to infinite: if the static equilibrium does
	// not converge, this would be the default solution. That
	// will force the optimizer to reject this solution.

	int N = (int) m_targetLap.size();

	// Check force residual

	Real forceNorm = this->ComputeForceResidual(this->m_pSolver, this->m_pModel);

	if (forceNorm > m_pSolver->getSolverError())
	{
		logSimu("[INFO] Force norm: %f. Computing static equilibrium", forceNorm);

		if (!SolveStaticEquilibrium(m_pSolver, m_pModel))
		{
			logSimu("[FAILURE] Static equilibrium error: returning infinity");

			return -1;
		}
		else
		{
			logSimu("[SUCCESS] Static equilibrium OK: computing residual");
		}
	}
	else
	{
		logSimu("[INFO] Force norm: %f. Model already in equilibrium", forceNorm);
	}

	// Compute residual: laplacian * (current - target)

	VectorXd vXE;
	ComputePositionVector(m_pModel->getSimMesh_x(), vXE);
	residual = (vXE - this->GetTargetCartesianCoords());

	logSimu("[INFO] Computed residual (cartesian). Norm: %f", residual.norm());

	residual = this->GetLaplacianMatrix()*residual;

	logSimu("[INFO] Computed residual (laplacian). Norm: %f", residual.norm());

	logSimu("===================== JSandbox trace end ======================");
	logSimu("");

	return 0;
}

int RodMeshSimulator::ComputeJacobian_Laplacian(const VectorXd& params, tVector& jacobian)
{
	logSimu("");
	logSimu("===================== JSandbox trace begin ====================");

	logSimu("---------------------------------------------------------------");
	logSimu("---------------------- Computing Jacobian ---------------------");
	logSimu("---------------------------------------------------------------");

	logSimu("[INFO] Received parameters. Min: %f, Max: %f, Avg: %f", params.minCoeff(), params.maxCoeff(), params.mean());

	this->m_pDesign->setParameters(params); // Set parameters

	jacobian.clear();

	// Check force residual

	Real forceNorm = this->ComputeForceResidual(this->m_pSolver, this->m_pModel);

	if (forceNorm > m_pSolver->getSolverError())
	{
		logSimu("[INFO] Force norm: %f. Computing static equilibrium", forceNorm);

		if (!SolveStaticEquilibrium(m_pSolver, m_pModel))
		{
			logSimu("[FAILURE] Static equilibrium error: returning infinity");

			return -1;
		}
		else
		{
			logSimu("[SUCCESS] Static equilibrium OK: computing residual");
		}
	}
	else
	{
		logSimu("[INFO] Force norm: %f. Model already in equilibrium", forceNorm);
	}

	logSimu("[INFO] Storing equilibrium state");
	m_state = m_pModel->getState_x()->clone();

	// Compute jacobian: laplacian * selection * shape derivatives

	MatrixSd jacMatrix = GetLaplacianMatrix()*m_pDesign->get_St()*m_pDesign->get_DxDp();

	// Convert matrix to triplets

	toTriplets(jacMatrix, jacobian);

	logSimu("[INFO] Computed Jacobian. Triplets: %i, Norm: %f", jacobian.size(), jacMatrix.norm());

	logSimu("===================== JSandbox trace end ======================");
	logSimu("");

	return 0;
}

int RodMeshSimulator::InitializeOptimization(RodMeshBasicModel* pModel, PhysSolver* pSolver,
	const VectorXd& targetPos,
	const VectorXd& vpMin, const VectorXd& vpRan,
	const VectorXd& vfMin, const VectorXd& vfRan,
	VectorXd initialPar)
{
	logSimu("");
	logSimu("==================== JSandbox trace begin =====================");

	logSimu("---------------------------------------------------------------");
	logSimu("------------------ Initializing optimization ------------------");
	logSimu("---------------------------------------------------------------");

	if (this->m_pModel != NULL)
		delete this->m_pModel;
	if (this->m_pSolver != NULL)
		delete this->m_pSolver;
	if (this->m_pDesign != NULL)
		delete this->m_pDesign;

	this->m_pModel = pModel;
	this->m_pSolver = pSolver;


	// Create design problem

	this->m_pDesign = this->CreateBasicProblem(pModel, pSolver, vpMin, vpMin + vpRan);

	if (initialPar.size() != 0)
	{
		this->m_pDesign->setParameters(initialPar);
	}
	else
	{
		initialPar = this->m_pDesign->getParameters();
	}

	logSimu("[INFO] Initial parameters. Min: %f, Max: %f, Avg: %f", initialPar.minCoeff(), initialPar.maxCoeff(), initialPar.mean());

	// Check force residual

	Real forceNorm = this->ComputeForceResidual(this->m_pSolver, this->m_pModel);

	if (forceNorm > m_pSolver->getSolverError())
	{
		logSimu("[INFO] Force norm: %f. Computing static equilibrium", forceNorm);

		if (!SolveStaticEquilibrium(m_pSolver, m_pModel))
		{
			logSimu("[FAILURE] Static equilibrium error: returning false");

			return -1;
		}
		else
		{
			logSimu("[SUCCESS] Static equilibrium OK: computing residual");
		}
	}
	else
	{
		logSimu("[INFO] Force norm: %f. Model already in equilibrium", forceNorm);
	}

	// Create Laplacian matrix

	this->ComputeLaplacianMatrix(this->m_pModel->getSimMesh_x(), *m_L);

	// Set target configurations
	
	const MatrixSd& L = this->GetLaplacianMatrix();

	m_targetCar = targetPos;
	m_targetLap = L*targetPos;

	// Create scale derivatives

	int numCon = this->m_pModel->getNumCon();
	this->Assemble_DDefaultDNormalized(numCon, vpRan, *this->m_DpdDpn);
	this->Assemble_DNormalizedDDefault(numCon, vfRan, *this->m_DfnDfd);

	// Store current state

	logSimu("[INFO] Storing equilibrium state");
	m_state = m_pModel->getState_x()->clone(); 

	logSimu("[INFO] Optimization initialized");

	logSimu("===================== JSandbox trace end ======================");
	logSimu("");

	return 0;
}

Real RodMeshSimulator::ComputeForceResidual(PhysSolver* pSolver, SolidModel* pModel)
{
	VectorXd vresidual;
	pSolver->getResidual(
		pModel->getPositions_x(),
		pModel->getVelocities_x(),
		vresidual);

	return vresidual.norm();
}

bool RodMeshSimulator::StepStaticEquilibrium(PhysSolver* pSolver, SolidModel* pModel)
{
	//pModel->testForceGlobal();
	//pModel->testJacobianGlobal();

	pSolver->advanceBoundary();

	if (!pSolver->solve())
	{
		logSimu("[FAILURE] Impossible to perform static step block\n");
		return false;
	}
	else
	{
		logSimu("[SUCCESS] Static equilibrium step block solved\n");
		return true;
	}
}

bool RodMeshSimulator::SolveStaticEquilibrium(PhysSolver* pSolver, SolidModel* pModel)
{
	int itCur = 0, itMax = 250;

	Real resNorm = HUGE_VAL;

	do
	{
		pSolver->advanceBoundary();
		bool OK = pSolver->solve();
		if (!OK)
		{
			//logSimu("[FAILURE] Impossible to perform static step block\n");
			return false;
		}
		else
		{
			//logSimu("[SUCCESS] Static equilibrium step block solved\n");
		}

		// Compute the new force residual value
		resNorm = ComputeForceResidual(pSolver, pModel);

		logSimu("--");
		
		//if (resNorm <= pSolver->getSolverError())
		//{
		//	logSimu("[INFO] Residual forces bellow solver tolerance");
		//}
		//else
		//{
		//	logSimu("[INFO] Residual forces above solver tolerance");
		//}

		//if (pSolver->isFullyLoaded())
		//{
		//	logSimu("[INFO] Solver IS fully loaded");
		//}
		//else
		//{
		//	logSimu("[INFO] Solver NOT fully loaded");
		//}

		// Check if max. iterations were reached
		itCur += pSolver->getSolverMaxIters();

		if (itCur >= itMax)
		{
			logSimu("[INFO] Maximum projection iterations reached");

			return false;
		}
	} 
	while (!pSolver->isFullyLoaded() || resNorm > pSolver->getSolverError());

	return true;
}

void RodMeshSimulator::CreateStraightRod(const Vector3d& a, const Vector3d& b, int numPoint, Rod& rodOutput)
{
	dVector vx(6);
	set3D(0, a, vx);
	set3D(1, b, vx);
	rodOutput = Rod(Curve(vx));
	rodOutput.resample(numPoint);
}

void RodMeshSimulator::TutorialTest1()
{
	////////////////////////////////////////////////
	//				Creating rod mesh
	////////////////////////////////////////////////


	//			    0               1
	//		0 ------------> 1 ------------> 2
	//		|				|				|
	//		|  6			|  7			|  8
	//		|				|				|
	//		v		2		v		3		v
	//		3 ------------> 4 ------------> 5
	//		|				|				|
	//		|  9			|  10			|  11
	//		|				|				|
	//		v		4		v		5		v
	//		6 ------------> 7 ------------> 8

	// Rod meshes are built as a collection of connected rods. Each rod can have an arbitrarily number of nodes so
	// you have to create rods separately. Rods are curves with an 3D frame adapte to each edge. You can just create
	// the curve and frames are initialized automatically to a twist-free parallel-transported frame. The frames of
	// the rods are aligned in the direction of the rod.

	// Nodes
	vector<Vector3d> vnodes;
	vnodes.push_back(Vector3d(-1, 0, 1));
	vnodes.push_back(Vector3d(0, 0, 1));
	vnodes.push_back(Vector3d(1, 0, 1));
	vnodes.push_back(Vector3d(-1, 0, 0));
	vnodes.push_back(Vector3d(0, 0, 0));
	vnodes.push_back(Vector3d(1, 0, 0));
	vnodes.push_back(Vector3d(-1, 0, -1));
	vnodes.push_back(Vector3d(0, 0, -1));
	vnodes.push_back(Vector3d(1, 0, -1));

	// Randomize a little bit
	for (int i = 0; i < 9; ++i)
	{
		vnodes[i] += Vector3d::Random()*0.50;
	}

	vector<Rod> vrods(12);
	this->CreateStraightRod(vnodes[0], vnodes[1], 2, vrods[0]);
	this->CreateStraightRod(vnodes[1], vnodes[2], 2, vrods[1]);
	this->CreateStraightRod(vnodes[3], vnodes[4], 2, vrods[2]);
	this->CreateStraightRod(vnodes[4], vnodes[5], 2, vrods[3]);
	this->CreateStraightRod(vnodes[6], vnodes[7], 2, vrods[4]);
	this->CreateStraightRod(vnodes[7], vnodes[8], 2, vrods[5]);
	this->CreateStraightRod(vnodes[0], vnodes[3], 2, vrods[6]);
	this->CreateStraightRod(vnodes[1], vnodes[4], 2, vrods[7]);
	this->CreateStraightRod(vnodes[2], vnodes[5], 2, vrods[8]);
	this->CreateStraightRod(vnodes[3], vnodes[6], 2, vrods[9]);
	this->CreateStraightRod(vnodes[4], vnodes[7], 2, vrods[10]);
	this->CreateStraightRod(vnodes[5], vnodes[8], 2, vrods[11]);

	// You can get the internal curve (points)

	const Curve& curve = vrods[0].getCurve();

	// And also check the frame adapted to each edge

	const vector<Frame3d> vframes = vrods[0].getFrames();

	// Frames contain tangent, normal and binormal always in left-hand form:
	//		tan = nor x bin
	//		bin = tan x nor
	//		nor = bin x tan

	RodMesh rodMesh;

	// You can create the rod mesh just by passing through the rod vector as a parameter. Then,
	// connections are initialized by detecting rod extremes that are colliding with each other.
	// However, then, it is not possible to keep track of each individual connection and we will
	// need that for mapping quads to connections.

	rodMesh = RodMesh(vrods);

	// Alternatively, you can also provide the connections. Each connection contains a set of 
	// pair<int,int>. The first value indicates the index of the connected rod. The second
	// value indicates the extreme of the rod that is connected: 0 if the rod is connected 
	// at its beginning (frame pointing outwards), 1 if the rod is connected at its end 
	// (frame pointing inwards).

	vector<Con> vcons(9);
	// Con 0
	vcons[0].m_srods.insert(pair<int, int>(0, 0));
	vcons[0].m_srods.insert(pair<int, int>(6, 0));
	// Con 1
	vcons[1].m_srods.insert(pair<int, int>(0, 1));
	vcons[1].m_srods.insert(pair<int, int>(1, 0));
	vcons[1].m_srods.insert(pair<int, int>(7, 0));
	// Con 2
	vcons[2].m_srods.insert(pair<int, int>(1, 1));
	vcons[2].m_srods.insert(pair<int, int>(8, 0));
	// Con 3
	vcons[3].m_srods.insert(pair<int, int>(2, 0));
	vcons[3].m_srods.insert(pair<int, int>(6, 1));
	vcons[3].m_srods.insert(pair<int, int>(9, 0));
	// Con 4
	vcons[4].m_srods.insert(pair<int, int>(2, 1));
	vcons[4].m_srods.insert(pair<int, int>(3, 0));
	vcons[4].m_srods.insert(pair<int, int>(7, 1));
	vcons[4].m_srods.insert(pair<int, int>(10, 0));
	// Con 5
	vcons[5].m_srods.insert(pair<int, int>(3, 1));
	vcons[5].m_srods.insert(pair<int, int>(8, 1));
	vcons[5].m_srods.insert(pair<int, int>(11, 0));
	// Con 6
	vcons[6].m_srods.insert(pair<int, int>(4, 0));
	vcons[6].m_srods.insert(pair<int, int>(9, 1));
	// Con 7
	vcons[7].m_srods.insert(pair<int, int>(4, 1));
	vcons[7].m_srods.insert(pair<int, int>(5, 0));
	vcons[7].m_srods.insert(pair<int, int>(10, 1));
	// Con 8
	vcons[8].m_srods.insert(pair<int, int>(5, 1));
	vcons[8].m_srods.insert(pair<int, int>(11, 1));

	// Then, rod mesh creation results:

	rodMesh = RodMesh(vrods, vcons);

	////////////////////////////////////////////////
	//				Getting out data
	////////////////////////////////////////////////

	// Please, note that because we need it to be this way for another project, nodes and edges belong to each rod.
	// Hence the list of node and edges of the rod mesh stores all the nodes of a connection separately. The indexing
	// works as follows: [Nodes_Rod_0] [Nodes_Rod_1] ... [Nodes_Rod_N] and [Edges_Rod_0] [Edges_Rod_1] ... [Edges_Rod_N]

	// You can get the 3D position of a connection

	Vector3d vcon1pos = rodMesh.getConPoint(1);

	// You can get all node indices afecting a connection, that is, the nodes around the connected edge.
	// From each pair of nodes, the first one of them corresponds to the connection node itself, while the
	// other one is the other node of the edge. Ej: for connection 1: [[1, 0], [2, 3], [14, 15]]

	iVector vcon1NodeIdx = rodMesh.getConNodes(1);

	// You can extract the position of any node, all the following positions should be the same as vcon0pos

	assert((rodMesh.getPoint(vcon1NodeIdx[0]) - vcon1pos).isZero(1e-9)); // This should be the same
	assert((rodMesh.getPoint(vcon1NodeIdx[2]) - vcon1pos).isZero(1e-9)); // This should be the same
	assert((rodMesh.getPoint(vcon1NodeIdx[4]) - vcon1pos).isZero(1e-9)); // This should be the same

	// By default, nodes and edges are indexed globally (mesh-basis). You can change to a rod-basis using:

	int rodIdxForNode, nodeIdx;

	rodMesh.getRodNodeForMeshNode(vcon1NodeIdx[0], rodIdxForNode, nodeIdx);
	Rod& rod0 = rodMesh.getRod(rodIdxForNode); // Get rod
	Vector3d pos0 = rod0.getPosition(nodeIdx); // Get rod node
	assert((pos0 - vcon1pos).isZero(1e-9)); // This should be the same

	rodMesh.getRodNodeForMeshNode(vcon1NodeIdx[2], rodIdxForNode, nodeIdx);
	Rod& rod1 = rodMesh.getRod(rodIdxForNode); // Get rod
	Vector3d pos1 = rod1.getPosition(nodeIdx); // Get rod node
	assert((pos1 - vcon1pos).isZero(1e-9)); // This should be the same

	rodMesh.getRodNodeForMeshNode(vcon1NodeIdx[4], rodIdxForNode, nodeIdx);
	Rod& rod7 = rodMesh.getRod(rodIdxForNode); // Get rod
	Vector3d pos7 = rod1.getPosition(nodeIdx); // Get rod node
	assert((pos7 - vcon1pos).isZero(1e-9)); // This should be the same

	// You can also get access to the edges (mesh-basis indexed) incident to a specific connection using:

	iVector vcon0EdgeIdx = rodMesh.getConEdges(0);
	Frame3d frame01 = rodMesh.getFrame(vcon0EdgeIdx[0]);
	Frame3d frame03 = rodMesh.getFrame(vcon0EdgeIdx[1]);

	// This indices are sorted in rod index ascending order. You can retrieve the rod 
	// corresponding to a global edge index in the same way that was done with nodes above.

	int rodIdxForEdge, edgeIdx;

	rodMesh.getRodEdgeForMeshEdge(vcon0EdgeIdx[0], rodIdxForEdge, edgeIdx);
	Frame3d frame01_test = rodMesh.getRod(rodIdxForEdge).getFrame(edgeIdx);
	assert((frame01_test.tan - frame01.tan).isZero(1e-9)); // Should be the same

	rodMesh.getRodEdgeForMeshEdge(vcon0EdgeIdx[1], rodIdxForEdge, edgeIdx);
	Frame3d frame03_test = rodMesh.getRod(rodIdxForEdge).getFrame(edgeIdx);
	assert((frame03_test.tan - frame03.tan).isZero(1e-9)); // Should be the same

	////////////////////////////////////////////////
	//			Initializing Frames
	////////////////////////////////////////////////

	// The rod mesh above was created with rods whose frames had been initialized somehow randomly. You would
	// like to initialize the frames in such a way that they make some sense (e.g. if the rod mesh resemble a
	// sphere, it might be preferable that all the normals point outwards). The RodMesh class provides a way
	// of initializing frames as a best-fit rotation from a given configuration of connection tangents. Let's
	// assume the known configuration is in the plane XY.

	vector<vector<Vector3d>> vtangents(9); // 9 connections

	// Con 0
	vtangents[0].push_back(Vector3d(1, 0, 0));		// 0 1
	vtangents[0].push_back(Vector3d(0, -1, 0));		// 0 3
	// Con 1
	vtangents[1].push_back(Vector3d(1, 0, 0));		// 0 1
	vtangents[1].push_back(Vector3d(1, 0, 0));		// 1 2
	vtangents[1].push_back(Vector3d(0, -1, 0));		// 1 4
	// Con 2
	vtangents[2].push_back(Vector3d(1, 0, 0));		// 1 2
	vtangents[2].push_back(Vector3d(0, -1, 0));		// 2 5
	// Con 3
	vtangents[3].push_back(Vector3d(1, 0, 0));		// 3 4
	vtangents[3].push_back(Vector3d(0, -1, 0));		// 0 3
	vtangents[3].push_back(Vector3d(0, -1, 0));		// 3 6
	// Con 4
	vtangents[4].push_back(Vector3d(1, 0, 0));		// 3 4
	vtangents[4].push_back(Vector3d(1, 0, 0));		// 4 5
	vtangents[4].push_back(Vector3d(0, -1, 0));		// 1 4
	vtangents[4].push_back(Vector3d(0, -1, 0));		// 4 7
	// Con 5
	vtangents[5].push_back(Vector3d(1, 0, 0));		// 4 5
	vtangents[5].push_back(Vector3d(0, -1, 0));		// 2 5
	vtangents[5].push_back(Vector3d(0, -1, 0));		// 5 8
	// Con 6
	vtangents[6].push_back(Vector3d(1, 0, 0));		// 6 7
	vtangents[6].push_back(Vector3d(0, -1, 0));		// 3 6
	// Con 7
	vtangents[7].push_back(Vector3d(1, 0, 0));		// 6 7
	vtangents[7].push_back(Vector3d(1, 0, 0));		// 7 8
	vtangents[7].push_back(Vector3d(0, -1, 0));		// 4 7
	// Con 8
	vtangents[8].push_back(Vector3d(1, 0, 0));		// 7 8
	vtangents[8].push_back(Vector3d(0, -1, 0));		// 5 8

	// NOTE: It is important to be consistent with the orientation of the tangents:
	// if the frame of an edge incident to a connection is pointing inwards, the 
	// specified tangent should point inwards, and viceversa.

	if (!RodMesh::InitializeFramesFromRotatedTangents(rodMesh, vtangents))
	{
		logSimu("[FAILURE] There was an error with frame initialization");
	}

	////////////////////////////////////////////////
	//			Creating the model
	////////////////////////////////////////////////

	// Defautl parameters

	SolidMaterial material;
	material.setStretchK(1.0); // Stretch
	material.setBendingKw(0.01); // Bending in normal direction
	material.setBendingKh(0.01); // Bending in binormal direction
	material.setTwistK(0.01); // Twist

	Vector3d vgravity = Vector3d(0.0, 0.0, 0.0);

	RodMeshModel* pModel = this->CreateModel(&rodMesh, material, vgravity);

	// The creation of the model makes a copy of the specified rod mesh. To retrieve the state
	// of the current simulated mesh, you can call the method getSimMesh_x(), for the deformed
	// configuration and getSimMesh_0() for the rest configuration. (NOTE: The rest configuration
	// is not being used for our application).

	RodMesh& mesh = pModel->getSimMesh_x();

	int numEdge = mesh.getNumEdge();
	int numNode = mesh.getNumNode();
	int numRod = mesh.getNumRod();

	// Within the simulation model, you can access:

	//DERModel* pRodModel = pModel->getRodModel(0); // Objects handling the mechanics/parameters of individual rods
	//DECModel* pConModel = pModel->getConModel(0); // Objects handling the mechanics/parameters of each connection

	////////////////////////////////////////////////
	//			Creating the solver
	////////////////////////////////////////////////

	// Create the solver specifying the number of iterations to solve in block (for incremental visualization),
	// and also the type of linear solver used to perform the step (Simplicial Cholesky and Conjugate Gradient
	// available).

	PhysSolver* pSolver = this->CreateSolver(pModel, 1, PhysSolver::LinearSolver::Cholesky);

	// Set the boundary conditions (fixed nodes/frames). Highly advisable to guarantee convergence.
	// It is not necessary to fix all the rod nodes incident in a connection, just one is enought

	iVector vfixedNodes;
	iVector vfixedEdges;
	vfixedNodes.push_back(5); // The central point

	//this->SetBoundaryConditions(pSolver, pModel, vfixedNodes, vfixedEdges);

	////////////////////////////////////////////////
	//			Changing parameters
	////////////////////////////////////////////////

	// Parameters available
	//		Affecting rods
	//		- stretch stiffness
	//		- edge rest length 
	//		Affecting cons
	//		- bending stiffness
	//		- twist stiffness
	//		- rest connection

	// SETTING REST LENGTH: Set the rest length of each rod

	VectorXd vL0(numRod);
	vL0.setConstant(2.0);
	pModel->setParam_RestLengthRod(vL0);

	// SETTING STRETCH K: Set the stretch K of each edge

	//VectorXd vK(numEdge);
	//vK.setConstant(1.0);
	//pModel->setParam_StretchK(vK);

	// PARAMETERS per connection

	//int numConRods = mesh.getCon(0).m_srods.size();

	// SETTING BENDING K: Set the bending K for each incident edge of each connection 
	// (generally constant per connection). If not specified, all are set to the default.

	//vector<VectorXd> vKb(numConRods);
	//for (int i = 0; i < numConRods; ++i)
	//{  
	//	vKb[i].resize(2);
	//	vKb[i][0] = 0.0; // Normal direction
	//	vKb[i][1] = 0.0; // Binormal direction
	//}
	//pModel->getConModel(0)->setParam_BendingK(vKb);

	// SETTING TWIST K: Set the twist K for each incident edge of each connection 
	// (generally constant per connection). If not specified, all are set to the default.

	//vector<VectorXd> vKt(numConRods);
	//for (int i = 0; i < numConRods; ++i)
	//{
	//	vKt[i].resize(1);
	//	vKt[i][0] = 0.0;
	//}
	//pModel->getConModel(0)->setParam_TwistK(vKt);

	// SETTING CONNECTION REST STATE: Set the rest state of the connections using tangents (assume planar configuration)

	int numCon = mesh.getNumCon();
	for (int i = 0; i < numCon; ++i)
		pModel->getConModel(i)->setParam_RestTangents(vtangents[i]);

	////////////////////////////////////////////////
	//			Solve static equilibrium
	////////////////////////////////////////////////

	// Solve a few iterations of the static equilibrium.

	if (!StepStaticEquilibrium(pSolver, pModel))
	{
		logSimu("[FAILURE] Impossible to step static equilibrium");
	}

	// Solve the full static equilibrium until convergence.

	if (!SolveStaticEquilibrium(pSolver, pModel))
	{
		logSimu("[FAILURE] Impossible to solve static equilibrium");
	}

	delete pSolver;
	delete pModel;

}

void RodMeshSimulator::TutorialTest2()
{
	////////////////////////////////////////////////
	//				Creating rod mesh
	////////////////////////////////////////////////


	//			    0               1
	//		0 ------------> 1 ------------> 2
	//		|				|				|
	//		|  6			|  7			|  8
	//		|				|				|
	//		v		2		v		3		v
	//		3 ------------> 4 ------------> 5
	//		|				|				|
	//		|  9			|  10			|  11
	//		|				|				|
	//		v		4		v		5		v
	//		6 ------------> 7 ------------> 8

	// Rod meshes are built as a collection of connected rods. Each rod can have an arbitrarily number of nodes so
	// you have to create rods separately. Rods are curves with an 3D frame adapte to each edge. You can just create
	// the curve and frames are initialized automatically to a twist-free parallel-transported frame. The frames of
	// the rods are aligned in the direction of the rod.

	// Nodes
	vector<Vector3d> vnodes;
	vnodes.push_back(Vector3d(-1, 1, 0));
	vnodes.push_back(Vector3d(0, 1, 0));
	vnodes.push_back(Vector3d(1, 1, 0));
	vnodes.push_back(Vector3d(-1, 0, 0));
	vnodes.push_back(Vector3d(0, 0, 0));
	vnodes.push_back(Vector3d(1, 0, 0));
	vnodes.push_back(Vector3d(-1, -1, 0));
	vnodes.push_back(Vector3d(0, -1, 0));
	vnodes.push_back(Vector3d(1, -1, 0));

	// Randomize a little bit
	for (int i = 0; i < 9; ++i)
	{
		vnodes[i] += Vector3d::Random()*0.50;
	}

	vector<Rod> vrods(12);
	this->CreateStraightRod(vnodes[0], vnodes[1], 2, vrods[0]);
	this->CreateStraightRod(vnodes[1], vnodes[2], 2, vrods[1]);
	this->CreateStraightRod(vnodes[3], vnodes[4], 2, vrods[2]);
	this->CreateStraightRod(vnodes[4], vnodes[5], 2, vrods[3]);
	this->CreateStraightRod(vnodes[6], vnodes[7], 2, vrods[4]);
	this->CreateStraightRod(vnodes[7], vnodes[8], 2, vrods[5]);
	this->CreateStraightRod(vnodes[0], vnodes[3], 2, vrods[6]);
	this->CreateStraightRod(vnodes[1], vnodes[4], 2, vrods[7]);
	this->CreateStraightRod(vnodes[2], vnodes[5], 2, vrods[8]);
	this->CreateStraightRod(vnodes[3], vnodes[6], 2, vrods[9]);
	this->CreateStraightRod(vnodes[4], vnodes[7], 2, vrods[10]);
	this->CreateStraightRod(vnodes[5], vnodes[8], 2, vrods[11]);

	// You can get the internal curve (points)

	const Curve& curve = vrods[0].getCurve();

	// And also check the frame adapted to each edge

	const vector<Frame3d> vframes = vrods[0].getFrames();

	// Frames contain tangent, normal and binormal always in left-hand form:
	//		tan = nor x bin
	//		bin = tan x nor
	//		nor = bin x tan

	RodMesh rodMesh;

	// You can create the rod mesh just by passing through the rod vector as a parameter. Then,
	// connections are initialized by detecting rod extremes that are colliding with each other.
	// However, then, it is not possible to keep track of each individual connection and we will
	// need that for mapping quads to connections.

	rodMesh = RodMesh(vrods);

	// Alternatively, you can also provide the connections. Each connection contains a set of 
	// pair<int,int>. The first value indicates the index of the connected rod. The second
	// value indicates the extreme of the rod that is connected: 0 if the rod is connected 
	// at its beginning (frame pointing outwards), 1 if the rod is connected at its end 
	// (frame pointing inwards).

	vector<Con> vcons(9);
	// Con 0
	vcons[0].m_srods.insert(pair<int, int>(0, 0));
	vcons[0].m_srods.insert(pair<int, int>(6, 0));
	// Con 1
	vcons[1].m_srods.insert(pair<int, int>(0, 1));
	vcons[1].m_srods.insert(pair<int, int>(1, 0));
	vcons[1].m_srods.insert(pair<int, int>(7, 0));
	// Con 2
	vcons[2].m_srods.insert(pair<int, int>(1, 1));
	vcons[2].m_srods.insert(pair<int, int>(8, 0));
	// Con 3
	vcons[3].m_srods.insert(pair<int, int>(2, 0));
	vcons[3].m_srods.insert(pair<int, int>(6, 1));
	vcons[3].m_srods.insert(pair<int, int>(9, 0));
	// Con 4
	vcons[4].m_srods.insert(pair<int, int>(2, 1));
	vcons[4].m_srods.insert(pair<int, int>(3, 0));
	vcons[4].m_srods.insert(pair<int, int>(7, 1));
	vcons[4].m_srods.insert(pair<int, int>(10, 0));
	// Con 5
	vcons[5].m_srods.insert(pair<int, int>(3, 1));
	vcons[5].m_srods.insert(pair<int, int>(8, 1));
	vcons[5].m_srods.insert(pair<int, int>(11, 0));
	// Con 6
	vcons[6].m_srods.insert(pair<int, int>(4, 0));
	vcons[6].m_srods.insert(pair<int, int>(9, 1));
	// Con 7
	vcons[7].m_srods.insert(pair<int, int>(4, 1));
	vcons[7].m_srods.insert(pair<int, int>(5, 0));
	vcons[7].m_srods.insert(pair<int, int>(10, 1));
	// Con 8
	vcons[8].m_srods.insert(pair<int, int>(5, 1));
	vcons[8].m_srods.insert(pair<int, int>(11, 1));

	// Then, rod mesh creation results:

	rodMesh = RodMesh(vrods, vcons);

	////////////////////////////////////////////////
	//			Initializing Frames
	////////////////////////////////////////////////

	// The rod mesh above was created with rods whose frames had been initialized somehow randomly. You would
	// like to initialize the frames in such a way that they make some sense (e.g. if the rod mesh resemble a
	// sphere, it might be preferable that all the normals point outwards). The RodMesh class provides a way
	// of initializing frames as a best-fit rotation from a given configuration of connection tangents. Let's
	// assume the known configuration is in the plane XY.

	vector<vector<Vector3d>> vtangents(9); // 9 connections

													// Con 0
	vtangents[0].push_back(Vector3d(1, 0, 0));		// 0 1
	vtangents[0].push_back(Vector3d(0, -1, 0));		// 0 3
													// Con 1
	vtangents[1].push_back(Vector3d(1, 0, 0));		// 0 1
	vtangents[1].push_back(Vector3d(1, 0, 0));		// 1 2
	vtangents[1].push_back(Vector3d(0, -1, 0));		// 1 4
													// Con 2
	vtangents[2].push_back(Vector3d(1, 0, 0));		// 1 2
	vtangents[2].push_back(Vector3d(0, -1, 0));		// 2 5
													// Con 3
	vtangents[3].push_back(Vector3d(1, 0, 0));		// 3 4
	vtangents[3].push_back(Vector3d(0, -1, 0));		// 0 3
	vtangents[3].push_back(Vector3d(0, -1, 0));		// 3 6
													// Con 4
	vtangents[4].push_back(Vector3d(1, 0, 0));		// 3 4
	vtangents[4].push_back(Vector3d(1, 0, 0));		// 4 5
	vtangents[4].push_back(Vector3d(0, -1, 0));		// 1 4
	vtangents[4].push_back(Vector3d(0, -1, 0));		// 4 7
													// Con 5
	vtangents[5].push_back(Vector3d(1, 0, 0));		// 4 5
	vtangents[5].push_back(Vector3d(0, -1, 0));		// 2 5
	vtangents[5].push_back(Vector3d(0, -1, 0));		// 5 8
													// Con 6
	vtangents[6].push_back(Vector3d(1, 0, 0));		// 6 7
	vtangents[6].push_back(Vector3d(0, -1, 0));		// 3 6
													// Con 7
	vtangents[7].push_back(Vector3d(1, 0, 0));		// 6 7
	vtangents[7].push_back(Vector3d(1, 0, 0));		// 7 8
	vtangents[7].push_back(Vector3d(0, -1, 0));		// 4 7
													// Con 8
	vtangents[8].push_back(Vector3d(1, 0, 0));		// 7 8
	vtangents[8].push_back(Vector3d(0, -1, 0));		// 5 8

	// NOTE: It is important to be consistent with the orientation of the tangents:
	// if the frame of an edge incident to a connection is pointing inwards, the 
	// specified tangent should point inwards, and viceversa.

	if (!RodMesh::InitializeFramesFromRotatedTangents(rodMesh, vtangents))
	{
		logSimu("[FAILURE] There was an error with frame initialization");
	}

	////////////////////////////////////////////////
	//			Creating the model
	////////////////////////////////////////////////

	// Defautl parameters

	SolidMaterial material;
	material.setStretchK(1.0); // Stretch
	material.setBendingKw(0.01); // Bending in normal direction
	material.setBendingKh(0.01); // Bending in binormal direction
	material.setTwistK(0.01); // Twist

	Vector3d vgravity = Vector3d(0.0, 0.0, 0.0);

	RodMeshBasicModel* pModel = this->CreateBasicModel(&rodMesh, material, vgravity);

	// The creation of the model makes a copy of the specified rod mesh. To retrieve the state
	// of the current simulated mesh, you can call the method getSimMesh_x(), for the deformed
	// configuration and getSimMesh_0() for the rest configuration. (NOTE: The rest configuration
	// is not being used for our application).

	RodMesh& mesh = pModel->getSimMesh_x();

	int numEdge = mesh.getNumEdge();
	int numNode = mesh.getNumNode();
	int numRod = mesh.getNumRod();
	int numCon = mesh.getNumCon();

	////////////////////////////////////////////////
	//			Creating the solver
	////////////////////////////////////////////////

	// Create the solver specifying the number of iterations to solve in block (for incremental visualization),
	// and also the type of linear solver used to perform the step (Simplicial Cholesky and Conjugate Gradient
	// available).

	PhysSolver* pSolver = this->CreateSolver(pModel, 1, PhysSolver::LinearSolver::Cholesky);

	// Set the boundary conditions (fixed nodes/frames). Highly advisable to guarantee convergence.
	// It is not necessary to fix all the rod nodes incident in a connection, just one is enought

	iVector vfixedNodes;
	iVector vfixedEdges;
	vfixedNodes.push_back(5); // The central point

	//this->SetBoundaryConditions(pSolver, pModel, vfixedNodes, vfixedEdges);

	////////////////////////////////////////////////
	//			Changing parameters
	////////////////////////////////////////////////

	// Parameters available
	//		- stretch stiffness
	//		- edge rest length 
	//		- bending stiffness
	//		- twist stiffness
	//		- rest connection

	// SETTING REST LENGTH: Set the rest length of each rod (SPLITTED IN TWO). Each rod is shared by two
	// quads. The segment of the rod contained in each quad is not exactly the same, it depends on the
	// relative sizes of the quads. This method must specify, for each rod, the lengths of these two
	// segments, a, b, such that the total rest length of the rod is a + b. For instance, for the rod
	// going from quad 1 to quad 2, you must specify the rod length as [a,b] where:
	//
	//	o-----------o---------------------o
	//  |        a  |	  b               |
	//  |	  1-----|-----------2		  |
	//  |			|					  |
	//  o-----------o---------------------o
	//
	// For a quick test, just specify for each rod [L/2,L/2], for a rest length of L.
	// For instance, the code below is setting the rest length of all the rods to 2.0.

	VectorXd vL0(2*numRod);
	vL0.setConstant(1.0);
	pModel->setParam_RestLengthRodSplit(vL0);

	// SETTING STRETCH K: Set the stretch K of each connectoin (ONE parameter for each connection, same creation order) 
	
	VectorXd vKs(numCon);
	vKs.setConstant(1.0);
	pModel->setParam_ConnectionStretchK(vKs);

	// SETTING TWIST K: Set the twist K of each connection (ONE parameter for each connection, same creation order)

	VectorXd vKt(numCon);
	vKt.setConstant(0.01);
	pModel->setParam_ConnectionTwistK(vKt);

	// SETTING BENDING K: Set the bending Kw and Kh of each connection (TWO parameters for each connection, same creation order)

	VectorXd vKb(2*numCon);
	vKb.setConstant(0.01);
	pModel->setParam_ConnectionBendingK(vKb);

	// SETTING CONNECTION REST STATE: Set the rest state of the connections using tangents (assume planar configuration, same creation order)

	pModel->setParam_RestTangents(vtangents);

	////////////////////////////////////////////////
	//			Solve static equilibrium
	////////////////////////////////////////////////

	// Solve a few iterations of the static equilibrium.

	if (!StepStaticEquilibrium(pSolver, pModel))
	{
		logSimu("[FAILURE] Impossible to step static equilibrium");
	}

	// Solve the full static equilibrium until convergence.

	if (!SolveStaticEquilibrium(pSolver, pModel))
	{
		logSimu("[FAILURE] Impossible to solve static equilibrium");
	}

	////////////////////////////////////////////////
	//			Create design problem
	////////////////////////////////////////////////

	// A design problem has two important set of variables:
	//		The position DoF, x, indicating the shape of the object in space
	//		The design parameters, p, indicating any value we want to optimize
	//
	// For our problem, with NumCon connections and NumRod rods:
	//		x has size 3*NumCon + NumRod + 3*NumCon, with format: [3D node, twist angles, 3D euler angles]
	//		p has size 4*NumCon, with format [[ParametersCon_0], [ParametersCon_1], ..., [ParametersCon_N]] 

	VectorXd vpMinDummy;
	VectorXd vpMaxDummy;
	ModelDesignProblem* pDesign = this->CreateBasicProblem(pModel, pSolver, vpMinDummy, vpMaxDummy);

	// Get/Set all parameters in a single parameter vector; this vector
	// contains all the parameters of our problem in the following format:
	// [[ParametersCon_0], [ParametersCon_1], ..., [ParametersCon_N]] with
	//
	// ParametersCon_i (size 4) = [kStretch, kTwist, kBendingW, kBendingH]

	VectorXd vp;
	vp = pDesign->getParameters();
	pDesign->setParameters(vp);

	// Get derivative of forces w.r.t. positions. This is the Hessian of the elastic energy,
	// the so-called Jacobian of the forces that is also used in simulation. For N = size(x), 
	// this is a NxN matrix that contains how the forces at each DOF change for a 
	// differential change in each DoF.

	const MatrixSd& mDfDx = pDesign->get_DfDx();

	// Get derivative of the forces w.r.t. parameters. This is the Jacobian of the forces
	// not w.r.t. to positions but w.r.t. model parameters. Equivalently, for N = size(x)
	// and M = size(p), this is a NxM matrix that contains how the forces at each DoF
	// change for a differential change in the parameters.

	const MatrixSd& mDfDp = pDesign->get_DfDp();

	// Get derivative of the DoF w.r.t. parameters, DxDp, is a NxM matrix indicating how the
	// shape in static equilibrium change with a differential change in the design parameters. 
	// 
	// This is internally computed using the implicit function theorem. This works as follows: 
	//
	// Assume a configuration of DoF x is in static equilibrium for some given parameters p, 
	// that means the net force is zero, f(x,p) = 0. For different parameter values p', the configuration 
	// of DoF at which the force is 0 is generally different x', f(x',p') = 0. In some sense, for each p, 
	// there exists an x such that f(x,p) = 0. Thus, we can understand x in static equilibrium as an 
	// implicit function of p: x(p), and write f(x(p),p) = 0. To compute the derivative of this
	// implicit function, DxDp:
	//
	// 1. Linearize f w.r.t. p using Taylor expansion: 
	//						f = f_0 + DfDx*DxDp*dp + DfDp*dp = 0
	//
	// 2. We know f_0 = 0 and dp can be factored out:
	//						DfDx*DxDp + DfDp = 0
	//
	// 3. Compute analytically DfDx and DfDp matrices
	//
	// 4. Substitute in the equation to calculate DxDp
	//					    DxDp = -(DfDx)^-1 * DfDp
	// This is done just by solving a linear system

	const MatrixSd& mDxDp = pDesign->get_DxDp();

	// With DxDp, we have an approximation of how the configuration x in static equilibrium
	// changes with parameters p. If we formulate any objetive function for the optimization
	// that depends on x, U(x(p)), then the gradient of the objective function w.r.t. the
	// parameters is easy to compute as:
	//
	//				DUDp = DUDx*DxDp
	//
	// Example: The simplest U is the distance to a target t: U(x(p),t) = 0.5*(S*x(p) - t)^2
	// 
	// Then the gradient is: DUDp = 0.5*2*(S*x(p) - t)*S*DxDp = (x(p) - t)*DxDp
	//
	// Here, S is a selection matrix of size TxN, where T is 3*NumCon. Consider that the
	// objective is only defined in terms of the positions of the connections but there
	// are more DoF in the simulation (twist angles at edges and rotation Euler angles).
	// That is  NumDoF = 3*NumCon + NumRest, NumRest = NumRod (twist) + 3*NumCon (Euler).
	//
	// Thus, in our case, S = [I, 0] with 
	//				I = 3*NumCon x 3*NumCon identity matrix
	//				0 = 3*NumCon x NumRest zero matrix

	const MatrixSd& S = pDesign->get_St();

	////////////////////////////////////////////////
	//			PArameter space change
	////////////////////////////////////////////////

	// We've got 4 parameters spaces:
	// Pd : Deformation model parameters 4*NumCon in some range [minP, minP + ranP]
	// Fd : Spiral fabrication parameters 3*NumCon in some range [minF, minF + ranF]
	// Pn : Normalized deformation model parameters 4*NumCon in [0,1]
	// Fn : Normalized spiral fabrication parameters 3*NumCon in [0,1]

	// The Radial Basis Network function will provide this ranges. For example:

	VectorXd vpMin = Vector4d(1, 1e-4, 1e-4, 1e-4);
	VectorXd vpRan = Vector4d(10, 1e-2, 1e-2, 1e-2);

	VectorXd vfMin = Vector3d(0.001, 0.01, 0);
	VectorXd vfRan = Vector3d(0.001, 0.03, 360);

	// RodMeshSimulator provides methods for changing between this sets of parameters.

	VectorXd vpNormal;
	vpNormal.setConstant(8, 0.5);
	VectorXd vpDefault;
	Transform_Normalized2Default(vpNormal, vpMin, vpRan, vpDefault);
	Transform_Default2Normalized(vpDefault, vpMin, vpRan, vpNormal);

	VectorXd vfNormal;
	vfNormal.setConstant(6, 0.5);
	VectorXd vfDefault;
	Transform_Normalized2Default(vfNormal, vfMin, vfRan, vfDefault);
	Transform_Default2Normalized(vfDefault, vfMin, vfRan, vfNormal);

	// ModelDesignProblem class allows to get/set and compute derivatives w.r.t. the parameters in Pd space. 

	vp = pDesign->getParameters();

	VectorXd vX;
	ComputePositionVector(pModel->getSimMesh_x(), vX);
	this->InitializeOptimization(pModel, pSolver,
								 vX,
								 vpMin, vpRan, 
								 vfMin, vfRan);

	VectorXd vr;
	this->ComputeResidual_Laplacian(vp, vr);
	tVector vJ;
	this->ComputeJacobian_Laplacian(vp, vJ);

	// To compute the Jacobian w.r.t. parameters in the normalized space Pn, we can multipy DrDpd * DpdDpn
	// This is easier to do building the global matrices and just multiplying once instead of using blocks.
	// This global matrices are internally created when you call InitializeOptimization and stored in the
	// RodMeshSimulator object.

	MatrixSd DrDpd(3*numCon, 4*numCon);
	DrDpd.setFromTriplets(
		vJ.begin(),
		vJ.end());
	DrDpd.makeCompressed();

	// Create map derivatives

	MatrixSd DpnDfn;

	// Local 4x3 Jacobians by Ceres
	vector<MatrixXd> vDpnDfn(numCon);
	for (int i = 0; i < numCon; ++i)
	{
		vDpnDfn[i] = MatrixXd(4,3);
		vDpnDfn[i].setRandom();
	}

	this->Assemble_DParametersDFabrication(vDpnDfn, DpnDfn);

	// Compute Jacobian

	MatrixSd DrDfn = DrDpd * (*m_DpdDpn) * DpnDfn;

	delete pModel;
	delete pSolver;
	delete pDesign;
}

void RodMeshSimulator::DemoHangingStrip(int x, int y, Real scale, Real Ks, Real Kbh, Real Kbw, Real Kt, Real mass)
{
	int numRods = y*(x - 1) + (y - 1)*x;

	vector<Rod> vrods(numRods);
	Vector3d dX(scale, 0.0, 0.0);
	Vector3d dY(0.0, scale, 0.0);

	int countRod = 0;

	// Build rods in x direction

	for (int i = 0; i < y; ++i)
	{
		for (int j = 0; j < x - 1; ++j)
		{
			Vector3d a = j*dX + i*dY;
			Vector3d b = (j + 1)*dX + i*dY;
			this->CreateStraightRod(a, b, 2, vrods[countRod++]);
		}
	}

	// Build rods in y direction

	for (int i = 0; i < (y - 1); ++i)
	{
		for (int j = 0; j < x; ++j)
		{
			Vector3d a = j*dX + i*dY;
			Vector3d b = j*dX + (i + 1)*dY;
			this->CreateStraightRod(a, b, 2, vrods[countRod++]);
		}
	}

	// Build connections

	int countCons = 0;

	int numCons = x*y;

	vector<Con> vcons(numCons);

	for (int i = 0; i < y; ++i)
	{
		for (int j = 0; j < x; ++j)
		{
			if (j != 0)
			{
				vcons[countCons].m_srods.insert(pair<int, int>((x - 1)*i + j - 1, 1));
			}

			if (j != x - 1)
			{
				vcons[countCons].m_srods.insert(pair<int, int>((x - 1)*i + j, 0));
			}

			if (i != 0)
			{
				vcons[countCons].m_srods.insert(pair<int, int>((x - 1)*y + (i - 1)*x + j, 1));
			}

			if (i != y - 1)
			{
				vcons[countCons].m_srods.insert(pair<int, int>((x - 1)*y + i*x + j, 0));
			}

			countCons++;
		}
	}

	// Create Rod Mesh

	RodMesh mesh(vrods, vcons);

	// Create basic model

	RodMeshBasicModel* pRMModel = new RodMeshBasicModel();
	SolidMaterial material;
	material.setDensity(0);
	material.setStretchK(Ks);
	material.setBendingKw(Kbw);
	material.setBendingKh(Kbh);
	material.setTwistK(Kt);
	Vector3d vgravity(0.0, 0.0, -9.8);
	pRMModel->configureMesh(mesh, mesh);
	pRMModel->configureMaterial(material);
	pRMModel->setGravity(vgravity);
	pRMModel->setup();
	double massPerSpiral = mass / numCons;
	VectorXd vmass(pRMModel->getNumSimDOF_x());
	vmass.setZero();
	for (int i = 0; i < numCons; ++i)
	{
		int offset = 3 * i;
		vmass[offset + 0] = massPerSpiral;
		vmass[offset + 1] = massPerSpiral;
		vmass[offset + 2] = massPerSpiral;
	}
	pRMModel->set_Mass(vmass);

	this->m_pModel = pRMModel;

	// Create basic solver

	m_pSolver = this->CreateSolver(m_pModel, 1, PhysSolver::LinearSolver::Cholesky);

	// Set the boundary conditions (fixed nodes/frames). Highly advisable to guarantee convergence.
	// It is not necessary to fix all the rod nodes incident in a connection, just one is enought

	iVector vfixedNodes;
	iVector vfixedEdges;
	vfixedNodes.push_back(0);
	vfixedNodes.push_back(1);
	vfixedNodes.push_back(2*(x - 1) + 0);
	vfixedNodes.push_back(2*(x - 1) + 1);

	this->SetBoundaryConditions(m_pSolver, pRMModel, vfixedNodes, vfixedEdges);
}

void RodMeshSimulator::SetBoundaryConditions(PhysSolver* pSolver, RodMeshBasicModel* pModel, const iVector& vnodes, const iVector& vedges)
{
	// Set boundary conditions

	dVector vvalues;
	iVector vindices;

	VectorXd vrawDof;
	pModel->getRawPositions_x(vrawDof);

	for (int i = 0; i < (int)vnodes.size(); ++i)
	{
		iVector vidx = pModel->getNodeRawDOF(vnodes[i]);
		for (int i = 0; i < 3; ++i)
		{
			vindices.push_back(vidx[i]);
			vvalues.push_back(vrawDof(vidx[i]));
		}
	}

	for (int i = 0; i < (int)vedges.size(); ++i)
	{
		iVector vidx = pModel->getEdgeRawDOF(vedges[i]);
		vindices.push_back(vidx[0]);
		vvalues.push_back(0);
	}

	VectorXd vvaluesEigen = toEigen(vvalues);
	VectorXi vindicesEigen = toEigen(vindices);
	BConditionFixed* pFixedBC = new BConditionFixed();
	pFixedBC->setValues(vvaluesEigen);
	pFixedBC->setIndices(vindicesEigen);
	pFixedBC->setCurrentStep(0);
	pFixedBC->setMaximumStep(1);
	pFixedBC->updateStencil(pModel);
	pSolver->addBCondition(pFixedBC);
}

void RodMeshSimulator::SetBoundaryConditions(PhysSolver* pSolver, RodMeshModel* pModel, const iVector& vnodes, const iVector& vedges)
{
	// Set boundary conditions

	dVector vvalues;
	iVector vindices;

	VectorXd vrawDof;
	pModel->getRawPositions_x(vrawDof);

	for (int i = 0; i < (int)vnodes.size(); ++i)
	{
		iVector vidx = pModel->getNodeRawDOF(vnodes[i]);
		for (int i = 0; i < 3; ++i)
		{
			vindices.push_back(vidx[i]);
			vvalues.push_back(vrawDof(vidx[i]));
		}
	}

	for (int i = 0; i < (int)vedges.size(); ++i)
	{
		iVector vidx = pModel->getEdgeRawDOF(vedges[i]);
		vindices.push_back(vidx[0]);
		vvalues.push_back(0);
	}

	VectorXd vvaluesEigen = toEigen(vvalues);
	VectorXi vindicesEigen = toEigen(vindices);
	BConditionFixed* pFixedBC = new BConditionFixed();
	pFixedBC->setValues(vvaluesEigen);
	pFixedBC->setIndices(vindicesEigen);
	pFixedBC->setCurrentStep(0);
	pFixedBC->setMaximumStep(1);
	pFixedBC->updateStencil(pModel);
	pSolver->addBCondition(pFixedBC);
}

RodMeshModel* RodMeshSimulator::CreateModel(RodMesh* pRodMesh, const SolidMaterial& material, const Vector3d& vgravity)
{
	logSimu("---------------------------------------------------------------");
	logSimu("---------------- Creating Rod Mesh model (new) ----------------");
	logSimu("---------------------------------------------------------------");

	RodMeshModel* pModel = new RodMeshModel();
	pModel->configureMesh(*pRodMesh, *pRodMesh);
	pModel->configureMaterial(material);
	pModel->setGravity(vgravity);
	pModel->setup();

	return pModel;
}

ModelDesignProblem* RodMeshSimulator::CreateBasicProblem(RodMeshBasicModel* pModel, PhysSolver* pSolver, const VectorXd& vpMin, const VectorXd& vpMax)
{
	logSimu("---------------------------------------------------------------");
	logSimu("------------------- Creating Design Problem -------------------");
	logSimu("---------------------------------------------------------------");

	ModelDesignProblem* pDesign = new ModelDesignProblem("BASICDESIGN");

	// Define parameter set

	vector<ParameterSet*> vpSets(1);
	ParameterSet_Basic* pParSet = new ParameterSet_Basic();
	vpSets[0] = pParSet;
	pParSet->setIsBoxed(true);
	if (vpMin.size() != 0)
	{
		pParSet->setMinimumValues(vpMin);
	}
	else
	{
		pParSet->setMinimumValue(0);
	}
	if (vpMax.size() != 0)
	{
		pParSet->setMaximumValues(vpMax);
	}
	else
	{
		pParSet->setMaximumValue(HUGE_VAL);
	}

	iVector vfixed; // No fixed parameter
	pParSet->setFixedParameters(vfixed);
	pParSet->setModel(pModel);
	pParSet->setup();

	// Add parameter set

	pDesign->setParameterSets(vpSets);

	// Set model and solver

	pDesign->setModel(pModel);
	pDesign->setSolver(pSolver);

	// Setup design

	pDesign->setup();

	// Define fixed DOF (DOF defined fixed in the solver)

	pDesign->setDOFFixed(pSolver->getFixedIndices());

	// Define target DOF (only nodes, not edges or rotations)

	int numNodeDOF = 3 * pModel->getSimMesh_x().getNumNode();
	iVector vtargetNodeDOF(numNodeDOF);
	for (int i = 0; i < numNodeDOF; ++i)
		vtargetNodeDOF[i] = i;

	pDesign->setDOFTargets(vtargetNodeDOF);

	return pDesign;
}

RodMeshBasicModel* RodMeshSimulator::CreateBasicModel(RodMesh* pRodMesh, const SolidMaterial& material, const Vector3d& vgravity)
{
	logSimu("---------------------------------------------------------------");
	logSimu("---------------- Creating Rod Mesh model (new) ----------------");
	logSimu("---------------------------------------------------------------");

	RodMeshBasicModel* pModel = new RodMeshBasicModel();
	pModel->configureMesh(*pRodMesh, *pRodMesh);
	pModel->configureMaterial(material);
	pModel->setGravity(vgravity);
	pModel->setup();

	return pModel;
}

PhysSolver* RodMeshSimulator::CreateSolver(SolidModel* pModel, int steps, PhysSolver::LinearSolver solverType)
{
	logSimu("---------------------------------------------------------------");
	logSimu("-------------------- Creating static solver -------------------");
	logSimu("---------------------------------------------------------------");

	PhysSolver* pSolver = new SolverGenericDynSta();

	// Configure the solver

	pSolver->setIsNonLin(true);
	pSolver->setIsStatic(true);
	pSolver->setSolverError(1e-4);
	pSolver->setLinearError(1e-9);
	pSolver->setSolverMaxIters(steps);
	pSolver->setLinearMaxIters(5000);
	pSolver->hasMaximumStepSize(true);
	pSolver->setMaximumStepSize(1.0);
	pSolver->setLineSearchFactor(0.5);
	pSolver->setRegularizeMaxIters(10);
	pSolver->setLineSearchMaxIters(10);
	pSolver->setRegularizeFactor(1e-3);
	pSolver->setLinearSolver(PhysSolver::LinearSolver::Conjugate);

	// Setup

	pSolver->setModel(pModel);

	pSolver->setup();

	return pSolver;
}