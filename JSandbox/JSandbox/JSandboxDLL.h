/*=====================================================================================*/
/*! 
/file		JSandboxDLL.h
/author		jesusprod
/brief		Describes how to export JSandbox core.
 */
/*=====================================================================================*/

#ifndef JSANDBOX_DLL_H
#define JSANDBOX_DLL_H

#if _MSC_VER > 1000
#pragma once 
#endif

////JSandbox
//
//#ifdef P3_OS_WIN
//	#ifdef JSANDBOX_BUILD_DLL
//#define   __declspec(dllexport)
//	#else
//#define   __declspec(dllimport)
//	#endif
//#else
//	#define  
//#endif

#endif
