/*=====================================================================================*/
/*! 
/file		MathUtils.cpp
/author		jesusprod
/brief		Implementation of MathUtils.h
 */
/*=====================================================================================*/

#include <JSandbox/MathUtils.h>

#include <JSandbox/Maple/rodriguesRotation.h>

// Number utilities

bool isApprox(double a, double val, double tol)
{
	return abs(a - val) <= tol;
}

bool isApprox(const dVector& va, double val, double tol)
{
	for (size_t i = 0; i < va.size(); ++i)
		if (abs(va[i] - val) > tol)
			return false;
	return true;
}

// Mesh utilities	

bool isTriangleMesh(const dVector& vpos, const iVector& vidx)
{
	int nv = (int) vpos.size();
	int ni = (int) vidx.size();
	return nv >= 3 && nv % 3 == 0 &&
		   ni >= 3 && ni % 3 == 0;
}

void computeMeshNormalsArea(const dVector& vpos, const iVector& vidx, dVector& vnor)
{
	assert(isTriangleMesh(vpos, vidx));

	int nv = (int) vpos.size()/3;
	int nf = (int) vidx.size()/3;

	vnor.clear();
	vnor.resize(3*nv);
	vector<Eigen::Vector3d> vnor3D(nv);
	for (int i = 0; i < (int) vnor3D.size(); ++i)
		vnor3D[i].setZero(); // Initialize to zero

	for (int i = 0; i < nf; ++i)
	{
		int offset = 3 * i;
		int v0 = vidx[offset + 0];
		int v1 = vidx[offset + 1];
		int v2 = vidx[offset + 2];

		Eigen::Vector3d p0(vpos[3 * v0], vpos[3 * v0 + 1], vpos[3 * v0 + 2]);
		Eigen::Vector3d p1(vpos[3 * v1], vpos[3 * v1 + 1], vpos[3 * v1 + 2]);
		Eigen::Vector3d p2(vpos[3 * v2], vpos[3 * v2 + 1], vpos[3 * v2 + 2]);
		Eigen::Vector3d n = ((p1 - p0).cross(p2 - p0));

		vnor3D[v0] += n;
		vnor3D[v1] += n;
		vnor3D[v2] += n;
	}

	for (int i = 0; i < nv; ++i)
	{
		Eigen::Vector3d& n = vnor3D[i];
		double norm = n.norm();
		if (norm > EPS_APPROX)
			n /= norm;
		
		int offset = 3*i;
		vnor[offset + 0] = n(0);
		vnor[offset + 1] = n(1);
		vnor[offset + 2] = n(2);
	}
}

void computeMeshNormalsAngle(const dVector& vpos, const iVector& vidx, dVector& vnor)
{
	assert(isTriangleMesh(vpos, vidx));

	int nv = (int)vpos.size() / 3;
	int nf = (int)vidx.size() / 3;

	vnor.clear();
	vnor.resize(3 * nv);
	vector<Eigen::Vector3d> vnor3D(nv);
	for (int i = 0; i < (int)vnor3D.size(); ++i)
		vnor3D[i].setZero(); // Initialize to zero

	for (int i = 0; i < nf; ++i)
	{
		int offset = 3 * i;
		int v0 = vidx[offset + 0];
		int v1 = vidx[offset + 1];
		int v2 = vidx[offset + 2];

		Eigen::Vector3d p0(vpos[3 * v0], vpos[3 * v0 + 1], vpos[3 * v0 + 2]);
		Eigen::Vector3d p1(vpos[3 * v1], vpos[3 * v1 + 1], vpos[3 * v1 + 2]);
		Eigen::Vector3d p2(vpos[3 * v2], vpos[3 * v2 + 1], vpos[3 * v2 + 2]);
		Eigen::Vector3d n = ((p1 - p0).cross(p2 - p0)).normalized();

		double w0 = acos((p2 - p0).normalized().dot((p1 - p0).normalized()));
		double w1 = acos((p2 - p1).normalized().dot((p0 - p1).normalized()));
		double w2 = acos((p1 - p2).normalized().dot((p0 - p2).normalized()));
		//assert(w0 >= 0); 
		//assert(w1 >= 0);
		//assert(w2 >= 0); 
		//assert(abs(w0 + w1 + w2 - M_PI) < EPS_APPROX);

		vnor3D[v0] += w0*n;
		vnor3D[v1] += w1*n;
		vnor3D[v2] += w2*n;
	}

	for (int i = 0; i < nv; ++i)
	{
		Eigen::Vector3d& n = vnor3D[i]; 
		double norm = n.norm();
		if (norm > EPS_APPROX)
			n /= norm;

		int offset = 3 * i;
		vnor[offset + 0] = n(0);
		vnor[offset + 1] = n(1);
		vnor[offset + 2] = n(2);
	}
}

Real computePolarDecomposition(const Matrix3d& M, Matrix3d& Q, Matrix3d& S, Real tol)
{
	double det = M.determinant();
	if (abs(det) < EPS_APPROX)
	{
		return -1.0;
	}

	Matrix3d Mk;
	Matrix3d Ek;
	double M_oneNorm, M_infNorm, E_oneNorm;

	// Initialize
	Mk = M.transpose();
	M_oneNorm = Mk.lpNorm<1>();
	M_infNorm = Mk.lpNorm<Eigen::Infinity>();

	// Iterate
	do
	{
		Matrix3d MadjTk;

		Vector3d row0 = Mk.transpose().col(0);
		Vector3d row1 = Mk.transpose().col(1);
		Vector3d row2 = Mk.transpose().col(2);

		MadjTk.row(0) = row1.cross(row2);
		MadjTk.row(1) = row2.cross(row0);
		MadjTk.row(2) = row0.cross(row1);

		det = Mk(0, 0) * MadjTk(0, 0) + Mk(0, 1) * MadjTk(0, 1) + Mk(0, 2) * MadjTk(0, 2);
		if (abs(det) < EPS_APPROX)
		{
			return -1.0;
		}

		double MadjT_one = MadjTk.lpNorm<1>();
		double MadjT_inf = MadjTk.lpNorm<Eigen::Infinity>();

		double gamma = sqrt(sqrt((MadjT_one*MadjT_inf) / (M_oneNorm*M_infNorm)) / fabs(det));

		double g2 = 0.5 / (gamma * det);
		double g1 = gamma * 0.5;

		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
			{
			Ek(i, j) = Mk(i, j); // Modify previous Mk
			Mk(i, j) = g1 * Mk(i, j) + g2 * MadjTk(i, j);
			Ek(i, j) -= Mk(i, j);
			}

		E_oneNorm = Ek.lpNorm<1>();
		M_oneNorm = Mk.lpNorm<1>();
		M_infNorm = Mk.lpNorm<Eigen::Infinity>();
	} 
	while (E_oneNorm > M_oneNorm * tol);

	Q = Mk.transpose();
	S = Q.inverse()*M;

	return 1.0;
}

//void toQTColors(const dVector& vcol, cVector& vQTcol)
//{
//	int nv = (int) vcol.size()/3;
//	vQTcol.resize(nv);
//	for (int i = 0; i < nv; ++i)
//	{
//		int offset = 3 * i;
//		vQTcol[i] = qRgb(vcol[offset + 0], 
//						 vcol[offset + 1], 
//						 vcol[offset + 2]);
//	}
//}

void   transformIndex(const vector<iVector>& vInToOutMap, const iVector& vidxIn, int i, iVector& vidxOut)
{
	int nIdx = (int)vidxIn.size();
	vidxOut.resize(nIdx); // Output

	for (int iIdx = 0; iIdx < nIdx; ++iIdx)
		vidxOut[iIdx] = vInToOutMap[vidxIn[iIdx]][i];
}

void   applyIndexMap(const vector<iVector>& vInToOutMap, const dVector& vposIn, int D, dVector& vposOut)
{
	int nPos = (int)vposIn.size()/D;

	for (int iPos = 0; iPos < nPos; ++iPos)
	{
		int inOffset = iPos*D;
		 
		const iVector& vtarPos = vInToOutMap[iPos];
		int nTarPos = (int)vtarPos.size(); // Multimap
		for (int iTarPos = 0; iTarPos < nTarPos; ++iTarPos)
		{
			int outOffset = vtarPos[iTarPos] * D;
			for (int iD = 0; iD < D; ++iD) // Map positions
				vposOut[outOffset + iD] = vposIn[inOffset + iD];
		}
	}
}

Matrix3d computeAxisAngleRotation(const Vector3d& axis, Real angle)
{
	Vector3d vu = axis.normalized();
	Real cosa = cos(angle);
	Real sina = sin(angle);

	Matrix3d I = Matrix3d::Identity();
	Matrix3d C = crossProductMatrix(vu);
	Matrix3d T = tensorProduct(vu, vu);

	return I*cosa + C*sina + T*(1 - cosa);
}

void getSubvector(int i, int nele, const dVector& vin, dVector& vout)
{
	vout.resize(nele);
	for (int it = 0; it < nele; it++)
		vout[it] = vin[i + it];
}

void setSubvector(int i, int nele, const dVector& vin, dVector& vout)
{
	for (int it = 0; it < nele; it++)
		vout[i + it] = vin[it];
}

void getSubvector(int i, int nele, const VectorXd& vin, VectorXd& vout)
{
	vout.resize(nele);
	for (int it = 0; it < nele; it++)
		vout(it) = vin(i + it);
}

void setSubvector(int i, int nele, const VectorXd& vin, VectorXd& vout)
{
	for (int it = 0; it < nele; it++)
		vout(i + it) = vin(it);
}

Eigen::Matrix3d crossProductMatrix(const Eigen::Vector3d& v)
{
	Matrix3d M;
	M.setZero();

	M(1, 2) = -v(0);
	M(2, 1) = v(0);

	M(0, 2) = v(1);
	M(2, 0) = -v(1);

	M(0, 1) = -v(2);
	M(1, 0) = v(2);

	return M;
}

Eigen::Matrix3d tensorProduct(const Eigen::Vector3d& u, const Eigen::Vector3d v)
{
	return u*v.transpose();
}

Frame3d frameRotation(const Frame3d& frame, Matrix3d& R)
{
	Frame3d newFrame;
	newFrame.bin = R * frame.bin;
	newFrame.nor = R * frame.nor;
	newFrame.tan = R * frame.tan;
	return newFrame;
}

Frame3d frameRotation(const Frame3d& vF, Real angle)
{
	Frame3d rot;
	rot.tan = vF.tan;
	Vector3d nor0 = vF.nor;
	Vector3d bin0 = vF.bin;
	rodriguesRotation(rot.tan.data(), angle, nor0.data(), rot.nor.data());
	rodriguesRotation(rot.tan.data(), angle, bin0.data(), rot.bin.data());

	return rot;
}

void frameRotation(const vector<Frame3d>& vF, const dVector& va, vector<Frame3d>& vFRot)
{
	int ne = (int)vF.size();
	assert(ne == va.size());
	assert(ne == vFRot.size());
	for (int i = 0; i < ne; i++)
	{
		vFRot[i] = frameRotation(vF[i], va[i]);
	}
}

//void framePTForward(const Frame& f0, const Curve& curve, vector<Frame>& frames)
//{
//	assert(frames.size() == curve.getNumEdge());
//
//	int ne = curve.getNumEdge();
//	frames[0] = f0; // Define at 0
//	for (int i = 1; i < ne; i++)
//		frames[i] = framePT(frames[i - 1], curve.getEdgeTangent(i - 1), curve.getEdgeTangent(i));
//}
//
//void framePTBackward(const Frame& fn, const Curve& curve, vector<Frame>& frames)
//{
//	assert(frames.size() == curve.getNumEdge());
//
//	int ne = curve.getNumEdge();
//	frames[ne - 1] = fn; // Define at n-1
//	for (int i = ne - 2; i >= 0; i--)
//		frames[i] = framePT(frames[i + 1], curve.getEdgeTangent(i + 1), curve.getEdgeTangent(i));
//}

//Frame3d parallelTransport(const Frame3d &prevFrame, const Vector3d &n0, const Vector3d &n1)
//{
//	Frame3d newFrame;
//	newFrame.tan = n1.normalized();
//	newFrame.bin = parallelTransport(prevFrame.bin, n0, n1);
//	newFrame.bin = (newFrame.bin - newFrame.tan * (newFrame.bin.dot(newFrame.tan))).normalized();
//	newFrame.nor = (newFrame.bin.cross(newFrame.tan)).normalized(); // Should be normalized t%b
//	return newFrame;
//}

Frame3d parallelTransport(const Frame3d& F, const Vector3d& t2)
{
	//if (isApprox(t1.squaredNorm(), 1.0, EPS_POS) || isApprox(t2.squaredNorm(), 1.0, EPS_POS))
	//	logSimu("[WARNING] Parallel transport between non-unit vectors"); // Something is wrong

	Frame3d Fin = F;

	//if (!isApprox(Fin.tan.norm(), 1.0, 1e-9))
	//	logSimu("Non unit director");
	//if (!isApprox(Fin.nor.norm(), 1.0, 1e-9))
	//	logSimu("Non unit director");
	//if (!isApprox(Fin.bin.norm(), 1.0, 1e-9))
	//	logSimu("Non unit director");
	//if (!isApprox(Fin.tan.dot(Fin.nor), 0.0, 1e-9))
	//	logSimu("Non orthogonal");
	//if (!isApprox(Fin.nor.dot(Fin.bin), 0.0, 1e-9))
	//	logSimu("Non orthogonal");
	//if (!isApprox(Fin.bin.dot(Fin.tan), 0.0, 1e-9))
	//	logSimu("Non orthogonal");

	Frame3d Fout;
	Fout.tan = t2.normalized();
	Fout.nor = parallelTransport(F.nor, F.tan, Fout.tan);
	//Fout.nor.normalize();
	Fout.bin = Fout.tan.cross(Fout.nor);
	//Fout.bin.normalize();

	//if (!isApprox(Fout.tan.norm(), 1.0, 1e-9))
	//	logSimu("Non unit director");
	//if (!isApprox(Fout.nor.norm(), 1.0, 1e-9))
	//	logSimu("Non unit director");
	//if (!isApprox(Fout.bin.norm(), 1.0, 1e-9))
	//	logSimu("Non unit director");
	//if (!isApprox(Fout.tan.dot(Fout.nor), 0.0, 1e-9))
	//	logSimu("Non orthogonal");
	//if (!isApprox(Fout.nor.dot(Fout.bin), 0.0, 1e-9))
	//	logSimu("Non orthogonal");
	//if (!isApprox(Fout.bin.dot(Fout.tan), 0.0, 1e-9))
	//	logSimu("Non orthogonal");

	return Fout;
}

Vector3d parallelTransport(const Vector3d& u, const Vector3d& t1, const Vector3d& t2)
{
	//if (isApprox(t1.squaredNorm(), 1.0, EPS_POS) || isApprox(t2.squaredNorm(), 1.0, EPS_POS))
	//	logSimu("[WARNING] Parallel transport between non-unit vectors"); // Something is wrong

	Vector3d te = t1.normalized();
	Vector3d tf = t2.normalized();

	double d = te.dot(tf);

	//if (d > +1 - EPS_APPROX)
	//	return u;

	//if (d < -1 + EPS_APPROX)
	//	return u;

	Vector3d b = te.cross(tf);

	return u * d + b * (b.dot(u)) * 1 / (1 + d) + b.cross(u);
};

double signedAngle(const Vector3d& v0, const Vector3d& v1, const Vector3d& r, double a)
{
	Vector3d n = v0.normalized();
	Vector3d v = v1.normalized();
	Vector3d t = r.normalized();

	Vector3d nxv = n.cross(v);
	double cosAngle = n.dot(v);
	if (cosAngle > 1.0) cosAngle = 1.0;
	else if (cosAngle < -1.0) cosAngle = -1.0;
	double theta = acos(cosAngle); // New angle
	if (nxv.dot(t) < 0) 
		theta = -theta;

	double oldTheta = a;
	int mod = oldTheta / (2 * M_PI);
	double rest = oldTheta - mod * 2 * M_PI;

	if (rest > 0)
	{
		double newTheta = theta + 2 * M_PI; // Plus 2pi
		if (abs(newTheta - rest) < abs(theta - rest))
			theta = newTheta + mod * 2 * M_PI;
		else theta = theta + mod * 2 * M_PI;
	}
	else if (rest < 0)
	{
		double newTheta = theta - 2 * M_PI; // Less 2pi
		if (abs(newTheta - rest) < abs(theta - rest))
			theta = newTheta + mod * 2 * M_PI;
		else theta = theta + mod * 2 * M_PI;
	}

	return theta;
}

// Eigen related

void add3D(int i, const Eigen::Vector3d& v, dVector& vd)
{
	int offset = 3*i;
	assert(offset < (int) vd.size());
	for (int i = 0; i < 3; ++i)
		vd[offset + i] += v(i);
}

void set3D(int i, const Eigen::Vector3d& v, dVector& vd)
{
	int offset = 3*i;
	assert(offset < (int) vd.size());
	for (int i = 0; i < 3; ++i)
		vd[offset + i] = v(i);
}

Eigen::Vector3d get3D(int i, const dVector& vd)
{
	Eigen::Vector3d v;

	int offset = 3*i;
	assert(offset < (int) vd.size());
	for (int i = 0; i < 3; ++i)
		v(i) = vd[offset + i];

	return v;
}

void add2D(int i, const Eigen::Vector2d& v, dVector& vd)
{
	int offset = 2*i;
	assert(offset < (int) vd.size());
	for (int i = 0; i < 2; ++i) 
		vd[offset + i] += v(i);
}

void set2D(int i, const Eigen::Vector2d& v, dVector& vd)
{
	int offset = 2*i;
	assert(offset < (int) vd.size());
	for (int i = 0; i < 2; ++i)
		vd[offset + i] = v(i);
}

Eigen::Vector2d get2D(int i, const dVector& vd)
{
	Eigen::Vector2d v;

	int offset = 2*i;
	assert(offset < (int) vd.size());
	for (int i = 0; i < 2; ++i)
		v(i) = vd[offset + i];

	return v;
}

void setBlock3x1(int i, const Vector3d& v, VectorXd& vd)
{
	vd.block(3*i, 0, 3, 1) = v;
}

void addBlock3x1(int i, const Vector3d& v, VectorXd& vd)
{
	vd.block(3*i, 0, 3, 1) += v;
}

Vector3d getBlock3x1(int i, const VectorXd& vd)
{
	return vd.block(3*i, 0, 3, 1);
}

void setBlock2x1(int i, const Vector2d& v, VectorXd& vd)
{
	vd.block(2 * i, 0, 2, 1) = v;
}

void addBlock2x1(int i, const Vector2d& v, VectorXd& vd)
{
	vd.block(2 * i, 0, 2, 1) += v;
}

Vector2d getBlock2x1(int i, const VectorXd& vd)
{
	return vd.block(2 * i, 0, 2, 1);
}


void setBlock3x3From(int i, int j, const Matrix3d& M, MatrixXd& mD, bool sym)
{
	mD.block(i, j, 3, 3) = M;
	if (sym && i != j) mD.block(j, i, 3, 3) = M.transpose();
}

void addBlock3x3From(int i, int j, const Matrix3d& M, MatrixXd& mD, bool sym)
{
	mD.block(i, j, 3, 3) += M;
	if (sym && i != j) mD.block(j, i, 3, 3) += M.transpose();
}

Matrix3d getBlock3x3From(int i, int j, MatrixXd& mD)
{
	return mD.block(3 * i, 3 * j, 3, 3);
}

void setBlock3x3(int i, int j, const Matrix3d& M, MatrixXd& mD, bool sym )
{
	int offseti = 3*i;
	int offsetj = 3*j;
	mD.block(offseti, offsetj, 3, 3) = M;	
	if (sym && i != j) mD.block(offsetj, offseti, 3, 3) = M.transpose();	
}

void addBlock3x3(int i, int j, const Matrix3d& M, MatrixXd& mD, bool sym)
{
	int offseti = 3*i;
	int offsetj = 3*j;
	mD.block(offseti, offsetj, 3, 3) += M;	
	if (sym && i != j) mD.block(offsetj, offseti, 3, 3) += M.transpose();	
}

Matrix3d getBlock3x3(int i, int j, MatrixXd& mD)
{
	return mD.block(3*i, 3*j, 3, 3);
}

void addBlockNxM(int n, int m, int i, int j, const MatrixXd& M, MatrixXd& mD, bool sym)
{
	int offseti = n*i;
	int offsetj = m*j;
	mD.block(offseti, offsetj, n, m) += M;	
	if (sym && i != j) mD.block(offsetj, offseti, n, m) += M.transpose();	
}

void setBlockNxM(int n, int m, int i, int j, const MatrixXd& M, MatrixXd& mD, bool sym)
{
	int offseti = n*i;
	int offsetj = m*j;
	mD.block(offseti, offsetj, n, m) += M;	
	if (sym && i != j) mD.block(offsetj, offseti, n, m) = M.transpose();	
}

MatrixXd getBlockNxM(int n, int m, int i, int j, MatrixXd& mD)
{
	return mD.block(n*i, m*j, n, m);
}

VectorXd toEigen(const dVector& v)
{
	int n = (int) v.size();
	VectorXd out(n);
	for (int i = 0; i < n; ++i)
		out(i) = v[i];
	return out;
}

VectorXi toEigen(const iVector& v)
{
	int n = (int) v.size();
	VectorXi out(n);
	for (int i = 0; i < n; ++i)
		out(i) = v[i];
	return out;
}

dVector toSTL(const Eigen::VectorXd& v)
{
	int n = (int)v.size();
	dVector out(n);
	for (int i = 0; i < n; ++i)
		out[i] = v(i);
	return out;
}

iVector toSTL(const Eigen::VectorXi& v)
{
	int n = (int)v.size();
	iVector out(n);
	for (int i = 0; i < n; ++i)
		out[i] = v(i);
	return out;
}

dVector toSTL(const Eigen::Vector3d& v)
{
	dVector out(3);
	out[0] = v(0);
	out[1] = v(1);
	out[2] = v(2);
	return out;
}

void toEigen(const dVector& v, Eigen::VectorXd& ve)
{
	int n = (int)v.size();
	ve.resize(n);
	for (int i = 0; i < n; ++i)
		ve(i) = v[i];
}

void toEigen(const iVector& v, Eigen::VectorXi& ve)
{
	int n = (int)v.size();
	ve.resize(n);
	for (int i = 0; i < n; ++i)
		ve(i) = v[i];
}

void toSTL(const Eigen::VectorXd& v, dVector& vs)
{
	int n = (int)v.size();
	vs.resize(n);
	for (int i = 0; i < n; ++i)
		vs[i] = v(i);
}

void toSTL(const Eigen::VectorXi& v, iVector& vs)
{
	int n = (int)v.size();
	vs.resize(n);
	for (int i = 0; i < n; ++i)
		vs[i] = v(i);
}

void to3D(const vector<Eigen::Vector2d>& v2D, vector<Eigen::Vector3d>& v3D, int addedD)
{
	assert(addedD >= 0 && addedD <= 2);

	int n = (int)v2D.size();
	v3D.resize(n);
	for (int i = 0; i < n; ++i)
	{
		if (addedD == 0)
		{
			v3D[i](1) = v2D[i](0);
			v3D[i](2) = v2D[i](1);
			v3D[i](0) = 0.0;
		}
		if (addedD == 1)
		{
			v3D[i](0) = v2D[i](0);
			v3D[i](2) = v2D[i](1);
			v3D[i](1) = 0.0;
		}
		if (addedD == 2)
		{
			v3D[i](0) = v2D[i](0);
			v3D[i](1) = v2D[i](1);
			v3D[i](2) = 0.0;
		}
	}
}

void to3D(const Eigen::VectorXd& v2D, Eigen::VectorXd& v3D, int addedD)
{
	assert(addedD >= 0 && addedD <= 2);

	int n = (int)v2D.size() / 2;
	v3D.resize(3 * n);
	for (int i = 0; i < n; ++i)
	{
		int offset2D = 2 * i;
		int offset3D = 3 * i;

		if (addedD == 0)
		{
			v3D(offset3D + 1) = v2D(offset2D + 0);
			v3D(offset3D + 2) = v2D(offset2D + 1);
			v3D(offset3D + 0) = 0.0;
		}
		if (addedD == 1)
		{
			v3D(offset3D + 0) = v2D(offset2D + 0);
			v3D(offset3D + 2) = v2D(offset2D + 1);
			v3D(offset3D + 1) = 0.0;
		}
		if (addedD == 2)
		{
			v3D(offset3D + 0) = v2D(offset2D + 0);
			v3D(offset3D + 1) = v2D(offset2D + 1);
			v3D(offset3D + 2) = 0.0;
		}
	}
}

void to3D(const dVector& v2D, dVector& v3D, int addedD)
{
	assert(addedD >= 0 && addedD <= 2);

	int n = (int) v2D.size() / 2;
	v3D.resize(3 * n);
	for (int i = 0; i < n; ++i)
	{
		int offset2D = 2 * i;
		int offset3D = 3 * i;

		if (addedD == 0)
		{
			v3D[offset3D + 1] = v2D[offset2D + 0];
			v3D[offset3D + 2] = v2D[offset2D + 1];
			v3D[offset3D + 0] = 0.0;
		}
		if (addedD == 1)
		{
			v3D[offset3D + 0] = v2D[offset2D + 0];
			v3D[offset3D + 2] = v2D[offset2D + 1];
			v3D[offset3D + 1] = 0.0;
		}
		if (addedD == 2)
		{
			v3D[offset3D + 0] = v2D[offset2D + 0];
			v3D[offset3D + 1] = v2D[offset2D + 1];
			v3D[offset3D + 2] = 0.0;
		}
	}
}

Eigen::Vector3d to3D(const Eigen::Vector2d& p2D, int addedD)
{
	assert(addedD >= 0 && addedD <= 2);

	Vector3d out;
	if (addedD == 0)
	{
		out(1) = p2D(0);
		out(2) = p2D(1);
		out(0) = 0.0;
	}
	if (addedD == 1)
	{
		out(0) = p2D(0);
		out(2) = p2D(1);
		out(1) = 0.0;
	}
	if (addedD == 2)
	{
		out(0) = p2D(0);
		out(1) = p2D(1);
		out(2) = 0.0;
	}
	return out;
}

void to2D(const vector<Eigen::Vector3d>& v3D, vector<Eigen::Vector2d>& v2D, int removedD)
{
	assert(removedD >= 0 && removedD <= 2);

	int n = (int)v3D.size();
	v2D.resize(n);

	for (int i = 0; i < n; ++i)
	{
		if (removedD == 0)
		{
			v2D[i](0) = v3D[i](1);
			v2D[i](1) = v3D[i](2);
		}
		if (removedD == 1)
		{
			v2D[i](0) = v3D[i](0);
			v2D[i](1) = v3D[i](2);
		}
		if (removedD == 2)
		{
			v2D[i](0) = v3D[i](0);
			v2D[i](1) = v3D[i](1);
		}
	}
}

void to2D(const Eigen::VectorXd& v3D, Eigen::VectorXd& v2D, int removedD)
{
	assert(removedD >= 0 && removedD <= 2);

	int n = (int)v3D.size() / 3;
	v2D.resize(2 * n);
	for (int i = 0; i < n; ++i)
	{
		int offset2D = 2 * i;
		int offset3D = 3 * i;

		if (removedD == 0)
		{
			v2D(offset2D + 0) = v3D(offset3D + 1);
			v2D(offset2D + 1) = v3D(offset3D + 2);
		}
		if (removedD == 1)
		{
			v2D(offset2D + 0) = v3D(offset3D + 0);
			v2D(offset2D + 1) = v3D(offset3D + 2);
		}
		if (removedD == 2)
		{
			v2D(offset2D + 0) = v3D(offset3D + 0);
			v2D(offset2D + 1) = v3D(offset3D + 1);
		}
	}
}

void to2D(const dVector& v3D, dVector& v2D, int removedD)
{
	assert(removedD >= 0 && removedD <= 2);

	int n = (int)v3D.size() / 3;
	v2D.resize(2 * n);
	for (int i = 0; i < n; ++i)
	{
		int offset2D = 2 * i;
		int offset3D = 3 * i;

		if (removedD == 0)
		{
			v2D[offset2D + 0] = v3D[offset3D + 1];
			v2D[offset2D + 1] = v3D[offset3D + 2];
		}
		if (removedD == 1)
		{
			v2D[offset2D + 0] = v3D[offset3D + 0];
			v2D[offset2D + 1] = v3D[offset3D + 2];
		}
		if (removedD == 2)
		{
			v2D[offset2D + 0] = v3D[offset3D + 0];
			v2D[offset2D + 1] = v3D[offset3D + 1];
		}
	}
}

Eigen::Vector2d to2D(const Eigen::Vector3d& p3D, int removedD)
{
	assert(removedD >= 0 && removedD <= 2);

	Vector2d out;
	if (removedD == 0)
	{
		out(0) = p3D(1);
		out(1) = p3D(2);
	}
	if (removedD == 1)
	{
		out(0) = p3D(0);
		out(1) = p3D(2);
	}
	if (removedD == 2)
	{
		out(0) = p3D(0);
		out(1) = p3D(1);
	}
	return out;
}

string vectorToString(const VectorXd& v)
{
	ostringstream str;
	int np = v.size();
	for (int j = 0; j < np; ++j)
		str << v(j) << " ";

	return str.str();
}

//void writeToFile(const VectorXd& v, const QString& str, bool concatenate)
//{
//	logSimu("[INFO] In: %s: writing vector to file (%u)\n", __FUNCTION__, v.size());
//
//	QFile file(str);
//	file.open(QFile::WriteOnly | QFile::Append);
//	for(int i = 0; i < (int) v.size(); ++i)
//	{
//		QString sVal = QString::number(v(i), 'e', 8);
//
//		QString strValue;	
//		if (i == (int) v.size() - 1)
//			strValue.sprintf("%s", sVal.toAscii().data());
//		else strValue.sprintf("%s,", sVal.toAscii().data());
//		file.write(strValue.toAscii());
//	}
//
//	QString strValue;
//	strValue.sprintf("/r\n");
//	file.write(strValue.toAscii());
//
//	// Close if needed
//	if (!concatenate)
//		file.close();
//}

string matrixToString(const MatrixXd& M)
{
	ostringstream str;
	int nr = M.rows();
	int nc = M.cols();
	for (int i = 0; i < nr; ++i)
	{
		for (int j = 0; j < nc; ++j)
		{
			str << M(i, j) << " ";
		}
		str << "\n";
	}

	return str.str();
}

//void writeToFile(const MatrixXd& M, const QString& str)
//{
//	QFile file(str);
//	file.open(QFile::WriteOnly);
//
//	logSimu("[INFO] In: %s: writing matrix to file (%u,%u)\n", __FUNCTION__, M.rows(), M.cols());
//
//	QString sVal;
//	for (int i = 0; i < (int)M.rows(); i++)
//	{
//		for (int j = 0; j < (int)M.cols(); j++)
//		{
//			QString sVal;
//			QString strValue;
//
//			sVal = QString::number(M(i, j), 'e', 12);
//
//			if (j == (int)M.cols() - 1)
//				strValue.sprintf("%s", sVal.toAscii().data());
//			else strValue.sprintf("%s,", sVal.toAscii().data());
//			file.write(strValue.toAscii());
//		}
//		QString strValue;
//		strValue.sprintf("/r\n");
//		file.write(strValue.toAscii());
//	}
//	file.close();
//}

string matrixToString(const MatrixSd& M, bool sym)
{
	ostringstream str;
	int nr = M.rows();
	int nc = M.cols();
	for (int i = 0; i < nr; ++i)
	{
		for (int j = 0; j < nc; ++j)
		{
			if (i < j && sym)
				str << M.coeff(j,i) << " ";
			else str << M.coeff(i,j) << " ";
		}
		str << "\n";
	}

	return str.str();
}

//void writeToFile(const MatrixSd& M, const QString& str, bool sym)
//{
//	QFile file(str);
//	file.open(QFile::WriteOnly);
//
//	logSimu("[INFO] In: %s: writing matrix to file (%u,%u)\n", __FUNCTION__, M.rows(), M.cols());
//
//	QString sVal;
//	for(int i = 0; i < (int) M.rows(); i++)
//	{
//		for(int j = 0; j < (int) M.cols(); j++)
//		{
//			QString sVal;
//			QString strValue;
//
//			if (i <= j && sym)
//				sVal = QString::number(M.coeff(j,i), 'e', 16);
//			else sVal = QString::number(M.coeff(i,j), 'e', 16);
//
//			if (j == (int) M.cols() - 1)
//				strValue.sprintf("%s", sVal.toAscii().data());
//			else strValue.sprintf("%s,", sVal.toAscii().data());
//			file.write(strValue.toAscii());
//		}
//		QString strValue;
//		strValue.sprintf("/r\n");
//		file.write(strValue.toAscii());
//	}
//	file.close();
//}

#include <stdio.h>
#include <stdarg.h>

void logSimu(const char *format, ...)
{
	static char message[1024];
	va_list vl;

	va_start(vl, format);   
	vsprintf(message, format, vl);
	va_end(vl);

	cout << message << endl;
}

//void listDirFiles(const string& path, vector<string>& files)
//{
//	QDir qdir(QString(path.c_str()));
//	qdir.setFilter(QDir::Files);
//	QStringList entries = qdir.entryList();
//	for (QStringList::ConstIterator entry = entries.begin(); entry != entries.end(); ++entry)
//		files.push_back((*entry).toStdString()); 
//}

//bool loadPolylineFromFile(const string& path, vector<Vector3>& vpos, vector<iVector>& vlin)
//{
//	//vpos.clear();
//	//vlin.clear();
//	//
//	//ifstream infile;
//	//infile.open(path.c_str());
//	//if (!infile.is_open())
//	//	return false;
//
//	//infile.precision(12);	
//
//	//int idx;
//	//Eigen::Vector3d v;
//	//iVector line;
//	//char nextc;
//
//	//int nlines = 0;
//	//bool out = false;
//	//while (!infile.eof())
//	//{
//	//	char c;
//	//	infile >> c;
//	//	switch (c)
//	//	{
//
//	//	// Load vertex
//	//	case 'v':
//	//		
//	//		infile >> v.x();
//	//		infile >> v.y();
//	//		infile >> v.z();
//	//		vpos.push_back(v);
//	//		break;
//
//	//	// Load line
//	//	case 'l':
//	//		line.clear();
//	//		do
//	//		{
//	//			out = false;
//	//			infile >> nextc;
//	//			if (nextc != 'v' && nextc != 'l')
//	//			{
//	//				infile.putback(nextc);
//	//				infile >> idx; // Index
//
//	//				if (!line.empty() && idx - 1 == line.back())
//	//					break; // Reached end of line
//	//				else line.push_back(idx - 1);
//	//			}
//	//			else 
//	//			{
//	//				infile.putback(nextc);
//	//				out = true; // Line end
//	//			}
//	//		}
//	//		while (!out && !infile.eof());
//	//		vlin.push_back(line); // :)
//	//		nlines++;
//	//		break;
//
//	//	default:
//	//		infile.close();
//	//		return false;
//	//	}
//	//}
//
//	//infile.close();
//}

void orthogonalize_GS(const MatrixXd& mM, MatrixXd& mO)
{
	int ncol = mM.cols();
	int nrow = mM.rows();
	mO.resize(nrow, ncol);
	mO.col(0) = mM.col(0);
	for (int i = 1; i < ncol; ++i)
	{
		const VectorXd& vcur = mM.col(i);
		const VectorXd& upre = mO.col(i-1);
		mO.col(i) = vcur - (vcur.dot(upre)/upre.dot(upre))*upre;
	}
}

Eigen::Matrix2d computeTranslate(const Vector2d& vt)
{
	Matrix2d T;
	T(0, 0) = vt(0);
	T(1, 1) = vt(1);
	return T;
}

Eigen::Matrix2d computeRotation(double angle)
{
	Real cosa = cos(angle);
	Real sina = sin(angle);
	Matrix2d R;
	R(0, 0) = cosa;
	R(0, 1) = -sina;
	R(1, 0) = sina;
	R(1, 1) = cosa;
	return R;
}

Eigen::Vector3d computeKB(const Eigen::Vector3d& e0, const Eigen::Vector3d& e1)
{
	Vector3d t0 = e0/e0.norm();
	Vector3d t1 = e1/e1.norm();
	return t0.cross(t1) * 2 * ( 1 / (1 + t0.dot(t1)) );
}

//Eigen::Matrix3d eulerRotation(Real x, Real y, Real z)
//{
//	Matrix3d Rx;
//	Matrix3d Ry;
//	Matrix3d Rz;
//
//	// TODO
//
//	throw new exception("[FAILURE] Not implemented yet");
//
//	return Rz*Ry*Rx;
//}

void setSparseMatrix0(MatrixSd& mM)
{
	int nz = mM.nonZeros();
	for (int i = 0; i < nz; ++i)
		mM.valuePtr()[i] = 0.0;
}

int getInternalDataIndex(int row, int col, const MatrixSd& mH, bool sym)
{
	assert(mH.isCompressed());

	int i,j;

	// Symmetric sparse matrices are stored in this
	// framework as sparse lower triangular matrices
	if (sym && row < col)
	{
		i = col;
		j = row;
	}
	else
	{
		i = row;
		j = col;
	}

	int index = -1;
	int colEnd;
	if (j < mH.cols() - 1)
		colEnd = mH.outerIndexPtr()[j+1];
	else colEnd = mH.nonZeros(); // Last
	int colSta = mH.outerIndexPtr()[j];
	for (int k = colSta; k < colEnd; ++k)
	{
		if (mH.innerIndexPtr()[k] == i)
		{
			// Found
			index = k;
			break; 
		}
	}	

	return index;
}

// Deformation related

// Green strain for the given deformation F: e = 1/2*(F + F^T)
void computeGreenStrain(const MatrixXd& mF, MatrixXd& me)
{
	me = 0.5*(mF + mF.transpose());
}

// Cauchy strain for the given deformation F: E = 1/2*(F*F^T - I)
void computeCauchyStrain(const MatrixXd& mF, MatrixXd& mE)
{
	mE = 0.5*(mF*mF.transpose() - MatrixXd::Identity(mF.rows(), mF.cols()));
}

// Visualization related

dVector convertHSLtoRGB(const dVector& hsl)
{
	double h = hsl[0];
	double sl = hsl[1];
	double l = hsl[2];

	double v;
	double r,g,b;

	r = l;   // default to gray
	g = l;
	b = l;
	v = (l <= 0.5) ? (l * (1.0 + sl)) : (l + sl - l * sl);
	if (v > 0)
	{
		double m;
		double sv;
		int sextant;
		double fract, vsf, mid1, mid2;

		m = l + l - v;
		sv = (v - m ) / v;
		h *= 6.0;
		sextant = (int)h;
		fract = h - sextant;
		vsf = v * sv * fract;
		mid1 = m + vsf;
		mid2 = v - vsf;
		switch (sextant)
		{
		case 0:
			r = v;
			g = mid1;
			b = m;
			break;
		case 1:
			r = mid2;
			g = v;
			b = m;
			break;
		case 2:
			r = m;
			g = v;
			b = mid1;
			break;
		case 3:
			r = m;
			g = mid2;
			b = v;
			break;
		case 4:
			r = mid1;
			g = m;
			b = v;
			break;
		case 5:
			r = v;
			g = m;
			b = mid2;
			break;
		}
	}

	dVector rgb;
	rgb.push_back(r * 255.0f);
	rgb.push_back(g * 255.0f);
	rgb.push_back(b * 255.0f);
	return rgb;
}

dVector computeHeatColorRGB(double alpha)
{
	dVector heatHSL = computeHeatColorHSL(alpha);
	return convertHSLtoRGB(heatHSL); // Convert
}

dVector computeHeatColorHSL(double alpha)
{
	double MAX = (240.0/360.0);
	double h = (1.0-alpha)*MAX;
	double s = 0.85;
	double l = 0.5;

	dVector hsl;
	hsl.push_back(h);
	hsl.push_back(s);
	hsl.push_back(l);
	return hsl;
}

// Miscellanea

string generateGUID()
{
	string str;

	int i;

	//srand(static_cast<unsigned int> (time(NULL)));  /* Randomize based on time. */

	/*Data1 - 8 characters.*/
	str.append("{");
	for (i = 0; i < 8; i++)
	{
		char c[2] { 0, 0 };
		((c[0] = (rand() % 16)) < 10) ? c[0] += 48 : c[0] += 55;
		str.append(c);
	}

	/*Data2 - 4 characters.*/
	str.append("-");
	for (i = 0; i < 4; i++)
	{
		char c[2] { 0, 0 };
		((c[0] = (rand() % 16)) < 10) ? c[0] += 48 : c[0] += 55;
		str.append(c);
	}

	/*Data3 - 4 characters.*/
	str.append("-");
	for (i = 0; i < 4; i++)
	{
		char c[2] { 0, 0 };
		((c[0] = (rand() % 16)) < 10) ? c[0] += 48 : c[0] += 55;
		str.append(c);
	}

	/*Data4 - 4 characters.*/
	str.append("-");
	for (i = 0; i < 4; i++)
	{
		char c[2] { 0, 0 };
		((c[0] = (rand() % 16)) < 10) ? c[0] += 48 : c[0] += 55;
		str.append(c);
	}

	/*Data5 - 12 characters.*/
	str.append("-");
	for (i = 0; i < 12; i++)
	{
		char c[2] { 0, 0 };
		((c[0] = (rand() % 16)) < 10) ? c[0] += 48 : c[0] += 55;
		str.append(c);
	}

	str.append("}");

	return str;
}

void cylindricalHelix(double R, double H, int nS, int nL, int spinX, int spinZ, vector<Vector3d>& voutPoints)
{
	voutPoints.clear();
	voutPoints.resize(nL + 1);

	double deltaz = H / nL;

	for (int i = 0; i <= nL; ++i)
	{
		double y = -H/2.0 + i*deltaz;
		double phi = (nS*M_PI*y) / H;
		double x = R*cos(phi);
		double z = R*sin(phi);
		if (spinX < 0)
			x *= -1;
		if (spinZ < 0)
			z *= -1;
		voutPoints[i] = Vector3d(x, y, z);
	}
}

void sphericalHelix(double R, int nS, int nL, int spinX, int spinZ, vector<Vector3d>& voutPoints)
{
	voutPoints.clear();
	voutPoints.resize(nL + 1);
	voutPoints[0] = Vector3d(0.0, -R, 0.0);

	double deltaz = (2*R)/nL;

	for (int i = 1; i <= nL; ++i)
	{
		double y = -R + i*deltaz;

		double theta = acos(y/R);
		double phi = (nS*M_PI*y) / R + 1;
		double x = R*sin(theta)*cos(phi);
		double z = R*sin(theta)*sin(phi);
		//if (spin < 0)
		//{
		//	double xTemp = x;
		//	x = z; // Swap
		//	z = xTemp;
		//}
		if (spinX < 0)
			x *= -1;
		if (spinZ < 0)
			z *= -1;
		voutPoints[i] = Vector3d(x, y, z);
	}
}

bool pointTriIntersect(const Vector3d& p, const Vector3d& v0, 
					   const Vector3d& v1, const Vector3d& v2, 
					   double& u, double& v, double tol)
{
	// Ray direction

	Vector3d s = v0 - p;
	Vector3d e1 = v0 - v1;
	Vector3d e2 = v0 - v2;
	Vector3d d = e1.cross(e2).normalized();

	Vector3d h = d.cross(e2);
	double a = e1.dot(h);
	if (a > -tol && a < tol)
		return false;

	// U coordinate

	double f = 1.0 / a;
	u = f*(s.dot(h));
	if (u < -tol || u > 1.0 + tol)
		return false;

	// V coordinate

	Vector3d q = s.cross(e1);
	v = f*(d.dot(q));
	if (v < -tol || u + v > 1.0 + tol)
		return false;

	// At this stage we can compute t to find out
	// where the intersection point is on the line

	double t = f * (e2.dot(q));

	Vector3d pp = p + d*t;

	return (pp - p).norm() < tol && u >= 0.0 && u <= 1.0 && v >= 0.0 && v <= 1.0;
}


bool lineLineIntersect(const dVector& p1, const dVector& q1,
					   const dVector& p2, const dVector& q2,
					   double& s, double& t, double tol)
{
	double EPS1 = 1e-6;
	double EPS2 = tol;

	double d1[3], d2[3], r[3], a, e, f;
	double c1[3], c2[3];

	d1[0] = q1[0] - p1[0];
	d1[1] = q1[1] - p1[1];
	d1[2] = q1[2] - p1[2];

	d2[0] = q2[0] - p2[0];
	d2[1] = q2[1] - p2[1];
	d2[2] = q2[2] - p2[2];

	r[0] = p1[0] - p2[0];
	r[1] = p1[1] - p2[1];
	r[2] = p1[2] - p2[2];

	a = d1[0] * d1[0] + d1[1] * d1[1] + d1[2] * d1[2];
	e = d2[0] * d2[0] + d2[1] * d2[1] + d2[2] * d2[2];
	f = d2[0] * r[0] + d2[1] * r[1] + d2[2] * r[2];

	// Check if either or both segments degenerate into points
	//
	if ((a <= EPS1) && (e <= EPS1))
	{
		s = t = 0.0f;
		c1[0] = p1[0]; c1[1] = p1[1]; c1[2] = p1[2];
		c2[0] = p2[0]; c2[1] = p2[1]; c2[2] = p2[2];
		return (((c1[0] - c2[0])*(c1[0] - c2[0]) + (c1[1] - c2[1])*(c1[1] - c2[1]) + (c1[2] - c2[2])*(c1[2] - c2[2]))) < EPS1;
	}

	if (a <= EPS1)
	{
		// First segment degenerates into a point
		//
		s = 0.0f;
		t = f / e;
		if (t<0.0f) t = 0.0f;
		if (t>1.0f) t = 1.0f;
	}
	else
	{
		double c = d1[0] * r[0] + d1[1] * r[1] + d1[2] * r[2];

		if (e <= EPS1)
		{
			// Second segment degenerates into a point
			//
			t = 0.0f;
			s = -c / a;
			if (s<0.0f) s = 0.0f;
			if (s>1.0f) s = 1.0f;
		}
		else
		{
			// Nondegenerate case
			//
			double b = d1[0] * d2[0] + d1[1] * d2[1] + d1[2] * d2[2];
			double denom = a*e - b*b;

			if (denom != 0.0f)
			{
				s = (b*f - c*e) / denom;
				if (s<0.0f) s = 0.0f;
				if (s>1.0f) s = 1.0f;
			}
			else s = 0.0f;

			double tnom = b*s + f;
			if (tnom < 0.0f)
			{
				t = 0.0f;
				s = -c / a;
				if (s<0.0f) s = 0.0f;
				if (s>1.0f) s = 1.0f;
			}
			else if (tnom > e)
			{
				t = 1.0f;
				s = (b - c) / a;
				if (s<0.0f) s = 0.0f;
				if (s>1.0f) s = 1.0f;
			}
			else
				t = tnom / e;
		}
	}

	c1[0] = p1[0] + d1[0] * s;
	c1[1] = p1[1] + d1[1] * s;
	c1[2] = p1[2] + d1[2] * s;

	c2[0] = p2[0] + d2[0] * t;
	c2[1] = p2[1] + d2[1] * t;
	c2[2] = p2[2] + d2[2] * t;

	double dist = sqrt((c1[0] - c2[0])*(c1[0] - c2[0]) + (c1[1] - c2[1])*(c1[1] - c2[1]) + (c1[2] - c2[2])*(c1[2] - c2[2]));

	return dist < EPS2 && s >= 0.0 && s <= 1.0 && t >= 0.0 && t <= 1.0;
}

void hermiteInterpolation(const vector<Vector3d>& vin, vector<Vector3d>& vout, bool loop, int outnp, const Vector3d& tanIni, const Vector3d& tanEnd)
{
	int innp = (int) vin.size();

	dVector vsin;
	for (int i = 0; i < innp - 1; ++i)
		vsin.push_back((vin[i + 1] - vin[i]).norm());

	int inns = (int) vsin.size();
	
	double sT = 0;
	for (int i = 0; i < inns; ++i)
		sT += vsin[i]; // Sum length

	int outns = outnp - 1; // Out number of domains
	double outs = sT / outns; // Out domain length

	// Must match ends
	vout.resize(outnp);
	vout.back() = vin.back();
	vout.front() = vin.front();

	int sit = 0;
	int nxtp = 1;
	double sums = 0;
	double nxts = outs;
	for (int i = 1; i < outnp - 1; i++)
	{
		double s = vsin[sit];
		while (i*outs > sums + s)
		{
			sit++;
			sums += s;
			s = vsin[sit];
		}

		double t = (nxts - sums) / s;
		double t2 = t*t;
		double t3 = t*t*t;

		const Vector3d& p0 = vin[sit]; // In point 0
		const Vector3d& p1 = vin[sit + 1]; // In point 1
		Vector3d ts = (p1 - p0)*(1.0 / s); // In tan

		// Get tangents
		Vector3d t0, t1;
		if (inns == 1)
		{
			if (tanIni.norm() > 0.0)
			{
				t0 = tanIni;
			}
			else t0 = ts;

			if (tanEnd.norm() > 0.0)
			{
				t1 = tanEnd;
			}
			else t1 = ts;
		}
		else if (sit == 0 && !loop) // First domain (no loop)
		{
			if (tanIni.norm() > 0.0)
			{
				t0 = tanIni;
			}
			else t0 = ts;

			const Vector3d& pn = vin[sit + 2];
			t1 = 0.5*(ts + (pn - p1)*(1.0 / s)); 
		}
		else if (sit == inns - 1 && !loop) // Last domain (no loop)
		{
			if (tanEnd.norm() > 0.0)
			{
				t1 = tanEnd;
			}
			else t1 = ts;

			const Vector3d& pp = vin[sit - 1];
			t0 = 0.5*(ts + (p0 - pp)*(1.0 / s));
		}
		else if (sit == 0 && loop) // First domain (loop)
		{
			const Vector3d& pp = vin[innp - 2];
			const Vector3d& pn = vin[sit + 2];
			t0 = 0.5*(ts + (p0 - pp)*(1.0 / s)); // Mean
			t1 = 0.5*(ts + (pn - p1)*(1.0 / s)); // Mean
		}
		else if (sit == inns - 1 && loop) // Last domain (loop)
		{
			const Vector3d& pp = vin[sit - 1];
			const Vector3d& pn = vin[0];
			t0 = 0.5*(ts + (p0 - pp)*(1.0 / s)); // Mean
			t1 = 0.5*(ts + (pn - p1)*(1.0 / s)); // Mean
		}
		else
		{
			const Vector3d& pp = vin[sit - 1];
			const Vector3d& pn = vin[sit + 2];
			t0 = 0.5*(ts + (p0 - pp)*(1.0 / s)); // Mean
			t1 = 0.5*(ts + (pn - p1)*(1.0 / s)); // Mean
		}

		// Hermite interpolation

		Vector3d p = p0*(2 * t3 - 3 * t2 + 1) +
				     t0*(t3 - 2 * t2 + t)*s +
					 p1*(-2 * t3 + 3 * t2) +
					 t1*(t3 - t2)*s;

		vout[nxtp++] = p; // Point

		nxts += outs;
	}
}

void hermiteInterpolation(const vector<Vector2d>& vin, const dVector& vsin, vector<Vector2d>& vout, bool loop, int outnp)
{
	int innp = (int)vin.size();
	int inns = (int)vsin.size();

	assert(inns == innp - 1);

	double sT = 0;
	for (int i = 0; i < inns; ++i)
		sT += vsin[i]; // Sum length

	int outns = outnp - 1; // Out number of domains
	double outs = sT / outns; // Out domain length

	// Must match ends
	vout.resize(outnp);
	vout.back() = vin.back();
	vout.front() = vin.front();

	int sit = 0;
	int nxtp = 1;
	double sums = 0;
	double nxts = outs;
	for (int i = 1; i < outnp - 1; i++)
	{
		double s = vsin[sit];
		while (i*outs > sums + s)
		{
			sit++;
			sums += s;
			s = vsin[sit];
		}

		double t = (nxts - sums) / s;
		double t2 = t*t;
		double t3 = t*t*t;

		const Vector2d& p0 = vin[sit]; // In point 0
		const Vector2d& p1 = vin[sit + 1]; // In point 1
		Vector2d ts = (p1 - p0)*(1.0 / s); // In tan

		// Get tangents
		Vector2d t0, t1;
		if (inns == 1)
		{
			t0 = ts;
			t1 = ts;
		}
		else if (sit == 0 && !loop) // First domain (no loop)
		{
			t0 = ts;
			const Vector2d& pn = vin[sit + 2];
			t1 = 0.5*(ts + (pn - p1)*(1.0 / s));
		}
		else if (sit == inns - 1 && !loop) // Last domain (no loop)
		{
			t1 = ts;
			const Vector2d& pp = vin[sit - 1];
			t0 = 0.5*(ts + (p0 - pp)*(1.0 / s));
		}
		else if (sit == 0 && loop) // First domain (loop)
		{
			const Vector2d& pp = vin[innp - 2];
			const Vector2d& pn = vin[sit + 2];
			t0 = 0.5*(ts + (p0 - pp)*(1.0 / s)); // Mean
			t1 = 0.5*(ts + (pn - p1)*(1.0 / s)); // Mean
		}
		else if (sit == inns - 1 && loop) // Last domain (loop)
		{
			const Vector2d& pp = vin[sit - 1];
			const Vector2d& pn = vin[0];
			t0 = 0.5*(ts + (p0 - pp)*(1.0 / s)); // Mean
			t1 = 0.5*(ts + (pn - p1)*(1.0 / s)); // Mean
		}
		else
		{
			const Vector2d& pp = vin[sit - 1];
			const Vector2d& pn = vin[sit + 2];
			t0 = 0.5*(ts + (p0 - pp)*(1.0 / s)); // Mean
			t1 = 0.5*(ts + (pn - p1)*(1.0 / s)); // Mean
		}

		// Hermite interpolation

		Vector2d p = p0*(2 * t3 - 3 * t2 + 1) +
			t0*(t3 - 2 * t2 + t)*s +
			p1*(-2 * t3 + 3 * t2) +
			t1*(t3 - t2)*s;

		vout[nxtp++] = p; // Point

		nxts += outs;
	}
}

void hermiteInterpolation(const vector<Frame3d>& vin, const dVector& vsin, vector<Frame3d>& vout, bool loop, int outnp)
{
	int innp = (int)vin.size();
	int inns = (int)vsin.size();

	assert(inns == innp - 1);

	double sT = 0;
	for (int i = 0; i < inns; ++i)
		sT += vsin[i]; // Sum length

	int outns = outnp - 1; // Out number of domains
	double outs = sT / outns; // Out domain length

	// Must match ends
	vout.resize(outnp);
	vout.back() = vin.back();
	vout.front() = vin.front();

	int sit = 0;
	int nxtp = 1;
	double sums = 0;
	double nxts = outs;
	for (int i = 1; i < outnp - 1; i++)
	{
		double s = vsin[sit];
		while (i*outs > sums + s)
		{
			sit++;
			sums += s;
			s = vsin[sit];
		}

		double t = (nxts - sums) / s;
		double t2 = t*t;
		double t3 = t*t*t;

		const Frame3d& p0 = vin[sit]; // In point 0
		const Frame3d& p1 = vin[sit + 1]; // In point 1
		Frame3d ts; // In tan
		ts.tan = (p1.tan - p0.tan)*(1.0 / s);
		ts.nor = (p1.nor - p0.nor)*(1.0 / s);
		ts.bin = (p1.bin - p0.bin)*(1.0 / s); 

		// Get tangents
		Frame3d t0, t1;
		if (inns == 1)
		{
			t0 = ts;
			t1 = ts;
		}
		else if (sit == 0 && !loop) // First domain (no loop)
		{
			t0 = ts;
			const Frame3d& pn = vin[sit + 2];
			t1.tan = 0.5*(ts.tan + (pn.tan - p1.tan)*(1.0 / s));
			t1.nor = 0.5*(ts.nor + (pn.nor - p1.nor)*(1.0 / s));
			t1.bin = 0.5*(ts.bin + (pn.bin - p1.bin)*(1.0 / s));
		}
		else if (sit == inns - 1 && !loop) // Last domain (no loop)
		{
			t1 = ts;
			const Frame3d& pp = vin[sit - 1];
			t0.tan = 0.5*(ts.tan + (p0.tan - pp.tan)*(1.0 / s));
			t0.nor = 0.5*(ts.nor + (p0.nor - pp.nor)*(1.0 / s));
			t0.bin = 0.5*(ts.bin + (p0.bin - pp.bin)*(1.0 / s));
		}
		else if (sit == 0 && loop) // First domain (loop)
		{
			const Frame3d& pp = vin[innp - 2];
			const Frame3d& pn = vin[sit + 2];
			t0.tan = 0.5*(ts.tan + (p0.tan - pp.tan)*(1.0 / s)); // Mean
			t0.nor = 0.5*(ts.nor + (p0.nor - pp.nor)*(1.0 / s)); // Mean
			t0.bin = 0.5*(ts.bin + (p0.bin - pp.bin)*(1.0 / s)); // Mean

			t1.tan = 0.5*(ts.tan + (pn.tan - p1.tan)*(1.0 / s)); // Mean
			t1.nor = 0.5*(ts.nor + (pn.nor - p1.nor)*(1.0 / s)); // Mean
			t1.bin = 0.5*(ts.bin + (pn.bin - p1.bin)*(1.0 / s)); // Mean
		}
		else if (sit == inns - 1 && loop) // Last domain (loop)
		{
			const Frame3d& pp = vin[sit - 1];
			const Frame3d& pn = vin[0];
			t0.tan = 0.5*(ts.tan + (p0.tan - pp.tan)*(1.0 / s)); // Mean
			t0.nor = 0.5*(ts.nor + (p0.nor - pp.nor)*(1.0 / s)); // Mean
			t0.bin = 0.5*(ts.bin + (p0.bin - pp.bin)*(1.0 / s)); // Mean

			t1.tan = 0.5*(ts.tan + (pn.tan - p1.tan)*(1.0 / s)); // Mean
			t1.nor = 0.5*(ts.nor + (pn.nor - p1.nor)*(1.0 / s)); // Mean
			t1.bin = 0.5*(ts.bin + (pn.bin - p1.bin)*(1.0 / s)); // Mean
		}
		else
		{
			const Frame3d& pp = vin[sit - 1];
			const Frame3d& pn = vin[sit + 2];
			t0.tan = 0.5*(ts.tan + (p0.tan - pp.tan)*(1.0 / s)); // Mean
			t0.nor = 0.5*(ts.nor + (p0.nor - pp.nor)*(1.0 / s)); // Mean
			t0.bin = 0.5*(ts.bin + (p0.bin - pp.bin)*(1.0 / s)); // Mean

			t1.tan = 0.5*(ts.tan + (pn.tan - p1.tan)*(1.0 / s)); // Mean
			t1.nor = 0.5*(ts.nor + (pn.nor - p1.nor)*(1.0 / s)); // Mean
			t1.bin = 0.5*(ts.bin + (pn.bin - p1.bin)*(1.0 / s)); // Mean
		}

		// Hermite interpolation

		Frame3d p;

		p.tan = p0.tan*(2 * t3 - 3 * t2 + 1) +
				t0.tan*(t3 - 2 * t2 + t)*s +
				p1.tan*(-2 * t3 + 3 * t2) +
				t1.tan*(t3 - t2)*s;

		p.bin = p0.bin*(2 * t3 - 3 * t2 + 1) +
				t0.bin*(t3 - 2 * t2 + t)*s +
				p1.bin*(-2 * t3 + 3 * t2) +
				t1.bin*(t3 - t2)*s;

		p.nor = p0.nor*(2 * t3 - 3 * t2 + 1) +
				t0.nor*(t3 - 2 * t2 + t)*s +
				p1.nor*(-2 * t3 + 3 * t2) +
				t1.nor*(t3 - t2)*s;

		orthonormalize(p);

		vout[nxtp++] = p; // Point

		nxts += outs;
	}
}

void hermiteInterpolation(const vector<Vector3d>& vin, vector<Vector3d>& vout, bool loop, double outTol, const Vector3d& tanIni, const Vector3d& tanEnd)
{
	double length = 0;
	int nPoint = (int)vin.size();
	for (int i = 0; i < nPoint - 1; ++i)
		length += (vin[i + 1] - vin[i]).norm();
	int outnp = ceil(length / outTol) + 1; // Output

	hermiteInterpolation(vin, vout, loop, outnp, tanIni, tanEnd);
}

void hermiteInterpolation(const vector<Vector2d>& vin, const dVector& vsin, vector<Vector2d>& vout, bool loop, double outTol)
{
	assert(vin.size() - 1 == vsin.size());

	double length = 0;
	int nPoint = (int)vin.size();
	for (int i = 0; i < nPoint - 1; ++i)
		length += vsin[i]; // Add total

	int outnp = ceil(length / outTol) + 1; // Output
	hermiteInterpolation(vin, vsin, vout, loop, outnp);
}

void hermiteInterpolation(const vector<Frame3d>& vin, const dVector& vsin, vector<Frame3d>& vout, bool loop, double outTol)
{
	assert(vin.size() - 1 == vsin.size());

	double length = 0;
	int nPoint = (int)vin.size();
	for (int i = 0; i < nPoint - 1; ++i)
		length += vsin[i]; // Add total

	int outnp = ceil(length / outTol) + 1; // Output
	hermiteInterpolation(vin, vsin, vout, loop, outnp);
}

void toVector(const MatrixXd& mA, bool colMajor, VectorXd& va)
{
	va.resize(mA.rows()*mA.cols());

	int count = 0;
	if (colMajor)
	{
		for (int j = 0; j < mA.cols(); ++j)
			for (int i = 0; i < mA.rows(); ++i)
				va(count++) = mA(i, j);
	}
	else
	{
		for (int i = 0; i < mA.rows(); ++i)
			for (int j = 0; j < mA.cols(); ++j)
				va(count++) = mA(i, j);
	}
}

void computeBezierSurfacePatchCanonCon(Real sizeu, Real sizev, MatrixXd& mP)
{
	Real minX = -sizeu / 2;
	Real minZ = -sizev / 2;

	VectorXd vconx(4);
	for (int i = 0; i < 4; ++i)
		vconx(i) = minX + (sizeu / 3)*i;

	VectorXd vconz(4);
	for (int i = 0; i < 4; ++i)
		vconz(i) = minZ + (sizev/ 3)*i;

	MatrixXd Xcon(4, 4);
	MatrixXd Zcon(4, 4);
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
		{
			Xcon(i, j) = vconx(i);
			Zcon(i, j) = vconz(j);
		}

	mP.resize(16, 3);
	mP.setZero();
	int count = 0;
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
		{
		mP(count, 0) = Xcon(i, j);
		mP(count, 2) = Zcon(i, j);
		count++;
		}
}

void computeBezierSurfacePatchCanonInt(const MatrixXd& mPcon, int nuint, int nvint, MatrixXd& mP)
{
	int nint = nuint*nvint;

	VectorXd vxcon = mPcon.col(0);
	VectorXd vycon = mPcon.col(1);
	VectorXd vzcon = mPcon.col(2);

	VectorXd vuint(nuint);
	for (int i = 0; i < nuint; ++i)
		vuint(i) = (1.0 / (nuint - 1))*i;

	VectorXd vvint(nvint);
	for (int i = 0; i < nvint; ++i)
		vvint(i) = (1.0 / (nvint - 1))*i;

	MatrixXd Uint(nuint, nvint);
	MatrixXd Vint(nuint, nvint);
	for (int i = 0; i < nuint; ++i)
		for (int j = 0; j < nvint; ++j)
		{
		Uint(i, j) = vuint(i);
		Vint(i, j) = vvint(j);
		}

	MatrixXd M(4, 4);
	M(0, 0) = -1;	M(0, 1) = 3;	 M(0, 2) = -3;	 M(0, 3) = 1;
	M(1, 0) = 3;	M(1, 1) = -6;	 M(1, 2) = 3;	 M(1, 3) = 0;
	M(2, 0) = -3;	M(2, 1) = 3;	 M(2, 2) = 0;	 M(2, 3) = 0;
	M(3, 0) = 1;	M(3, 1) = 0;	 M(3, 2) = 0;	 M(3, 3) = 0;

	mP.resize(nint, 3);

	int count = 0;
	for (int i = 0; i < nuint; ++i)
		for (int j = 0; j < nvint; ++j)
		{
		Vector4d vu(Uint(i, j)*Uint(i, j)*Uint(i, j), Uint(i, j)*Uint(i, j), Uint(i, j), 1.0);
		Vector4d vv(Vint(i, j)*Vint(i, j)*Vint(i, j), Vint(i, j)*Vint(i, j), Vint(i, j), 1.0);

		Vector4d a = vu.transpose()*M;
		Vector4d b = M.transpose()*vv;
		MatrixXd H = b*a.transpose();
		VectorXd vh;
		toVector(H, true, vh);
		mP(count, 0) = vh.dot(vxcon);
		mP(count, 1) = vh.dot(vycon);
		mP(count, 2) = vh.dot(vzcon);
		count++;
		}
}

void computeBezierSurfacePatchInterpolationMatrix(int nuint, int nvint, int coord, MatrixXd& mHout)
{
	int nint = nuint*nvint;

	VectorXd vuint(nuint);
	for (int i = 0; i < nuint; ++i)
		vuint(i) = (1.0 / (nuint - 1))*i;

	VectorXd vvint(nvint);
	for (int i = 0; i < nvint; ++i)
		vvint(i) = (1.0 / (nvint - 1))*i;

	MatrixXd Uint(nuint, nvint);
	MatrixXd Vint(nuint, nvint);
	for (int i = 0; i < nuint; ++i)
		for (int j = 0; j < nvint; ++j)
		{
		Uint(i, j) = vuint(i);
		Vint(i, j) = vvint(j);
		}

	MatrixXd M(4, 4);
	M(0, 0) = -1;	M(0, 1) = 3;	 M(0, 2) = -3;	 M(0, 3) = 1;
	M(1, 0) = 3;	M(1, 1) = -6;	 M(1, 2) = 3;	 M(1, 3) = 0;
	M(2, 0) = -3;	M(2, 1) = 3;	 M(2, 2) = 0;	 M(2, 3) = 0;
	M(3, 0) = 1;	M(3, 1) = 0;	 M(3, 2) = 0;	 M(3, 3) = 0;

	MatrixXd mH(3 * nint, 3 * 16);
	mH.setZero(); // Init. to zero.

	int count = 0;
	for (int i = 0; i < nuint; ++i)
		for (int j = 0; j < nvint; ++j)
		{
		Vector4d vu(Uint(i, j)*Uint(i, j)*Uint(i, j), Uint(i, j)*Uint(i, j), Uint(i, j), 1.0);
		Vector4d vv(Vint(i, j)*Vint(i, j)*Vint(i, j), Vint(i, j)*Vint(i, j), Vint(i, j), 1.0);

		Vector4d a = vu.transpose()*M;
		Vector4d b = M.transpose()*vv;
		MatrixXd H = b*a.transpose();
		VectorXd vh;
		toVector(H, true, vh);
		for (int k = 0; k < 3; ++k)
		{
			for (int l = 0; l < 16; ++l) // Add row
				mH(nint*k + count, 16*k + l) = vh(l);
		}
		count++;
		}

	if (coord != -1)
	{
		assert(coord > 0 && coord < 3);

		mHout.resize(nint, 16);
		for (int i = 0; i < nint; ++i)
			for (int l = 0; l < 16; ++l) // Copy selected row
				mHout(i, l) = mH(3*i + coord, 16*coord + l);
	}
	else
	{
		mHout = mH;
	}
}

void orthonormalize(Frame3d& F)
{
	F.tan.normalize();
	F.bin = F.tan.cross(F.nor).normalized();
	F.nor = F.bin.cross(F.tan).normalized();
}

Vector3d computeCurvatureBinormal(const Vector3d& e0, const Vector3d& e1)
{
	Vector3d t0 = e0.normalized();
	Vector3d t1 = e1.normalized();
	return t0.cross(t1) * 2 * (1 / (1 + t0.dot(t1)));
}

double signedDistanceVF(const Vector3d& x, const Vector3d& y0, const Vector3d& y1, const Vector3d &y2, Vector3d& n, dVector& w)
{
	n = (y1 - y0).normalized().cross((y2 - y0).normalized());
	if (n.squaredNorm() < 1e-6)
		return INFINITY;
	n.normalize();
	double h = (x - y0).dot(n);
	double b0 = (y1 - x).dot((y2 - x).cross(n));
	double b1 = (y2 - x).dot((y0 - x).cross(n));
	double b2 = (y0 - x).dot((y1 - x).cross(n));
	w.resize(4);
	w[0] = 1.0;
	w[1] = -b0 / (b0 + b1 + b2);
	w[2] = -b1 / (b0 + b1 + b2);
	w[3] = -b2 / (b0 + b1 + b2);
	return h;
}

double signedDistanceEE(const Vector3d& x0, const Vector3d& x1, const Vector3d& y0, const Vector3d& y1, Vector3d& n, dVector& w) {
	n = (x1 - x0).normalized().cross((y1 - y0).normalized());
	if (n.squaredNorm() < 1e-6)
		return INFINITY;
	n.normalize();
	double h = (x0 - y0).dot(n);
	double a0 = (y1 - x1).dot((y0 - x1).cross(n));
	double a1 = (y0 - x0).dot((y1 - x0).cross(n));
	double b0 = (x0 - y1).dot((x1 - y1).cross(n));
	double b1 = (x1 - y0).dot((x0 - y0).cross(n));
	w.resize(4);
	w[0] = a0 / (a0 + a1);
	w[1] = a1 / (a0 + a1);
	w[2] = -b0 / (b0 + b1);
	w[3] = -b1 / (b0 + b1);
	return h;
}

double   signedAngle(const Vector2d& v0, const Vector2d& v1)
{
	double perpDot = v0(0)*v1(1) - v0(1)*v1(0);
	return atan2(perpDot, v0.dot(v1)); // Signed
}

double   signedAngle(const Vector3d& v0, const Vector3d& v1, const Vector3d& ref)
{
	Vector3d v0u = v0.normalized();
	Vector3d v1u = v1.normalized();

	if (v0u.cross(v1u).dot(ref) < 0.0)
		return -acos(v0u.dot(v1u));
	else return acos(v0u.dot(v1u));
}

Real sqrDistanceVertexTriangle(const Vector3d &v, const Vector3d &f0, const Vector3d &f1, const Vector3d &f2, Real* pfSParam, Real* pfTParam)
{
	Vector3d kDiff = f0 - v; //distance between a triangle point and the vertex

	Vector3d edge0 = f1 - f0; //the first edge of the triangle
	Vector3d edge1 = f2 - f0; // the second edge of the triangle

	Real fA00 = edge0[0] * edge0[0] + edge0[1] * edge0[1] + edge0[2] * edge0[2]; //squared lenght of the first edge of the triangle
	Real fA01 = edge0[0] * edge1[0] + edge0[1] * edge1[1] + edge0[2] * edge1[2]; //edge0 dot edge1
	Real fA11 = edge1[0] * edge1[0] + edge1[1] * edge1[1] + edge1[2] * edge1[2]; //squared lenght of the second edge of the triangle
	Real fB0 = edge0[0] * kDiff[0] + edge0[1] * kDiff[1] + edge0[2] * kDiff[2]; //kDiff dot edge0
	Real fB1 = kDiff[0] * edge1[0] + kDiff[1] * edge1[1] + kDiff[2] * edge1[2]; //kDiff dot edge1
	Real fC = kDiff[0] * kDiff[0] + kDiff[1] * kDiff[1] + kDiff[2] * kDiff[2];	//squared lenght of kDiff
	Real fDet = fabs(fA00*fA11 - fA01*fA01);
	Real fS = fA01*fB1 - fA11*fB0;
	Real fT = fA01*fB0 - fA00*fB1;
	Real fSqrDist;

	if (fS + fT <= fDet)
	{
		if (fS < 0.0f)
		{
			if (fT < 0.0f)  // region 4
			{
				if (fB0 < 0.0f)
				{
					fT = 0.0f;
					if (-fB0 >= fA00)
					{
						fS = 1.0f;
						fSqrDist = fA00 + 2.0f*fB0 + fC;
					}
					else
					{
						fS = -fB0 / fA00;
						fSqrDist = fB0*fS + fC;
					}
				}
				else
				{
					fS = 0.0f;
					if (fB1 >= 0.0f)
					{
						fT = 0.0f;
						fSqrDist = fC;
					}
					else if (-fB1 >= fA11)
					{
						fT = 1.0f;
						fSqrDist = fA11 + 2.0f*fB1 + fC;
					}
					else
					{
						fT = -fB1 / fA11;
						fSqrDist = fB1*fT + fC;
					}
				}
			}
			else  // region 3
			{
				fS = 0.0f;
				if (fB1 >= 0.0f)
				{
					fT = 0.0f;
					fSqrDist = fC;
				}
				else if (-fB1 >= fA11)
				{
					fT = 1.0f;
					fSqrDist = fA11 + 2.0f*fB1 + fC;
				}
				else
				{
					fT = -fB1 / fA11;
					fSqrDist = fB1*fT + fC;
				}
			}
		}
		else if (fT < 0.0f)  // region 5
		{
			fT = 0.0f;
			if (fB0 >= 0.0f)
			{
				fS = 0.0f;
				fSqrDist = fC;
			}
			else if (-fB0 >= fA00)
			{
				fS = 1.0f;
				fSqrDist = fA00 + 2.0f*fB0 + fC;
			}
			else
			{
				fS = -fB0 / fA00;
				fSqrDist = fB0*fS + fC;
			}
		}
		else  // region 0
		{
			// minimum at interior point
			Real fInvDet = 1.0f / fDet;
			fS *= fInvDet;
			fT *= fInvDet;
			fSqrDist = fS*(fA00*fS + fA01*fT + 2.0f*fB0) +
				fT*(fA01*fS + fA11*fT + 2.0f*fB1) + fC;
		}
	}
	else
	{
		Real fTmp0, fTmp1, fNumer, fDenom;

		if (fS < 0.0f)  // region 2
		{
			fTmp0 = fA01 + fB0;
			fTmp1 = fA11 + fB1;
			if (fTmp1 > fTmp0)
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00 - 2.0f*fA01 + fA11;
				if (fNumer >= fDenom)
				{
					fS = 1.0f;
					fT = 0.0f;
					fSqrDist = fA00 + 2.0f*fB0 + fC;
				}
				else
				{
					fS = fNumer / fDenom;
					fT = 1.0f - fS;
					fSqrDist = fS*(fA00*fS + fA01*fT + 2.0f*fB0) +
						fT*(fA01*fS + fA11*fT + 2.0f*fB1) + fC;
				}
			}
			else
			{
				fS = 0.0f;
				if (fTmp1 <= 0.0f)
				{
					fT = 1.0f;
					fSqrDist = fA11 + 2.0f*fB1 + fC;
				}
				else if (fB1 >= 0.0f)
				{
					fT = 0.0f;
					fSqrDist = fC;
				}
				else
				{
					fT = -fB1 / fA11;
					fSqrDist = fB1*fT + fC;
				}
			}
		}
		else if (fT < 0.0f)  // region 6
		{
			fTmp0 = fA01 + fB1;
			fTmp1 = fA00 + fB0;
			if (fTmp1 > fTmp0)
			{
				fNumer = fTmp1 - fTmp0;
				fDenom = fA00 - 2.0f*fA01 + fA11;
				if (fNumer >= fDenom)
				{
					fT = 1.0f;
					fS = 0.0f;
					fSqrDist = fA11 + 2.0f*fB1 + fC;
				}
				else
				{
					fT = fNumer / fDenom;
					fS = 1.0f - fT;
					fSqrDist = fS*(fA00*fS + fA01*fT + 2.0f*fB0) +
						fT*(fA01*fS + fA11*fT + 2.0f*fB1) + fC;
				}
			}
			else
			{
				fT = 0.0f;
				if (fTmp1 <= 0.0f)
				{
					fS = 1.0f;
					fSqrDist = fA00 + 2.0f*fB0 + fC;
				}
				else if (fB0 >= 0.0f)
				{
					fS = 0.0f;
					fSqrDist = fC;
				}
				else
				{
					fS = -fB0 / fA00;
					fSqrDist = fB0*fS + fC;
				}
			}
		}
		else  // region 1
		{
			fNumer = fA11 + fB1 - fA01 - fB0;
			if (fNumer <= 0.0f)
			{
				fS = 0.0f;
				fT = 1.0f;
				fSqrDist = fA11 + 2.0f*fB1 + fC;
			}
			else
			{
				fDenom = fA00 - 2.0f*fA01 + fA11;
				if (fNumer >= fDenom)
				{
					fS = 1.0f;
					fT = 0.0f;
					fSqrDist = fA00 + 2.0f*fB0 + fC;
				}
				else
				{
					fS = fNumer / fDenom;
					fT = 1.0f - fS;
					fSqrDist = fS*(fA00*fS + fA01*fT + 2.0f*fB0) +
						fT*(fA01*fS + fA11*fT + 2.0f*fB1) + fC;
				}
			}
		}
	}

	if (pfSParam)
		*pfSParam = fS;

	if (pfTParam)
		*pfTParam = fT;

	return fabs(fSqrDist);
};

template< class Real, class Vector3d >
Real sqrDistanceEdgeTriangle(const Vector3d &e0, const Vector3d &e1, const Vector3d &f0, const Vector3d &f1, const Vector3d &f2, Real* pfSegP, Real* pfTriP0, Real* pfTriP1)
{
	Vector3d tmp;

	Vector3d kDiff = f0 - e0;

	Vector3d edge = e1 - e0;	// the edge	 
	Vector3d edge0 = f1 - f0; //the first edge of the triangle
	Vector3d edge1 = f2 - f0; // the second edge of the triangle

	Real fA00 = edge[0] * edge[0] + edge[1] * edge[1] + edge[2] * edge[2]; //squared lenght of the edge
	Real fA01 = -(edge[0] * edge0[0] + edge[1] * edge0[1] + edge[2] * edge0[2]); // - edge dot edge0
	Real fA02 = -(edge[0] * edge1[0] + edge[1] * edge1[1] + edge[2] * edge1[2]); // - edge dot edge1
	Real fA11 = edge0[0] * edge0[0] + edge0[1] * edge0[1] + edge0[2] * edge0[2]; //squared lenght of the edge0
	Real fA12 = edge0[0] * edge1[0] + edge0[1] * edge1[1] + edge0[2] * edge1[2]; // edge0 dot edge1
	Real fA22 = edge1[0] * edge1[0] + edge1[1] * edge1[1] + edge1[2] * edge1[2]; // edge1 dot edge1
	Real fB0 = -(kDiff[0] * edge[0] + kDiff[1] * edge[1] + kDiff[2] * edge[2]); // - kDiff dot edge
	Real fB1 = kDiff[0] * edge0[0] + kDiff[1] * edge0[1] + kDiff[2] * edge0[2];	// kDiff dot edge0
	Real fB2 = kDiff[0] * edge1[0] + kDiff[1] * edge1[1] + kDiff[2] * edge1[2];	// kDiff dot edge1

	Vector3d kTriSegOrigin;
	Vector3d kTriSegDirection;

	Real fSqrDist, fSqrDist0, fR, fS, fT, fR0, fS0, fT0;

	// Set up for a relative error test on the angle between ray direction
	// and triangle normal to determine parallel/nonparallel status.

	Vector3d kN; //= edge0.Cross(edge1);
	kN[0] = edge0[1] * edge1[2] - edge0[2] * edge1[1];
	kN[1] = edge0[2] * edge1[0] - edge0[0] * edge1[2];
	kN[2] = edge0[0] * edge1[1] - edge0[1] * edge1[0];

	Real fNSqrLen = kN[0] * kN[0] + kN[1] * kN[1] + kN[2] * kN[2]; //squared lenght of kN
	Real fDot = edge[0] * kN[0] + edge[1] * kN[1] + edge[2] * kN[2]; // edge dot kN
	//bool bNotParallel = (fDot*fDot >= gs_fTolerance*fA00*fNSqrLen);
	bool bNotParallel = (fDot*fDot >= 1e-05f*fA00*fNSqrLen);

	if (bNotParallel)
	{
		Real fCof00 = fA11*fA22 - fA12*fA12;
		Real fCof01 = fA02*fA12 - fA01*fA22;
		Real fCof02 = fA01*fA12 - fA02*fA11;
		Real fCof11 = fA00*fA22 - fA02*fA02;
		Real fCof12 = fA02*fA01 - fA00*fA12;
		Real fCof22 = fA00*fA11 - fA01*fA01;
		Real fInvDet = 1.0f / (fA00*fCof00 + fA01*fCof01 + fA02*fCof02);
		Real fRhs0 = -fB0*fInvDet;
		Real fRhs1 = -fB1*fInvDet;
		Real fRhs2 = -fB2*fInvDet;

		fR = fCof00*fRhs0 + fCof01*fRhs1 + fCof02*fRhs2;
		fS = fCof01*fRhs0 + fCof11*fRhs1 + fCof12*fRhs2;
		fT = fCof02*fRhs0 + fCof12*fRhs1 + fCof22*fRhs2;

		if (fR < 0.0f)
		{
			if (fS + fT <= 1.0f)
			{
				if (fS < 0.0f)
				{
					if (fT < 0.0f)  // region 4m
					{
						// min on face s=0 or t=0 or r=0
						kTriSegOrigin = f0;
						kTriSegDirection = edge1;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
						fS = 0.0f;
						kTriSegOrigin = f0;
						kTriSegDirection = edge0;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fS0);
						fT0 = 0.0f;
						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
						fSqrDist0 = sqrDistanceVertexTriangle(e0, f0, f1, f2, &fS0, &fT0);
						fR0 = 0.0f;
						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 3m
					{
						// min on face s=0 or r=0
						kTriSegOrigin = f0;
						kTriSegDirection = edge1;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
						fS = 0.0f;
						fSqrDist0 = sqrDistanceVertexTriangle(e0, f0, f1, f2, &fS0, &fT0);
						fR0 = 0.0f;
						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
				}
				else if (fT < 0.0f)  // region 5m
				{
					// min on face t=0 or r=0
					kTriSegOrigin = f0;
					kTriSegDirection = edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fS);
					fT = 0.0f;
					fSqrDist0 = sqrDistanceVertexTriangle(e0, f0, f1, f2, &fS0, &fT0);
					fR0 = 0.0f;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 0m
				{
					// min on face r=0
					fSqrDist = sqrDistanceVertexTriangle(e0, f0, f1, f2, &fS, &fT);
					fR = 0.0f;
				}
			}
			else
			{
				if (fS < 0.0f)  // region 2m
				{
					// min on face s=0 or s+t=1 or r=0
					kTriSegOrigin = f0;
					kTriSegDirection = edge1;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
					fS = 0.0f;
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fT0);
					fS0 = 1.0f - fT0;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
					fSqrDist0 = sqrDistanceVertexTriangle(e0, f0, f1, f2, &fS0, &fT0);
					fR0 = 0.0f;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else if (fT < 0.0f)  // region 6m
				{
					// min on face t=0 or s+t=1 or r=0
					kTriSegOrigin = f0;
					kTriSegDirection = edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fS);
					fT = 0.0f;
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fT0);
					fS0 = 1.0f - fT0;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
					fSqrDist0 = sqrDistanceVertexTriangle(e0, f0, f1, f2, &fS0, &fT0);
					fR0 = 0.0f;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 1m
				{
					// min on face s+t=1 or r=0
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
					fS = 1.0f - fT;
					fSqrDist0 = sqrDistanceVertexTriangle(e0, f0, f1, f2, &fS0, &fT0);
					fR0 = 0.0f;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
			}
		}
		else if (fR <= 1.0f)
		{
			if (fS + fT <= 1.0f)
			{
				if (fS < 0.0f)
				{
					if (fT < 0.0f)  // region 4
					{
						// min on face s=0 or t=0
						kTriSegOrigin = f0;
						kTriSegDirection = edge1;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
						fS = 0.0f;
						kTriSegOrigin = f0;
						kTriSegDirection = edge0;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fS0);
						fT0 = 0.0f;
						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 3
					{
						// min on face s=0
						kTriSegOrigin = f0;
						kTriSegDirection = edge1;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
						fS = 0.0f;
					}
				}
				else if (fT < 0.0f)  // region 5
				{
					// min on face t=0
					kTriSegOrigin = f0;
					kTriSegDirection = edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fS);
					fT = 0.0f;
				}
				else  // region 0
				{
					// global minimum is interior, done
					fSqrDist = 0.0f;
				}
			}
			else
			{
				if (fS < 0.0f)  // region 2
				{
					// min on face s=0 or s+t=1
					kTriSegOrigin = f0;
					kTriSegDirection = edge1;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
					fS = 0.0f;
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fT0);
					fS0 = 1.0f - fT0;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else if (fT < 0.0f)  // region 6
				{
					// min on face t=0 or s+t=1
					kTriSegOrigin = f0;
					kTriSegDirection = edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fS);
					fT = 0.0f;
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fT0);
					fS0 = 1.0f - fT0;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 1
				{
					// min on face s+t=1
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
					fS = 1.0f - fT;
				}
			}
		}
		else  // fR > 1
		{
			if (fS + fT <= 1.0f)
			{
				if (fS < 0.0f)
				{
					if (fT < 0.0f)  // region 4p
					{
						// min on face s=0 or t=0 or r=1
						kTriSegOrigin = f0;
						kTriSegDirection = edge1;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
						fS = 0.0f;
						kTriSegOrigin = f0;
						kTriSegDirection = edge0;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fS0);
						fT0 = 0.0f;
						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}

						fSqrDist0 = sqrDistanceVertexTriangle(e1, f0, f1, f2, &fS0, &fT0);
						fR0 = 1.0f;
						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
					else  // region 3p
					{
						// min on face s=0 or r=1
						kTriSegOrigin = f0;
						kTriSegDirection = edge1;
						tmp = kTriSegOrigin + kTriSegDirection;
						fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
						fS = 0.0f;
						fSqrDist0 = sqrDistanceVertexTriangle(e1, f0, f1, f2, &fS0, &fT0);
						fR0 = 1.0f;
						if (fSqrDist0 < fSqrDist)
						{
							fSqrDist = fSqrDist0;
							fR = fR0;
							fS = fS0;
							fT = fT0;
						}
					}
				}
				else if (fT < 0.0f)  // region 5p
				{
					// min on face t=0 or r=1
					kTriSegOrigin = f0;
					kTriSegDirection = edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fS);
					fT = 0.0f;
					fSqrDist0 = sqrDistanceVertexTriangle(e1, f0, f1, f2, &fS0, &fT0);
					fR0 = 1.0f;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 0p
				{
					// min face on r=1
					fSqrDist = sqrDistanceVertexTriangle(e1, f0, f1, f2, &fS, &fT);
					fR = 1.0f;
				}
			}
			else
			{
				if (fS < 0.0f)  // region 2p
				{
					// min on face s=0 or s+t=1 or r=1
					kTriSegOrigin = f0;
					kTriSegDirection = edge1;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
					fS = 0.0f;
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fT0);
					fS0 = 1.0f - fT0;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
					fSqrDist0 = sqrDistanceVertexTriangle(e1, f0, f1, f2, &fS0, &fT0);
					fR0 = 1.0f;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else if (fT < 0.0f)  // region 6p
				{
					// min on face t=0 or s+t=1 or r=1
					kTriSegOrigin = f0;
					kTriSegDirection = edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fS);
					fT = 0.0f;
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fT0);
					fS0 = 1.0f - fT0;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
					fSqrDist0 = sqrDistanceVertexTriangle(e1, f0, f1, f2, &fS0, &fT0);
					fR0 = 1.0f;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
				else  // region 1p
				{
					// min on face s+t=1 or r=1
					kTriSegOrigin = f0 + edge0;
					kTriSegDirection = edge1 - edge0;
					tmp = kTriSegOrigin + kTriSegDirection;
					fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fT);
					fS = 1.0f - fT;
					fSqrDist0 = sqrDistanceVertexTriangle(e1, f0, f1, f2, &fS0, &fT0);
					fR0 = 1.0f;
					if (fSqrDist0 < fSqrDist)
					{
						fSqrDist = fSqrDist0;
						fR = fR0;
						fS = fS0;
						fT = fT0;
					}
				}
			}
		}
	}
	else
	{
		// segment and triangle are parallel
		kTriSegOrigin = f0;
		kTriSegDirection = edge0;
		tmp = kTriSegOrigin + kTriSegDirection;
		fSqrDist = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR, &fS);
		fT = 0.0f;

		kTriSegDirection = edge1;
		tmp = kTriSegOrigin + kTriSegDirection;
		fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fT0);
		fS0 = 0.0f;
		if (fSqrDist0 < fSqrDist)
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		kTriSegOrigin = f0 + edge0;
		kTriSegDirection = edge1 - edge0;
		tmp = kTriSegOrigin + kTriSegDirection;
		fSqrDist0 = sqrDistanceEdgeEdge(e0, e1, kTriSegOrigin, tmp, &fR0, &fT0);
		fS0 = 1.0f - fT0;
		if (fSqrDist0 < fSqrDist)
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		fSqrDist0 = sqrDistanceVertexTriangle(e0, f0, f1, f2, &fS0, &fT0);
		fR0 = 0.0f;
		if (fSqrDist0 < fSqrDist)
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}

		fSqrDist0 = sqrDistanceVertexTriangle(e1, f0, f1, f2, &fS0, &fT0);
		fR0 = 1.0f;
		if (fSqrDist0 < fSqrDist)
		{
			fSqrDist = fSqrDist0;
			fR = fR0;
			fS = fS0;
			fT = fT0;
		}
	}

	if (pfSegP)
		*pfSegP = fR;

	if (pfTriP0)
		*pfTriP0 = fS;

	if (pfTriP1)
		*pfTriP1 = fT;

	return fSqrDist;
};


void toTriplets(const MatrixSd& mM, tVector& vM)
{
	int NZ = mM.nonZeros();
	if (vM.capacity() < NZ)
		vM.reserve(NZ);
	vM.clear();

	for (int k = 0; k < mM.outerSize(); ++k)
		for (MatrixSd::InnerIterator it(mM, k); it; ++it)
			vM.push_back(Triplet<Real>(it.row(), it.col(), it.value()));
}

void buildSelectionMatrix(const vector<iVector>& vselMap, MatrixSd& mS, bool shrink)
{
	int numAll = (int) vselMap.size();

	set<int> vset;
	for (int i = 0; i < numAll; ++i)
		vset.insert(vselMap[i].begin(), vselMap[i].end());

	// How many different indices?

	int numSel = (int) vset.size();

	if (shrink)
	{
		tVector vS;
		vS.reserve(numAll);
		for (int i = 0; i < numAll; ++i)
		{
			for (int j = 0; j < (int)vselMap[i].size(); ++j)
				vS.push_back(Triplet<Real>(vselMap[i][j], i, 1.0));
		}

		mS = MatrixSd(numSel, numAll);
		mS.setFromTriplets(vS.begin(), vS.end());
		mS.makeCompressed();
	}
	else
	{
		tVector vS;
		vS.reserve(numAll);
		for (int i = 0; i < numAll; ++i)
		{
			for (int j = 0; j < (int)vselMap[i].size(); ++j)
				vS.push_back(Triplet<Real>(vselMap[i][j], i, 1.0));
		}

		mS = MatrixSd(numAll, numAll);
		mS.setFromTriplets(vS.begin(), vS.end());
		mS.makeCompressed();
	}
}

void buildExtendedMatrix(const MatrixSd& mSin, MatrixSd& mSout, int dim)
{
	int nrows = mSin.rows();
	int ncols = mSin.cols();

	tVector vin;
	toTriplets(mSin, vin);
	int numC = (int) vin.size();

	tVector vout;
	vout.reserve(dim*numC);
	for (int i = 0; i < numC; ++i)
		for (int d = 0; d < dim; ++d)
			vout.push_back(Triplet<Real>(dim*vin[i].row() + d, dim*vin[i].col() + d, vin[i].value()));

	mSout = MatrixSd(dim*nrows, dim*ncols);
	mSout.setFromTriplets(vout.begin(), vout.end());
	mSout.makeCompressed();
}
