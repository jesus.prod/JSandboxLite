#ifndef TD_RODMESH_SIMULATOR_H
#define TD_RODMESH_SIMULATOR_H


#include <JSandbox/Solver/PhysSolver.h>
#include <JSandbox/Model/RodMeshModel.h>
#include <JSandbox/Model/RodMeshBasicModel.h>
#include <JSandbox/Solver/SolverGenericDynSta.h>
#include <JSandbox/Model/ModelDesignProblem.h>

#include <JSandbox/Solver/BConditionGravity.h>
#include <JSandbox/Solver/BConditionFixed.h>
#include <JSandbox/Solver/BConditionForce.h>

class RodMeshSimulator
{
public:
	RodMeshSimulator();
	~RodMeshSimulator();

	Real ComputeForceResidual(PhysSolver* pSolver, SolidModel* pModel);
	bool StepStaticEquilibrium(PhysSolver* pSolver, SolidModel* pModel);
	bool SolveStaticEquilibrium(PhysSolver* pSolver, SolidModel* pModel);

	void SetBoundaryConditions(PhysSolver* pSolver, RodMeshBasicModel* pModel, const iVector& vnodeFixed, const iVector& edgeFixed);
	void SetBoundaryConditions(PhysSolver* pSolver, RodMeshModel* pModel, const iVector& vnodeFixed, const iVector& edgeFixed);

	void TutorialTest1();
	void TutorialTest2();
	void DemoHangingStrip(int x, int y, Real scale, Real Ks, Real Kbh, Real Kbw, Real Kt, Real mass);

	const MatrixSd& GetLaplacianMatrix() const { return *this->m_L; }
	const MatrixSd& GetDpdDpn() const { return *this->m_DpdDpn; }
	const MatrixSd& GetDfnDfd() const { return *this->m_DfnDfd; }
	const VectorXd& GetTargetLaplacianCoords() const { return m_targetLap; }
	const VectorXd& GetTargetCartesianCoords() const { return m_targetCar; }

	void ComputePositionVector(const RodMesh& mesh, VectorXd& vX);
	void ComputeLaplacianMatrix(const RodMesh& mesh, MatrixSd& mL);
	void Transform_Default2Normalized(const VectorXd& vpDef, const VectorXd& vpMin, const VectorXd& vpRan, VectorXd& vpNor);
	void Transform_Normalized2Default(const VectorXd& vpNor, const VectorXd& vpMin, const VectorXd& vpRan, VectorXd& vpDef);
	void Assemble_DNormalizedDDefault(int numBlocks, const VectorXd& vran, MatrixSd& DnDd);
	void Assemble_DDefaultDNormalized(int numBlocks, const VectorXd& vran, MatrixSd& DdDn);
	void Assemble_DParametersDFabrication(const vector<MatrixXd>& vDpDfBlock, MatrixSd& DpDf);
	int ComputeResidual_Laplacian(const VectorXd& vparams, VectorXd& residual);
	int ComputeJacobian_Laplacian(const VectorXd& vparams, tVector& jacobian);
	int InitializeOptimization(RodMeshBasicModel* pModel, PhysSolver* pSolver,
		const VectorXd& targetPos,
		const VectorXd& vpMin, const VectorXd& vpRan,
		const VectorXd& vfMin, const VectorXd& vfRan,
		VectorXd initialPar = VectorXd());
	
	ModelDesignProblem* CreateBasicProblem(RodMeshBasicModel* pModel, PhysSolver* pSolver, const VectorXd& vpMin, const VectorXd& vpMax);
	RodMeshBasicModel* CreateBasicModel(RodMesh* rodMeshx, const SolidMaterial& material, const Vector3d& vgravity);
	RodMeshModel* CreateModel(RodMesh* rodMeshx, const SolidMaterial& material, const Vector3d& vgravity);
	PhysSolver* CreateSolver(SolidModel* pModel, int blockSteps, PhysSolver::LinearSolver solverType);

	void CreateStraightRod(const Vector3d& a, const Vector3d& b, int numPoints, Rod& rodOutput);

	PhysSolver* m_pSolver;			// Physics solver
	RodMeshBasicModel* m_pModel;	// Rod mesh model
	ModelDesignProblem* m_pDesign;	// Design problem

	pSolidState m_state;		// Current optimization state

	MatrixSd m_test;
	MatrixSd* m_L;
	MatrixSd* m_DpdDpn;
	MatrixSd* m_DfnDfd;
	VectorXd m_targetLap;		// Target in Laplacian coordinates
	VectorXd m_targetCar;		// Target in Cartesian coordinates

};

#endif
