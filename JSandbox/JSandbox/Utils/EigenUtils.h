/*=====================================================================================*/
/*!
\file		EigenUtils.h
\author		jesusprod
\brief		Declaration of Eigen related utilities.
*/
/*=====================================================================================*/

#ifndef EIGEN_UTILS_H
#define EIGEN_UTILS_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

class MatrixElementsXd
{
public:
	MatrixElementsXd()
	{
		m_velements.clear();
		m_numElements = -1;
	}

	virtual tVector& getTripletVector() { return this->m_velements; }

	virtual bool isAllocated() const { return m_numElements != -1; }

	virtual void allocate(int numElements) 
	{
		assert(numElements > 0);
		this->m_numElements = numElements;
		this->m_velements.reserve(numElements);
	}

protected:
	tVector m_velements;
	int m_numElements;
};

#endif