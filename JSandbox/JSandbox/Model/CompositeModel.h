/*=====================================================================================*/
/*!
/file		CompositeModel.h
/author		jesusprod
/brief		Base abstract class for all composite models.
*/
/*=====================================================================================*/

#ifndef COMPOSITE_MODEL_H
#define COMPOSITE_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>
#include <JSandbox/Model/SolidModel.h>
#include <JSandbox/Model/ModelCoupling.h>

class   CompositeState : public SolidState
{
public:
	vector<pSolidState> m_vstates;

	CompositeState()
	{
		// Nothing to do here
	}

	virtual ~CompositeState()
	{
		// Nothing to do here
	}

	CompositeState(const CompositeState& toCopy) : SolidState(toCopy)
	{
		assert(this->m_vstates.size() == toCopy.m_vstates.size());
		int ns = (int) this->m_vstates.size();
		for (int i = 0; i < ns; ++i)
			*this->m_vstates[i] = *toCopy.m_vstates[i];
	}

	virtual pSolidState clone() const
	{
		CompositeState* pcloned = new CompositeState();
		pcloned->m_vx = this->m_vx;
		pcloned->m_vv = this->m_vv;
		int ns = (int) this->m_vstates.size();
		pcloned->m_vstates.resize(ns);
		for (int i = 0; i < ns; ++i)
			pcloned->m_vstates[i] = this->m_vstates[i]->clone();
		return pSolidState(pcloned);
	}

	virtual bool operator==(const SolidState& other) const
	{
		bool same = true;
		same &= (this->m_vx - other.m_vx).isZero(1e-6);
		same &= (this->m_vv - other.m_vv).isZero(1e-6);
		const CompositeState* pOther = dynamic_cast<const CompositeState*>(&other);
		same &= pOther != NULL;
		same &= this->m_vstates.size() == pOther->m_vstates.size();
		int ns = (int) this->m_vstates.size();
		for (int i = 0; i < ns; ++i)
		{
			same &= *this->m_vstates[i] == *pOther->m_vstates[i];
		}
		return same;
	}
};

typedef std::shared_ptr<CompositeState> pCompositeState;

class   CompositeModel : public SolidModel
{
public:
	CompositeModel(const string& ID);

	virtual ~CompositeModel();

	/* From SolidModel */

	virtual void setup();

	virtual void setState_x(pSolidState s);
	virtual void setState_0(pSolidState s);
	virtual void setPositions_x(const VectorXd& vx);
	virtual void setPositions_0(const VectorXd& v0);
	virtual void setVelocities_x(const VectorXd& vv);

	virtual Real getEnergy();
	virtual void addForce(VectorXd& vf, const vector<bool>* pvFixed = NULL);
	virtual void addJacobian(tVector& vJ, const vector<bool>* pvFixed = NULL);

	virtual void update_Mass();

	virtual void add_Mass(VectorXd& vM);
	virtual void add_Mass(tVector& mM);

	virtual void add_vgravity(VectorXd& vg);
	virtual void add_fgravity(VectorXd& vf);

	virtual void addGravity(const VectorXd& vg, VectorXd& vf, const vector<bool>* pvFixed = NULL);

	virtual void update_SimDOF();

	virtual iVector getAABBIndices(const dVector& vaabb) const;

	virtual void deprecateConfiguration();
	virtual void deprecateDiscretization();

	virtual void testForceLocal();
	virtual void testForceGlobal();
	virtual void testJacobianLocal();
	virtual void testJacobianGlobal();

	virtual void add_fx(VectorXd& vfx) { /* throw new exception("[ERROR] Not implemented yet"); */ }
	virtual void add_f0(VectorXd& vf0) { /* throw new exception("[ERROR] Not implemented yet"); */ }

	virtual void add_DfxDx(tVector& vJ, const vector<bool>* pvFixed = NULL);
	virtual void add_DfxD0(tVector& vJ, const vector<bool>* pvFixed = NULL);

	virtual void getParam_Rest(VectorXd& vX) const { /* throw new exception("[ERROR] Not implemented yet"); */ }
	virtual void setParam_Rest(const VectorXd& vX) { /* throw new exception("[ERROR] Not implemented yet"); */ }

	virtual void setParam_Material(const vector<SolidModel>& vmats) { /* throw new exception("[ERROR] Not implemented yet"); */ }
	virtual void getParam_Material(vector<SolidModel>& vmats) const { /* throw new exception("[ERROR] Not implemented yet"); */ }

	virtual void setGravity(const VectorXd& vg) 
	{ 
		SolidModel::setGravity(vg); 
		for (int i = 0; i < (int) this->m_vpModels.size(); ++i) 
			this->m_vpModels[i]->setGravity(vg); 
	}

	////////////////////////////////////////////////////////////////////////////
	// Events and event handlers

	virtual void hookSubmodelDiscretizationChanged();
	virtual void unhookSubmodelDiscretizationChanged();

	virtual void onSubmodelDiscretizationChanged(SolidModel* pmodel, const iVector& vnewIdxRaw);

public:

	/* From SolidModel */

	virtual void addLocal2Global(int i, const VectorXd& vin, VectorXd& vout) const;
	virtual void addGlobal2Local(int i, const VectorXd& vin, VectorXd& vout) const;

	virtual void addLocal2Global(int i, const tVector& vin, tVector& vout) const;
	virtual void addLocal2Global_Tri(int i, const tVector& vin, tVector& vout) const;
	virtual void addLocal2Global_Row(int i, const tVector& vin, tVector& vout) const;

	virtual void mapLocal2GlobalSim_x(int i, const VectorXd& vin, VectorXd& vout) const;
	virtual void mapGlobal2LocalSim_x(int i, const VectorXd& vin, VectorXd& vout) const;
	virtual void mapLocal2GlobalSim_0(int i, const VectorXd& vin, VectorXd& vout) const;
	virtual void mapGlobal2LocalSim_0(int i, const VectorXd& vin, VectorXd& vout) const;

	virtual void mapLocal2GlobalRaw(int i, const VectorXd& vin, VectorXd& vout) const;
	virtual void mapGlobal2LocalRaw(int i, const VectorXd& vin, VectorXd& vout) const;

	virtual void mapLocal2GlobalRaw(int i, const iVector& vin, iVector& vout) const;
	virtual void mapGlobal2LocalRaw(int i, const iVector& vin, iVector& vout) const;

	virtual void getGlobal2Local(int i, MatrixSd& mg2l) { /* TODO */ }

	/*! This method require to be overwritten as it is not straightforwad 
	* just to make the conversion directly in the case of composite models.
	*/
	virtual void mapSim2Raw(const VectorXd& vin, VectorXd& vout) const;

	void configureModels(const vector<SolidModel*>& vpModels, const vector<ModelCoupling*>& vpCouplings);

	int getNumCoupling() const { return (int) this->m_vpCouplings.size(); }
	const vector<ModelCoupling*> getCouplings() const { return this->m_vpCouplings; }
	ModelCoupling* getCoupling(int i) { assert(i >= 0 && i < this->getNumModel()); return this->m_vpCouplings[i]; }
	const ModelCoupling* getCoupling(int i) const { assert(i >= 0 && i < this->getNumModel()); return this->m_vpCouplings[i]; }

	int getNumModel() const { return (int) this->m_vpModels.size(); }
	const vector<SolidModel*> getModels() const { return this->m_vpModels; }
	SolidModel* getModel(int i) { assert(i >= 0 && i < this->getNumModel()); return this->m_vpModels[i]; }
	const SolidModel* getModel(int i) const { assert(i >= 0 && i < this->getNumModel()); return this->m_vpModels[i]; }

protected:

	virtual void projectFixedToModel(int i, const bVector& vglo, bVector& vloc);

	virtual void freeModelsAndCouplings();

	vector<SolidModel*> m_vpModels;
	vector<ModelCoupling*> m_vpCouplings;

	iVector m_vmodelRawIdx;

	vector<vector<DOFLinks>> m_vDOFLinks;

	pCompositeState m_stateC_x;
	pCompositeState m_stateC_0;

	vector<VectorXd> m_vfModel;
	vector<tVector> m_vJModel;

	// Transforming local - global
	vector<MatrixSd> m_vDgDl;

	vector<CustomTimer> m_vassembleJacTimerModel;

};

#endif