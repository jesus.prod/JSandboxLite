/*=====================================================================================*/
/*!
/file		CompositeModel.cpp
/author		jesusprod
/brief		Implementation of CompositeModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/CompositeModel.h>

#ifdef _OPENMP 
#include <omp.h>
#endif

CompositeModel::CompositeModel(const string& ID) : SolidModel(ID)
{
	this->m_stateC_x = pCompositeState(new CompositeState());
	this->m_stateC_0 = pCompositeState(new CompositeState());
	this->m_state_x = this->m_stateC_x;
	this->m_state_0 = this->m_stateC_0;

	this->deprecateConfiguration();
}

CompositeModel::~CompositeModel()
{
	this->freeModelsAndCouplings();

	logSimu("[INFO] Destroying CompositeModel: %u", this);
}

void CompositeModel::configureModels(const vector<SolidModel*>& vpModels, const vector<ModelCoupling*>& vpCouplings)
{
	this->freeModelsAndCouplings();

	this->m_vpModels = vpModels;
	this->m_vpCouplings = vpCouplings;

	this->hookSubmodelDiscretizationChanged();

	this->deprecateConfiguration();
}

void CompositeModel::deprecateConfiguration()
{
	SolidModel::deprecateConfiguration();

	this->m_vmodelRawIdx.clear();
	this->m_vDOFLinks.clear();
}

void CompositeModel::deprecateDiscretization()
{
	SolidModel::deprecateDiscretization();

	// Nothing to do here...
}

void CompositeModel::setup()
{
	this->m_vassembleJacTimerModel.resize(this->getNumModel());
	for (int i = 0; i < this->getNumModel(); ++i) // Initialize
	{
		ostringstream IDstreamAJ;
		IDstreamAJ << "SUBMOD_" << i << "_" << this->m_ID.c_str(); // Name
		this->m_vassembleJacTimerModel[i] = CustomTimer(1, IDstreamAJ.str());
		this->m_vassembleJacTimerModel[i].initialize();
	}

	// Initial DOF world

	this->m_nrawDOF = 0;
	this->m_vmodelRawIdx.clear();
	for (int i = 0; i < this->getNumModel(); ++i) // Each
	{
		if (!this->m_vpModels[i]->isReady_Setup())
			this->m_vpModels[i]->setup(); // Remesh?

		// Store last count of raw degrees-of-freedom
		this->m_vmodelRawIdx.push_back(this->m_nrawDOF);

		this->m_nrawDOF += this->m_vpModels[i]->getNumRawDOF();
	}

	this->m_isReady_Setup = true;
	this->deprecateDiscretization();

	// Update simulation
	this->update_SimDOF();

	this->m_vfModel.resize(this->getNumModel());
	this->m_vJModel.resize(this->getNumModel());
}

void CompositeModel::setState_x(pSolidState s)
{
	//this->m_stateC_x = pCompositeState(static_cast<CompositeState*>(s.get()));

	//*this->m_stateC_x = *static_cast<CompositeState*>(s.get());

	CompositeState* pCS = static_cast<CompositeState*>(s.get());

	this->m_stateC_x->m_vx = s->m_vx;
	this->m_stateC_x->m_vv = s->m_vv;

	int nm = this->getNumModel();
	for (int i = 0; i < nm; ++i)
		this->m_vpModels[i]->setState_x(pCS->m_vstates[i]);

	this->deprecateDeformation();
}

void CompositeModel::setState_0(pSolidState s)
{
	//this->m_stateC_0 = pCompositeState(static_cast<CompositeState*>(s.get()));

	CompositeState* pCS = static_cast<CompositeState*>(s.get());

	this->m_stateC_0->m_vx = s->m_vx;
	this->m_stateC_0->m_vv = s->m_vv;

	int nm = this->getNumModel();
	for (int i = 0; i < nm; ++i)
		this->m_vpModels[i]->setState_x(this->m_stateC_0->m_vstates[i]);

	this->deprecateDeformation();
}

void CompositeModel::setPositions_x(const VectorXd& vx)
{
	for (int i = 0; i < this->getNumModel(); ++i)
	{
		VectorXd vxModel(this->m_vpModels[i]->getNumSimDOF_x());
		this->mapGlobal2LocalSim_x(i, vx, vxModel);
		this->m_vpModels[i]->setPositions_x(vxModel);
	}

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		this->mapLocal2GlobalSim_x(i, this->m_vpModels[i]->getPositions_x(), this->m_state_x->m_vx);
	}

	this->deprecateDeformation();
}

void CompositeModel::setPositions_0(const VectorXd& v0)
{
	for (int i = 0; i < this->getNumModel(); ++i)
	{
		VectorXd v0Model(this->m_vpModels[i]->getNumSimDOF_x());
		this->mapGlobal2LocalSim_x(i, v0, v0Model);
		this->m_vpModels[i]->setPositions_0(v0Model);
	}

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		this->mapLocal2GlobalSim_x(i, this->m_vpModels[i]->getPositions_0(), this->m_state_0->m_vx);
	}

	this->deprecateDeformation();
}

void CompositeModel::setVelocities_x(const VectorXd& vv)
{
	for (int i = 0; i < this->getNumModel(); ++i)
	{
		VectorXd vvModel(this->m_vpModels[i]->getNumSimDOF_x());
		this->mapGlobal2LocalSim_x(i, vv, vvModel);
		this->m_vpModels[i]->setVelocities_x(vvModel);
	}

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		this->mapLocal2GlobalSim_x(i, this->m_vpModels[i]->getVelocities_x(), this->m_state_x->m_vv);
	}
}

void CompositeModel::testForceLocal()
{
	for (int i = 0; i < this->getNumModel(); ++i)
		this->m_vpModels[i]->testForceLocal(); 
}

void CompositeModel::testJacobianLocal()
{
	for (int i = 0; i < this->getNumModel(); ++i)
		this->m_vpModels[i]->testJacobianLocal();
}

void CompositeModel::testForceGlobal()
{
	for (int i = 0; i < this->getNumModel(); ++i)
		this->m_vpModels[i]->testForceGlobal();

	SolidModel::testForceGlobal();
}

void CompositeModel::testJacobianGlobal()
{
	for (int i = 0; i < this->getNumModel(); ++i)
		this->m_vpModels[i]->testJacobianGlobal();

	SolidModel::testJacobianGlobal();
}

void CompositeModel::hookSubmodelDiscretizationChanged()
{
	//for (int i = 0; i < this->m_vpModels.size(); ++i)
	//	__hook(&SolidModel::discretizationChanged, this->m_vpModels[i], &CompositeModel::onSubmodelDiscretizationChanged);
}

void CompositeModel::unhookSubmodelDiscretizationChanged()
{
	//for (int i = 0; i < this->m_vpModels.size(); ++i)
	//	__unhook(&SolidModel::discretizationChanged, this->m_vpModels[i], &CompositeModel::onSubmodelDiscretizationChanged);
}

void CompositeModel::onSubmodelDiscretizationChanged(SolidModel* pmodel, const iVector& vnewRawIdx)
{
	int midx = -1;
	for (int i = 0; i < this->getNumModel(); ++i)
		if (this->m_vpModels[i] == pmodel)
		{
		midx = i; break;
		}

	// Our model?
	if (midx == -1)
		return;

	// Just notify that it is necessary to reset
	// all configuration dependent preprocesses.
	// Any discretization change might produce a
	// change in the total DOF number so we just
	// force the model to reset.

	this->deprecateConfiguration();

	// TODO: Is there a better solution here?
	// If just a reordering of the DOF happens
	// but the number remains constant it would
	// be enought to remap the DOF of the model.

	iVector vnewRawIdxGlobal; // Map to global and transmit
	this->mapLocal2GlobalRaw(midx, vnewRawIdx, vnewRawIdxGlobal);

	// Notify about the changes in the submodel topology indices
	//__raise this->discretizationChanged(this, vnewRawIdxGlobal);
}

void CompositeModel::update_SimDOF()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	// Update DOF links at deformed ----------------------------------------------------------------

	this->m_vDOFLinks.resize(this->getNumModel());

	// Update simulation indices

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		this->m_vDOFLinks[i].clear(); // Delete all previous links
		int nsimDOFModel = this->m_vpModels[i]->getNumSimDOF_x();
		this->m_vDOFLinks[i].resize(nsimDOFModel);
	}

	this->m_nsimDOF_x = 0;	

	// Coupling

	for (int i = 0; i < this->getNumCoupling(); ++i) 
	{
		const ModelCoupling* pC = this->m_vpCouplings[i];

		if (pC->getType() == ModelCoupling::Type::Constraint)
		{
			// Ignore, these are treated using soft/hard constraints
		}
		else if (pC->getType() == ModelCoupling::Type::Shared)
		{
			int m0 = pC->getModel0();
			int m1 = pC->getModel1();
			const ModelPoint& point0 = pC->getModelPoint0();
			const ModelPoint& point1 = pC->getModelPoint1();
			assert(point0.isExplicit());
			assert(point1.isExplicit());
			int numDim = point0.getNumDim();
			int numSup = point0.getNumSup();
			const iVector& vsim0 = this->m_vpModels[m0]->getRawToSim_x();
			const iVector& vsim1 = this->m_vpModels[m1]->getRawToSim_x();

			for (int j = 0; j < numDim; ++j)
			{
				int point0RawIdx = point0.getPointIdx() + j;
				int point1RawIdx = point1.getPointIdx() + j;
				int point0SimIdx = vsim0[point0RawIdx];
				int point1SimIdx = vsim1[point1RawIdx];

				// Initialize both linked DOF: the following lines allocate both coupled 
				// DOF. If any of the DOF are already initialized to an index, the other 
				// is copied. Otherwise, bot are allocated to the same DOF.

				if (this->m_vDOFLinks[m1][point1SimIdx].m_isInit) // Copy m1 to m0
				{
					this->m_vDOFLinks[m0][point0SimIdx] = this->m_vDOFLinks[m1][point1SimIdx];
				}
				else if (this->m_vDOFLinks[m0][point0SimIdx].m_isInit) // Copy m0 to m1
				{
					this->m_vDOFLinks[m1][point1SimIdx] = this->m_vDOFLinks[m0][point0SimIdx];
				}
				else // Initialize both of them to the same DOF
				{
					this->m_vDOFLinks[m0][point0SimIdx].addLink(this->m_nsimDOF_x, 1.0);
					this->m_vDOFLinks[m1][point1SimIdx].addLink(this->m_nsimDOF_x, 1.0);
					this->m_nsimDOF_x++;
				}
			}
		}
		else if (pC->getType() == ModelCoupling::Type::Embedded)
		{
			int m0 = pC->getModel0();
			int m1 = pC->getModel1();
			const ModelPoint& pointSlave = pC->getModelPoint0();
			const ModelPoint& pointMaster = pC->getModelPoint1();
			assert(pointSlave.isExplicit());
			int numDim = pointMaster.getNumDim();
			int numSup = pointMaster.getNumSup();

			const iVector& vsimSlave = this->m_vpModels[m0]->getRawToSim_x();
			const iVector& vsimMaster = this->m_vpModels[m1]->getRawToSim_x();

			for (int j = 0; j < numDim; ++j)
			{
				int slaveIdx = pointSlave.getPointIdx() + j;	
				iVector vmasterIdx = pointMaster.getIndices();
				dVector vmasterWei = pointMaster.getWeights();

				// Add the dimension offset
				for (int k = 0; k < numSup; ++k) 
					vmasterIdx[k] = vmasterIdx[k] + j;

				// Initialize master embedding: the following loop allocates the 
				// master DOF of the embedding relation to their corresponding 
				// simulation indices if they haven't been initialized yet.

				for (int l = 0; l < numSup; ++l)
					if (!this->m_vDOFLinks[m1][vsimMaster[vmasterIdx[l]]].m_isInit)
						this->m_vDOFLinks[m1][vsimMaster[vmasterIdx[l]]].addLink(this->m_nsimDOF_x++, 1.0);

				// Initialize slave embedding: the following loop allocates the 
				// slave DOF of the embedding to the same indices of the master 
				// DOF, with the interpolation weight.

				for (int l = 0; l < numSup; ++l)
				{
					int masterGlobalIdx = this->m_vDOFLinks[m1][vsimMaster[vmasterIdx[l]]].m_vidx[0];
					this->m_vDOFLinks[m0][vsimSlave[slaveIdx]].addLink(masterGlobalIdx, vmasterWei[l]);
				}
			}
		}
		else
		{
			assert(false); // Something is wrong here
		}
	}

	// Initialize remaining degrees-of-freedom

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		int nsimDOFModel = this->m_vpModels[i]->getNumSimDOF_x();

		for (int j = 0; j < nsimDOFModel; ++j)
		{
			if (this->m_vDOFLinks[i][j].m_isInit)
				continue; // Already initialized

			this->m_vDOFLinks[i][j].addLink(this->m_nsimDOF_x++, 1.0);
		}
	}


	// Update simulation DOF

	this->m_vraw2sim_x.resize(this->m_nrawDOF);
	for (int i = 0; i < this->getNumModel(); ++i)
	{
		const iVector& vsimDOFModel = this->m_vpModels[i]->getRawToSim_x();
		int nrawDOFModel = this->m_vpModels[i]->getNumRawDOF();
		assert((int)vsimDOFModel.size() == nrawDOFModel);

		for (int j = 0; j < nrawDOFModel; ++j)
		{
			const DOFLinks& links = this->m_vDOFLinks[i][vsimDOFModel[j]];

			if (links.m_numLink != 1) 
				this->m_vraw2sim_x[this->m_vmodelRawIdx[i] + j] = -1; // Ignore it!
			else this->m_vraw2sim_x[this->m_vmodelRawIdx[i] + j] = links.m_vidx[0];
		}
	}

	// There is no embedding of the rest state of the object, so the number of simulation DOF
	// for the rest configuration is equal to the the sum of the one from internal models. 

	int totalRawCount = 0;
	this->m_nsimDOF_0 = 0;
	
	this->m_vraw2sim_0.resize(this->m_nrawDOF);
	for (int i = 0; i < this->getNumModel(); ++i)
	{
		const iVector& vsimDOF0Model = this->m_vpModels[i]->getRawToSim_0();

		int nrawDOFModel_0 = this->m_vpModels[i]->getNumRawDOF();
		int nsimDOFModel_0 = this->m_vpModels[i]->getNumSimDOF_0();

		for (int j = 0; j < nrawDOFModel_0; ++j) // Concatenation
			this->m_vraw2sim_0[totalRawCount + j] = vsimDOF0Model[j];

		totalRawCount += nrawDOFModel_0;
		this->m_nsimDOF_0 += nsimDOFModel_0;
	}

	// Update vectors

	this->m_stateC_x->m_vx.resize(this->m_nsimDOF_x);
	this->m_stateC_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_stateC_0->m_vx.resize(this->m_nsimDOF_0);
	this->m_stateC_0->m_vv.resize(this->m_nsimDOF_0);
	this->m_stateC_x->m_vv.setZero();
	this->m_stateC_0->m_vv.setZero();
	this->m_stateC_x->m_vstates.resize(this->getNumModel());
	this->m_stateC_0->m_vstates.resize(this->getNumModel());
	for (int i = 0; i < this->getNumModel(); ++i)
	{
		this->m_stateC_x->m_vstates[i] = this->m_vpModels[i]->getStatePointer_x();
		this->m_stateC_0->m_vstates[i] = this->m_vpModels[i]->getStatePointer_0();
		this->mapLocal2GlobalSim_x(i, this->m_vpModels[i]->getPositions_x(), this->m_state_x->m_vx);
		this->mapLocal2GlobalSim_0(i, this->m_vpModels[i]->getPositions_0(), this->m_state_0->m_vx);
	}
}

void CompositeModel::add_Mass(VectorXd& vM)
{
	for (int i = 0; i < this->getNumModel(); ++i) // Check sub-models
		this->m_isReady_Mass &= this->m_vpModels[i]->isReady_Mass();

	if (!this->m_isReady_Mass)
		this->update_Mass();

	SolidModel::add_Mass(vM);
}

void CompositeModel::add_Mass(tVector& mM)
{
	for (int i = 0; i < this->getNumModel(); ++i) // Check sub-models
		this->m_isReady_Mass &= this->m_vpModels[i]->isReady_Mass();

	if (!this->m_isReady_Mass)
		this->update_Mass();

	SolidModel::add_Mass(mM);
}

void CompositeModel::update_Mass()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	for (int i = 0; i < this->getNumModel(); ++i) // Check sub-models
		this->m_isReady_Mass &= this->m_vpModels[i]->isReady_Mass();

	if (this->m_isReady_Mass)
		return; // Already done

	// Update lumped mass vector

	this->m_vMass.resize(this->m_nsimDOF_x);
	this->m_vMass.setZero(); // Initialize

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		VectorXd vmModel;
		this->m_vpModels[i]->add_Mass(vmModel);
		assert(vmModel.size() == this->m_vpModels[i]->getNumSimDOF_x());

		// Add local mass entries to global mass vector
		this->addLocal2Global(i, vmModel, this->m_vMass);
	}

	// Update sparse mass matrix

	this->updateSparseMatrixToLumpedMass();

	this->deprecateDeformation();
	this->m_isReady_Mass = true;
}

Real CompositeModel::getEnergy()
{
	Real energy = 0;

#ifdef OMP_ENFORJAC
	#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumModel(); ++i)
	{
		Real localEnergy = this->m_vpModels[i]->getEnergy();

#pragma omp critical
		{
			energy += localEnergy;
		}
	}

	return energy;
}

void CompositeModel::addForce(VectorXd& vf, const vector<bool>* pvFixed)
{
	int nm = this->getNumModel();

	for (int i = 0; i < nm; ++i)
	{
		if ((int) this->m_vfModel[i].size() != this->m_vpModels[i]->getNumSimDOF_x())
			this->m_vfModel[i].resize(this->m_vpModels[i]->getNumSimDOF_x()); // Resize

		this->m_vfModel[i].setZero();
	}

#ifdef OMP_ENFORJAC
	#pragma omp parallel for
#endif
	for (int i = 0; i < nm; ++i)
	{
		this->m_vpModels[i]->addForce(this->m_vfModel[i]);
	}

	this->m_assembleForTimer.restart();

	for (int i = 0; i < nm; ++i)
	{
		this->addLocal2Global(i, this->m_vfModel[i], vf);
	}

	this->m_assembleForTimer.stopStoreLog();
}

void CompositeModel::addJacobian(tVector& vJ, const vector<bool>* pvFixed)
{
	int nm = this->getNumModel();

	for (int i = 0; i < nm; ++i)
	{
		int numNonZeros = this->m_vpModels[i]->getNumNonZeros_Jacobian();

		if ((int) this->m_vJModel[i].capacity() != this->m_vpModels[i]->getNumNonZeros_Jacobian())
			this->m_vJModel[i].reserve(this->m_vpModels[i]->getNumNonZeros_Jacobian()); // Reserve

		this->m_vJModel[i].clear();
	}

#ifdef OMP_ENFORJAC
	#pragma omp parallel for
#endif
	for (int i = 0; i < nm; ++i)
	{
		this->m_vpModels[i]->addJacobian(this->m_vJModel[i]);
	}

	this->m_assembleJacTimer.restart();

	for (int i = 0; i < nm; ++i)
	{
		this->addLocal2Global_Tri(i, this->m_vJModel[i], vJ);
	}

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, this->m_nsimDOF_x);
	compressionMatrix.setFromTriplets(vJ.begin(), vJ.end());
	toTriplets(compressionMatrix, vJ);

	// Compress vector ------------------
#endif

	this->m_assembleJacTimer.stopStoreLog();
}

void CompositeModel::add_DfxDx(tVector& vDfxDx, const vector<bool>* pvFixed)
{
	tVector vJ; 
	vJ.reserve(this->getNumNonZeros_Jacobian());
	this->addJacobian(vJ, pvFixed); // Triangular

	// Convert to non-triangular
	int ncoefs = (int)vJ.size();
	for (int i = 0; i < ncoefs; ++i)
	{
		const Triplet<Real> t = vJ[i];
		Real val = t.value();
		int col = t.col();
		int row = t.row();

		if (col == row)
		{
			vDfxDx.push_back(Triplet<Real>(row, col, val));
		}
		else
		{
			vDfxDx.push_back(Triplet<Real>(row, col, val));
			vDfxDx.push_back(Triplet<Real>(col, row, val));
		}
	}
}

void CompositeModel::add_DfxD0(tVector& vDfxDx, const vector<bool>* pvFixed)
{
	//throw new exception("[ERROR] Not implemented yet");
}

void CompositeModel::mapSim2Raw(const VectorXd& vin, VectorXd& vout) const
{
	vout.resize(this->m_nrawDOF);
	vout.setZero(); // Initialize
	for (int i = 0; i < this->getNumModel(); ++i)
	{
		int nsimDOFModel = this->m_vpModels[i]->getNumSimDOF_x();
		int nrawDOFModel = this->m_vpModels[i]->getNumRawDOF();

		VectorXd vinsim(nsimDOFModel);
		VectorXd vinraw(nrawDOFModel);
		vinsim.setZero();
		vinraw.setZero();

		this->mapGlobal2LocalSim_x(i, vin, vinsim);
		this->m_vpModels[i]->mapSim2Raw(vinsim, vinraw);
		this->mapLocal2GlobalRaw(i, vinraw, vout); // Add
	}
}

//void CompositeModel::mapRaw2Sim(const VectorXd& vin, VectorXd& vout) const
//{
//	vout.resize(this->m_nsimDOF_x);
//	vout.setZero(); // Initialize
//	for (int i = 0; i < this->getNumModel(); ++i)
//	{
//		int nsimDOFModel = this->m_vpModels[i]->getSimDOFNumber_x();
//		int nrawDOFModel = this->m_vpModels[i]->getRawDOFNumber();
//
//		VectorXd vinsim(nsimDOFModel);
//		VectorXd vinraw(nrawDOFModel);
//		vinsim.setZero();
//		vinraw.setZero();
//
//		this->mapGlobal2LocalRaw(i, vin, vinraw);
//		this->m_vpModels[i]->mapRaw2Sim(vinraw, vinsim);
//		this->mapLocal2GlobalSim(i, vinsim, vout); // Add
//	}
//}

void CompositeModel::mapLocal2GlobalSim_x(int i, const VectorXd& vin, VectorXd& vout) const
{
	assert(i >= 0 && i < this->getNumModel());

	int nsimDOFMod= this->m_vpModels[i]->getNumSimDOF_x();

	assert((int) vin.size() == nsimDOFMod);
	assert((int) vout.size() == m_nsimDOF_x);

	// Consider only DOF with just one link
	// the rest of them are dependent on some
	// other DOF of the system.
	
	for (int j = 0; j < nsimDOFMod; ++j)
	{
		if (this->m_vDOFLinks[i][j].m_numLink == 1)
		{
			vout(this->m_vDOFLinks[i][j].m_vidx[0]) = vin(j);
		}
	}
}

void CompositeModel::mapLocal2GlobalSim_0(int i, const VectorXd& vin, VectorXd& vout) const
{
	// TODO
}

void CompositeModel::mapGlobal2LocalSim_x(int i, const VectorXd& vin, VectorXd& vout) const
{
	assert(i >= 0 && i < this->getNumModel());
	assert((int) vin.size() == m_nsimDOF_x);

	int nsimDOFMod = this->m_vpModels[i]->getNumSimDOF_x();

	vout.resize(nsimDOFMod);
	vout.setZero();

	// Consider local DOF to be a linear
	// interpolation of all linked DOFs

	for (int j = 0; j < nsimDOFMod; ++j)
	{
		if (this->m_vDOFLinks[i][j].m_numLink == 1)
		{
			vout(j) += vin(this->m_vDOFLinks[i][j].m_vidx[0]);
		}
		else
		{
			for (int k = 0; k < this->m_vDOFLinks[i][j].m_numLink; ++k)
				vout(j) += vin(this->m_vDOFLinks[i][j].m_vidx[k])*this->m_vDOFLinks[i][j].m_vwei[k];
		}
	}
		
}

void CompositeModel::mapGlobal2LocalSim_0(int i, const VectorXd& vin, VectorXd& vout) const
{
	// TODO
}

void CompositeModel::mapLocal2GlobalRaw(int i, const VectorXd& vin, VectorXd& vout) const
{
	int nrawDOFMod = this->m_vpModels[i]->getNumRawDOF();

	assert(i >= 0 && i < this->getNumModel());
	assert((int)vin.size() == nrawDOFMod);

	vout.resize(m_nrawDOF);

	int rawOffset = this->m_vmodelRawIdx[i];

#ifdef OMP_ASSEMBLE
#pragma omp parallel for
#endif
	for (int j = 0; j < nrawDOFMod; ++j)
		vout(rawOffset + j) = vin(j);
}

void CompositeModel::mapGlobal2LocalRaw(int i, const VectorXd& vin, VectorXd& vout) const
{
	assert(i >= 0 && i < this->getNumModel());
	assert((int)vin.size() == m_nrawDOF);

	int nrawDOFMod = this->m_vpModels[i]->getNumRawDOF();

	vout.resize(nrawDOFMod);

	int rawOffset = this->m_vmodelRawIdx[i];

#ifdef OMP_ASSEMBLE
#pragma omp parallel for
#endif
	for (int j = 0; j < nrawDOFMod; ++j)
		vout(j) = vin(rawOffset + j);
}

void CompositeModel::mapLocal2GlobalRaw(int i, const iVector& vin, iVector& vout) const
{
	assert(i >= 0 && i < this->getNumModel());

	int numIdx = (int) vin.size();

	vout.resize(numIdx);

	int rawOffset = this->m_vmodelRawIdx[i];

#ifdef OMP_ASSEMBLE
#pragma omp parallel for
#endif
	for (int j = 0; j < numIdx; ++j)
		vout[j] = vin[j] + rawOffset;
}

void CompositeModel::mapGlobal2LocalRaw(int i, const iVector& vin, iVector& vout) const
{
	assert(i >= 0 && i < this->getNumModel());

	int numIdx = (int)vin.size();

	vout.resize(numIdx);

	int rawOffset = this->m_vmodelRawIdx[i];

#ifdef OMP_ASSEMBLE
#pragma omp parallel for
#endif
	for (int j = 0; j < numIdx; ++j)
		vout[j] = vin[j] - rawOffset;
}

void CompositeModel::addLocal2Global(int i, const VectorXd& vin, VectorXd& vout) const
{
	assert(i >= 0 && i < this->getNumModel());

	int nsimDOFMod = this->m_vpModels[i]->getNumSimDOF_x();

	assert((int)vin.size() == nsimDOFMod);
	assert((int)vout.size() == m_nsimDOF_x);

	for (int j = 0; j < nsimDOFMod; ++j)
	{
		if (this->m_vDOFLinks[i][j].m_numLink == 1)
		{
			vout(this->m_vDOFLinks[i][j].m_vidx[0]) += vin(j);
		}
		else
		{
			for (int k = 0; k < this->m_vDOFLinks[i][j].m_numLink; ++k)
				vout(this->m_vDOFLinks[i][j].m_vidx[k]) += vin(j) * this->m_vDOFLinks[i][j].m_vwei[k];
		}
	}
}

void CompositeModel::addGlobal2Local(int i, const VectorXd& vin, VectorXd& vout) const
{
	assert(i >= 0 && i < this->getNumModel());

	int nsimDOFMod = this->m_vpModels[i]->getNumSimDOF_x();

	assert((int)vin.size() == m_nsimDOF_x);
	assert((int)vout.size() == nsimDOFMod);

	for (int j = 0; j < nsimDOFMod; ++j)
	{
		if (this->m_vDOFLinks[i][j].m_numLink == 1)
		{
			vout(j) += vin(this->m_vDOFLinks[i][j].m_vidx[0]);
		}
		else
		{
			for (int k = 0; k < this->m_vDOFLinks[i][j].m_numLink; ++k)
				vout(j) += vin(this->m_vDOFLinks[i][j].m_vidx[k]) * this->m_vDOFLinks[i][j].m_vwei[k];
		}
	}
}

void CompositeModel::addLocal2Global(int i, const tVector& vin, tVector& vout) const
{
	assert(i >= 0 && i < this->getNumModel());

	int nCoeff = (int)vin.size();

//#ifdef OMP_ASSEMBLE
//#pragma omp parallel for
//#endif
	for (int iCoeff = 0; iCoeff < nCoeff; iCoeff++)
	{
		Real val = vin[iCoeff].value();
		int row = vin[iCoeff].row();
		int col = vin[iCoeff].col();

		const DOFLinks& rowLinks = this->m_vDOFLinks[i][row];
		const DOFLinks& colLinks = this->m_vDOFLinks[i][col];

		if (rowLinks.m_numLink == 1 && colLinks.m_numLink == 1)
		{
			vout.push_back(Triplet<Real>(rowLinks.m_vidx[0], colLinks.m_vidx[0], val));
		}
		else
		{
			for (int rowl = 0; rowl < rowLinks.m_numLink; ++rowl)
				for (int coll = 0; coll < colLinks.m_numLink; ++coll)
					vout.push_back(Triplet<Real>(rowLinks.m_vidx[rowl], colLinks.m_vidx[coll], rowLinks.m_vwei[rowl] * val * colLinks.m_vwei[coll]));
		}
	}
}


void CompositeModel::addLocal2Global_Tri(int i, const tVector& vin, tVector& vout) const
{
	int nCoeff = (int)vin.size();

//#ifdef OMP_ASSEMBLE
//#pragma omp parallel for
//#endif
	for (int iCoeff = 0; iCoeff < nCoeff; iCoeff++)
	{
		Real val = vin[iCoeff].value();

		int row = vin[iCoeff].row();
		int col = vin[iCoeff].col();
		assert(row >= col);

		const DOFLinks& rowLinks = this->m_vDOFLinks[i][row];
		const DOFLinks& colLinks = this->m_vDOFLinks[i][col];

		// Link Jacobian

		if (rowLinks.m_numLink == 1 || colLinks.m_numLink == 1)
		{
			if (rowLinks.m_vidx[0] >= colLinks.m_vidx[0])
				vout.push_back(Triplet<Real>(rowLinks.m_vidx[0], colLinks.m_vidx[0], val));
			else vout.push_back(Triplet<Real>(colLinks.m_vidx[0], rowLinks.m_vidx[0], val));

			continue;
		}

		if (colLinks.m_numLink > 1 && rowLinks.m_numLink > 1)
		{
			if (col == row)
			{
				for (int rowl = 0; rowl < rowLinks.m_numLink; ++rowl)
					for (int coll = 0; coll <= rowl; ++coll)
						if (rowLinks.m_vidx[rowl] >= colLinks.m_vidx[coll])
							vout.push_back(Triplet<Real>(rowLinks.m_vidx[rowl], colLinks.m_vidx[coll], rowLinks.m_vwei[rowl] * val * colLinks.m_vwei[coll]));
						else vout.push_back(Triplet<Real>(colLinks.m_vidx[coll], rowLinks.m_vidx[rowl], rowLinks.m_vwei[rowl] * val * colLinks.m_vwei[coll]));
			}
			else
			{
				for (int rowl = 0; rowl < rowLinks.m_numLink; ++rowl)
					for (int coll = 0; coll < colLinks.m_numLink; ++coll)
						if (rowLinks.m_vidx[rowl] > colLinks.m_vidx[coll])
							vout.push_back(Triplet<Real>(rowLinks.m_vidx[rowl], colLinks.m_vidx[coll], rowLinks.m_vwei[rowl] * val * colLinks.m_vwei[coll]));
						else if (rowLinks.m_vidx[rowl] < colLinks.m_vidx[coll])
							vout.push_back(Triplet<Real>(colLinks.m_vidx[coll], rowLinks.m_vidx[rowl], rowLinks.m_vwei[rowl] * val * colLinks.m_vwei[coll]));
						else
						{
							vout.push_back(Triplet<Real>(rowLinks.m_vidx[rowl], colLinks.m_vidx[coll], rowLinks.m_vwei[rowl] * val * colLinks.m_vwei[coll]));
							vout.push_back(Triplet<Real>(colLinks.m_vidx[coll], rowLinks.m_vidx[rowl], rowLinks.m_vwei[rowl] * val * colLinks.m_vwei[coll]));
						}
			}

			continue;
		}
	}
}

void CompositeModel::addLocal2Global_Row(int i, const tVector& vin, tVector& vout) const
{
	assert(i >= 0 && i < this->getNumModel());

	// Map to global considering the derived variables
	// are not necessary part of the model, but they can
	// be whatever. That would transform an [Nl x M] matrix
	// into an [Ng x M] matrix, where Nl is the number of
	// simulation DOF of the local model and Ng is the 
	// total number of DOF of the composite model.

	int nCoeff = (int)vin.size();

//#ifdef OMP_ASSEMBLE
//#pragma omp parallel for
//#endif
	for (int iCoeff = 0; iCoeff < nCoeff; iCoeff++)
	{
		Real v = vin[iCoeff].value();
		int row = vin[iCoeff].row();
		int col = vin[iCoeff].col();

		const DOFLinks& rowLinks = this->m_vDOFLinks[i][row];

		if (rowLinks.m_numLink == 1)
		{
			vout.push_back(Triplet<Real>(rowLinks.m_vidx[0], col, v));
		}
		else
		{
			for (int rowl = 0; rowl < rowLinks.m_numLink; ++rowl) // Do map matrix rows but not columns
				vout.push_back(Triplet<Real>(rowLinks.m_vidx[rowl], col, rowLinks.m_vwei[rowl] * v));
		}
	}
}

void CompositeModel::add_fgravity(VectorXd& vf)
{
	assert(vf.size() == this->m_nsimDOF_x);

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		int nsimDOFModel = this->m_vpModels[i]->getNumSimDOF_x();
		VectorXd vfModel(nsimDOFModel);
		vfModel.setZero();

		this->m_vpModels[i]->add_fgravity(vfModel);

		for (int j = 0; j < nsimDOFModel; ++j)
		{
			const DOFLinks& links = this->m_vDOFLinks[i][j];

			if (links.m_numLink == 1)
			{
				vf(links.m_vidx[0]) += vfModel(j);
			}
			else
			{
				for (int l = 0; l < links.m_numLink; ++l) // Map to global
					vf(links.m_vidx[l]) += links.m_vwei[l] * vfModel(j);
			}
		}
	}
}

void CompositeModel::add_vgravity(VectorXd& vg)
{
	assert(vg.size() == this->m_nsimDOF_x);

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		int nsimDOFModel = this->m_vpModels[i]->getNumSimDOF_x();
		VectorXd vgModel(nsimDOFModel);
		vgModel.setZero();

		this->m_vpModels[i]->add_vgravity(vgModel);

		for (int j = 0; j < nsimDOFModel; ++j)
		{
			const DOFLinks& links = this->m_vDOFLinks[i][j];

			if (links.m_numLink == 1)
			{
				vg(links.m_vidx[0]) += vgModel(j);
			}
			else
			{
				for (int l = 0; l < links.m_numLink; ++l) // Map to global
					vg(links.m_vidx[l]) += links.m_vwei[l] * vgModel(j);
			}
		}
	}
}

void CompositeModel::freeModelsAndCouplings()
{
	// Do not delete models, those where created
	// outside the scope of the class so let live.

	this->unhookSubmodelDiscretizationChanged();

	for (int i = 0; i < this->getNumCoupling(); ++i)
		delete this->m_vpCouplings[i]; // Free
	this->m_vpCouplings.clear();

	this->m_vpModels.clear();
}

iVector CompositeModel::getAABBIndices(const dVector& vaabb) const
{
	iVector vselection;

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		iVector vselectionModel = this->m_vpModels[i]->getAABBIndices(vaabb);

		int numSelectedModel = (int)vselectionModel.size();
		for (int j = 0; j < numSelectedModel; ++j) // Offset considering model
			vselection.push_back(this->m_vmodelRawIdx[i] + vselectionModel[j]);
	}

	return vselection;
}

// DEPRECATED --------------------------------------------------------------------------------

void CompositeModel::addGravity(const VectorXd& vg, VectorXd& vf, const vector<bool>* pvFixed)
{
	assert(vf.size() == this->m_nsimDOF_x);
	if (pvFixed != NULL) // Check also fixed stencil
		assert((int)pvFixed->size() == this->m_nsimDOF_x);

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		int nsimDOFModel = this->m_vpModels[i]->getNumSimDOF_x();
		VectorXd vfModel(nsimDOFModel);
		vfModel.setZero();

		if (pvFixed == NULL) // No fixed vector defined
			this->m_vpModels[i]->addGravity(vg, vfModel, NULL);
		else
		{
			vector<bool> vfixedModel;
			this->projectFixedToModel(i, *pvFixed, vfixedModel);
			this->m_vpModels[i]->addGravity(vg, vfModel, &vfixedModel);
		}

		for (int j = 0; j < nsimDOFModel; ++j)
		{
			const DOFLinks& links = this->m_vDOFLinks[i][j];
			for (int l = 0; l < links.m_numLink; ++l)  // Map to global
				vf(links.m_vidx[l]) += links.m_vwei[l] * vfModel(j);
		}
	}
}

void CompositeModel::projectFixedToModel(int i, const bVector& vglo, bVector& vloc)
{
	int nsimDOFModel = this->m_vpModels[i]->getNumSimDOF_x();
	vector<bool> vfixedModel(nsimDOFModel, false);
	for (int j = 0; j < nsimDOFModel; ++j)
	{
		bool allFixed = true;
		const DOFLinks& links = this->m_vDOFLinks[i][j];
		for (int l = 0; l < links.m_numLink; ++l)
			if (!vglo[links.m_vidx[l]])
			{
			allFixed = false;
			break; // Free DOF
			}

		vloc[j] = allFixed;
	}
}