/*=====================================================================================*/
/*!
/file		ModelCoupling.h
/author		Jes�s P�rez (jesus.prod@gmail.com)
*/
/*=====================================================================================*/

#ifndef MODEL_COUPLING_H
#define MODEL_COUPLING_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

typedef struct ModelCoupling
{
public:

	enum Type
	{
		Constraint,			// Any points explicit
		Embedded,			// Only embedded explicit
		Shared				// Both points explicit
	};

	ModelCoupling()
	{
		this->m_type = Shared;
		this->m_modelIdx0 = -1;
		this->m_modelIdx1 = -1;
		this->m_softK = -1.0;
	}

	~ModelCoupling()
	{
		// Nothing to do here...
	}

	ModelCoupling(const ModelCoupling& toCopy)
	{
		this->m_type = toCopy.m_type;
		this->m_modelIdx0 = toCopy.m_modelIdx0;
		this->m_modelIdx1 = toCopy.m_modelIdx1;
		this->m_point0 = toCopy.m_point0;
		this->m_point1 = toCopy.m_point1;
		this->m_softK = toCopy.m_softK;
	}

	Type getType() const { return this->m_type; }
	void setType(Type type) { this->m_type = type; }

	int getModel0() const { return this->m_modelIdx0; }
	int getModel1() const { return this->m_modelIdx1; }
	void setModel0(int modelIdx) { this->m_modelIdx0 = modelIdx; }
	void setModel1(int modelIdx) { this->m_modelIdx1 = modelIdx; }

	ModelPoint& getModelPoint0() { return this->m_point0; }
	ModelPoint& getModelPoint1() { return this->m_point1; }
	const ModelPoint& getModelPoint0() const { return this->m_point0; }
	const ModelPoint& getModelPoint1() const { return this->m_point1; }
	void setModelPoint0(const ModelPoint& point) { this->m_point0 = point; }
	void setModelPoint1(const ModelPoint& point) { this->m_point1 = point; }

	Real getSoftK() const { return this->m_softK; }
	void setSoftK(Real sK) { this->m_softK = sK; }

	bool operator == (const ModelCoupling& other)
	{
		bool boolAnd = m_type == other.m_type;

		int numExp0 = 0;
		int numExp1 = 0;
		if (this->m_point0.isExplicit()) numExp0++;
		if (this->m_point1.isExplicit()) numExp0++;
		if (other.m_point0.isExplicit()) numExp1++;
		if (other.m_point1.isExplicit()) numExp1++;

		if (numExp0 == 2 && numExp1 == 2)
		{
			boolAnd &= (m_modelIdx0 == other.m_modelIdx0 && m_point0 == other.m_point0) &&
					   (m_modelIdx0 == other.m_modelIdx1 && m_point0 == other.m_point1);
		}
		else
		{
			boolAnd &= (m_modelIdx0 == other.m_modelIdx0 && m_point0 == other.m_point0) ||
					   (m_modelIdx0 == other.m_modelIdx1 && m_point0 == other.m_point1);
		}

		return boolAnd;
	}

	bool operator != (const ModelCoupling& other)
	{
		return !(*this != other);
	}

protected:
	Type m_type;
	int m_modelIdx0;
	int m_modelIdx1;
	ModelPoint m_point0;
	ModelPoint m_point1;
	double m_softK;

} ModelCoupling;

typedef struct DOFLinks
{
public:
	DOFLinks()
	{
		this->m_numLink = 0;
		this->m_vidx.clear();
		this->m_vwei.clear();
		this->m_isInit = false;
	}

	~DOFLinks()
	{
		// Nothing to do here
	}

	DOFLinks(const DOFLinks& toCopy)
	{
		this->m_vidx = toCopy.m_vidx;
		this->m_vwei = toCopy.m_vwei;
		this->m_numLink = toCopy.m_numLink;
		this->m_isInit = toCopy.m_isInit;
	}

	void addLink(int idx, double wei)
	{
		this->m_numLink++;
		this->m_vidx.push_back(idx);
		this->m_vwei.push_back(wei);
		this->m_isInit = true;
	}

public:
	int m_numLink;
	iVector m_vidx;
	dVector m_vwei;
	bool m_isInit;

} DOFLinks;

#endif