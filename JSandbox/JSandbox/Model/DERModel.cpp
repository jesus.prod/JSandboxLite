/*=====================================================================================*/
/*!
/file		DERModel.cpp
/author		jesusprod
/brief		Implementation of DERModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/DERModel.h>

#ifdef _OPENMP 
#include <omp.h>
#endif

DERModel::DERModel() : SolidModel("DER")
{
	this->m_isDummy = false;
	this->m_isStraight = false;

	this->m_stateDER_x = pDERState(new DERState());
	this->m_stateDER_0 = pDERState(new DERState());
	this->m_state_x = this->m_stateDER_x;
	this->m_state_0 = this->m_stateDER_0;

	this->deprecateConfiguration();
}

DERModel::~DERModel()
{
	logSimu("[INFO] Destroying DERModel: %u", this);
}

void DERModel::freeElements()
{
	SolidModel::freeElements();

	this->m_vpBTEles.clear();
	this->m_vpBendingEles.clear();
	this->m_vpStretchEles.clear();
	this->m_vpTwistEles.clear();
}

void DERModel::configureRod(const Rod& rod_x, const Rod& rod_0)
{
	this->m_stateDER_x->m_rod = rod_x;
	this->m_stateDER_0->m_rod = rod_0;

	this->deprecateConfiguration();
}

void DERModel::setup()
{
	const Curve& curvex = this->m_stateDER_x->m_rod.getCurve();
	int numv = curvex.getNumNode();
	int nume = curvex.getNumEdge();

	// Initialize DOF

	this->m_numDOFa = 1*nume;
	this->m_numDOFv = 3*numv;

	this->m_nrawDOF = this->m_nsimDOF_x = this->m_nsimDOF_0 = 3 * numv + nume;
	
	this->m_stateDER_x->m_va.resize(nume, 0.0);
	this->m_stateDER_x->m_vt.resize(numv, 0.0);
	this->m_stateDER_0->m_va.resize(nume, 0.0);
	this->m_stateDER_0->m_vt.resize(numv, 0.0);
	this->m_stateDER_x->m_vF = this->m_stateDER_x->m_rod.getFrames();
	this->m_stateDER_0->m_vF = this->m_stateDER_0->m_rod.getFrames();

	this->m_state_x->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_x->m_vv.setZero();
	this->m_state_0->m_vv.setZero();
	this->m_state_x->m_vx.setZero();
	this->m_state_0->m_vx.setZero();

	const dVector& vxx = this->m_stateDER_x->m_rod.getPositions();
	for (int i = 0; i < this->m_numDOFv; ++i)
		this->m_state_x->m_vx(i) = vxx[i];

	const dVector& vx0 = this->m_stateDER_0->m_rod.getPositions();
	for (int i = 0; i < this->m_numDOFv; ++i)
		this->m_state_0->m_vx(i) = vx0[i];

	// Initialize reference twists

	this->updateMetadata_x();
	this->updateMetadata_0();

	// Initialize elements

	this->freeElements();

	// Initialize Materials

	this->initializeMaterials();

	// Initialize stretch elements

	for (int i = 0; i < nume; ++i)
	{
		int pv, nv;
		curvex.getEdgeNeighborNodes(i, pv, nv);
		iVector veind(1);
		iVector vvind(2);
		iVector vDOFind(7);
		veind[0] = i;
		vvind[0] = pv;
		vvind[1] = nv;
		for (int j = 0; j < 3; ++j)
		{
			vDOFind[0 + j] = 3 * pv + j;
			vDOFind[3 + j] = 3 * nv + j;
		}
		vDOFind[6] = this->m_numDOFv + i;

		vector<SolidMaterial*> vpMats(1);
		vpMats[0] = &this->m_vmats[i];
		
		DERStretchElement *pEle = new DERStretchElement();
		pEle->setIndicesx(vDOFind);
		pEle->setEdgeIndices(veind);
		pEle->setVertexIndices(vvind);
		pEle->setMaterials(vpMats);
		
		this->m_vpEles.push_back(pEle);

		this->m_vpStretchEles.push_back(pEle);
	}

	// Initialize bending & twist

	for (int i = 0; i < numv; ++i)
	{
		int pv, nv, pe, ne;
		curvex.getNodeNeighborEdges(i, pe, ne);
		curvex.getNodeNeighborNodes(i, pv, nv);

		if (pv == -1)
			continue;
		if (nv == -1)
			continue;

		iVector veind(2);
		iVector vvind(3);
		iVector vDOFind(11);
		veind[0] = pe;
		veind[1] = ne;
		vvind[0] = pv;
		vvind[1] = i;
		vvind[2] = nv;
		for (int j = 0; j < 3; ++j)
		{
			vDOFind[0 + j] = 3 * pv + j;
			vDOFind[3 + j] = 3 * i + j;
			vDOFind[6 + j] = 3 * nv + j;
		}
		vDOFind[9] = 3*numv + pe;
		vDOFind[10] = 3*numv + ne;

		vector<SolidMaterial*> vpMats(2);
		vpMats[0] = &this->m_vmats[pe];
		vpMats[1] = &this->m_vmats[ne];

		DERBendTwistElement *pEle = new DERBendTwistElement();
		pEle->setIndicesx(vDOFind);
		pEle->setEdgeIndices(veind);
		pEle->setVertexIndices(vvind);
		pEle->setMaterials(vpMats);

		this->m_vpEles.push_back(pEle);
		this->m_vpBTEles.push_back(pEle);
	}

	this->m_isReady_Setup = true;
	this->deprecateDiscretization();

	// Update simulation 
	this->update_SimDOF();

	this->update_Rest();

	logSimu("[INFO] Initialized DERModel. Raw: %u, Sim: %u", this->m_nrawDOF, this->m_nsimDOF_x);
}

void DERModel::setStateToMesh_x(const Rod& rod)
{
	int ne = rod.getNumEdge();
	int nv = rod.getNumNode();

	this->m_stateDER_x->m_rod = rod;
	this->m_stateDER_x->m_va.resize(ne);
	this->m_stateDER_x->m_vt.resize(nv);
	for (int i = 0; i < ne; ++i)
	{
		this->m_stateDER_x->m_va[i] = 0.0;
	}
	for (int i = 0; i < nv; ++i)
	{
		this->m_stateDER_x->m_vt[i] = 0.0;
	}
	this->m_stateDER_x->m_vF = rod.getFrames();

	const dVector& vx = rod.getCurve().getPositions();

	this->m_stateDER_x->m_vx.setZero();
	this->m_stateDER_x->m_vx.setZero();

	for (int i = 0; i < this->m_numDOFv; ++i)
		this->m_stateDER_x->m_vx[i] = vx[i];

	this->deprecateDeformation();
}

void DERModel::setStateToMesh_0(const Rod& rod)
{
	int ne = rod.getNumEdge();
	int nv = rod.getNumNode();

	this->m_stateDER_0->m_rod = rod;
	this->m_stateDER_0->m_va.resize(ne);
	this->m_stateDER_0->m_vt.resize(nv);
	for (int i = 0; i < ne; ++i)
	{
		this->m_stateDER_0->m_va[i] = 0.0;
	}
	for (int i = 0; i < nv; ++i)
	{
		this->m_stateDER_0->m_vt[i] = 0.0;
	}
	this->m_stateDER_0->m_vF = rod.getFrames();

	const dVector& vx = rod.getCurve().getPositions();

	this->m_stateDER_0->m_vx.setZero();
	this->m_stateDER_0->m_vv.setZero();

	for (int i = 0; i < this->m_numDOFv; ++i)
		this->m_stateDER_0->m_vx[i] = vx[i];

	this->deprecateDeformation();
	this->deprecateUndeformed();
}

void DERModel::setState_x(pSolidState s)
{
	*this->m_stateDER_x = DERState(*static_cast<DERState*>(s.get()));

	this->deprecateDeformation();
}

void DERModel::setState_0(pSolidState s)
{
	*this->m_stateDER_0 = DERState(*static_cast<DERState*>(s.get()));

	this->deprecateUndeformed();
}

void DERModel::setPositions_x(const VectorXd& vx)
{
	SolidModel::setPositions_x(vx);
	this->updateRodCenter(this->m_stateDER_x->m_vx, this->m_stateDER_x->m_rod);
	this->updateRodAngles(this->m_stateDER_x->m_vx, this->m_stateDER_x->m_va);
	this->updateRodRefFrame(this->m_stateDER_x->m_rod, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_vt);
	this->updateRodMatFrame(this->m_stateDER_x->m_va, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod);
	this->deprecateDeformation();
}

void DERModel::setPositions_0(const VectorXd& vX)
{
	SolidModel::setPositions_0(vX);
	this->updateRodCenter(this->m_stateDER_0->m_vx, this->m_stateDER_0->m_rod);
	this->updateRodAngles(this->m_stateDER_0->m_vx, this->m_stateDER_0->m_va);
	this->updateRodRefFrame(this->m_stateDER_0->m_rod, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_vt);
	this->updateRodMatFrame(this->m_stateDER_0->m_va, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_rod);
	this->deprecateUndeformed();
}

void DERModel::update_Rest()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Rest)
		return; // Already done

	const dVector& vx = this->m_stateDER_0->m_rod.getPositions();
	const vector<Frame>& vmat = this->m_stateDER_0->m_rod.getFrames();

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_Rest(vx, this->m_stateDER_0->m_va, this->m_stateDER_0->m_vt, this->m_stateDER_0->m_vF, vmat);

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_Rest(vx, this->m_stateDER_0->m_va, this->m_stateDER_0->m_vt, this->m_stateDER_0->m_vF, vmat);

	if (this->m_isStraight)
	{
#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
		// Enforce rest kappa and twist to be null
		for (int i = 0; i < this->getNumBTEle(); ++i)
		{
			this->m_vpBTEles[i]->setRestKappa(Vector2d(0, 0));
			this->m_vpBTEles[i]->setRestVertexTwist(0.0);
		}
	}

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void DERModel::update_Mass()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mass)
		return; // Already done

	const dVector& vx = this->m_stateDER_0->m_rod.getPositions();
	const vector<Frame>& vmat = this->m_stateDER_0->m_rod.getFrames();

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_Mass(vx, this->m_stateDER_0->m_va, this->m_stateDER_0->m_vt, this->m_stateDER_0->m_vF, vmat);

	this->m_vMass.resize(this->m_nsimDOF_x);
	this->m_vMass.setZero(); // Initialize

	// Cache lumped mass vector and matrix

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->addMass(this->m_vMass);

	this->updateSparseMatrixToLumpedMass();

	this->m_isReady_Mass = true;
	this->deprecateDeformation();
}


void DERModel::update_Energy()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Energy)
		return; // Already done

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const vector<Frame>& vmat = this->m_stateDER_x->m_rod.getFrames();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_Energy(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_Energy(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

	this->m_isReady_Energy = true;
}

void DERModel::update_Force()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();
	
	if (this->m_isReady_Force)
		return; // Already done

	this->m_updateForTimer.restart();

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const vector<Frame>& vmat = this->m_stateDER_x->m_rod.getFrames();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_Force(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_Force(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

	this->m_isReady_Force = true;

	this->m_updateForTimer.stopStoreLog();
}

void DERModel::update_Jacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian)
		return; // Already done

	this->m_updateJacTimer.restart();

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const vector<Frame>& vmat = this->m_stateDER_x->m_rod.getFrames();


#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_Jacobian(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_Jacobian(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

	this->m_isReady_Jacobian = true;

	this->m_updateJacTimer.stopStoreLog();
}

void DERModel::update_EnergyForce()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Energy && this->m_isReady_Force)
		return; // Both of them already computed, out

	this->m_updateForTimer.restart();

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const vector<Frame>& vmat = this->m_stateDER_x->m_rod.getFrames();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_EnergyForce(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_EnergyForce(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

	this->m_isReady_Energy = true;
	this->m_isReady_Force = true;

	this->m_updateForTimer.stopStoreLog();
}

void DERModel::update_ForceJacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Force && this->m_isReady_Jacobian)
		return; // Both of them already computed, out

	this->m_updateForTimer.restart();
	this->m_updateJacTimer.restart();

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const vector<Frame>& vmat = this->m_stateDER_x->m_rod.getFrames();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_ForceJacobian(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_ForceJacobian(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

	this->m_isReady_Force = true;
	this->m_isReady_Jacobian = true;

	this->m_updateForTimer.stopStoreLog();
	this->m_updateJacTimer.stopStoreLog();
}

void DERModel::update_EnergyForceJacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian &&
		this->m_isReady_Force &&
		this->m_isReady_Energy)
		return; // All done

	this->m_updateForTimer.restart();
	this->m_updateJacTimer.restart();

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const vector<Frame>& vmat = this->m_stateDER_x->m_rod.getFrames();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_EnergyForceJacobian(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_EnergyForceJacobian(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, vmat);

	this->m_isReady_Energy = true;
	this->m_isReady_Force = true;
	this->m_isReady_Jacobian = true;

	this->m_updateForTimer.stopStoreLog();
	this->m_updateJacTimer.stopStoreLog();
}

void DERModel::testForceLocal()
{
	// Element tests not available...
}

void DERModel::testJacobianLocal()
{
	// Element tests not available...
}

void DERModel::updateMetadata_0()
{
	this->updateRodCenter(this->m_state_0->m_vx, this->m_stateDER_0->m_rod);
	this->updateRodAngles(this->m_state_0->m_vx, this->m_stateDER_0->m_va);
	this->updateRodRefFrame(this->m_stateDER_0->m_rod, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_vt);
	this->updateRodMatFrame(this->m_stateDER_0->m_va, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_rod);
}

void DERModel::updateMetadata_x()
{
	this->updateRodCenter(this->m_state_x->m_vx, this->m_stateDER_x->m_rod);
	this->updateRodAngles(this->m_state_x->m_vx, this->m_stateDER_x->m_va);
	this->updateRodRefFrame(this->m_stateDER_x->m_rod, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_vt);
	this->updateRodMatFrame(this->m_stateDER_x->m_va, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod);
}

void DERModel::updateRodCenter(const VectorXd& vxSim, Rod& rod) const
{
	dVector vx(this->m_numDOFv);
	for (int i = 0; i < this->m_numDOFv; ++i)
		vx[i] = vxSim(i); // First variables

	rod.setPositions(vx);
}

void DERModel::updateRodAngles(const VectorXd& vxSim, dVector& va) const
{
	va.resize(this->m_numDOFa);
	for (int i = 0; i < this->m_numDOFa; ++i)
		va[i] = vxSim(this->m_numDOFv + i); // Next
}

void DERModel::updateRodFrames(Rod& rod, dVector& va, vector<Frame3d>& vref, dVector& vt) const
{
	const Curve& curve = rod.getCurve();

	int ne = curve.getNumEdge();
	int nv = curve.getNumNode();

	assert((int)vref.size() == ne);
	assert((int)vt.size() == nv);

	for (int j = 0; j < ne; ++j)
	{
		const Frame3d& oldFrame = vref[j];

		const Vector3d& e0 = oldFrame.tan;
		Vector3d e1 = curve.getEdge(j).normalized();
		Frame3d oldFramePT= parallelTransport(oldFrame, e1);
		Frame3d oldFrameRot = frameRotation(oldFramePT, va[j]);

		va[j] = 0.0;
		vref[j] = oldFrameRot;
		rod.setFrame(j, oldFrameRot);
	}

	for (int j = 0; j < nv; j++)
	{
		if (j == 0 || j == nv - 1)
		{
			if (!curve.getIsClosed())
			{
				vt[j] = 0.0;
				continue;
			}
		}

		int pe, ne;
		curve.getNodeNeighborEdges(j, pe, ne);
		const Frame3d& F0 = vref[pe];
		const Frame3d& F1 = vref[ne];
		Frame3d F0t = parallelTransport(F0, F1.tan); // Untwist
		vt[j] = signedAngle(F0t.bin, F1.bin, F1.tan, vt[j]);
	}
}

void DERModel::updateRodRefFrame(const Rod& rod, vector<Frame3d>& vref, dVector& vt) const
{
	const Curve& curve = rod.getCurve();

	int ne = curve.getNumEdge();
	int nv = curve.getNumNode();

	assert((int) vref.size() == ne);
	assert((int) vt.size() == nv);

	for (int j = 0; j < ne; ++j)
	{
		const Frame3d& oldFrame = vref[j];

		Vector3d e1 = curve.getEdge(j).normalized();
		vref[j] = parallelTransport(oldFrame, e1); 
	}

	for (int j = 0; j < nv; j++)
	{
		if (j == 0 || j == nv - 1)
		{
			if (!curve.getIsClosed())
			{
				vt[j] = 0.0;
				continue;
			}
		}

		int pe, ne;
		curve.getNodeNeighborEdges(j, pe, ne);
		const Frame3d& F0 = vref[pe];
		const Frame3d& F1 = vref[ne];
		Frame3d F0t = parallelTransport(F0, F1.tan); // Untwist
		vt[j] = signedAngle(F0t.bin, F1.bin, F1.tan, vt[j]);
	}
}

void DERModel::updateRodMatFrame(const dVector& va, const vector<Frame3d>& vref, Rod& rod) const
{
	const Curve& curve = rod.getCurve();
	int ne = curve.getNumEdge();

	assert((int)va.size() == ne);
	assert((int)vref.size() == ne);

	for (int j = 0; j < ne; j++)  // Update frames
		rod.setFrame(j, frameRotation(vref[j], va[j]));
}

void DERModel::add_fgravity(VectorXd& vf)
{
	assert(this->m_vg.size() == 3);

	if (!this->m_isReady_Mass)
		this->update_Mass();

	// Lumped mass vector implementation

	int nv = this->m_stateDER_x->m_rod.getNumNode();
	for (int i = 0; i < nv; ++i)
	{
		int offset = 3 * i;
		for (int j = 0; j < 3; ++j)
			vf(offset + j) += this->m_vMass(offset + j)*this->m_vg(j);
	}
}

void DERModel::add_vgravity(VectorXd& vf)
{
	assert(this->m_vg.size() == 3);

	if (!this->m_isReady_Mass)
		this->update_Mass();

	// Lumped mass vector implementation

	int nv = this->m_stateDER_x->m_rod.getNumNode();
	for (int i = 0; i < nv; ++i)
	{
		int offset = 3 * i;
		for (int j = 0; j < 3; ++j)
			vf(offset + j) += this->m_vg(j);
	}
}

void DERModel::addGravity(const VectorXd& vg, VectorXd& vf, const vector<bool>* pvFixed)
{
	if (!this->m_isReady_Mass)
		this->update_Mass();

	assert(vg.size() == 3);

	// Lumped mass vector implementation

	int nv = this->m_stateDER_x->m_rod.getNumNode();
	for (int i = 0; i < nv; ++i)
	{
		int offset = 3 * i;
		for (int j = 0; j < 3; ++j)
			if (pvFixed == NULL || !pvFixed->at(offset + j))
				vf(offset + j) += this->m_vMass(offset + j)*vg(j);
	}
}

iVector DERModel::getAABBIndices(const dVector& vaabb) const
{
	iVector vselection;

	const Curve& rodCurve = this->m_stateDER_x->m_rod.getCurve();
	int numNodes = rodCurve.getNumNode();
	int numEdges = rodCurve.getNumEdge();
	bVector vnodeSelected(numNodes, false);
	
	// Check frames
	for (int i = 0; i < numNodes; ++i)
	{
		Vector3d x = rodCurve.getPosition(i);

		bool selected = true;
		for (int j = 0; j < 3; ++j)
		{
			double min = vaabb[2*j + 0];
			double max = vaabb[2*j + 1];
			if (x(j) < min || x(j) > max)
			{
				selected = false;
				break; // Found
			}
		}

		if (selected)
		{
			vnodeSelected[i] = true;

			int offset = 3*i;
			for (int j = 0; j < 3; ++j)
				vselection.push_back(offset + j);
		}
	}

	// Check frames
	for (int j = 0; j < numEdges; ++j)
	{
		int pn, nn;
		rodCurve.getEdgeNeighborNodes(j, pn, nn);
		if (vnodeSelected[pn] && vnodeSelected[nn])
			vselection.push_back(3*numNodes + j);
	}

	return vselection;
}

void DERModel::deprecateDiscretization()
{
	SolidModel::deprecateDiscretization();

	this->m_nNZ_DfxDre = -1;
	this->m_nNZ_DfxDrr = -1;
	this->m_nNZ_DfxDlr = -1;
	this->m_nNZ_DfxDle = -1;
	this->m_nNZ_DfxD0v = -1;
}

void DERModel::deprecateDeformation()
{
	SolidModel::deprecateDeformation();

	this->m_isReady_fre = false;
	this->m_isReady_frr = false;
	this->m_isReady_flr = false;
	this->m_isReady_fle = false;
	this->m_isReady_f0v = false;
	this->m_isReady_DfxDre = false;
	this->m_isReady_DfxDrr = false;
	this->m_isReady_DfxDlr = false;
	this->m_isReady_DfxDle = false;
	this->m_isReady_DfxD0v = false;
}

void DERModel::initializeMaterials()
{
	int ne = this->m_stateDER_x->m_rod.getNumEdge();
	this->m_vmats.resize(ne);
	for (int i = 0; i < ne; ++i)
	{
		if (this->m_isDummy)
		{
			this->m_vmats[i] = this->m_dummyMat;
		}
		else
		{
			this->m_vmats[i] = this->m_material;
		}

		Vector2d vr = this->m_stateDER_x->m_rod.getRadius(i);
		this->m_vmats[i].setWRadius(vr.x());
		this->m_vmats[i].setHRadius(vr.y());
	}
}

void DERModel::precompute_DfxD0v()
{
	tVector testJ;
	this->add_DfxD0v(testJ); // Precompute
	this->m_nNZ_DfxD0v = (int)testJ.size();
}

void DERModel::precompute_DfxDlr()
{
	tVector testJ;
	this->add_DfxDlr(testJ); // Precompute
	this->m_nNZ_DfxDlr = (int)testJ.size();
}

void DERModel::precompute_DfxDle()
{
	tVector testJ;
	this->add_DfxDle(testJ); // Precompute
	this->m_nNZ_DfxDle = (int)testJ.size();
}

void DERModel::precompute_DfxDrr()
{
	tVector testJ;
	this->add_DfxDrr(testJ); // Precompute
	this->m_nNZ_DfxDrr = (int)testJ.size();
}

void DERModel::precompute_DfxDre()
{
	tVector testJ;
	this->add_DfxDre(testJ); // Precompute
	this->m_nNZ_DfxDre = (int)testJ.size();
}

void DERModel::getParam_Split(iVector& vidx) const
{
	vidx.clear();

	Rod& rod = this->m_stateDER_0->m_rod;
	int nv = rod.getCurve().getNumNode();

	int offset = (!rod.getCurve().getIsClosed()) ? 1 : 0;

	vidx.reserve(nv);

	for (int i = 0; i < this->m_vpBTEles.size(); ++i)
	{
		if (this->m_vpBTEles[i]->getIsSplit())
			vidx.push_back(offset + i); // Add
	}
}

void DERModel::setParam_Split(const iVector& vidx)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	Rod& rod = this->m_stateDER_0->m_rod;
	int nv = rod.getCurve().getNumNode();

	int offset = (!rod.getCurve().getIsClosed()) ? -1 : 0;

	for (int i = 0; i < this->m_vpBTEles.size(); ++i)
	{
		this->m_vpBTEles[i]->setIsSplit(false);
	}

	for (int i = 0; i < (int) vidx.size(); ++i)
	{
		this->m_vpBTEles[offset + vidx[i]]->setIsSplit(true);
	}

	this->deprecateDeformation();
}

bool DERModel::getParam_Dummy() const
{
	return this->m_isDummy;
}

void DERModel::setParam_Dummy(bool s)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (s == this->m_isDummy)
		return; // No changes

	// Toggle dummy state
	this->m_isDummy = s;

	this->initializeMaterials();

	this->deprecateUndeformed();
}

bool DERModel::getParam_Straight() const
{
	return this->m_isStraight;
}

void DERModel::setParam_Straight(bool s)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (s == this->m_isStraight)
		return; // No changes

	// Toggle straight state
	this->m_isStraight = s;

	this->deprecateUndeformed();
}


void DERModel::getParam_VertexRest(VectorXd& v0v) const
{
	v0v = toEigen(this->m_stateDER_0->m_rod.getCurve().getPositions());
}

void DERModel::getParam_RadiusEdge(vector<Vector2d>& vre) const
{
	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	vre.resize(ne);
	for (int i = 0; i < ne; ++i)
	{
		vre[i].x() = this->m_vmats[i].getWRadius();
		vre[i].y() = this->m_vmats[i].getHRadius();
	}
}

void DERModel::getParam_RadiusRod(Vector2d& vrr) const
{
	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	vrr.setZero();

	for (int i = 0; i < ne; ++i)
	{
		vrr.x() += this->m_vmats[i].getWRadius();
		vrr.y() += this->m_vmats[i].getHRadius();
	}

	// Average
	vrr /= ne;
}

void DERModel::setParam_VertexRest(const VectorXd& v0v)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int nv = this->m_stateDER_0->m_rod.getNumNode();

	assert((int)v0v.size() == 3*nv);

	for (int i = 0; i < 3*nv; ++i)
		this->m_state_0->m_vx(i) = v0v(i);

	// Update rod metadata
	this->updateMetadata_0();

	this->deprecateUndeformed();
}

void DERModel::setParam_RadiusRod(const Vector2d& vr)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	vector<Vector2d> vre(ne);
	for (int i = 0; i < ne; ++i)
		vre[i] = vr;

	// Update rods geometry
	this->m_stateDER_x->m_rod.setRadius(vre);
	this->m_stateDER_0->m_rod.setRadius(vre);

	for (int i = 0; i < ne; ++i)
	{
		this->m_vmats[i].setWRadius(vre[i].x());
		this->m_vmats[i].setHRadius(vre[i].y());
	}

	this->m_isReady_Mass = false;
	this->deprecateDeformation();
}

void DERModel::setParam_RadiusEdge(const vector<Vector2d>& vr)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	assert((int) vr.size() == ne);

	// Update rods geometry
	this->m_stateDER_x->m_rod.setRadius(vr);
	this->m_stateDER_0->m_rod.setRadius(vr);

	for (int i = 0; i < ne; ++i)
	{
		this->m_vmats[i].setWRadius(vr[i].x());
		this->m_vmats[i].setHRadius(vr[i].y());
	}

	this->m_isReady_Mass = false;
	this->deprecateDeformation();
}

void DERModel::update_f0v()
{
	if (this->m_isReady_f0v)
		return; // Already done

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const dVector& vX = this->m_stateDER_0->m_rod.getPositions();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_fv(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames(),
											vX, this->m_stateDER_0->m_va, this->m_stateDER_0->m_vt, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_rod.getFrames());

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_f0v(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames(),
									    vX, this->m_stateDER_0->m_va, this->m_stateDER_0->m_vt, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_rod.getFrames());

	this->m_isReady_f0v = true;
}

void DERModel::update_flr()
{
	this->update_fle(); // Same
	this->m_isReady_flr = true;
}

void DERModel::update_fle()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_fle)
		return; // Already done

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_fl(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_fl(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

	this->m_isReady_fle = true;
}

void DERModel::update_frr()
{
	this->update_fre(); // Same
	this->m_isReady_frr = true;
}

void DERModel::update_fre()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_fre)
		return; // Already done

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_fr(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_fr(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

	this->m_isReady_fre = true;
}

void DERModel::update_DfxD0v()
{
	if (this->m_isReady_DfxD0v)
		return; // Already done

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const dVector& vX = this->m_stateDER_0->m_rod.getPositions();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_DfxDv(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames(),
											   vX, this->m_stateDER_0->m_va, this->m_stateDER_0->m_vt, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_rod.getFrames());

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_DfxD0v(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames(),
										   vX, this->m_stateDER_0->m_va, this->m_stateDER_0->m_vt, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_rod.getFrames());

	// Update also mass derivative

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_DmxDv(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames(),
											   vX, this->m_stateDER_0->m_va, this->m_stateDER_0->m_vt, this->m_stateDER_0->m_vF, this->m_stateDER_0->m_rod.getFrames());


	this->m_isReady_DfxD0v = true;
}

void DERModel::update_DfxDlr()
{
	this->update_DfxDle(); // Same
	this->m_isReady_DfxDlr = true;
}

void DERModel::update_DfxDle()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_DfxDle)
		return; // Already done

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_DfxDl(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_DfxDl(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

	// Update also mass derivative

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_DmxDl(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

	this->m_isReady_DfxDle = true;
}


void DERModel::update_DfxDrr()
{
	this->update_DfxDre(); // Same
	this->m_isReady_DfxDrr = true;
}

void DERModel::update_DfxDre()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_DfxDre)
		return; // Already done

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_DfxDr(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->update_DfxDr(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

	// Update also mass derivative

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_DmxDr(vx, this->m_stateDER_x->m_va, this->m_stateDER_x->m_vt, this->m_stateDER_x->m_vF, this->m_stateDER_x->m_rod.getFrames());

	this->m_isReady_DfxDre = true;
}

void DERModel::add_f0v(VectorXd& vf0)
{
	int nv = this->m_stateDER_0->m_rod.getNumNode();

	assert((int)vf0.size() == 3*nv);

	if (!this->m_isReady_f0v)
		this->update_f0v();

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->add_f0(vf0);

	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->add_f0(vf0);
}

void DERModel::add_flr(VectorXd& vfl)
{
	assert((int)vfl.size() == 1);

	if (!this->m_isReady_flr)
		this->update_flr();

	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	VectorXd vfle(ne);
	vfle.setZero();

	this->add_fle(vfle);

	// Edge length is uniformly dist.
	vfl(0) = vfle.sum() / (Real) ne;
}

void DERModel::add_fle(VectorXd& vfl)
{
	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	assert((int)vfl.size() == ne);

	if (!this->m_isReady_fle)
		this->update_fle();

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->add_fl(vfl);

	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->add_fl(vfl);
}

void DERModel::add_frr(VectorXd& vfr)
{
	assert((int)vfr.size() == 1);

	if (!this->m_isReady_frr)
		this->update_frr();

	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	VectorXd vfre(ne);
	vfre.setZero();

	this->add_fre(vfre);

	// Sum not average
	vfr(0) = vfre.sum();
}

void DERModel::add_fre(VectorXd& vfr)
{
	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	assert((int)vfr.size() == ne);

	if (!this->m_isReady_fre)
		this->update_fre();

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->add_fr(vfr);

	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->add_fr(vfr);
}

void DERModel::add_DfxD0v(tVector& vDfxD0v)
{
	if (!this->m_isReady_DfxD0v)
		this->update_DfxD0v();

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->add_DfxDv(vDfxD0v);

	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->add_DfxDv(vDfxD0v);

	// Add gravity contribution
	if (this->m_vg.norm() > 1e-6)
	{
		VectorXd vg(this->m_nsimDOF_x);
		vg.setZero(); // Gravity vector

		this->add_vgravity(vg);

		for (int i = 0; i < this->getNumStretchEle(); ++i) // Add
		{
			tVector vDmxDv; vDmxDv.reserve(42);
			m_vpStretchEles[i]->add_DmxDv(vDmxDv);

			int nC = (int) vDmxDv.size();
			for (int j = 0; j < nC; ++j)
			{
				const Triplet<Real>& t = vDmxDv[j]; // Multiply by gravity times identity
				vDfxD0v.push_back(Triplet<Real>(t.row(), t.col(), t.value()*vg(t.row())));
			}
		}
	}

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, 3*this->m_stateDER_x->m_rod.getNumNode());
	compressionMatrix.setFromTriplets(vDfxD0v.begin(), vDfxD0v.end());
	toTriplets(compressionMatrix, vDfxD0v);

	// Compress vector ------------------
#endif
}

void DERModel::add_DfxDlr(tVector& vDfxDlr)
{
	if (!this->m_isReady_DfxDlr)
		this->update_DfxDlr();

	int ncoeff = this->getNumNonZeros_DfxDle();

	tVector vDfxDle;
	vDfxDle.reserve(ncoeff);
	this->add_DfxDle(vDfxDle);

	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	for (int i = 0; i < ncoeff; ++i)
	{
		const Triplet<Real>& t = vDfxDle[i]; // Average row triplets
		vDfxDlr.push_back(Triplet<Real>(t.row(), 0, t.value() / ne));
	}
}

void DERModel::add_DfxDle(tVector& vDfxDle)
{
	if (!this->m_isReady_DfxDle)
		this->update_DfxDle();

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->add_DfxDl(vDfxDle);

	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->add_DfxDl(vDfxDle);

	// Add gravity contribution
	if (this->m_vg.norm() > 1e-6)
	{
		VectorXd vg(this->m_nsimDOF_x);
		vg.setZero(); // Gravity vector

		this->add_vgravity(vg);

		for (int i = 0; i < this->getNumStretchEle(); ++i) // Add
		{
			tVector vDmxDl; vDmxDl.reserve(7);
			m_vpStretchEles[i]->add_DmxDl(vDmxDl);

			int nC = (int) vDmxDl.size();
			for (int j = 0; j < nC; ++j)
			{
				const Triplet<Real>& t = vDmxDl[j]; // Multiply by gravity times identity
				vDfxDle.push_back(Triplet<Real>(t.row(), t.col(), t.value()*vg(t.row())));
			}
		}
	}

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, this->m_stateDER_x->m_rod.getNumEdge());
	compressionMatrix.setFromTriplets(vDfxDle.begin(), vDfxDle.end());
	toTriplets(compressionMatrix, vDfxDle);

	// Compress vector ------------------
#endif
}

void DERModel::add_DfxDrr(tVector& vDfxDrr)
{
	if (!this->m_isReady_DfxDrr)
		this->update_DfxDrr();

	int ncoeff = this->getNumNonZeros_DfxDre();

	tVector vDfxDre;
	vDfxDre.reserve(ncoeff);
	this->add_DfxDre(vDfxDre);

	for (int i = 0; i < ncoeff; ++i)
	{
		const Triplet<Real>& t = vDfxDre[i];

		vDfxDrr.push_back(Triplet<Real>(t.row(), t.col() % 2, t.value()));
	}
}

void DERModel::add_DfxDre(tVector& vDfxDre)
{
	if (!this->m_isReady_DfxDre)
		this->update_DfxDre();

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->add_DfxDr(vDfxDre);

	for (int i = 0; i < this->getNumBTEle(); ++i)
		this->m_vpBTEles[i]->add_DfxDr(vDfxDre);

	// Add gravity contribution
	if (this->m_vg.norm() > 1e-6)
	{
		VectorXd vg(this->m_nsimDOF_x);
		vg.setZero(); // Gravity vector

		this->add_vgravity(vg);

		for (int i = 0; i < this->getNumStretchEle(); ++i) // Add
		{
			tVector vDmxDr; vDmxDr.reserve(7);
			m_vpStretchEles[i]->add_DmxDr(vDmxDr);

			int nC = (int) vDmxDr.size();
			for (int j = 0; j < nC; ++j)
			{
				const Triplet<Real>& t = vDmxDr[j]; // Multiply by gravity times identity
				vDfxDre.push_back(Triplet<Real>(t.row(), t.col(), t.value()*vg(t.row())));
			}
		}
	}

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, 2*this->m_stateDER_x->m_rod.getNumEdge());
	compressionMatrix.setFromTriplets(vDfxDre.begin(), vDfxDre.end());
	toTriplets(compressionMatrix, vDfxDre);

	// Compress vector ------------------
#endif
}

void DERModel::compute_NodeStretchStrain(VectorXd& vsv)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	const Curve& curve = this->m_stateDER_0->m_rod.getCurve();

	int ne = curve.getNumEdge();
	int nv = curve.getNumNode();

	VectorXd vse(ne);
	vsv.resize(nv);
	vsv.setZero();
	vse.setZero();

	const dVector& vx = this->m_stateDER_x->m_rod.getPositions();
	const dVector& vX = this->m_stateDER_0->m_rod.getPositions();

	for (int i = 0; i < ne; ++i)
	{
		vse(i) = this->m_vpStretchEles[i]->compute_strain(vx, vX);
	}

	for (int i = 0; i < ne; ++i)
	{
		int pv, nv;
		curve.getEdgeNeighborNodes(i, pv, nv);
		vsv(pv) += 0.5*vse(i);
		vsv(nv) += 0.5*vse(i);
	}
}

void DERModel::compute_NodeExtensionStrain(VectorXd& vsv)
{
	int nv = this->m_stateDER_0->m_rod.getNumNode();
	this->compute_NodeStretchStrain(vsv);
	for (int i = 0; i < nv; ++i)
	{
		if (vsv(i) < 1.0)
			vsv(i) = 0.0;
	}
}

void DERModel::compute_NodeCompressionStrain(VectorXd& vsv)
{
	int nv = this->m_stateDER_0->m_rod.getNumNode();
	this->compute_NodeStretchStrain(vsv);
	for (int i = 0; i < nv; ++i)
	{
		if (vsv(i) > 1.0)
			vsv(i) = 0.0;
	}
}

void DERModel::remeshUniform(const pVector& vnodesExtI, pVector& vnodesExtO, int numV, Real maxL)
{
	// Add external constrained points

	pVector vMP = vnodesExtI;
	int nvExtC = (int)vnodesExtI.size();

	// Add model internal constrained points

	vMP.insert(vMP.begin(), this->m_sdcs.begin(), this->m_sdcs.end());

	int nvModC = (int) this->m_sdcs.size();

	int nvC = nvExtC + nvModC;

	// Create basic model curve

	const Rod& rod_x = this->getRod_x();
	const Rod& rod_0 = this->getRod_0();
	const Curve& curve_x = rod_x.getCurve();
	const Curve& curve_0 = rod_0.getCurve();
	int nvCurve = curve_x.getNumNode();

	ModelCurve vMC_x(nvCurve);
	ModelCurve vMC_0(nvCurve);

	dVector vlx = curve_x.getNodeParameters();
	dVector vl0 = curve_0.getNodeParameters();

	// Initialize model curve points

	for (int i = 0; i < nvCurve; ++i)
	{
		Vector3d tanx = curve_x.getNodeTangent(i);
		Vector3d tan0 = curve_0.getNodeTangent(i);

		vMC_x[i].m_tanI = tanx;
		vMC_x[i].m_tanO = tanx;
		vMC_x[i].m_arcLen = vlx[i];
		vMC_x[i].m_mp = ModelPoint(3, i);
		vMC_x[i].m_vp = curve_x.getPosition(i);

		vMC_0[i].m_tanI = tan0;
		vMC_0[i].m_tanO = tan0;
		vMC_0[i].m_arcLen = vl0[i];
		vMC_0[i].m_mp = ModelPoint(3, i);
		vMC_0[i].m_vp = curve_0.getPosition(i);

		if (curve_x.getIsClosed())
		{
			vMC_x[i].m_isMasterPoint = false;
			vMC_0[i].m_isMasterPoint = false;
		}
		else
		{
			vMC_x[i].m_isMasterPoint = (i == 0 || i == nvCurve - 1);
			vMC_0[i].m_isMasterPoint = (i == 0 || i == nvCurve - 1);
		}
	}

	// Add master points angle

	for (int j = 0; j < nvCurve; ++j)
	{
		if (vMC_x[j].m_tanI.dot(vMC_x[j].m_tanO) <= 0.5 &&
			vMC_0[j].m_tanI.dot(vMC_0[j].m_tanO) <= 0.5)
		{
			// Smooth boundary point
			vMC_x[j].m_isMasterPoint = false;
			vMC_0[j].m_isMasterPoint = false;
		}
		else
		{
			// Non-smooth boundary
			vMC_x[j].m_isMasterPoint = true;
			vMC_0[j].m_isMasterPoint = true;

			int pe, ne;
			curve_x.getNodeNeighborEdges(j, pe, ne);
			vMC_x[j].m_tanI = curve_x.getEdgeTangent(pe);
			vMC_x[j].m_tanO = curve_x.getEdgeTangent(ne);
			vMC_0[j].m_tanI = curve_0.getEdgeTangent(pe);
			vMC_0[j].m_tanO = curve_0.getEdgeTangent(ne);
		}
	}

	// Add master constraints

	for (int i = 0; i < nvC; ++i)
	{
		const ModelPoint& point = vMP[i];

		if (point.isExplicit())
		{
			// Look for the actual point
			int idx = point.getPointIdx();

			for (int j = 0; j < nvCurve; ++j)
			{
				if (vMC_x[j].m_mp.getPointIdx() == idx)
				{
					vMC_x[j].m_isMasterPoint = true;
					vMC_x[j].m_isMasterPoint = true;
					vMC_x[j].m_spointers.insert(i);
					break;
				}
			}
		}
		else
		{
			assert(point.getNumSup() == 2);

			// Is on node?
			int idx = -1;

			if (isApprox(point.getWeights()[0], 1.0, EPS_POS)) idx = point.getIndices()[0];
			else if (isApprox(point.getWeights()[1], 1.0, EPS_POS)) idx = point.getIndices()[1];

			if (idx != -1) // Explicit
			{
				for (int j = 0; j < nvCurve; ++j)
				{
					if (vMC_x[j].m_mp.getPointIdx() == idx)
					{
						vMC_x[j].m_isMasterPoint = true;
						vMC_0[j].m_isMasterPoint = true;
						vMC_x[j].m_spointers.insert(i);
						vMC_0[j].m_spointers.insert(i);
						break;
					}
				}
			}
			else
			{
				int iv0raw = point.getIndices()[0];
				int iv1raw = point.getIndices()[1];
				int iv0;
				int iv1;
				if (iv0raw < iv1raw)
				{
					iv0 = iv0raw;
					iv1 = iv1raw;
				}
				else
				{
					iv0 = iv1raw;
					iv1 = iv0raw;
				}

				vMC_x.push_back(CurvePoint());
				vMC_0.push_back(CurvePoint());
				CurvePoint& BP_x = vMC_x.back();
				CurvePoint& BP_0 = vMC_0.back();

				BP_x.m_mp = point;
				BP_0.m_mp = point;

				BP_x.m_vp = curve_x.evaluatePoint(point);
				BP_0.m_vp = curve_0.evaluatePoint(point);

				BP_x.m_tanI = (BP_x.m_vp - vMC_x[iv0].m_vp).normalized();
				BP_x.m_tanO = (vMC_x[iv1].m_vp - BP_x.m_vp).normalized();
				BP_0.m_tanI = (BP_0.m_vp - vMC_0[iv0].m_vp).normalized();
				BP_0.m_tanO = (vMC_0[iv1].m_vp - BP_0.m_vp).normalized();
				assert((BP_x.m_tanI - BP_x.m_tanO).isZero(EPS_POS));
				assert((BP_0.m_tanI - BP_0.m_tanO).isZero(EPS_POS));

				BP_x.m_arcLen = vMC_x[iv0].m_arcLen + (BP_x.m_vp - vMC_x[iv0].m_vp).norm();
				BP_0.m_arcLen = vMC_0[iv0].m_arcLen + (BP_0.m_vp - vMC_0[iv0].m_vp).norm();

				BP_x.m_isMasterPoint = true;
				BP_x.m_spointers.insert(i);

				BP_0.m_isMasterPoint = true;
				BP_0.m_spointers.insert(i);
			}
		}
	}

	Rod newRod0;
	Rod newRodx;

	// Sort the curve
	LT_CurvePoint LTC;

	sort(vMC_x.begin(), vMC_x.end(), LTC);
	sort(vMC_0.begin(), vMC_0.end(), LTC);

	// Split at master curve points

	vector<ModelCurve> vMC_Split_x;
	vector<ModelCurve> vMC_Split_0;

	vMC_Split_x.push_back(ModelCurve());
	vMC_Split_0.push_back(ModelCurve());

	for (int j = 1; j < nvCurve; ++j)
	{
		// Add the current point to curve
		vMC_Split_x.back().push_back(vMC_x[j]);
		vMC_Split_0.back().push_back(vMC_0[j]);

		// If this is master, new one
		if (vMC_x[j].m_isMasterPoint)
		{
			vMC_Split_x.push_back(ModelCurve());
			vMC_Split_0.push_back(ModelCurve());
			vMC_Split_x.back().push_back(vMC_x[j]);
			vMC_Split_0.back().push_back(vMC_0[j]);
		}
	}

	vMC_Split_x.back().push_back(vMC_x[0]);
	vMC_Split_0.back().push_back(vMC_0[0]);

	int numS = (int)vMC_Split_x.size();

	// Resample the splitted curve

	vector<Curve> vcurveSplit_x(vMC_Split_x.size());
	vector<Curve> vcurveSplit_0(vMC_Split_0.size());

	for (int j = 0; j < numS; ++j)
	{
		const vector<CurvePoint>& vsplit_x = vMC_Split_x[j];
		const vector<CurvePoint>& vsplit_0 = vMC_Split_0[j];

		// Build curve

		int nv = (int) vsplit_x.size();

		dVector vx_x(3 * nv);
		dVector vx_0(3 * nv);
		for (int v = 0; v < nv; ++v)
		{
			set3D(v, vsplit_x[v].m_vp, vx_x);
			set3D(v, vsplit_0[v].m_vp, vx_0);
		}

		vcurveSplit_x[j].initialize(vx_x, false);
		vcurveSplit_x[j].resample(numV,
								  vMC_Split_x[j].front().m_tanO,
								  vMC_Split_x[j].back().m_tanI);

		vcurveSplit_0[j].initialize(vx_0, false);
		vcurveSplit_0[j].resample(numV,
								  vMC_Split_0[j].front().m_tanO,
								  vMC_Split_0[j].back().m_tanI);
	}

	// Extract new coordinates

	int nvCCount = 0;

	// Maps constraint index to new
	map<int, int> mconIdx2newIdx;

	dVector vxNew_x;
	dVector vxNew_0;
	int nv = curve_x.getNumNode();
	vxNew_x.reserve(3*nv*numS);
	vxNew_0.reserve(3*nv*numS);

	for (int j = 0; j < numS; ++j)
	{
		int numV = (int) vcurveSplit_x[j].getNumNode();
		const dVector& vx_x = vcurveSplit_x[j].getPositions();
		const dVector& vx_0 = vcurveSplit_0[j].getPositions();

		int nvBSplit = nvCCount;

		// Fill map

		for (int k = 0; k < numV - 1; ++k)
		{
			set<int> spointers = vMC_Split_x[j][k].m_spointers;
			set<int>::iterator s_begin = spointers.begin();
			set<int>::iterator s_end = spointers.end();

			for (set<int>::iterator s_it = s_begin; s_it != s_end; ++s_it)
				mconIdx2newIdx[*s_it] = nvBSplit + k; // Map to new index
		}

		// Add boundary vertices and edges

		for (int k = 0; k < numV - 1; ++k)
		{
			int offset = 3 * k;
			vxNew_x.push_back(vx_x[offset + 0]);
			vxNew_x.push_back(vx_x[offset + 1]);
			vxNew_x.push_back(vx_x[offset + 2]);
			vxNew_0.push_back(vx_0[offset + 0]);
			vxNew_0.push_back(vx_0[offset + 1]);
			vxNew_0.push_back(vx_0[offset + 2]);
		}
	}

	// Create new curves

	dVector vs_x;
	dVector vs_0;
	Curve newCurve_x(vxNew_x, curve_x.getIsClosed());
	Curve newCurve_0(vxNew_x, curve_x.getIsClosed());
	int nvNew = newCurve_x.getNumNode();
	int neNew = newCurve_0.getNumEdge();
	curve_x.getEdgesLength(vs_x);
	curve_0.getEdgesLength(vs_0);
	assert(nvNew == newCurve_0.getNumNode());
	assert(neNew == newCurve_0.getNumEdge());
	
	// Interpolate state elements

	vector<Frame> vmatNew_x;
	vector<Frame> vmatNew_0;

	hermiteInterpolation(this->m_stateDER_x->m_rod.getFrames(), vs_x, vmatNew_x, newCurve_x.getIsClosed(), neNew);
	hermiteInterpolation(this->m_stateDER_0->m_rod.getFrames(), vs_0, vmatNew_0, newCurve_0.getIsClosed(), neNew);

	vector<Vector2d> vRNew;

	hermiteInterpolation(this->m_stateDER_0->m_rod.getRadius(), vs_0, vRNew, newCurve_0.getIsClosed(), neNew);

	this->m_stateDER_x->m_rod.initialize(newCurve_x, vmatNew_x, vRNew);
	this->m_stateDER_0->m_rod.initialize(newCurve_0, vmatNew_0, vRNew);

	this->m_stateDER_x->m_va.resize(neNew, 0.0);
	this->m_stateDER_0->m_va.resize(neNew, 0.0);
	this->m_stateDER_x->m_vt.resize(nvNew, 0.0);
	this->m_stateDER_0->m_vt.resize(nvNew, 0.0);
	this->m_stateDER_x->m_vF = this->m_stateDER_x->m_rod.getFrames();
	this->m_stateDER_0->m_vF = this->m_stateDER_0->m_rod.getFrames();

	// Compute discretization changes map

	vnodesExtO.resize(nvExtC);
	
	for (int i = 0; i < nvExtC; ++i)
	{
		vnodesExtO[i] = ModelPoint(3, mconIdx2newIdx[i]);
	}

	int countC = nvExtC;
	iVector vnewIdxRaw;
	vnewIdxRaw.resize(this->m_nrawDOF, -1);
	for (set<ModelPoint>::iterator s_it = this->m_sdcs.begin(); s_it != this->m_sdcs.end(); ++s_it, countC++)
	{
		int offsetOld = 3 * (*s_it).getPointIdx();
		int offsetNew = 3 * mconIdx2newIdx[countC];
		vnewIdxRaw[offsetOld + 0] = offsetNew + 0;
		vnewIdxRaw[offsetOld + 1] = offsetNew + 1;
		vnewIdxRaw[offsetOld + 2] = offsetNew + 2;
	}

	// Send discretization changed signal out
	this->onDiscretizationChanged(vnewIdxRaw);
}

void DERModel::getParam_StretchK(vector<VectorXd>& vsK) const
{
	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	vsK.resize(ne);
	for (int i = 0; i < ne; ++i)
		vsK[i][0] = this->m_vpStretchEles[i]->getMaterial()->getStretchK();
}

void DERModel::setParam_StretchK(const vector<VectorXd>& vsK)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	for (int i = 0; i < ne; ++i)
		this->m_vpStretchEles[i]->getMaterial()->setStretchK(vsK[i][0]);

	this->deprecateDeformation();
}


Real DERModel::getParam_RestLengthRod() const
{
	Real L = 0;

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		L += this->m_vpStretchEles[i]->getRestEdgeLength(0);

	return L;
}

void DERModel::getParam_RestLengthEdge(VectorXd& vle) const
{
	int ne = this->m_stateDER_x->m_rod.getNumEdge();

	vle.resize(ne);
	for (int i = 0; i < ne; ++i)
		vle(i) = this->m_vpStretchEles[i]->getRestEdgeLength(0);
}

void DERModel::setParam_RestLengthRod(Real L)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	int ne = this->m_stateDER_0->m_rod.getNumEdge();

	// Equal length for all
	Real li = L / (double)ne;

	for (int i = 0; i < this->getNumStretchEle(); ++i)
	{
		this->m_vpStretchEles[i]->setRestEdgeLength(0, li);
		this->m_vpStretchEles[i]->setIntegrationVolume(li);
	}

	for (int i = 0; i < this->getNumBTEle(); ++i)
	{
		this->m_vpBTEles[i]->setRestEdgeLength(0, li);
		this->m_vpBTEles[i]->setRestEdgeLength(1, li);
	}

	this->deprecateUndeformed();
	this->m_isReady_Rest = true;
}

void DERModel::setParam_RestLengthEdge(const VectorXd& vle)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->setRestEdgeLength(0, vle(i));

	const Curve& curve = this->m_stateDER_0->m_rod.getCurve();
	for (int i = 0; i < this->getNumBTEle(); ++i)
	{
		int pe, ne;

		if (!curve.getIsClosed())
			curve.getNodeNeighborEdges(i + 1, pe, ne);
		else curve.getNodeNeighborEdges(i, pe, ne);

		this->m_vpBTEles[i]->setRestEdgeLength(0, vle[pe]);
		this->m_vpBTEles[i]->setRestEdgeLength(1, vle[ne]);
	}

	this->deprecateUndeformed();
	this->m_isReady_Rest = true;
}

void DERModel::setParam_RestAngle(const VectorXd& vav)
{
	// TODO
}

void DERModel::getParam_RestAngle(VectorXd& vav) const
{
	// TODO
}