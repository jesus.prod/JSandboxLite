/*=====================================================================================*/
/*!
/file		NodalSolidModel.cpp
/author		jesusprod
/brief		Implementation of NodalSolidModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/NodalSolidModel.h>

void NodalSolidModel::add_fgravity(VectorXd& vf)
{
	assert((int)this->m_vg.size() == this->m_numDim_x);

	if (!this->m_isReady_Mass)
		this->update_Mass();

	assert((int)vf.size() == this->m_nsimDOF_x);

	// Using lumped mass vector

	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim_x*i;
		for (int j = 0; j < this->m_numDim_x; ++j)
			vf[offset + j] += this->m_vMass[offset + j] * this->m_vg[j];
	}
}

void NodalSolidModel::add_vgravity(VectorXd& vf)
{
	assert((int)this->m_vg.size() == this->m_numDim_x);

	if (!this->m_isReady_Mass)
		this->update_Mass();

	assert((int)vf.size() == this->m_nsimDOF_x);

	// Using lumped mass vector

	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim_x*i;
		for (int j = 0; j < this->m_numDim_x; ++j)
			vf[offset + j] += this->m_vg[j];
	}
}

void NodalSolidModel::addGravity(const VectorXd& vg, VectorXd& vf, const vector<bool>* pvFixed)
{
	if (!this->m_isReady_Mass)
		this->update_Mass();

	assert((int) vg.size() == this->m_numDim_x);
	assert((int) vf.size() == this->m_nsimDOF_x);
	if (pvFixed != NULL)
		assert((int) pvFixed->size() == this->m_nrawDOF);

	// Using lumped mass vector

	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim_x*i;
		for (int j = 0; j < this->m_numDim_x; ++j)
		{
			int index = offset + j;

			if (pvFixed != NULL)
			{
				if (!pvFixed->at(this->m_vraw2sim_x[index])) // If it is not fixed
					vf[index] += this->m_vMass[this->m_vraw2sim_x[index]] * vg[j];
			}
			else
			{
				vf[index] += this->m_vMass[this->m_vraw2sim_x[index]] * vg[j];
			}
		}
	}
}

iVector NodalSolidModel::getNodeRawDOF(int nodei) const
{
	assert(nodei >= 0 && nodei < this->m_numNodeRaw);

	iVector vidx(this->m_numDimRaw);
	for (int i = 0; i < this->m_numDimRaw; ++i)
		vidx[i] = this->m_numDimRaw*nodei + i;

	return vidx;
}

iVector NodalSolidModel::getNodeSimDOF_x(int nodei) const
{
	iVector vidxRaw = this->getNodeRawDOF(nodei);

	iVector vidxSim(this->m_numDimRaw);
	const iVector& raw2sim_x = this->getRawToSim_x();
	for (int i = 0; i < this->m_numDimRaw; ++i)
		vidxSim[i] = raw2sim_x[vidxRaw[i]];

	return vidxSim;
}

iVector NodalSolidModel::getNodeSimDOF_0(int nodei) const
{
	iVector vidxRaw = this->getNodeRawDOF(nodei);

	iVector vidxSim(this->m_numDimRaw);
	const iVector& raw2sim_x = this->getRawToSim_0();
	for (int i = 0; i < this->m_numDimRaw; ++i)
		vidxSim[i] = raw2sim_x[vidxRaw[i]];

	return vidxSim;
}

iVector NodalSolidModel::getAABBIndices(const dVector& vaabb) const
{
	assert((int)vaabb.size() == this->m_numDim_x*2);

	iVector vselection;
	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim_x*i;

		bool selected = true;
		for (int j = 0; j < this->m_numDim_x; ++j)
		{
			double val = this->m_state_x->m_vx[offset + j];
			double min = vaabb[2*j + 0];
			double max = vaabb[2*j + 1];
			if (val < min || val > max)
			{
				selected = false;
				break; // Found
			}
		}

		if (selected)
		{
			for (int j = 0; j < this->m_numDim_x; ++j)
				vselection.push_back(offset + j);
		}
	}

	return vselection;
}