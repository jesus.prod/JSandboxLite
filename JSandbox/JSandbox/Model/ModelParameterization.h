/*=====================================================================================*/
/*!
/file		ModelParameterization.h
/author		jesusprod
/brief		TODO
*/
/*=====================================================================================*/

#ifndef MODEL_PARAMETERIZATION_H
#define MODEL_PARAMETERIZATION_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Model/SolidModel.h>
#include <JSandbox/Solver/PhysSolver.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/CustomTimer.h>

class ParameterSet;

class   ModelParameterization
{
public:
	ModelParameterization(const string& ID);

	virtual ~ModelParameterization();

	virtual void setModel(SolidModel* pModel);
	virtual void setSolver(PhysSolver* pSolver);
	virtual SolidModel* getModel() const;
	virtual PhysSolver* getSolver() const;

	virtual void setParameterSets(const vector<ParameterSet*>& vs);
	virtual const vector<ParameterSet*>& getParameterSets() const;

	virtual Real getSeamNode_K() const { return this->m_seamNodeK; }
	virtual Real getHingeNode_K() const { return this->m_hingeNodeK; }
	virtual Real getMinSurface_K() const { return this->m_minSurfaceK; }
	virtual Real getEquiNode_K() const { return this->m_equiNodeK; }

	virtual void setSeamNode_K(Real sn) { this->m_seamNodeK = sn; }
	virtual void setHingeNode_K(Real hn) { this->m_hingeNodeK = hn; }
	virtual void setMinSurface_K(Real ms) { this->m_minSurfaceK = ms; }
	virtual void setEquiNode_K(Real en) { this->m_equiNodeK = en; }

	virtual int getParNumber() const { return this->m_np; }
	virtual int getSetNumber() const { return this->m_ns; };

	virtual void setDOFFixed(const iVector& vi);
	virtual const iVector& getDOFFixed() const;

	virtual void setDOFSeams(const iVector& vi);
	virtual const iVector& getDOFSeams0() const;
	virtual const iVector& getDOFSeams1() const;

	virtual void setDOFHinges(const iVector& vi);

	virtual void setDOFTargets(const iVector& vi);
	virtual const iVector& getDOFTargets() const;

	virtual void setDOFGoals(const iVector& vi, const dVector& vw);
	virtual const iVector& getDOFGoalIndices() const;
	virtual const dVector& getDOFGoalWeigths() const;

	virtual bool isReady_St() const;
	virtual bool isReady_Pt() const;
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDx() const;
	virtual bool isReady_DfDp() const;
	virtual bool isReady_DxDp() const;
	virtual bool isReady_DpDg() const;
	virtual bool isReady_DxDg() const;
	virtual bool isReady_DfDxp() const;
	virtual bool isReady_DfDp2() const;
	virtual bool isReady_DxDp2() const;
	virtual bool isReady_DxDpS2() const;
	virtual bool isReady_DfDxp2() const;
	
	virtual void update_St();
	virtual void update_Pt();
	virtual void update_fp();
	virtual void update_DfDx();
	virtual void update_DfDp();
	virtual void update_DxDp();
	virtual void process_DxDp();
	virtual void update_DpDg();
	virtual void update_DxDg();
	virtual void update_DfDxp();
	virtual void update_DxDp2();
	virtual void update_DxDpS2();
	virtual void update_DfDp2();
	virtual void update_DfDxp2();

	virtual const VectorXd& get_fp();
	virtual const MatrixSd& get_Ss0();
	virtual const MatrixSd& get_Ss1();
	virtual const MatrixSd& get_Pt();
	virtual const MatrixSd& get_St();
	virtual const MatrixSd& get_DfDx();
	virtual const MatrixSd& get_DfDp();
	virtual const MatrixSd& get_DxDp_Pro();
	virtual const MatrixSd& get_DxDp_Raw();
	virtual const MatrixSd& get_DpDg();
	virtual const MatrixSd& get_DxDg();
	virtual const MatrixSd& get_DfDxp();
	virtual const MatrixSd& get_DfDp2();
	virtual const MatrixSd& get_DxDp2();
	virtual const MatrixSd& get_DxDpS2();
	virtual const MatrixSd& get_DfDxp2();
	virtual const vector<MatrixSd>& get_Sh();

	virtual void analyse_DxDp_Single(VectorXd& vpS, Real& avgNorm, Real& minNorm, Real& maxNorm);
	virtual void analyse_DxDp_Grouped(VectorXd& vpS, Real& avgNorm, Real& minNorm, Real& maxNorm);

	virtual bool getParameterMapping() const { return this->m_parameterMapping; }
	virtual bool getParameterScaling() const { return this->m_parameterScaling; }
	virtual void setParameterMapping(bool pm) { this->m_parameterMapping = pm; }
	virtual void setParameterScaling(bool ps) { this->m_parameterScaling = ps; }

	virtual void setScale_Identity();
	virtual void setScale_DxDpNorm_Single();
	virtual void setScale_DxDpNorm_Grouped();

	///* TO DEPRECATE */
	//virtual void setScale_DxDpNorm_Custom(const dVector& vwei);
	//virtual void setScale_GradNorm(const VectorXd vt);
	///* TO DEPRECATE */

	virtual const MatrixSd& getParametersScaleD() { return this->m_mpS; }
	virtual MatrixSd getParametersScaleI() { return m_mpS.cwiseInverse(); }

	virtual void setParametersScale(const MatrixSd& mpS) { this->m_mpS = mpS; }

	virtual VectorXd randomizeParameters();

	virtual void mapParameters_Raw2Unconstrained(const VectorXd& vpRaw, VectorXd& vpUnc) const;
	virtual void mapParameters_Unconstrained2Raw(const VectorXd& vpUnc, VectorXd& vpRaw) const;

	virtual const VectorXd& getParametersLowerBound_Raw() const;
	virtual const VectorXd& getParametersUpperBound_Raw() const;

	virtual const VectorXd& getParameters_Raw();

	virtual const VectorXd& getParameters_Pro();
	virtual const VectorXd& getParametersLowerBound_Pro();
	virtual const VectorXd& getParametersUpperBound_Pro();
	virtual void setParameters_Pro(const VectorXd& vp);

	virtual void capParameterValue(VectorXd& vp);
	virtual void capParameterStep(VectorXd& vp);

	virtual bool isValidParameters(const VectorXd& vp, VectorXd& vpBD, double tol = EPS_APPROX);

	virtual void setup();

	virtual void updateBoundsVector();

	virtual void testParameterLinearization();

	// Param --------------------------------------------------------------------------

	virtual Real Param_computeObjective();
	virtual void Param_computeGradient(VectorXd& vgParam);
	virtual void Param_computeHessian(MatrixSd& mHParam);

	// Goal --------------------------------------------------------------------------

	virtual Real Goal_computeObjective(const VectorXd& vt);
	virtual void Goal_computeGradient(const VectorXd& vt, VectorXd& vgGaol);
	virtual void Goal_computeHessian(const VectorXd& vt, MatrixSd& mHGoal);

	// Seams --------------------------------------------------------------------------

	virtual Real Seam_computeObjective();
	virtual void Seam_computeGradient(VectorXd& vgSeam);
	virtual void Seam_computeHessian(MatrixSd& mHSeam);

	// Hinges -------------------------------------------------------------------------

	//virtual Real Hinge_computeObjective();
	//virtual void Hinge_computeGradient(VectorXd& vgHinge);
	//virtual void Hinge_computeHessian(MatrixSd& mHHinge);

	// Force-based error methods ------------------------------------------------------

	virtual Real FB_computeObjective();

	virtual void FB_computeDefaultGradient(VectorXd& vgDef);
	virtual void FB_computeBoundedGradient(VectorXd& vgCon);

	virtual void FB_computeDefaultGradient_FD(VectorXd& vgDef);
	virtual void FB_computeBoundedGradient_FD(VectorXd& vgCon);

	virtual void FB_testObjectiveGradient();

	virtual bool FB_computeParameterStep_CQP(VectorXd& vdp, string& error);
	virtual bool FB_computeParameterStep_UQP_ProjectStep(VectorXd& vdp, string& error);
	virtual bool FB_computeParameterStep_UQP_ProjectGrad(VectorXd& vdp, string& error);

	// Mixed-based error methods ------------------------------------------------------

	virtual Real MB_computeObjective();

	virtual void MB_computeDefaultGradient(VectorXd& vgDef);
	virtual void MB_computeBoundedGradient(VectorXd& vgCon);

	virtual void MB_computeDefaultGradient_FD(VectorXd& vgDef);
	virtual void MB_computeBoundedGradient_FD(VectorXd& vgCon);

	virtual void MB_testObjectiveGradient();

	virtual bool MB_computeParameterStep_CQP(VectorXd& vdp, string& error);
	virtual bool MB_computeParameterStep_UQP_ProjectStep(VectorXd& vdp, string& error);
	virtual bool MB_computeParameterStep_UQP_ProjectGrad(VectorXd& vdp, string& error);

	// Position-based error methods ------------------------------------------------------

	/*! For a given target displacement of deformed configuration, and considering the current selection matrix,
	*	this method computes the corresponding parameter and deformed step lying on the linearized manifold of
	*	static equilibrium constraints.
	*/

	virtual Real PB_computeObjective(const VectorXd& vt);
	virtual void PB_computeDefaultGradient(const VectorXd& vt, VectorXd& vgDef);
	virtual void PB_computeBoundedGradient(const VectorXd& vt, VectorXd& vgCon);
	virtual void PB_computeHessian(const VectorXd& vt, MatrixSd& mH);

	virtual void PB_computeDefaultGradient_FD(const VectorXd& vt, VectorXd& vgDef);
	virtual void PB_computeBoundedGradient_FD(const VectorXd& vt, VectorXd& vgCon);

	virtual void PB_testObjectiveGradient(const VectorXd& vt);

	virtual bool PB_computeParameterStep_CQP(const VectorXd& vt, VectorXd& vdp, string& error);
	virtual bool PB_computeParameterStep_UQP_ProjectStep(const VectorXd& vt, VectorXd& vdp, string& error);
	virtual bool PB_computeParameterStep_UQP_ProjectGrad(const VectorXd& vt, VectorXd& vdp, string& error);

	// Eigenspace related methods ------------------------------------------------------

	virtual void computeEditSuitability(const vector<VectorXd>& vedit, VectorXd& vcost);
	virtual void computeEigenvaluesSpace_DxDg(vector<EigenPair>& veigenPairs, bool sort);
	virtual void computeEigenvaluesSpace_DxDp(vector<EigenPair>& veigenPairs, bool sort);

	virtual void computeEigenvaluesSpace_DpDx(vector<EigenPair>& veigenPairs, bool sort) {}

	virtual void getEigenvaluesSpaceRepresentation(int idx, const vector<EigenPair>& veigenPairs, vector<Vector3d>& vp, vector<Vector3d>& vv) {};

protected:

	virtual void addDiagonal(tVector& vM, const bVector& vstencil, Real value);
	virtual void removeCols(tVector& vM, const bVector& vstencil);
	virtual void removeRows(tVector& vM, const bVector& vstencil);

	virtual void freeParameters();

protected:

	string m_ID;

	int m_ns;
	int m_np;
	int m_nx;

	bool m_parameterScaling;
	bool m_parameterMapping;

	VectorXd m_vpRaw;
	VectorXd m_vpUBRaw;
	VectorXd m_vpLBRaw;

	VectorXd m_vpPro;
	VectorXd m_vpUBPro;
	VectorXd m_vpLBPro;

	iVector m_vsetOff;

	vector<ParameterSet*> m_vpset;

	iVector m_vDOFGoalsRaw;
	dVector m_vDOFGoalsWei;
	iVector m_vDOFTargetsRaw;
	iVector m_vDOFSeamsRaw0;
	iVector m_vDOFSeamsRaw1;
	iVector m_vDOFHingeRaw;
	iVector m_vDOFFixedRaw;

	bVector m_vGoalsStencil;
	bVector m_vTargetsStencil;
	bVector m_vSeamsStencil0;
	bVector m_vSeamsStencil1;
	bVector m_vParamStencil;
	bVector m_vFixedStencil;

	Real m_minSurfaceK;
	Real m_hingeNodeK;
	Real m_equiNodeK;
	Real m_seamNodeK;

	MatrixSd m_mpS;

	VectorXd m_vfp;

	MatrixSd m_mDfDxs;
	MatrixSd m_mDfDps;
	MatrixSd m_mDxDpsPro;
	MatrixSd m_mDxDpsRaw;
	MatrixSd m_mDpDgs;
	MatrixSd m_mDxDgs;
	MatrixSd m_mDfDxps;
	MatrixSd m_mDfDp2s;
	MatrixSd m_mDxDp2s;
	MatrixSd m_mDxDpS2s;
	MatrixSd m_mDfDxp2s;
	MatrixSd m_mPs;
	MatrixSd m_mKs;
	MatrixSd m_mSs;
	MatrixSd m_mNs;
	MatrixSd m_mS0s;
	MatrixSd m_mS1s;
	vector<MatrixSd> m_vSh;

	MatrixSd m_mDrDu;

	vector<MatrixSd> m_vmBm;
	vector<MatrixSd> m_vmRm;

	vector<MatrixSd> m_vmSim2Mem;
	vector<MatrixSd> m_vmMem2Int;
	vector<MatrixSd> m_vmMem2Bou;

	SolidModel* m_pModel;
	PhysSolver* m_pSolver;

	bool m_isReady_fp;
	bool m_isReady_DfDx;
	bool m_isReady_DfDp;
	bool m_isReady_DxDp;
	bool m_isReady_DpDg;
	bool m_isReady_DxDg;
	bool m_isReady_DfDxp;
	bool m_isReady_DfDp2;
	bool m_isReady_DxDp2;
	bool m_isReady_DxDpS2;
	bool m_isReady_DfDxp2;
	bool m_isReady_St;
	bool m_isReady_Pt;

	//Curve3DAngleElement m_hingeElement;
	SolidMaterial m_hingeMaterial;

};

#endif