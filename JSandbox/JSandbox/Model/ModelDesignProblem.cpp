/*=====================================================================================*/
/*!
/file		ModelDesignProblem.cpp
/author		jesusprod
/brief		Implementation of ModelDesignProblem.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/ModelDesignProblem.h>

#include <JSandbox/Optim/AlgLib_Sparse_BoxC_QP.h>
#include <JSandbox/Optim/AlgLib_Sparse_EQIN_QP.h>


ModelDesignProblem::ModelDesignProblem(const string& ID)
{
	this->m_ID = ID;

	this->m_ns = 0;
	this->m_np = 0;

	this->freeParameters();
}

ModelDesignProblem::~ModelDesignProblem()
{
	freeParameters();
}

void ModelDesignProblem::setModel(SolidModel* pModel)
{
	assert(pModel != NULL);
	this->m_pModel = pModel;
}

void ModelDesignProblem::setSolver(PhysSolver* pSolver)
{
	assert(pSolver != NULL);
	this->m_pSolver = pSolver;
}

SolidModel* ModelDesignProblem::getModel() const
{
	return this->m_pModel;
}

PhysSolver* ModelDesignProblem::getSolver() const
{
	return this->m_pSolver;
}

void ModelDesignProblem::setParameterSets(const vector<ParameterSet*>& vpset)
{
	int ns = (int)vpset.size();

	assert(ns >= 0);
	freeParameters();

	this->m_vpset = vpset;
	this->m_ns = ns;
	this->m_np = 0;
}

void ModelDesignProblem::freeParameters()
{
	if (this->m_ns != 0)
	{
		for (int i = 0; i < this->m_ns; ++i)
			delete this->m_vpset[i];
		this->m_vpset.clear();

		this->m_ns = 0;
		this->m_np = 0;
	}

	this->m_isReady_DfDp = false;
}

const vector<ParameterSet*>& ModelDesignProblem::getParameterSets() const
{
	return this->m_vpset;
}

void ModelDesignProblem::setup()
{
	// Setup parameters

	this->m_np = 0;

	this->m_vsetOff.clear(); // Offsets
	for (int i = 0; i < this->m_ns; ++i)
	{
		this->m_vpset[i]->setup();
		this->m_vsetOff.push_back(this->m_np);
		this->m_np += this->m_vpset[i]->getNumParameter();
	}

	int offset = 0;
	this->m_vParamStencil.assign(this->m_np, false);
	for (int i = 0; i < this->m_ns; ++i)
	{
		const iVector& vfixed = this->m_vpset[i]->getFixedParameters();

		for (int j = 0; j < (int)vfixed.size(); ++j)
		{
			this->m_vParamStencil[offset + vfixed[j]] = true; // Filter
			logSimu("[INFO] Fixed parameter (%u, %u)", i, vfixed[j]);
		}

		offset += this->m_vpset[i]->getNumParameter();
	}

	this->m_nx = this->m_pModel->getNumSimDOF_x();

	// Update bounds vector

	this->updateBoundsVector();

	// Deprecate linearisation

	this->m_isReady_St = false;
	this->m_isReady_DfDx = false;
	this->m_isReady_DfDp = false;
	this->m_isReady_DxDp = false;
	this->m_isReady_DxDp2 = false;

	this->getParameters();

	this->testParameterLinearization();
}

void ModelDesignProblem::testParameterLinearization()
{
	// Test all parameters linearization
	for (int i = 0; i < this->m_ns; ++i)
		this->m_vpset[i]->test_DfDp();
}

// Goal --------------------------------------------------------------------------

Real ModelDesignProblem::Goal_computeObjective(const VectorXd& vt)
{
	VectorXd vx = m_pModel->getPositions_x();

	logSimu("[EXTRA TRACE] Distance to target: %.9f", (vt - vx).norm());

	VectorXd vdt = this->get_St()*(vt - vx);

	Real energy = 0.5*vdt.squaredNorm();

	logSimu("[INFO] /t-Position goals potential: %.9f", energy);

	//logSimu("[INFO] Goals distance: %.9f", vdt.norm());
	//logSimu("[INFO] Goals potential: %.9f", energy);

	return energy;
}

void ModelDesignProblem::Goal_computeGradient(const VectorXd& vt, VectorXd& vgGoal)
{
	const MatrixSd& mDxDp = this->get_DxDp();

	vgGoal = -mDxDp.transpose()*(this->get_St().transpose()*(this->get_St()*(vt - m_pModel->getPositions_x())));

	//logSimu("[INFO] Goals gradient: %.9f", vgGoal.norm());
}

void ModelDesignProblem::Goal_computeHessian(const VectorXd& vdt, MatrixSd& mHGoal)
{
	mHGoal = this->get_DxDp2();
}

// Position-based objectives ------------------------------------------------------

Real ModelDesignProblem::computeObjective(const VectorXd& vt)
{
	Real error = 0;

	error += this->Goal_computeObjective(vt);

	return error;
}

void ModelDesignProblem::computeDefaultGradient(const VectorXd& vt, VectorXd& vgDef)
{
	this->Goal_computeGradient(vt, vgDef);
}

void ModelDesignProblem::computeBoundedGradient(const VectorXd& vt, VectorXd& vgCon)
{
	this->computeDefaultGradient(vt, vgCon);

	const VectorXd vpMax = this->getParametersUpperBound();
	const VectorXd vpMin = this->getParametersLowerBound();
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(this->m_vp[i], vpMax[i], 1e-6) && vgCon[i] > 0)
			vgCon[i] = 0.0; // This is not going higher, make zero

		if (isApprox(this->m_vp[i], vpMin[i], 1e-6) && vgCon[i] < 0)
			vgCon[i] = 0.0; // This is not going lower, make zero
	}
}

void ModelDesignProblem::computeHessian(const VectorXd& vt, MatrixSd& mH)
{
	MatrixSd mHGoal;
	MatrixSd mHSeam;
	MatrixSd mHHinge;
	MatrixSd mHParam;
	this->Goal_computeHessian(vt, mHGoal);
	mH = mHGoal;
}

void ModelDesignProblem::computeDefaultGradient_FD(const VectorXd& vt, VectorXd& vgDef)
{
	Real solverError = this->m_pSolver->getSolverError();
	int solverIters = this->m_pSolver->getSolverMaxIters();

	VectorXd vpFD = this->getParameters();

	pSolidState pStateIni = this->m_pModel->getState_x()->clone();

	vgDef.resize(this->m_np);

	// Set the sensitivity of the solver
	this->m_pSolver->setSolverError(1e-12);
	this->m_pSolver->setSolverMaxIters(100);

	for (int i = 0; i < this->m_np; ++i)
	{
		this->m_pModel->setState_x(pStateIni);

		// Plus
		vpFD(i) += 1e-6;
		this->setParameters(vpFD);
		this->m_pSolver->solve();
		Real errorp = 0.5*(this->m_pModel->getPositions_x() - vt).squaredNorm();

		this->m_pModel->setState_x(pStateIni);

		// Minus
		vpFD(i) -= 2e-6;
		this->setParameters(vpFD);
		this->m_pSolver->solve();
		Real errorm = 0.5*(this->m_pModel->getPositions_x() - vt).squaredNorm();

		// Estimate
		vgDef(i) = (errorp - errorm) / 2e-6;

		vpFD(i) += 1e-6;
	}

	this->setParameters(vpFD);

	this->m_pModel->setState_x(pStateIni);

	// Restore the original solver sensitivity
	this->m_pSolver->setSolverError(solverError);
	this->m_pSolver->setSolverMaxIters(solverIters);
}

void ModelDesignProblem::computeBoundedGradient_FD(const VectorXd& vt, VectorXd& vgCon)
{
	this->computeDefaultGradient_FD(vt, vgCon);

	const VectorXd vpMax = this->getParametersUpperBound();
	const VectorXd vpMin = this->getParametersLowerBound();
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(this->m_vp[i], vpMax[i], 1e-6) && vgCon[i] > 0)
			vgCon[i] = 0.0; // This is not going higher, make zero

		if (isApprox(this->m_vp[i], vpMin[i], 1e-6) && vgCon[i] < 0)
			vgCon[i] = 0.0; // This is not going lower, make zero
	}
}

void ModelDesignProblem::testObjectiveGradient(const VectorXd& vt)
{
	VectorXd vgA;
	VectorXd vgF;
	this->computeDefaultGradient(vt, vgA);
	this->computeDefaultGradient_FD(vt, vgF);

	VectorXd vgD = vgA - vgF;
	Real normF = vgF.norm();
	Real normD = vgD.norm();
	Real errorRel = normD / normF;

	if (errorRel > 1e-9)
	{
		logSimu("[ERROR] Error computing the objective gradient: %.9f", errorRel);
		//writeToFile(vgF, "objectiveGradientF.csv");
		//writeToFile(vgA, "objectiveGradientA.csv");
	}
}

bool ModelDesignProblem::computeParameterStep_CQP(const VectorXd& vt, VectorXd& vdp, string& error)
{
	logSimu("[INFO] Computing step using CQP (Box-Constrained QP)");

	assert((int)vt.size() == this->m_pModel->getNumSimDOF_x());

	// Solve the problem min q(p) = 1/2 (Sx - St)^T(Sx - St) which is a quadratic
	// problem of the form: min q(p) ~ 1/2 p^T * G * p + p^T * c where coefficients:
	//		* G = A^T*S^T*S*A
	//		* c = -A^T*S^T*S*t
	// And:
	//		* A = Dx/Dp
	//
	// The matrix G is probably singular if rank(S) < rank(A) or rank(S) > rank(A)
	// but the number of design parameters is greater than the number of defomration
	// DOF. We trade-off some convergence regularizing the matrix.

	// Create the vector/matrix of linear/quadratic coefficients

	VectorXd vc;
	MatrixSd mGs;
	this->computeHessian(vt, mGs);
	this->computeDefaultGradient(vt, vc);

	// Get the vectors of parameter bounds

	const VectorXd& vpub = this->getParametersUpperBound();
	const VectorXd& vplb = this->getParametersLowerBound();

	VectorXd vdpUB = vpub - this->m_vp;
	VectorXd vdpLB = vplb - this->m_vp;

	// Solve QP

	AlgLib_Sparse_BoxC_QP QPsolver;

	tVector tGs;

	VectorXd vx0(this->m_np);
	vx0.setZero(); // Init.

	VectorXd vxo(this->m_np);
	vxo.setZero(); // Init.

	toTriplets(mGs, tGs);

	bool solved = QPsolver.solve(tGs, vc, vx0, vdpLB, vdpUB, vxo, error);

	if (solved)
	{
		// Copy
		vdp = vxo;

		if ((-vc).dot(vdp) < 0.0)
		{
			logSimu("[WARNING] Non-descendent direction (CQP): %.9f", (-vc).dot(vdp));

			//writeToFile(mGs, "hessianCQP.csv");

			return false;
		}
	}

	return solved;
}

bool ModelDesignProblem::computeParameterStep_UQP_ProjectGrad(const VectorXd& vt, VectorXd& vdp, string& error)
{
	logSimu("[INFO] Computing step using UQP-Grad (Unconstrained QP Gradient Projection)");

	assert((int)vt.size() == this->m_pModel->getNumSimDOF_x());

	VectorXd vgCon;
	MatrixSd mGs;
	this->computeHessian(vt, mGs);
	this->computeDefaultGradient(vt, vgCon);

	VectorXd vgConNeg = -1.0*vgCon;

	tVector vGs;
	toTriplets(mGs, vGs);
	this->removeRows(vGs, this->m_vParamStencil);
	this->removeCols(vGs, this->m_vParamStencil);
	this->addDiagonal(vGs, this->m_vParamStencil, 1.0);

	mGs.setFromTriplets(vGs.begin(), vGs.end());
	mGs.makeCompressed();

	PhysSolver::SolveLinearSystem(mGs, vgConNeg, vdp);

	if (vgConNeg.dot(vdp) < 0.0)
	{
		logSimu("[WARNING] Non-descendent direction (UQP Project Grad): %.9f", vgConNeg.dot(vdp));

		//writeToFile(mGs, "hessianUQPGrad.csv");

		return false;
	}

	return true;
}

bool ModelDesignProblem::computeParameterStep_UQP_ProjectStep(const VectorXd& vt, VectorXd& vdp, string& error)
{
	logSimu("[INFO] Computing step using UQP-Step (Unconstrained QP Step Projection)");

	assert((int)vt.size() == this->m_pModel->getNumSimDOF_x());

	VectorXd vgDef;
	MatrixSd mGs;
	this->computeHessian(vt, mGs);
	this->computeBoundedGradient(vt, vgDef);

	VectorXd vgDefNeg = -1.0*vgDef;

	PhysSolver::SolveLinearSystem(mGs, vgDefNeg, vdp);

	if (vgDefNeg.dot(vdp) < 0.0)
	{
		logSimu("[WARNING] Non-descendent direction (UQP Project Step): %.9f", vgDefNeg.dot(vdp));

		//writeToFile(mGs, "hessianUQPStep.csv");

		return false;
	}

	return true;
}

// Parameterization ---------------------------------------------------------------

void ModelDesignProblem::capParameterValue(VectorXd& vp)
{
	const VectorXd& vpMax = this->getParametersUpperBound();
	const VectorXd& vpMin = this->getParametersLowerBound();

	for (int i = 0; i < (int)vp.size(); ++i)
	{
		if (vp(i) < vpMin(i))
			vp(i) = vpMin(i);

		if (vp(i) > vpMax(i))
			vp(i) = vpMax(i);
	}
}

void ModelDesignProblem::capParameterStep(VectorXd& vdp)
{
	VectorXd vpNew = this->getParameters() + vdp;
	const VectorXd& vpMax = this->getParametersUpperBound();
	const VectorXd& vpMin = this->getParametersLowerBound();

	for (int i = 0; i < (int)vpNew.size(); ++i)
	{
		if (vpNew(i) < vpMin(i))
			vpNew(i) = vpMin(i);

		if (vpNew(i) > vpMax(i))
			vpNew(i) = vpMax(i);
	}

	vdp = vpNew - this->getParameters();
}

void ModelDesignProblem::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	logSimu("[INFO] Taking a step of size: %.9f", (vp - this->m_vp).norm());

	for (int i = 0; i < this->m_ns; ++i)
	{
		VectorXd vpi;
		int npi = this->m_vpset[i]->getNumParameter();
		getSubvector(this->m_vsetOff[i], npi, vp, vpi);
		this->m_vpset[i]->setParameters(vpi);
	}

	this->m_vp = vp;

	this->m_isReady_DfDp = false;
	this->m_isReady_DfDx = false;
}

const VectorXd& ModelDesignProblem::getParameters()
{
	this->m_vp.resize(this->m_np);
	this->m_vp.setZero(); // Init.

	for (int i = 0; i < this->m_ns; ++i)
	{
		VectorXd vpi;
		this->m_vpset[i]->getParameters(vpi);
		int npi = this->m_vpset[i]->getNumParameter();
		setSubvector(m_vsetOff[i], npi, vpi, m_vp);
	}

	return this->m_vp;
}

const VectorXd& ModelDesignProblem::getParametersLowerBound() const
{
	return this->m_vpLB;
}

const VectorXd& ModelDesignProblem::getParametersUpperBound() const
{
	return this->m_vpUB;
}

VectorXd ModelDesignProblem::randomizeParameters()
{
	VectorXd vp(this->m_np);
	vp.setZero(); // Init.

	const VectorXd& vpMax = this->getParametersUpperBound();
	const VectorXd& vpMin = this->getParametersLowerBound();

	srand(time(NULL));

	for (int i = 0; i < this->m_np; ++i)
	{
		Real alpha = (double)rand() / (double)RAND_MAX;
		if (vpMax[i] != HUGE_VAL && vpMin[i] != -HUGE_VAL)
		{
			Real minp = -0.01;
			Real maxp = +0.01;
			Real range = maxp - minp;
			Real dp = minp + alpha*range;
			vp(i) = this->m_vp(i) + dp;
		}
		else
		{
			Real minp = vpMin(i);
			Real maxp = vpMax(i);
			Real range = maxp - minp;
			vp(i) = minp + alpha*range;
		}
	}

	return vp;
}

void ModelDesignProblem::updateBoundsVector()
{
	// Set lower and upper bounds

	this->m_vpLB.resize(this->m_np);
	this->m_vpUB.resize(this->m_np);

	for (int i = 0; i < this->m_ns; ++i)
	{
		this->m_vpset[i]->updateBoundsVector();
		int Ni = this->m_vpset[i]->getNumParameter();
		const VectorXd& vlbRaw = this->m_vpset[i]->getMinimumValues();
		const VectorXd& vubRaw = this->m_vpset[i]->getMaximumValues();
		for (int j = 0; j < Ni; ++j)
		{
			this->m_vpLB(this->m_vsetOff[i] + j) = vlbRaw(j);
			this->m_vpUB(this->m_vsetOff[i] + j) = vubRaw(j);
		}
	}
}

bool ModelDesignProblem::isValidParameters(const VectorXd& vp, VectorXd& vpBD, double tol)
{
	bool valid = true;

	vpBD.resize(this->m_np);
	vpBD.setZero(); // Init.

	const VectorXd& vpUB = this->getParametersUpperBound();
	const VectorXd& vpLB = this->getParametersLowerBound();

	for (int i = 0; i < this->m_np; ++i)
	{
		if (vpLB(i) - vp[i] > tol)
		{
			vpBD[i] = vpLB(i) - vp[i];
			valid &= false;
		}
		if (vp[i] - vpUB(i) > tol)
		{
			vpBD[i] = vp[i] - vpUB(i);
			valid &= false;
		}
	}

	return valid;
}

// Matrices -------------------------------------------------------------------------

const MatrixSd& ModelDesignProblem::get_St()
{
	if (!this->isReady_St())
		this->update_St();

	return this->m_mSs;
}

const MatrixSd& ModelDesignProblem::get_DfDx()
{
	if (!this->isReady_DfDx())
		this->update_DfDx();

	return this->m_mDfDxs;
}

const MatrixSd& ModelDesignProblem::get_DfDp()
{
	if (!this->isReady_DfDp())
		this->update_DfDp();

	return this->m_mDfDps;
}

const MatrixSd& ModelDesignProblem::get_DxDp()
{
	if (!this->isReady_DxDp())
		this->update_DxDp();

	return this->m_mDxDps;
}

const MatrixSd& ModelDesignProblem::get_DxDp2()
{
	if (!this->isReady_DxDp2())
		this->update_DxDp2();

	return this->m_mDxDp2s;
}

bool ModelDesignProblem::isReady_St() const
{
	return this->m_isReady_St;
}

bool ModelDesignProblem::isReady_DfDx() const
{
	return this->m_isReady_DfDx;
}

bool ModelDesignProblem::isReady_DfDp() const
{
	bool isParReady = true;
	for (int i = 0; i < this->m_ns; ++i)
		isParReady &= this->m_vpset[i]->isReady_DfDp();

	if (!isParReady)
		return false;

	return this->m_isReady_DfDp;
}

bool ModelDesignProblem::isReady_DxDp() const
{
	return this->isReady_DfDp() && this->m_isReady_DxDp;
}

bool ModelDesignProblem::isReady_DxDp2() const
{
	return this->isReady_DfDp() && this->m_isReady_DxDp2;
}

void ModelDesignProblem::setDOFFixed(const iVector& vidx)
{
	assert((int)vidx.size() <= this->m_pModel->getNumRawDOF());

	this->m_vDOFFixedRaw = vidx;

	// Build stencil

	const iVector& vsimIdx = this->m_pModel->getRawToSim_x();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();
	this->m_vFixedStencil.assign(nsimDOF, false);

	for (int i = 0; i < (int) this->m_vDOFFixedRaw.size(); ++i)
	{
		if (vsimIdx[this->m_vDOFFixedRaw[i]] == -1)
			continue; // This DOF is not simulated

		m_vFixedStencil[vsimIdx[this->m_vDOFFixedRaw[i]]] = true;
	}

	this->m_isReady_DfDx = false;
	this->m_isReady_DfDp = false;
}

void ModelDesignProblem::setDOFTargets(const iVector& vidx)
{
	assert((int)vidx.size() <= this->m_pModel->getNumRawDOF());

	this->m_vDOFTargetRaw = vidx;

	// Build stencil

	const iVector& vsimIdx = this->m_pModel->getRawToSim_x();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();
	this->m_vTargetStencil.assign(nsimDOF, false);

	for (int i = 0; i < (int) this->m_vDOFTargetRaw.size(); ++i)
	{
		if (vsimIdx[this->m_vDOFTargetRaw[i]] == -1)
			continue; // This DOF is not simulated

		m_vTargetStencil[vsimIdx[this->m_vDOFTargetRaw[i]]] = true;
	}

	this->m_isReady_St = false;
}

const iVector& ModelDesignProblem::getDOFFixed() const
{
	return this->m_vDOFFixedRaw;
}

const iVector& ModelDesignProblem::getDOFTargets() const
{
	return this->m_vDOFTargetRaw;
}

void ModelDesignProblem::update_St()
{
	if (this->isReady_St())
		return; // Updated

	tVector vSs;

	int ntargetDOFRaw = (int) this->m_vDOFTargetRaw.size();

	if (ntargetDOFRaw != 0)
	{
		for (int i = 0; i < this->m_nx; ++i)
		{
			if (this->m_vTargetStencil[i]) // Target?
				vSs.push_back(Triplet<Real>(i, i, 1));
		}
	}

	int ntargetDOFSim = (int) vSs.size();
	
	if (ntargetDOFSim > 0)
	{
		this->m_mSs = MatrixSd(ntargetDOFSim, this->m_nx);
		this->m_mSs.setFromTriplets(vSs.begin(), vSs.end());
		this->m_mSs.makeCompressed();
	}
	else
	{
		this->m_mSs.setZero();
	}

	//writeToFile(this->m_mSs, "S.csv");

	logSimu("[INFO] Updating S");
}

void ModelDesignProblem::update_DfDx()
{
	if (this->isReady_DfDx())
		return; // Updated

	tVector vDfDxs;

	this->m_pModel->addJacobian(vDfDxs);
	this->m_pSolver->addBoundaryJacobian(
		this->m_pModel->getPositions_x(),
		this->m_pModel->getVelocities_x(),
		vDfDxs);

	// Fix variables

	this->removeRows(vDfDxs, this->m_vFixedStencil);
	this->removeCols(vDfDxs, this->m_vFixedStencil);
	this->addDiagonal(vDfDxs, this->m_vFixedStencil, -1.0);

	this->m_mDfDxs = MatrixSd(this->m_nx, this->m_nx);
	this->m_mDfDxs.setFromTriplets(vDfDxs.begin(), vDfDxs.end());
	this->m_mDfDxs.makeCompressed();

	this->m_mDfDxs = this->m_mDfDxs.selfadjointView<Lower>();

	this->m_isReady_DfDx = true;
	this->m_isReady_DxDp = false;

	//writeToFile(this->m_mDfDxs, "DfDx.csv");

	logSimu("[INFO] Updating DfDx");
}

void ModelDesignProblem::update_DfDp()
{
	if (this->isReady_DfDp())
		return; // Updated

	tVector vDfDps;

	for (int i = 0; i < this->m_ns; ++i)
	{
		tVector vDfDpi;
		this->m_vpset[i]->get_DfDp(vDfDpi);
		int ncoefficients = (int)vDfDpi.size();

		// First extend the previous capacity of the vector
		vDfDps.reserve((int)vDfDps.size() + ncoefficients);

		for (int j = 0; j < ncoefficients; ++j) // Append coeficients
			vDfDps.push_back(Triplet<Real>(vDfDpi[j].row(), vDfDpi[j].col() + this->m_vsetOff[i], vDfDpi[j].value()));
	}

	// Fix variables

	this->removeRows(vDfDps, this->m_vFixedStencil);
	this->removeCols(vDfDps, this->m_vParamStencil);

	this->m_mDfDps = MatrixSd(this->m_nx, this->m_np);
	this->m_mDfDps.setFromTriplets(vDfDps.begin(), vDfDps.end());
	this->m_mDfDps.makeCompressed();

	this->m_isReady_DfDp = true;
	this->m_isReady_DxDp = false;

	//writeToFile(this->m_mDfDps, "DfDp.csv");

	logSimu("[INFO] Updating DfDp");
}

void ModelDesignProblem::update_DxDp()
{
	if (this->isReady_DxDp())
		return; // Updated

	// Dependent on DfDp
	if (!this->isReady_DfDp())
		this->update_DfDp();

	// Create DfxDx

	MatrixSd mDfDxs = this->get_DfDx();
	MatrixSd mDfDps = this->get_DfDp();

	mDfDxs *= -1.0;

	MatrixXd mDxDp;

	PhysSolver::SolveLinearSystem(mDfDxs, mDfDps, mDxDp);

	// Work around: convert to sparse

	int numCoeff = mDxDp.rows()*mDxDp.cols();
	tVector vDxDp;
	vDxDp.reserve(numCoeff);
	for (int i = 0; i < mDxDp.rows(); ++i)
		for (int j = 0; j < mDxDp.cols(); ++j)
			vDxDp.push_back(Triplet<Real>(i, j, mDxDp(i, j)));
	this->m_mDxDps = MatrixSd(mDxDp.rows(), mDxDp.cols());
	this->m_mDxDps.setFromTriplets(vDxDp.begin(), vDxDp.end());
	this->m_mDxDps.makeCompressed();

	this->m_isReady_DxDp = true;
	this->m_isReady_DxDp2 = false;

	//writeToFile(this->m_mDxDpsRaw, "DxDpRaw.csv");

	logSimu("[INFO] Updating DxDp");
}

void ModelDesignProblem::update_DxDp2()
{
	if (this->isReady_DxDp2())
		return; // Updated

	MatrixSd mDxDpS = this->get_DxDp()*this->get_St();

	this->m_mDxDp2s = mDxDpS.transpose()*mDxDpS;

	this->m_isReady_DxDp2 = true;

	//writeToFile(this->m_mDxDp2s, "testDxDp2.csv");

	logSimu("[INFO] Updating DxDp2");
}

void ModelDesignProblem::addDiagonal(tVector& vM, const bVector& vstencil, Real value)
{
	int N = (int)vstencil.size();
	for (int i = 0; i < N; ++i)
	{
		if (!vstencil[i])
			continue;

		vM.push_back(Triplet<Real>(i, i, value));
	}
}

void ModelDesignProblem::removeCols(tVector& vM, const bVector& vstencil)
{
	int ncoeff = (int)vM.size();
	for (int i = 0; i < ncoeff; ++i)
	{
		if (vstencil[vM[i].col()])
			vM[i] = Triplet<Real>(vM[i].row(), vM[i].col(), 0.0);
	}
}

void ModelDesignProblem::removeRows(tVector& vM, const bVector& vstencil)
{
	int ncoeff = (int)vM.size();
	for (int i = 0; i < ncoeff; ++i)
	{
		if (vstencil[vM[i].row()])
			vM[i] = Triplet<Real>(vM[i].row(), vM[i].col(), 0.0);
	}
}