/*=====================================================================================*/
/*!
/file		DECModel.h
/author		jesusprod
/brief		DECModel.h implementation.
*/
/*=====================================================================================*/

#ifndef DEC_MODEL_H
#define DEC_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Model/DERModel.h>
#include <JSandbox/Model/RodMeshModel.h>

#include <JSandbox/Element/DERConnectionElement.h>

class   DECState : public SolidState
{
public:
	ConState m_con;
	Matrix3d m_R0x;

	DECState()
	{
		m_R0x.setZero();
	}

	virtual ~DECState()
	{
		// Nothing to do here...
	}

	DECState(const DECState& toCopy) : SolidState(toCopy)
	{
		m_con = toCopy.m_con;
		m_R0x = toCopy.m_R0x;
	}

	virtual pSolidState clone() const
	{
		DECState* pcloned = new DECState();
		*pcloned = *this;

		return pSolidState(pcloned);
	}
};

typedef std::shared_ptr<DECState> pDECState;

class RodMeshModel;

class   DECModel : public SolidModel
{
public:
	DECModel();

	virtual ~DECModel();

	/* From SolidModel */

	virtual void setup();

	virtual void setState_x(pSolidState s);
	virtual void setState_0(pSolidState s);
	virtual void setPositions_x(const VectorXd& vx);
	virtual void setPositions_0(const VectorXd& v0);

	virtual void update_Rest();

	virtual void update_Force();
	virtual void update_Energy();
	virtual void update_Jacobian();

	virtual void update_EnergyForce();
	virtual void update_ForceJacobian();
	virtual void update_EnergyForceJacobian();

	void deprecateDiscretization();
	void deprecateDeformation();

	/* From SolidModel */

	// BASIC

	virtual void setParam_RestTangents(const vector<Vector3d>&);
	virtual void getParam_RestTangents(vector<Vector3d>&) const;

	virtual void setParam_BendingK(const vector<VectorXd>&);
	virtual void getParam_BendingK(vector<VectorXd>&) const;

	virtual void setParam_TwistK(const vector<VectorXd>&);
	virtual void getParam_TwistK(vector<VectorXd>&) const;

	// BASIC

	void setStateToMesh_x(const Matrix3d& rot);
	void setStateToMesh_0(const Matrix3d& rot);

	virtual void configureConnection(const Con& con, const Matrix3d& rot, const vector<DERModel*> vpRodModels);

	virtual void updateVertexRest();
	virtual void updateRadiusEdge();
	virtual void updateLengthEdge();

	virtual int getNumNonZeros_DfxD0v() { if (this->m_nNZ_DfxD0v == -1) this->precompute_DfxD0v(); return this->m_nNZ_DfxD0v; }
	virtual int getNumNonZeros_DfxDl() { if (this->m_nNZ_DfxDl == -1) this->precompute_DfxDl(); return this->m_nNZ_DfxDl; }
	virtual int getNumNonZeros_DfxDr() { if (this->m_nNZ_DfxDr == -1) this->precompute_DfxDr(); return this->m_nNZ_DfxDr; }

	virtual bool isReady_f0v() const { return this->m_isReady_f0v; }
	virtual bool isReady_fl() const { return this->m_isReady_fl; }
	virtual bool isReady_fr() const { return this->m_isReady_fr; }

	virtual bool isReady_DfxD0v() const { return this->m_isReady_DfxD0v; }
	virtual bool isReady_DfxDl() const { return this->m_isReady_DfxDl; }
	virtual bool isReady_DfxDr() const { return this->m_isReady_DfxDr; }

	virtual void update_fv();
	virtual void update_fl();
	virtual void update_fr();

	virtual void update_DfxD0v();
	virtual void update_DfxDl();
	virtual void update_DfxDr();

	virtual void add_f0v(VectorXd& vfv);
	virtual void add_fl(VectorXd& vfl);
	virtual void add_fr(VectorXd& vfr);

	virtual void add_DfxD0v(tVector& vJ);
	virtual void add_DfxDl(tVector& vJ);
	virtual void add_DfxDr(tVector& vJ);

	DERModel* getRodModel(int i) { return this->m_vprodModel[i]; }

	virtual int getNumConEle() const { return (int) this->m_vpConEles.size(); }

	virtual const vector<DERConnectionElement*> getConnectionElements() const { return this->m_vpConEles; }

	virtual const Con& getConnection() const { return this->m_connection; }
	virtual const ConState& getConState_x() const { return this->m_stateDEC_x->m_con; }
	virtual const ConState& getConState_0() const { return this->m_stateDEC_0->m_con; }
	virtual const Matrix3d& getRotation() const { return this->m_stateDEC_x->m_R0x; }

	virtual void update_SimDOF();

protected:

	virtual void precompute_DfxD0v();
	virtual void precompute_DfxDl();
	virtual void precompute_DfxDr();

	virtual void freeElements();

	virtual void updateConnectionToState(ConState& CS, VectorXd& vx);
	virtual void updateStateToConnection(VectorXd& vx, ConState& CS);
	virtual void resetReference(ConState& CS);
	virtual void resetRotations(Matrix3d& mR);
	virtual void resetBasicRotation();
	virtual void resetEulerRotation();
	virtual void resetRotatedFrames();

protected:

	vector<DERConnectionElement*> m_vpConEles;

	vector<DERModel*> m_vprodModel;

	int m_nNZ_DfxD0v;
	int m_nNZ_DfxDl;
	int m_nNZ_DfxDr;

	bool m_isReady_f0v;
	bool m_isReady_fl;
	bool m_isReady_fr;

	bool m_isReady_DfxD0v;
	bool m_isReady_DfxDl;
	bool m_isReady_DfxDr;

	pDECState m_stateDEC_x;
	pDECState m_stateDEC_0;

	Con m_connection;

};

#endif