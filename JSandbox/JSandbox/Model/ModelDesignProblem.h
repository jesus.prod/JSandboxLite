/*=====================================================================================*/
/*!
/file		ModelDesignProblem.h
/author		jesusprod
/brief		TODO
*/
/*=====================================================================================*/

#ifndef MODEL_DESIGN_PROBLEM_H
#define MODEL_DESIGN_PROBLEM_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Model/SolidModel.h>
#include <JSandbox/Solver/PhysSolver.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/CustomTimer.h>

class ParameterSet;

class ModelDesignProblem
{
public:
	ModelDesignProblem(const string& ID);

	virtual ~ModelDesignProblem();

	virtual void setModel(SolidModel* pModel);
	virtual void setSolver(PhysSolver* pSolver);
	virtual SolidModel* getModel() const;
	virtual PhysSolver* getSolver() const;

	virtual void setParameterSets(const vector<ParameterSet*>& vs);
	virtual const vector<ParameterSet*>& getParameterSets() const;

	virtual int getParNumber() const { return this->m_np; }
	virtual int getSetNumber() const { return this->m_ns; };

	virtual void setDOFFixed(const iVector& vi);
	virtual const iVector& getDOFFixed() const;

	virtual void setDOFTargets(const iVector& vi);
	virtual const iVector& getDOFTargets() const;

	virtual bool isReady_St() const;
	virtual bool isReady_DfDx() const;
	virtual bool isReady_DfDp() const;
	virtual bool isReady_DxDp() const;
	virtual bool isReady_DxDp2() const;

	virtual void update_St();
	virtual void update_DfDx();
	virtual void update_DfDp();
	virtual void update_DxDp();
	virtual void update_DxDp2();

	virtual const MatrixSd& get_St();
	virtual const MatrixSd& get_DfDx();
	virtual const MatrixSd& get_DfDp();
	virtual const MatrixSd& get_DxDp();
	virtual const MatrixSd& get_DxDp2();

	virtual VectorXd randomizeParameters();

	virtual const VectorXd& getParametersLowerBound() const;
	virtual const VectorXd& getParametersUpperBound() const;

	virtual const VectorXd& getParameters();
	virtual void setParameters(const VectorXd& vp);

	virtual void capParameterValue(VectorXd& vp);
	virtual void capParameterStep(VectorXd& vp);

	virtual bool isValidParameters(const VectorXd& vp, VectorXd& vpBD, double tol = EPS_APPROX);

	virtual void setup();

	virtual void updateBoundsVector();

	virtual void testParameterLinearization();

	virtual Real Goal_computeObjective(const VectorXd& vt);
	virtual void Goal_computeGradient(const VectorXd& vt, VectorXd& vgGaol);
	virtual void Goal_computeHessian(const VectorXd& vt, MatrixSd& mHGoal);

	virtual Real computeObjective(const VectorXd& vt);
	virtual void computeDefaultGradient(const VectorXd& vt, VectorXd& vgDef);
	virtual void computeBoundedGradient(const VectorXd& vt, VectorXd& vgCon);
	virtual void computeHessian(const VectorXd& vt, MatrixSd& mH);

	virtual void computeDefaultGradient_FD(const VectorXd& vt, VectorXd& vgDef);
	virtual void computeBoundedGradient_FD(const VectorXd& vt, VectorXd& vgCon);

	virtual void testObjectiveGradient(const VectorXd& vt);

	virtual bool computeParameterStep_CQP(const VectorXd& vt, VectorXd& vdp, string& error);
	virtual bool computeParameterStep_UQP_ProjectStep(const VectorXd& vt, VectorXd& vdp, string& error);
	virtual bool computeParameterStep_UQP_ProjectGrad(const VectorXd& vt, VectorXd& vdp, string& error);

protected:

	virtual void addDiagonal(tVector& vM, const bVector& vstencil, Real value);
	virtual void removeCols(tVector& vM, const bVector& vstencil);
	virtual void removeRows(tVector& vM, const bVector& vstencil);

	virtual void freeParameters();

protected:

	string m_ID;

	int m_ns;
	int m_np;
	int m_nx;

	VectorXd m_vp;
	VectorXd m_vpUB;
	VectorXd m_vpLB;

	iVector m_vsetOff;

	vector<ParameterSet*> m_vpset;

	iVector m_vDOFTargetRaw;
	iVector m_vDOFFixedRaw;

	bVector m_vTargetStencil;
	bVector m_vFixedStencil;
	bVector m_vParamStencil;

	MatrixSd m_mpS;

	MatrixSd m_mDfDxs;
	MatrixSd m_mDfDps;
	MatrixSd m_mDxDps;
	MatrixSd m_mDxDp2s;
	MatrixSd m_mSs;

	SolidModel* m_pModel;
	PhysSolver* m_pSolver;

	bool m_isReady_DfDx;
	bool m_isReady_DfDp;
	bool m_isReady_DxDp;
	bool m_isReady_DxDp2;
	bool m_isReady_St;

};

#endif