/*=====================================================================================*/
/*!
/file		RodMeshModel.cpp
/author		jesusprod
/brief		Implementation of RodMeshModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/RodMeshModel.h>

#include <JSandbox/Model/DERModel.h>

#ifdef _OPENMP 
#include <omp.h>
#endif

RodMeshModel::RodMeshModel() : CompositeModel("RM")
{
	this->deprecateConfiguration();
}

RodMeshModel::~RodMeshModel()
{
	logSimu("[INFO] Destroying RodMeshModel: %u", this);
}

void RodMeshModel::initializeMaterials()
{
	// Nothing to do here
}

void RodMeshModel::deprecateDiscretization()
{
	CompositeModel::deprecateDiscretization();

	this->m_nNZ_DfxDre = -1;
	this->m_nNZ_DfxDrr = -1;
	this->m_nNZ_DfxDle = -1;
	this->m_nNZ_DfxDlr = -1;
	this->m_nNZ_DfxD0v = -1;
}

void RodMeshModel::setup()
{
	// Initialize rod meshes

	int nr = this->getNumRod();
	int nc = this->getNumCon();

	this->m_vprodModel.clear();
	this->m_vpconModel.clear();

	vector<SolidModel*> vpModels;
	vpModels.reserve(nr + nc);

	RodMesh& mesh = m_rodMesh_x;

	// Initialize rod and connections models

	for (int ridx = 0; ridx < nr; ++ridx)
	{
		DERModel* pModel = new DERModel();
		pModel->setGravity(this->getGravity());
		pModel->configureMaterial(this->m_material);
		pModel->configureRod(m_rodMesh_x.getRod(ridx),
							 m_rodMesh_0.getRod(ridx));
		pModel->setup();

		this->m_vprodModel.push_back(pModel);
		vpModels.push_back(pModel); // Common
	}

	for (int cidx = 0; cidx < nc; ++cidx)
	{
		DECModel* pModel = new DECModel();
		pModel->setGravity(this->getGravity());
		pModel->configureMaterial(this->m_material);
		pModel->configureConnection(mesh.getCon(cidx), 
									mesh.getRot(cidx), 
									this->m_vprodModel);
		pModel->setup();

		this->m_vpconModel.push_back(pModel);
		vpModels.push_back(pModel); // Common
	}

	// Create couplings rod-
	vector<ModelCoupling*> vpCouplings;

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		const Con con = this->getConModel(i)->getConnection();

		int nrCon = (int)con.m_srods.size();
		set<pair<int, int>>::iterator s_end = con.m_srods.end();
		set<pair<int, int>>::iterator s_begin = con.m_srods.begin();

		int conModelIdx = nr + i;

		for (set<pair<int, int>>::iterator s_it0 = s_begin; s_it0 != s_end; ++s_it0)
		{
			set<pair<int, int>>::iterator s_it1 = s_it0;
			s_it1++;
			for (; s_it1 != s_end; ++s_it1)
			{
				int ridx0 = (*s_it0).first;
				int side0 = (*s_it0).second;
				int ridx1 = (*s_it1).first;
				int side1 = (*s_it1).second;
				int rodModelIdx0 = ridx0;
				int rodModelIdx1 = ridx1;

				DERModel* pRodModel0 = this->getRodModel(ridx0);
				DERModel* pRodModel1 = this->getRodModel(ridx1);

				int rodNode0Idx = -1;
				int rodNode1Idx = -1;
				int nv0 = pRodModel0->getRod_x().getNumNode();
				int nv1 = pRodModel1->getRod_x().getNumNode();

				if (side0 != 0 && side0 != 1)
				{
					logSimu("[WARNING] Invalid side identifier creating coupling point: %d", side0);
				}

				if (side1 != 0 && side1 != 1)
				{
					logSimu("[WARNING] Invalid side identifier creating coupling point: %d", side1);
				}

				if (nv0 < 2)
				{
					logSimu("[WARNING] Not enough rod nodes");
				}

				if (nv1 < 2)
				{
					logSimu("[WARNING] Not enough rod nodes");
				}

				if (side0 == 0)
				{
					rodNode0Idx = 0;
				}
				if (side0 == 1)
				{
					rodNode0Idx = 3 * nv0 - 3;
				}
				if (side1 == 0)
				{
					rodNode1Idx = 0;
				}
				if (side1 == 1)
				{
					rodNode1Idx = 3 * nv1 - 3;
				}

				if (rodNode0Idx < 0)
				{
					logSimu("[WARNING] Negative coupling index at rod-rod");
				}

				if (rodNode1Idx < 0)
				{
					logSimu("[WARNING] Negative coupling index at rod-rod");
				}

				ModelCoupling* pCoupling;

				// Add identity coupling for extremes

				pCoupling = new ModelCoupling();
				pCoupling->setModel0(rodModelIdx0);
				pCoupling->setModel1(rodModelIdx1);
				pCoupling->setModelPoint0(ModelPoint(3, rodNode0Idx));
				pCoupling->setModelPoint1(ModelPoint(3, rodNode1Idx));
				pCoupling->setType(ModelCoupling::Type::Shared);
				vpCouplings.push_back(pCoupling);
			}
		}
	}

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		const Con con = this->getConModel(i)->getConnection();

		int nrCon = (int)con.m_srods.size();
		set<pair<int, int>>::iterator s_end = con.m_srods.end();
		set<pair<int, int>>::iterator s_begin = con.m_srods.begin();

		int conModelIdx = nr + i;

		int j = 0;

		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
		{
			int ridx = (*s_it).first;
			int side = (*s_it).second;

			int rodModelIdx = ridx;

			DERModel* pRodModel = this->getRodModel(ridx);

			int rodNode0Idx = -1;
			int rodNode1Idx = -1;
			int rodAngleIdx = -1;
			int nv = pRodModel->getRod_x().getNumNode();
			int ne = pRodModel->getRod_x().getNumEdge();
			if (side == 0)
			{
				rodNode0Idx = 0;
				rodNode1Idx = 3;
				rodAngleIdx = 3 * nv + 0;
			}
			if (side == 1)
			{
				rodNode0Idx = 3 * nv - 3;
				rodNode1Idx = 3 * nv - 6;
				rodAngleIdx = 3 * nv + ne - 1;
			}

			int conNode0Idx = 6 * j + 0;
			int conNode1Idx = 6 * j + 3;
			int conAngleIdx = 6 * nrCon + j;

			if (rodNode0Idx < 0)
			{
				logSimu("[WARNING] Negative coupling index at rod-con");
			}

			if (rodNode1Idx < 0)
			{
				logSimu("[WARNING] Negative coupling index at rod-con");
			}

			if (conNode0Idx < 0)
			{
				logSimu("[WARNING] Negative coupling index at rod-con");
			}

			if (conNode1Idx < 0)
			{
				logSimu("[WARNING] Negative coupling index at rod-con");
			}

			ModelCoupling* pCoupling;

			// Add itentity coupling for point 0

			pCoupling = new ModelCoupling();
			pCoupling->setModel0(conModelIdx);
			pCoupling->setModel1(rodModelIdx);
			pCoupling->setModelPoint0(ModelPoint(3, conNode0Idx));
			pCoupling->setModelPoint1(ModelPoint(3, rodNode0Idx));
			pCoupling->setType(ModelCoupling::Type::Shared);
			vpCouplings.push_back(pCoupling);

			// Add itentity coupling for point 1

			pCoupling = new ModelCoupling();
			pCoupling->setModel0(conModelIdx);
			pCoupling->setModel1(rodModelIdx);
			pCoupling->setModelPoint0(ModelPoint(3, conNode1Idx));
			pCoupling->setModelPoint1(ModelPoint(3, rodNode1Idx));
			pCoupling->setType(ModelCoupling::Type::Shared);
			vpCouplings.push_back(pCoupling);

			// Add itentity coupling for angle

			pCoupling = new ModelCoupling();
			pCoupling->setModel0(conModelIdx);
			pCoupling->setModel1(rodModelIdx);
			pCoupling->setModelPoint0(ModelPoint(1, conAngleIdx));
			pCoupling->setModelPoint1(ModelPoint(1, rodAngleIdx));
			pCoupling->setType(ModelCoupling::Type::Shared);
			vpCouplings.push_back(pCoupling);
		}
	}

	CompositeModel::configureModels(vpModels, vpCouplings);

	CompositeModel::setup();

	logSimu("[INFO] Initialized RodMeshModel. Raw: %u, Sim: %u", this->m_nrawDOF, this->m_nsimDOF_x);
}

void RodMeshModel::setStateToMesh_x(const RodMesh& mesh)
{
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		this->getRodModel(i)->setStateToMesh_x(mesh.getRod(i));
	}

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		this->getConModel(i)->setStateToMesh_x(mesh.getRot(i));
	}

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		this->mapLocal2GlobalSim_x(i, this->m_vpModels[i]->getPositions_x(), this->m_state_x->m_vx);
	}

	this->deprecateDeformation();
}

void RodMeshModel::setStateToMesh_0(const RodMesh& mesh)
{
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		for (int i = 0; i < this->getNumRod(); ++i)
		{
			this->getRodModel(i)->setStateToMesh_0(mesh.getRod(i));
		}

		for (int i = 0; i < this->getNumCon(); ++i)
		{
			this->getConModel(i)->setStateToMesh_0(mesh.getRot(i));
		}
	}

	this->deprecateUndeformed();
}

DERModel* RodMeshModel::getRodModel(int i) 
{
	assert(i >= 0 && i < this->getNumRod()); 

	return this->m_vprodModel[i];
}

DECModel* RodMeshModel::getConModel(int i)
{
	assert(i >= 0 && i < this->getNumCon()); 

	return this->m_vpconModel[i];
}

void RodMeshModel::freeElements()
{
	CompositeModel::freeElements();
}

void RodMeshModel::configureMesh(const RodMesh& rodMesh_x, const RodMesh& rodMesh_0)
{
	this->m_rodMesh_x = rodMesh_x;
	this->m_rodMesh_0 = rodMesh_0;

	for (int i = 0; i < this->m_rodMesh_x.getNumCon(); ++i)
	{
		set<pair<int, int>>::iterator s_end = m_rodMesh_x.getCon(i).m_srods.end();
		set<pair<int, int>>::iterator s_begin = m_rodMesh_x.getCon(i).m_srods.begin();
		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; s_it++)
		{
			if ((*s_it).second != 0 && (*s_it).second != 1)
			{
				logSimu("[WARNING] Invalid side identifier: %d", (*s_it).second);
			}
		}
	}

	for (int i = 0; i < this->m_rodMesh_0.getNumCon(); ++i)
	{
		set<pair<int, int>>::iterator s_end = m_rodMesh_0.getCon(i).m_srods.end();
		set<pair<int, int>>::iterator s_begin = m_rodMesh_0.getCon(i).m_srods.begin();
		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; s_it++)
		{
			if ((*s_it).second != 0 && (*s_it).second != 1)
			{
				logSimu("[WARNING] Invalid side identifier: %d", (*s_it).second);
			}
		}
	}

	this->m_vpconModel.clear();
	this->m_vprodModel.clear();

	this->deprecateConfiguration();
}

void RodMeshModel::update_SimMesh()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mesh)
		return; // Already done

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];
		this->m_rodMesh_0.setRod(i, pRodModel->getRod_0(), ""); // Update with model
		this->m_rodMesh_x.setRod(i, pRodModel->getRod_x(), ""); // Update with model
	}

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		DECModel* pConModel = this->m_vpconModel[i];

		Matrix3d RTot;
		Matrix3d RSum = pConModel->getRotation();
		Vector3d euler = pConModel->getConState_x().m_vR;
		eulerRotation(euler.data(), RSum.col(0).data(), RTot.col(0).data());
		eulerRotation(euler.data(), RSum.col(1).data(), RTot.col(1).data());
		eulerRotation(euler.data(), RSum.col(2).data(), RTot.col(2).data());
		this->m_rodMesh_x.setRot(i, RTot);
	}

	this->m_isReady_Mesh = true;
}

int RodMeshModel::getNumNodeRaw() const
{
	int numNode = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
		numNode += this->getSimRod_x(i).getNumNode();
	return numNode;
}

int RodMeshModel::getNumEdgeRaw() const
{
	int numEdge = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
		numEdge += this->getSimRod_x(i).getNumEdge();
	return numEdge;
}

iVector RodMeshModel::getNodeRawDOF(int inode) const
{
	iVector vidx;

	int nodeSum = 0;

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		const Rod& rod0 = this->getSimRod_0(i);

		int rodNodeNum = rod0.getNumNode();
		if (inode < nodeSum + rodNodeNum) // Is in this?
		{
			int inodeLocal = inode - nodeSum;
			vidx.push_back(this->m_vmodelRawIdx[i] + 3 * inodeLocal + 0);
			vidx.push_back(this->m_vmodelRawIdx[i] + 3 * inodeLocal + 1);
			vidx.push_back(this->m_vmodelRawIdx[i] + 3 * inodeLocal + 2);
			return vidx; // Found
		}

		nodeSum += rodNodeNum;
	}
	
	assert(false); // Not found

	return vidx;
}

iVector RodMeshModel::getEdgeRawDOF(int iedge) const
{
	iVector vidx;

	int edgeSum = 0;

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		const Rod& rod0 = this->getSimRod_0(i);

		int rodNodeNum = rod0.getNumNode();
		int rodEdgeNum = rod0.getNumEdge();
		if (iedge < edgeSum + rodEdgeNum) // Is in this?
		{
			int iedgeLocal = iedge - edgeSum;
			vidx.push_back(this->m_vmodelRawIdx[i] + 3 * rodNodeNum + iedgeLocal);
			return vidx; // Found
		}

		edgeSum += rodEdgeNum;
	}

	assert(false); // Not found

	return vidx;
}

iVector RodMeshModel::getNodeRawDOF(int irod, int inode) const
{
	assert(irod >= 0 && irod < this->getNumRod());

	const Rod& rod0 = this->getSimRod_0(irod);

	assert(inode >= 0 && inode < rod0.getNumNode());

	iVector vidx;
	vidx.push_back(this->m_vmodelRawIdx[irod] + 3 * inode + 0);
	vidx.push_back(this->m_vmodelRawIdx[irod] + 3 * inode + 1);
	vidx.push_back(this->m_vmodelRawIdx[irod] + 3 * inode + 2);
	return vidx;
}

iVector RodMeshModel::getEdgeRawDOF(int irod, int iedge) const
{
	assert(irod >= 0 && irod < this->getNumRod());

	const Rod& rod0 = this->getSimRod_0(irod);

	assert(iedge >= 0 && iedge < rod0.getNumEdge());

	iVector vidx;
	int numn = rod0.getNumNode();
	vidx.push_back(this->m_vmodelRawIdx[irod] + 3*numn + iedge);
	return vidx;
}

void RodMeshModel::precompute_DfxD0v()
{
	tVector testJ;
	this->add_DfxD0v(testJ); // Precompute
	this->m_nNZ_DfxD0v = (int)testJ.size();
}

void RodMeshModel::precompute_DfxDle()
{
	tVector testJ;
	this->add_DfxDle(testJ); // Precompute
	this->m_nNZ_DfxDle = (int)testJ.size();
}

void RodMeshModel::precompute_DfxDlr()
{
	tVector testJ;
	this->add_DfxDlr(testJ); // Precompute
	this->m_nNZ_DfxDlr = (int)testJ.size();
}

void RodMeshModel::precompute_DfxDrr()
{
	tVector testJ;
	this->add_DfxDrr(testJ); // Precompute
	this->m_nNZ_DfxDrr = (int)testJ.size();
}

void RodMeshModel::precompute_DfxDre()
{
	tVector testJ;
	this->add_DfxDre(testJ); // Precompute
	this->m_nNZ_DfxDre = (int)testJ.size();
}

bool RodMeshModel::isReady_f0v() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_f0v())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_f0v())
			return false;
	return true;
}

bool RodMeshModel::isReady_fle() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_fle())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_fl())
			return false;
	return true;
}

bool RodMeshModel::isReady_flr() const
{
	for (int i = 0; i < this->getNumRod(); ++i) 
		if (!this->m_vprodModel[i]->isReady_flr())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_fl())
			return false;
	return true;
}

bool RodMeshModel::isReady_frr() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_frr())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_fr())
			return false;
	return true;
}

bool RodMeshModel::isReady_fre() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_fre())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_fr())
			return false;
	return true;
}

bool RodMeshModel::isReady_DfxD0v() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_DfxD0v())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_DfxD0v())
			return false;
	return true;
}

bool RodMeshModel::isReady_DfxDle() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_DfxDle())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_DfxDl())
			return false;
	return true;
}

bool RodMeshModel::isReady_DfxDlr() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_DfxDlr())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_DfxDl())
			return false;
	return true;
}

bool RodMeshModel::isReady_DfxDre() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_DfxDre())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_DfxDr())
			return false;
	return true;
}

bool RodMeshModel::isReady_DfxDrr() const
{
	for (int i = 0; i < this->getNumRod(); ++i)
		if (!this->m_vprodModel[i]->isReady_DfxDrr())
			return false;
	for (int i = 0; i < this->getNumCon(); ++i)
		if (!this->m_vpconModel[i]->isReady_DfxDr())
			return false;
	return true;
}

void RodMeshModel::getParam_VertexRest(VectorXd& v0vSplit) const
{
	const vector<iVector>& vsplitToWhole = this->m_rodMesh_0.getSplitToWholeMap();

	int nvSplit = (int)vsplitToWhole.size();

	v0vSplit.resize(3 * nvSplit);
	v0vSplit.setZero(); // Initial.

	int DOFCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		int nv = this->getSimRod_x(i).getNumNode();

		VectorXd v0v_i(3*nv);

		this->m_vprodModel[i]->getParam_VertexRest(v0v_i);

		for (int e = 0; e < 3*nv; ++e)
			v0vSplit(DOFCount++) = v0v_i[e];
	}
}

void RodMeshModel::getParam_RadiusEdge(VectorXd& vr) const
{
	int radiiCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
		radiiCount += this->getSimRod_x(i).getNumEdge();

	vr.resize(2*radiiCount);

	radiiCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		int ne = this->getSimRod_x(i).getNumEdge();

		vector<Vector2d> vri(ne);

		this->m_vprodModel[i]->getParam_RadiusEdge(vri);

		for (int e = 0; e < ne; ++e)
		{
			vr(radiiCount++) = vri[e].x();
			vr(radiiCount++) = vri[e].y();
		}
	}
}

void RodMeshModel::getParam_RadiusRod(VectorXd& vr) const
{
	vr.resize(2*this->getNumRod());

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		Vector2d vri;
		this->m_vprodModel[i]->getParam_RadiusRod(vri);
		vr(2*i + 0) = vri.x();
		vr(2*i + 1) = vri.y();
	}
}

void RodMeshModel::setParam_VertexRest(const VectorXd& v0vSplit)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	const vector<iVector>& vsplitToWhole = this->m_rodMesh_0.getSplitToWholeMap();

	int nvSplit = (int) vsplitToWhole.size();
	assert((int)v0vSplit.size() == 3*nvSplit);

	int DOFCount = 0;

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		int nv = this->getSimRod_x(i).getNumNode();

		int DOFCount_i = 3*nv;
		VectorXd v0v_i(DOFCount_i);
		for (int e = 0; e < DOFCount_i; ++e)
			v0v_i(e) = v0vSplit(DOFCount++);

		this->m_vprodModel[i]->setParam_VertexRest(v0v_i);
	}

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		this->m_vpconModel[i]->updateVertexRest();
	}

	this->m_isReady_Mesh = false;
}

void RodMeshModel::setParam_RadiusEdge(const VectorXd& vr)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int radiiCount = 2*this->getSimMesh_0().getNumEdge();

	assert((int)vr.size() == radiiCount);

	radiiCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		int ne = this->getSimRod_x(i).getNumEdge();

		vector<Vector2d> vri(ne);

		for (int e = 0; e < ne; ++e)
		{
			// Both radii are equal
			Real rw = vr[radiiCount++];
			Real rh = vr[radiiCount++];
			vri[e] = Vector2d(rw, rh);
		}

		this->m_vprodModel[i]->setParam_RadiusEdge(vri);
	}

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		this->m_vpconModel[i]->updateRadiusEdge();
	}

	this->m_isReady_Mesh = false;
}

void RodMeshModel::setParam_RadiusRod(const VectorXd& vr)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int radiiCount = 2 * this->getSimMesh_0().getNumRod();

	assert((int)vr.size() == radiiCount);

	radiiCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		Vector2d vrRod;
		vrRod.x() = vr(radiiCount++);
		vrRod.y() = vr(radiiCount++);
		this->m_vprodModel[i]->setParam_RadiusRod(vrRod);
	}

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		this->m_vpconModel[i]->updateRadiusEdge();
	}

	this->m_isReady_Mesh = false;
}

void RodMeshModel::add_fv0(VectorXd& vf0)
{
	//throw new exception("[ERROR] Not implemented yet");

	//vf0.resize(3*this->getNumNodeRaw());
	//vf0.setZero(); // Initialize vector

	//int count = 0;

	//for (int i = 0; i < this->getNumRod(); ++i)
	//{
	//	DERModel* pRodModel = this->m_vprodModel[i];

	//	int nv = pRodModel->getRod_x().getNumNode();

	//	// Local force
	//	VectorXd vf0v_i(3*nv); 
	//	vf0v_i.setZero();

	//	pRodModel->add_fv(vf0v_i);

	//	for (int j = 0; j < 3*nv; ++j)
	//		vf0(count + j) = vf0v_i(j);

	//	count += 3*nv;
	//}
}

void RodMeshModel::add_fle(VectorXd& vfl)
{
	vfl.resize(this->getNumEdgeRaw());
	vfl.setZero(); // Initialize

	int count = 0;

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];

		int ne = pRodModel->getRod_x().getNumEdge();

		// Local force
		VectorXd vfli(ne);
		vfli.setZero();

		pRodModel->add_fle(vfli);

		for (int j = 0; j < ne; ++j)
			vfl(count + j) = vfli(j);

		count += ne;
	}
}

void RodMeshModel::add_flr(VectorXd& vfl)
{
	vfl.resize(this->getNumRod());
	vfl.setZero(); // Initialize

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];

		// Local force
		VectorXd vfli(1);
		vfli.setZero();

		pRodModel->add_flr(vfli); 

		vfl(i) = vfli(0); // Add
	}
}

void RodMeshModel::add_fre(VectorXd& vfr)
{
	vfr.resize(this->getNumEdgeRaw());
	vfr.setZero(); // Initialize

	int count = 0;

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];

		int ne = pRodModel->getRod_x().getNumEdge();

		// Local force
		VectorXd vfri(ne);
		vfri.setZero();

		pRodModel->add_fre(vfri);

		for (int j = 0; j < ne; ++j)
			vfr(count + j) = vfri(j);

		count += ne;
	}
}


void RodMeshModel::add_frr(VectorXd& vfr)
{
	vfr.resize(this->getNumEdgeRaw());
	vfr.setZero(); // Initialize

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];

		// Local force
		VectorXd vfri(1);
		vfri.setZero();

		pRodModel->add_frr(vfri);

		vfr(i) = vfri(0);
	}
}

void RodMeshModel::add_DfxD0v(tVector& vJ)
{
	int count = 0;

	// RODS

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];
		int nCoef = pRodModel->getNumNonZeros_DfxD0v();

		tVector vDfxD0v_i;
		vDfxD0v_i.reserve(nCoef);
		pRodModel->add_DfxD0v(vDfxD0v_i);

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real> t = vDfxD0v_i[j]; // Offset the rod parameters
			vDfxD0v_i[j] = Triplet<Real>(t.row(), count + t.col(), t.value());
		}

		// Accumulate number of previous parameters
		count += 3*this->getSimRod_x(i).getNumNode();

		this->addLocal2Global_Row(i, vDfxD0v_i, vJ);
	}

	int nr = this->getNumRod();

	// CONS

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		DECModel* pConModel = this->m_vpconModel[i];
		int nCoef = pConModel->getNumNonZeros_DfxD0v();

		tVector vDfxD0v_i;
		vDfxD0v_i.reserve(nCoef);
		pConModel->add_DfxD0v(vDfxD0v_i);

		const iVector& vmapCon2Rod = this->m_rodMesh_x.getConNodes(i);

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxD0v_i[j];

			int colCon = t.col(); // Map the connection parameter
			int col = 3 * vmapCon2Rod[colCon / 3] + (colCon % 3);
			vDfxD0v_i[j] = Triplet<Real>(t.row(), col, t.value());
		}

		this->addLocal2Global_Row(nr + i, vDfxD0v_i, vJ); // Transform to global DOF
	}
}

void RodMeshModel::add_DfxDle(tVector& vJ)
{
	int count = 0;

	// RODS

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];
		int nCoef = pRodModel->getNumNonZeros_DfxDle();

		tVector vDfxDl_i;
		vDfxDl_i.reserve(nCoef);
		pRodModel->add_DfxDle(vDfxDl_i);

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDl_i[j]; // Offset the rod parameters
			vDfxDl_i[j] = Triplet<Real>(t.row(), t.col() + count, t.value());
		}

		// Accumulate the number of previous edges
		count += this->getSimRod_x(i).getNumEdge();

		this->addLocal2Global_Row(i, vDfxDl_i, vJ); // Transform to global DOF
	}

	int nr = this->getNumRod();

	// CONS

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		DECModel* pConModel = this->m_vpconModel[i];
		int nCoef = pConModel->getNumNonZeros_DfxDl();

		tVector vDfxDl_i;
		vDfxDl_i.reserve(nCoef);
		pConModel->add_DfxDl(vDfxDl_i);

		const iVector& vmapCon2Rod = this->m_rodMesh_x.getConEdges(i);

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDl_i[j]; // Map the connection parameter
			vDfxDl_i[j] = Triplet<Real>(t.row(), vmapCon2Rod[t.col()], t.value());
		}

		this->addLocal2Global_Row(nr + i, vDfxDl_i, vJ); // Transform to global DOF
	}
}

void RodMeshModel::add_DfxDlr(tVector& vJ)
{
	// RODS

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];

		tVector vDfxDl_i;
		vDfxDl_i.reserve(pRodModel->getNumNonZeros_DfxDlr());
		pRodModel->add_DfxDlr(vDfxDl_i); // Get local Jacobian

		int nCoef = (int) vDfxDl_i.size();
		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDl_i[j]; // Offset rod parameters
			vDfxDl_i[j] = Triplet<Real>(t.row(), t.col() + i, t.value());
		}

		this->addLocal2Global_Row(i, vDfxDl_i, vJ); // Transform to global DOF
	}

	int nr = this->getNumRod();

	// CONS

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		DECModel* pConModel = this->m_vpconModel[i];
		int nCoef = pConModel->getNumNonZeros_DfxDl();

		tVector vDfxDl_i;
		vDfxDl_i.reserve(nCoef);
		pConModel->add_DfxDl(vDfxDl_i);

		iVector vmapRod;
		const Con& con = this->m_rodMesh_0.getCon(i);
		set<pair<int, int>>::const_iterator itCur = con.m_srods.begin();
		set<pair<int, int>>::const_iterator itEnd = con.m_srods.end();
		for (; itCur != itEnd; ++itCur)
			vmapRod.push_back(itCur->first);

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDl_i[j]; // Map the connection parameter
			int ne = this->getSimRod_0(vmapRod[t.col()]).getNumEdge(); // Average
			vDfxDl_i[j] = Triplet<Real>(t.row(), vmapRod[t.col()], t.value()/ne);
		}

		this->addLocal2Global_Row(nr + i, vDfxDl_i, vJ); // Transform to global DOF
	}
}

void RodMeshModel::add_DfxDre(tVector& vJ)
{
	int count = 0;

	// RODS

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];
		int nCoef = pRodModel->getNumNonZeros_DfxDre();

		tVector vDfxDr_i;
		vDfxDr_i.reserve(nCoef);
		pRodModel->add_DfxDre(vDfxDr_i); 

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDr_i[j]; // Offset the rod parameters
			vDfxDr_i[j] = Triplet<Real>(t.row(), t.col() + count, t.value());
		}

		// Accumulate the number of previous edges
		count += 2*this->getSimRod_x(i).getNumEdge();

		this->addLocal2Global_Row(i, vDfxDr_i, vJ); // Transform to global DOF
	}

	int nr = this->getNumRod();

	// CONS

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		DECModel* pConModel = this->m_vpconModel[i];
		int nCoef = pConModel->getNumNonZeros_DfxDr();

		tVector vDfxDr_i;
		vDfxDr_i.reserve(nCoef);
		pConModel->add_DfxDr(vDfxDr_i); // Get local Jacobian

		const iVector& vmapCon2Rod = this->m_rodMesh_x.getConEdges(i);

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDr_i[j];

			int conEdge = t.col() / 2; // Connection edge
			int edgeRad = t.col() % 2; // Rod edge radius

			vDfxDr_i[j] = Triplet<Real>(t.row(), 2*vmapCon2Rod[conEdge] + edgeRad, t.value());
		}

		this->addLocal2Global_Row(nr + i, vDfxDr_i, vJ); // Transform to global DOF
	}
}

void RodMeshModel::add_DfxDrr(tVector& vJ)
{
	// RODS

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		DERModel* pRodModel = this->m_vprodModel[i];
		int nCoef = pRodModel->getNumNonZeros_DfxDrr();

		tVector vDfxDr_i;
		vDfxDr_i.reserve(nCoef);
		pRodModel->add_DfxDrr(vDfxDr_i);

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDr_i[j]; // Offset rod parameters
			vDfxDr_i[j] = Triplet<Real>(t.row(), t.col() + 2*i, t.value());
		}

		this->addLocal2Global_Row(i, vDfxDr_i, vJ); // Transform to global DOF
	}

	int nr = this->getNumRod();

	// CONS

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		DECModel* pConModel = this->m_vpconModel[i];
		int nCoef = pConModel->getNumNonZeros_DfxDr();

		tVector vDfxDr_i;
		vDfxDr_i.reserve(nCoef);
		pConModel->add_DfxDr(vDfxDr_i);

		iVector vmapRod;
		const Con& con = this->m_rodMesh_0.getCon(i);
		set<pair<int, int>>::const_iterator itCur = con.m_srods.begin();
		set<pair<int, int>>::const_iterator itEnd = con.m_srods.end();
		for (; itCur != itEnd; ++itCur)
			vmapRod.push_back(itCur->first);

		for (int j = 0; j < nCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDr_i[j]; // Map the connection parameter

			int conRod = t.col() / 2;
			int rodRad = t.col() % 2;

			vDfxDr_i[j] = Triplet<Real>(t.row(), 2*vmapRod[conRod] + rodRad, t.value());
		}

		this->addLocal2Global_Row(nr + i, vDfxDr_i, vJ); // Transform to global DOF
	}
}

void RodMeshModel::compute_NodeStretchStrain(VectorXd& vsv)
{
	int nv = this->m_rodMesh_0.getNumNode();
	vsv.resize(nv);
	vsv.setZero();
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		VectorXd vsv_rod;
		this->m_vprodModel[i]->compute_NodeStretchStrain(vsv_rod);
		int nv_rod = (int) vsv_rod.size();
		for (int j = 0; j < nv_rod; ++j)
		{
			int nodeIdx;
			this->m_rodMesh_0.getMeshNodeForRodNode(j, i, nodeIdx);
			vsv(nodeIdx) = vsv_rod(j);
		}
	}
}

void RodMeshModel::compute_NodeExtensionStrain(VectorXd& vsv)
{
	int nv = this->m_rodMesh_0.getNumNode();
	vsv.resize(nv);
	vsv.setZero();
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		VectorXd vsv_rod;
		this->m_vprodModel[i]->compute_NodeExtensionStrain(vsv_rod);
		int nv_rod = (int)vsv_rod.size();
		for (int j = 0; j < nv_rod; ++j)
		{
			int nodeIdx;
			this->m_rodMesh_0.getMeshNodeForRodNode(j, i, nodeIdx);
			vsv(nodeIdx) = vsv_rod(j);
		}
	}
}

void RodMeshModel::compute_NodeCompressionStrain(VectorXd& vsv)
{
	int nv = this->m_rodMesh_0.getNumNode();
	vsv.resize(nv);
	vsv.setZero();
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		VectorXd vsv_rod;
		this->m_vprodModel[i]->compute_NodeCompressionStrain(vsv_rod);
		int nv_rod = (int)vsv_rod.size();
		for (int j = 0; j < nv_rod; ++j)
		{
			int nodeIdx;
			this->m_rodMesh_0.getMeshNodeForRodNode(j, i, nodeIdx);
			vsv(nodeIdx) = vsv_rod(j);
		}
	}
}

void RodMeshModel::remeshUniform(const pVector& vnodesExt, iVector& vnewIdxExt, int numV, Real maxL)
{
	vector<pVector> vMPRods;
	vMPRods.resize(this->getNumRod());

	// Add external constrained points

	pVector vMP = vnodesExt;
	int nvExtC = (int) vnodesExt.size();
	
	// Add model internal constrained points

	vMP.insert(vMP.begin(), this->m_sdcs.begin(), this->m_sdcs.end());

	int nvModC = (int) this->m_sdcs.size();

	int nvC = nvExtC + nvModC;

	// Split constraints

	for (int i = 0; i < nvC; ++i)
	{
		const ModelPoint& mp = vMP[i];

		if (mp.isExplicit())
		{
			int rodIdx;
			int nodeIdx;
			this->m_rodMesh_x.getRodNodeForMeshNode(mp.getPointIdx(), rodIdx, nodeIdx);

			// Add model point to the given rod with index
			vMPRods[rodIdx].push_back(ModelPoint(3, nodeIdx));
		}
		else
		{
			assert(mp.getNumSup() == 2);
			int rodIdx0;
			int rodIdx1;
			int nodeIdx0;
			int nodeIdx1;
			this->m_rodMesh_x.getRodNodeForMeshNode(mp.getIndices()[0], rodIdx0, nodeIdx0);
			this->m_rodMesh_x.getRodNodeForMeshNode(mp.getIndices()[1], rodIdx1, nodeIdx1);
			assert(rodIdx0 == rodIdx1); // Ensure same rod
			
			// Add model point to the rod model
			vMPRods[rodIdx0].push_back(mp);
			vMPRods[rodIdx0].back().getIndices()[0] = nodeIdx0;
			vMPRods[rodIdx0].back().getIndices()[1] = nodeIdx1;
		}
	}

	// Remesh each rod separately

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		pVector vnodesExtO;
		this->m_vprodModel[i]->remeshUniform(vMPRods[i], vnodesExtO, numV, maxL);

		// TODO: What to do here with new indices
	}

	// TODO: Create index map and event

	//iVector vnewIdxRaw(3 * nv, -1);
	//vnewIdxExt.resize(nvExtC, -1);
	//int countC = nvExtC;
	//for (int i = 0; i < nvExtC; ++i)
	//{
	//	vnewIdxExt[i] = mconIdx2newIdx[i];
	//}
	//for (set<ModelPoint>::iterator s_it = this->m_sdcs.begin(); s_it != this->m_sdcs.end(); ++s_it, countC++)
	//{
	//	int offsetOld = 3 * (*s_it).getPointIdx();
	//	int offsetNew = 3 * mconIdx2newIdx[countC];
	//	vnewIdxRaw[offsetOld + 0] = offsetNew + 0;
	//	vnewIdxRaw[offsetOld + 1] = offsetNew + 1;
	//	vnewIdxRaw[offsetOld + 2] = offsetNew + 2;
	//}

	//// Send discretization changed signal out
	//this->onDiscretizationChanged(vnewIdxRaw);
}

void RodMeshModel::onDiscretizationChanged(const iVector& vnewIdxRaw)
{
	// Default implementation: deprecate all
	// precomputed information related with
	// configuration and setup again.

	this->deprecateConfiguration();

	// Notify about the changes in the topology indices

	//__raise this->discretizationChanged(this, vnewIdxRaw);
}

void RodMeshModel::setParam_StretchK(const VectorXd& vk)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert((int)vk.size() == this->getNumEdgeRaw());

	int edgeCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		int ne = this->getSimRod_x(i).getNumEdge();

		vector<VectorXd> vki(ne);

		for (int e = 0; e < ne; ++e)
		{
			vki[e].resize(1);
			vki[e][0] = vk(edgeCount++);
		}

		this->m_vprodModel[i]->setParam_StretchK(vki);
	}

	//for (int i = 0; i < this->getNumCon(); ++i)
	//{
	//	this->m_vpconModel[i]->updateLengthEdge();
	//}

	this->m_isReady_Mesh = false;
}

void RodMeshModel::setParam_RestLengthEdge(const VectorXd& vl)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert((int)vl.size() == this->getNumEdgeRaw());

	int edgeCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		int ne = this->getSimRod_x(i).getNumEdge();

		VectorXd vli(ne);

		for (int e = 0; e < ne; ++e)
			vli(e) = vl(edgeCount++);

		this->m_vprodModel[i]->setParam_RestLengthEdge(vli);
	}

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		this->m_vpconModel[i]->updateLengthEdge();
	}

	this->m_isReady_Mesh = false;
}

void RodMeshModel::setParam_RestLengthRod(const VectorXd& vl)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert((int)vl.size() == this->getNumRod());

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		this->m_vprodModel[i]->setParam_RestLengthRod(vl[i]);
	}

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		this->m_vpconModel[i]->updateLengthEdge();
	}

	this->m_isReady_Mesh = false;
}

void RodMeshModel::getParam_StretchK(VectorXd& vk) const
{
	vk.resize(this->getNumEdgeRaw());

	int edgeCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		int ne = this->getSimRod_x(i).getNumEdge();

		vector<VectorXd> vki(ne);

		this->m_vprodModel[i]->getParam_StretchK(vki);

		for (int e = 0; e < ne; ++e)
			vk(edgeCount++) = vki[e][0];
	}
}

void RodMeshModel::getParam_RestLengthEdge(VectorXd& vl) const
{
	vl.resize(this->getNumEdgeRaw());

	int edgeCount = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
	{
		int ne = this->getSimRod_x(i).getNumEdge();

		VectorXd vli(ne);

		this->m_vprodModel[i]->getParam_RestLengthEdge(vli);

		for (int e = 0; e < ne; ++e)
			vl(edgeCount++) = vli[e];
	}
}

void RodMeshModel::getParam_RestLengthRod(VectorXd& vl) const
{
	vl.resize(this->getNumRod());
	for (int i = 0; i < this->getNumRod(); ++i)
		vl[i] = this->m_vprodModel[i]->getParam_RestLengthRod();
}