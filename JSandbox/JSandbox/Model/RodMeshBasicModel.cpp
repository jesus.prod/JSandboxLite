/*=====================================================================================*/
/*!
file		RodMeshBasicModel.cpp
author		jesusprod
brief		Implementation of RodMeshBasicModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/RodMeshBasicModel.h>

#include <JSandbox/Maple/eulerRotation.h>
#include <JSandbox/Maple/parallelTransportNormalized.h>
#include <JSandbox/Maple/computeReferenceTwistChange.h>

#ifdef _OPENMP 
#include <omp.h>
#endif

RodMeshBasicModel::RodMeshBasicModel() : SolidModel("RMB")
{
	this->m_stateRM_x = pRMState(new RMState());
	this->m_stateRM_0 = pRMState(new RMState());
	this->m_state_x = this->m_stateRM_x;
	this->m_state_0 = this->m_stateRM_0;

	this->deprecateConfiguration();
}

RodMeshBasicModel::~RodMeshBasicModel()
{
	logSimu("[INFO] Destroying RodMeshBasicModel: %u", this);
}

void RodMeshBasicModel::initializeMaterials()
{
	// Nothing to do here
}

void RodMeshBasicModel::deprecateDiscretization()
{
	SolidModel::deprecateDiscretization();

	this->m_nNZ_DfxDp = -1;
}

void RodMeshBasicModel::deprecateDeformation()
{
	SolidModel::deprecateDeformation();

	this->m_isReady_DfxDp = false;
}

void RodMeshBasicModel::setup()
{
	// Get nodes and edges

	this->m_numNodes = this->m_rodMesh_x.getNumNode();
	this->m_numEdges = this->m_rodMesh_x.getNumEdge();
	this->m_numCons = this->m_rodMesh_x.getNumCon();
	this->m_numRods = this->m_rodMesh_x.getNumRod();

	//////////////////////////// Initialize Node Maps ////////////////////////////

	m_vnodeRaw2Sim.resize(m_numNodes);
	for (int i = 0; i < m_numNodes; ++i)
		m_vnodeRaw2Sim[i] = -1;

	// Initialize conn indices

	int countNode = 0;
	int countEdge = 0;

	for (int i = 0; i < this->m_numCons; ++i)
	{
		int numConRods = this->m_rodMesh_x.getCon(i).m_srods.size();
		iVector vnodes = this->m_rodMesh_x.getConNodes(i);
		for (int i = 0; i < numConRods; ++i)
		{
			this->m_vnodeRaw2Sim[vnodes[2 * i]] = countNode;
		}

		countNode++;
	}

	// Initialize rest indices

	for (int i = 0; i < this->m_numNodes; ++i)
	{
		if (this->m_vnodeRaw2Sim[i] == -1)
			this->m_vnodeRaw2Sim[i] = countNode++;
	}

	this->m_numPoint = countNode;

	this->m_nrawDOF = 3 * this->m_numNodes + this->m_numEdges + 3 * this->m_numCons;
	this->m_nsimDOF_x = 3 * this->m_numPoint + this->m_numEdges + 3 * this->m_numCons;
	this->m_nsimDOF_0 = 3 * this->m_numPoint + this->m_numEdges + 3 * this->m_numCons;
	this->m_stateRM_x->m_vx.resize(this->m_nsimDOF_x);
	this->m_stateRM_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_stateRM_0->m_vx.resize(this->m_nsimDOF_0);
	this->m_stateRM_0->m_vv.resize(this->m_nsimDOF_0);
	this->m_stateRM_x->m_vx.setZero();
	this->m_stateRM_x->m_vv.setZero();
	this->m_stateRM_0->m_vx.setZero();
	this->m_stateRM_0->m_vv.setZero();

	//////////////////////////// Initialize DOF map ////////////////////////////

	int edgeRawDoFOffset = 3 * this->m_numNodes;
	int edgeSimDoFOffset = 3 * this->m_numPoint;
	int connRawDoFOffset = 3 * this->m_numNodes + this->m_numEdges;
	int connSimDoFOffset = 3 * this->m_numPoint + this->m_numEdges;

	this->m_vraw2sim_x.resize(this->m_nrawDOF);
	this->m_vraw2sim_0.resize(this->m_nrawDOF);

	for (int i = 0; i < this->m_numNodes; ++i)
		for (int j = 0; j < 3; ++j)
		{
			this->m_vraw2sim_x[3 * i + j] = this->m_vnodeRaw2Sim[i] * 3 + j;
			this->m_vraw2sim_x[3 * i + j] = this->m_vnodeRaw2Sim[i] * 3 + j;
		}
	for (int i = 0; i < this->m_numCons; ++i)
		for (int j = 0; j < 3; ++j)
		{
			this->m_vraw2sim_x[connRawDoFOffset + 3 * i + j] = connSimDoFOffset + 3 * i + j;
			this->m_vraw2sim_x[connRawDoFOffset + 3 * i + j] = connSimDoFOffset + 3 * i + j;
		}
	for (int i = 0; i < this->m_numEdges; ++i)
	{
		this->m_vraw2sim_x[edgeRawDoFOffset + i] = edgeSimDoFOffset + i;
		this->m_vraw2sim_0[edgeRawDoFOffset + i] = edgeSimDoFOffset + i;
	}

	//////////////////////////// Initialize Rods State ////////////////////////////

	countNode = 0;
	countEdge = 0;

	this->m_stateRM_x->m_va.resize(this->m_numEdges);
	this->m_stateRM_0->m_va.resize(this->m_numEdges);
	this->m_stateRM_x->m_vF.resize(this->m_numEdges);
	this->m_stateRM_0->m_vF.resize(this->m_numEdges);
	this->m_stateRM_x->m_vt.resize(this->m_numNodes);
	this->m_stateRM_0->m_vt.resize(this->m_numNodes);
	for (int i = 0; i < this->m_numRods; ++i)
	{
		int numRodEdges = this->m_rodMesh_x.getRod(i).getNumEdge();
		for (int j = countEdge; j < countEdge + numRodEdges; ++j)
		{
			this->m_stateRM_x->m_va[j] = 0.0;
			this->m_stateRM_0->m_va[j] = 0.0;
			this->m_stateRM_x->m_vF[j] = this->m_rodMesh_x.getRod(i).getFrame(j - countEdge);
			this->m_stateRM_0->m_vF[j] = this->m_rodMesh_0.getRod(i).getFrame(j - countEdge);
		}

		int numRodNodes = this->m_rodMesh_x.getRod(i).getNumNode();
		const dVector& vx = this->m_rodMesh_x.getRod(i).getPositions();
		const dVector& v0 = this->m_rodMesh_0.getRod(i).getPositions();
		for (int j = countNode; j < countNode + numRodNodes; ++j)
		{
			this->m_stateRM_x->m_vt[j] = 0.0;
			this->m_stateRM_0->m_vt[j] = 0.0;
			int simIdx = m_vnodeRaw2Sim[j];
			int simOffset = 3 * simIdx;
			int rodOffset = 3 * (j - countNode);
			this->m_stateRM_x->m_vx[simOffset + 0] = vx[rodOffset + 0];
			this->m_stateRM_x->m_vx[simOffset + 1] = vx[rodOffset + 1];
			this->m_stateRM_x->m_vx[simOffset + 2] = vx[rodOffset + 2];
			this->m_stateRM_0->m_vx[simOffset + 0] = v0[rodOffset + 0];
			this->m_stateRM_0->m_vx[simOffset + 1] = v0[rodOffset + 1];
			this->m_stateRM_0->m_vx[simOffset + 2] = v0[rodOffset + 2];
		}

		countEdge += numRodEdges;
		countNode += numRodNodes;
	}

	this->updateMetadata_x();
	this->updateMetadata_0();

	//////////////////////////// Initialize Conn State ////////////////////////////

	this->m_stateRM_x->m_vcon.resize(this->m_numCons);
	this->m_stateRM_0->m_vcon.resize(this->m_numCons);
	this->m_stateRM_x->m_vR0x.resize(this->m_numCons);
	this->m_stateRM_0->m_vR0x.resize(this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		const Con& rc = this->m_rodMesh_x.getCon(i);

		int nr = (int)rc.m_srods.size();
		this->m_stateRM_x->m_vcon[i].m_vrods.resize(nr);
		this->m_stateRM_0->m_vcon[i].m_vrods.resize(nr);
		this->m_stateRM_x->m_vR0x[i] = this->m_rodMesh_x.getRot(i);
		this->m_stateRM_0->m_vR0x[i] = this->m_rodMesh_0.getRot(i);

		set<pair<int, int>>::iterator s_end = rc.m_srods.end();
		set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

		// Initialize connection edges

		int j = 0;
		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
		{
			int ridx = (*s_it).first;
			int side = (*s_it).second;
			const Rod& rod_x = m_rodMesh_x.getRod(ridx);
			const Rod& rod_0 = m_rodMesh_0.getRod(ridx);
			int nv = rod_x.getNumNode();
			int ne = rod_x.getNumEdge();

			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_S = (side == 0) ? 1.0 : -1.0;
			this->m_stateRM_x->m_vcon[i].m_vrods[j].m_S = (side == 0) ? 1.0 : -1.0;

			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFr = this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFc = (side == 0) ? rod_0.getFrame(0) : rod_0.getFrame(ne - 1);
			this->m_stateRM_x->m_vcon[i].m_vrods[j].m_rodFr = this->m_stateRM_x->m_vcon[i].m_vrods[j].m_rodFc = (side == 0) ? rod_x.getFrame(0) : rod_x.getFrame(ne - 1);

			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_vr = (side == 0) ? rod_x.getPosition(1) : rod_x.getPosition(nv - 2);
			this->m_stateRM_x->m_vcon[i].m_vrods[j].m_vr = (side == 0) ? rod_0.getPosition(1) : rod_0.getPosition(nv - 2);

			if (j == 0)
			{
				this->m_stateRM_x->m_vcon[i].m_vc = (side == 0) ? rod_x.getPosition(0) : rod_x.getPosition(nv - 1);
				this->m_stateRM_0->m_vcon[i].m_vc = (side == 0) ? rod_0.getPosition(0) : rod_0.getPosition(nv - 1);
			}
		}
	}

	this->resetConnectionsBasicRotations();
	this->resetConnectionsRotFrames();
	this->resetConnectionsRefFrames();
	this->updateConnectionsToState();

	//////////////////////////// Initialize Materials ////////////////////////////

	this->m_vnodeMats.resize(this->m_numNodes);
	this->m_vedgeMats.resize(this->m_numEdges);
	this->m_vconnMats.resize(this->m_numCons);
	for (int i = 0; i < this->m_numNodes; ++i)
		this->m_vnodeMats[i] = this->m_material;
	for (int i = 0; i < this->m_numEdges; ++i)
		this->m_vedgeMats[i] = this->m_material;
	for (int i = 0; i < this->m_numCons; ++i)
		this->m_vconnMats[i] = this->m_material;

	//////////////////////////// Initialize Elements ////////////////////////////

	this->freeElements();

	//// Initialize stretch elements

	//for (int i = 0; i < this->m_numRods; ++i)
	//{
	//	Rod& rod = this->m_rodMesh_x.getRod(i);
	//	int numRodEdges = rod.getNumEdge();
	//	for (int j = 0; j < numRodEdges; ++j)
	//	{
	//		iVector veind(1);
	//		iVector vvind(2);
	//		iVector vDOFind(7);

	//		int pv, nv;
	//		rod.getCurve().getEdgeNeighborNodes(j, pv, nv);
	//		m_rodMesh_x.getMeshEdgeForRodEdge(j, i, veind[0]);
	//		m_rodMesh_x.getMeshNodeForRodNode(pv, i, vvind[0]);
	//		m_rodMesh_x.getMeshNodeForRodNode(nv, i, vvind[1]);
	//		vvind[0] = this->m_vnodeRaw2Sim[vvind[0]];
	//		vvind[1] = this->m_vnodeRaw2Sim[vvind[1]];
	//		for (int j = 0; j < 3; ++j)
	//		{
	//			vDOFind[0 + j] = 3 * vvind[0] + j;
	//			vDOFind[3 + j] = 3 * vvind[1] + j;
	//		}
	//		vDOFind[6] = 3 * this->m_numPoint + veind[0];

	//		vector<SolidMaterial*> vpMats(1);
	//		vpMats[0] = &m_vedgeMats[veind[0]];

	//		DERStretchElement *pEle = new DERStretchElement();
	//		pEle->setIndicesx(vDOFind);
	//		pEle->setIndices0(vDOFind);
	//		pEle->setEdgeIndices(veind);
	//		pEle->setVertexIndices(vvind);
	//		pEle->setMaterials(vpMats);

	//		this->m_vpEles.push_back(pEle);

	//		this->m_vpStretchEles.push_back(pEle);
	//	}
	//}

	// Initialize Bending & Twist

	//for (int i = 0; i < this->m_numRods; ++i)
	//{
	//	Rod& rod = this->m_rodMesh_x.getRod(i);
	//	int numRodEdges = rod.getNumEdge();
	//	for (int j = 0; j < numRodEdges; ++j)
	//	{
	//		iVector veind(1);
	//		iVector vvind(2);
	//		iVector vDOFind(7);

	//		int pv, nv;
	//		rod.getCurve().getEdgeNeighborNodes(j, pv, nv);
	//		m_rodMesh_x.getMeshEdgeForRodEdge(i, j, veind[0]);
	//		vvind[0] = this->m_vnodeRaw2Sim[pv];
	//		vvind[1] = this->m_vnodeRaw2Sim[nv];
	//		for (int j = 0; j < 3; ++j)
	//		{
	//			vDOFind[0 + j] = 3 * vvind[0] + j;
	//			vDOFind[3 + j] = 3 * vvind[1] + j;
	//		}
	//		vDOFind[6] = 3 * this->m_numPoint + veind[0];

	//		vector<SolidMaterial*> vpMats(1);
	//		vpMats[0] = &m_vedgeMats[veind[0]];

	//		DERStretchElement *pEle = new DERStretchElement();
	//		pEle->setIndicesx(vDOFind);
	//		pEle->setEdgeIndices(veind);
	//		pEle->setVertexIndices(vvind);
	//		pEle->setMaterials(vpMats);

	//this->m_vpEles.push_back(pEle);

	//		this->m_vpStretchEles.push_back(pEle);
	//	}
	//}

	this->m_vpRodStretchEles.resize(this->m_numRods);
	this->m_vpRodConnectionEles.resize(this->m_numRods);
	for (int i = 0; i < this->m_numRods; ++i)
	{
		this->m_vpRodStretchEles[i].resize(2);
		this->m_vpRodConnectionEles[i].resize(2);
		this->m_vpRodStretchEles[i][0] = NULL;
		this->m_vpRodStretchEles[i][1] = NULL;
		this->m_vpRodConnectionEles[i][0] = NULL;
		this->m_vpRodConnectionEles[i][1] = NULL;
	}

	for (int i = 0; i < this->m_numCons; ++i)
	{
		const Con& rc = this->m_rodMesh_x.getCon(i);

		set<pair<int, int>>::iterator s_end = rc.m_srods.end();
		set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

		int j = 0;

		iVector vconNodes = this->m_rodMesh_x.getConNodes(i);
		iVector vconEdges = this->m_rodMesh_x.getConEdges(i);

		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, j++)
		{
			int rodIdx, edgeIdx;
			iVector vconEdgesEle;
			iVector vconNodesEle;
			vconEdgesEle.push_back(vconEdges[j]);
			vconNodesEle.push_back(this->m_vnodeRaw2Sim[vconNodes[2 * j + 0]]);
			vconNodesEle.push_back(this->m_vnodeRaw2Sim[vconNodes[2 * j + 1]]);
			this->m_rodMesh_x.getRodEdgeForMeshEdge(vconEdges[j], rodIdx, edgeIdx);
			int side = (*s_it).second;

			// Connection element

			this->m_vpConnectionEles.push_back(new DERConnectionElement());
			DERConnectionElement* pConEle = this->m_vpConnectionEles.back();

			if (side == 0)
			{
				this->m_vpRodConnectionEles[rodIdx][0] = pConEle;
			}
			if (side == 1)
			{
				this->m_vpRodConnectionEles[rodIdx][1] = pConEle;
			}

			iVector vDOFRaw(10), vDOFSim(10);

			// Rod nodes DOF
			for (int k = 0; k < 3; ++k)
			{
				vDOFRaw[0 + k] = 3 * vconNodes[2 * j + 0] + k;
				vDOFRaw[3 + k] = 3 * vconNodes[2 * j + 1] + k;
			}

			// Connection  DOF
			for (int k = 0; k < 3; ++k)
				vDOFRaw[6 + k] = connRawDoFOffset + 3 * i + k;

			// Rod edge DOF
			vDOFRaw[9] = edgeRawDoFOffset + vconEdges[j];

			// Get simulation DOF mapping
			this->mapRaw2Sim(vDOFRaw, vDOFSim);

			vector<SolidMaterial*> vpmats(1);
			vpmats[0] = &this->m_vconnMats[i];

			pConEle->setIndicesx(vDOFSim);
			pConEle->setIndices0(vDOFSim);
			pConEle->setMaterials(vpmats);
			pConEle->setNodeIndices(vconNodesEle);
			pConEle->setEdgeIndices(vconEdgesEle);
			pConEle->setRodIndex(j);
			pConEle->setConIndex(i);

			this->m_vpEles.push_back(pConEle);

			// Connection stretch element

			this->m_vpStretchEles.push_back(new DERStretchElement());
			DERStretchElement *pStrEle = this->m_vpStretchEles.back();

			if (side == 0)
			{
				this->m_vpRodStretchEles[rodIdx][0] = pStrEle;
			}
			if (side == 1)
			{
				this->m_vpRodStretchEles[rodIdx][1] = pStrEle;
			}

			iVector vDOFind(7);

			for (int j = 0; j < 3; ++j)
			{
				vDOFind[0 + j] = 3 * vconNodesEle[0] + j;
				vDOFind[3 + j] = 3 * vconNodesEle[1] + j;
			}
			vDOFind[6] = 3 * this->m_numPoint + vconEdgesEle[0];

			vector<SolidMaterial*> vpMats(1);
			vpMats[0] = &this->m_vconnMats[i];

			pStrEle->setIndicesx(vDOFind);
			pStrEle->setIndices0(vDOFind);
			pStrEle->setEdgeIndices(vconEdgesEle);
			pStrEle->setVertexIndices(vconNodesEle);
			pStrEle->setMaterials(vpMats);

			this->m_vpEles.push_back(pStrEle);
		}
	}

	this->m_isReady_Setup = true;
	this->deprecateDiscretization();

	this->update_Rest();

	logSimu("[INFO] Initialized RodMeshBasicModel. Raw: %u, Sim: %u", this->m_nrawDOF, this->m_nsimDOF_x);
	logSimu("[INFO] \t\t-Rods: %u", this->m_numRods);
	logSimu("[INFO] \t\t-Cons: %u", this->m_numCons);
	logSimu("[INFO] \t\t-Edges: %u", this->m_numEdges);
	logSimu("[INFO] \t\t-Nodes: %u", this->m_numNodes);
}


void RodMeshBasicModel::setState_x(pSolidState s)
{
	*this->m_stateRM_x = RMState(*static_cast<RMState*>(s.get()));

	this->deprecateDeformation();
}

void RodMeshBasicModel::setState_0(pSolidState s)
{
	*this->m_stateRM_0 = RMState(*static_cast<RMState*>(s.get()));

	this->deprecateUndeformed();
}

void RodMeshBasicModel::setPositions_x(const VectorXd& vx)
{
	SolidModel::setPositions_x(vx);

	this->updateMetadata_x();
	this->updateStateToConnections();
	this->resetConnectionsEulerRotation();
	this->resetConnectionsRotFrames();
	this->resetConnectionsRefFrames();

	this->deprecateDeformation();
}

void RodMeshBasicModel::setPositions_0(const VectorXd& vX)
{
	assert(false);
	
	// This should not be called as it is...
}

void RodMeshBasicModel::add_vgravity(VectorXd& vf)
{
	for (int i = 0; i < this->m_numCons; ++i)
	{
		int offset = 3 * i;
		for (int j = 0; j < 3; ++j)
			vf(offset + j) += this->m_vg(j);
	}
}

void RodMeshBasicModel::add_fgravity(VectorXd& vf)
{
	for (int i = 0; i < this->m_numCons; ++i)
	{
		int offset = 3 * i;
		//Vector3d nodalMass(
		//	this->m_vMass(offset + 0),
		//	this->m_vMass(offset + 1),
		//	this->m_vMass(offset + 2));
		//Vector3d nodalAcce = this->m_vg;

		//std::cout << "Nodal mass: " << i << " " << nodalMass[0] << " " << nodalMass[1] << " " << nodalMass[2];
		//std::cout << std::endl;
		//std::cout << "Nodal acc.: " << i << " " << nodalAcce[0] << " " << nodalAcce[1] << " " << nodalAcce[2];
		//std::cout << std::endl;
		//std::cout << std::endl;

		for (int j = 0; j < 3; ++j)
			vf(offset + j) += this->m_vMass(offset + j)*this->m_vg(j);
	}
}

void RodMeshBasicModel::update_Rest()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Rest)
		return; // Already done

	dVector vx;
	toSTL(this->m_stateRM_0->m_vx, vx);

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
	{
		this->m_vpStretchEles[i]->update_Rest(vx, this->m_stateRM_0->m_va, this->m_stateRM_0->m_vt, this->m_stateRM_0->m_vF, this->m_stateRM_0->m_vF);
		this->m_vpStretchEles[i]->setIntegrationVolume(0.5*this->m_vpStretchEles[i]->getRestEdgeLength(0));
	}

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBendTwistEle(); ++i)
		this->m_vpBendTwistEles[i]->update_Rest(vx, this->m_stateRM_0->m_va, this->m_stateRM_0->m_vt, this->m_stateRM_0->m_vF, this->m_stateRM_0->m_vF);

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < (int)m_vpConnectionEles.size(); ++i)
		this->m_vpConnectionEles[i]->update_Rest(this->m_stateRM_0->m_vcon[this->m_vpConnectionEles[i]->getConIndex()]);

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void RodMeshBasicModel::update_Energy()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Energy)
		return; // Already done

	dVector vx;
	toSTL(this->m_stateRM_x->m_vx, vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_Energy(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBendTwistEle(); ++i)
		this->m_vpBendTwistEles[i]->update_Energy(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int)m_vpConnectionEles.size(); ++i)
		this->m_vpConnectionEles[i]->update_Energy(this->m_stateRM_x->m_vcon[this->m_vpConnectionEles[i]->getConIndex()]);

	this->m_isReady_Energy = true;
}

void RodMeshBasicModel::update_Force()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Force)
		return; // Already done

	this->m_updateForTimer.restart();

	dVector vx;
	toSTL(this->m_stateRM_x->m_vx, vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_Force(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBendTwistEle(); ++i)
		this->m_vpBendTwistEles[i]->update_Force(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int)m_vpConnectionEles.size(); ++i)
		this->m_vpConnectionEles[i]->update_Force(this->m_stateRM_x->m_vcon[this->m_vpConnectionEles[i]->getConIndex()]);

	this->m_isReady_Force = true;

	this->m_updateForTimer.stopStoreLog();
}

void RodMeshBasicModel::update_Jacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian)
		return; // Already done

	this->m_updateJacTimer.restart();

	dVector vx;
	toSTL(this->m_stateRM_x->m_vx, vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_Jacobian(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBendTwistEle(); ++i)
		this->m_vpBendTwistEles[i]->update_Jacobian(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int)m_vpConnectionEles.size(); ++i)
		this->m_vpConnectionEles[i]->update_Jacobian(this->m_stateRM_x->m_vcon[this->m_vpConnectionEles[i]->getConIndex()]);

	this->m_isReady_Jacobian = true;

	this->m_updateJacTimer.stopStoreLog();
}

void RodMeshBasicModel::update_EnergyForce()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Energy && this->m_isReady_Force)
		return; // Both of them already computed, out

	this->m_updateForTimer.restart();

	dVector vx;
	toSTL(this->m_stateRM_x->m_vx, vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_EnergyForce(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBendTwistEle(); ++i)
		this->m_vpBendTwistEles[i]->update_EnergyForce(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int)m_vpConnectionEles.size(); ++i)
		this->m_vpConnectionEles[i]->update_EnergyForce(this->m_stateRM_x->m_vcon[this->m_vpConnectionEles[i]->getConIndex()]);

	this->m_isReady_Energy = true;
	this->m_isReady_Force = true;

	this->m_updateForTimer.stopStoreLog();
}

void RodMeshBasicModel::update_ForceJacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Force && this->m_isReady_Jacobian)
		return; // Both of them already computed, out

	this->m_updateForTimer.restart();
	this->m_updateJacTimer.restart();

	dVector vx;
	toSTL(this->m_stateRM_x->m_vx, vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_ForceJacobian(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBendTwistEle(); ++i)
		this->m_vpBendTwistEles[i]->update_ForceJacobian(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int)m_vpConnectionEles.size(); ++i)
		this->m_vpConnectionEles[i]->update_ForceJacobian(this->m_stateRM_x->m_vcon[this->m_vpConnectionEles[i]->getConIndex()]);

	this->m_isReady_Force = true;
	this->m_isReady_Jacobian = true;

	this->m_updateForTimer.stopStoreLog();
	this->m_updateJacTimer.stopStoreLog();
}

void RodMeshBasicModel::update_EnergyForceJacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian &&
		this->m_isReady_Force &&
		this->m_isReady_Energy)
		return; // All done

	this->m_updateForTimer.restart();
	this->m_updateJacTimer.restart();

	dVector vx;
	toSTL(this->m_stateRM_x->m_vx, vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_EnergyForceJacobian(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumBendTwistEle(); ++i)
		this->m_vpBendTwistEles[i]->update_EnergyForceJacobian(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int)m_vpConnectionEles.size(); ++i)
		this->m_vpConnectionEles[i]->update_EnergyForceJacobian(this->m_stateRM_x->m_vcon[this->m_vpConnectionEles[i]->getConIndex()]);

	this->m_isReady_Energy = true;
	this->m_isReady_Force = true;
	this->m_isReady_Jacobian = true;

	this->m_updateForTimer.stopStoreLog();
	this->m_updateJacTimer.stopStoreLog();
}

void RodMeshBasicModel::update_DfxDp()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_DfxDp)
		return; // Already done

	dVector vx;
	toSTL(this->m_stateRM_x->m_vx, vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumStretchEle(); ++i)
		this->m_vpStretchEles[i]->update_DfxDp(vx);

//#ifdef OMP_ENFORJAC
//#pragma omp parallel for
//#endif
//	for (int i = 0; i < this->getNumBendTwistEle(); ++i)
//		this->m_vpBendTwistEles[i]->update_EnergyForceJacobian(vx, this->m_stateRM_x->m_va, this->m_stateRM_x->m_vt, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vF);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int)m_vpConnectionEles.size(); ++i)
		this->m_vpConnectionEles[i]->update_DfxDp(this->m_stateRM_x->m_vcon[this->m_vpConnectionEles[i]->getConIndex()]);

	this->m_isReady_DfxDp = true;
}

void RodMeshBasicModel::updateConnectionsToState()
{
	int numNodeDOF = 3 * this->m_numPoint;
	int numEdgeDOF = this->m_numEdges;

	for (int i = 0; i < this->m_numCons; ++i)
	{
		const Con& rc = this->m_rodMesh_x.getCon(i);
		ConState& CSx = this->m_stateRM_x->m_vcon[i];
		VectorXd& vx = this->m_stateRM_x->m_vx;

		int nr = (int)rc.m_srods.size();

		set<pair<int, int>>::iterator s_end = rc.m_srods.end();
		set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

		int connOffset = numNodeDOF + numEdgeDOF + 3 * i;
		vx(connOffset + 0) = CSx.m_vR.x();
		vx(connOffset + 1) = CSx.m_vR.y();
		vx(connOffset + 2) = CSx.m_vR.z();

		iVector vconNodes = this->m_rodMesh_x.getConNodes(i);
		iVector vconEdges = this->m_rodMesh_x.getConEdges(i);

		int j = 0;

		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
		{
			int centerNodeIdx = vconNodes[2 * j + 0];
			int otherNodeIdx = vconNodes[2 * j + 1];

			// Update point values

			vx(3 * m_vnodeRaw2Sim[centerNodeIdx] + 0) = CSx.m_vc.x();
			vx(3 * m_vnodeRaw2Sim[centerNodeIdx] + 1) = CSx.m_vc.y();
			vx(3 * m_vnodeRaw2Sim[centerNodeIdx] + 2) = CSx.m_vc.z();

			vx(3 * m_vnodeRaw2Sim[otherNodeIdx] + 0) = CSx.m_vrods[j].m_vr.x();
			vx(3 * m_vnodeRaw2Sim[otherNodeIdx] + 1) = CSx.m_vrods[j].m_vr.y();
			vx(3 * m_vnodeRaw2Sim[otherNodeIdx] + 2) = CSx.m_vrods[j].m_vr.z();

			// Update angle values

			vx(numNodeDOF + vconEdges[j]) = CSx.m_vrods[j].m_roda;
		}
	}
}

void RodMeshBasicModel::updateStateToConnections()
{
	int numNodeDOF = 3 * this->m_numPoint;
	int numEdgeDOF = this->m_numEdges;

	for (int i = 0; i < this->m_numCons; ++i)
	{
		const Con& rc = this->m_rodMesh_x.getCon(i);
		ConState& CSx = this->m_stateRM_x->m_vcon[i];
		VectorXd& vx = this->m_stateRM_x->m_vx;

		int nr = (int)rc.m_srods.size();

		set<pair<int, int>>::iterator s_end = rc.m_srods.end();
		set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

		int connOffset = numNodeDOF + numEdgeDOF + 3 * i;
		CSx.m_vR.x() = vx(connOffset + 0);
		CSx.m_vR.y() = vx(connOffset + 1);
		CSx.m_vR.z() = vx(connOffset + 2);

		iVector vconNodes = this->m_rodMesh_x.getConNodes(i);
		iVector vconEdges = this->m_rodMesh_x.getConEdges(i);

		int j = 0;

		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
		{
			int centerNodeIdx = vconNodes[2 * j + 0];
			int otherNodeIdx = vconNodes[2 * j + 1];

			// Update point values

			CSx.m_vc.x() = vx(3 * m_vnodeRaw2Sim[centerNodeIdx] + 0);
			CSx.m_vc.y() = vx(3 * m_vnodeRaw2Sim[centerNodeIdx] + 1);
			CSx.m_vc.z() = vx(3 * m_vnodeRaw2Sim[centerNodeIdx] + 2);

			CSx.m_vrods[j].m_vr.x() = vx(3 * m_vnodeRaw2Sim[otherNodeIdx] + 0);
			CSx.m_vrods[j].m_vr.y() = vx(3 * m_vnodeRaw2Sim[otherNodeIdx] + 1);
			CSx.m_vrods[j].m_vr.z() = vx(3 * m_vnodeRaw2Sim[otherNodeIdx] + 2);

			// Update angle values

			CSx.m_vrods[j].m_roda = vx(numNodeDOF + vconEdges[j]);
		}
	}
}

void RodMeshBasicModel::resetConnectionsRefFrames()
{
	for (int i = 0; i < this->m_numCons; ++i)
	{
		const Con& rc = this->m_rodMesh_x.getCon(i);
		ConState& CSx = this->m_stateRM_x->m_vcon[i];
		VectorXd& vx = this->m_stateRM_x->m_vx;

		int nr = (int)rc.m_srods.size();

		set<pair<int, int>>::iterator s_end = rc.m_srods.end();
		set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

		int j = 0;

		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
		{
			Vector3d matTanROx;
			if (CSx.m_vrods[j].m_S > 0)
				matTanROx = CSx.m_vrods[j].m_vr - CSx.m_vc;
			else matTanROx = CSx.m_vc - CSx.m_vrods[j].m_vr;
			matTanROx.normalize();

			// Update reference frame

			Vector3d matNorROx;
			parallelTransportNormalized(CSx.m_vrods[j].m_rodFr.tan.data(), matTanROx.data(), CSx.m_vrods[j].m_rodFr.nor.data(), matNorROx.data());
			CSx.m_vrods[j].m_rodFr.tan = matTanROx;
			CSx.m_vrods[j].m_rodFr.nor = matNorROx;
			CSx.m_vrods[j].m_rodFr.bin = matTanROx.cross(matNorROx);

			// Update reference twist

			CSx.m_vrods[j].m_rodt += computeReferenceTwistChange(
				CSx.m_vrods[j].m_rodFc.tan.data(),
				CSx.m_vrods[j].m_rodFc.nor.data(),
				CSx.m_vrods[j].m_rodFr.tan.data(),
				CSx.m_vrods[j].m_rodFr.nor.data(),
				CSx.m_vrods[j].m_rodt);
		}
	}
}

void RodMeshBasicModel::resetConnectionsBasicRotations()
{
	for (int i = 0; i < this->m_numCons; ++i)
	{
		if (!this->m_stateRM_x->m_vR0x[i].isZero(1e-6))
		{
			logSimu("[INFO] Initial connection rotation already initialized");
			continue;
		}

		const Con& rc = this->m_rodMesh_x.getCon(i);
		ConState& CSx = this->m_stateRM_x->m_vcon[i];
		ConState& CS0 = this->m_stateRM_0->m_vcon[i];
		VectorXd& vx = this->m_stateRM_x->m_vx;

		int nr = (int)rc.m_srods.size();

		set<pair<int, int>>::iterator s_end = rc.m_srods.end();
		set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

		int j = 0;

		// Initialize connections

		Matrix3d mQ;
		mQ.setZero();

		Matrix3d mA;
		mA.setZero();

		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
		{
			// Estimate initial rotation

			const Frame3d& F0 = this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFr;
			const Frame3d& Fx = this->m_stateRM_x->m_vcon[i].m_vrods[j].m_rodFr;

			mQ +=
				F0.bin*F0.bin.transpose() +
				F0.tan*F0.tan.transpose() +
				F0.nor*F0.nor.transpose();

			mA +=
				Fx.bin*F0.bin.transpose() +
				Fx.tan*F0.tan.transpose() +
				Fx.nor*F0.nor.transpose();
		}

		logSimu("[INFO] Estimating initial rotation with Polar Decomposition");

		if (abs(mQ.determinant()) < EPS_POS)
		{
			logSimu("[WARNING] Singular shape matching");
		}

		if (isnan(mQ.norm()))
		{
			logSimu("[WARNING] NaN shape matching matrix");
		}

		Matrix3d B = mA * mQ.inverse();
		Matrix3d R;
		Matrix3d S;
		if (computePolarDecomposition(B, R, S, EPS_POS) < 0.0)
		{
			logSimu("[WARNING] Polar decomposition error");

			R.setIdentity();
			S.setIdentity();
		}
		else
		{
			logSimu("[INFO] Polar decomposition correct");
		}

		if (!(R*R.transpose()).isIdentity(EPS_POS))
		{
			logSimu("[WARNING] Not initial exact rotation R*RT: %f", (R*R.transpose()).norm());
		}

		this->m_stateRM_x->m_vR0x[i] = R;

		// Initialized to the best fit rotation
		this->m_stateRM_x->m_vcon[i].m_vR.setZero();
	}
}

void RodMeshBasicModel::resetConnectionsEulerRotation()
{
	int numNodeDOF = 3 * this->m_numPoint;
	int numEdgeDOF = this->m_numEdges;

	for (int i = 0; i < this->m_numCons; ++i)
	{
		const Con& rc = this->m_rodMesh_x.getCon(i);
		ConState& CSx = this->m_stateRM_x->m_vcon[i];
		ConState& CS0 = this->m_stateRM_0->m_vcon[i];
		VectorXd& vx = this->m_stateRM_x->m_vx;

		int nr = (int)rc.m_srods.size();

		set<pair<int, int>>::iterator s_end = rc.m_srods.end();
		set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

		int j = 0;

		int connOffset = numNodeDOF + numEdgeDOF + 3 * i;

		// Extract Euler angles
		double eulerAngles[3];
		eulerAngles[0] = vx(connOffset + 0);
		eulerAngles[1] = vx(connOffset + 1);
		eulerAngles[2] = vx(connOffset + 2);

		// Make rotation zero
		vx(connOffset + 0) = 0.0;
		vx(connOffset + 1) = 0.0;
		vx(connOffset + 2) = 0.0;
		CSx.m_vR.setZero();

		// Update R
		Matrix3d newR;
		eulerRotation(eulerAngles, this->m_stateRM_x->m_vR0x[i].col(0).data(), newR.col(0).data());
		eulerRotation(eulerAngles, this->m_stateRM_x->m_vR0x[i].col(1).data(), newR.col(1).data());
		eulerRotation(eulerAngles, this->m_stateRM_x->m_vR0x[i].col(2).data(), newR.col(2).data());
		this->m_stateRM_x->m_vR0x[i] = newR;
	}
}

void RodMeshBasicModel::resetConnectionsRotFrames()
{
	for (int i = 0; i < this->m_numCons; ++i)
	{
		const Con& rc = this->m_rodMesh_x.getCon(i);
		ConState& CSx = this->m_stateRM_x->m_vcon[i];
		ConState& CS0 = this->m_stateRM_0->m_vcon[i];
		VectorXd& vx = this->m_stateRM_x->m_vx;

		int nr = (int)rc.m_srods.size();

		set<pair<int, int>>::iterator s_end = rc.m_srods.end();
		set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

		int j = 0;

		// Update connection frames
		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
		{
			CSx.m_vrods[j].m_rodFc.tan = this->m_stateRM_x->m_vR0x[i] * CS0.m_vrods[j].m_rodFr.tan;
			CSx.m_vrods[j].m_rodFc.nor = this->m_stateRM_x->m_vR0x[i] * CS0.m_vrods[j].m_rodFr.nor;
			CSx.m_vrods[j].m_rodFc.bin = CSx.m_vrods[j].m_rodFc.tan.cross(CSx.m_vrods[j].m_rodFc.nor);
		}
	}
}

void RodMeshBasicModel::updateMetadata_x()
{
	this->updateRodCenter(this->m_stateRM_x->m_vx, this->m_rodMesh_x);
	this->updateRodAngles(this->m_stateRM_x->m_vx, this->m_stateRM_x->m_va);
	this->updateRodRefFrame(this->m_rodMesh_x, this->m_stateRM_x->m_vF, this->m_stateRM_x->m_vt);
	this->updateRodMatFrame(this->m_stateRM_x->m_va, this->m_stateRM_x->m_vF, this->m_rodMesh_x);
	this->m_isReady_Mesh = true;
}

void RodMeshBasicModel::updateMetadata_0()
{
	this->updateRodCenter(this->m_stateRM_0->m_vx, this->m_rodMesh_0);
	this->updateRodAngles(this->m_stateRM_0->m_vx, this->m_stateRM_0->m_va);
	this->updateRodRefFrame(this->m_rodMesh_0, this->m_stateRM_0->m_vF, this->m_stateRM_0->m_vt);
	this->updateRodMatFrame(this->m_stateRM_0->m_va, this->m_stateRM_0->m_vF, this->m_rodMesh_0);
	this->m_isReady_Mesh = true;
}

void RodMeshBasicModel::updateRodCenter(const VectorXd& vx, RodMesh& mesh)
{
	int countNode = 0;

	for (int i = 0; i < mesh.getNumRod(); ++i)
	{
		Rod& rod = m_rodMesh_x.getRod(i);
		int numRodNode = rod.getNumNode();

		dVector vxRod(3 * numRodNode);
		for (int j = 0; j < numRodNode; ++j)
		{
			int idx = m_vnodeRaw2Sim[countNode + j];

			int offsetRod = 3 * j;
			int offsetGlo = 3 * idx;

			vxRod[offsetRod + 0] = vx(offsetGlo + 0);
			vxRod[offsetRod + 1] = vx(offsetGlo + 1);
			vxRod[offsetRod + 2] = vx(offsetGlo + 2);
		}
		rod.setPositions(vxRod);

		countNode += numRodNode;
	}
}

void RodMeshBasicModel::updateRodAngles(const VectorXd& vx, dVector& va)
{
	int numNodeDOF = 3 * this->m_numPoint;

	int countEdge = 0;

	for (int i = 0; i < this->m_rodMesh_x.getNumRod(); ++i)
	{
		Rod& rod = m_rodMesh_x.getRod(i);
		int numRodEdge = rod.getNumEdge();

		for (int j = countEdge; j < countEdge + numRodEdge; ++j)
			va[j] = vx[numNodeDOF + j];

		countEdge += numRodEdge;
	}

}

void RodMeshBasicModel::updateRodRefFrame(const RodMesh& mesh, vector<Frame3d>& vref, dVector& vt)
{
	int countEdge = 0;
	int countNode = 0;

	for (int i = 0; i < mesh.getNumRod(); ++i)
	{
		const Curve& curve = mesh.getRod(i).getCurve();

		int ne = curve.getNumEdge();
		int nv = curve.getNumNode();

		for (int j = 0; j < ne; ++j)
		{
			vref[countEdge + j] = parallelTransport(vref[countEdge + j], curve.getEdge(j).normalized());
		}

		for (int j = 1; j < nv - 1; j++)
		{
			int pe, ne;
			curve.getNodeNeighborEdges(j, pe, ne);
			const Frame3d& F0 = vref[countEdge + pe];
			const Frame3d& F1 = vref[countEdge + ne];
			Frame3d F0t = parallelTransport(F0, F1.tan); // Untwist
			vt[countNode + j] = signedAngle(F0t.bin, F1.bin, F1.tan, vt[countNode + j]);
		}

		countEdge += ne;
		countNode += nv;
	}
}

void RodMeshBasicModel::updateRodMatFrame(const dVector& va, const vector<Frame3d>& vref, RodMesh& mesh)
{
	int countEdge = 0;

	for (int i = 0; i < mesh.getNumRod(); ++i)
	{
		Rod& rod = mesh.getRod(i);
		int ne = rod.getNumEdge();

		for (int j = 0; j < ne; ++j)
		{
			rod.setFrame(j, frameRotation(vref[countEdge + j], va[countEdge + j]));
		}

		countEdge += ne;
	}
}

void RodMeshBasicModel::configureMesh(const RodMesh& rodMesh_x, const RodMesh& rodMesh_0)
{
	this->m_rodMesh_x = rodMesh_x;
	this->m_rodMesh_0 = rodMesh_0;

	this->deprecateConfiguration();
}

void RodMeshBasicModel::update_SimMesh()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mesh)
		return; // Already done

	// Nothing to do here...

	this->m_isReady_Mesh = true;
}

int RodMeshBasicModel::getNumNodeRaw() const
{
	int numNode = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
		numNode += this->getSimRod_x(i).getNumNode();
	return numNode;
}

int RodMeshBasicModel::getNumEdgeRaw() const
{
	int numEdge = 0;
	for (int i = 0; i < this->getNumRod(); ++i)
		numEdge += this->getSimRod_x(i).getNumEdge();
	return numEdge;
}

iVector RodMeshBasicModel::getNodeRawDOF(int inode) const
{
	iVector vidx;

	vidx.push_back(3 * inode + 0);
	vidx.push_back(3 * inode + 1);
	vidx.push_back(3 * inode + 2);

	return vidx;
}

iVector RodMeshBasicModel::getEdgeRawDOF(int iedge) const
{
	iVector vidx;

	vidx.push_back(3 * this->m_numNodes + iedge);

	return vidx;
}

void RodMeshBasicModel::precompute_DfxDp()
{
	tVector testJ;
	this->add_DfxDp(testJ); // Precompute
	this->m_nNZ_DfxDp = (int)testJ.size();
}

bool RodMeshBasicModel::isReady_DfxDp() const
{
	return this->m_isReady_DfxDp;
}

void RodMeshBasicModel::add_DfxDp(tVector& vJ)
{
	if (!this->isReady_DfxDp())
		this->update_DfxDp();

	int numStretchEle = (int) this->m_vpStretchEles.size();
	int numBendTwistEle = (int) this->m_vpBendTwistEles.size();
	int numConnectionEle = (int) this->m_vpConnectionEles.size();

	// CONN stretch/bending/twist

	for (int i = 0; i < numConnectionEle; ++i)
	{
		tVector vDfxDp_i;
		int numCoef = 0;

		int conIdx = this->m_vpConnectionEles[i]->getConIndex();

		// Connection stretch

		vDfxDp_i.clear();
		vDfxDp_i.reserve(7 * 1);
		this->m_vpStretchEles[i]->add_DfxDp(vDfxDp_i);
		numCoef = (int)vDfxDp_i.size();
		for (int j = 0; j < numCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDp_i[j]; // Offset the rod connection
			vJ.push_back(Triplet<Real>(t.row(), t.col() + 4*conIdx + 0, t.value()));
		}

		// Bending and twist
		
		vDfxDp_i.clear();
		vDfxDp_i.reserve(10 * 3);
		this->m_vpConnectionEles[i]->add_DfxDp(vDfxDp_i);
		numCoef = (int)vDfxDp_i.size();
		for (int j = 0; j < numCoef; ++j)
		{
			const Triplet<Real>& t = vDfxDp_i[j]; // Offset the rod connection
			vJ.push_back(Triplet<Real>(t.row(), t.col() + 4*conIdx + 1, t.value()));
		}
	}
}

void RodMeshBasicModel::getParam_StretchK(VectorXd& vk) const
{
	vk.resize(this->m_numEdges);

	for (int i = 0; i < this->m_numEdges; ++i)
	{
		vk(i) = this->m_vedgeMats[i].getStretchK();
	}
}

void RodMeshBasicModel::setParam_StretchK(const VectorXd& vk)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert((int)vk.size() == this->m_numEdges);
	for (int i = 0; i < this->m_numEdges; ++i)
	{
		this->m_vedgeMats[i].setStretchK(vk(i));
	}

	this->deprecateDeformation();
}

void RodMeshBasicModel::getParam_ConnectionStretchK(VectorXd& vk) const
{
	vk.resize(this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		vk[i] = this->m_vconnMats[i].getStretchK();
	}
}

void RodMeshBasicModel::setParam_ConnectionStretchK(const VectorXd& vk)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert(vk.size() == this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		this->m_vconnMats[i].setStretchK(vk[i]);
	}

	this->deprecateDeformation();
}

void RodMeshBasicModel::getParam_ConnectionBendingK(VectorXd& vk) const
{
	vk.resize(2*this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		vk[2*i + 0] = this->m_vconnMats[i].getBendingKh();
		vk[2*i + 1] = this->m_vconnMats[i].getBendingKw();
	}
}

void RodMeshBasicModel::setParam_ConnectionBendingK(const VectorXd& vk)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert(vk.size() == 2*this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		this->m_vconnMats[i].setBendingKh(vk[2 * i + 0]);
		this->m_vconnMats[i].setBendingKw(vk[2 * i + 1]);
	}

	this->deprecateDeformation();
}

void RodMeshBasicModel::getParam_ConnectionTwistK(VectorXd& vk) const
{
	vk.resize(this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		vk[i] = this->m_vconnMats[i].getTwistK();
	}
}

void RodMeshBasicModel::setParam_ConnectionTwistK(const VectorXd& vk)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert(vk.size() == this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		this->m_vconnMats[i].setTwistK(vk[i]);
	}

	this->deprecateDeformation();
}

void RodMeshBasicModel::getParam_RestLengthRodSplit(VectorXd& vl) const
{
	vl.resize(2*this->m_numEdges);
	
	for (int i = 0; i < this->m_numRods; ++i)
	{
		vector<DERStretchElement*> vpStretchEle = this->m_vpRodStretchEles[i];
		assert((int) vpStretchEle.size() == 2);
		Real intVol0 = vpStretchEle[0]->getIntegrationVolume();
		Real intVol1 = vpStretchEle[1]->getIntegrationVolume();
		Real restLength0 = vpStretchEle[0]->getRestEdgeLength(0);
		Real restLength1 = vpStretchEle[1]->getRestEdgeLength(0);
		assert(abs(intVol0 + intVol1 - restLength0) < EPS_APPROX);
		assert(abs(intVol0 + intVol1 - restLength1) < EPS_APPROX);
		vl(2 * i + 0) = intVol0;
		vl(2 * i + 1) = intVol1;
	}
}

void RodMeshBasicModel::setParam_RestLengthRodSplit(const VectorXd& vl)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert((int)vl.size() == 2*this->m_numEdges);

	for (int i = 0; i < this->m_numRods; ++i)
	{
		Real intVol0 = vl(2 * i + 0);
		Real intVol1 = vl(2 * i + 1);
		Real restLength = intVol0 + intVol1;

		// Stretch elements
		vector<DERStretchElement*> vpStretchEle = this->m_vpRodStretchEles[i];
		assert((int)vpStretchEle.size() == 2);
		vpStretchEle[0]->setIntegrationVolume(intVol0);
		vpStretchEle[1]->setIntegrationVolume(intVol1);
		vpStretchEle[0]->setRestEdgeLength(0, restLength);
		vpStretchEle[1]->setRestEdgeLength(0, restLength);

		// Connection elements
		vector<DERConnectionElement*> vpConnectionEle = this->m_vpRodConnectionEles[i];
		assert((int)vpStretchEle.size() == 2);
		vpConnectionEle[0]->setHalfEdgeLength(intVol0);
		vpConnectionEle[1]->setHalfEdgeLength(intVol1);
	}

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void RodMeshBasicModel::getParam_RestTangents(vector<vector<Vector3d>>& vt0) const
{
	vt0.resize(this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		int numConRod = (int) this->m_rodMesh_x.getCon(i).m_srods.size();
		vt0[i].resize(numConRod);
		for (int j = 0; j < numConRod; ++j)
		{
			vt0[i][j] = this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFr.tan;
		}
	}
}

void RodMeshBasicModel::setParam_RestTangents(const vector<vector<Vector3d>>& vt0)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert((int)vt0.size() == this->m_numCons);

	for (int i = 0; i < this->m_numCons; ++i)
	{
		int numConRod = (int) this->m_rodMesh_x.getCon(i).m_srods.size();

		assert((int)vt0[i].size() == numConRod);
		this->m_stateRM_x->m_vR0x[i].setZero();

		//// Test: randomly rotating input tangents
		//vector<Vector3d> testing = vt0;
		//Vector3d axis = Vector3d(0, 0, 1);
		//Real angle = ((double) rand() / (double) RAND_MAX)* 2 * M_PI;
		//std::cout << std::endl << "Axis (0, 0, 1), Angle: " << angle;
		//for (int i = 0; i < numRod; ++i)
		//{
		//	Vector3d original = vt0[i];
		//	testing[i].setZero();
		//	rodriguesRotation(axis.data(), angle, original.data(), testing[i].data());
		//	std::cout << std::endl << "Original: " << original.x() << " " << original.y() << " " << original.z();
		//	std::cout << std::endl << "Testing: " << testing[i].x() << " " << testing[i].y() << " " << testing[i].z();
		//}
		//std::cout << std::endl;

		for (int j = 0; j < numConRod; ++j)
		{
			if ((vt0[i][j].norm() - 1.0) > EPS_APPROX)
			{
				logSimu("[WARNING] Non unitary rest tangent specified");
			}

			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFc.tan = this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFr.tan = vt0[i][j];
			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFc.nor = this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFr.nor = Vector3d(0, 0, 1);
			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFc.bin = this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodFr.bin = vt0[i][j].cross(Vector3d(0, 0, 1));
			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_roda = HUGE_VAL; // Invalidate
			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_rodt = HUGE_VAL; // Invalidate
			this->m_stateRM_0->m_vcon[i].m_vrods[j].m_vr = Vector3d::Ones()*HUGE_VAL; // Invalidate
		}
	}

	this->resetConnectionsBasicRotations();
	this->resetConnectionsRotFrames();
	this->resetConnectionsRefFrames();
	this->updateConnectionsToState();

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

//void RodMeshBasicModel::getParam_RestLengthEdge(VectorXd& vl) const
//{
//	vl.resize(this->m_numEdges);
//
//	for (int i = 0; i < this->m_numEdges; ++i)
//	{
//		vl(i) = this->m_vpStretchEles[i]->getRestEdgeLength(0);
//	}
//}
//
//void RodMeshBasicModel::setParam_RestLengthEdge(const VectorXd& vl)
//{
//	if (!this->m_isReady_Setup)
//		this->setup(); // Init.
//
//	assert((int)vl.size() == this->m_numEdges);
//
//	int numStretchEle = (int) this->m_vpStretchEles.size();
//	int numBendTwistEle = (int) this->m_vpBendTwistEles.size();
//	int numConnectionEle = (int) this->m_vpConnectionEles.size();
//
//	for (int i = 0; i < numStretchEle; ++i)
//	{
//		this->m_vpStretchEles[i]->setRestEdgeLength(0, vl(this->m_vpStretchEles[i]->getEdgeIndices()[0]));
//	}
//
//	for (int i = 0; i < numBendTwistEle; ++i)
//	{
//		this->m_vpBendTwistEles[i]->setRestEdgeLength(0, vl(this->m_vpBendTwistEles[i]->getEdgeIndices()[0]));
//		this->m_vpBendTwistEles[i]->setRestEdgeLength(1, vl(this->m_vpBendTwistEles[i]->getEdgeIndices()[1]));
//	}
//
//	for (int i = 0; i < numConnectionEle; ++i)
//	{
//		this->m_vpConnectionEles[i]->setHalfEdgeLength(0.5*vl(this->m_vpConnectionEles[i]->getEdgeIndices()[0]));
//	}
//
//	this->m_isReady_Rest = true;
//	this->deprecateDeformation();
//}
//
//void RodMeshBasicModel::getParam_RestLengthRod(VectorXd& vl) const
//{
//	VectorXd vle(this->m_numEdges);
//	this->getParam_RestLengthEdge(vle);
//
//	vl.resize(this->m_numRods);
//
//	int countEdge = 0;
//
//	for (int i = 0; i < this->m_numRods; ++i)
//	{
//		int numRodEdge = this->m_rodMesh_x.getRod(i).getNumEdge();
//
//		Real averageLength = 0;
//		for (int j = 0; j < numRodEdge; ++j)
//			averageLength += vle(countEdge + j);
//		averageLength /= numRodEdge;
//
//		vl(i) = averageLength;
//
//		countEdge += numRodEdge;
//	}
//}
//
//void RodMeshBasicModel::setParam_RestLengthRod(const VectorXd& vl)
//{
//	if (!this->m_isReady_Setup)
//		this->setup(); // Init.
//
//	assert((int)vl.size() == this->m_numRods);
//
//	VectorXd vle(this->m_numEdges);
//
//	int countEdge = 0;
//
//	for (int i = 0; i < this->m_numRods; ++i)
//	{
//		int numRodEdge = this->m_rodMesh_x.getRod(i).getNumEdge();
//
//		Real averageLength = vl(i)/numRodEdge;
//		for (int j = 0; j < numRodEdge; ++j)
//			vle(countEdge + j) = averageLength;
//
//		countEdge += numRodEdge;
//	}
//
//	this->setParam_RestLengthEdge(vle);
//
//	this->m_isReady_Rest = true;
//	this->deprecateDeformation();
//}