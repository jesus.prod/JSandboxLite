/*=====================================================================================*/
/*!
/file		ModelParameterization.cpp
/author		jesusprod
/brief		Implementation of ModelParameterization.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/ModelParameterization.h>

//#include <JSandbox/Model/TensileStructureModel.h>

#include <JSandbox/Optim/AlgLib_Sparse_BoxC_QP.h>
#include <JSandbox/Optim/AlgLib_Sparse_EQIN_QP.h>

//#include <JSandbox/Optim/ParameterSet_Tensile_HermiteEdgeRadius.h>
//#include <JSandbox/Optim/ParameterSet_Tensile_HermiteRodMeshRC.h>
//#include <JSandbox/Optim/ParameterSet_Tensile_SurfaceStretch.h>

ModelParameterization::ModelParameterization(const string& ID)
{
	this->m_ID = ID;

	this->m_ns = 0;
	this->m_np = 0;

	this->m_parameterMapping = false;
	this->m_parameterScaling = false;

	this->freeParameters();
}

ModelParameterization::~ModelParameterization()
{
	freeParameters();
}

void ModelParameterization::setModel(SolidModel* pModel)
{
	assert(pModel != NULL);
	this->m_pModel = pModel;
}

void ModelParameterization::setSolver(PhysSolver* pSolver)
{
	assert(pSolver != NULL);
	this->m_pSolver = pSolver;
}

SolidModel* ModelParameterization::getModel() const
{
	return this->m_pModel;
}

PhysSolver* ModelParameterization::getSolver() const
{
	return this->m_pSolver;
}

void ModelParameterization::setParameterSets(const vector<ParameterSet*>& vpset)
{
	int ns = (int)vpset.size();

	assert(ns >= 0);
	freeParameters();

	this->m_vpset = vpset;
	this->m_ns = ns;
	this->m_np = 0;
}

void ModelParameterization::freeParameters()
{
	if (this->m_ns != 0)
	{
		for (int i = 0; i < this->m_ns; ++i)
			delete this->m_vpset[i];
		this->m_vpset.clear();

		this->m_ns = 0;
		this->m_np = 0;
	}

	this->m_isReady_fp = false;
	this->m_isReady_DfDp = false;
}

const vector<ParameterSet*>& ModelParameterization::getParameterSets() const
{
	return this->m_vpset;
}

void ModelParameterization::setup()
{
	// Setup parameters

	this->m_np = 0;

	this->m_vsetOff.clear(); // Offsets
	for (int i = 0; i < this->m_ns; ++i)
	{
		this->m_vpset[i]->setup();
		this->m_vsetOff.push_back(this->m_np);
		this->m_np += this->m_vpset[i]->getNumParameter();
	}

	int offset = 0;
	this->m_vParamStencil.assign(this->m_np, false);
	for (int i = 0; i < this->m_ns; ++i)
	{
		const iVector& vfixed = this->m_vpset[i]->getFixedParameters();

		for (int j = 0; j < (int)vfixed.size(); ++j)
		{	
			this->m_vParamStencil[offset + vfixed[j]] = true; // Filter
			logSimu("[INFO] Fixed parameter (%u, %u)", i, vfixed[j]);
		}

		offset += this->m_vpset[i]->getNumParameter();
	}

	this->m_nx = this->m_pModel->getNumSimDOF_x();

	// Initialize parameters scale

	this->setScale_Identity();

	// Update bounds vector

	this->updateBoundsVector();

	// Initialize hinge element

	iVector vnodeIdx;
	vnodeIdx.push_back(0);
	vnodeIdx.push_back(1);
	vnodeIdx.push_back(2);
	//this->m_hingeElement = Curve3DAngleElement(3);
	//this->m_hingeElement.setNodeIndicesx(vnodeIdx);
	//this->m_hingeElement.setNodeIndices0(vnodeIdx);
	//this->m_hingeElement.setRestAngle(1.0);
	//this->m_hingeElement.setVertexArea(1.0);

	this->m_hingeMaterial.setBendingKw(1);
	this->m_hingeMaterial.setBendingKh(1);
	this->m_hingeMaterial.initFromYoungPoisson(0, 0, 0);
	//this->m_hingeElement.setMaterial(&m_hingeMaterial);

	// Deprecate linearisation

	this->m_isReady_Pt = false;
	this->m_isReady_St = false;
	this->m_isReady_fp = false;
	this->m_isReady_DfDx = false;
	this->m_isReady_DfDp = false;

	//this->testParameterLinearization();

	//TensileStructureModel* pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);

	//const iVector& vmapRaw2Sim = pTSModel->getRawToSim_x();

	//int numM = pTSModel->getNumSurfaceModels();

	//int numSim = pTSModel->getNumSimDOF_x();

	//// Cache some matrices

	//this->m_vmSim2Mem.resize(numM);
	//this->m_vmMem2Int.resize(numM);
	//this->m_vmMem2Bou.resize(numM);
	//for (int i = 0; i < numM; ++i)
	//{
	//	SurfaceModel* pSModel = pTSModel->getSurfaceModel(i);

	//	iVector vBoundIdx;
	//	iVector vInterIdx;
	//	pSModel->getSimMesh().getSortedBoundaryIndices(vBoundIdx, vInterIdx);

	//	int numMem = pSModel->getNumRawDOF();
	//	int numInt = (int)vInterIdx.size();
	//	int numBou = (int)vBoundIdx.size();

	//	int rawOff = pTSModel->getSurfaceModelRawOffset(i);

	//	// Build selection matrices

	//	vector<iVector> vmapSim2Mem(numSim);
	//	for (int j = 0; j < numMem; ++j)
	//		vmapSim2Mem[vmapRaw2Sim[rawOff + j]].push_back(j);

	//	vector<iVector> vmapMem2Int(numMem);
	//	for (int j = 0; j < numInt; ++j)
	//	{
	//		vmapMem2Int[3 * vInterIdx[j] + 0].push_back(3 * j + 0);
	//		vmapMem2Int[3 * vInterIdx[j] + 1].push_back(3 * j + 1);
	//		vmapMem2Int[3 * vInterIdx[j] + 2].push_back(3 * j + 2);
	//	}
	//	vector<iVector> vmapMem2Bou(numMem);
	//	for (int j = 0; j < numBou; ++j)
	//	{
	//		vmapMem2Bou[3 * vBoundIdx[j] + 0].push_back(3 * j + 0);
	//		vmapMem2Bou[3 * vBoundIdx[j] + 1].push_back(3 * j + 1);
	//		vmapMem2Bou[3 * vBoundIdx[j] + 2].push_back(3 * j + 2);
	//	}

	//	buildSelectionMatrix(vmapSim2Mem, this->m_vmSim2Mem[i], true);
	//	buildSelectionMatrix(vmapMem2Int, this->m_vmMem2Int[i], true);
	//	buildSelectionMatrix(vmapMem2Bou, this->m_vmMem2Bou[i], true);
	//}
}

void ModelParameterization::testParameterLinearization()
{
	// Test all parameters linearization
	for (int i = 0; i < this->m_ns; ++i)
		this->m_vpset[i]->test_DfDp();
}

// Param --------------------------------------------------------------------------

Real ModelParameterization::Param_computeObjective()
{
	Real energy = 0;

	for (int i = 0; i < this->m_ns; ++i)
	{
		energy += this->m_vpset[i]->computeParameterRegularizer_Potential(this->m_vpRaw);
	}

	logSimu("[INFO] /t-Regularizer potential: %.9f", energy);

	return energy;

	//return 0;
}

void ModelParameterization::Param_computeGradient(VectorXd& vgParam)
{
	vgParam.resize(this->m_np);
	vgParam.setZero();

	for (int i = 0; i < this->m_ns; ++i)
	{
		int np_i = this->m_vpset[i]->getNumParameter();

		int setOff = this->m_vsetOff[i];

		VectorXd vpRaw_i, vgPar_i;
		getSubvector(setOff, np_i, m_vpRaw, vpRaw_i);
		this->m_vpset[i]->computeParameterRegularizer_Gradient(vpRaw_i, vgPar_i);

		for (int j = 0; j < np_i; ++j) // Add
			vgParam(setOff + j) = vgPar_i(j);
	}

	vgParam = this->get_Pt().transpose()*this->getParametersScaleD().transpose()*vgParam;

	//vgParam.setZero();
}

void ModelParameterization::Param_computeHessian(MatrixSd& mHParam)
{
	tVector vH;

	for (int i = 0; i < this->m_ns; ++i)
	{
		int np_i = this->m_vpset[i]->getNumParameter();

		int setOff = this->m_vsetOff[i];

		tVector vHPar_i;
		VectorXd vpRaw_i;
		getSubvector(setOff, np_i, m_vpRaw, vpRaw_i);
		this->m_vpset[i]->computeParameterRegularizer_Hessian(vpRaw_i, vHPar_i);

		int numCoeff = (int) vHPar_i.size();
		vH.reserve((int) vH.size() + numCoeff);

		for (int j = 0; j < numCoeff; ++j) // Offset
		{
			vH.push_back(Triplet<Real>(vHPar_i[j].row() + setOff,
									   vHPar_i[j].col() + setOff,
									   vHPar_i[j].value()));
		}
	}

	mHParam = MatrixSd(this->m_np, this->m_np);
	mHParam.setFromTriplets(vH.begin(), vH.end());
	mHParam.makeCompressed();

	mHParam = mHParam*this->getParametersScaleD()*this->get_Pt();

	//mHParam.setZero();
}


// Seams --------------------------------------------------------------------------

Real ModelParameterization::Goal_computeObjective(const VectorXd& vt)
{
	VectorXd vx = m_pModel->getPositions_x();

	logSimu("[EXTRA TRACE] Distance to target: %.9f", (vt - vx).norm());

	VectorXd vdt = this->get_St()*(vt - vx);

	Real energy = 0.5*vdt.squaredNorm();

	logSimu("[INFO] /t-Position goals potential: %.9f", energy);

	//logSimu("[INFO] Goals distance: %.9f", vdt.norm());
	//logSimu("[INFO] Goals potential: %.9f", energy);

	return energy;
}

void ModelParameterization::Goal_computeGradient(const VectorXd& vt, VectorXd& vgGoal)
{
	MatrixSd mDxDpP = this->get_DxDp_Pro()*this->get_Pt();

	vgGoal = -mDxDpP.transpose()*(this->get_St().transpose()*(this->get_St()*(vt - m_pModel->getPositions_x())));

	//logSimu("[INFO] Goals gradient: %.9f", vgGoal.norm());
}

void ModelParameterization::Goal_computeHessian(const VectorXd& vdt, MatrixSd& mHGoal)
{
	mHGoal = this->get_DxDpS2();
}

// Seams --------------------------------------------------------------------------

Real ModelParameterization::Seam_computeObjective()
{
	const VectorXd& vx = m_pModel->getPositions_x();

	VectorXd vs0 = this->get_Ss0()*vx;
	VectorXd vs1 = this->get_Ss1()*vx;
	VectorXd vds = (vs0 - vs1);

	Real energy = 0.5*vds.squaredNorm();

	logSimu("[INFO] /t-Seam goals potential: %.9f", energy);

	//logSimu("[INFO] Seams distance: %.9f", vds.norm());
	//logSimu("[INFO] Seams potential: %.9f", energy);

	return energy;
}

void ModelParameterization::Seam_computeGradient(VectorXd& vgSeam)
{
	const VectorXd& vx = m_pModel->getPositions_x();

	VectorXd vs0 = this->m_mS0s*vx;
	VectorXd vs1 = this->m_mS1s*vx;
	VectorXd vds = (vs0 - vs1);

	vgSeam = 
		(this->get_DxDp_Pro().transpose()*(this->m_mS0s.transpose()*vds)) - 
		(this->get_DxDp_Pro().transpose()*(this->m_mS1s.transpose()*vds));

	//logSimu("[INFO] Seams gradient: %.9f", vgSeam.norm());
}

void ModelParameterization::Seam_computeHessian(MatrixSd& mHSeam)
{
	MatrixSd mHS = this->get_DxDp_Pro().transpose()*this->get_Ss0().transpose() -
						 this->get_DxDp_Pro().transpose()*this->get_Ss1().transpose();
	mHSeam = mHS*mHS.transpose();
}

// Hinges -------------------------------------------------------------------------

//Real ModelParameterization::Hinge_computeObjective()
//{
//	const VectorXd& vx = m_pModel->getPositions_x();
//
//	Real energy = 0;
//
//	int nh = (int) m_vSh.size();
//	for (int i = 0; i < nh; ++i)
//	{
//		VectorXd vxH = m_vSh[i]*vx;
//		VectorXd vvH(vxH.size());
//		vvH.setZero();
//		this->m_hingeElement.update_Energy(vxH, vvH);
//		energy += m_hingeElement.getEnergyIntegral();
//	}
//
//	logSimu("[INFO] /t-Hinge goals potential: %.9f", energy);
//
//	return energy;
//}

//void ModelParameterization::Hinge_computeGradient(VectorXd& vgHinge)
//{
//	const VectorXd& vx = m_pModel->getPositions_x();
//
//	vgHinge.resize(this->m_np);
//
//	vgHinge.setZero();
//
//	int nh = (int)m_vSh.size();
//	for (int i = 0; i < nh; ++i)
//	{
//		VectorXd vxH = m_vSh[i]*vx;
//		VectorXd vvH(vxH.size());
//		vvH.setZero();
//
//		this->m_hingeElement.update_Force(vxH, vvH);
//
//		VectorXd vf((int)vxH.size());
//		vf.setZero();
//		m_hingeElement.addForce(vf);
//
//		vgHinge -= this->get_DxDp_Pro().transpose()*(m_vSh[i].transpose()*vf);
//	}
//
//	//logSimu("[INFO] Hinge gradient: %.9f", vgHinge.norm());
//}

//void ModelParameterization::Hinge_computeHessian(MatrixSd& mHHinge)
//{
//	const VectorXd& vx = m_pModel->getPositions_x();
//
//	mHHinge.resize(this->m_np, this->m_np);
//
//	mHHinge.setZero();
//
//	int nh = (int)m_vSh.size();
//	for (int i = 0; i < nh; ++i)
//	{
//		VectorXd vxH = m_vSh[i]*vx;
//		VectorXd vvH((int)vxH.size());
//		vvH.setZero();
//
//		this->m_hingeElement.update_Jacobian(vxH, vvH);
//
//		tVector vJ;
//		vJ.reserve(81);
//		m_hingeElement.addJacobian(vJ);
//		MatrixSd mJacobian(9, 9);
//		mJacobian.setFromTriplets(vJ.begin(), vJ.end());
//		mJacobian.makeCompressed();
//
//		MatrixSd mDfxDxs = mJacobian.selfadjointView<Lower>();
//
//		MatrixSd mDxDpS = m_vSh[i]*get_DxDp_Pro();
//		mHHinge -= mDxDpS.transpose()*mDfxDxs*mDxDpS;
//	}
//}

// Position-based objectives ------------------------------------------------------

Real ModelParameterization::PB_computeObjective(const VectorXd& vt)
{
	Real error = 0;

	error += this->Goal_computeObjective(vt);
		//this->m_seamNodeK*this->Seam_computeObjective() +
		//this->m_hingeNodeK*this->Hinge_computeObjective() +
		//this->m_equiNodeK*this->Param_computeObjective();

	return error;
}

void ModelParameterization::PB_computeDefaultGradient(const VectorXd& vt, VectorXd& vgDef)
{
	this->Goal_computeGradient(vt, vgDef);

	//VectorXd vgSeam;
	//VectorXd vgHinge;
	//VectorXd vgParam;
	//this->Seam_computeGradient(vgSeam);
	//this->Hinge_computeGradient(vgHinge);
	//this->Param_computeGradient(vgParam);
	//vgDef += vgSeam*this->m_seamNodeK;
	//vgDef += vgHinge*this->m_hingeNodeK;
	//vgDef += vgParam*this->m_equiNodeK;
}

void ModelParameterization::PB_computeBoundedGradient(const VectorXd& vt, VectorXd& vgCon)
{
	this->PB_computeDefaultGradient(vt, vgCon);

	const VectorXd vpMax = this->getParametersUpperBound_Pro();
	const VectorXd vpMin = this->getParametersLowerBound_Pro();
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(this->m_vpPro[i], vpMax[i], 1e-6) && vgCon[i] > 0)
			vgCon[i] = 0.0; // This is not going higher, make zero
		
		if (isApprox(this->m_vpPro[i], vpMin[i], 1e-6) && vgCon[i] < 0)
			vgCon[i] = 0.0; // This is not going lower, make zero
	}
}

void ModelParameterization::PB_computeHessian(const VectorXd& vt, MatrixSd& mH)
{
	MatrixSd mHGoal;
	MatrixSd mHSeam;
	MatrixSd mHHinge;
	MatrixSd mHParam;
	this->Goal_computeHessian(vt, mHGoal);
	//this->Hinge_computeHessian(mHHinge);
	//this->Seam_computeHessian(mHSeam);
	//this->Param_computeHessian(mHParam);
	mH = mHGoal;
		//this->m_seamNodeK*mHSeam +
		//this->m_hingeNodeK*mHHinge +
		//this->m_equiNodeK*mHParam;
}

void ModelParameterization::PB_computeDefaultGradient_FD(const VectorXd& vt, VectorXd& vgDef)
{
	Real solverError = this->m_pSolver->getSolverError();
	int solverIters = this->m_pSolver->getSolverMaxIters();

	VectorXd vpFD = this->getParameters_Pro();

	pSolidState pStateIni = this->m_pModel->getState_x()->clone();

	vgDef.resize(this->m_np);

	// Set the sensitivity of the solver
	this->m_pSolver->setSolverError(1e-12);
	this->m_pSolver->setSolverMaxIters(100);

	for (int i = 0; i < this->m_np; ++i)
	{
		this->m_pModel->setState_x(pStateIni);

		// Plus
		vpFD(i) += 1e-6;
		this->setParameters_Pro(vpFD);
		this->m_pSolver->solve();
		Real errorp = 0.5*(this->m_pModel->getPositions_x() - vt).squaredNorm();

		this->m_pModel->setState_x(pStateIni);

		// Minus
		vpFD(i) -= 2e-6;
		this->setParameters_Pro(vpFD);
		this->m_pSolver->solve();
		Real errorm = 0.5*(this->m_pModel->getPositions_x() - vt).squaredNorm();

		// Estimate
		vgDef(i) = (errorp - errorm) / 2e-6;

		vpFD(i) += 1e-6;
	}

	this->setParameters_Pro(vpFD);

	this->m_pModel->setState_x(pStateIni);

	// Restore the original solver sensitivity
	this->m_pSolver->setSolverError(solverError);
	this->m_pSolver->setSolverMaxIters(solverIters);
}

void ModelParameterization::PB_computeBoundedGradient_FD(const VectorXd& vt, VectorXd& vgCon)
{
	this->PB_computeDefaultGradient_FD(vt, vgCon);

	const VectorXd vpMax = this->getParametersUpperBound_Pro();
	const VectorXd vpMin = this->getParametersLowerBound_Pro();
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(this->m_vpPro[i], vpMax[i], 1e-6) && vgCon[i] > 0)
			vgCon[i] = 0.0; // This is not going higher, make zero

		if (isApprox(this->m_vpPro[i], vpMin[i], 1e-6) && vgCon[i] < 0)
			vgCon[i] = 0.0; // This is not going lower, make zero
	}
}

void ModelParameterization::PB_testObjectiveGradient(const VectorXd& vt)
{
	VectorXd vgA;
	VectorXd vgF;
	this->PB_computeDefaultGradient(vt, vgA);
	this->PB_computeDefaultGradient_FD(vt, vgF);

	VectorXd vgD = vgA-vgF;
	Real normF = vgF.norm();
	Real normD = vgD.norm();
	Real errorRel = normD / normF;

	if (errorRel > 1e-9)
	{
		logSimu("[ERROR] Error computing the objective gradient: %.9f", errorRel);
		//writeToFile(vgF, "objectiveGradientF.csv");
		//writeToFile(vgA, "objectiveGradientA.csv");
	}
}

bool ModelParameterization::PB_computeParameterStep_CQP(const VectorXd& vt, VectorXd& vdp, string& error)
{
	logSimu("[INFO] Computing step using CQP (Box-Constrained QP)");

	assert((int) vt.size() == this->m_pModel->getNumSimDOF_x());

	// Solve the problem min q(p) = 1/2 (Sx - St)^T(Sx - St) which is a quadratic
	// problem of the form: min q(p) ~ 1/2 p^T * G * p + p^T * c where coefficients:
	//		* G = A^T*S^T*S*A
	//		* c = -A^T*S^T*S*t
	// And:
	//		* A = Dx/Dp
	//
	// The matrix G is probably singular if rank(S) < rank(A) or rank(S) > rank(A)
	// but the number of design parameters is greater than the number of defomration
	// DOF. We trade-off some convergence regularizing the matrix.

	// Create the vector/matrix of linear/quadratic coefficients

	VectorXd vc;
	MatrixSd mGs;
	this->PB_computeHessian(vt, mGs);
	this->PB_computeDefaultGradient(vt, vc);

	// Get the vectors of parameter bounds

	const VectorXd& vpub = this->getParametersUpperBound_Pro();
	const VectorXd& vplb = this->getParametersLowerBound_Pro();

	VectorXd vdpUB = vpub - this->m_vpPro;
	VectorXd vdpLB = vplb - this->m_vpPro;

	// Solve QP

	AlgLib_Sparse_BoxC_QP QPsolver;

	tVector tGs;

	VectorXd vx0(this->m_np);
	vx0.setZero(); // Init.

	VectorXd vxo(this->m_np);
	vxo.setZero(); // Init.

	toTriplets(mGs, tGs);

	bool solved = QPsolver.solve(tGs, vc, vx0, vdpLB, vdpUB, vxo, error);

	if (solved)
	{
		// Copy
		vdp = vxo;

		if ((-vc).dot(vdp) < 0.0)
		{
			logSimu("[WARNING] Non-descendent direction (CQP): %.9f", (-vc).dot(vdp));

			//writeToFile(mGs, "hessianCQP.csv");

			return false;
		}
	}

	return solved;
}

bool ModelParameterization::PB_computeParameterStep_UQP_ProjectGrad(const VectorXd& vt, VectorXd& vdp, string& error)
{
	logSimu("[INFO] Computing step using UQP-Grad (Unconstrained QP Gradient Projection)");

	assert((int) vt.size() == this->m_pModel->getNumSimDOF_x());

	VectorXd vgCon;
	MatrixSd mGs;
	this->PB_computeHessian(vt, mGs);
	this->PB_computeDefaultGradient(vt, vgCon);

	VectorXd vgConNeg = -1.0*vgCon;
	
	//writeToFile(mGs, "hessianSinCon.csv");

	MatrixSd mI(this->m_np, this->m_np);
	mI.setIdentity();

	//mGs += mI;
	mGs += this->m_mKs.transpose()*this->m_mKs;

	tVector vGs;
	toTriplets(mGs, vGs);
	this->removeRows(vGs, this->m_vParamStencil);
	this->removeCols(vGs, this->m_vParamStencil);
	this->addDiagonal(vGs, this->m_vParamStencil, 1.0);

	mGs.setFromTriplets(vGs.begin(), vGs.end());
	mGs.makeCompressed();

	//writeToFile(mGs, "hessianConCon.csv");

	//MatrixSd mR(this->m_np, this->m_np);
	//mR.setIdentity();
	//mGs += mR*0.001;

	PhysSolver::SolveLinearSystem(mGs, vgConNeg, vdp);

	if (vgConNeg.dot(vdp) < 0.0)
	{
		logSimu("[WARNING] Non-descendent direction (UQP Project Grad): %.9f", vgConNeg.dot(vdp));

		//writeToFile(mGs, "hessianUQPGrad.csv");

		return false;
	}

	return true;
}

bool ModelParameterization::PB_computeParameterStep_UQP_ProjectStep(const VectorXd& vt, VectorXd& vdp, string& error)
{
	logSimu("[INFO] Computing step using UQP-Step (Unconstrained QP Step Projection)");

	assert((int) vt.size() == this->m_pModel->getNumSimDOF_x());

	VectorXd vgDef;
	MatrixSd mGs;
	this->PB_computeHessian(vt, mGs);
	this->PB_computeBoundedGradient(vt, vgDef);

	//MatrixSd mR(this->m_np, this->m_np);
	//mR.setIdentity();
	//mGs += mR*0.001;

	VectorXd vgDefNeg = -1.0*vgDef;

	PhysSolver::SolveLinearSystem(mGs, vgDefNeg, vdp);

	if (vgDefNeg.dot(vdp) < 0.0)
	{
		logSimu("[WARNING] Non-descendent direction (UQP Project Step): %.9f", vgDefNeg.dot(vdp));

		//writeToFile(mGs, "hessianUQPStep.csv");

		return false;
	}

	return true;
}

// Parameterization ---------------------------------------------------------------

void ModelParameterization::capParameterValue(VectorXd& vp)
{
	const VectorXd& vpMax = this->getParametersUpperBound_Pro();
	const VectorXd& vpMin = this->getParametersLowerBound_Pro();

	for (int i = 0; i < (int) vp.size(); ++i)
	{
		if (vp(i) < vpMin(i))
			vp(i) = vpMin(i);

		if (vp(i) > vpMax(i))
			vp(i) = vpMax(i);
	}
}

void ModelParameterization::capParameterStep(VectorXd& vdp)
{
	VectorXd vpNew = this->getParameters_Pro() + vdp;
	const VectorXd& vpMax = this->getParametersUpperBound_Pro();
	const VectorXd& vpMin = this->getParametersLowerBound_Pro();

	for (int i = 0; i < (int)vpNew.size(); ++i)
	{
		if (vpNew(i) < vpMin(i))
			vpNew(i) = vpMin(i);

		if (vpNew(i) > vpMax(i))
			vpNew(i) = vpMax(i);
	}

	vdp = vpNew - this->getParameters_Pro();
}

void ModelParameterization::setParameters_Pro(const VectorXd& vpPro)
{
	assert((int) vpPro.size() == this->m_np);

	logSimu("[INFO] Taking a step of size (Pro): %.9f", (vpPro - this->m_vpPro).norm());

	VectorXd vpRaw = vpPro;

	if (this->m_parameterScaling)
	{
		vpRaw = this->getParametersScaleD()*vpRaw;
	}

	if (this->m_parameterMapping)
	{
		VectorXd vpUnc = vpRaw;

		this->mapParameters_Unconstrained2Raw(vpUnc, vpRaw);
	}

	logSimu("[INFO] Taking a step of size (Raw): %.9f", (vpRaw - this->m_vpRaw).norm());

	for (int i = 0; i < this->m_ns; ++i)
	{
		VectorXd vpiOld, vpiNew;
		int npi = this->m_vpset[i]->getNumParameter();
		getSubvector(this->m_vsetOff[i], npi, vpRaw, vpiNew);
		getSubvector(this->m_vsetOff[i], npi, m_vpRaw, vpiOld);
		this->m_vpset[i]->setParameters(vpiNew);

		logSimu("[INFO] Set %u. Taking a step of size (Raw): %.9f", i, (vpiOld - vpiNew).norm());
	}

	this->m_vpPro = vpPro;
	this->m_vpRaw = vpRaw;

	this->m_isReady_fp = false;
	this->m_isReady_DfDp = false;
	this->m_isReady_DfDx = false;
}

const VectorXd& ModelParameterization::getParameters_Raw()
{
	this->m_vpRaw.resize(this->m_np);
	this->m_vpRaw.setZero(); // Init.

	for (int i = 0; i < this->m_ns; ++i)
	{
		VectorXd vpi;
		this->m_vpset[i]->getParameters(vpi);
		int npi = this->m_vpset[i]->getNumParameter();
		setSubvector(m_vsetOff[i], npi, vpi, m_vpRaw);
	}

	return this->m_vpRaw;
}

const VectorXd& ModelParameterization::getParametersLowerBound_Raw() const
{
	return this->m_vpLBRaw;
}

const VectorXd& ModelParameterization::getParametersUpperBound_Raw() const
{
	return this->m_vpUBRaw;
}

void ModelParameterization::mapParameters_Raw2Unconstrained(const VectorXd& vpRaw, VectorXd& vpUnc) const
{
	const VectorXd& vpUB = this->getParametersUpperBound_Raw();
	const VectorXd& vpLB = this->getParametersLowerBound_Raw();

	vpUnc.resize(this->m_np);

	for (int i = 0; i < this->m_np; ++i)
	{
		Real ui = vpUB(i);
		Real li = vpLB(i);
		Real oi = (ui - li) / 2;
		vpUnc(i) = ((ui - li) / M_PI)*atan(vpRaw(i) - oi) + oi;
	}
}

void ModelParameterization::mapParameters_Unconstrained2Raw(const VectorXd& vpUnc, VectorXd& vpRaw) const
{
	const VectorXd& vpUB = this->getParametersUpperBound_Raw();
	const VectorXd& vpLB = this->getParametersLowerBound_Raw();

	vpRaw.resize(this->m_np);

	for (int i = 0; i < this->m_np; ++i)
	{
		Real ui = vpUB(i);
		Real li = vpLB(i);
		Real oi = (ui - li)/2;
		vpRaw(i) = tan((vpUnc(i) - oi)*(M_PI / (ui - li))) + oi;
	}
}

const VectorXd& ModelParameterization::getParameters_Pro()
{

	const VectorXd& vpRaw = this->getParameters_Raw();

	VectorXd vpPro = vpRaw;

	if (this->m_parameterMapping)
	{
		VectorXd vpRaw = vpPro;

		this->mapParameters_Raw2Unconstrained(vpRaw, vpPro);
	}

	if (this->m_parameterScaling)
	{
		vpPro = this->getParametersScaleI()*vpPro;
	}

	this->m_vpPro = vpPro;

	return this->m_vpPro;
}

VectorXd ModelParameterization::randomizeParameters()
{
	VectorXd vp(this->m_np);
	vp.setZero(); // Init.

	const VectorXd& vpMax = this->getParametersUpperBound_Pro();
	const VectorXd& vpMin = this->getParametersLowerBound_Pro();

	srand(time(NULL));

	for (int i = 0; i < this->m_np; ++i)
	{
		Real alpha = (double)rand() / (double)RAND_MAX;
		if (vpMax[i] != HUGE_VAL && vpMin[i] != -HUGE_VAL)
		{
			Real minp = -0.01;
			Real maxp = +0.01;
			Real range = maxp - minp;
			Real dp = minp + alpha*range;
			vp(i) = this->m_vpPro(i) + dp;
		}
		else
		{
			Real minp = vpMin(i);
			Real maxp = vpMax(i);
			Real range = maxp - minp;
			vp(i) = minp + alpha*range;
		}
	}

	return vp;
}

void ModelParameterization::updateBoundsVector()
{
	// Set lower and upper bounds

	this->m_vpLBRaw.resize(this->m_np);
	this->m_vpUBRaw.resize(this->m_np);

	for (int i = 0; i < this->m_ns; ++i)
	{
		this->m_vpset[i]->updateBoundsVector();
		int Ni = this->m_vpset[i]->getNumParameter();
		const VectorXd& vlbRaw = this->m_vpset[i]->getMinimumValues();
		const VectorXd& vubRaw = this->m_vpset[i]->getMaximumValues();
		for (int j = 0; j < Ni; ++j)
		{
			this->m_vpLBRaw(this->m_vsetOff[i] + j) = vlbRaw(j);
			this->m_vpUBRaw(this->m_vsetOff[i] + j) = vubRaw(j);
		}
	}
}

const VectorXd& ModelParameterization::getParametersLowerBound_Pro()
{
	this->m_vpLBPro = this->m_vpLBRaw;

	if (this->m_parameterScaling)
	{
		for (int i = 0; i < this->m_np; ++i)
		{
			this->m_vpLBPro(i) = -HUGE_VAL;
		}
	}

	if (this->m_parameterMapping)
	{
		this->m_vpLBPro = this->getParametersScaleI()*this->m_vpLBPro;
	}

	return this->m_vpLBPro;
}

const VectorXd& ModelParameterization::getParametersUpperBound_Pro()
{
	this->m_vpUBPro = this->m_vpUBRaw;

	if (this->m_parameterScaling)
	{
		for (int i = 0; i < this->m_np; ++i)
		{
			this->m_vpUBPro(i) = +HUGE_VAL;
		}
	}

	if (this->m_parameterMapping)
	{
		this->m_vpUBPro = this->getParametersScaleI()*this->m_vpUBPro;
	}


	return this->m_vpUBPro;
}

bool ModelParameterization::isValidParameters(const VectorXd& vp, VectorXd& vpBD, double tol)
{
	bool valid = true;

	vpBD.resize(this->m_np);
	vpBD.setZero(); // Init.

	const VectorXd& vpUB = this->getParametersUpperBound_Pro();
	const VectorXd& vpLB = this->getParametersLowerBound_Pro();

	for (int i = 0; i < this->m_np; ++i)
	{
		if (vpLB(i) - vp[i] > tol)
		{
			vpBD[i] = vpLB(i) - vp[i];
			valid &= false;
		}
		if (vp[i] - vpUB(i) > tol)
		{
			vpBD[i] = vp[i] - vpUB(i);
			valid &= false;
		}
	}

	return valid;
}

// Matrices -------------------------------------------------------------------------

const vector<MatrixSd>& ModelParameterization::get_Sh()
{
	if (!this->isReady_St())
		this->update_St();

	return this->m_vSh;
}

const MatrixSd& ModelParameterization::get_Ss0()
{
	if (!this->isReady_St())
		this->update_St();

	return this->m_mS0s;
}

const MatrixSd& ModelParameterization::get_Ss1()
{
	if (!this->isReady_St())
		this->update_St();

	return this->m_mS1s;
}

const MatrixSd& ModelParameterization::get_St()
{
	if (!this->isReady_St())
		this->update_St();

	return this->m_mSs;
}

const MatrixSd& ModelParameterization::get_Pt()
{
	if (!this->isReady_Pt())
		this->update_Pt();

	return this->m_mPs;
}

const VectorXd& ModelParameterization::get_fp()
{
	if (!this->isReady_fp())
		this->update_fp();

	return this->m_vfp;
}

const MatrixSd& ModelParameterization::get_DfDx()
{
	if (!this->isReady_DfDx())
		this->update_DfDx();

	return this->m_mDfDxs;
}

const MatrixSd& ModelParameterization::get_DfDp()
{
	if (!this->isReady_DfDp())
		this->update_DfDp();

	return this->m_mDfDps;
}

const MatrixSd& ModelParameterization::get_DxDp_Pro()
{
	if (!this->isReady_DxDp())
		this->update_DxDp();

	return this->m_mDxDpsPro;
}

const MatrixSd& ModelParameterization::get_DxDp_Raw()
{
	if (!this->isReady_DxDp())
		this->update_DxDp();

	return this->m_mDxDpsRaw;
}

const MatrixSd& ModelParameterization::get_DpDg()
{
	if (!this->isReady_DpDg())
		this->update_DpDg();

	return this->m_mDpDgs;
}

const MatrixSd& ModelParameterization::get_DxDg()
{
	if (!this->isReady_DxDg())
		this->update_DxDg();

	return this->m_mDxDgs;
}

const MatrixSd& ModelParameterization::get_DfDp2()
{
	if (!this->isReady_DfDp2())
		this->update_DfDp2();

	return this->m_mDfDp2s;
}

const MatrixSd& ModelParameterization::get_DxDp2()
{
	if (!this->isReady_DxDp2())
		this->update_DxDp2();

	return this->m_mDxDp2s;
}

const MatrixSd& ModelParameterization::get_DxDpS2()
{
	if (!this->isReady_DxDpS2())
		this->update_DxDpS2();

	return this->m_mDxDpS2s;
}

const MatrixSd& ModelParameterization::get_DfDxp()
{
	if (!this->isReady_DfDxp())
		this->update_DfDxp();

	return this->m_mDfDxps;
}

const MatrixSd& ModelParameterization::get_DfDxp2()
{
	if (!this->isReady_DfDxp2())
		this->update_DfDxp2();

	return this->m_mDfDxp2s;
}

bool ModelParameterization::isReady_St() const
{
	return this->m_isReady_St;
}

bool ModelParameterization::isReady_Pt() const
{
	return this->m_isReady_Pt;
}

bool ModelParameterization::isReady_fp() const
{
	bool isParReady = true;
	for (int i = 0; i < this->m_ns; ++i)
		isParReady &= this->m_vpset[i]->isReady_fp();

	if (!isParReady)
		return false;

	return this->m_isReady_fp;
}

bool ModelParameterization::isReady_DfDx() const
{
	return this->m_isReady_DfDx;
}

bool ModelParameterization::isReady_DfDp() const
{
	bool isParReady = true;
	for (int i = 0; i < this->m_ns; ++i)
		isParReady &= this->m_vpset[i]->isReady_DfDp();

	if (!isParReady)
		return false;

	return this->m_isReady_DfDp;
}

bool ModelParameterization::isReady_DxDp() const
{
	return this->isReady_DfDp() && this->m_isReady_DxDp;
}

bool ModelParameterization::isReady_DpDg() const
{
	return this->isReady_DfDp() && this->m_isReady_DpDg;
}

bool ModelParameterization::isReady_DxDg() const
{
	return this->isReady_DfDp() && this->m_isReady_DxDg;
}

bool ModelParameterization::isReady_DfDp2() const
{
	return this->isReady_DfDp() && this->m_isReady_DfDp2;
}

bool ModelParameterization::isReady_DxDp2() const
{
	return this->isReady_DfDp() && this->m_isReady_DxDp2;
}

bool ModelParameterization::isReady_DxDpS2() const
{
	return this->isReady_DfDp() && this->m_isReady_DxDpS2;
}


bool ModelParameterization::isReady_DfDxp() const
{
	return this->isReady_DfDp() && this->m_isReady_DfDxp;
}

bool ModelParameterization::isReady_DfDxp2() const
{
	return this->isReady_DfDp() && this->m_isReady_DfDxp2;
}

void ModelParameterization::setDOFFixed(const iVector& vidx)
{
	assert((int)vidx.size() <= this->m_pModel->getNumRawDOF());

	this->m_vDOFFixedRaw = vidx;

	// Build stencil

	const iVector& vsimIdx = this->m_pModel->getRawToSim_x();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();
	this->m_vFixedStencil.assign(nsimDOF, false);

	for (int i = 0; i < (int) this->m_vDOFFixedRaw.size(); ++i)
	{
		if (vsimIdx[this->m_vDOFFixedRaw[i]] == -1)
			continue; // This DOF is not simulated

		m_vFixedStencil[vsimIdx[this->m_vDOFFixedRaw[i]]] = true;
	}

	this->m_isReady_DfDx = false;
	this->m_isReady_DfDp = false;
}

void ModelParameterization::setDOFHinges(const iVector& vidx)
{
	assert((int) vidx.size() <= this->m_pModel->getNumRawDOF());

	assert((int) vidx.size() % 3 == 0);

	this->m_vDOFHingeRaw = vidx;

	this->m_isReady_St = false;
}

void ModelParameterization::setDOFSeams(const iVector& vidx)
{
	assert((int)vidx.size() <= this->m_pModel->getNumRawDOF());

	assert((int)vidx.size() % 2 == 0);

	int ns = (int) vidx.size() / 2;
		
	this->m_vDOFSeamsRaw0.resize(ns);
	this->m_vDOFSeamsRaw1.resize(ns);

	for (int i = 0; i < ns; ++i)
	{
		this->m_vDOFSeamsRaw0[i] = vidx[2 * i + 0];
		this->m_vDOFSeamsRaw1[i] = vidx[2 * i + 1];
	}

	// Build stencil

	const iVector& vsimIdx = this->m_pModel->getRawToSim_x();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();
	this->m_vSeamsStencil0.assign(nsimDOF, false);
	this->m_vSeamsStencil1.assign(nsimDOF, false);

	for (int i = 0; i < ns; ++i)
	{
		if (vsimIdx[this->m_vDOFSeamsRaw0[i]] == -1)
			continue; // This DOF is not simulated

		if (vsimIdx[this->m_vDOFSeamsRaw1[i]] == -1)
			continue; // This DOF is not simulated

		m_vSeamsStencil0[vsimIdx[this->m_vDOFSeamsRaw0[i]]] = true;
		m_vSeamsStencil1[vsimIdx[this->m_vDOFSeamsRaw1[i]]] = true;
	}

	this->m_isReady_St = false;
}

void ModelParameterization::setDOFGoals(const iVector& vidx, const dVector& vwei)
{
	assert((int) vidx.size() <= this->m_pModel->getNumRawDOF());

	this->m_vDOFGoalsRaw = vidx;
	this->m_vDOFGoalsWei = vwei;

	// Build stencil

	const iVector& vsimIdx = this->m_pModel->getRawToSim_x();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();
	this->m_vGoalsStencil.assign(nsimDOF, false);

	for (int i = 0; i < (int) this->m_vDOFGoalsRaw.size(); ++i)
	{
		if (vsimIdx[this->m_vDOFGoalsRaw[i]] == -1)
			continue; // This DOF is not simulated

		m_vGoalsStencil[vsimIdx[this->m_vDOFGoalsRaw[i]]] = true;
	}

	this->m_isReady_St = false;
}

void ModelParameterization::setDOFTargets(const iVector& vidx)
{
	assert((int)vidx.size() <= this->m_pModel->getNumRawDOF());

	this->m_vDOFTargetsRaw = vidx;

	// Build stencil

	const iVector& vsimIdx = this->m_pModel->getRawToSim_x();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();
	this->m_vTargetsStencil.assign(nsimDOF, false);

	for (int i = 0; i < (int) this->m_vDOFTargetsRaw.size(); ++i)
	{
		if (vsimIdx[this->m_vDOFTargetsRaw[i]] == -1)
			continue; // This DOF is not simulated

		m_vTargetsStencil[vsimIdx[this->m_vDOFTargetsRaw[i]]] = true;
	}

	this->m_isReady_Pt = false;
}

const iVector& ModelParameterization::getDOFSeams0() const
{
	return this->m_vDOFSeamsRaw0;
}

const iVector& ModelParameterization::getDOFSeams1() const
{
	return this->m_vDOFSeamsRaw1;
}

const iVector& ModelParameterization::getDOFFixed() const
{
	return this->m_vDOFFixedRaw;
}

const iVector& ModelParameterization::getDOFGoalIndices() const
{
	return this->m_vDOFGoalsRaw;
}

const dVector& ModelParameterization::getDOFGoalWeigths() const
{
	return this->m_vDOFGoalsWei;
}

const iVector& ModelParameterization::getDOFTargets() const
{
	return this->m_vDOFTargetsRaw;
}

void ModelParameterization::update_St()
{
	if (this->isReady_St())
		return; // Updated

	tVector vSs;
	
	this->m_mSs = MatrixSd(this->m_nx, this->m_nx);

	iVector vDOFGoalsSim;
	int ngoalDOF = (int) this->m_vDOFGoalsRaw.size();
	this->m_pModel->mapRaw2Sim(this->m_vDOFGoalsRaw, vDOFGoalsSim);

	if (ngoalDOF != 0)
	{
		this->m_mSs.setZero();

		for (int i = 0; i < ngoalDOF; ++i)
		{
			int d = vDOFGoalsSim[i];

			if (d == -1)
				continue;

			vSs.push_back(Triplet<Real>(d, d, this->m_vDOFGoalsWei[i]));
		}
	}
	else
	{
		this->m_mSs.setZero();
	}

	this->m_mSs = MatrixSd(this->m_nx, this->m_nx);
	this->m_mSs.setFromTriplets(vSs.begin(), vSs.end());
	this->m_mSs.makeCompressed();

	//// Create seams matrices

	//const iVector& vsimIdx = this->m_pModel->getRawToSim_x();

	//int nsimDOF = this->m_pModel->getNumSimDOF_x();
	//int nseamsDOF0 = (int)m_vDOFSeamsRaw0.size();
	//int nseamsDOF1 = (int)m_vDOFSeamsRaw1.size();

	//assert(nseamsDOF0 == nseamsDOF1);
	//vector<iVector> vDOFSeamsSim0(nsimDOF);
	//vector<iVector> vDOFSeamsSim1(nsimDOF);
	//for (int i = 0; i < nseamsDOF0; ++i)
	//{
	//	vDOFSeamsSim0[vsimIdx[m_vDOFSeamsRaw0[i]]].push_back(i);
	//	vDOFSeamsSim1[vsimIdx[m_vDOFSeamsRaw1[i]]].push_back(i);
	//}
	//buildSelectionMatrix(vDOFSeamsSim0, this->m_mS0s, true);
	//buildSelectionMatrix(vDOFSeamsSim1, this->m_mS1s, true);

	//// Create hinge matrices

	//int nh = (int) this->m_vDOFHingeRaw.size() / 9;
	//this->m_vSh.resize(nh);
	//for (int i = 0; i < nh; ++i)
	//{
	//	vector<iVector> vDOFHingeSim(nsimDOF);

	//	int offset = 9*i;
	//	for (int j = 0; j < 9; ++j)
	//		vDOFHingeSim[vsimIdx[m_vDOFHingeRaw[offset + j]]].push_back(j);

	//	buildSelectionMatrix(vDOFHingeSim, this->m_vSh[i], true);
	//}

	this->m_isReady_St = true;

	this->m_isReady_DfDxp = false;
	this->m_isReady_DxDpS2 = false;

	//writeToFile(this->m_mSs, "S.csv");

	logSimu("[INFO] Updating S");
}

void ModelParameterization::update_Pt()
{
	this->m_mPs = MatrixSd(this->m_np, this->m_np);

	if (!this->m_vDOFTargetsRaw.empty())
	{
		int count = 0;

		tVector vTs;

		iVector vDOFTargetSim;
		int ntargetDOF = (int) this->m_vDOFTargetsRaw.size();
		this->m_pModel->mapRaw2Sim(this->m_vDOFTargetsRaw, vDOFTargetSim);

		for (int i = 0; i < ntargetDOF; ++i)
		{
			int d = vDOFTargetSim[i];

			if (d == -1)
				continue;

			vTs.push_back(Triplet<Real>(count++, d, 1.0));
		}

		MatrixSd mJs = MatrixSd(count, this->m_nx);
		mJs.setFromTriplets(vTs.begin(), vTs.end());
		mJs.makeCompressed();

		//writeToFile(mJs, "constraintsJ.csv");

		MatrixSd mDxDp = get_DxDp_Pro();

		//writeToFile(mDxDp, "DxDpPro.csv");

		this->m_mKs = mJs*get_DxDp_Pro();

		MatrixSd mK = mJs*get_DxDp_Pro();
		MatrixSd mK2 = mK*mK.transpose();

		//writeToFile(mK, "constraintsK.csv");
		//writeToFile(mK2, "constraintsK2.csv");

		MatrixSd mInx = MatrixSd(count, count);
		MatrixSd mInp = MatrixSd(m_np, m_np);
		mInx.setIdentity();
		mInp.setIdentity();

		MatrixSd mK2i;
		PhysSolver::SolveLinearSystem(mK2, mInx, mK2i);

		//writeToFile(mK2i, "constraintsK2i.csv");

		MatrixSd mKTK2iK = mK.transpose()*mK2i*mK;

		//writeToFile(mKTK2iK, "constraintsKTK2iK.csv");

		this->m_mPs = mInp - mKTK2iK;

		//writeToFile(m_mPs, "constraintsP.csv");
	}
	else
	{
		this->m_mPs.setIdentity();
	}

	this->m_isReady_Pt = true;

	logSimu("[INFO] Updating P");
}

void ModelParameterization::update_fp()
{
	if (this->isReady_fp())
		return; // Updated

	this->m_vfp.resize(this->m_np);
	for (int i = 0; i < this->m_ns; ++i)
	{
		VectorXd vfpi;
		this->m_vpset[i]->get_fp(vfpi);
		int npi = this->m_vpset[i]->getNumParameter();
		setSubvector(this->m_vsetOff[i], npi, vfpi, this->m_vfp);
	}
}

void ModelParameterization::update_DfDx()
{
	if (this->isReady_DfDx())
		return; // Updated

	tVector vDfDxs;

	this->m_pModel->add_DfxDx(vDfDxs);
	this->m_pSolver->addBoundaryJacobian(
		this->m_pModel->getPositions_x(),
		this->m_pModel->getVelocities_x(),
		vDfDxs);
	
	// Fix variables

	this->removeRows(vDfDxs, this->m_vFixedStencil);
	this->removeCols(vDfDxs, this->m_vFixedStencil);
	this->addDiagonal(vDfDxs, this->m_vFixedStencil, -1.0);

	this->m_mDfDxs = MatrixSd(this->m_nx, this->m_nx);
	this->m_mDfDxs.setFromTriplets(vDfDxs.begin(), vDfDxs.end());
	this->m_mDfDxs.makeCompressed();

	this->m_isReady_DfDx = true;
	this->m_isReady_DxDp = false;
	this->m_isReady_DfDxp = false;

	//writeToFile(this->m_mDfDxs, "DfDx.csv");

	logSimu("[INFO] Updating DfDx");
}

void ModelParameterization::update_DfDp()
{
	if (this->isReady_DfDp())
		return; // Updated

	tVector vDfDps;

	for (int i = 0; i < this->m_ns; ++i)
	{
		tVector vDfDpi;
		this->m_vpset[i]->get_DfDp(vDfDpi);
		int ncoefficients = (int) vDfDpi.size();

		// First extend the previous capacity of the vector
		vDfDps.reserve((int) vDfDps.size() + ncoefficients);

		for (int j = 0; j < ncoefficients; ++j) // Append coeficients
			vDfDps.push_back(Triplet<Real>(vDfDpi[j].row(), vDfDpi[j].col() + this->m_vsetOff[i], vDfDpi[j].value()));
	}

	// Fix variables

	this->removeRows(vDfDps, this->m_vFixedStencil);
	this->removeCols(vDfDps, this->m_vParamStencil);

	this->m_mDfDps = MatrixSd(this->m_nx, this->m_np);
	this->m_mDfDps.setFromTriplets(vDfDps.begin(), vDfDps.end());
	this->m_mDfDps.makeCompressed();

	this->m_isReady_DfDp = true;
	this->m_isReady_DxDp = false;
	this->m_isReady_DfDxp = false;
	this->m_isReady_DfDp2 = false;

	//writeToFile(this->m_mDfDps, "DfDp.csv");

	logSimu("[INFO] Updating DfDp");
}

void ModelParameterization::analyse_DxDp_Single(VectorXd& vps, Real& avgNorm, Real& minNorm, Real& maxNorm)
{
	vps = VectorXd(this->m_np);

	dVector vpS_STL;
	vpS_STL.reserve(this->m_np);
	for (int i = 0; i < this->m_np; ++i)
	{
		vps(i) = m_mDxDpsPro.col(i).norm();
		if (vps(i) < 1e-6)
			continue;

		vpS_STL.push_back(vps(i));
	}

	VectorXd vpS_EIG(this->m_np);
	toEigen(vpS_STL, vpS_EIG);

	avgNorm = vpS_EIG.mean();
	minNorm = vpS_EIG.minCoeff();
	maxNorm = vpS_EIG.maxCoeff();
}

void ModelParameterization::analyse_DxDp_Grouped(VectorXd& vps, Real& avgNorm, Real& minNorm, Real& maxNorm)
{
	vps = VectorXd(this->m_ns);

	for (int j = 0; j < this->m_ns; ++j)
	{
		int num_j = this->m_vpset[j]->getNumParameter();

		dVector vps_j_STL;

		for (int i = 0; i < num_j; ++i)
		{
			Real V = m_mDxDpsPro.col(this->m_vsetOff[j] + i).norm();
			if (V < 1e-6)
				continue;

			vps_j_STL.push_back(V);
		}

		VectorXd vpS_j_EIG(num_j);
		toEigen(vps_j_STL, vpS_j_EIG);

		vps(j) = vpS_j_EIG.mean();
	}

	avgNorm = vps.mean();
	minNorm = vps.minCoeff();
	maxNorm = vps.maxCoeff();
}

void ModelParameterization::setScale_Identity()
{
	this->m_mpS = MatrixSd(this->m_np, this->m_np);
	this->m_mpS.setIdentity(); // Initialize to identity
}

void ModelParameterization::setScale_DxDpNorm_Single()
{
	this->setScale_Identity();

	// Recompute raw S
	this->update_DxDp();

	VectorXd vpS;
	Real psAvg;
	Real psMin;
	Real psMax;
	this->analyse_DxDp_Single(vpS, psAvg, psMin, psMax);

	logSimu("[INFO] Parameters sensitivity: [%f, %f] - %f", psMin, psMax, psAvg);

	tVector vtS(m_np);
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(vpS(i), 0.0, EPS_POS))
			vtS[i] = Triplet<Real>(i, i, 1.0);
		else vtS[i] = Triplet<Real>(i, i, psAvg/vpS(i));
	}

	this->m_mpS = MatrixSd(this->m_np, this->m_np);
	this->m_mpS.setFromTriplets(vtS.begin(), vtS.end());
	this->m_mpS.makeCompressed();

	this->m_isReady_DxDp = true;

	this->process_DxDp();

	// Check

	VectorXd vpS_Test;
	Real psAvg_Test;
	Real psMin_Test;
	Real psMax_Test;
	this->analyse_DxDp_Single(vpS_Test, psAvg_Test, psMin_Test, psMax_Test);

	logSimu("[INFO] Parameters sensitivity test: [%f, %f] - %f", psMin_Test, psMax_Test, psAvg_Test);
}

void ModelParameterization::setScale_DxDpNorm_Grouped()
{
	this->setScale_Identity();

	// Recompute raw S
	this->update_DxDp();

	VectorXd vpS;
	Real psAvg;
	Real psMin;
	Real psMax;
	this->analyse_DxDp_Grouped(vpS, psAvg, psMin, psMax);

	logSimu("[INFO] Parameters sensitivity: [%f, %f] - %f", psMin, psMax, psAvg);

	tVector vtS(m_np);
	for (int j = 0; j < this->m_ns; ++j)
	{
		int num_j = this->m_vpset[j]->getNumParameter();

		for (int i = 0; i < num_j; ++i)
		{
			int parIdx = this->m_vsetOff[j] + i;
			if (isApprox(vpS(j), 0.0, EPS_POS))
				vtS[parIdx] = Triplet<Real>(parIdx, parIdx, 1.0);
			else vtS[parIdx] = Triplet<Real>(parIdx, parIdx, psAvg / vpS(j));
		}
	}
	
	this->m_mpS = MatrixSd(this->m_np, this->m_np);
	this->m_mpS.setFromTriplets(vtS.begin(), vtS.end());
	this->m_mpS.makeCompressed();

	this->m_isReady_DxDp = true;

	this->process_DxDp();
	
	// Check

	VectorXd vpS_Test;
	Real psAvg_Test;
	Real psMin_Test;
	Real psMax_Test;
	this->analyse_DxDp_Grouped(vpS_Test, psAvg_Test, psMin_Test, psMax_Test);

	logSimu("[INFO] Parameters sensitivity test: [%f, %f] - %f", psMin_Test, psMax_Test, psAvg_Test);
}

void ModelParameterization::update_DxDp()
{
	if (this->isReady_DxDp())
		return; // Updated

	// Dependent on DfDp
	if (!this->isReady_DfDp())
		this->update_DfDp();

	// Create DfxDx

	MatrixSd mDfDxs = this->get_DfDx();
	MatrixSd mDfDps = this->get_DfDp();

	mDfDxs *= -1.0;

	PhysSolver::SolveLinearSystem(mDfDxs, mDfDps, this->m_mDxDpsRaw);
	
	this->m_isReady_Pt = false;
	this->m_isReady_DxDp = true;
	this->m_isReady_DpDg = false;
	this->m_isReady_DxDp2 = false;
	this->m_isReady_DxDpS2 = false;

	//writeToFile(this->m_mDxDpsRaw, "DxDpRaw.csv");

	logSimu("[INFO] Updating DxDp");

	this->process_DxDp();
}

void ModelParameterization::process_DxDp()
{
	if (!this->isReady_DxDp())
		this->update_DxDp();

	this->m_mDxDpsPro = this->m_mDxDpsRaw;

	if (this->m_parameterMapping)
	{
		tVector vDrDp(this->m_np);

		const VectorXd& vpPro = this->getParameters_Raw();
		const VectorXd& vpUB = this->getParametersUpperBound_Raw();
		const VectorXd& vpLB = this->getParametersLowerBound_Raw();

		for (int i = 0; i < this->m_np; ++i)
		{
			if (vpUB(i) == HUGE_VAL || vpLB(i) == -HUGE_VAL)
			{
				vDrDp[i] = Triplet<Real>(i, i, 1.0);

				continue;
			}

			Real pi = vpPro(i);
			Real ui = vpUB(i);
			Real li = vpLB(i);
			Real oi = (ui - li) / 2;
			Real ri = M_PI/(ui - li);

			Real cosVal = cos((pi - oi)*ri);
			Real secVal = 1/(cosVal*cosVal);
			Real val = secVal*ri;

			vDrDp[i] = Triplet<Real>(i, i, val);
		}

		MatrixSd mDrDp(this->m_np, this->m_np);
		mDrDp.setFromTriplets(vDrDp.begin(), vDrDp.end());
		mDrDp.makeCompressed();

		//writeToFile(mDrDp, "sensitivityR.csv");

		this->m_mDxDpsPro = this->m_mDxDpsPro*mDrDp;
	}

	if (this->m_parameterScaling)
	{
		//writeToFile(m_mpS, "sensitivityS.csv");

		this->m_mDxDpsPro = this->m_mDxDpsPro*this->getParametersScaleD();
	}

	this->m_isReady_DpDg = false;
	this->m_isReady_DxDp2 = false;
	this->m_isReady_DxDpS2 = false;

	//writeToFile(this->m_mDxDpsPro, "DxDpPro.csv");

	logSimu("[INFO] Processing DxDp");
}

void ModelParameterization::update_DxDp2()
{
	if (this->isReady_DxDp2())
		return; // Updated

	MatrixSd mDxDpP = this->get_DxDp_Pro()*this->get_Pt();

	this->m_mDxDp2s = mDxDpP.transpose()*mDxDpP;

	this->m_isReady_DxDp2 = true;
	this->m_isReady_DpDg = false;

	//writeToFile(this->m_mDxDp2s, "testDxDp2.csv");

	logSimu("[INFO] Updating DxDp2");
}

void ModelParameterization::update_DxDpS2()
{
	if (this->isReady_DxDpS2())
		return; // Updated

	MatrixSd mDxDpP = this->get_DxDp_Pro()*this->get_Pt();

	MatrixSd mDxDpS = mDxDpP.transpose()*this->get_St().transpose();

	this->m_mDxDpS2s = mDxDpS*mDxDpS.transpose();

	this->m_isReady_DxDpS2 = true;

	//writeToFile(this->m_mDxDp2s, "testDxDp2.csv");

	logSimu("[INFO] Updating DxDpS2");
}

// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
// ----------------------------------- DEPRECATED -----------------------------------
// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------

void ModelParameterization::update_DfDxp()
{
	if (this->isReady_DfDxp())
		return; // Updated

	tVector vDfDxs;
	tVector vDfDps;
	toTriplets(this->get_DfDx(), vDfDxs);
	toTriplets(this->get_DfDp(), vDfDps);

	// Fix variables

	this->removeCols(vDfDxs, this->m_vGoalsStencil);
	this->removeCols(vDfDps, this->m_vParamStencil);
	this->addDiagonal(vDfDxs, this->m_vFixedStencil, -1.0);

	// Assemble matrices

	tVector vDfDxps = vDfDxs;
	int ncoeffDfDx = (int) vDfDxs.size();
	int ncoeffDfDp = (int) vDfDps.size();
	vDfDxps.reserve(ncoeffDfDx + ncoeffDfDp);
	int N = this->m_pModel->getNumSimDOF_x();

	for (int i = 0; i < ncoeffDfDp; ++i)
	{
		vDfDxps.push_back(Triplet<Real>(vDfDps[i].row(), N + vDfDps[i].col(), vDfDps[i].value()));
	}

	this->m_mDfDxps = MatrixSd(this->m_nx, this->m_nx + this->m_np);
	this->m_mDfDxps.setFromTriplets(vDfDxps.begin(), vDfDxps.end());
	this->m_mDfDxps.makeCompressed();

	this->m_isReady_DfDxp = true;
	this->m_isReady_DfDxp2 = false;

	//writeToFile(this->m_mDfDxps, "DfDxp.csv");
}

void ModelParameterization::update_DfDp2()
{
	if (this->isReady_DfDp2())
		return; // Updated

	// Dependent on DfDp
	if (!this->isReady_DfDp())
		this->update_DfDp();

	this->m_mDfDp2s = this->get_DfDp().transpose()*this->get_DfDp();

	this->m_isReady_DfDp2 = true;

	//writeToFile(this->m_mDfDp2s, "testDfDp2.csv");
}

void ModelParameterization::update_DfDxp2()
{
	if (this->isReady_DfDxp2())
		return; // Updated

	this->m_mDfDxp2s = this->get_DfDxp().transpose()*this->get_DfDxp();

	this->m_isReady_DfDxp2 = true;

	//writeToFile(this->m_mDfDp2s, "testDfDxp2.csv");
}

void ModelParameterization::update_DpDg()
{
	if (this->isReady_DpDg())
		return; // Updated

	// Compute matrix DpDx as ((DxDp^T)*S^T*S*DxDp)^-1 * DxDp^T * S
	MatrixSd mDxDpP = this->get_DxDp_Pro()*this->get_Pt();
	MatrixSd mDxDpS = mDxDpP.transpose()*get_St().transpose();
	MatrixSd mDxDpS2 = mDxDpS*mDxDpS.transpose();
	PhysSolver::SolveLinearSystem(mDxDpS2, mDxDpS, this->m_mDpDgs);

	this->m_isReady_DpDg = true;
	this->m_isReady_DxDg = false;

	logSimu("[INFO] Updating DpDg");
}

void ModelParameterization::update_DxDg()
{
	if (this->isReady_DxDg())
		return; // Updated

	// Compute the editing matrix DxDg as DxDp * DpDg
	this->m_mDxDgs = this->get_DxDp_Pro() * this->get_Pt() * this->get_DpDg();

	this->m_isReady_DxDg = true;

	logSimu("[INFO] Updating DxDg");
}

void ModelParameterization::computeEditSuitability(const vector<VectorXd>& vedits, VectorXd& vcost)
{
	// Dependent on DpDx
	if (!this->isReady_DpDg())
		this->update_DpDg();

	// Dependent on DxDg
	if (!this->isReady_DxDg())
		this->update_DxDg();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();
	int nrawDOF = this->m_pModel->getNumRawDOF();

	int nselRaw = (int) this->m_vDOFGoalsRaw.size();

	int neditSamples = (int) vedits.size();

	// Store previous state
	VectorXd vP = this->getParameters_Pro();
	VectorXd vX = this->m_pModel->getPositions_x();

	VectorXd vf(nsimDOF);
	vf.setZero(); // Init.

	// Safe current configuration
	pSolidState pStateIni = this->m_pModel->getState_x()->clone();

	vcost.resize(neditSamples); // Compute
	for (int i = 0; i < neditSamples; ++i)
	{
		const VectorXd& veditRawSel = vedits[i];
		assert((int)veditRawSel.size() == nselRaw);

		// Get whole vector in raw
		VectorXd veditRawAll(nrawDOF);
		VectorXd veditSimAll(nsimDOF);
		veditRawAll.setZero();
		veditSimAll.setZero();
		for (int j = 0; j < nselRaw; ++j)
			veditRawAll(this->m_vDOFGoalsRaw[j]) = veditRawSel[j];

		// Transform the edit vector from raw to sim. spaces
		this->m_pModel->mapRaw2Sim(veditRawAll, veditSimAll);

		// Compute the displacements corresponding to a goal
		VectorXd vdispSimAll = this->m_mDxDgs * veditSimAll;
		VectorXd vdispParAll = this->m_mDpDgs * veditSimAll;

		logSimu("[INFO] Position change %f", vdispSimAll.norm());
		logSimu("[INFO] Parameter change: %f", vdispParAll.norm());

		// Update the parameters of the model
		this->setParameters_Pro(vP + vdispParAll);

		// Update the state of the model and compute force
		this->m_pModel->setPositions_x(vX + vdispSimAll);

		// Compute force
		this->m_pSolver->getResidual(this->m_pModel->getPositions_x(), this->m_pModel->getVelocities_x(), vf);

		vcost(i) = vf.norm();

		// Restore previous state
		this->m_pModel->setState_x(pStateIni);
	}

	this->setParameters_Pro(vP);
}

void ModelParameterization::computeEigenvaluesSpace_DxDg(vector<EigenPair>& veigenPair, bool sort)
{
	// Dependent on DxDg
	if (!this->isReady_DxDg())
		this->update_DxDg();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();

	MatrixXd mDxDg = m_mDxDgs.toDense();

	EigenSolver<MatrixXd> eigenSolver;
	eigenSolver.compute(mDxDg, true);

	if (eigenSolver.info() != Eigen::Success)
	{
		logSimu("[ERROR] Impossible to compute Eigenvalues");
		veigenPair.clear();
		return;
	}

	EigenSolver<MatrixXd>::EigenvalueType veigenValComplex = eigenSolver.eigenvalues();
	EigenSolver<MatrixXd>::EigenvectorsType veigenVecComplex = eigenSolver.eigenvectors();

	// Extract eigenvalues
	veigenPair.resize(nsimDOF);

	for (int i = 0; i < nsimDOF; ++i)
	{
		veigenPair[i].m_eigenVal_r = veigenValComplex(i).real();
		veigenPair[i].m_eigenVal_i = veigenValComplex(i).imag();
		veigenPair[i].m_veigenVec_r.resize(nsimDOF);
		veigenPair[i].m_veigenVec_i.resize(nsimDOF);
		for (int j = 0; j < nsimDOF; ++j)
		{
			veigenPair[i].m_veigenVec_r(j) = veigenVecComplex(j, i).real();
			veigenPair[i].m_veigenVec_i(j) = veigenVecComplex(j, i).imag();
		}
	}

	if (sort)
	{
		MT_EigenPair comparator; // Sort here by real and imaginary
		std::sort(veigenPair.begin(), veigenPair.end(), comparator);
	}

	// Map simulation to raw spaces
	for (int i = 0; i < nsimDOF; ++i)
	{
		// TODO: This might not be working properly with complex numbers.
		// In the case of composite models, complex number might not be 
		// properly interpolated.

		VectorXd veigenVec_r_raw;
		VectorXd veigenVec_i_raw;
		this->m_pModel->mapSim2Raw(veigenPair[i].m_veigenVec_r, veigenVec_r_raw);
		this->m_pModel->mapSim2Raw(veigenPair[i].m_veigenVec_i, veigenVec_i_raw);
		veigenPair[i].m_veigenVec_r = veigenVec_r_raw;
		veigenPair[i].m_veigenVec_i = veigenVec_i_raw;
	}
}

void ModelParameterization::computeEigenvaluesSpace_DxDp(vector<EigenPair>& veigenPair, bool sort)
{
	// Dependent on DxDg
	if (!this->isReady_DxDp())
		this->update_DxDp();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();

	MatrixXd mDxDp = this->get_DxDp_Pro().toDense();

	// SVD decomposition of a non-square matrix is related 
	// to Eigenvalue decomposition: sval = sqrt(eval) and 
	// svec = evec.

	MatrixXd mDxDp2 = mDxDp*mDxDp.transpose();

	EigenSolver<MatrixXd> eigenSolver;
	eigenSolver.compute(mDxDp2, true);

	if (eigenSolver.info() != Eigen::Success)
	{
		logSimu("[ERROR] Impossible to compute Eigenvalues");
		veigenPair.clear();
		return;
	}

	EigenSolver<MatrixXd>::EigenvalueType veigenValComplex = eigenSolver.eigenvalues();
	EigenSolver<MatrixXd>::EigenvectorsType veigenVecComplex = eigenSolver.eigenvectors();

	// Extract eigenvalues
	veigenPair.resize(nsimDOF);

	for (int i = 0; i < nsimDOF; ++i)
	{
		veigenPair[i].m_eigenVal_r = sqrt(veigenValComplex(i).real());
		veigenPair[i].m_eigenVal_i = sqrt(veigenValComplex(i).imag());
		veigenPair[i].m_veigenVec_r.resize(nsimDOF);
		veigenPair[i].m_veigenVec_i.resize(nsimDOF);
		for (int j = 0; j < nsimDOF; ++j)
		{
			veigenPair[i].m_veigenVec_r(j) = veigenVecComplex(j, i).real();
			veigenPair[i].m_veigenVec_i(j) = veigenVecComplex(j, i).imag();
		}
	}

	if (sort)
	{
		MT_EigenPair comparator; // Sort here by real and imaginary
		std::sort(veigenPair.begin(), veigenPair.end(), comparator);
	}

	// Map simulation to raw spaces
	for (int i = 0; i < nsimDOF; ++i)
	{
		// TODO: This might not be working properly with complex numbers.
		// In the case of composite models, complex number might not be 
		// properly interpolated.

		VectorXd veigenVec_r_raw;
		VectorXd veigenVec_i_raw;
		this->m_pModel->mapSim2Raw(veigenPair[i].m_veigenVec_r, veigenVec_r_raw);
		this->m_pModel->mapSim2Raw(veigenPair[i].m_veigenVec_i, veigenVec_i_raw);
		veigenPair[i].m_veigenVec_r = veigenVec_r_raw;
		veigenPair[i].m_veigenVec_i = veigenVec_i_raw;
	}
}

void ModelParameterization::addDiagonal(tVector& vM, const bVector& vstencil, Real value)
{
	int N = (int) vstencil.size();
	for (int i = 0; i < N; ++i)
	{
		if (!vstencil[i]) 
			continue;

		vM.push_back(Triplet<Real>(i, i, value));
	}
}

void ModelParameterization::removeCols(tVector& vM, const bVector& vstencil)
{
	int ncoeff = (int) vM.size();
	for (int i = 0; i < ncoeff; ++i)
	{
		if (vstencil[vM[i].col()])
			vM[i] = Triplet<Real>(vM[i].row(), vM[i].col(), 0.0);
	}
}

void ModelParameterization::removeRows(tVector& vM, const bVector& vstencil)
{
	int ncoeff = (int)vM.size();
	for (int i = 0; i < ncoeff; ++i)
	{
		if (vstencil[vM[i].row()])
			vM[i] = Triplet<Real>(vM[i].row(), vM[i].col(), 0.0);
	}
}

Real ModelParameterization::FB_computeObjective()
{
	VectorXd vresidual;
	this->m_pSolver->getResidual(
		this->m_pModel->getPositions_x(),
		this->m_pModel->getVelocities_x(),
		vresidual);

	return 0.5*vresidual.squaredNorm();
}

void ModelParameterization::FB_computeDefaultGradient(VectorXd& vgDef)
{
	VectorXd vresidual;
	this->m_pSolver->getResidual(
		this->m_pModel->getPositions_x(),
		this->m_pModel->getVelocities_x(),
		vresidual);

	vgDef = this->get_DfDp().transpose()*vresidual;
}

void ModelParameterization::FB_computeBoundedGradient(VectorXd& vgCon)
{
	this->FB_computeDefaultGradient(vgCon);

	const VectorXd vpMax = this->getParametersUpperBound_Pro();
	const VectorXd vpMin = this->getParametersLowerBound_Pro();
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(this->m_vpPro[i], vpMax[i], 1e-6) && vgCon[i] > 0)
			vgCon[i] = 0.0; // This is not going higher, make zero

		if (isApprox(this->m_vpPro[i], vpMin[i], 1e-6) && vgCon[i] < 0)
			vgCon[i] = 0.0; // This is not going lower, make zero
	}
}

void ModelParameterization::FB_computeDefaultGradient_FD(VectorXd& vgDef)
{
	VectorXd vpF = this->getParameters_Pro();
	VectorXd vp0 = this->getParameters_Pro();

	pSolidState pStateIni = this->m_pModel->getState_x()->clone();

	vgDef.resize(this->m_np);
	vgDef.setZero();

	for (int i = 0; i < this->m_np; ++i)
	{
		if (this->m_vParamStencil[i])
			continue; // Ignore

		this->setParameters_Pro(vpF);

		// One-sided
		if (this->m_vpUBPro(i) - vpF(i) < 1e-6)
		{
			Real error0 = this->FB_computeObjective();

			// Minus
			vpF(i) -= 1e-6;
			this->setParameters_Pro(vpF);
			Real errorm = this->FB_computeObjective();

			// Estimate
			vgDef(i) = (error0 - errorm) / 1e-6;

			vpF(i) = vp0(i);
			continue;
		}

		// One-sided
		if (vpF(i) - this->m_vpLBPro(i) < 1e-6)
		{
			Real error0 = this->FB_computeObjective();

			// Plus
			vpF(i) += 1e-6;
			this->setParameters_Pro(vpF);
			Real errorp = this->FB_computeObjective();

			// Estimate
			vgDef(i) = (errorp - error0) / 1e-6;

			vpF(i) = vp0(i);
			continue;
		}

		// Centered

		// Plus
		vpF(i) += 1e-6;
		this->setParameters_Pro(vpF);
		Real errorp = this->FB_computeObjective();

		// Minus
		vpF(i) -= 2e-6;
		this->setParameters_Pro(vpF);
		Real errorm = this->FB_computeObjective();

		// Estimate
		vgDef(i) = (errorp - errorm) / 2e-6;

		vpF(i) = vp0(i);
	}

	this->setParameters_Pro(vp0);
}

void ModelParameterization::FB_computeBoundedGradient_FD(VectorXd& vgCon)
{
	this->FB_computeDefaultGradient_FD(vgCon);

	const VectorXd vpMax = this->getParametersUpperBound_Pro();
	const VectorXd vpMin = this->getParametersLowerBound_Pro();
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(this->m_vpPro[i], vpMax[i], 1e-6) && vgCon[i] > 0)
			vgCon[i] = 0.0; // This is not going higher, make zero

		if (isApprox(this->m_vpPro[i], vpMin[i], 1e-6) && vgCon[i] < 0)
			vgCon[i] = 0.0; // This is not going lower, make zero
	}
}

void ModelParameterization::FB_testObjectiveGradient()
{
	//this->m_pModel->testForceGlobal();
	//this->testParameterLinearization();

	VectorXd vgA;
	VectorXd vgF;
	this->FB_computeDefaultGradient(vgA);
	this->FB_computeDefaultGradient_FD(vgF);

	VectorXd vgD = vgA - vgF;
	Real normF = vgF.norm();
	Real normD = vgD.norm();
	Real errorRel = normD / normF;

	if (errorRel > 1e-9)
	{
		logSimu("[ERROR] Error computing the objective gradient: %.9f", errorRel);
		//writeToFile(vgF, "objectiveGradientF.csv");
		//writeToFile(vgA, "objectiveGradientA.csv");
	}
}

bool ModelParameterization::FB_computeParameterStep_CQP(VectorXd& vdp, string& error)
{
	// Solve the problem min q(p) = 1/2 f^T*f which is a quadratic
	// problem of the form: min q(p) = 1/2 p^T * G * p + p^T * c 
	// where coefficients:
	//		* G = A^T*A
	//		* c = A^T*f
	// And:
	//		* A = Df/Dp

	// Get force residual

	VectorXd vf;
	this->m_pSolver->getResidual(
		this->m_pModel->getPositions_x(),
		this->m_pModel->getVelocities_x(),
		vf);

	// Create the matrix of quadratic coefficients

	tVector tGs;

	MatrixSd mGs = this->get_DfDp2();

	MatrixSd mRs(this->m_np, this->m_np);
	mRs.setIdentity(); // Used as regularizer

	//// Regularize matrix
	//mGs = mGs + 1e-3*mRs;

	toTriplets(mGs, tGs);

	// Create the vector of linear coefficients given the target deformation

	VectorXd vc = this->get_DfDp().transpose()*vf;

	// Get the vectors of parameter bounds

	const VectorXd& vpub = this->getParametersUpperBound_Pro();
	const VectorXd& vplb = this->getParametersLowerBound_Pro();

	VectorXd vdpUB = vpub - this->m_vpPro;
	VectorXd vdpLB = vplb - this->m_vpPro;

	// Solve QP

	AlgLib_Sparse_BoxC_QP QPsolver;

	VectorXd vx0(this->m_np);
	vx0.setZero(); // Init.

	VectorXd vxo(this->m_np);
	vxo.setZero(); // Init.

	bool solved = QPsolver.solve(tGs, vc, vx0, vdpLB, vdpUB, vxo, error);

	if (solved)
	{
		// Copy
		vdp = vxo;
	}

	return solved;
}

bool ModelParameterization::FB_computeParameterStep_UQP_ProjectGrad(VectorXd& vdp, string& error)
{
	VectorXd vgCon;
	this->FB_computeBoundedGradient(vgCon);

	VectorXd vgConNeg = -1.0*vgCon;
	MatrixSd mDfDp2s = this->get_DfDp2();
	PhysSolver::SolveLinearSystem(mDfDp2s, vgConNeg, vdp);

	if (vgConNeg.dot(vdp) < 0.0)
	{
		logSimu("[WARNING] Non-descendent direction (UQP Project Grad): %.9f", vgConNeg.dot(vdp));
	}

	return true;
}

bool ModelParameterization::FB_computeParameterStep_UQP_ProjectStep(VectorXd& vdp, string& error)
{
	VectorXd vgDef;
	this->FB_computeDefaultGradient(vgDef);

	VectorXd vgDefNeg = -1.0*vgDef;
	MatrixSd mDfDp2s = this->get_DfDp2();
	PhysSolver::SolveLinearSystem(mDfDp2s, vgDefNeg, vdp);

	if (vgDefNeg.dot(vdp) < 0.0)
	{
		logSimu("[WARNING] Non-descendent direction (UQP Project Step): %.9f", vgDef.dot(vdp));
	}

	return true;
}

Real ModelParameterization::MB_computeObjective()
{
	VectorXd vresidual;
	this->m_pSolver->getResidual(
		this->m_pModel->getPositions_x(),
		this->m_pModel->getVelocities_x(),
		vresidual);
	return 0.5*vresidual.squaredNorm();
}

void ModelParameterization::MB_computeDefaultGradient(VectorXd& vgDef)
{
	VectorXd vresidual;
	this->m_pSolver->getResidual(
		this->m_pModel->getPositions_x(),
		this->m_pModel->getVelocities_x(),
		vresidual);

	vgDef = this->get_DfDxp().transpose()*vresidual;

	for (int i = 0; i < this->m_nx; ++i)
		if (this->m_vGoalsStencil[i])
			vgDef(i) = 0.0;
}

void ModelParameterization::MB_computeBoundedGradient(VectorXd& vgCon)
{
	this->MB_computeDefaultGradient(vgCon);

	const VectorXd vpMax = this->getParametersUpperBound_Pro();
	const VectorXd vpMin = this->getParametersLowerBound_Pro();
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(this->m_vpPro[i], vpMax[i], 1e-6) && vgCon[i] > 0)
			vgCon[i] = 0.0; // This is not going higher, make zero

		if (isApprox(this->m_vpPro[i], vpMin[i], 1e-6) && vgCon[i] < 0)
			vgCon[i] = 0.0; // This is not going lower, make zero
	}
}

void ModelParameterization::MB_computeDefaultGradient_FD(VectorXd& vgDef)
{
	VectorXd vxF = this->m_pModel->getPositions_x();
	VectorXd vvF = this->m_pModel->getVelocities_x();
	VectorXd vpF = this->getParameters_Pro();

	VectorXd vx0 = vxF;
	VectorXd vv0 = vvF;
	VectorXd vp0 = vpF;

	pSolidState pStateIni = this->m_pModel->getState_x()->clone();

	vgDef.resize(this->m_nx + this->m_np);
	vgDef.setZero();

	for (int i = 0; i < this->m_nx; ++i)
	{
		if (this->m_vGoalsStencil[i])
			continue; // Ignore

		this->m_pModel->setState_x(pStateIni);

		// Plus
		vxF(i) += 1e-6;
		this->m_pModel->setPositions_x(vxF);
		Real errorp = this->MB_computeObjective();

		this->m_pModel->setState_x(pStateIni);

		// Minus
		vxF(i) -= 2e-6;
		this->m_pModel->setPositions_x(vxF);
		Real errorm = this->MB_computeObjective();

		vgDef(i) = (errorp - errorm) / 2e-6;

		vxF(i) = vx0(i);
	}

	this->m_pModel->setState_x(pStateIni);

	for (int i = 0; i < this->m_np; ++i)
	{
		if (this->m_vParamStencil[i])
			continue; // Ignore

		this->setParameters_Pro(vpF);

		// One-sided
		if (this->m_vpUBPro(i) - vpF(i) < 1e-6)
		{
			Real error0 = this->FB_computeObjective();

			// Minus
			vpF(i) -= 1e-6;
			this->setParameters_Pro(vpF);
			Real errorm = this->FB_computeObjective();

			// Estimate
			vgDef(this->m_nx + i) = (error0 - errorm) / 1e-6;

			vpF(i) = vp0(i);
			continue;
		}

		// One-sided
		if (vpF(i) - this->m_vpLBPro(i) < 1e-6)
		{
			Real error0 = this->FB_computeObjective();

			// Plus
			vpF(i) += 1e-6;
			this->setParameters_Pro(vpF);
			Real errorp = this->FB_computeObjective();

			// Estimate
			vgDef(this->m_nx + i) = (errorp - error0) / 1e-6;

			vpF(i) = vp0(i);
			continue;
		}

		// Centered

		// Plus
		vpF(i) += 1e-6;
		this->setParameters_Pro(vpF);
		Real errorp = this->FB_computeObjective();

		// Minus
		vpF(i) -= 2e-6;
		this->setParameters_Pro(vpF);
		Real errorm = this->FB_computeObjective();

		// Estimate
		vgDef(this->m_nx + i) = (errorp - errorm) / 2e-6;

		vpF(i) = vp0(i);
	}

	this->setParameters_Pro(vpF);
}

void ModelParameterization::MB_computeBoundedGradient_FD(VectorXd& vgCon)
{
	this->MB_computeDefaultGradient_FD(vgCon);

	const VectorXd vpMax = this->getParametersUpperBound_Pro();
	const VectorXd vpMin = this->getParametersLowerBound_Pro();
	for (int i = 0; i < this->m_np; ++i)
	{
		if (isApprox(this->m_vpPro[i], vpMax[i], 1e-6) && vgCon[this->m_nx + i] > 0)
			vgCon[this->m_nx + i] = 0.0; // This is not going higher, make zero

		if (isApprox(this->m_vpPro[i], vpMin[i], 1e-6) && vgCon[this->m_nx + i] < 0)
			vgCon[this->m_nx + i] = 0.0; // This is not going lower, make zero
	}
}

void ModelParameterization::MB_testObjectiveGradient()
{
	VectorXd vgA;
	VectorXd vgF;
	this->MB_computeDefaultGradient(vgA);
	this->MB_computeDefaultGradient_FD(vgF);

	VectorXd vgD = vgA - vgF;
	Real normF = vgF.norm();
	Real normD = vgD.norm();
	Real errorRel = normD / normF;

	if (errorRel > 1e-9)
	{
		logSimu("[ERROR] Error computing the objective gradient: %.9f", errorRel);
		//writeToFile(vgF, "objectiveGradientF.csv");
		//writeToFile(vgA, "objectiveGradientA.csv");
	}
}

bool ModelParameterization::MB_computeParameterStep_CQP(VectorXd& vdp, string& error)
{
	// Solve the problem min q(p) = 1/2 f^T*f which is a quadratic
	// problem of the form: min q(p) = 1/2 p^T * G * p + p^T * c 
	// where coefficients:
	//		* G = A^T*A
	//		* c = A^T*f
	// And:
	//		* A = Df/Dp

	// Get force residual

	VectorXd vf;
	this->m_pSolver->getResidual(
		this->m_pModel->getPositions_x(),
		this->m_pModel->getVelocities_x(),
		vf);

	// Create the matrix of quadratic coefficients

	tVector tGs;

	MatrixSd mGs = this->get_DfDxp2();

	MatrixSd mRs(this->m_nx + this->m_np,
		this->m_nx + this->m_np);
	mRs.setIdentity(); // Used as regularizer

	// Regularize matrix
	mGs = mGs + 1e-3*mRs;

	toTriplets(mGs, tGs);

	// Create the vector of linear coefficients given the target deformation

	VectorXd vc = this->get_DfDxp().transpose()*vf;

	// Get the vectors of parameter bounds

	const VectorXd& vpub = this->getParametersUpperBound_Pro();
	const VectorXd& vplb = this->getParametersLowerBound_Pro();

	VectorXd vdpUB(this->m_nx + this->m_np);
	VectorXd vdpLB(this->m_nx + this->m_np);
	for (int i = 0; i < this->m_nx; ++i)
	{
		vdpUB(i) = HUGE_VAL;
		vdpLB(i) = -HUGE_VAL;
	}
	for (int i = this->m_nx; i < this->m_nx + this->m_np; ++i)
	{
		vdpUB(i) = vpub(i - this->m_nx) - this->m_vpPro(i - this->m_nx);
		vdpLB(i) = vplb(i - this->m_nx) - this->m_vpPro(i - this->m_nx);
	}

	// Solve QP

	AlgLib_Sparse_BoxC_QP QPsolver;

	VectorXd vx0(this->m_nx + this->m_np);
	vx0.setZero(); // Initialize to zero

	VectorXd vxo(this->m_nx + this->m_np);
	vxo.setZero(); // Initialize to zero

	bool solved = QPsolver.solve(tGs, vc, vx0, vdpLB, vdpUB, vxo, error);

	if (solved)
	{
		// Copy
		vdp = vxo;
	}

	return solved;
}

bool ModelParameterization::MB_computeParameterStep_UQP_ProjectGrad(VectorXd& vdp, string& error)
{
	VectorXd vgCon;
	this->MB_computeBoundedGradient(vgCon);

	VectorXd vgConNeg = -1.0*vgCon;
	MatrixSd mDfDp2s = this->get_DfDp2();
	PhysSolver::SolveLinearSystem(mDfDp2s, vgConNeg, vdp);

	if (vgConNeg.dot(vdp) < 0.0)
	{
		logSimu("[WARNING] Non-descendent direction (UQP Project Grad): %.9f", vgConNeg.dot(vdp));
	}

	return true;
}

bool ModelParameterization::MB_computeParameterStep_UQP_ProjectStep(VectorXd& vdp, string& error)
{
	VectorXd vgDef;
	this->MB_computeDefaultGradient(vgDef);

	VectorXd vgDefNeg = -1.0*vgDef;
	MatrixSd mDfDxp2s = this->get_DfDxp2();
	PhysSolver::SolveLinearSystem(mDfDxp2s, vgDefNeg, vdp);

	if (vgDefNeg.dot(vdp) < 0.0)
	{
		logSimu("[WARNING] Non-descendent direction (UQP Project Step): %.9f", vgDef.dot(vdp));
	}

	return true;
}

