/*=====================================================================================*/
/*!
/file		SolidModel.cpp
/author		jesusprod
/brief		Implementation of SolidModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/SolidModel.h>

#ifdef _OPENMP 
#include <omp.h>
#endif

SolidModel::SolidModel(const string& ID)
{
	this->m_ID = ID;

	m_dummyMat.initFromYoungPoisson(0, 0, 0);
	m_dummyMat.setBendingKw(-1);
	m_dummyMat.setBendingKh(-1);
	m_dummyMat.setStretchK(-1);

	ostringstream IDstreamUF;
	IDstreamUF << "UPD_FOR_" << this->m_ID.c_str();
	this->m_updateForTimer = CustomTimer(10, IDstreamUF.str());

	ostringstream IDstreamUJ;
	IDstreamUJ << "UPD_JAC_" << this->m_ID.c_str();
	this->m_updateJacTimer = CustomTimer(10, IDstreamUJ.str());

	ostringstream IDstreamAF;
	IDstreamAF << "ASS_FOR_" << this->m_ID.c_str();
	this->m_assembleForTimer = CustomTimer(10, IDstreamAF.str());

	ostringstream IDstreamAJ;
	IDstreamAJ << "ASS_JAC_" << this->m_ID.c_str();
	this->m_assembleJacTimer = CustomTimer(10, IDstreamAJ.str());

	this->m_updateForTimer.initialize();
	this->m_updateJacTimer.initialize();
	this->m_assembleForTimer.initialize();
	this->m_assembleJacTimer.initialize();

	this->m_state_x.reset(new SolidState());
	this->m_state_0.reset(new SolidState());

	// Forces the initialization
	this->deprecateConfiguration();
}

SolidModel::~SolidModel()
{
	logSimu("[INFO] Destroying SolidModel: %u", this);

	freeElements();
}

////////////////////////////////////////////////////////////////////////////
// Model state control

void SolidModel::deprecateConfiguration()
{
	this->m_isReady_Setup = false;

	this->deprecateDiscretization();
}

void SolidModel::deprecateDiscretization()
{
	this->m_nNZ_Jacobian = -1;
	this->m_nNZ_MassM = -1;
	this->m_nNZ_DfxDx = -1;
	this->m_nNZ_DfxD0 = -1;

	this->deprecateUndeformed();
}

void SolidModel::deprecateUndeformed()
{
	this->m_isReady_Rest = false;
	this->m_isReady_Mass = false;

	this->deprecateDeformation();
}

void SolidModel::deprecateDeformation()
{
	this->m_isReady_Force = false;
	this->m_isReady_Energy = false;
	this->m_isReady_Jacobian = false;
	this->m_isReady_fx = false;
	this->m_isReady_f0 = false;
	this->m_isReady_DfxDx = false;
	this->m_isReady_DfxD0 = false;
	this->m_isReady_Mesh = false;
}

void SolidModel::onDiscretizationChanged(const iVector& vnewIdxRaw)
{
	// Default implementation: deprecate all
	// precomputed information related with
	// configuration and setup again.

	this->deprecateConfiguration();

	// Notify about the changes in the topology indices

	//__raise this->discretizationChanged(this, vnewIdxRaw);
}

void SolidModel::freeElements()
{
	int numEle = this->getNumEle();
	for (int i = 0; i < numEle; ++i)
		delete this->m_vpEles[i];
	this->m_vpEles.clear();
}

void SolidModel::configureMaterial(const SolidMaterial& material)
{
	this->m_material = material;
	this->deprecateConfiguration();
}

////////////////////////////////////////////////////////////////////////////
// Parameterization

const VectorXd& SolidModel::getParam_Rest() const
{
	return this->m_state_0->m_vx;
}

void SolidModel::setParam_Rest(const VectorXd& vX)
{
	assert((int)vX.size() == this->m_nsimDOF_0);

	if (!this->m_isReady_Setup)
		this->setup(); // Need?

	// Update rest
	this->m_state_0->m_vx = vX;

	this->deprecateUndeformed();
}


const SolidMaterial& SolidModel::getParam_Material() const
{
	return this->m_material;
}

void SolidModel::setParam_Material(const SolidMaterial& material)
{
	// Update common material

	this->m_material = material;
	this->initializeMaterials();

	this->setIsReady_Mass(false);
	this->deprecateDeformation();
}

void SolidModel::initializeMaterials()
{
	// Initialize elements materials

	for (int i = 0; i < this->getNumEle(); ++i) // Same all
		this->m_vpEles[i]->setMaterial(&this->m_material);
}

////////////////////////////////////////////////////////////////////////////
// Matrix precomputation

void SolidModel::precompute_Jacobian()
{
	tVector testJ;
	this->addJacobian(testJ); // Precompute
	this->m_nNZ_Jacobian = (int)testJ.size();
}

void SolidModel::precompute_MassM()
{
	tVector testJ;
	this->add_Mass(testJ); // Precompute
	this->m_nNZ_MassM = (int)testJ.size();
}

void SolidModel::precompute_DfxDx()
{
	tVector testJ;
	this->add_DfxDx(testJ); // Precompute
	this->m_nNZ_DfxDx = (int)testJ.size();
}

void SolidModel::precompute_DfxD0()
{
	tVector testJ;
	this->add_DfxD0(testJ); // Precompute
	this->m_nNZ_DfxD0 = (int)testJ.size();
}


////////////////////////////////////////////////////////////////////////////
// State management

pSolidState SolidModel::getState_x() const
{
	return this->m_state_x;
}

pSolidState SolidModel::getState_0() const
{
	return this->m_state_0;
}

void SolidModel::setState_x(pSolidState s)
{
	*this->m_state_x = SolidState(*s);
	this->deprecateDeformation();
}

void SolidModel::setState_0(pSolidState s)
{
	*this->m_state_0 = SolidState(*s);
	this->deprecateUndeformed();
}

void SolidModel::setPositions_x(const VectorXd& vx)
{
	this->m_state_x->m_vx = vx;
	this->deprecateDeformation();
}

void SolidModel::setPositions_0(const VectorXd& vx)
{
	this->m_state_0->m_vx = vx;
	this->deprecateUndeformed();
}

void SolidModel::setVelocities_x(const VectorXd& vv)
{
	this->m_state_x->m_vv = vv;
}

////////////////////////////////////////////////////////////////////////////
// Computed data updaters

void SolidModel::update_Rest()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Rest)
		return; // Already done

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_Rest(this->m_state_0->m_vx);

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void SolidModel::update_Mass()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mass)
		return; // Already done

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_Mass(this->m_state_0->m_vx);

	this->m_vMass.resize(this->m_nsimDOF_x);
	this->m_vMass.setZero(); // Initialize

	// Cache lumped mass vector and matrix

	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->addMass(this->m_vMass);

	this->updateSparseMatrixToLumpedMass();

	this->m_isReady_Mass = true;
	this->deprecateDeformation();
}

void SolidModel::update_Energy()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Energy)
		return; // Already done

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_Energy(this->m_state_x->m_vx, this->m_state_x->m_vv);

	this->m_isReady_Energy = true;
}

void SolidModel::update_Force()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Force)
		return; // Already done

	this->m_updateForTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_Force(this->m_state_x->m_vx, this->m_state_x->m_vv);

	this->m_isReady_Force = true;

	this->m_updateForTimer.stopStoreLog();
}

void SolidModel::update_Jacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian)
		return; // Already done

	this->m_updateJacTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_Jacobian(this->m_state_x->m_vx, this->m_state_x->m_vv);

	this->m_isReady_Jacobian = true;

	this->m_updateJacTimer.stopStoreLog();
}

void SolidModel::update_EnergyForce()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Energy && this->m_isReady_Force)
		return; // Both of them already computed, out

	this->m_updateForTimer.restart();
	
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_EnergyForce(this->m_state_x->m_vx, this->m_state_x->m_vv);

	this->m_isReady_Energy = true;
	this->m_isReady_Force = true;

	this->m_updateForTimer.stopStoreLog();
}

void SolidModel::update_ForceJacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian && this->m_isReady_Force)
		return; // Both of them already computed, out

	this->m_updateForTimer.restart();
	this->m_updateJacTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_ForceJacobian(this->m_state_x->m_vx, this->m_state_x->m_vv);

	this->m_isReady_Force = true;
	this->m_isReady_Jacobian = true;

	this->m_updateForTimer.stopStoreLog();
	this->m_updateJacTimer.stopStoreLog();
}

void SolidModel::update_EnergyForceJacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian && 
		this->m_isReady_Force && 
		this->m_isReady_Energy)
		return; // All done

	this->m_updateForTimer.restart();
	this->m_updateJacTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_EnergyForceJacobian(this->m_state_x->m_vx, this->m_state_x->m_vv);

	this->m_isReady_Energy = true;
	this->m_isReady_Force = true;
	this->m_isReady_Jacobian = true;

	this->m_updateForTimer.stopStoreLog();
	this->m_updateJacTimer.stopStoreLog();
}

void SolidModel::update_fx()
{
	this->update_Force(); // Same
	this->m_isReady_fx = true;
}

void SolidModel::update_f0()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_f0)
		return; // Updated

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_f0(this->m_state_x->m_vx, this->m_state_0->m_vx);

	this->m_isReady_f0 = true;
}

void SolidModel::update_DfxDx()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_DfxDx)
		return; // Already done

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_DfxDx(this->m_state_x->m_vx, this->m_state_0->m_vx);

	// Update mass derivative
	if (this->m_vg.norm() > 1e-6)
	{
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
		for (int i = 0; i < this->getNumEle(); ++i)
			this->m_vpEles[i]->update_DmxDx(this->m_state_x->m_vx, this->m_state_0->m_vx);
	}

	this->m_isReady_DfxDx = true;
}

void SolidModel::update_DfxD0()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_DfxD0)
		return; // Already done

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->update_DfxD0(this->m_state_x->m_vx, this->m_state_0->m_vx);

	// Update mass derivative
	if (this->m_vg.norm() > 1e-6)
	{
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
		for (int i = 0; i < this->getNumEle(); ++i)
			this->m_vpEles[i]->update_DmxD0(this->m_state_x->m_vx, this->m_state_0->m_vx);
	}

	this->m_isReady_DfxD0 = true;
}

////////////////////////////////////////////////////////////////////////////
// Computed data getters

Real SolidModel::getEnergy()
{
	if (!this->m_isReady_Energy)
		this->update_Energy();

	Real energy = 0;
	for (int i = 0; i < this->getNumEle(); ++i)
		energy += this->m_vpEles[i]->getEnergyIntegral();

	//logSimu("[INFO] Total elastic energy: %f", energy);

	// Add gravity contribution
	if (m_vg.norm() > EPS_POS)
	{
		VectorXd vfg(this->m_nsimDOF_x);
		vfg.setZero(); // Init. to zero

		this->add_fgravity(vfg);
		energy -= m_state_x->m_vx.dot(vfg);
	}

	return energy;
}

void SolidModel::addForce(VectorXd& vf, const vector<bool>* pFixed)
{
	if (!this->m_isReady_Force)
		this->update_Force();

	this->m_assembleForTimer.restart();

	if ((int)vf.size() != this->m_nsimDOF_x)
	{
		vf.resize(this->m_nsimDOF_x);
		vf.setZero(); // Initialize
	}

	assert(vf.size() == this->m_nsimDOF_x);
	if (pFixed != NULL) // Check also fixed stencil
		assert((int)pFixed->size() == this->m_nsimDOF_x);

	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->addForce(vf, pFixed);

	//logSimu("[INFO] Total elastic forces: %f", vf.norm());

	// Add gravity contribution
	if (m_vg.norm() > EPS_POS)
	{
		this->add_fgravity(vf);
	}

	this->m_assembleForTimer.stopStoreLog();
}

void SolidModel::addJacobian(tVector& vJ, const vector<bool>* pFixed)
{
	if (!this->m_isReady_Jacobian)
		this->update_Jacobian();

	this->m_assembleJacTimer.restart();

	if (pFixed != NULL) // Check also fixed stencil
		assert((int)pFixed->size() == this->m_nsimDOF_x);

	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->addJacobian(vJ, pFixed);

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, this->m_nsimDOF_x);
	compressionMatrix.setFromTriplets(vJ.begin(), vJ.end());
	toTriplets(compressionMatrix, vJ);

	// Compress vector ------------------
#endif
}

void SolidModel::add_fx(VectorXd& vfx)
{
	this->addForce(vfx);
}

void SolidModel::add_f0(VectorXd& vf0)
{
	if (!this->m_isReady_f0)
		this->update_f0();

	if ((int)vf0.size() != this->m_nsimDOF_0)
	{
		vf0.resize(this->m_nsimDOF_0);
		vf0.setZero(); // Initialize
	}

	for (int i = 0; i < this->getNumEle(); ++i)
		this->m_vpEles[i]->add_f0(vf0); // Add
}

void SolidModel::add_DfxDx(tVector& vDfxDx, const vector<bool>* pFixed)
{
	if (!this->m_isReady_DfxDx)
		this->update_DfxDx();

	if (pFixed != NULL) // Check also fixed stencil
		assert((int)pFixed->size() == this->m_nsimDOF_x);

	for (int i = 0; i < this->getNumEle(); ++i) // Add
		this->m_vpEles[i]->add_DfxDx(vDfxDx, pFixed);

	// Add gravity contribution
	if (this->m_vg.norm() > 1e-6)
	{
		VectorXd vg(this->m_nsimDOF_x);
		vg.setZero(); // Gravity vector

		this->add_vgravity(vg);

		for (int i = 0; i < this->getNumEle(); ++i) // Add
		{
			tVector vDmxDx; vDmxDx.reserve(100);
			this->m_vpEles[i]->add_DfxDx(vDmxDx);

			int nC = (int) vDmxDx.size();
			for (int j = 0; j < nC; ++j)
			{
				const Triplet<Real>& t = vDmxDx[j]; // Multiply by gravity times identity
				vDfxDx.push_back(Triplet<Real>(t.row(), t.col(), t.value()*vg(t.row())));
			}
		}
	}

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, this->m_nsimDOF_x);
	compressionMatrix.setFromTriplets(vDfxDx.begin(), vDfxDx.end());
	toTriplets(compressionMatrix, vDfxDx);

	// Compress vector ------------------
#endif
}

void SolidModel::add_DfxD0(tVector& vDfxD0, const vector<bool>* pFixed)
{
	if (!this->m_isReady_DfxD0)
		this->update_DfxD0();

	if (pFixed != NULL) // Check also fixed stencil
		assert((int)pFixed->size() == this->m_nsimDOF_x);

	for (int i = 0; i < this->getNumEle(); ++i) // Add
		this->m_vpEles[i]->add_DfxD0(vDfxD0, pFixed);

	// Add gravity contribution
	if (this->m_vg.norm() > 1e-6)
	{
		VectorXd vg(this->m_nsimDOF_x);
		vg.setZero(); // Gravity vector

		this->add_vgravity(vg);

		for (int i = 0; i < this->getNumEle(); ++i) // Add
		{
			tVector vDmxD0; vDmxD0.reserve(100);
			this->m_vpEles[i]->add_DmxD0(vDmxD0);

			int nC = (int)vDmxD0.size();
			for (int j = 0; j < nC; ++j)
			{
				const Triplet<Real>& t = vDmxD0[j]; // Multiply by gravity times identity
				vDfxD0.push_back(Triplet<Real>(t.row(), t.col(), t.value()*vg(t.row())));
			}
		}
	}

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, this->m_nsimDOF_0);
	compressionMatrix.setFromTriplets(vDfxD0.begin(), vDfxD0.end());
	toTriplets(compressionMatrix, vDfxD0);

	// Compress vector ------------------
#endif
}

const VectorXd& SolidModel::get_Mass() const
{
	return this->m_vMass;
}

void SolidModel::set_Mass(const VectorXd& vm)
{
	assert((int) vm.size() == this->m_nsimDOF_x);

	this->m_vMass = vm;
}

void SolidModel::add_Mass(VectorXd& vM)
{
	if (!this->m_isReady_Mass)
		this->update_Mass();

	if ((int)vM.size() != this->m_nsimDOF_x)
	{
		vM.resize(this->m_nsimDOF_x);
		vM.setZero(); // Initialize
	}

	vM += this->m_vMass;
}

void SolidModel::add_Mass(tVector& mM)
{
	if (!this->m_isReady_Mass)
		this->update_Mass();

	for (int i = 0; i < this->m_nNZ_MassM; ++i)
		mM.push_back(this->m_mMass[i]); // Append
}

void SolidModel::updateSparseMatrixToLumpedMass()
{
	assert((int) this->m_vMass.size() == this->m_nsimDOF_x);

	this->m_mMass.clear();

	for (int i = 0; i < this->m_nsimDOF_x; ++i) // Add to the diagonal
		this->m_mMass.push_back(Triplet<Real>(i, i, this->m_vMass[i]));
}

////////////////////////////////////////////////////////////////////////////
// Discretization management

void SolidModel::update_SimDOF()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	this->m_nsimDOF_x = this->m_nrawDOF;
	this->m_nsimDOF_0 = this->m_nrawDOF;

	// RAW -> SIM

	this->m_vraw2sim_x.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_nrawDOF; ++i)
		this->m_vraw2sim_x[i] = i; // Basic

	this->m_vraw2sim_0.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_nrawDOF; ++i)
		this->m_vraw2sim_0[i] = i; // Basic
}

void SolidModel::getRawToSim_x(MatrixSd& ms2r) const
{
	ms2r.resize(this->m_nsimDOF_x, this->m_nrawDOF);

	int count = 0; 
	tVector vtriplets(this->m_nsimDOF_x);
	for (int i = 0; i < this->m_nrawDOF; ++i)
	{
		if (this->m_vraw2sim_x[i] != -1)
		{
			vtriplets[count++] = Triplet<Real>(this->m_vraw2sim_x[i], i, 1.0);
		}
	}
}

void SolidModel::getRawToSim_0(MatrixSd& ms2r) const
{
	ms2r.resize(this->m_nsimDOF_0, this->m_nrawDOF);

	int count = 0;
	tVector vtriplets(this->m_nsimDOF_0);
	for (int i = 0; i < this->m_nrawDOF; ++i)
	{
		if (this->m_vraw2sim_0[i] != -1)
		{
			vtriplets[count++] = Triplet<Real>(this->m_vraw2sim_0[i], i, 1.0);
		}
	}
}

void SolidModel::mapRaw2Sim(const iVector& vin, iVector& vout) const
{
	int nraw = (int) vin.size();
	vout.resize(nraw);
	for (int i = 0; i < nraw; ++i)
	{
		if (this->m_vraw2sim_x[vin[i]] != -1)
			vout[i] = this->m_vraw2sim_x[vin[i]];
	}
}

void SolidModel::mapSim2Raw(const iVector& vin, iVector& vout) const
{
	int nsim = (int)vin.size();
	vout.resize(nsim);

	for (int i = 0; i < nsim; ++i)
	{
		int idx = vin[i];
		for (int j = 0; j < this->m_nrawDOF; ++j)
		{
			if (this->m_vraw2sim_x[j] == idx)
			{
				// Found index
				vout[i] = j;
				break;
			}
		}
	}
}

void SolidModel::mapRaw2Sim(const VectorXd& vin, VectorXd& vout) const
{
	assert((int)vin.size() == this->m_nrawDOF);
	
	vout.resize(this->m_nsimDOF_x);
	
	for (int i = 0; i < this->m_nrawDOF; ++i)
	{
		if (this->m_vraw2sim_x[i] != -1)
			vout[this->m_vraw2sim_x[i]] = vin[i];
	}
}

void SolidModel::mapSim2Raw(const VectorXd& vin, VectorXd& vout) const
{
	assert((int)vin.size() == this->m_nsimDOF_x);

	vout.resize(this->m_nrawDOF);

	for (int i = 0; i < this->m_nrawDOF; ++i)
	{
		if (this->m_vraw2sim_x[i] != -1)
			vout[i] = vin[this->m_vraw2sim_x[i]];
		else vout[i] = -1; // Not simulated
	}
}

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
// Tests

void SolidModel::testForceLocal()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	for (int i = 0; i < this->getNumEle(); ++i)
	{
		Real error = this->m_vpEles[i]->testForceLocal(this->m_state_x->m_vx, this->m_state_x->m_vv);
		if (error > 1e-9)
			logSimu("[ERROR] Model: %s. Element: %u. Force test error (local): %.9f", this->m_ID.c_str(), i, error);
	}
}

void SolidModel::testJacobianLocal()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	for (int i = 0; i < this->getNumEle(); ++i)
	{
		Real error = this->m_vpEles[i]->testJacobianLocal(this->m_state_x->m_vx, this->m_state_x->m_vv);
		if (error > 1e-9)
			logSimu("[ERROR] Model: %s. Element: %u. Jacobian test error (local): %.9f", this->m_ID.c_str(), i, error);
	}
}

void SolidModel::testDfxDxLocal()
{
	for (int i = 0; i < this->getNumEle(); ++i)
	{
		Real error = this->m_vpEles[i]->testDfxDxLocal(this->m_state_x->m_vx, this->m_state_0->m_vx);
		if (error > 1e-9)
			logSimu("[ERROR] Model: %s. Element: %u. DfxDx test error (local): %.9f", this->m_ID.c_str(), i, error);
	}
}

void SolidModel::testDfxD0Local()
{
	for (int i = 0; i < this->getNumEle(); ++i)
	{
		Real error = this->m_vpEles[i]->testDfxD0Local(this->m_state_x->m_vx, this->m_state_0->m_vx);
		if (error > 1e-9)
			logSimu("[ERROR] Model: %s. Element: %u. DfxDX test error (local): %.9f\n", this->m_ID.c_str(), i, error);
	}
}

void SolidModel::testForceGlobal()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	pSolidState pStateIni = this->m_state_x->clone();

	double EPS = 1e-6;
	VectorXd vxFD = pStateIni->m_vx;
	VectorXd vfFD(this->m_nsimDOF_x);
	vfFD.setZero();
	for (int i = 0; i < this->m_nsimDOF_x; ++i)
	{
		// +
		this->setState_x(pStateIni);
		vxFD(i) += EPS;
		this->setPositions_x(vxFD);
		this->update_Energy();
		double ep = this->getEnergy();

		// -
		this->setState_x(pStateIni);
		vxFD(i) -= 2 * EPS;
		this->setPositions_x(vxFD);
		this->update_Energy();
		double em = this->getEnergy();

		// Estimate
		vfFD(i) = -(ep - em) / (2 * EPS);

		vxFD(i) += EPS;
	}

	// Recover previous
	this->setState_x(pStateIni);

	// Get analytic
	VectorXd vfA(this->m_nsimDOF_x);
	vfA.setZero();
	this->update_Force();
	this->addForce(vfA);

	// Compare
	VectorXd vfDiff = (vfA - vfFD);
	Real FDNorm = vfFD.norm();
	Real diffNorm = vfDiff.norm() / FDNorm;

	if (FDNorm < 1e-9)
		diffNorm = 0.0;

	if (diffNorm > 1e-9)
	{
		//writeToFile(vfA, "forceA.csv");
		//writeToFile(vfFD, "forceF.csv");
		//writeToFile(vfDiff, "forceError.csv"); // Trace
		logSimu("[ERROR] Model: %s. Force test error (global): %.9f", this->m_ID.c_str(), diffNorm);
	}
}

void SolidModel::testJacobianGlobal()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	pSolidState pStateIni = this->m_state_x->clone();

	double EPS = 1e-6;
	VectorXd vxFD = pStateIni->m_vx;
	MatrixXd mJFD(this->m_nsimDOF_x, this->m_nsimDOF_x);
	mJFD.setZero();
	for (int j = 0; j < this->m_nsimDOF_x; ++j)
	{
		// +
		this->setState_x(pStateIni);
		vxFD(j) += EPS;
		VectorXd vfp(this->m_nsimDOF_x);
		this->setPositions_x(vxFD);
		this->update_Force();
		vfp.setZero();
		this->addForce(vfp);

		// -
		this->setState_x(pStateIni);
		vxFD(j) -= 2 * EPS;
		VectorXd vfm(this->m_nsimDOF_x);
		this->setPositions_x(vxFD);
		this->update_Force();
		vfm.setZero();
		this->addForce(vfm);

		// Estimate
		VectorXd vfFD = (vfp - vfm) / (2 * EPS);
		for (int i = 0; i < this->m_nsimDOF_x; ++i)
			mJFD(i, j) = vfFD(i); // Set Jacobian

		vxFD(j) += EPS;
	}

	// Recover previous
	this->setState_x(pStateIni);

	// Get analytic
	tVector vJA;
	this->update_Jacobian();
	vJA.reserve(this->getNumNonZeros_Jacobian());
	this->addJacobian(vJA); // Add Jacobian coefs.

	MatrixSd mJASparseT(m_nsimDOF_x, m_nsimDOF_x);
	mJASparseT.setFromTriplets(vJA.begin(), vJA.end());
	MatrixSd mJASparseC = mJASparseT.selfadjointView<Lower>();
	MatrixXd mJA = MatrixXd(mJASparseC); // Create dense matrix copy

	// Compare
	MatrixXd mJDiff = (mJA - mJFD);
	Real FDNorm = mJFD.norm();
	Real diffNorm = mJDiff.norm() / FDNorm;

	if (FDNorm < 1e-9)
		diffNorm = 0.0;

	if (diffNorm > 1e-9)
	{
		//writeToFile(mJA, "JacobianA.csv"); // Trace
		//writeToFile(mJFD, "JacobianFD.csv"); // Trace
		//writeToFile(mJDiff, "JacobianError.csv"); // Trace
		logSimu("[ERROR] Model: %s. Jacobian test error (global): %.9f", this->m_ID.c_str(), diffNorm);
	}
}

void SolidModel::testDfxDxGlobal()
{
	pSolidState pStateIni = this->m_state_x->clone();

	double EPS = 1e-6;
	VectorXd vxFD = pStateIni->m_vx;
	MatrixXd mJFD(this->m_nsimDOF_x, this->m_nsimDOF_x);
	mJFD.setZero();
	for (int j = 0; j < this->m_nsimDOF_x; ++j)
	{
		// +
		this->setState_x(pStateIni);
		vxFD(j) += EPS;
		VectorXd vfp(this->m_nsimDOF_x);
		this->setPositions_x(vxFD);
		this->update_Force();
		vfp.setZero();
		this->addForce(vfp);

		// -
		this->setState_x(pStateIni);
		vxFD(j) -= 2 * EPS;
		VectorXd vfm(this->m_nsimDOF_x);
		this->setPositions_x(vxFD);
		this->update_Force();
		vfm.setZero();
		this->addForce(vfm);

		// Estimate
		VectorXd vfFD = (vfp - vfm) / (2 * EPS);
		for (int i = 0; i < this->m_nsimDOF_x; ++i)
			mJFD(i, j) = vfFD(i); // Set Jacobian

		vxFD(j) += EPS;
	}

	// Recover previous
	this->setState_x(pStateIni);

	// Get analytic
	tVector vJA;
	this->update_DfxDx();
	this->add_DfxDx(vJA);
	MatrixSd mJASparse(m_nsimDOF_x, m_nsimDOF_x);
	mJASparse.setFromTriplets(vJA.begin(), vJA.end());
	MatrixXd mJA = MatrixXd(mJASparse);

	// Compare
	MatrixXd mJDiff = (mJA - mJFD);
	Real FDNorm = mJFD.norm();
	Real diffNorm = mJDiff.norm() / FDNorm;

	if (FDNorm < 1e-9)
		diffNorm = 0.0;

	if (diffNorm > 1e-9)
	{
		//writeToFile(mJA, "JacobianA.csv"); // Trace
		//writeToFile(mJFD, "JacobianFD.csv"); // Trace
		//writeToFile(mJDiff, "JacobianFDError.csv"); // Trace
		logSimu("[ERROR] Model: %s. Jacobian test error (global): %.9f", this->m_ID.c_str(), diffNorm);
	}

	this->update_Force(); // Recover force vector
}

void SolidModel::testDfxD0Global()
{
	double EPS = 1e-6;
	VectorXd vX = this->m_state_0->m_vx;
	VectorXd vXFD = this->m_state_0->m_vx;
	MatrixXd mJFD(this->m_nsimDOF_x, this->m_nsimDOF_0);
	mJFD.setZero();
	for (int j = 0; j < this->m_nsimDOF_0; ++j)
	{
		// +
		vXFD(j) += EPS;
		VectorXd vfp(this->m_nsimDOF_x);
		this->setParam_Rest(vXFD);
		this->update_Force();
		vfp.setZero();
		this->addForce(vfp);

		// -
		vXFD(j) -= 2 * EPS;
		VectorXd vfm(this->m_nsimDOF_x);
		this->setParam_Rest(vXFD);
		this->update_Force();
		vfm.setZero();
		this->addForce(vfm);

		// Estimate
		VectorXd vfFD = (vfp - vfm) / (2 * EPS);
		for (int i = 0; i < this->m_nsimDOF_x; ++i)
			mJFD(i, j) = vfFD(i); // Set Jacobian

		vXFD(j) += EPS;
	}

	this->setParam_Rest(vX);

	// Get analytic
	tVector vJA;
	this->update_DfxD0();
	this->add_DfxD0(vJA);
	MatrixSd mJASparse(m_nsimDOF_x, m_nsimDOF_0);
	mJASparse.setFromTriplets(vJA.begin(), vJA.end());
	MatrixXd mJA = MatrixXd(mJASparse);

	// Compare
	MatrixXd mJDiff = (mJA - mJFD);
	Real FDNorm = mJFD.norm();
	Real diffNorm = mJDiff.norm() / FDNorm;

	if (FDNorm < 1e-9)
		diffNorm = 0.0;

	if (diffNorm > 1e-9)
	{
		//writeToFile(mJA, "JacobianA.csv"); // Trace
		//writeToFile(mJFD, "JacobianFD.csv"); // Trace
		//writeToFile(mJDiff, "JacobianFDError.csv"); // Trace
		logSimu("[ERROR] Model: %s. Jacobian test error (global): %.9f", this->m_ID.c_str(), diffNorm);
	}

	this->update_Force(); // Recover force vector
}
