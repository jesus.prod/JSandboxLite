/*=====================================================================================*/
/*!
/file		DERModel.h
/author		jesusprod
/brief		DERModel.h implementation.
*/
/*=====================================================================================*/

#ifndef DER_MODEL_H
#define DER_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/Rod.h>
#include <JSandbox/Model/SolidModel.h>
#include <JSandbox/Element/DERBendingElement.h>
#include <JSandbox/Element/DERStretchElement.h>
#include <JSandbox/Element/DERTwistElement.h>
#include <JSandbox/Element/DERBendTwistElement.h>

#include <JSandbox/Maple/eulerRotation.h>
#include <JSandbox/Maple/parallelTransportNormalized.h>
#include <JSandbox/Maple/computeReferenceTwistChange.h>

class   DERState : public SolidState
{
public:
	Rod m_rod;
	dVector m_va;
	dVector m_vt;
	vector<Frame> m_vF;

	DERState()
	{
		// Nothing to do here...
	}

	virtual ~DERState()
	{
		// Nothing to do here...
	}

	DERState(const DERState& toCopy) : SolidState(toCopy)
	{
		this->m_rod = toCopy.m_rod;
		this->m_va = toCopy.m_va;
		this->m_vt = toCopy.m_vt;
		this->m_vF = toCopy.m_vF;
	}

	virtual pSolidState clone() const
	{
		DERState* pcloned = new DERState();
		*pcloned = *this;

		return pSolidState(pcloned);
	}
};

typedef std::shared_ptr<DERState> pDERState;

class   DERModel : public SolidModel
{
public:
	DERModel();

	virtual ~DERModel();

	/* From SolidModel */

	virtual void setup();

	virtual void setState_x(pSolidState s);
	virtual void setState_0(pSolidState s);
	virtual void setPositions_x(const VectorXd& vx);
	virtual void setPositions_0(const VectorXd& v0);

	virtual void update_Rest();
	virtual void update_Mass();

	virtual void update_Force();
	virtual void update_Energy();
	virtual void update_Jacobian();

	virtual void update_EnergyForce();
	virtual void update_ForceJacobian();
	virtual void update_EnergyForceJacobian();

	virtual void add_vgravity(VectorXd& vf);
	virtual void add_fgravity(VectorXd& vf);

	virtual void addGravity(const VectorXd& vg, VectorXd& vf, const vector<bool>* pvFixed = NULL);

	virtual iVector getAABBIndices(const dVector& vaabb) const;

	virtual void testForceLocal();
	virtual void testJacobianLocal();

	void deprecateDiscretization();
	void deprecateDeformation();

	void initializeMaterials();

	/* From SolidModel */

	virtual void setStateToMesh_x(const Rod& rod);
	virtual void setStateToMesh_0(const Rod& rod);

	virtual void getParam_Split(iVector& vidx) const;
	virtual void setParam_Split(const iVector& vidx);

	virtual bool getParam_Dummy() const;
	virtual void setParam_Dummy(bool s);

	virtual bool getParam_Straight() const;
	virtual void setParam_Straight(bool s);

	virtual void configureRod(const Rod& rod_x, const Rod& rod_0);

	virtual const Rod& getRod_x() const { return this->m_stateDER_x->m_rod; }
	virtual const dVector& getMatTwist_x() const { return this->m_stateDER_x->m_va; }
	virtual const dVector& getRefTwist_x() const { return this->m_stateDER_x->m_vt; }
	virtual const vector<Frame>& getRefFrames_x() const { return this->m_stateDER_x->m_vF; }
	virtual const vector<Frame>& getMatFrames_x() const { return this->m_stateDER_x->m_rod.getFrames(); }

	virtual const Rod& getRod_0() const { return this->m_stateDER_0->m_rod; }
	virtual const dVector& getMatTwist_0() const { return this->m_stateDER_0->m_va; }
	virtual const dVector& getRefTwist_0() const { return this->m_stateDER_0->m_vt; }
	virtual const vector<Frame>& getRefFrames_0() const { return this->m_stateDER_0->m_vF; }
	virtual const vector<Frame>& getMatFrames_0() const { return this->m_stateDER_0->m_rod.getFrames(); }


	// BASIC

	virtual void setParam_StretchK(const vector<VectorXd>& vsK);
	virtual void getParam_StretchK(vector<VectorXd>& vsK) const;

	virtual void setParam_RestLengthEdge(const VectorXd& vl);
	virtual void getParam_RestLengthEdge(VectorXd& vl) const;

	virtual void setParam_RestLengthRod(Real l);
	virtual Real getParam_RestLengthRod() const;

	virtual void setParam_RestAngle(const VectorXd& va);
	virtual void getParam_RestAngle(VectorXd& va) const;

	// BASIC

	virtual void setParam_VertexRest(const VectorXd& v0v);
	virtual void getParam_VertexRest(VectorXd& v0v) const;

	//virtual void setParam_LengthEdge(const VectorXd& vl);
	//virtual void getParam_LengthEdge(VectorXd& vl) const;

	//virtual void setParam_LengthRod(Real l);
	//virtual Real getParam_LengthRod() const;

	virtual void setParam_RadiusEdge(const vector<Vector2d>& vr);
	virtual void getParam_RadiusEdge(vector<Vector2d>& vr) const;

	virtual void setParam_RadiusRod(const Vector2d& vr);
	virtual void getParam_RadiusRod(Vector2d& vr) const;

	virtual int getNumNonZeros_DfxD0v() { if (this->m_nNZ_DfxD0v == -1) this->precompute_DfxD0v(); return this->m_nNZ_DfxD0v; }
	virtual int getNumNonZeros_DfxDlr() { if (this->m_nNZ_DfxDlr == -1) this->precompute_DfxDlr(); return this->m_nNZ_DfxDlr; }
	virtual int getNumNonZeros_DfxDle() { if (this->m_nNZ_DfxDle == -1) this->precompute_DfxDle(); return this->m_nNZ_DfxDle; }
	virtual int getNumNonZeros_DfxDrr() { if (this->m_nNZ_DfxDrr == -1) this->precompute_DfxDrr(); return this->m_nNZ_DfxDrr; }
	virtual int getNumNonZeros_DfxDre() { if (this->m_nNZ_DfxDre == -1) this->precompute_DfxDre(); return this->m_nNZ_DfxDre; }
	
	virtual bool isReady_f0v() const { return this->m_isReady_f0v; }
	virtual bool isReady_flr() const { return this->m_isReady_flr; }
	virtual bool isReady_fle() const { return this->m_isReady_fle; }
	virtual bool isReady_frr() const { return this->m_isReady_frr; }
	virtual bool isReady_fre() const { return this->m_isReady_fre; }

	virtual bool isReady_DfxD0v() const { return this->m_isReady_DfxD0v; }
	virtual bool isReady_DfxDlr() const { return this->m_isReady_DfxDlr; }
	virtual bool isReady_DfxDle() const { return this->m_isReady_DfxDle; }
	virtual bool isReady_DfxDrr() const { return this->m_isReady_DfxDrr; }
	virtual bool isReady_DfxDre() const { return this->m_isReady_DfxDre; }

	virtual void update_f0v();
	virtual void update_flr();
	virtual void update_fle();
	virtual void update_frr();
	virtual void update_fre();

	virtual void update_DfxD0v();
	virtual void update_DfxDlr();
	virtual void update_DfxDle();
	virtual void update_DfxDrr();
	virtual void update_DfxDre();

	virtual void add_f0v(VectorXd& vfv);
	virtual void add_flr(VectorXd& vfl);
	virtual void add_fle(VectorXd& vfe);
	virtual void add_frr(VectorXd& vfr);
	virtual void add_fre(VectorXd& vfr);

	virtual void add_DfxD0v(tVector& vJ);
	virtual void add_DfxDlr(tVector& vJ);
	virtual void add_DfxDle(tVector& vJ);
	virtual void add_DfxDrr(tVector& vJ);
	virtual void add_DfxDre(tVector& vJ);

	virtual int getNumStretchEle() const { return (int) this->m_vpStretchEles.size(); }
	virtual int getNumBendingEle() const { return (int) this->m_vpBendingEles.size(); }
	virtual int getNumTwistEle() const { return (int) this->m_vpTwistEles.size(); }
	virtual int getNumBTEle() const { return (int) this->m_vpBTEles.size(); }

	virtual const vector<DERStretchElement*> getStretchElements() const { return this->m_vpStretchEles; }
	virtual const vector<DERBendingElement*> getBendingElements() const { return this->m_vpBendingEles; }
	virtual const vector<DERTwistElement*> getTwistElements() const { return this->m_vpTwistEles; }
	virtual const vector<DERBendTwistElement*> getBTElements() const { return this->m_vpBTEles; }

	void remeshUniform(const ModelCurve& vMC, const Rod& oldRod, Rod& newRod, int numV = -1, Real maxL = HUGE_VAL);
	void remeshUniform(const pVector& vnodesExtI, pVector& vnodesExtO, int numV = -1, Real maxL = HUGE_VAL);

	void compute_NodeStretchStrain(VectorXd& vs);
	void compute_NodeExtensionStrain(VectorXd& vs);
	void compute_NodeCompressionStrain(VectorXd& vs);

	SolidMaterial* getMaterial(int i) { return &this->m_vmats[i]; }

protected:

	virtual void precompute_DfxD0v();
	virtual void precompute_DfxDlr();
	virtual void precompute_DfxDle();
	virtual void precompute_DfxDrr();
	virtual void precompute_DfxDre();

	virtual void freeElements();

	virtual void updateMetadata_x();
	virtual void updateMetadata_0();
	virtual void updateRodCenter(const VectorXd& vx, Rod& rod) const;
	virtual void updateRodAngles(const VectorXd& vx, dVector& va) const;
	virtual void updateRodRefFrame(const Rod& rod, vector<Frame3d>& vref, dVector& vt) const;
	virtual void updateRodMatFrame(const dVector& va, const vector<Frame3d>& vref, Rod& rod) const;

	virtual void updateRodFrames(Rod& rod, dVector& va, vector<Frame3d>& vref, dVector& vt) const;

protected:

	bool m_isDummy;
	bool m_isStraight;

	int m_numDOFv;
	int m_numDOFa;

	pDERState m_stateDER_x;
	pDERState m_stateDER_0;

	vector<DERBendTwistElement*> m_vpBTEles;
	vector<DERStretchElement*> m_vpStretchEles;
	vector<DERBendingElement*> m_vpBendingEles;
	vector<DERTwistElement*> m_vpTwistEles;

	vector<SolidMaterial> m_vmats;

	int m_nNZ_DfxD0v;
	int m_nNZ_DfxDlr;
	int m_nNZ_DfxDle;
	int m_nNZ_DfxDrr;
	int m_nNZ_DfxDre;

	bool m_isReady_f0v;
	bool m_isReady_flr;
	bool m_isReady_fle;
	bool m_isReady_frr;
	bool m_isReady_fre;

	bool m_isReady_DfxD0v;
	bool m_isReady_DfxDlr;
	bool m_isReady_DfxDle;
	bool m_isReady_DfxDrr;
	bool m_isReady_DfxDre;

};

#endif