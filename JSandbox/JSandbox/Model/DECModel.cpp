/*=====================================================================================*/
/*!
/file		DECModeol.cpp
/author		jesusprod
/brief		Implementation of DECModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/DECModel.h>

#include <JSandbox/Maple/rodriguesRotation.h>
#include <JSandbox/Maple/getReferenceTwistOPT.h>

#ifdef _OPENMP 
#include <omp.h>
#endif

DECModel::DECModel() : SolidModel("DEC")
{
	this->m_stateDEC_x = pDECState(new DECState());
	this->m_stateDEC_0 = pDECState(new DECState());
	this->m_state_x = this->m_stateDEC_x;
	this->m_state_0 = this->m_stateDEC_0;

	this->deprecateConfiguration();
}

DECModel::~DECModel()
{
	logSimu("[INFO] Destroying DECModel: %u", this);
}

void DECModel::freeElements()
{
	SolidModel::freeElements();

	this->m_vpConEles.clear();
}

void DECModel::configureConnection(const Con& con, const Matrix3d& rot, const vector<DERModel*> vpRodModels)
{
	this->m_vprodModel.clear();

	this->m_stateDEC_x->m_R0x = rot;
	this->m_stateDEC_0->m_R0x = rot;
	this->m_connection = con;

	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();
	this->m_vprodModel.reserve(this->m_connection.m_srods.size());

	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it)
		this->m_vprodModel.push_back(vpRodModels[s_it->first]); // Add rods

	this->deprecateConfiguration();
}

void DECModel::setup()
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	int nr = (int) this->m_connection.m_srods.size();

	this->m_nrawDOF = 7 * nr + 3;
	this->m_nsimDOF_x = 4 * nr + 6;
	this->m_nsimDOF_0 = 4 * nr + 6;

	this->m_state_x->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vv.resize(this->m_nsimDOF_x);
	this->m_stateDEC_x->m_con.m_vrods.resize(nr);
	this->m_stateDEC_0->m_con.m_vrods.resize(nr);
	this->m_stateDEC_x->m_con.m_vR.setZero();
	this->m_stateDEC_0->m_con.m_vR.setZero();
	this->m_state_x->m_vv.setZero();
	this->m_state_0->m_vv.setZero();
	this->m_state_x->m_vx.setZero();
	this->m_state_0->m_vx.setZero();

	// Initialize connections

	int j = 0;
	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
	{
		int side = (*s_it).second;

		DERModel* pModel = this->getRodModel(j);
		const Rod& rod_x = pModel->getRod_x();
		const Rod& rod_0 = pModel->getRod_0();
		int nv = rod_x.getNumNode();
		int ne = rod_x.getNumEdge();

		this->m_stateDEC_0->m_con.m_vrods[j].m_S = (side == 0) ? 1.0 : -1.0;
		this->m_stateDEC_x->m_con.m_vrods[j].m_S = (side == 0) ? 1.0 : -1.0;

		this->m_stateDEC_0->m_con.m_vrods[j].m_rodFr = this->m_stateDEC_0->m_con.m_vrods[j].m_rodFc = (side == 0) ? rod_0.getFrame(0) : rod_0.getFrame(ne - 1);
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodFr = this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc = (side == 0) ? rod_x.getFrame(0) : rod_x.getFrame(ne - 1);

		this->m_stateDEC_x->m_con.m_vrods[j].m_vr = (side == 0) ? rod_x.getPosition(1) : rod_x.getPosition(nv - 2);
		this->m_stateDEC_0->m_con.m_vrods[j].m_vr = (side == 0) ? rod_0.getPosition(1) : rod_0.getPosition(nv - 2);

		if (j == 0)
		{
			this->m_stateDEC_x->m_con.m_vc = (side == 0) ? rod_x.getPosition(0) : rod_x.getPosition(nv - 1);
			this->m_stateDEC_0->m_con.m_vc = (side == 0) ? rod_0.getPosition(0) : rod_0.getPosition(nv - 1);
		}
	}

	if (this->m_stateDEC_x->m_R0x.isZero(1e-6))
	{
		this->resetBasicRotation();
	}
	else
	{
		logSimu("[INFO] Initial rotation already provided by the rod mesh");
	}

	if (this->m_stateDEC_x->m_R0x.isIdentity(EPS_POS))
	{
		logSimu("[INFO] Initial rotation is Identity");
	}
	else
	{
		logSimu("[INFO] Initial rotation not Identity");
	}

	this->resetRotatedFrames();
	this->resetReference(this->m_stateDEC_x->m_con);
	this->resetReference(this->m_stateDEC_0->m_con);
	this->updateConnectionToState(this->m_stateDEC_x->m_con, this->m_state_x->m_vx);
	this->updateConnectionToState(this->m_stateDEC_0->m_con, this->m_state_0->m_vx);

	int connOffset = 7 * nr;
	int edgeOffset = 6 * nr;

	this->m_vraw2sim_x.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_nrawDOF; ++i)
		this->m_vraw2sim_x[i] = i; // Basic

	// Make all first rod nodes point to the common node
	int connNodeOffset = 4 * (int) this->m_stateDEC_x->m_con.m_vrods.size();
	for (int i = 0; i < (int) this->m_stateDEC_x->m_con.m_vrods.size(); ++i)
	{
		int rodNodeOffset = 6 * i;
		this->m_vraw2sim_x[rodNodeOffset + 0] = connNodeOffset + 0;
		this->m_vraw2sim_x[rodNodeOffset + 1] = connNodeOffset + 1;
		this->m_vraw2sim_x[rodNodeOffset + 2] = connNodeOffset + 2;
		this->m_vraw2sim_x[rodNodeOffset + 3] -= 3 * (i + 1);
		this->m_vraw2sim_x[rodNodeOffset + 4] -= 3 * (i + 1);
		this->m_vraw2sim_x[rodNodeOffset + 5] -= 3 * (i + 1);
		this->m_vraw2sim_x[edgeOffset + i] -= 3 * nr;
	}
	this->m_vraw2sim_x[connOffset + 0] = connNodeOffset + 3;
	this->m_vraw2sim_x[connOffset + 1] = connNodeOffset + 4;
	this->m_vraw2sim_x[connOffset + 2] = connNodeOffset + 5;

	// Initialize connection elements

	this->freeElements();

	// Reserve space for elements
	this->m_vpConEles.reserve(nr);

	// Build elements

	int rcount = 0;

	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, rcount++)
	{
		int side = (*s_it).second;

		this->m_vpConEles.push_back(new DERConnectionElement());
		DERConnectionElement* pConEle = this->m_vpConEles.back();

		vector<SolidMaterial*> vpmats;
		int nodeOffset = 6 * rcount;
		iVector vDOFRaw, vDOFSim;

		// Rod nodes DOF
		for (int i = 0; i < 6; ++i)
			vDOFRaw.push_back(nodeOffset + i);

		// Connection  DOF
		for (int i = 0; i < 3; ++i)
			vDOFRaw.push_back(connOffset + i);

		// Rod edge DOF
		vDOFRaw.push_back(edgeOffset + rcount);

		// Get simulation DOF mapping
		this->mapRaw2Sim(vDOFRaw, vDOFSim);

		if (side == 0) // Start
		{
			vpmats.push_back(this->m_vprodModel[rcount]->getMaterial(0));
		}
		if (side == 1) // End
		{
			int ne = this->m_vprodModel[rcount]->getRod_x().getNumEdge();
			vpmats.push_back(this->m_vprodModel[rcount]->getMaterial(ne-1));
		}

		pConEle->setIndicesx(vDOFSim);
		pConEle->setIndices0(vDOFSim);
		pConEle->setRodIndex(rcount);
		pConEle->setMaterials(vpmats);

		this->m_vpEles.push_back(pConEle);
	}

	this->m_isReady_Setup = true;
	this->deprecateDiscretization();

	this->update_SimDOF();

	this->update_Rest();

	logSimu("[INFO] Initialized DECModel. Raw: %u, Sim: %u", this->m_nrawDOF, this->m_nsimDOF_x);
}

void DECModel::setStateToMesh_x(const Matrix3d& rot)
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	int nr = (int) this->m_connection.m_srods.size();

	this->m_state_x->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vv.resize(this->m_nsimDOF_x);
	this->m_stateDEC_x->m_con.m_vrods.resize(nr);
	this->m_stateDEC_0->m_con.m_vrods.resize(nr);
	this->m_stateDEC_x->m_con.m_vR.setZero();
	this->m_stateDEC_0->m_con.m_vR.setZero();
	this->m_state_x->m_vv.setZero();
	this->m_state_0->m_vv.setZero();
	this->m_state_x->m_vx.setZero();
	this->m_state_0->m_vx.setZero();

	this->m_stateDEC_x->m_R0x = rot;

	// Initialize frames and twist
	for (int j = 0; j < nr; ++j)
	{
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.tan = this->m_stateDEC_x->m_R0x*this->m_stateDEC_0->m_con.m_vrods[j].m_rodFc.tan;
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.nor = this->m_stateDEC_x->m_R0x*this->m_stateDEC_0->m_con.m_vrods[j].m_rodFc.nor;
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.bin = this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.tan.cross(this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.nor);
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodt += computeReferenceTwistChange(
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.tan.data(),
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.nor.data(),
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodFr.tan.data(),
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodFr.nor.data(),
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodt);
	}

	// Update state from connection

	this->updateConnectionToState(this->m_stateDEC_x->m_con, this->m_state_x->m_vx);
	this->updateConnectionToState(this->m_stateDEC_0->m_con, this->m_state_0->m_vx);
}

void DECModel::setStateToMesh_0(const Matrix3d& rot)
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	int nr = (int) this->m_connection.m_srods.size();

	this->m_state_x->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vv.resize(this->m_nsimDOF_x);
	this->m_stateDEC_x->m_con.m_vrods.resize(nr);
	this->m_stateDEC_0->m_con.m_vrods.resize(nr);
	this->m_stateDEC_x->m_con.m_vR.setZero();
	this->m_stateDEC_0->m_con.m_vR.setZero();
	this->m_state_x->m_vv.setZero();
	this->m_state_0->m_vv.setZero();
	this->m_state_x->m_vx.setZero();
	this->m_state_0->m_vx.setZero();

	this->m_stateDEC_x->m_R0x = rot;

	// Initialize frames and twist
	for (int j = 0; j < nr; ++j)
	{
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.tan = this->m_stateDEC_x->m_R0x*this->m_stateDEC_0->m_con.m_vrods[j].m_rodFc.tan;
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.nor = this->m_stateDEC_x->m_R0x*this->m_stateDEC_0->m_con.m_vrods[j].m_rodFc.nor;
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.bin = this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.tan.cross(this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.nor);
		this->m_stateDEC_x->m_con.m_vrods[j].m_rodt += computeReferenceTwistChange(
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.tan.data(),
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodFc.nor.data(),
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodFr.tan.data(),
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodFr.nor.data(),
			this->m_stateDEC_x->m_con.m_vrods[j].m_rodt);
	}

	// Update state from connection

	this->updateConnectionToState(this->m_stateDEC_x->m_con, this->m_state_x->m_vx);
	this->updateConnectionToState(this->m_stateDEC_0->m_con, this->m_state_0->m_vx);

	this->deprecateDeformation();
	this->deprecateUndeformed();
}

void DECModel::setState_x(pSolidState s)
{
	*this->m_stateDEC_x = DECState(*static_cast<DECState*>(s.get()));

	this->deprecateDeformation();
}

void DECModel::setState_0(pSolidState s)
{
	*this->m_stateDEC_0 = DECState(*static_cast<DECState*>(s.get()));

	this->deprecateUndeformed();
}

void DECModel::setPositions_x(const VectorXd& vx)
{
	SolidModel::setPositions_x(vx);
	this->updateStateToConnection(this->m_stateDEC_x->m_vx, this->m_stateDEC_x->m_con);
	this->resetEulerRotation();
	this->resetRotatedFrames();
	this->resetReference(this->m_stateDEC_x->m_con);
}

void DECModel::setPositions_0(const VectorXd& vX)
{
	SolidModel::setPositions_0(vX);
	this->updateStateToConnection(this->m_stateDEC_0->m_vx, this->m_stateDEC_0->m_con);
	this->resetEulerRotation();
	this->resetRotatedFrames();
	this->resetReference(this->m_stateDEC_0->m_con);
}

void DECModel::update_Rest()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Rest)
		return; // Already done

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumConEle(); ++i)
		this->m_vpConEles[i]->update_Rest(this->m_stateDEC_0->m_con);

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void DECModel::update_Energy()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Energy)
		return; // Already done

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumConEle(); ++i)
		this->m_vpConEles[i]->update_Energy(this->m_stateDEC_x->m_con);

	this->m_isReady_Energy = true;
}

void DECModel::update_Force()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Force)
		return; // Already done

	this->m_updateForTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumConEle(); ++i)
		this->m_vpConEles[i]->update_Force(this->m_stateDEC_x->m_con);

	this->m_isReady_Force = true;

	this->m_updateForTimer.stopStoreLog();
}

void DECModel::update_Jacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian)
		return; // Already done

	this->m_updateJacTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumConEle(); ++i)
		this->m_vpConEles[i]->update_Jacobian(this->m_stateDEC_x->m_con);

	this->m_isReady_Jacobian = true;

	this->m_updateJacTimer.stopStoreLog();
}

void DECModel::update_EnergyForce()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Energy && this->m_isReady_Force)
		return; // Both of them already computed, out

	this->m_updateForTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumConEle(); ++i)
		this->m_vpConEles[i]->update_EnergyForce(this->m_stateDEC_x->m_con);


	this->m_isReady_Energy = true;
	this->m_isReady_Force = true;

	this->m_updateForTimer.stopStoreLog();
}

void DECModel::update_ForceJacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Force && this->m_isReady_Jacobian)
		return; // Both of them already computed, out

	this->m_updateForTimer.restart();
	this->m_updateJacTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumConEle(); ++i)
		this->m_vpConEles[i]->update_ForceJacobian(this->m_stateDEC_x->m_con);

	this->m_isReady_Force = true;
	this->m_isReady_Jacobian = true;

	this->m_updateForTimer.stopStoreLog();
	this->m_updateJacTimer.stopStoreLog();
}

void DECModel::update_EnergyForceJacobian()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_Jacobian &&
		this->m_isReady_Force &&
		this->m_isReady_Energy)
		return; // All done

	this->m_updateForTimer.restart();
	this->m_updateJacTimer.restart();

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < this->getNumConEle(); ++i)
		this->m_vpConEles[i]->update_EnergyForceJacobian(this->m_stateDEC_x->m_con);


	this->m_isReady_Energy = true;
	this->m_isReady_Force = true;
	this->m_isReady_Jacobian = true;

	this->m_updateForTimer.stopStoreLog();
	this->m_updateJacTimer.stopStoreLog();
}

void DECModel::deprecateDiscretization()
{
	SolidModel::deprecateDiscretization();

	this->m_nNZ_DfxD0v = -1;
	this->m_nNZ_DfxDr = -1;
	this->m_nNZ_DfxDl = -1;
}

void DECModel::deprecateDeformation()
{
	SolidModel::deprecateDeformation();

	this->m_isReady_f0v = false;
	this->m_isReady_fr = false;
	this->m_isReady_fl = false;
	this->m_isReady_DfxD0v = false;
	this->m_isReady_DfxDr = false;
	this->m_isReady_DfxDl = false;
}

void DECModel::precompute_DfxD0v()
{
	tVector testJ;
	this->add_DfxD0v(testJ); // Precompute
	this->m_nNZ_DfxD0v = (int)testJ.size();
}

void DECModel::precompute_DfxDl()
{
	tVector testJ;
	this->add_DfxDl(testJ); // Precompute
	this->m_nNZ_DfxDl = (int)testJ.size();
}

void DECModel::precompute_DfxDr()
{
	tVector testJ;
	this->add_DfxDr(testJ); // Precompute
	this->m_nNZ_DfxDr = (int)testJ.size();
}

void DECModel::update_fv()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_f0v)
		return; // Already done

	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->update_f0v(this->m_stateDEC_0->m_con, this->m_stateDEC_x->m_con, this->m_stateDEC_x->m_R0x);

	this->m_isReady_f0v = true;
}

void DECModel::update_fl()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_fl)
		return; // Already done

	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->update_fl(this->m_stateDEC_x->m_con);

	this->m_isReady_fl = true;
}

void DECModel::update_fr()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_fr)
		return; // Already done

	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->update_fr(this->m_stateDEC_x->m_con);

	this->m_isReady_fr = true;
}

void DECModel::update_DfxD0v()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_DfxD0v)
		return; // Already done

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->update_DfxD0v(this->m_stateDEC_0->m_con, this->m_stateDEC_x->m_con, this->m_stateDEC_x->m_R0x);

	this->m_isReady_DfxD0v = true;
}

void DECModel::update_DfxDl()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_DfxDl)
		return; // Already done

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->update_DfxDl(this->m_stateDEC_x->m_con);

	this->m_isReady_DfxDl = true;
}


void DECModel::update_DfxDr()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	if (this->m_isReady_DfxDr)
		return; // Already done

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->update_DfxDr(this->m_stateDEC_x->m_con);

	this->m_isReady_DfxDr = true;
}

void DECModel::add_f0v(VectorXd& vfl)
{
	if (!this->m_isReady_f0v)
		this->update_fv();

	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->add_f0v(vfl);
}

void DECModel::add_fl(VectorXd& vfl)
{
	if (!this->m_isReady_fl)
		this->update_fl();

	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->add_fl(vfl);
}

void DECModel::add_fr(VectorXd& vfr)
{
	if (!this->m_isReady_fr)
		this->update_fr();

	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->add_fl(vfr);
}

void DECModel::add_DfxD0v(tVector& vDfxD0v)
{
	if (!this->m_isReady_DfxD0v)
		this->update_DfxD0v();

	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->add_DfxD0v(vDfxD0v);

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, 6 * this->getNumConEle());
	compressionMatrix.setFromTriplets(vDfxD0v.begin(), vDfxD0v.end());
	toTriplets(compressionMatrix, vDfxD0v);

	// Compress vector ------------------
#endif
}

void DECModel::add_DfxDl(tVector& vDfxDl)
{
	if (!this->m_isReady_DfxDl)
		this->update_DfxDl();

	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->add_DfxDl(vDfxDl);

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, this->getNumConEle());
	compressionMatrix.setFromTriplets(vDfxDl.begin(), vDfxDl.end());
	toTriplets(compressionMatrix, vDfxDl);

	// Compress vector ------------------
#endif
}

void DECModel::add_DfxDr(tVector& vDfxDr)
{
	if (!this->m_isReady_DfxDr)
		this->update_DfxDr();
	
	for (int i = 0; i < (int) this->m_vpConEles.size(); ++i)
		this->m_vpConEles[i]->add_DfxDr(vDfxDr);

#ifdef MCOMPRESSION
	// Compress vector ------------------

	MatrixSd compressionMatrix(this->m_nsimDOF_x, 2*this->getNumConEle());
	compressionMatrix.setFromTriplets(vDfxDr.begin(), vDfxDr.end());
	toTriplets(compressionMatrix, vDfxDr);

	// Compress vector ------------------
#endif
}

void DECModel::updateConnectionToState(ConState& CS, VectorXd& vx)
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	int nr = (int)CS.m_vrods.size();

	int conDOFOffset = nr * 4;

	// Center
	vx(conDOFOffset + 0) = CS.m_vc.x();
	vx(conDOFOffset + 1) = CS.m_vc.y();
	vx(conDOFOffset + 2) = CS.m_vc.z();

	// Rotation
	vx(conDOFOffset + 3) = CS.m_vR.x();
	vx(conDOFOffset + 4) = CS.m_vR.y();
	vx(conDOFOffset + 5) = CS.m_vR.z();

	int j = 0;

	int rodEdgeDOFOffset = nr * 3;

	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
	{
		// Update node values
		int rodNodeDOFOffset = 3*j;
		vx(rodNodeDOFOffset + 0) = CS.m_vrods[j].m_vr.x();
		vx(rodNodeDOFOffset + 1) = CS.m_vrods[j].m_vr.y();
		vx(rodNodeDOFOffset + 2) = CS.m_vrods[j].m_vr.z();

		// Update angle values
		vx(rodEdgeDOFOffset + j) = CS.m_vrods[j].m_roda;
	}
}

void DECModel::updateStateToConnection(VectorXd& vx, ConState& CS)
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	int nr = (int)CS.m_vrods.size();

	int conDOFOffset = nr * 4;

	CS.m_vc = Vector3d(vx(conDOFOffset + 0),
					   vx(conDOFOffset + 1),
					   vx(conDOFOffset + 2));

	CS.m_vR = Vector3d(vx(conDOFOffset + 3),
					   vx(conDOFOffset + 4),
					   vx(conDOFOffset + 5));

	int j = 0;

	int rodEdgeDOFOffset = nr * 3;

	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
	{
		// Update node values
		int rodNodeDOFOffset = 3 * j;
		CS.m_vrods[j].m_vr.x() = vx(rodNodeDOFOffset + 0);
		CS.m_vrods[j].m_vr.y() = vx(rodNodeDOFOffset + 1);
		CS.m_vrods[j].m_vr.z() = vx(rodNodeDOFOffset + 2);

		// Update angle values
		CS.m_vrods[j].m_roda = vx(rodEdgeDOFOffset + j);
	}
}

void DECModel::resetReference(ConState& CS)
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	int j = 0;

	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
	{
		Vector3d matTanROx;
		if (CS.m_vrods[j].m_S > 0)
			matTanROx = CS.m_vrods[j].m_vr - CS.m_vc;
		else matTanROx = CS.m_vc - CS.m_vrods[j].m_vr;
		matTanROx.normalize();

		// Update reference frame

		Vector3d matNorROx;
		parallelTransportNormalized(CS.m_vrods[j].m_rodFr.tan.data(), matTanROx.data(), CS.m_vrods[j].m_rodFr.nor.data(), matNorROx.data());
		CS.m_vrods[j].m_rodFr.tan = matTanROx;
		CS.m_vrods[j].m_rodFr.nor = matNorROx;
		CS.m_vrods[j].m_rodFr.bin = matTanROx.cross(matNorROx);

		// Update reference twist

		CS.m_vrods[j].m_rodt += computeReferenceTwistChange(CS.m_vrods[j].m_rodFc.tan.data(),
															CS.m_vrods[j].m_rodFc.nor.data(),
															CS.m_vrods[j].m_rodFr.tan.data(),
															CS.m_vrods[j].m_rodFr.nor.data(),
															CS.m_vrods[j].m_rodt);
	}
}

void DECModel::resetRotations(Matrix3d& mR)
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	VectorXd& vx = this->m_state_x->m_vx;
	ConState& CSx = this->m_stateDEC_x->m_con;
	ConState& CS0 = this->m_stateDEC_0->m_con;

	int nr = (int)CSx.m_vrods.size();

	int conDOFOffset = nr * 4;

	double eulerAngles[3];
	eulerAngles[0] = vx(conDOFOffset + 3);
	eulerAngles[1] = vx(conDOFOffset + 4);
	eulerAngles[2] = vx(conDOFOffset + 5);

	// Make rotation zero
	vx(conDOFOffset + 3) = 0;
	vx(conDOFOffset + 4) = 0;
	vx(conDOFOffset + 5) = 0;
	CSx.m_vR.setZero();

	// Update R
	Matrix3d newR;
	eulerRotation(eulerAngles, this->m_stateDEC_x->m_R0x.col(0).data(), newR.col(0).data());
	eulerRotation(eulerAngles, this->m_stateDEC_x->m_R0x.col(1).data(), newR.col(1).data());
	eulerRotation(eulerAngles, this->m_stateDEC_x->m_R0x.col(2).data(), newR.col(2).data());
	this->m_stateDEC_x->m_R0x = newR;

	int j = 0;

	// Update connection frames
	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
	{
		CSx.m_vrods[j].m_rodFc.tan = this->m_stateDEC_x->m_R0x * CS0.m_vrods[j].m_rodFr.tan;
		CSx.m_vrods[j].m_rodFc.nor = this->m_stateDEC_x->m_R0x * CS0.m_vrods[j].m_rodFr.nor;
		CSx.m_vrods[j].m_rodFc.bin = CSx.m_vrods[j].m_rodFc.tan.cross(CSx.m_vrods[j].m_rodFc.nor);
	}
}

void DECModel::update_SimDOF()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	// RAW -> SIM

	this->m_vraw2sim_x.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_nrawDOF; ++i)
		this->m_vraw2sim_x[i] = i; // Basic

	int nr = (int) this->m_stateDEC_x->m_con.m_vrods.size();

	int connOffset = 7 * nr;
	int edgeOffset = 6 * nr;

	// Make all first rod nodes point to the common node
	int connNodeOffset = 4 * (int) this->m_stateDEC_x->m_con.m_vrods.size();
	for (int i = 0; i < (int) this->m_stateDEC_x->m_con.m_vrods.size(); ++i)
	{
		int rodNodeOffset = 6 * i;
		this->m_vraw2sim_x[rodNodeOffset + 0] = connNodeOffset + 0;
		this->m_vraw2sim_x[rodNodeOffset + 1] = connNodeOffset + 1;
		this->m_vraw2sim_x[rodNodeOffset + 2] = connNodeOffset + 2;
		this->m_vraw2sim_x[rodNodeOffset + 3] -= 3 * (i + 1);
		this->m_vraw2sim_x[rodNodeOffset + 4] -= 3 * (i + 1);
		this->m_vraw2sim_x[rodNodeOffset + 5] -= 3 * (i + 1);
		this->m_vraw2sim_x[edgeOffset + i] -= 3 * nr;
	}
	this->m_vraw2sim_x[connOffset + 0] = connNodeOffset + 3;
	this->m_vraw2sim_x[connOffset + 1] = connNodeOffset + 4;
	this->m_vraw2sim_x[connOffset + 2] = connNodeOffset + 5;

	this->m_vraw2sim_0.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_nrawDOF; ++i)
		this->m_vraw2sim_0[i] = i; // Basic

	// Check S
}

void DECModel::updateVertexRest()
{
	set<pair<int, int>>::iterator itCur = this->m_connection.m_srods.begin();
	set<pair<int, int>>::iterator itEnd = this->m_connection.m_srods.end();

	int j = 0;
	for (; itCur != itEnd; ++itCur, j++)
	{
		int nv = this->m_vprodModel[j]->getRod_0().getNumNode();

		if (itCur->second == 0)
		{
			if (j == 0) this->m_stateDEC_0->m_con.m_vc = this->m_vprodModel[j]->getRod_0().getCurve().getPosition(0);
			this->m_stateDEC_0->m_con.m_vrods[j].m_vr = this->m_vprodModel[j]->getRod_0().getCurve().getPosition(1);
		}

		if (itCur->second == 1)
		{
			if (j == 0) this->m_stateDEC_0->m_con.m_vc = this->m_vprodModel[j]->getRod_0().getCurve().getPosition(nv-1);
			this->m_stateDEC_0->m_con.m_vrods[j].m_vr = this->m_vprodModel[j]->getRod_0().getCurve().getPosition(nv-2);
		}
	}

	this->updateConnectionToState(this->m_stateDEC_0->m_con, this->m_state_0->m_vx);
	this->resetReference(this->m_stateDEC_0->m_con);
	this->resetEulerRotation();
	this->resetRotatedFrames();
	this->resetReference(this->m_stateDEC_x->m_con);

	this->deprecateUndeformed();
}

void DECModel::updateRadiusEdge()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void DECModel::updateLengthEdge()
{
	if (!this->m_isReady_Rest)
		this->update_Rest();

	set<pair<int, int>>::iterator itCur = this->m_connection.m_srods.begin();
	set<pair<int, int>>::iterator itEnd = this->m_connection.m_srods.end();

	int j = 0;
	for (; itCur != itEnd; ++itCur, j++)
	{
		if (itCur->second == 0)
			this->m_vpConEles[j]->setHalfEdgeLength(0.5*this->m_vprodModel[j]->getStretchElements().front()->getRestEdgeLength(0));
		if (itCur->second == 1)
			this->m_vpConEles[j]->setHalfEdgeLength(0.5*this->m_vprodModel[j]->getStretchElements().back()->getRestEdgeLength(0));
	}

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void DECModel::getParam_RestTangents(vector<Vector3d>& vt0) const
{
	int numRod = this->m_connection.m_srods.size();
	vt0.resize(numRod);
	for (int i = 0; i < numRod; ++i)
	{
		if (this->m_stateDEC_0->m_con.m_vrods[i].m_S > 0)
			vt0[i] = this->m_stateDEC_0->m_con.m_vrods[i].m_rodFr.tan;
		else vt0[i] = -this->m_stateDEC_0->m_con.m_vrods[i].m_rodFr.tan;
	}
}

void DECModel::setParam_RestTangents(const vector<Vector3d>& vt0)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int numRod = this->m_connection.m_srods.size();

	//// Test: randomly rotating input tangents
	//vector<Vector3d> testing = vt0;
	//Vector3d axis = Vector3d(0, 0, 1);
	//Real angle = ((double) rand() / (double) RAND_MAX)* 2 * M_PI;
	//std::cout << std::endl << "Axis (0, 0, 1), Angle: " << angle;
	//for (int i = 0; i < numRod; ++i)
	//{
	//	Vector3d original = vt0[i];
	//	testing[i].setZero();
	//	rodriguesRotation(axis.data(), angle, original.data(), testing[i].data());
	//	std::cout << std::endl << "Original: " << original.x() << " " << original.y() << " " << original.z();
	//	std::cout << std::endl << "Testing: " << testing[i].x() << " " << testing[i].y() << " " << testing[i].z();
	//}
	//std::cout << std::endl;

	for (int i = 0; i < numRod; ++i)
	{
		if ((vt0[i].norm() - 1.0) > EPS_APPROX)
		{
			logSimu("[WARNING] Non unitary rest tangent specified");
		}

		this->m_stateDEC_0->m_con.m_vrods[i].m_rodFc.tan = this->m_stateDEC_0->m_con.m_vrods[i].m_rodFr.tan = vt0[i];
		this->m_stateDEC_0->m_con.m_vrods[i].m_rodFc.nor = this->m_stateDEC_0->m_con.m_vrods[i].m_rodFr.nor = Vector3d(0, 0, 1);
		this->m_stateDEC_0->m_con.m_vrods[i].m_rodFc.bin = this->m_stateDEC_0->m_con.m_vrods[i].m_rodFr.bin = vt0[i].cross(Vector3d(0, 0, 1));
		this->m_stateDEC_0->m_con.m_vrods[i].m_roda = HUGE_VAL; // Invalidate
		this->m_stateDEC_0->m_con.m_vrods[i].m_rodt = HUGE_VAL; // Invalidate
		this->m_stateDEC_0->m_con.m_vrods[i].m_vr = Vector3d::Ones()*HUGE_VAL; // Invalidate
	}

	this->resetBasicRotation();
	this->resetRotatedFrames();
	this->resetReference(this->m_stateDEC_x->m_con);
	this->updateConnectionToState(this->m_stateDEC_x->m_con, this->m_state_x->m_vx);
	this->updateConnectionToState(this->m_stateDEC_0->m_con, this->m_state_0->m_vx);

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void DECModel::getParam_BendingK(vector<VectorXd>& vbK) const
{
	int numRod = this->m_connection.m_srods.size();
	vbK.resize(numRod);
	for (int i = 0; i < numRod; ++i)
	{
		vbK[i].resize(2);
		vbK[i][0] = this->m_vpEles[i]->getMaterial()->getBendingKw();
		vbK[i][1] = this->m_vpEles[i]->getMaterial()->getBendingKh();
	}
}

void DECModel::setParam_BendingK(const vector<VectorXd>& vbK)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int numRod = this->m_connection.m_srods.size();

	for (int i = 0; i < numRod; ++i)
	{
		this->m_vpEles[i]->getMaterial()->setBendingKw(vbK[i][0]);
		this->m_vpEles[i]->getMaterial()->setBendingKh(vbK[i][1]);
	}

	this->deprecateDeformation();
}

void DECModel::getParam_TwistK(vector<VectorXd>& vtK) const
{
	int numRod = this->m_connection.m_srods.size();
	vtK.resize(numRod);
	for (int i = 0; i < numRod; ++i)
	{
		vtK[i].resize(1);

		vtK[i][0] = this->m_vpEles[i]->getMaterial()->getTwistK();
	}
}

void DECModel::setParam_TwistK(const vector<VectorXd>& vtK)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int numRod = this->m_connection.m_srods.size();

	for (int i = 0; i < numRod; ++i)
	{
		this->m_vpEles[i]->getMaterial()->setTwistK(vtK[i][0]);
	}

	this->deprecateDeformation();
}

void DECModel::resetBasicRotation()
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	// Initialize connections

	Matrix3d mQ;
	mQ.setZero();

	Matrix3d mA;
	mA.setZero();

	int j = 0;
	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
	{
		// Estimate initial rotation

		const Frame3d& F0 = this->m_stateDEC_0->m_con.m_vrods[j].m_rodFr;
		const Frame3d& Fx = this->m_stateDEC_x->m_con.m_vrods[j].m_rodFr;

		mQ +=
			F0.tan*F0.tan.transpose() +
			F0.nor*F0.nor.transpose() +
			F0.bin*F0.bin.transpose();

		mA +=
			Fx.tan*F0.tan.transpose() +
			Fx.nor*F0.nor.transpose() +
			Fx.bin*F0.bin.transpose();
	}

	logSimu("[INFO] Estimating initial rotation with Polar Decomposition");

	if (abs(mQ.determinant()) < EPS_POS)
	{
		logSimu("[WARNING] Singular shape matching");
	}

	if (isnan(mQ.norm()))
	{
		logSimu("[WARNING] NaN shape matching matrix");
	}

	Matrix3d B = mA * mQ.inverse();
	Matrix3d R;
	Matrix3d S;
	if (computePolarDecomposition(B, R, S, EPS_POS) < 0.0)
	{
		logSimu("[WARNING] Polar decomposition error");

		R.setIdentity();
		S.setIdentity();
	}
	else
	{
		logSimu("[INFO] Polar decomposition correct");
	}

	if (!(R*R.transpose()).isIdentity(EPS_POS))
	{
		logSimu("[WARNING] Not initial exact rotation R*RT: %f", (R*R.transpose()).norm());
	}

	this->m_stateDEC_x->m_R0x = R;

	// Initialized to the best fit rotation
	// so euler angles should be set to zero

	this->m_stateDEC_x->m_con.m_vR.setZero();
}

void DECModel::resetEulerRotation()
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	VectorXd& vx = this->m_state_x->m_vx;
	ConState& CSx = this->m_stateDEC_x->m_con;
	ConState& CS0 = this->m_stateDEC_0->m_con;

	int nr = (int)CSx.m_vrods.size();

	int conDOFOffset = nr * 4;

	double eulerAngles[3];
	eulerAngles[0] = vx(conDOFOffset + 3);
	eulerAngles[1] = vx(conDOFOffset + 4);
	eulerAngles[2] = vx(conDOFOffset + 5);

	// Make rotation zero
	vx(conDOFOffset + 3) = 0;
	vx(conDOFOffset + 4) = 0;
	vx(conDOFOffset + 5) = 0;
	CSx.m_vR.setZero();

	// Update R
	Matrix3d newR;
	eulerRotation(eulerAngles, this->m_stateDEC_x->m_R0x.col(0).data(), newR.col(0).data());
	eulerRotation(eulerAngles, this->m_stateDEC_x->m_R0x.col(1).data(), newR.col(1).data());
	eulerRotation(eulerAngles, this->m_stateDEC_x->m_R0x.col(2).data(), newR.col(2).data());
	this->m_stateDEC_x->m_R0x = newR;
}

void DECModel::resetRotatedFrames()
{
	const Con& rc = this->m_connection;

	set<pair<int, int>>::iterator s_end = rc.m_srods.end();
	set<pair<int, int>>::iterator s_begin = rc.m_srods.begin();

	ConState& CSx = this->m_stateDEC_x->m_con;
	ConState& CS0 = this->m_stateDEC_0->m_con;

	int j = 0;

	// Update connection frames
	for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; ++s_it, ++j)
	{
		CSx.m_vrods[j].m_rodFc.tan = this->m_stateDEC_x->m_R0x * CS0.m_vrods[j].m_rodFr.tan;
		CSx.m_vrods[j].m_rodFc.nor = this->m_stateDEC_x->m_R0x * CS0.m_vrods[j].m_rodFr.nor;
		CSx.m_vrods[j].m_rodFc.bin = CSx.m_vrods[j].m_rodFc.tan.cross(CSx.m_vrods[j].m_rodFc.nor);
	}
}