/*=====================================================================================*/
/*!
/file		NodalSolidModel.h
/author		jesusprod
/brief		Basic abstract implementation of SolidModel to inherit from.
			All nodal solid models should inherit from this abstract class. 
			It includes functions for nodes access and management.
*/
/*=====================================================================================*/

#ifndef NODAL_SOLID_MODEL_H
#define NODAL_SOLID_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>
#include <JSandbox/Model/SolidModel.h>

class   NodalSolidModel : public SolidModel
{
public:
	NodalSolidModel(const string& ID) : SolidModel(ID) {}

	virtual ~NodalSolidModel() {}

	/* From Solid Model */

	virtual void add_vgravity(VectorXd& vg);
	virtual void add_fgravity(VectorXd& vf);

	virtual void addGravity(const VectorXd& vg, VectorXd& vf, const vector<bool>* pvFixed = NULL);

	virtual iVector getAABBIndices(const dVector& vaabb) const;

	/* From Solid Model */

	virtual int getNumSimDim_x() const { return this->m_numDim_x; }
	virtual int getNumSimDim_0() const { return this->m_numDim_0; }
	virtual int getNumRawDim() const { return this->m_numDimRaw; }

	virtual int getNumNodeSim() const { return this->m_numNodeSim; }
	virtual int getNumNodeRaw() const { return this->m_numNodeRaw; }

	virtual iVector getNodeRawDOF(int nodei) const;
	virtual iVector getNodeSimDOF_x(int nodei) const;
	virtual iVector getNodeSimDOF_0(int nodei) const;

protected:

	int m_numDim_x;
	int m_numDim_0;
	int m_numDimRaw;

	int m_numNodeSim;
	int m_numNodeRaw;
};

#endif