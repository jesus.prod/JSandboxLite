/*=====================================================================================*/
/*!
file		RodMeshModel.h
author		jesusprod
brief		RodMeshModel.h implementation.
*/
/*=====================================================================================*/

#ifndef ROD_MESH_MODEL_BASIC_H
#define ROD_MESH_MODEL_BASIC_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/RodMesh.h>

#include <JSandbox/Model/SolidModel.h>

#include <JSandbox/Element/DERTwistElement.h>
#include <JSandbox/Element/DERBendingElement.h>
#include <JSandbox/Element/DERStretchElement.h>
#include <JSandbox/Element/DERBendTwistElement.h>
#include <JSandbox/Element/DERConnectionElement.h>

class RMState : public SolidState
{
public:
	dVector m_va;			// For each edge
	dVector m_vt;			// For each node
	vector<Frame> m_vF;		// For each edge
	vector<ConState> m_vcon;	// For each connection
	vector<Matrix3d> m_vR0x;	// For each connection

	RMState()
	{
		// Nothing to do here...
	}

	virtual ~RMState()
	{
		// Nothing to do here...
	}

	RMState(const RMState& toCopy) : SolidState(toCopy)
	{
		this->m_va = toCopy.m_va;
		this->m_vt = toCopy.m_vt;
		this->m_vF = toCopy.m_vF;
		this->m_vR0x = toCopy.m_vR0x;
		this->m_vcon = toCopy.m_vcon;
	}

	virtual pSolidState clone() const
	{
		RMState* pcloned = new RMState();
		*pcloned = *this;

		return pSolidState(pcloned);
	}
};

typedef std::shared_ptr<RMState> pRMState;

class RodMeshBasicModel : public SolidModel
{
public:
	RodMeshBasicModel();

	virtual ~RodMeshBasicModel();

	/* From SolidModel */

	virtual void setup();

	virtual void setState_x(pSolidState s);
	virtual void setState_0(pSolidState s);
	virtual void setPositions_x(const VectorXd& vx);
	virtual void setPositions_0(const VectorXd& v0);

	void deprecateDiscretization();
	void deprecateDeformation();

	virtual void add_vgravity(VectorXd& vf);
	virtual void add_fgravity(VectorXd& vf);

	/* From SolidModel */

	virtual void configureMesh(const RodMesh& rod_x, const RodMesh& rod_0);

	virtual void update_SimMesh();

	virtual RodMesh& getSimMesh_x() { if (!this->m_isReady_Mesh) this->update_SimMesh(); return this->m_rodMesh_x; }
	virtual RodMesh& getSimMesh_0() { if (!this->m_isReady_Mesh) this->update_SimMesh(); return this->m_rodMesh_0; }

	virtual Rod& getSimRod_x(int i) { assert(i >= 0 && i < this->getNumRod()); return this->m_rodMesh_x.getRod(i); }
	virtual Rod& getSimRod_0(int i) { assert(i >= 0 && i < this->getNumRod()); return this->m_rodMesh_0.getRod(i); }

	virtual const Rod& getSimRod_x(int i) const { assert(i >= 0 && i < this->getNumRod()); return this->m_rodMesh_x.getRod(i); }
	virtual const Rod& getSimRod_0(int i) const { assert(i >= 0 && i < this->getNumRod()); return this->m_rodMesh_0.getRod(i); }

	//void setStateToMesh_x(const RodMesh& mesh);
	//void setStateToMesh_0(const RodMesh& mesh);

public:

	virtual int getNumRod() const { return (int) this->m_rodMesh_x.getNumRod(); }
	virtual int getNumCon() const { return (int) this->m_rodMesh_x.getNumCon(); }

	virtual int getNumNodeRaw() const;
	virtual int getNumEdgeRaw() const;

	virtual iVector getNodeRawDOF(int inode) const;
	virtual iVector getEdgeRawDOF(int iedge) const;
	//virtual iVector getNodeRawDOF(int irod, int inode) const;
	//virtual iVector getEdgeRawDOF(int irod, int iedge) const;

	virtual void update_Rest();

	virtual void update_Force();
	virtual void update_Energy();
	virtual void update_Jacobian();

	virtual void update_EnergyForce();
	virtual void update_ForceJacobian();
	virtual void update_EnergyForceJacobian();

	// BASIC

	virtual void getParam_RestLengthRodSplit(VectorXd& vl) const;
	virtual void setParam_RestLengthRodSplit(const VectorXd& vl);
	
	virtual void getParam_ConnectionStretchK(VectorXd& vk) const;
	virtual void setParam_ConnectionStretchK(const VectorXd& vk);

	virtual void getParam_ConnectionBendingK(VectorXd& vk) const;
	virtual void setParam_ConnectionBendingK(const VectorXd& vk);

	virtual void getParam_ConnectionTwistK(VectorXd& vk) const;
	virtual void setParam_ConnectionTwistK(const VectorXd& vk);

	virtual void getParam_StretchK(VectorXd& vk) const;
	virtual void setParam_StretchK(const VectorXd& vk);

	virtual void getParam_RestTangents(vector<vector<Vector3d>>& vtangents) const;
	virtual void setParam_RestTangents(const vector<vector<Vector3d>>& vtangents);

	//virtual void getParam_RestLengthEdge(VectorXd& vl) const;
	//virtual void setParam_RestLengthEdge(const VectorXd& vl);

	//virtual void getParam_RestLengthRod(VectorXd& vl) const;
	//virtual void setParam_RestLengthRod(const VectorXd& vl);

	// BASIC

	virtual void update_DfxDp();

	virtual int getNumNonZeros_DfxDp() { if (this->m_nNZ_DfxDp == -1) precompute_DfxDp(); return this->m_nNZ_DfxDp; }

	virtual bool isReady_DfxDp() const;

	virtual void add_DfxDp(tVector& vJ);

	// ELEMENTS

	virtual int getNumStretchEle() const { return (int) this->m_vpStretchEles.size(); }
	virtual int getNumBendTwistEle() const { return (int) this->m_vpBendTwistEles.size(); }
	virtual int getNumConnectionEle() const { return (int) this->m_vpConnectionEles.size(); }

protected:

	// Conns stuff

	virtual void updateConnectionsToState();
	virtual void updateStateToConnections();
	virtual void resetConnectionsBasicRotations();
	virtual void resetConnectionsEulerRotation();
	virtual void resetConnectionsRefFrames();
	virtual void resetConnectionsRotFrames();

	// Rods stuff

	virtual void updateMetadata_x();
	virtual void updateMetadata_0();
	virtual void updateRodCenter(const VectorXd& vx, RodMesh& mesh);
	virtual void updateRodAngles(const VectorXd& vx, dVector& va);
	virtual void updateRodRefFrame(const RodMesh& mesh, vector<Frame3d>& vref, dVector& vt);
	virtual void updateRodMatFrame(const dVector& va, const vector<Frame3d>& vref, RodMesh& mesh);

	// Derivatives

	virtual void precompute_DfxDp();

	virtual void initializeMaterials();

	RodMesh m_rodMesh_x;
	RodMesh m_rodMesh_0;

private:

	bool m_isReady_DfxDp;

	int m_nNZ_DfxDp;

	int m_numPoint;
	int m_numNodes;
	int m_numEdges;
	int m_numCons;
	int m_numRods;

	iVector m_vnodeRaw2Sim;
	
	vector<vector<DERConnectionElement*>> m_vpRodConnectionEles;
	vector<vector<DERBendTwistElement*>> m_vpRodBendTwistEles;
	vector<vector<DERStretchElement*>> m_vpRodStretchEles;

	pRMState m_stateRM_x;
	pRMState m_stateRM_0;

	vector<DERConnectionElement*> m_vpConnectionEles;
	vector<DERBendTwistElement*> m_vpBendTwistEles;
	vector<DERStretchElement*> m_vpStretchEles;

	vector<SolidMaterial> m_vedgeMats;
	vector<SolidMaterial> m_vnodeMats;
	vector<SolidMaterial> m_vconnMats;

};

#endif
