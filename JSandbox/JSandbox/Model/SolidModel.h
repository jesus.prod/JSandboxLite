/*=====================================================================================*/
/*!
/file		SolidModel.h
/author		jesusprod
/brief		Base abstract class for all solid models.
*/
/*=====================================================================================*/

#ifndef SOLID_MODEL_H
#define SOLID_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/CustomTimer.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>
#include <JSandbox/Element/SolidElement.h>

#include <set>

#include <memory>

class SolidState;

typedef std::shared_ptr<SolidState> pSolidState;

class   SolidState
{
public:
	VectorXd m_vx;
	VectorXd m_vv;

	SolidState()
	{
		// Nothing to do here...
	}

	virtual ~SolidState()
	{
		// Nothing to do here...
	}

	SolidState(const SolidState& toCopy)
	{
		this->m_vx = toCopy.m_vx;
		this->m_vv = toCopy.m_vv;
	}

	virtual pSolidState clone() const
	{
		SolidState* pcloned = new SolidState();
		*pcloned = *this;
		
		return pSolidState(pcloned);
	}

	virtual bool operator==(const SolidState& other) const
	{
		bool same = true;
		same &= (this->m_vx - other.m_vx).isZero(1e-6);
		same &= (this->m_vv - other.m_vv).isZero(1e-6);
		return same;
	}
};

class   SolidModel
{
public:
	SolidModel(const string& ID);

	virtual ~SolidModel();

	////////////////////////////////////////////////////////////////////////////
	// Discretization management

	//! Returns the number of DOF in configuration space
	virtual int getNumRawDOF() const { return this->m_nrawDOF; }

	//! Returns the number of deformed DOF in simulation space
	virtual int getNumSimDOF_x() const { return this->m_nsimDOF_x; }

	//! Returns the number of rest state DOF in simulation space
	virtual int getNumSimDOF_0() const { return this->m_nsimDOF_0; }

	//! Returns the DOF mapping from configuration to simulation spaces (deformed)
	virtual const iVector& getRawToSim_x() const { return this->m_vraw2sim_x; }

	//! Returns the DOF mapping from configuration to simulations spaces (rest)
	virtual const iVector& getRawToSim_0() const { return this->m_vraw2sim_0; }

	//! Returns a sparse matrix mapping from configuration to simulation space (deformed)
	virtual void getRawToSim_x(MatrixSd& ms2r) const;

	//! Returns a sparse matrix mapping from configuration to simulation space (rest)
	virtual void getRawToSim_0(MatrixSd& ms2r) const;

	virtual void mapSim2Raw(const iVector& vin, iVector& vout) const;
	virtual void mapRaw2Sim(const iVector& vin, iVector& vout) const;
	virtual void mapSim2Raw(const VectorXd& vin, VectorXd& vout) const;
	virtual void mapRaw2Sim(const VectorXd& vin, VectorXd& vout) const;

	virtual int getNumEle() const { return (int) this->m_vpEles.size(); }
	virtual SolidElement* getElement(int i) { assert(i >= 0 && i < this->getNumEle()); return this->m_vpEles[i]; }
	virtual const SolidElement* getElement(int i) const { assert(i >= 0 && i < this->getNumEle()); return this->m_vpEles[i]; }

	virtual void update_SimDOF();

	////////////////////////////////////////////////////////////////////////////
	// Setup

	virtual void setup() = 0;

	virtual void configureMaterial(const SolidMaterial& mat);

	////////////////////////////////////////////////////////////////////////////
	// State management

	virtual void setState_x(pSolidState s);
	virtual void setState_0(pSolidState s);
	virtual void setPositions_x(const VectorXd& vx);
	virtual void setPositions_0(const VectorXd& v0);
	virtual void setVelocities_x(const VectorXd& vv);

	virtual pSolidState getState_x() const;
	virtual pSolidState getState_0() const;
	virtual const VectorXd& getPositions_x() const { return m_state_x->m_vx; }
	virtual const VectorXd& getVelocities_x() const { return m_state_x->m_vv; }
	virtual const VectorXd& getPositions_0() const { return m_state_0->m_vx; }

	virtual void getRawPositions_x(VectorXd& vxRaw) const { this->mapSim2Raw(m_state_x->m_vx, vxRaw); }
	virtual void getRawVelocities_x(VectorXd& vvRaw) const { this->mapSim2Raw(m_state_x->m_vv, vvRaw); }

	////////////////////////////////////////////////////////////////////////////
	// Gravity related 

	virtual void setGravity(const VectorXd& vg) { this->m_vg = vg; }
	virtual const VectorXd getGravity() const { return this->m_vg; }

	virtual void add_vgravity(VectorXd& vg) { /* Does nothing by default */ };
	virtual void add_fgravity(VectorXd& vf) { /* Does nothing by default */ };

	////////////////////////////////////////////////////////////////////////////
	// Parameterization

	virtual void setParam_Rest(const VectorXd& vX);
	virtual const VectorXd& getParam_Rest() const;

	virtual void setParam_Material(const SolidMaterial& mat);
	virtual const SolidMaterial& getParam_Material() const;

	////////////////////////////////////////////////////////////////////////////
	// Computed data updaters

	virtual void update_Rest();
	virtual void update_Mass();

	virtual void update_Energy();
	virtual void update_Force();
	virtual void update_Jacobian();
	virtual void update_EnergyForce();
	virtual void update_ForceJacobian();
	virtual void update_EnergyForceJacobian();

	virtual void update_fx();
	virtual void update_f0();
	virtual void update_DfxDx();
	virtual void update_DfxD0();

	////////////////////////////////////////////////////////////////////////////
	// Model state control

	virtual void deprecateConfiguration();
	virtual void deprecateDiscretization();
	virtual void deprecateUndeformed();
	virtual void deprecateDeformation();

	////////////////////////////////////////////////////////////////////////////
	// Computed data checkers

	virtual bool isReady_Setup() const { return this->m_isReady_Setup; }

	virtual bool isReady_Mesh() const { return this->m_isReady_Mesh; }

	virtual bool isReady_Rest() const { return this->m_isReady_Rest; }
	virtual bool isReady_Mass() const { return this->m_isReady_Mass; }

	virtual bool isReady_Force() const { return this->m_isReady_Force; }
	virtual bool isReady_Energy() const { return this->m_isReady_Energy; }
	virtual bool isReady_Jacobian() const { return this->m_isReady_Jacobian; }

	virtual bool isReady_fx() const { return this->m_isReady_fx; }
	virtual bool isReady_f0() const { return this->m_isReady_f0; }
	virtual bool isReady_DfxDx() const { return this->m_isReady_DfxDx; }
	virtual bool isReady_DfxD0() const { return this->m_isReady_DfxD0; }

	virtual void setIsReady_Rest(bool ready) { this->m_isReady_Rest = ready; }
	virtual void setIsReady_Mass(bool ready) { this->m_isReady_Mass = ready; }

	////////////////////////////////////////////////////////////////////////////
	// Computed data getters

	virtual Real getEnergy();
	virtual void addForce(VectorXd& vf, const vector<bool>* pvFixed = NULL);
	virtual void addJacobian(tVector& vJ, const vector<bool>* pvFixed = NULL);

	virtual const VectorXd& get_Mass() const;
	virtual void set_Mass(const VectorXd& vm);

	virtual void add_Mass(VectorXd& vM);
	virtual void add_Mass(tVector& mM);

	virtual void add_fx(VectorXd& vfx);
	virtual void add_f0(VectorXd& vf0);
	virtual void add_DfxDx(tVector& vJ, const vector<bool>* pvFixed = NULL);
	virtual void add_DfxD0(tVector& vJ, const vector<bool>* pvFixed = NULL);

	virtual int getNumNonZeros_MassM() { if (this->m_nNZ_MassM == -1) this->precompute_MassM();  return this->m_nNZ_MassM; }
	virtual int getNumNonZeros_DfxDx() { if (this->m_nNZ_DfxDx == -1) this->precompute_DfxDx();  return this->m_nNZ_DfxDx; }
	virtual int getNumNonZeros_DfxD0() { if (this->m_nNZ_DfxD0 == -1) this->precompute_DfxD0();  return this->m_nNZ_DfxD0; }

	virtual int getNumNonZeros_Jacobian() { if (this->m_nNZ_Jacobian == -1) this->precompute_Jacobian();  return this->m_nNZ_Jacobian; }

	virtual void addGravity(const VectorXd& vg, VectorXd& vf, const vector<bool>* pvFixed = NULL) { /* Does nothing by default */ };

	virtual SolidMaterial* getMaterial() { return &this->m_material; }

	////////////////////////////////////////////////////////////////////////////
	// Computed data testers

	virtual void testForceLocal();
	virtual void testForceGlobal();
	virtual void testJacobianLocal();
	virtual void testJacobianGlobal();

	virtual void testDfxDxLocal();
	virtual void testDfxD0Local();
	virtual void testDfxDxGlobal();
	virtual void testDfxD0Global();

	////////////////////////////////////////////////////////////////////////////
	// Other

	virtual iVector getAABBIndices(const dVector& vaabb) const { /* Does nothing by default */ return iVector(); };

	////////////////////////////////////////////////////////////////////////////
	// Discretization changes

	virtual void onDiscretizationChanged(const iVector& vnewIdxRaw);

	//__event	void discretizationChanged(SolidModel* pthis, const iVector& vnewIdxRaw);

	virtual const set<ModelPoint>& getDiscretizationConstraints() const { return this->m_sdcs; };
	virtual void addDiscretizationConstraint(const ModelPoint& mp) { assert(this->isValidPoint(mp)); this->m_sdcs.insert(mp); };
	virtual void remDiscretizationConstraint(const ModelPoint& mp) { assert(this->isValidPoint(mp)); this->m_sdcs.erase(mp); };
	virtual void clearDiscretizationConstraints() { this->m_sdcs.clear(); };

	virtual bool isValidPoint(const ModelPoint& mp) const { return true; };

	pSolidState getStatePointer_x() { return this->m_state_x; }
	pSolidState getStatePointer_0() { return this->m_state_0; }

protected:

	virtual void updateSparseMatrixToLumpedMass();

	virtual void initializeMaterials();
	virtual void precompute_Jacobian();
	virtual void precompute_MassM();
	virtual void precompute_DfxDx();
	virtual void precompute_DfxD0();

	virtual void freeElements();

protected:

	set<ModelPoint> m_sdcs;

	VectorXd m_vg;

	int m_nrawDOF;
	int m_nsimDOF_x;
	int m_nsimDOF_0;

	iVector m_vraw2sim_x;
	iVector m_vraw2sim_0;

	pSolidState m_state_x;
	pSolidState m_state_0;

	VectorXd m_vMass;
	tVector m_mMass;

	tVector m_vDmDx;
	tVector m_vDmD0;

	int m_nNZ_MassM;
	int m_nNZ_DfxDx;
	int m_nNZ_DfxD0;

	int m_nNZ_Jacobian;

	bool m_isReady_Setup;

	bool m_isReady_Mesh;

	bool m_isReady_Rest;
	bool m_isReady_Mass;

	bool m_isReady_Force;
	bool m_isReady_Energy;
	bool m_isReady_Jacobian;

	bool m_isReady_fx;
	bool m_isReady_f0;
	bool m_isReady_DfxDx;
	bool m_isReady_DfxD0;

	vector<SolidElement*> m_vpEles;

	SolidMaterial m_dummyMat;
	SolidMaterial m_material;

	CustomTimer m_updateForTimer;
	CustomTimer m_updateJacTimer;
	CustomTimer m_assembleForTimer;
	CustomTimer m_assembleJacTimer;

	string m_ID;

};

#endif