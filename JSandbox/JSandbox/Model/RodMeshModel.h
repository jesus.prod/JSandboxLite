/*=====================================================================================*/
/*!
/file		RodMeshModel.h
/author		jesusprod
/brief		RodMeshModel.h implementation.
*/
/*=====================================================================================*/

#ifndef ROD_MESH_MODEL_2_H
#define ROD_MESH_MODEL_2_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/RodMesh.h>

#include <JSandbox/Model/DERModel.h>
#include <JSandbox/Model/DECModel.h>
#include <JSandbox/Model/CompositeModel.h>

class DECModel;
class DERModel;

class   RodMeshModel : public CompositeModel
{
public:
	RodMeshModel();

	virtual ~RodMeshModel();

	/* From SolidModel */

	virtual void setup();

	void deprecateDiscretization();

	/* From SolidModel */

	virtual void configureMesh(const RodMesh& rod_x, const RodMesh& rod_0);

	virtual void update_SimMesh();

	virtual RodMesh& getSimMesh_x() { if (!this->m_isReady_Mesh) this->update_SimMesh(); return this->m_rodMesh_x; }
	virtual RodMesh& getSimMesh_0() { if (!this->m_isReady_Mesh) this->update_SimMesh(); return this->m_rodMesh_0; }

	virtual Rod& getSimRod_x(int i) { assert(i >= 0 && i < this->getNumRod()); return this->m_rodMesh_x.getRod(i); }
	virtual Rod& getSimRod_0(int i) { assert(i >= 0 && i < this->getNumRod()); return this->m_rodMesh_0.getRod(i); }

	virtual const Rod& getSimRod_x(int i) const { assert(i >= 0 && i < this->getNumRod()); return this->m_rodMesh_x.getRod(i); }
	virtual const Rod& getSimRod_0(int i) const { assert(i >= 0 && i < this->getNumRod()); return this->m_rodMesh_0.getRod(i); }

	void setStateToMesh_x(const RodMesh& mesh);
	void setStateToMesh_0(const RodMesh& mesh);

	void remeshUniform(const pVector& vnodesExt, iVector& vnewIdxExt, int numV, Real maxL);

	virtual void setRemeshTolerance(Real tol) { this->m_remeshTol = tol; }
	virtual Real getRemeshTolerance() const { return this->m_remeshTol; }

	virtual DERModel* getRodModel(int i);
	virtual DECModel* getConModel(int i);

public:

	virtual int getNumRod() const { return (int) this->m_rodMesh_x.getNumRod(); }
	virtual int getNumCon() const { return (int) this->m_rodMesh_x.getNumCon(); }

	virtual int getNumNodeRaw() const;
	virtual int getNumEdgeRaw() const;

	virtual iVector getNodeRawDOF(int inode) const;
	virtual iVector getEdgeRawDOF(int iedge) const;
	virtual iVector getNodeRawDOF(int irod, int inode) const;
	virtual iVector getEdgeRawDOF(int irod, int iedge) const;


	// BASIC

	virtual void getParam_StretchK(VectorXd& vl) const;
	virtual void setParam_StretchK(const VectorXd& vl);

	virtual void getParam_RestLengthEdge(VectorXd& vl) const;
	virtual void setParam_RestLengthEdge(const VectorXd& vl);

	virtual void getParam_RestLengthRod(VectorXd& vl) const;
	virtual void setParam_RestLengthRod(const VectorXd& vl);

	// BASIC

	virtual void getParam_VertexRest(VectorXd& v0v) const;
	virtual void setParam_VertexRest(const VectorXd& v0v);

	virtual void getParam_RadiusEdge(VectorXd& vr) const;
	virtual void setParam_RadiusEdge(const VectorXd& vr);

	virtual void getParam_RadiusRod(VectorXd& vr) const;
	virtual void setParam_RadiusRod(const VectorXd& vr);

	virtual int getNumNonZeros_DfxD0v() { if (this->m_nNZ_DfxD0v == -1) precompute_DfxD0v(); return this->m_nNZ_DfxD0v; }
	virtual int getNumNonZeros_DfxDlr() { if (this->m_nNZ_DfxDlr == -1) precompute_DfxDlr(); return this->m_nNZ_DfxDlr; }
	virtual int getNumNonZeros_DfxDle() { if (this->m_nNZ_DfxDle == -1) precompute_DfxDle(); return this->m_nNZ_DfxDle; }
	virtual int getNumNonZeros_DfxDre() { if (this->m_nNZ_DfxDre == -1) precompute_DfxDre(); return this->m_nNZ_DfxDre; }
	virtual int getNumNonZeros_DfxDrr() { if (this->m_nNZ_DfxDrr == -1) precompute_DfxDrr(); return this->m_nNZ_DfxDrr; }

	virtual bool isReady_f0v() const;
	virtual bool isReady_fle() const;
	virtual bool isReady_flr() const;
	virtual bool isReady_fre() const;
	virtual bool isReady_frr() const;

	virtual bool isReady_DfxD0v() const;
	virtual bool isReady_DfxDle() const;
	virtual bool isReady_DfxDlr() const;
	virtual bool isReady_DfxDre() const;
	virtual bool isReady_DfxDrr() const;

	virtual void add_fv0(VectorXd& vf0);
	virtual void add_fle(VectorXd& vfl);
	virtual void add_flr(VectorXd& vfl);
	virtual void add_fre(VectorXd& vfr);
	virtual void add_frr(VectorXd& vfr);

	virtual void add_DfxD0v(tVector& vJ);
	virtual void add_DfxDle(tVector& vJ);
	virtual void add_DfxDlr(tVector& vJ);
	virtual void add_DfxDre(tVector& vJ);
	virtual void add_DfxDrr(tVector& vJ);

	virtual void compute_NodeStretchStrain(VectorXd& vs);
	virtual void compute_NodeExtensionStrain(VectorXd& vs);
	virtual void compute_NodeCompressionStrain(VectorXd& vs);

	void onDiscretizationChanged(const iVector& vnewIdxRaw);

	//__event	void discretizationChanged(SolidModel* pthis, const iVector& vnewIdxRaw);

protected:

	virtual void precompute_DfxD0v();
	virtual void precompute_DfxDle();
	virtual void precompute_DfxDlr();
	virtual void precompute_DfxDre();
	virtual void precompute_DfxDrr();

	virtual void initializeMaterials();

	virtual void freeElements();

	RodMesh m_rodMesh_x;
	RodMesh m_rodMesh_0;

private:

	// Should not be used, use configureMesh instead to initialize the model (this composite is clearly defined)
	virtual void configureModels(const vector<SolidModel*>& vpModels, const vector<ModelCoupling*>& vpCouplings)
	{
		assert(false);
	} 

	int m_nNZ_DfxD0v;
	int m_nNZ_DfxDle;
	int m_nNZ_DfxDlr;
	int m_nNZ_DfxDre;
	int m_nNZ_DfxDrr;

	Real m_remeshTol;

	vector<DERModel*> m_vprodModel;
	vector<DECModel*> m_vpconModel;

	vector<iVector> m_mapEdgeDOFCon2Rod;
	vector<iVector> m_mapNodeDOFCon2Rod;

};

#endif