/*=====================================================================================*/
/*!
/file		ParameterSet_Rest.h
/author		jesusprod
/brief		Generic parameter set for rest-state coordinates.
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_REST_H
#define PARAMETER_SET_REST_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

class   ParameterSet_Rest : public ParameterSet
{
public:
	ParameterSet_Rest();

	virtual ~ParameterSet_Rest();

	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

};

#endif