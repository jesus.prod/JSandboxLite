/*=====================================================================================*/
/*!
/file		ParameterSet.h
/author		jesusprod
/brief		Base abstract class for optimization parameters.
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_H
#define PARAMETER_SET_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Solver/PhysSolver.h>
#include <JSandbox/Model/SolidModel.h>

class ModelParameterization;

#include <JSandbox/Model/ModelParameterization.h>

class   ParameterSet
{
public:
	ParameterSet(const string& ID);

	virtual ~ParameterSet();

	virtual void setModel(SolidModel* pSM);
	virtual SolidModel* getModel() const;

	virtual void setParam(ModelParameterization* pMP);
	virtual ModelParameterization* getParam() const;

	virtual const string& getID() const;

	virtual int getNumParameter() const;

	virtual Real getMaximumValue() const;
	virtual Real getMinimumValue() const;
	virtual void setMaximumValue(Real M);
	virtual void setMinimumValue(Real m);
	
	virtual const VectorXd& getMinimumValues() const { return this->m_vminValues; }
	virtual const VectorXd& getMaximumValues() const { return this->m_vmaxValues; }

	virtual bool getIsBoxed() const;
	virtual void setIsBoxed(bool B);

	virtual const iVector& getFixedParameters() const;
	virtual void setFixedParameters(const iVector& vidx);

	virtual void getParameters(VectorXd& vp) const = 0;
	virtual void setParameters(const VectorXd& vp) = 0;

	virtual bool checkParameters(const VectorXd& vp) const;
	virtual void limitParameters(VectorXd& vp) const;

	virtual Real computeParameterRegularizer_Potential(const VectorXd& vp) { return 0; }
	virtual void computeParameterRegularizer_Hessian(const VectorXd& vp, tVector& vH) { vH.resize(0); }
	virtual void computeParameterRegularizer_Gradient(const VectorXd& vp, VectorXd& vg) { vg.resize(this->m_np); vg.setZero();}

	virtual void setup() = 0;
	virtual bool isReady_fp() const = 0;
	virtual bool isReady_DfDp() const = 0;
	virtual void get_fp(VectorXd& vfp) const = 0;
	virtual void get_DfDp(tVector& vDfDp) const = 0;

	virtual void test_DfDp();

	virtual void updateBoundsVector();

protected:

	string m_ID;

	int m_np;

	iVector m_vfixIdx;

	Real m_maxValue;
	Real m_minValue;
	bool m_isBoxed;

	VectorXd m_vmaxValues;
	VectorXd m_vminValues;
	
	SolidModel* m_pModel;

	ModelParameterization* m_pParam;

};

#endif