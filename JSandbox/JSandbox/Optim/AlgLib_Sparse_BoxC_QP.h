/*=====================================================================================*/
/*!
/file		AlgLib_Sparse_BoxC_QP.h
/author		jesusprod
/brief		Wrapper for AlgLib QuickQP solver for box-constrained QP problems.
*/
/*=====================================================================================*/

#ifndef ALGLIB_SPARSE_BOXC_QP_H
#define ALGLIB_SPARSE_BOXC_QP_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

// AlgLib includes

#include <JSandbox/AlgLib/stdafx.h>
#include <JSandbox/AlgLib/optimization.h>

using namespace alglib;

class   AlgLib_Sparse_BoxC_QP
{
public:
	AlgLib_Sparse_BoxC_QP() 
	{
		// Nothing to do here...
	}

	virtual ~AlgLib_Sparse_BoxC_QP()
	{
		// Nothing to do here...
	}

	/*! This method solves problems of the kind:
	*	min q(x) = 0.5*x^T*G*x + x^T*c + D
	*		s.t. l <= x
	*			 x <= u
	*/
	virtual bool solve(const tVector& vG,		 // The quadratic coeffients matrix
					   const VectorXd& vc,		 // The linear coefficients vector
					   const VectorXd& vx,		 // The initial solution vector
					   const VectorXd& vl,		 // The lower bounds: -Inf if not bounded
					   const VectorXd& vu,		 // The uppder bounds: Inf if not bounded
					   VectorXd& vxo,			 // The solution of the QP problem
					   string& error);

};

#endif