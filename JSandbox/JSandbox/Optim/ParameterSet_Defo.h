/*=====================================================================================*/
/*!
/file		ParameterSet_Defo.h
/author		jesusprod
/brief		Generic parameter set for material coordinates.
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_DEFO_H
#define PARAMETER_SET_DEFO_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

class   ParameterSet_Defo : public ParameterSet
{
public:
	ParameterSet_Defo();

	virtual ~ParameterSet_Defo();

	virtual void setSolver(PhysSolver* pS);
	virtual PhysSolver* getSolver() const;

	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

protected:

	PhysSolver* m_pSolver;
};

#endif