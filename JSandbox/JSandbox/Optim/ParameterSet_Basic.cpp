/*=====================================================================================*/
/*!
/file		ParameterSet_Basic.cpp
/author		jesusprod
/brief		Implementation of ParameterSet_Basic.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Basic.h>

#include <JSandbox/Model/RodMeshBasicModel.h>

ParameterSet_Basic::ParameterSet_Basic() : ParameterSet("BASICMAT")
{
	// Nothing to do here
}

ParameterSet_Basic::~ParameterSet_Basic()
{
	// Nothing to do here
}

void ParameterSet_Basic::setSolver(PhysSolver* pS)
{
	assert(pS != NULL);
	this->m_pSolver = pS;
}

PhysSolver* ParameterSet_Basic::getSolver() const
{
	return this->m_pSolver;
}

void ParameterSet_Basic::getParameters(VectorXd& vp) const
{
	RodMeshBasicModel* pBModel = static_cast<RodMeshBasicModel*>(this->m_pModel);

	// Get parameters vectors

	VectorXd vks;
	pBModel->getParam_ConnectionStretchK(vks);
	int nps = vks.size();
	VectorXd vkb;
	pBModel->getParam_ConnectionBendingK(vkb);
	int npb = vkb.size();
	VectorXd vkt;
	pBModel->getParam_ConnectionTwistK(vkt);
	int npt = vkt.size();

	// Fill concatenated paramters

	vp.resize(this->m_np);

	int numCon = pBModel->getNumCon();

	int countPar = 0;
	for (int i = 0; i < numCon; ++i)
	{
		vp(countPar + 0) = vks(i);
		vp(countPar + 1) = vkb(2 * i + 0);
		vp(countPar + 2) = vkb(2 * i + 1);
		vp(countPar + 3) = vkt(i);
		countPar += 4;
	}
}

void ParameterSet_Basic::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	RodMeshBasicModel* pBModel = static_cast<RodMeshBasicModel*>(this->m_pModel);

	// Get parameter vector sizes

	VectorXd vks;
	pBModel->getParam_ConnectionStretchK(vks);
	int nps = vks.size();
	VectorXd vkb;
	pBModel->getParam_ConnectionBendingK(vkb);
	int npb = vkb.size();
	VectorXd vkt;
	pBModel->getParam_ConnectionTwistK(vkt);
	int npt = vkt.size();

	// Fill parameter vectors

	int numCon = pBModel->getNumCon();

	int countPar = 0;
	for (int i = 0; i < numCon; ++i)
	{
		vks(i) = vp(countPar + 0);
		vkb(2 * i + 0) = vp(countPar + 1);
		vkb(2 * i + 1) = vp(countPar + 2);
		vkt(i) = vp(countPar + 3);
		countPar += 4;
	}

	// Set parameters

	pBModel->setParam_ConnectionStretchK(vks);
	pBModel->setParam_ConnectionBendingK(vkb);
	pBModel->setParam_ConnectionTwistK(vkt);
}

bool ParameterSet_Basic::isReady_fp() const
{
	// Nothing to do here...

	assert(false);

	return false;
}

bool ParameterSet_Basic::isReady_DfDp() const
{
	RodMeshBasicModel* pBModel = static_cast<RodMeshBasicModel*>(this->m_pModel);
	return pBModel->isReady_DfxDp();
}

void ParameterSet_Basic::get_fp(VectorXd& vfp) const
{
	// Nothing to do here...

	assert(false);
}

void ParameterSet_Basic::get_DfDp(tVector& vDfDp) const
{
	assert(this->m_pModel != NULL); // Initialized model

	RodMeshBasicModel* pBModel = static_cast<RodMeshBasicModel*>(this->m_pModel);

	vDfDp.reserve(pBModel->getNumNonZeros_DfxDp());
	pBModel->add_DfxDp(vDfDp); // Jacobian w.r.t par
}

void ParameterSet_Basic::setup()
{
	assert(this->m_pModel != NULL); // Initialized model

	RodMeshBasicModel* pBModel = static_cast<RodMeshBasicModel*>(this->m_pModel);

	// Get parameter vector sizes

	VectorXd vks;
	pBModel->getParam_ConnectionStretchK(vks);
	int nps = vks.size();
	VectorXd vkb;
	pBModel->getParam_ConnectionBendingK(vkb);
	int npb = vkb.size();
	VectorXd vkt;
	pBModel->getParam_ConnectionTwistK(vkt);
	int npt = vkt.size();

	// Initialize total parameter size

	this->m_np = nps + npb + npt;

	// Intialize parameter bounds vector

	this->updateBoundsVector();
}

void ParameterSet_Basic::updateBoundsVector()
{
	RodMeshBasicModel* pBModel = static_cast<RodMeshBasicModel*>(this->m_pModel);

	int numCon = pBModel->getNumCon();

	if (vpMin.size() != 0 && vpMax.size() != 0)
	{
		this->m_vminValues.resize(this->m_np);
		this->m_vmaxValues.resize(this->m_np);
		for (int i = 0; i < numCon; ++i)
		{
			this->m_vminValues.block(4 * i, 0, 4, 1) = vpMin;
			this->m_vmaxValues.block(4 * i, 0, 4, 1) = vpMax;
		}
	}
	else
	{
		this->m_vminValues.resize(this->m_np);
		this->m_vmaxValues.resize(this->m_np);
		this->m_vminValues.setConstant(this->m_minValue);
		this->m_vmaxValues.setConstant(this->m_maxValue);
	}
}
