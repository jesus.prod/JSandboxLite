/*=====================================================================================*/
/*!
/file		ParameterSet_Rest.cpp
/author		jesusprod
/brief		Implementation of ParameterSet_Rest.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Rest.h>

ParameterSet_Rest::ParameterSet_Rest() : ParameterSet("RESCOORDS")
{
	// Nothing to do here
}

ParameterSet_Rest::~ParameterSet_Rest()
{
	// Nothing to do here
}

void ParameterSet_Rest::getParameters(VectorXd& vp) const
{
	//vp = this->m_pModel->getPositions_0();

	assert((int)vp.size() == this->m_np);
}

void ParameterSet_Rest::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	//const VectorXd& vv = this->m_pModel->getVelocities_0();
	//this->m_pModel->setParam_Rest(vp, vv); // Update state
}

bool ParameterSet_Rest::isReady_fp() const
{
	return this->m_pModel->isReady_f0();
}

bool ParameterSet_Rest::isReady_DfDp() const
{
	return this->m_pModel->isReady_DfxD0();
}

void ParameterSet_Rest::get_fp(VectorXd& vfp) const
{
	assert(this->m_pModel != NULL); // Initialized model

	vfp.resize(this->m_np);
	vfp.setZero(); // Init.

	this->m_pModel->add_f0(vfp);
}

void ParameterSet_Rest::get_DfDp(tVector& vDFDp) const
{
	assert(this->m_pModel != NULL); // Initialized model

	vDFDp.reserve(this->m_pModel->getNumNonZeros_DfxD0());
	this->m_pModel->add_DfxD0(vDFDp); // Forces Jacobian
}

void ParameterSet_Rest::setup()
{
	assert(this->m_pModel != NULL); // Initialized
	this->m_np = this->m_pModel->getNumSimDOF_0();

	this->updateBoundsVector();
}