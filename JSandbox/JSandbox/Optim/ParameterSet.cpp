/*=====================================================================================*/
/*!
/file		ParameterSet.cpp
/author		jesusprod
/brief		Implementation of ParameterSet.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet.h>

ParameterSet::ParameterSet(const string& ID)
{
	this->m_ID = ID;
	this->m_isBoxed = false;
	this->m_minValue = -HUGE_VAL;
	this->m_maxValue = HUGE_VAL;
}

ParameterSet::~ParameterSet()
{
	// Nothing to do here
}

void ParameterSet::setModel(SolidModel* pModel)
{
	assert(pModel != NULL);
	this->m_pModel = pModel;
}

SolidModel* ParameterSet::getModel() const
{
	return this->m_pModel;
}

void ParameterSet::setParam(ModelParameterization* pParam)
{
	assert(pParam != NULL);
	this->m_pParam = pParam;
}

ModelParameterization* ParameterSet::getParam() const
{
	return this->m_pParam;
}

const iVector& ParameterSet::getFixedParameters() const
{
	return this->m_vfixIdx;
}

void ParameterSet::setFixedParameters(const iVector& vidx)
{
	this->m_vfixIdx = vidx;
}

Real ParameterSet::getMaximumValue() const
{
	return this->m_maxValue;
}

Real ParameterSet::getMinimumValue() const
{
	return this->m_minValue;
}

void ParameterSet::setMaximumValue(Real M)
{
	this->m_maxValue = M;
}

void ParameterSet::setMinimumValue(Real m)
{
	this->m_minValue = m;
}

bool ParameterSet::getIsBoxed() const
{
	return this->m_isBoxed;
}

void ParameterSet::setIsBoxed(bool B)
{
	this->m_isBoxed = B;
}

const string& ParameterSet::getID() const
{
	return this->m_ID;
}

int ParameterSet::getNumParameter() const
{
	return this->m_np;
}

bool ParameterSet::checkParameters(const VectorXd& vp) const
{
	if (this->m_isBoxed)
	{
		for (int i = 0; i < this->m_np; ++i)
		{
			if (vp(i) < this->m_vminValues(i) && !isApprox(vp(i) - this->m_vminValues(i), 0.0, EPS_POS)) 
				return false;
			if (vp(i) > this->m_vmaxValues(i) && !isApprox(vp(i) - this->m_vmaxValues(i), 0.0, EPS_POS)) 
				return false;
		}
	}

	return true;
}

void ParameterSet::limitParameters(VectorXd& vp) const
{
	if (this->m_isBoxed)
	{
		for (int i = 0; i < this->m_np; ++i)
		{
			if (vp(i) < this->m_vminValues(i)) 
				vp(i) = this->m_vminValues(i);
			if (vp(i) > this->m_vmaxValues(i)) 
				vp(i) = this->m_vmaxValues(i);
		}
	}
}

void ParameterSet::updateBoundsVector()
{
	this->m_vminValues.resize(this->m_np);
	this->m_vmaxValues.resize(this->m_np);
	this->m_vminValues.setConstant(this->m_minValue);
	this->m_vmaxValues.setConstant(this->m_maxValue);
}

void ParameterSet::test_DfDp()
{
	logSimu("[INFO] Testing parameter: %s", this->m_ID.c_str());

	VectorXd vpFD;
	this->getParameters(vpFD);
	VectorXd vpInitial = vpFD;

	bool boxed = this->m_isBoxed;

	this->m_isBoxed = false;

	Real EPS = 1e-6;

	pSolidState pStateIni = this->m_pModel->getState_x()->clone();

	int nsimDOF = this->m_pModel->getNumSimDOF_x();

	MatrixXd mJFD(nsimDOF, this->m_np);
	mJFD.setZero(); // Init. from zero

	for (int j = 0; j < this->m_np; ++j)
	{
		this->m_pModel->setState_x(pStateIni);

		// +
		vpFD(j) += EPS;
		VectorXd vfp(nsimDOF);
		vfp.setZero();
		this->setParameters(vpFD);
		this->m_pModel->addForce(vfp);

		this->m_pModel->setState_x(pStateIni);

		// -
		vpFD(j) -= 2 * EPS;
		VectorXd vfm(nsimDOF);
		vfm.setZero();
		this->setParameters(vpFD);
		this->m_pModel->addForce(vfm);

		// Estimate
		VectorXd vfFD = (vfp - vfm) / (2 * EPS);
		for (int i = 0; i < nsimDOF; ++i)
			mJFD(i, j) = vfFD(i);

		vpFD(j) += EPS;
	}

	// Get analytic
	this->setParameters(vpInitial);

	// Recover previous state

	this->m_pModel->setState_x(pStateIni);

	tVector vJA;
	this->get_DfDp(vJA);
	MatrixSd mJASparse(nsimDOF, this->m_np);
	mJASparse.setFromTriplets(vJA.begin(), vJA.end());

	MatrixXd mJA = MatrixXd(mJASparse);

	// Compare
	MatrixXd mJDiff = (mJA - mJFD);
	Real FDNorm = mJFD.norm();
	Real diffNorm = mJDiff.norm() / FDNorm;

	if (FDNorm < 1e-9)
		diffNorm = 0.0;

	logSimu("[INFO] Model: %s. DfDp test error (global): %.9f", this->m_ID.c_str(), diffNorm);

	if (diffNorm > 1e-9)
	{
		//writeToFile(mJA, "DfDpA.csv"); // Trace
		//writeToFile(mJFD, "DfDpFD.csv"); // Trace
		//writeToFile(mJDiff, "DfDpFDError.csv"); // Trace
	}

	this->m_isBoxed = boxed;

	logSimu("[INFO] Finalized test: %s", this->m_ID.c_str());
}


