/*=====================================================================================*/
/*!
/file		AlgLib_Sparse_BoxC_QP.cpp
/author		jesusprod
/brief		Implementation of AlgLib_Sparse_BoxC_QP.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/AlgLib_Sparse_BoxC_QP.h>



bool AlgLib_Sparse_BoxC_QP::solve(const tVector& vG,		 // The quadratic coeffients matrix
								  const VectorXd& vc,		 // The linear coefficients vector
								  const VectorXd& vx,		 // The initial solution vector
								  const VectorXd& vl,		 // The lower bounds: -Inf if not bounded
								  const VectorXd& vu,		 // The uppder bounds: Inf if not bounded
								  VectorXd& vxo,			 // The solution of the QP problem
								  string& error)
{
	int N = (int)vc.size();

	real_1d_array vxoAL;

	// Create quadratic coefficients matrix

	int nC = (int) vG.size();
	sparsematrix mGAL;
	sparsecreate(N, N, nC, mGAL);
	for (int i = 0; i < nC; ++i)
		sparseset(mGAL, vG[i].row(), vG[i].col(), vG[i].value());

	// Create linear coefficients vector

	real_1d_array vcAL;
	vcAL.setlength(N);
	vcAL.setcontent(N, vc.data());

	// Create initial solution vector

	real_1d_array vxAL;
	vxAL.setlength(N);
	vxAL.setcontent(N, vx.data());

	// Create scale and bounds vectors
	real_1d_array vsAL;
	real_1d_array vlbAL;
	real_1d_array vubAL;
	vsAL.setlength(N);
	vlbAL.setlength(N);
	vubAL.setlength(N);
	for (int i = 0; i < N; ++i)
	{
		// Equal scale
		vsAL[i] = 1.0;

		// Lower bound
		if (vl(i) == -HUGE_VAL)
			vlbAL[i] = alglib::fp_neginf;
		else vlbAL[i] = vl(i); // Bounded

		// Upper bound
		if (vu(i) == HUGE_VAL)
			vubAL[i] = alglib::fp_posinf;
		else vubAL[i] = vu(i); // Bounded
	}

	minqpstate state;
	minqpreport repo;

	// Create and setup
	minqpcreate(N, state);
	minqpsetlinearterm(state, vcAL);
	minqpsetquadratictermsparse(state, mGAL, false);
	minqpsetstartingpoint(state, vxAL);
	minqpsetbc(state, vlbAL, vubAL);

	// Select the algorithm used, in this case QuickQP
	minqpsetalgoquickqp(state, 1e-6, 1e-6, 1e-6, 100, true);

	// Optimize problem!
	minqpoptimize(state);

	// Get the optimization results
	minqpresults(state, vxoAL, repo);

	ostringstream errorstr;

	bool solved = false;

	switch (repo.c_ptr()->terminationtype)
	{
		// Solved
	case 1: errorstr << "[SUCCESS] Relative improvement is no more than EpsF " << state.c_ptr()->qqpsettingscurrent.epsf << "\n"; solved = true; break;
	case 2: errorstr << "[SUCCESS] Scaled step magnitude is no more than EpsX " << state.c_ptr()->qqpsettingscurrent.epsx << "\n"; solved = true; break;
	case 4: errorstr << "[SUCCESS] Scaled gradient norm is no more than EpsG " << state.c_ptr()->qqpsettingscurrent.epsg << "\n"; solved = true; break;

		// Iterations
	case 5: errorstr << "[INFO] Maximum iterations was taken " << state.c_ptr()->qpbleicsettingscurrent.maxits << "\n"; solved = true; break;

	case 7: errorstr << "[INFO] Stopping conditions are too stringent"; solved = true; break;

		// Error
	case -5: errorstr << "[ERROR] Inappropriate solver was selected\n"; break;
	case -4: errorstr << "[ERROR] Solver found unconstrained direction of negative curvature\n"; break;
	case -3: errorstr << "[ERROR] Inconsistent constraints of feasible point too hard to find\n"; break;
	}

	errorstr << "/tInner iterations: " << repo.inneriterationscount << "\n";
	errorstr << "/tOuter iterations: " << repo.outeriterationscount << "\n";
	errorstr << "/tCholesky decompositions: " << repo.ncholesky << "\n";
	errorstr << "/tMatrix-vector products: " << repo.nmv << "\n";

	if (solved)
	{
		for (int i = 0; i < N; ++i)
			vxo(i) = vxoAL[i]; // Copy
	}

	error = errorstr.str();

	return solved;
}