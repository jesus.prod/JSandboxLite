/*=====================================================================================*/
/*!
/file		ParameterSet_Defo.cpp
/author		jesusprod
/brief		Implementation of ParameterSet_Defo.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Defo.h>

ParameterSet_Defo::ParameterSet_Defo() : ParameterSet("MATCOORDS")
{
	// Nothing to do here
}

ParameterSet_Defo::~ParameterSet_Defo()
{
	// Nothing to do here
}

void ParameterSet_Defo::setSolver(PhysSolver* pS)
{
	assert(pS != NULL);
	this->m_pSolver = pS;
}

PhysSolver* ParameterSet_Defo::getSolver() const
{
	return this->m_pSolver;
}

void ParameterSet_Defo::getParameters(VectorXd& vp) const
{
	vp = this->m_pModel->getPositions_x();
	assert((int)vp.size() == this->m_np);
}

void ParameterSet_Defo::setParameters(const VectorXd& vp)
{
	assert((int) vp.size() == this->m_np);
	this->m_pModel->setPositions_x(vp);
}

bool ParameterSet_Defo::isReady_fp() const
{
	return this->m_pModel->isReady_Force();
}

bool ParameterSet_Defo::isReady_DfDp() const
{
	return this->m_pModel->isReady_DfxDx();
}

void ParameterSet_Defo::get_fp(VectorXd& vfp) const
{
	assert(this->m_pModel != NULL);

	vfp.resize(this->m_np);
	vfp.setZero(); // Init.

	this->m_pModel->addForce(vfp);
	const VectorXd& vx = this->m_pModel->getPositions_x();
	const VectorXd& vv = this->m_pModel->getVelocities_x();
	this->m_pSolver->addBoundaryForce(vx, vv, vfp);

	this->m_pSolver->constrainBoundary(vfp);
}

void ParameterSet_Defo::get_DfDp(tVector& vDfDp) const
{
	assert(this->m_pModel != NULL); // Initialized model

	//this->m_pModel->testDfxDxLocal();
	//this->m_pModel->testDfxDxGlobal();

	vDfDp.reserve(this->m_pModel->getNumNonZeros_DfxDx());
	this->m_pModel->add_DfxDx(vDfDp); // Forces Jacobian

	const VectorXd& vx = this->m_pModel->getPositions_x();
	const VectorXd& vv = this->m_pModel->getVelocities_x();
	this->m_pSolver->addBoundaryJacobian(vx, vv, vDfDp);

	this->m_pSolver->constrainBoundary(vDfDp);
}

void ParameterSet_Defo::setup()
{
	assert(this->m_pModel != NULL); // Initialized
	this->m_np = this->m_pModel->getNumSimDOF_x();

	this->updateBoundsVector();
}