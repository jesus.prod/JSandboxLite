/*=====================================================================================*/
/*!
/file		AlgLib_Sparse_EQIN_QP.cpp
/author		jesusprod
/brief		Implementation of AlgLib_Sparse_EQIN_QP.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/AlgLib_Sparse_EQIN_QP.h>



bool AlgLib_Sparse_EQIN_QP::solve(const tVector& vG,		 // The quadratic coeffients matrix
								  const VectorXd& vc,		 // The linear coefficients vector
								  const MatrixXd& mA,		 // The constraints coefficients matrix
								  const VectorXd& vb,		 // The constraints independent vector
								  const iVector& vCT,		 // The type of constraints { -, +, 0}
								  const VectorXd& vx,		 // The initial solution vector
								  const VectorXd& vl,		 // The lower bounds: -Inf if not bounded
								  const VectorXd& vu,		 // The uppder bounds: Inf if not bounded
								  VectorXd& vxo,			 // The solution of the QP problem
								  string& error)
{
	int Nx = (int)vc.size();
	int Nc = (int)vb.size();

	real_1d_array vxoAL;

	// Create quadratic coefficients matrix

	int nC = (int) vG.size();
	sparsematrix mGAL;
	sparsecreate(Nx, Nx, nC, mGAL);
	for (int i = 0; i < nC; ++i)
		sparseset(mGAL, vG[i].row(), vG[i].col(), vG[i].value());

	// Create linear coefficients vector

	real_1d_array vcAL;
	vcAL.setlength(Nx);
	vcAL.setcontent(Nx, vc.data());

	// Add linearized constraints

	integer_1d_array vctAL;
	vctAL.setlength(Nc);

	real_2d_array vlcAL;
	vlcAL.setlength(Nc, Nx + 1);
	for (int i = 0; i < Nc; ++i)
	{
		// Set Jacobian coefficients
		for (int j = 0; j < Nx; ++j)
			vlcAL[i][j] = mA(i, j);

		// Set right-hand-side
		vlcAL[i][Nx] = vb(i);

		// Contraint type
		vctAL[i] = vCT[i];
	}

	// Create initial solution vector

	real_1d_array vxAL;
	vxAL.setlength(Nx);
	vxAL.setcontent(Nx, vx.data());

	// Create scale and bounds vectors

	real_1d_array vsAL;
	real_1d_array vlbAL;
	real_1d_array vubAL;
	vsAL.setlength(Nx);
	vlbAL.setlength(Nx);
	vubAL.setlength(Nx);
	for (int i = 0; i < Nx; ++i)
	{
		// Equal scale
		vsAL[i] = 1.0;

		// Lower bound
		if (vl(i) == -HUGE_VAL)
			vlbAL[i] = alglib::fp_neginf;
		else vlbAL[i] = vl(i); // Bounded

		// Upper bound
		if (vu(i) == HUGE_VAL)
			vubAL[i] = alglib::fp_posinf;
		else vubAL[i] = vu(i); // Bounded
	}

	minqpstate state;
	minqpreport repo;

	// Create and setup
	minqpcreate(Nx, state);
	minqpsetlinearterm(state, vcAL);
	minqpsetquadratictermsparse(state, mGAL, false);
	minqpsetbc(state, vlbAL, vubAL);
	minqpsetlc(state, vlcAL, vctAL);
	minqpsetstartingpoint(state, vxAL);

	// Select the algorithm used, in this case BLEIC
	minqpsetalgobleic(state, 1e-12, 1e-12, 1e-12, 200);

	// Optimize problem!
	minqpoptimize(state);

	// Get the optimization results
	minqpresults(state, vxoAL, repo);

	ostringstream errorstr;

	bool solved = false;

	switch (repo.c_ptr()->terminationtype)
	{
		// Solved
	case 1: errorstr << "[SUCCESS] Relative improvement is no more than EpsF " << state.c_ptr()->qpbleicsettingscurrent.epsf << "\n"; solved = true; break;
	case 2: errorstr << "[SUCCESS] Scaled step magnitude is no more than EpsX " << state.c_ptr()->qpbleicsettingscurrent.epsx << "\n"; solved = true; break;
	case 4: errorstr << "[SUCCESS] Scaled gradient norm is no more than EpsG " << state.c_ptr()->qpbleicsettingscurrent.epsg << "\n"; solved = true; break;

		// Iterations
	case 5: errorstr << "[INFO] Maximum iterations was taken " << state.c_ptr()->qpbleicsettingscurrent.maxits << "\n"; solved = true; break;

	case 7: errorstr << "[INFO] Stopping conditions are too stringent"; solved = true; break;

		// Error
	case -5: errorstr << "[ERROR] Inappropriate solver was selected\n"; break;
	case -4: errorstr << "[ERROR] Solver found unconstrained direction of negative curvature\n"; break;
	case -3: errorstr << "[ERROR] Inconsistent constraints of feasible point too hard to find\n"; break;
	}

	errorstr << "/tInner iterations: " << repo.inneriterationscount << "\n";
	errorstr << "/tOuter iterations: " << repo.outeriterationscount << "\n";
	errorstr << "/tCholesky decompositions: " << repo.ncholesky << "\n";
	errorstr << "/tMatrix-vector products: " << repo.nmv << "\n";

	if (solved)
	{
		for (int i = 0; i < Nx; ++i)
			vxo(i) = vxoAL[i]; // Copy
	}

	error = errorstr.str();

	return solved;
}