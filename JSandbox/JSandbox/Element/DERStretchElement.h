/*=====================================================================================*/
/*!
/file		DERStretchElement.h
/author		jesusprod
/brief		Implementation of DERStretchElement.h.
*/
/*=====================================================================================*/

#ifndef DER_STRETCH_ELEMENT_H
#define DER_STRETCH_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/DERBaseElement.h>

class DERStretchElement : public DERBaseElement
{
	enum Regime
	{
		Regular,
		FreeExt,
		FreeCom,
	};

public:
	DERStretchElement();
	~DERStretchElement();

	virtual void update_Rest(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Mass(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Force(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Energy(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Jacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	void update_fv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
				   const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0);
	void update_fl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	void update_fr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	void update_DfxDv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
					  const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0);
	void update_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	void update_DfxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	void update_DfxDp(const dVector& vx);

	virtual void add_DfxDp(tVector& vDfxDp) const;

	virtual const MatrixXd& getLocal_DfxDp() const { return this->m_mDfxDp; }

	virtual Real compute_strain(const dVector& vx, const dVector& vX);

	virtual void update_Rest(const VectorXd& vX);
	virtual void update_Mass(const VectorXd& vX);
	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual void setIntegrationVolume(Real iv) { this->m_iv = iv; }
	virtual Real getIntegrationVolume() const { return this->m_iv; }

	virtual void update_DmxDv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
							  const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0);
	virtual void update_DmxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_DmxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual void add_DmxDv(tVector& vDmxDv) const;
	virtual void add_DmxDl(tVector& vDmxDl) const;
	virtual void add_DmxDr(tVector& vDmxDr) const;

	virtual Real computeK() const;

	virtual Real testForceLocal(const VectorXd& vx, const VectorXd& vv);
	virtual Real testJacobianLocal(const VectorXd& vx, const VectorXd& vv);

protected:

	Real m_iv;

	MatrixXd m_mDmxDv;
	MatrixXd m_mDmxDl;
	MatrixXd m_mDmxDr;

	MatrixXd m_mDexDx;
	MatrixXd m_mDevDv;

	MatrixXd m_mDfxDp;

	Regime m_regime;

};

#endif