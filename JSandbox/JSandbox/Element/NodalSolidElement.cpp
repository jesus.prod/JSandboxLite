/*=====================================================================================*/
/*!
/file		NodalSolidElement.cpp
/author		jesusprod
/brief		Implementation of NodalSolidElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/NodalSolidElement.h>

const iVector& NodalSolidElement::getNodeIndicesx() const
{
	return this->m_vnodeIdxx;
}

void NodalSolidElement::setNodeIndicesx(const iVector& vidx)
{
	assert((int)vidx.size() == this->m_numNode);

	int count = 0;
	this->m_vnodeIdxx = vidx;
	this->m_vidxx.resize(this->m_numDOFx);
	for (int i = 0; i < this->m_numNode; ++i)
	{
		int offset = this->m_numDim_x*vidx[i];
		for (int d = 0; d < this->m_numDim_x; ++d)
			this->m_vidxx[count++] = offset + d;
	}
}

const iVector& NodalSolidElement::getNodeIndices0() const
{
	return this->m_vnodeIdx0;
}

void NodalSolidElement::setNodeIndices0(const iVector& vidx)
{
	assert((int)vidx.size() == this->m_numNode);

	int count = 0;
	this->m_vnodeIdx0 = vidx;
	this->m_vidx0.resize(this->m_numDOF0);
	for (int i = 0; i < this->m_numNode; ++i)
	{
		int offset = this->m_numDim_0*vidx[i];
		for (int d = 0; d < this->m_numDim_0; ++d)
			this->m_vidx0[count++] = offset + d;
	}
}