/*=====================================================================================*/
/*!
/file		NodalSolidElement.h
/author		jesusprod
/brief		Basic abstract implementation of SolidElement to inherit from.
			All (purely) nodal solid elements should inherit from this abstract
			class. It holds information about number of nodes and dimension n.
*/
/*=====================================================================================*/

#ifndef NODAL_SOLID_ELEMENT_H
#define NODAL_SOLID_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>

#include <JSandbox/Element/SolidElement.h>

class NodalSolidElement : public SolidElement
{
public:
	NodalSolidElement(int n, int dx, int d0) : SolidElement(n*dx, n*d0)
	{
		assert(n > 1);
		assert(dx > 0);
		assert(d0 > 0);

		this->m_numDim_x = dx;
		this->m_numDim_0 = d0;
		this->m_numNode = n;

		this->m_vnodeIdx0.resize(n);
		this->m_vnodeIdxx.resize(n);
	}

	virtual ~NodalSolidElement() { }

	virtual int getNumNodeSim() const { return m_numNode; }

	virtual int getNumSimDim_x() const { return m_numDim_x; }
	virtual int getNumSimDim_0() const { return m_numDim_0; }

	// Wrapper for making easier to use it, nodal indices 
	// should be specified and not the Degrees-of-Freedom

	virtual const iVector& getNodeIndicesx() const;
	virtual void setNodeIndicesx(const iVector& vi);

	virtual const iVector& getNodeIndices0() const;
	virtual void setNodeIndices0(const iVector& vi);

protected:

	int m_numNode;

	int m_numDim_x;
	int m_numDim_0;

	iVector m_vnodeIdxx;
	iVector m_vnodeIdx0;

};

#endif