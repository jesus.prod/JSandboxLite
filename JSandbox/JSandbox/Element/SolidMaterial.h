/*=====================================================================================*/
/*! 
/file		SolidMaterial.h
/author		jesusprod
/brief		Common class for several parametrizations of a solid homogeneous material. 
			Instances of this class should be created through the appropriate static 
			functions to ensure coherence
 */
/*=====================================================================================*/

#ifndef SOLID_MATERIAL_H
#define SOLID_MATERIAL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

class   SolidMaterial
{
public: 

	SolidMaterial()
	{
		this->m_thickness = -1;
		this->m_rH = -1;
		this->m_rW = -1;
		this->m_density = -1;
		this->m_young = -1;
		this->m_lame1 = -1;
		this->m_lame2 = -1;
		this->m_poisson = -1;
		this->m_bulk = -1;
		this->m_stretchK = -1;
		this->m_bendingKw = -1;
		this->m_bendingKh = -1;
		this->m_areaK = -1;
		this->m_twistK = -1;
	}

	virtual Real getThickness() const { return this->m_thickness; }
	virtual void setThickness(Real t) { this->m_thickness = t; }

	virtual Real getHRadius() const { return this->m_rH; }
	virtual void setHRadius(Real rH) { this->m_rH = rH; }

	virtual Real getWRadius() const { return this->m_rW; }
	virtual void setWRadius(Real rW) { this->m_rW = rW; }

	virtual Real getDensity() const { return this->m_density; }
	virtual void setDensity(Real p) { this->m_density = p; }

	virtual Real getYoung() const { return this->m_young; }
	virtual void setYoung(Real E) { this->m_young = E; }

	virtual Real getShear() const { return this->m_lame2; }
	virtual void setShear(Real G) { this->m_lame2 = G; }

	virtual Real getPoisson() const { return this->m_poisson; }
	virtual void setPoisson(Real v) { this->m_poisson = v; }

	virtual Real getBulk() const { return this->m_bulk; }
	virtual void setBulk(Real k) { this->m_bulk = k; }

	virtual Real getLameFirst() const { return this->m_lame1; }
	virtual void setLameFirst(Real l) { this->m_lame1 = l; }

	virtual Real getLameSecond() const { return this->m_lame2; }
	virtual void setLameSecond(Real G) { this->m_lame2 = G; }

	virtual Real getStretchK() const { return this->m_stretchK; }
	virtual void setStretchK(Real sK) { this->m_stretchK = sK; }

	virtual Real getBendingKh() const { return this->m_bendingKh; }
	virtual void setBendingKh(Real bK) { this->m_bendingKh = bK; }

	virtual Real getBendingKw() const { return this->m_bendingKw; }
	virtual void setBendingKw(Real bK) { this->m_bendingKw = bK; }

	virtual Real getAreaK() const { return this->m_areaK; }
	virtual void setAreaK(Real aK) { this->m_areaK = aK; }

	virtual Real getTwistK() const { return this->m_twistK; }
	virtual void setTwistK(Real tK) { this->m_twistK = tK; }

	virtual void initFromYoungPoisson(Real E, Real p, Real density);
	virtual void initFromLameCoefficients(Real l, Real g, Real density);

protected:

	Real m_rH;
	Real m_rW;

	Real m_thickness;

	Real m_density;				// Density

	// Coninuum 
	Real m_poisson;				// Poisson ratio
	Real m_young;				// Elastic modulus
	Real m_bulk;				// Bulk modulus
	Real m_lame1;			// Lame first parameter
	Real m_lame2;			// Lame second (shear modulus)

	// Other
	Real m_stretchK;		
	Real m_bendingKw;
	Real m_bendingKh;
	Real m_areaK;
	Real m_twistK;		

};

#endif