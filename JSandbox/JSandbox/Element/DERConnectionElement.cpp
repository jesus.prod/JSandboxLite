/*=====================================================================================*/
/*!
/file		DERConnectionElement.cpp
/author		jesusprod
/brief		Implementation of DERConnectionElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/DERConnectionElement.h>

// Support operations
#include <JSandbox/Maple/eulerRotation.h>
#include <JSandbox/Maple/parallelTransport.h>
#include <JSandbox/Maple/parallelTransportNormalized.h>
#include <JSandbox/Maple/computeReferenceTwistChange.h>

// Energy, gradient, Hessian
#include <JSandbox/Maple/getRBConEnergyOPT.h>
#include <JSandbox/Maple/getRBConEnergyTwistOPT.h>
#include <JSandbox/Maple/getRBConEnergyBendingOPT.h>
#include <JSandbox/Maple/getRBConEnergyOPTMetaTest.h>
#include <JSandbox/Maple/getRBConGradientOPT.h>
#include <JSandbox/Maple/getRBConGradientEnergyTwistOPT.h>
#include <JSandbox/Maple/getRBConGradientEnergyBendingOPT.h>
#include <JSandbox/Maple/getRBConGradientOPTMetaTest.h>
#include <JSandbox/Maple/getRBConHessianOPT.h>
#include <JSandbox/Maple/getRigidBodyEnergyGradienti3D.h>
#include <JSandbox/Maple/getRigidBodyEnergyHessiani3D.h>
#include <JSandbox/Maple/getRigidBodyEnergyi3D.h>
#include <JSandbox/Maple/getRBParamRestNodesJacobianOPT.h>
#include <JSandbox/Maple/getRBParamAniRadiusJacobianOPT.h>
#include <JSandbox/Maple/getRBParamIsoRadiusJacobianOPT.h>
#include <JSandbox/Maple/getRBParamLengthJacobianOPT.h>

#include <JSandbox/Maple/getRBConEnergyOPTBasic.h>
#include <JSandbox/Maple/getRBConGradientOPTBasic.h>
#include <JSandbox/Maple/getRBConHessianOPTBasic.h>
#include <JSandbox/Maple/getRBConJacobianParOPTBasic.h>

DERConnectionElement::DERConnectionElement() : SolidElement(10, 10)
{
	this->m_vpmat.resize(1);

	this->m_hl0 = 0.0;

	this->m_ridx = -1;

	this->m_vfr.resize(1);
	this->m_vfl.resize(1);
	this->m_vfr.setZero();
	this->m_vfl.setZero();
	this->m_mDfxDr.resize(10, 1);
	this->m_mDfxDl.resize(10, 1);
}

DERConnectionElement::~DERConnectionElement(void)
{
	// Nothing to do here...
}

void DERConnectionElement::update_Rest(const ConState& CS)
{
	this->m_hl0 = 0.5*(CS.m_vrods[this->m_ridx].m_vr - CS.m_vc).norm();
}

void DERConnectionElement::update_Energy(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	//assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	MatrixXd mDexDx(7, 10);
	MatrixXd mDevDv(3, 6);

	mDexDx.setZero();
	mDevDv.setZero();

	mDexDx(3, 6) = 1.0;
	mDexDx(4, 7) = 1.0;
	mDexDx(5, 8) = 1.0;
	mDexDx(6, 9) = 1.0;

	Matrix3d I;
	I.setIdentity();
	I *= (CS.m_vrods[this->m_ridx].m_S > 0) ? 1 : -1;

	addBlock3x3From(0, 0, I * -1.0, mDexDx);
	addBlock3x3From(0, 3, I, mDexDx);

	addBlock3x3From(0, 0, I * -1.0, mDevDv);
	addBlock3x3From(0, 3, I, mDevDv);

	this->m_mDexDx = mDexDx.sparseView(EPS_POS);
	this->m_mDevDv = mDevDv.sparseView(EPS_POS);

	this->m_energy = getRBConEnergyOPTBasic(
		this->m_vpmat[0]->getTwistK(),
		this->m_vpmat[0]->getBendingKw(),
		this->m_vpmat[0]->getBendingKh(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vR.data(),
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda);

	//if (m_energy > 1e-6)
	//{
	//	logSimu("Bending energy not zero: %f", this->m_energy);
	//}
}

void DERConnectionElement::update_Force(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	//assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	MatrixXd mDexDx(7, 10);
	MatrixXd mDevDv(3, 6);

	mDexDx.setZero();
	mDevDv.setZero();

	mDexDx(3, 6) = 1.0;
	mDexDx(4, 7) = 1.0;
	mDexDx(5, 8) = 1.0;
	mDexDx(6, 9) = 1.0;

	Matrix3d I;
	I.setIdentity();
	I *= (CS.m_vrods[this->m_ridx].m_S > 0) ? 1 : -1;

	addBlock3x3From(0, 0, I * -1.0, mDexDx);
	addBlock3x3From(0, 3, I, mDexDx);

	addBlock3x3From(0, 0, I * -1.0, mDevDv);
	addBlock3x3From(0, 3, I, mDevDv);

	this->m_mDexDx = mDexDx.sparseView(EPS_POS);
	this->m_mDevDv = mDevDv.sparseView(EPS_POS);

	VectorXd vg(7);

	getRBConGradientOPTBasic(
		this->m_vpmat[0]->getTwistK(),
		this->m_vpmat[0]->getBendingKw(),
		this->m_vpmat[0]->getBendingKh(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vR.data(),
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda,
		vg.data());

	this->m_vfVal = -1*this->m_mDexDx.transpose()*vg;
}

void DERConnectionElement::update_Jacobian(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	//assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	MatrixXd mDexDx(7, 10);
	MatrixXd mDevDv(3, 6);

	mDexDx.setZero();
	mDevDv.setZero();

	mDexDx(3, 6) = 1.0;
	mDexDx(4, 7) = 1.0;
	mDexDx(5, 8) = 1.0;
	mDexDx(6, 9) = 1.0;

	Matrix3d I;
	I.setIdentity();
	I *= (CS.m_vrods[this->m_ridx].m_S > 0) ? 1 : -1;

	addBlock3x3From(0, 0, I * -1.0, mDexDx);
	addBlock3x3From(0, 3, I, mDexDx);

	addBlock3x3From(0, 0, I * -1.0, mDevDv);
	addBlock3x3From(0, 3, I, mDevDv);

	this->m_mDexDx = mDexDx.sparseView(EPS_POS);
	this->m_mDevDv = mDevDv.sparseView(EPS_POS);

	MatrixXd mJ(7, 7);

	getRBConHessianOPTBasic(
		this->m_vpmat[0]->getTwistK(),
		this->m_vpmat[0]->getBendingKw(),
		this->m_vpmat[0]->getBendingKh(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vR.data(),
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda,
		mJ.transpose().data());

	MatrixXd mJFull = -1*this->m_mDexDx.transpose()*mJ*this->m_mDexDx;

	int count = 0;
	for (int i = 0; i < 10; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJFull(i, j);
}

void DERConnectionElement::update_DfxDp(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	//assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	MatrixXd mDfexDp(3, 7);

	getRBConJacobianParOPTBasic(
		this->m_vpmat[0]->getTwistK(),
		this->m_vpmat[0]->getBendingKw(),
		this->m_vpmat[0]->getBendingKh(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vR.data(),
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda,
		mDfexDp.data());

	this->m_mDfxDp = -1.0*this->m_mDexDx.transpose()*mDfexDp.transpose();
}

void DERConnectionElement::update_DfxD0v(const ConState& CS0, const ConState& CSx, const Matrix3d& R0x)
{
	Vector3d ve0;
	if (CS0.m_vrods[this->m_ridx].m_S > 0)
		ve0 = CS0.m_vrods[this->m_ridx].m_vr - CS0.m_vc;
	else ve0 = CS0.m_vc - CS0.m_vrods[this->m_ridx].m_vr;

	assert(ve0.normalized().dot(CS0.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	//assert(ve0.normalized().dot(CS0.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	Vector3d vex;
	if (CSx.m_vrods[this->m_ridx].m_S > 0)
		vex = CSx.m_vrods[this->m_ridx].m_vr - CSx.m_vc;
	else vex = CSx.m_vc - CSx.m_vrods[this->m_ridx].m_vr;

	assert(vex.normalized().dot(CSx.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	//assert(vex.normalized().dot(CSx.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	MatrixXd mDfexDev(3,7);

	getRBParamRestNodesJacobianOPT(this->m_vpmat[0]->getYoung(),
								   this->m_vpmat[0]->getShear(),
								   this->m_vpmat[0]->getWRadius(),
								   this->m_vpmat[0]->getHRadius(),
								   CS0.m_vrods[this->m_ridx].m_rodFr.tan.data(),
								   CS0.m_vrods[this->m_ridx].m_rodFr.nor.data(),
								   CSx.m_vrods[this->m_ridx].m_rodFr.tan.data(),
								   CSx.m_vrods[this->m_ridx].m_rodFr.nor.data(),
								   R0x.transpose().data(),
								   CSx.m_vR.data(),
								   ve0.data(),
								   vex.data(),
								   CSx.m_vrods[this->m_ridx].m_rodt,
								   CSx.m_vrods[this->m_ridx].m_roda,
								   mDfexDev.data());

	this->m_mDfxD0v = -1*this->m_mDexDx.transpose()*mDfexDev.transpose()*this->m_mDevDv;
}

void DERConnectionElement::update_DfxDl(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	//assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	MatrixXd mDfexDl(1,7);

	getRBParamLengthJacobianOPT(this->m_vpmat[0]->getYoung(),
								this->m_vpmat[0]->getShear(),
								this->m_vpmat[0]->getWRadius(),
								this->m_vpmat[0]->getHRadius(),
								this->m_hl0,
								CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
								CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
								CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
								CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
								CS.m_vrods[this->m_ridx].m_rodt,
								CS.m_vR.data(),
								ve.data(),
								CS.m_vrods[this->m_ridx].m_roda,
								mDfexDl.data());

	this->m_mDfxDl = -0.5*this->m_mDexDx.transpose()*mDfexDl.transpose();
}

void DERConnectionElement::update_DfxDr(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	//assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	MatrixXd mDfexDr(2, 7);

	getRBParamAniRadiusJacobianOPT(this->m_vpmat[0]->getYoung(),
								   this->m_vpmat[0]->getShear(),
								   this->m_vpmat[0]->getWRadius(),
								   this->m_vpmat[0]->getHRadius(),
								   this->m_hl0,
								   CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
								   CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
								   CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
								   CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
								   CS.m_vrods[this->m_ridx].m_rodt,
								   CS.m_vR.data(),
								   ve.data(),
								   CS.m_vrods[this->m_ridx].m_roda,
								   mDfexDr.data());

	this->m_mDfxDr = -this->m_mDexDx.transpose()*mDfexDr.transpose();
}

void updateStateForTesting(int j, ConState& CSx)
{
	// Update con frame
	Vector3d matTanRBx;
	Vector3d matNorRBx;
	eulerRotation(CSx.m_vR.data(), CSx.m_vrods[j].m_rodFc.tan.data(), matTanRBx.data());
	eulerRotation(CSx.m_vR.data(), CSx.m_vrods[j].m_rodFc.nor.data(), matNorRBx.data());
	CSx.m_vrods[j].m_rodFc.tan = matTanRBx;
	CSx.m_vrods[j].m_rodFc.nor = matNorRBx;
	CSx.m_vrods[j].m_rodFc.bin = matTanRBx.cross(matNorRBx);;
	CSx.m_vR.setZero();

	// Update rod frame

	Vector3d matTanROx;
	if (CSx.m_vrods[j].m_S > 0)
		matTanROx = CSx.m_vrods[j].m_vr - CSx.m_vc;
	else matTanROx = CSx.m_vc - CSx.m_vrods[j].m_vr;
	matTanROx.normalize();

	Vector3d matNorROx;
	parallelTransport(CSx.m_vrods[j].m_rodFr.tan.data(), matTanROx.data(), CSx.m_vrods[j].m_rodFr.nor.data(), matNorROx.data());
	CSx.m_vrods[j].m_rodFr.tan = matTanROx;
	CSx.m_vrods[j].m_rodFr.nor = matNorROx;
	CSx.m_vrods[j].m_rodFr.bin = matTanROx.cross(matNorROx);

	// Update ref twist
	CSx.m_vrods[j].m_rodt += computeReferenceTwistChange(
		CSx.m_vrods[j].m_rodFc.tan.data(),
		CSx.m_vrods[j].m_rodFc.nor.data(),
		matTanROx.data(),
		matNorROx.data(),
		CSx.m_vrods[j].m_rodt);
}

void updateStateForTesting(int j, ConState& CS0, ConState& CSx, Matrix3d& R0x)
{
	// Update 0
	Vector3d matTanRB0 = (CS0.m_vrods[j].m_vr - CS0.m_vc).normalized();
	Vector3d matNorRB0;
	parallelTransportNormalized(CS0.m_vrods[j].m_rodFr.tan.data(), matTanRB0.data(), CS0.m_vrods[j].m_rodFr.nor.data(), matNorRB0.data());
	CS0.m_vrods[j].m_rodFr.tan = matTanRB0;
	CS0.m_vrods[j].m_rodFr.nor = matNorRB0;
	CS0.m_vrods[j].m_rodFr.bin = matTanRB0.cross(matNorRB0);

	// Update x
	Vector3d matTanROx = (CSx.m_vrods[j].m_vr - CSx.m_vc).normalized();
	Vector3d matNorROx;
	parallelTransportNormalized(CSx.m_vrods[j].m_rodFr.tan.data(), matTanROx.data(), CSx.m_vrods[j].m_rodFr.nor.data(), matNorROx.data());
	CSx.m_vrods[j].m_rodFr.tan = matTanROx;
	CSx.m_vrods[j].m_rodFr.nor = matNorROx;
	CSx.m_vrods[j].m_rodFr.bin = matTanROx.cross(matNorROx);

	// Update R
	double eulerAngles[3];
	eulerAngles[0] = CSx.m_vR.x();
	eulerAngles[1] = CSx.m_vR.y();
	eulerAngles[2] = CSx.m_vR.z();
	CSx.m_vR.setZero();

	Matrix3d newR;
	eulerRotation(eulerAngles, R0x.col(0).data(), newR.col(0).data());
	eulerRotation(eulerAngles, R0x.col(1).data(), newR.col(1).data());
	eulerRotation(eulerAngles, R0x.col(2).data(), newR.col(2).data());
	R0x = newR;

	CSx.m_vrods[j].m_rodFc.tan = R0x * CS0.m_vrods[j].m_rodFr.tan;
	CSx.m_vrods[j].m_rodFc.nor = R0x * CS0.m_vrods[j].m_rodFr.nor;
	CSx.m_vrods[j].m_rodFc.bin = R0x * CS0.m_vrods[j].m_rodFr.bin;

	// Update reference
	Vector3d matTanRBxRot;
	Vector3d matNorRBxRot;
	eulerRotation(CSx.m_vR.data(), CSx.m_vrods[j].m_rodFc.tan.data(), matTanRBxRot.data());
	eulerRotation(CSx.m_vR.data(), CSx.m_vrods[j].m_rodFc.nor.data(), matNorRBxRot.data());

	CSx.m_vrods[j].m_rodt += computeReferenceTwistChange(matTanRBxRot.data(),
														 matNorRBxRot.data(),
														 CSx.m_vrods[j].m_rodFr.tan.data(),
														 CSx.m_vrods[j].m_rodFr.nor.data(),
														 CSx.m_vrods[j].m_rodt);
}

void DERConnectionElement::testForceChanging(const ConState& CS)
{
	Real EPS = 1e-3;

	ConState csIni = CS;
	
	updateStateForTesting(0, csIni);
	updateStateForTesting(1, csIni);

	ConState csFD = csIni;

	// Con position
	for (int i = 0; i < 3; ++i)
	{
		// Yes Meta
		csFD = csIni;
		csFD.m_vc(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->udpate_Force_Test(csFD);
		VectorXd fYes = this->getLocalForce();

		// No Meta
		csFD = csIni;
		csFD.m_vc(i) += EPS;
		this->udpate_Force_Test(csFD);
		VectorXd fNo = this->getLocalForce();

		logSimu("[INFO] Force difference: %.12f", (fYes-fNo).norm());
	}

	// Rod position
	for (int i = 0; i < 3; ++i)
	{
		// Yes Meta
		csFD = csIni;
		csFD.m_vrods[this->m_ridx].m_vr(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->udpate_Force_Test(csFD);
		VectorXd fYes = this->getLocalForce();

		// No Meta
		csFD = csIni;
		csFD.m_vrods[this->m_ridx].m_vr(i) += EPS;
		this->udpate_Force_Test(csFD);
		VectorXd fNo = this->getLocalForce();

		logSimu("[INFO] Force difference: %.12f", (fYes - fNo).norm());
	}

	// Euler angles
	for (int i = 0; i < 3; ++i)
	{
		// Yes Meta
		csFD = csIni;
		csFD.m_vR(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->udpate_Force_Test(csFD);
		VectorXd fYes = this->getLocalForce();

		// Yes Meta
		csFD = csIni;
		csFD.m_vR(i) += EPS;
		this->udpate_Force_Test(csFD);
		VectorXd fNo = this->getLocalForce();

		logSimu("[INFO] Force difference: %.12f", (fYes - fNo).norm());
	}

	// Twist angle

	// Yes Meta
	csFD = csIni;
	csFD.m_vrods[this->m_ridx].m_roda += EPS;
	updateStateForTesting(this->m_ridx, csFD);
	this->udpate_Force_Test(csFD);
	VectorXd fYes = this->getLocalForce();

	// No Meta
	csFD = csIni;
	csFD.m_vrods[this->m_ridx].m_roda += EPS;
	this->udpate_Force_Test(csFD);
	VectorXd fNo = this->getLocalForce();

	logSimu("[INFO] Force difference: %.12f", (fYes - fNo).norm());
}

void DERConnectionElement::testEnergyChanging(const ConState& CS)
{
	Real EPS = 1e-3;

	ConState csIni = CS;

	updateStateForTesting(0, csIni);
	updateStateForTesting(1, csIni);

	ConState csFD = csIni;

	// Con position
	for (int i = 0; i < 3; ++i)
	{
		// Yes Meta
		csFD = csIni;
		csFD.m_vc(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy_Test(csFD);
		Real epYes = this->getEnergyIntegral();

		// No Meta
		csFD = csIni;
		csFD.m_vc(i) += EPS;
		this->update_Energy_Test(csFD);
		Real epNo = this->getEnergyIntegral();

		logSimu("[INFO] Energy difference: %.12f", epYes - epNo);
	}

	// Rod position
	for (int i = 0; i < 3; ++i)
	{
		// Yes Meta
		csFD = csIni;
		csFD.m_vrods[this->m_ridx].m_vr(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy_Test(csFD);
		Real epYes = this->getEnergyIntegral();

		// No Meta
		csFD = csIni;
		csFD.m_vrods[this->m_ridx].m_vr(i) += EPS;
		this->update_Energy_Test(csFD);
		Real epNo = this->getEnergyIntegral();

		logSimu("[INFO] Energy difference: %.12f", epYes - epNo);
	}

	// Euler angles
	for (int i = 0; i < 3; ++i)
	{
		// Yes Meta
		csFD = csIni;
		csFD.m_vR(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy_Test(csFD);
		Real epYes = this->getEnergyIntegral();

		// Yes Meta
		csFD = csIni;
		csFD.m_vR(i) += EPS;
		this->update_Energy_Test(csFD);
		Real epNo = this->getEnergyIntegral();

		logSimu("[INFO] Energy difference: %.12f", epYes - epNo);
	}

	// Twist angle

	// Yes Meta
	csFD = csIni;
	csFD.m_vrods[this->m_ridx].m_roda += EPS;
	updateStateForTesting(this->m_ridx, csFD);
	this->update_Energy_Test(csFD);
	Real epYes = this->getEnergyIntegral();

	// No Meta
	csFD = csIni;
	csFD.m_vrods[this->m_ridx].m_roda += EPS;
	this->update_Energy_Test(csFD);
	Real epNo = this->getEnergyIntegral();

	logSimu("[INFO] Energy difference: %.12f", epYes - epNo);
}

void DERConnectionElement::test_DfxDl(const ConState& CS)
{
	MatrixXd mA(this->m_numDOFx, 1);
	mA.setZero(); // Init. from zero

	MatrixXd mF(this->m_numDOFx, 1);
	mF.setZero(); // Init. from zero

	// Compute analitical
	this->update_DfxDl(CS);
	mA = getLocal_DfxDl();

	// Compute +
	this->m_hl0 += 1e-6;
	this->update_Force(CS);
	VectorXd vfp = this->getLocalForce();

	// Compute -
	this->m_hl0 -= 2e-6;
	this->update_Force(CS);
	VectorXd vfm = this->getLocalForce();

	// Recover
	this->m_hl0 += 1e-6;
	this->update_Force(CS);

	// Estimate
	VectorXd vfDiff = (vfp - vfm) / 2e-6;
	for (int i = 0; i < this->m_numDOFx; ++i)
		mF(i, 0) = 0.5*vfDiff(i); // Fill matrix
	
	// Compute difference

	Real FDNorm = mF.norm(); // Estimation norm
	Real diffNorm = (mA - mF).norm() / FDNorm;

	if (FDNorm > 0.0 && diffNorm > 1e-9)
	{
		logSimu("[ERROR] Local error computing DfxDl: %.9f", diffNorm);
		//writeToFile(mA, "LocalDfxDlA.csv");
		//writeToFile(mF, "LocalDfxDlF.csv");
	}
}

void DERConnectionElement::test_DfxDr(const ConState& CS)
{
	MatrixXd mA(this->m_numDOFx, 1);
	mA.setZero(); // Init. from zero

	MatrixXd mF(this->m_numDOFx, 1);
	mF.setZero(); // Init. from zero

	// Compute analitical
	this->update_DfxDr(CS);
	mA = getLocal_DfxDr();

	Real rw = this->m_vpmat[0]->getWRadius();
	Real rh = this->m_vpmat[0]->getHRadius();

	// Compute +
	this->m_vpmat[0]->setWRadius(rw + 1e-6);
	this->m_vpmat[0]->setHRadius(rh + 1e-6);
	this->update_Force(CS);
	VectorXd vfp = this->getLocalForce();

	// Compute -
	this->m_vpmat[0]->setWRadius(rw - 1e-6);
	this->m_vpmat[0]->setHRadius(rh - 1e-6);
	this->update_Force(CS);
	VectorXd vfm = this->getLocalForce();

	// Recover
	this->m_vpmat[0]->setWRadius(rw);
	this->m_vpmat[0]->setHRadius(rh);
	this->update_Force(CS);

	// Estimate
	VectorXd vfDiff = (vfp - vfm) / 2e-6;
	for (int i = 0; i < this->m_numDOFx; ++i)
		mF(i, 0) = vfDiff(i); // Fill matrix

	// Compute difference

	Real FDNorm = mF.norm(); // Estimation norm
	Real diffNorm = (mA - mF).norm() / FDNorm;

	if (FDNorm > 0.0 && diffNorm > 1e-9)
	{
		logSimu("[ERROR] Local error computing DfxDr: %.9f", diffNorm);
		//writeToFile(mA, "LocalDfxDrA.csv");
		//writeToFile(mF, "LocalDfxDrF.csv");
	}
}

void DERConnectionElement::test_DfxD0v(const ConState& CS0, const ConState& CSx, const Matrix3d& R0x)
{
	Real EPS = 1e-6;

	MatrixXd mA(this->m_numDOFx, 6);
	mA.setZero(); // Init. from zero

	this->update_DfxD0v(CS0, CSx, R0x);

	mA = this->getLocal_DfxD0v();

	ConState CS0_FD = CS0;
	ConState CSx_FD = CSx;
	Matrix3d R0x_FD = R0x;

	MatrixXd mF(this->m_numDOFx, 6);
	mF.setZero(); // Init. from zero

	int offset = 0;

	// Check w.r.t connection

	for (int i = 0; i < 3; ++i)
	{
		CS0_FD = CS0;
		CSx_FD = CSx;
		R0x_FD = R0x;
		CS0_FD.m_vc(i) += EPS;
		updateStateForTesting(this->m_ridx, CS0_FD, CSx_FD, R0x_FD);
		this->update_Force(CSx_FD);
		VectorXd vfp = this->getLocalForce();

		CS0_FD = CS0;
		CSx_FD = CSx;
		R0x_FD = R0x;
		CS0_FD.m_vc(i) -= EPS;
		updateStateForTesting(this->m_ridx, CS0_FD, CSx_FD, R0x_FD);
		this->update_Force(CSx_FD);
		VectorXd vfm = this->getLocalForce();

		VectorXd vfDiff = (vfp - vfm) / (2*EPS);
		for (int j = 0; j < this->m_numDOFx; ++j)
			mF(j, offset + i) = vfDiff(j);
	}

	offset = 3;

	// Check w.r.t rod point

	for (int i = 0; i < 3; ++i)
	{
		CS0_FD = CS0;
		CSx_FD = CSx;
		R0x_FD = R0x;
		CS0_FD.m_vrods[this->m_ridx].m_vr(i) += EPS;
		updateStateForTesting(this->m_ridx, CS0_FD, CSx_FD, R0x_FD);
		this->update_Force(CSx_FD);
		VectorXd vfp = this->getLocalForce();

		CS0_FD = CS0;
		CSx_FD = CSx;
		R0x_FD = R0x;
		CS0_FD.m_vrods[this->m_ridx].m_vr(i) -= EPS;
		updateStateForTesting(this->m_ridx, CS0_FD, CSx_FD, R0x_FD);
		this->update_Force(CSx_FD);
		VectorXd vfm = this->getLocalForce();

		VectorXd vfDiff = (vfp - vfm) / (2 * EPS);
		for (int j = 0; j < this->m_numDOFx; ++j)
			mF(j, offset + i) = vfDiff(j);
	}

	// Compute difference

	Real FDNorm = mF.norm(); // Estimation norm
	Real diffNorm = (mA - mF).norm() / FDNorm;

	this->update_Force(CSx);

	if (FDNorm > 0.0 && diffNorm > 1e-9)
	{
		logSimu("[ERROR] Local error computing DfxD0v: %.9f", diffNorm);
		//writeToFile(mA, "LocalDfxD0vA.csv");
		//writeToFile(mF, "LocalDfxD0vF.csv");
	}
}

void DERConnectionElement::add_DfxDp(tVector& vDfxDp) const
{
	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			vDfxDp.push_back(Triplet<Real>(this->m_vidxx[i], j, this->m_mDfxDp(i, j)));
		}
	}
}

void DERConnectionElement::add_DfxDl(tVector& vDfxDl) const
{
	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < 1; ++j)
		{
			vDfxDl.push_back(Triplet<Real>(this->m_vidxx[i], this->m_ridx, this->m_mDfxDl(i, j)));
		}
	}
}

void DERConnectionElement::add_DfxDr(tVector& vDfxDr) const
{
	int offset = this->m_ridx * 2;

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			vDfxDr.push_back(Triplet<Real>(this->m_vidxx[i], offset + j, this->m_mDfxDr(i, j)));
		}
	}
}

void DERConnectionElement::add_DfxD0v(tVector& vDfxDv) const
{
	int offset = this->m_ridx * 6;

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < 6; ++j)
		{
			vDfxDv.push_back(Triplet<Real>(this->m_vidxx[i], offset + j, this->m_mDfxD0v(i, j)));
		}
	}
}

Real DERConnectionElement::testForceLocal(const ConState& CS)
{
	Real EPS = 1e-6;

	ConState csFD = CS;

	VectorXd vfFD(this->m_numDOFx);
	vfFD.setZero(); // Zero force

	// Con position
	for (int i = 0; i < 3; ++i)
	{
		// Plus
		csFD = CS;
		csFD.m_vc(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy(csFD);
		Real ep = this->getEnergyIntegral();

		// Minus
		csFD = CS;
		csFD.m_vc(i) -= EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy(csFD);
		Real em = this->getEnergyIntegral();

		vfFD[0 + i] = -(ep - em) / (2 * EPS);
	}

	// Rod position
	for (int i = 0; i < 3; ++i)
	{
		// Plus
		csFD = CS;
		csFD.m_vrods[this->m_ridx].m_vr(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy(csFD);
		Real ep = this->getEnergyIntegral();

		// Minus
		csFD = CS;
		csFD.m_vrods[this->m_ridx].m_vr(i) -= EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy(csFD);
		Real em = this->getEnergyIntegral();

		vfFD[3 + i] = -(ep - em) / (2 * EPS);
	}

	// Euler angles
	for (int i = 0; i < 3; ++i)
	{
		// Plus
		csFD = CS;
		csFD.m_vR(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy(csFD);
		Real ep = this->getEnergyIntegral();

		// Minus
		csFD = CS;
		csFD.m_vR(i) -= EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Energy(csFD);
		Real em = this->getEnergyIntegral();

		vfFD[6 + i] = -(ep - em) / (2 * EPS);
	}

	// Twist angle

	// Plus
	csFD = CS;
	csFD.m_vrods[this->m_ridx].m_roda += EPS;
	updateStateForTesting(this->m_ridx, csFD);
	this->update_Energy(csFD);
	Real ep = this->getEnergyIntegral();

	// Minus
	csFD = CS;
	csFD.m_vrods[this->m_ridx].m_roda -= EPS;
	updateStateForTesting(this->m_ridx, csFD);
	this->update_Energy(csFD);
	Real em = this->getEnergyIntegral();

	vfFD[9 + 0] = -(ep - em) / (2 * EPS);

	// Compute difference 

	this->update_Force(CS); // Analytical force
	const VectorXd& vfA = this->getLocalForce();

	Real FDNorm = vfFD.norm(); // Estimation norm
	Real diffNorm = (vfA - vfFD).norm() / FDNorm;

	this->update_Energy(CS); // Restore

	if (FDNorm > 0.0 && diffNorm > 1e-9)
	{
		logSimu("[ERROR] Local error computing force: %.12f", diffNorm);
		//writeToFile(vfA, "LocalForceA.csv");
		//writeToFile(vfFD, "LocalForceF.csv");
	}

	return diffNorm;
}

Real DERConnectionElement::testJacobianLocal(const ConState& CS)
{
	Real EPS = 1e-6;

	ConState csFD = CS;

	MatrixXd mJFD(this->m_numDOFx, this->m_numDOFx);
	mJFD.setZero(); // Start with a zero Jacobian

	// Con position
	for (int i = 0; i < 3; ++i)
	{
		// Plus
		csFD = CS;
		csFD.m_vc(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Force(csFD);
		VectorXd fp = this->getLocalForce();

		// Minus
		csFD = CS;
		csFD.m_vc(i) -= EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Force(csFD);
		VectorXd fm = this->getLocalForce();

		VectorXd fDiff = (fp - fm) / (2 * EPS);

		for (int j = 0; j < this->m_numDOFx; ++j)
			mJFD(j, 0 + i) = fDiff(j); // Fill
	}

	// Rod position
	for (int i = 0; i < 3; ++i)
	{
		// Plus
		csFD = CS;
		csFD.m_vrods[this->m_ridx].m_vr(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Force(csFD);
		VectorXd fp = this->getLocalForce();

		// Minus
		csFD = CS;
		csFD.m_vrods[this->m_ridx].m_vr(i) -= EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Force(csFD);
		VectorXd fm = this->getLocalForce();

		VectorXd fDiff = (fp - fm) / (2 * EPS);

		for (int j = 0; j < this->m_numDOFx; ++j)
			mJFD(j, 3 + i) = fDiff(j); // Fill
	}

	// Euler angles
	for (int i = 0; i < 3; ++i)
	{
		// Plus
		csFD = CS;
		csFD.m_vR(i) += EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Force(csFD);
		VectorXd fp = this->getLocalForce();

		// Minus
		csFD = CS;
		csFD.m_vR(i) -= EPS;
		updateStateForTesting(this->m_ridx, csFD);
		this->update_Force(csFD);
		VectorXd fm = this->getLocalForce();

		VectorXd fDiff = (fp - fm) / (2 * EPS);

		for (int j = 0; j < this->m_numDOFx; ++j)
			mJFD(j, 6 + i) = fDiff(j); // Fill
	}

	// Twist angle

	// Plus
	csFD = CS;
	csFD.m_vrods[this->m_ridx].m_roda += EPS;
	updateStateForTesting(this->m_ridx, csFD);
	this->update_Force(csFD);
	VectorXd fp = this->getLocalForce();

	// Minus
	csFD = CS;
	csFD.m_vrods[this->m_ridx].m_roda -= EPS;
	updateStateForTesting(this->m_ridx, csFD);
	this->update_Force(csFD);
	VectorXd fm = this->getLocalForce();

	VectorXd fDiff = (fp - fm) / (2 * EPS);

	for (int j = 0; j < this->m_numDOFx; ++j)
		mJFD(j, 9 + 0) = fDiff(j); // Fill

	// Compute difference 

	if (!(mJFD - mJFD.transpose()).isZero(1e-6))
	{
		logSimu("[WARNING] Estimated matrix non-symmetric");
	}

	this->update_Jacobian(CS); // Analytical 
	const VectorXd& vJA = this->getLocalJacobian();

	int count = 0;
	VectorXd vJFD((int) this->m_vJVal.size());
	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j <= i; ++j)
			vJFD(count++) = mJFD(i, j);

	Real FDNorm = vJFD.norm(); // Estimation norm
	Real diffNorm = (vJA - vJFD).norm() / FDNorm;

	this->update_Force(CS); // Restore

	if (FDNorm > 0.0 && diffNorm > 1e-9)
	{
		logSimu("[ERROR] Local error computing Jacobian: %.12f", diffNorm);
		//writeToFile(vJA, "LocalJacobianA.csv");
		//writeToFile(vJFD, "LocalJacobianF.csv");
	}

	return diffNorm;
}

void DERConnectionElement::update_Force_Twist(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	VectorXd vg(7);

	getRBConGradientEnergyTwistOPT(
		this->m_vpmat[0]->getYoung(),
		this->m_vpmat[0]->getShear(),
		this->m_vpmat[0]->getWRadius(),
		this->m_vpmat[0]->getHRadius(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vR.data(),
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda,
		vg.data());

	this->m_vfVal = -1 * this->m_mDexDx.transpose()*vg;
}

void DERConnectionElement::update_Force_Bending(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	VectorXd vg(7);

	getRBConGradientEnergyBendingOPT(
		this->m_vpmat[0]->getYoung(),
		this->m_vpmat[0]->getShear(),
		this->m_vpmat[0]->getWRadius(),
		this->m_vpmat[0]->getHRadius(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		0.0,
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vR.data(),
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda,
		vg.data());

	this->m_vfVal = -1 * this->m_mDexDx.transpose()*vg;
}

void DERConnectionElement::update_Force_Original(const ConState& CS)
{
	VectorXd vg(10);

	if (CS.m_vrods[this->m_ridx].m_S > 0)
		getRigidBodyEnergyGradienti3D(
		10,
		10,
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vc.data(),
		CS.m_vR.data(),
		CS.m_vrods[this->m_ridx].m_vr.data(),
		CS.m_vrods[this->m_ridx].m_roda,
		vg.data());
	else
		getRigidBodyEnergyGradienti3D(
		10,
		10,
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vrods[this->m_ridx].m_vr.data(),
		CS.m_vR.data(),
		CS.m_vc.data(),
		CS.m_vrods[this->m_ridx].m_roda,
		vg.data());

	this->m_vfVal = -1 * vg;
}

void DERConnectionElement::update_Force_Meta(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	VectorXd vg(7);

	getRBConGradientOPTMetaTest(
		this->m_vpmat[0]->getYoung(),
		this->m_vpmat[0]->getShear(),
		this->m_vpmat[0]->getWRadius(),
		this->m_vpmat[0]->getHRadius(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda,
		vg.data());

	this->m_vfVal = -1 * this->m_mDexDx.transpose()*vg;
}

void DERConnectionElement::udpate_Force_Test(const ConState& CS)
{
	update_Force_Meta(CS);
}

void DERConnectionElement::update_Energy_Twist(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	this->m_energy = getRBConEnergyTwistOPT(
		this->m_vpmat[0]->getYoung(),
		this->m_vpmat[0]->getShear(),
		this->m_vpmat[0]->getWRadius(),
		this->m_vpmat[0]->getHRadius(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vR.data(),
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda);
}

void DERConnectionElement::update_Energy_Bending(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	this->m_energy = getRBConEnergyBendingOPT(
		this->m_vpmat[0]->getYoung(),
		this->m_vpmat[0]->getShear(),
		this->m_vpmat[0]->getWRadius(),
		this->m_vpmat[0]->getHRadius(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		0.0,
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vR.data(),
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda);
}

void DERConnectionElement::update_Energy_Original(const ConState& CS)
{
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		this->m_energy = getRigidBodyEnergyi3D(
		10,
		10,
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vc.data(),
		CS.m_vR.data(),
		CS.m_vrods[this->m_ridx].m_vr.data(),
		CS.m_vrods[this->m_ridx].m_roda);
	else
		this->m_energy = getRigidBodyEnergyi3D(
		10,
		10,
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		CS.m_vrods[this->m_ridx].m_vr.data(),
		CS.m_vR.data(),
		CS.m_vc.data(),
		CS.m_vrods[this->m_ridx].m_roda);
}

void DERConnectionElement::update_Energy_Meta(const ConState& CS)
{
	Vector3d ve;
	if (CS.m_vrods[this->m_ridx].m_S > 0)
		ve = CS.m_vrods[this->m_ridx].m_vr - CS.m_vc;
	else ve = CS.m_vc - CS.m_vrods[this->m_ridx].m_vr;

	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFr.tan) > 0.0);
	assert(ve.normalized().dot(CS.m_vrods[this->m_ridx].m_rodFc.tan) > 0.0);

	this->m_energy = getRBConEnergyOPTMetaTest(this->m_vpmat[0]->getYoung(),
		this->m_vpmat[0]->getShear(),
		this->m_vpmat[0]->getWRadius(),
		this->m_vpmat[0]->getHRadius(),
		this->m_hl0,
		CS.m_vrods[this->m_ridx].m_rodFc.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFc.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.tan.data(),
		CS.m_vrods[this->m_ridx].m_rodFr.nor.data(),
		CS.m_vrods[this->m_ridx].m_rodt,
		ve.data(),
		CS.m_vrods[this->m_ridx].m_roda);
}

void DERConnectionElement::update_Energy_Test(const ConState& CS)
{
	update_Energy_Meta(CS);
}