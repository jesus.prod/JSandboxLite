/*=====================================================================================*/
/*!
/file		SolidElement.h
/author		jesusprod
/brief		Basic abstract implementation of SolidElement to inherit from.
			All solid elements should inherit from this abstract class. It
			includes functions for the precomputation and storage of the
			local gradient and hessian and sparsity management.
*/
/*=====================================================================================*/

#ifndef SOLID_ELEMENT_H
#define SOLID_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/MathUtils.h>
#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/Element/SolidMaterial.h>

class SolidElement
{
public:
	SolidElement(int ndx, int nd0)
	{
		assert(ndx > 0);
		this->m_numDOFx = ndx;
		this->m_numDOF0 = nd0;
		this->m_vidxx.resize(ndx);
		this->m_vidx0.resize(nd0);

		this->m_vfVal.resize(ndx);
		this->m_vfVal.setZero();

		int ne = (int) ((ndx*(ndx + 1)) / 2.0);
		this->m_vJVal.resize(ne);
		this->m_vJVal.setZero();

		// Lumped mass and derivatives

		this->m_vmx.resize(ndx);
		this->m_vmx.setZero();
		this->m_mDmxDx.resize(ndx, ndx);
		this->m_mDmxD0.resize(ndx, nd0);
		this->m_mDmxDx.setZero();
		this->m_mDmxD0.setZero();

		// Generic parameterization

		this->m_vfx.resize(ndx);
		this->m_vfx.setZero();
		this->m_vf0.resize(nd0);
		this->m_vf0.setZero();

		this->m_mDfxDx.resize(ndx, ndx);
		this->m_mDfxD0.resize(ndx, nd0);
		this->m_mDfxDx.setZero();
		this->m_mDfxD0.setZero();
	
		this->m_addMass = false;

		this->m_preStrain = 1.0;

		this->m_vpmat.resize(1);
	}

	virtual ~SolidElement() { }

	virtual int getNumDOF_x() const { return m_numDOFx; }
	virtual int getNumDOF_0() const { return m_numDOF0; }

	virtual const iVector& getIndicesx() const { return this->m_vidxx; }
	virtual const iVector& getIndices0() const { return this->m_vidx0; }

	virtual void setIndicesx(const iVector& vi) { assert((int)vi.size() == m_numDOFx); this->m_vidxx = vi; }
	virtual void setIndices0(const iVector& vi) { assert((int)vi.size() == m_numDOF0); this->m_vidx0 = vi; }

	virtual SolidMaterial *getMaterial() const { return this->m_vpmat[0]; }
	virtual void setMaterial(SolidMaterial *pm) { this->m_vpmat[0] = pm; }

	virtual vector<SolidMaterial*> getMaterials() const { return this->m_vpmat; }
	virtual void setMaterials(vector<SolidMaterial*> vp) { this->m_vpmat = vp; }

	virtual Real getPreStrain() const { return this->m_preStrain; }
	virtual void setPreStrain(Real st) { this->m_preStrain = st; }

	////////////////////////////////////////////////////////////////////////////////////////////////
	// Basic parameterization for simulation

	virtual Real getEnergyIntegral() const;
	virtual Real getEnergyDensity() const;

	virtual const VectorXd& getLocalMass() const;
	virtual const VectorXd& getLocalForce() const;
	virtual const VectorXd& getLocalJacobian() const;
	
	virtual void addMass(VectorXd& vm, const vector<bool>* pvFixed = NULL) const;
	virtual void addForce(VectorXd& vf, const vector<bool>* pvFixed = NULL) const;
	virtual void addJacobian(tVector& vJ, const vector<bool>* pvFixed = NULL) const;

	virtual void update_EnergyForce(const VectorXd& vx, const VectorXd& vv);
	virtual void update_ForceJacobian(const VectorXd& vx, const VectorXd& vv);
	virtual void update_EnergyForceJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real testForceLocal(const VectorXd& vx, const VectorXd& vv);
	virtual Real testJacobianLocal(const VectorXd& vx, const VectorXd& vv);

	// Must implement in derived

	virtual Real getIntegrationVolume() const = 0;

	virtual void update_Mass(const VectorXd& vX) = 0;
	virtual void update_Rest(const VectorXd& vX) = 0;

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv) = 0; 
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv) = 0; 
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv) = 0; 

	// Must implement in derived


	////////////////////////////////////////////////////////////////////////////////////////////////
	// Generic parameterization derivatives

	virtual const VectorXd& getLocal_fx() const;
	virtual const VectorXd& getLocal_f0() const;

	virtual const MatrixXd& getLocal_DfxDx() const;
	virtual const MatrixXd& getLocal_DfxD0() const;

	virtual const MatrixXd& getLocal_DmxDx() const;
	virtual const MatrixXd& getLocal_DmxD0() const;

	virtual void add_fx(VectorXd& vfx, const vector<bool>* pvFixed = NULL) const;
	virtual void add_f0(VectorXd& vf0, const vector<bool>* pvFixed = NULL) const;

	virtual void add_DfxDx(tVector& vDfxDx, const vector<bool>* pvFixed = NULL) const;
	virtual void add_DfxD0(tVector& vDfxD0, const vector<bool>* pvFixed = NULL) const;

	virtual void add_DmxDx(tVector& vDmxDx, const vector<bool>* pvFixed = NULL) const;
	virtual void add_DmxD0(tVector& vDmxD0, const vector<bool>* pvFixed = NULL) const;

	virtual void update_fx(const VectorXd& vx, const VectorXd& vX) { logSimu("[WARNING] Default implementation!"); this->m_vfx.setZero(); }
	virtual void update_f0(const VectorXd& vx, const VectorXd& vX) { logSimu("[WARNING] Default implementation!"); this->m_vf0.setZero(); }

	virtual void update_DfxDx(const VectorXd& vx, const VectorXd& vX) { logSimu("[WARNING] Default implementation!"); this->m_mDfxDx.setZero(); }
	virtual void update_DfxD0(const VectorXd& vx, const VectorXd& vX) { logSimu("[WARNING] Default implementation!"); this->m_mDfxD0.setZero(); }

	virtual void update_DmxDx(const VectorXd& vx, const VectorXd& vX) { logSimu("[WARNING] Default implementation!"); this->m_mDmxDx.setZero(); }
	virtual void update_DmxD0(const VectorXd& vx, const VectorXd& vX) { logSimu("[WARNING] Default implementation!"); this->m_mDmxD0.setZero(); }

	virtual Real testDfxDxLocal(const VectorXd& vx, const VectorXd& vX);
	virtual Real testDfxD0Local(const VectorXd& vx, const VectorXd& vX);

protected:

	int m_numDOFx;
	int m_numDOF0;

	iVector m_vidxx;					// Vector of global indices for element nodes
	iVector m_vidx0;					// Vector of global indices for element rest

	Real m_energy;						// Where to store precomputed energy
	VectorXd m_vfVal;					// Where to store precomputed force
	VectorXd m_vJVal;					// Where to store triangular Jacobian

	VectorXd m_vfx;						// Where to store precomputed deformed force
	VectorXd m_vf0;						// Where to store precomputed material force
	MatrixXd m_mDfxDx;					// Where to store precomputed DfxDx 
	MatrixXd m_mDfxD0;					// Where to store precomputed DfxDX

	VectorXd m_vmx;						// Where to store precomputed lumped mass
	MatrixXd m_mDmxDx;					// Where to store precomputed mass derivative
	MatrixXd m_mDmxD0;					// Where to store precomputed mass derivative
	bool m_addMass;

	Real m_preStrain;

	vector<SolidMaterial*> m_vpmat;		// Pointer to material elements, not managed

};

#endif