/*=====================================================================================*/
/*! 
/file		SolidMaterial.cpp
/author		jesusprod
/brief		Implementation of SolidMaterial.h
 */
/*=====================================================================================*/

#include <JSandbox/Element/SolidMaterial.h>


void SolidMaterial::initFromYoungPoisson(Real E, Real p, Real density)
{
	// Parameter equivalences: http://en.wikipedia.org/wiki/Lam�_parameter

	this->m_density = density;

	Real t0 = 1 + p;
	Real t1 = 1 - 2*p;

	this->m_young = E;
	this->m_poisson = p;
	this->m_lame1 = (E*p)/(t0*t1);
	this->m_lame2 = E/(2*t0);
	this->m_bulk = E/(3*t1);
}

void SolidMaterial::initFromLameCoefficients(Real l, Real G, Real density)
{
	// Parameter equivalences: http://en.wikipedia.org/wiki/Lam�_parameters
	
	this->m_density = density;

	this->m_lame1 = l;
	this->m_lame2 = G;
	this->m_bulk = l + (2/3)*G;
	this->m_poisson = l/(2*(l+G));
	this->m_young = G*(3*l + 2*G)/(l + G);
}