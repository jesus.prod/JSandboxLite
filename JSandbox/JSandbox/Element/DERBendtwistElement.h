/*=====================================================================================*/
/*!
/file		DERBendingTwistElement.h
/author		jesusprod
/brief		Declaration of DERBendingTwistElement class (Jonas Zehnder version)
*/
/*=====================================================================================*/

#ifndef DER_BENTWIST_ELEMENT_H
#define DER_BENTWIST_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/DERBaseElement.h>

class DERBendTwistElement : public DERBaseElement
{

public:
	DERBendTwistElement();
	virtual ~DERBendTwistElement();

	virtual void update_Rest(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Mass(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual void update_Energy(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Force(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Jacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual Real computeEnergy(const Vector3d& x1, const Vector3d& x2, const Vector3d& x3, const Frame3d& f0, const Frame3d& f1, double et, double ft, double rt);
	virtual void computeForce(const Vector3d& x1, const Vector3d& x2, const Vector3d& x3, const Frame3d& f0, const Frame3d& f1, double et, double ft, double rt, VectorXd& vf);
	virtual void computeJacobian(const Vector3d& x1, const Vector3d& x2, const Vector3d& x3, const Frame3d& f0, const Frame3d& f1, double et, double ft, double rt, MatrixXd& mJ);
	virtual void computeRest(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1, double et, double ft, double rt);

	virtual void updateStateForTesting(dVector& vx, dVector& va, dVector& vt, vector<Frame3d>& vFr, vector<Frame3d>& vFm);
	virtual void testEnergyChanging(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void testForceChanging(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual Real computeTwistStiffness() const;
	virtual Real computeBendingStiffness() const;
	virtual Vector3d computeKB(const Vector3d& e, const Vector3d& f) const;
	virtual Real computeVertexLength(const Vector3d& e, const Vector3d& f) const;
	virtual VectorXd computeKappa(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1) const;

	void update_f0v(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
				    const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0);
	void update_fl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	void update_fr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	void update_DfxD0v(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
					   const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0);
	void update_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	void update_DfxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual Real getIntegrationVolume() const { return this->getRestVertexLength(); }

	Real getRestVertexLength() const { return (this->m_vl0(0) + this->m_vl0(1))*0.5; }

	Vector2d getRestKappa() const { return this->m_K0; }
	void setRestKappa(Vector2d K0) { this->m_K0 = K0; }

	Real getRestVertexTwist() const { return this->m_t0; }
	void setRestVertexTwist(Real t0) { this->m_t0 = t0; }

	bool getIsSplit() const { return this->m_isSplit; }
	void setIsSplit(bool is) { this->m_isSplit = is; }

protected:

	bool m_isSplit;

	double m_t0; // Rest twist
	Vector2d m_K0; // Rest curvature

	MatrixSd m_mDexDx;
	MatrixSd m_mDevDv;

};

#endif
