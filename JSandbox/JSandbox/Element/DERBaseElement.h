/*=====================================================================================*/
/*!
/file		DERBaseElement.h
/author		jesusprod
/brief		Implementation of DERBaseElement.h.
*/
/*=====================================================================================*/

#ifndef DER_BASE_ELEMENT_H
#define DER_BASE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidElement.h>

class DERBaseElement : public SolidElement
{
public:
	DERBaseElement(int ndx, int nd0) : SolidElement(ndx, nd0) { }
	~DERBaseElement() { }

	const iVector& getVertexIndices() const { return this->m_vvinds; }
	void setVertexIndices(const iVector& vi) { this->m_vvinds = vi; }

	const iVector& getEdgeIndices() const { return this->m_veinds; }
	void setEdgeIndices(const iVector& vi) { this->m_veinds = vi; }

	// New interface: basic parameterization

	virtual void update_Rest(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm) = 0;
	virtual void update_Mass(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm) = 0;

	virtual void update_Force(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm) = 0;
	virtual void update_Energy(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm) = 0;
	virtual void update_Jacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm) = 0;

	virtual void update_EnergyForce(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_ForceJacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_EnergyForceJacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual Real testForceLocal(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm) { /* TODO */ assert(false); return -1.0; };
	virtual Real testJacobianLocal(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm) { /* TODO */ assert(false); return -1.0; };

	// New interface: generic parameterization

	virtual void update_fx(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual void update_fv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
						   const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0);
	virtual void update_fr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_fl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual void update_DfxDv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
							   const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0);
	virtual void update_DfxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual const VectorXd& getLocal_fv() const;
	virtual const VectorXd& getLocal_fr() const;
	virtual const VectorXd& getLocal_fl() const;

	virtual const MatrixXd& getLocal_DfxDv() const;
	virtual const MatrixXd& getLocal_DfxDr() const;
	virtual const MatrixXd& getLocal_DfxDl() const;

	virtual void add_fv(VectorXd& vfv) const;
	virtual void add_fr(VectorXd& vfr) const;
	virtual void add_fl(VectorXd& vfl) const;

	virtual void add_DfxDv(tVector& vDfxDv) const;
	virtual void add_DfxDr(tVector& vDfxDr) const;
	virtual void add_DfxDl(tVector& vDfxDl) const;

	virtual void test_DfxDv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
							const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0);
	virtual void test_DfxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void test_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	// Default interface is not valid here

	virtual void update_Rest(const VectorXd& vX) { assert(false); }
	virtual void update_Mass(const VectorXd& vX) { assert(false); }

	virtual void update_Force(const VectorXd& vx, const VectorXd& vv) { assert(false); }
	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv) { assert(false); }
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv) { assert(false); }

	virtual Real testForceLocal(const VectorXd& vx, const VectorXd& vv) { assert(false); return -1.0; }
	virtual Real testJacobianLocal(const VectorXd& vx, const VectorXd& vv) { assert(false); return -1.0; }

	virtual Real getRestEdgeLength(int i) const { return this->m_vl0(i); }
	virtual void setRestEdgeLength(int i, Real l0) { this->m_vl0(i) = l0; }

protected:

	void update_frames(int vi, const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0, dVector& vtNewt, vector<Frame3d>& vFrNew, vector<Frame3d>& vFmNew);

	iVector m_vvinds;
	iVector m_veinds;

	VectorXd m_vl0;

	VectorXd m_vf0v;	// Forces at rest vertices
	VectorXd m_vfr;		// Forces at radius parameter
	VectorXd m_vfl;		// Forces at length parameter

	MatrixXd m_mDfxD0v;
	MatrixXd m_mDfxDr;
	MatrixXd m_mDfxDl;

};

#endif