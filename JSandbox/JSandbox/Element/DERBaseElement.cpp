/*=====================================================================================*/
/*!
/file		DERBaseElement.cpp
/author		jesusprod
/brief		Implementation of DERBaseElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/DERBaseElement.h>

#include <JSandbox/MathUtils.h>

////////////////////////////////////////////////////////////////////////////////////////////////
// New interface: basic parameterization

void DERBaseElement::update_EnergyForce(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	this->update_Energy(vx, va, vt, vFr, vFm);
	this->update_Force(vx, va, vt, vFr, vFm);
}

void DERBaseElement::update_ForceJacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	this->update_Force(vx, va, vt, vFr, vFm);
	this->update_Jacobian(vx, va, vt, vFr, vFm);
}

void DERBaseElement::update_EnergyForceJacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	this->update_Energy(vx, va, vt, vFr, vFm);
	this->update_Force(vx, va, vt, vFr, vFm);
	this->update_Jacobian(vx, va, vt, vFr, vFm);
}

////////////////////////////////////////////////////////////////////////////////////////////////
// New interface: generic parameterization

const VectorXd& DERBaseElement::getLocal_fv() const
{
	return this->m_vf0v;
}

const VectorXd& DERBaseElement::getLocal_fr() const
{
	return this->m_vfr;
}

const VectorXd& DERBaseElement::getLocal_fl() const
{
	return this->m_vfl;
}

const MatrixXd& DERBaseElement::getLocal_DfxDv() const
{
	return this->m_mDfxD0v;
}

const MatrixXd& DERBaseElement::getLocal_DfxDr() const
{
	return this->m_mDfxDr;
}

const MatrixXd& DERBaseElement::getLocal_DfxDl() const
{
	return this->m_mDfxDl;
}

void DERBaseElement::update_fx(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	this->update_Force(vx, va, vt, vFr, vFm);
}

void DERBaseElement::update_frames(int vi,  const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0, dVector& vtNew, vector<Frame3d>& vFrNew, vector<Frame3d>& vFmNew)
{
	int e0 = this->m_veinds[0];
	int e1 = this->m_veinds[1];

	// PT 
	if (vi == 0 || vi == 1)
	{
		Vector3d v0 = (vi == 0) ? get3D(vi, vx0) : get3D(vi - 1, vx0);
		Vector3d v1 = (vi == 1) ? get3D(vi, vx0) : get3D(vi + 1, vx0);
		Vector3d t0 = (v1 - v0).normalized();
		vFrNew[e0] = parallelTransport(vFr0[e0], t0);
		vFmNew[e0] = frameRotation(vFrNew[e0], va0[e0]);
	}

	// PT
	if (vi == 1 || vi == 2)
	{
		Vector3d v1 = (vi == 1) ? get3D(vi, vx0) : get3D(vi - 1, vx0);
		Vector3d v2 = (vi == 2) ? get3D(vi, vx0) : get3D(vi + 1, vx0);
		Vector3d t1 = (v2 - v1).normalized();
		vFrNew[e1] = parallelTransport(vFr0[e1], t1);
		vFmNew[e1] = frameRotation(vFrNew[e1], va0[e1]);
	}

	vtNew[vi] = signedAngle(parallelTransport(vFrNew[e0], vFrNew[e1].tan).bin, vFrNew[e1].bin, vFrNew[e1].tan, vt0[vi]);
}

void DERBaseElement::update_fv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
							   const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0)
{
	dVector vx0New = vx0;
	dVector va0New = va0;
	dVector vt0New = vt0;
	vector<Frame> vFr0New = vFr0;
	vector<Frame> vFm0New = vFm0;

	int nv = (int) this->m_vvinds.size();
	int ne = (int) this->m_veinds.size();

	this->m_vf0.resize(3*nv);

	for (int i = 0; i < nv; ++i)
	{
		int offset = 3*this->m_vvinds[i];

		for (int d = 0; d < 3; ++d)
		{
			// +
			vx0New[offset + d] += 1e-6;
			update_frames(i, vx0, va0, vt0, vFr0, vFm0, vt0New, vFr0New, vFm0New);
			update_Rest(vx0New, va0New, vt0New, vFr0New, vFm0New);
			update_Energy(vxx, vax, vtx, vFrx, vFmx);
			Real ep = this->getEnergyIntegral();

			// -
			vx0New[offset + d] -= 2e-6;
			update_frames(i, vx0, va0, vt0, vFr0, vFm0, vt0New, vFr0New, vFm0New);
			update_Rest(vx0New, va0New, vt0New, vFr0New, vFm0New);
			update_Energy(vxx, vax, vtx, vFrx, vFmx);
			Real em = this->getEnergyIntegral();
			
			this->m_vf0(3*i + d) = (em - ep)/2e-6;

			vx0New[offset + d] += 1e-6;
		}
	}
}

void DERBaseElement::update_fr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	int ne = (int) this->m_veinds.size();

	this->m_vfr.resize(ne);
	this->m_vfr.setZero();

	for (int p = 0; p < ne; ++p)
	{
		double rw = this->m_vpmat[p]->getWRadius();
		double rh = this->m_vpmat[p]->getHRadius();
		assert(isApprox(rw, rh, EPS_APPROX));

		// +
		this->m_vpmat[p]->setWRadius(rw + 1e-6);
		this->m_vpmat[p]->setHRadius(rh + 1e-6);
		this->update_Energy(vx, va, vt, vFr, vFm);
		Real Ep = this->getEnergyIntegral();

		// -
		this->m_vpmat[p]->setWRadius(rw - 1e-6);
		this->m_vpmat[p]->setHRadius(rh - 1e-6);
		this->update_Energy(vx, va, vt, vFr, vFm);
		Real Em = this->getEnergyIntegral();

		this->m_vfr[p] = (Em - Ep) / (2e-6);

		this->m_vpmat[p]->setWRadius(rw);
		this->m_vpmat[p]->setHRadius(rh);
	}
}

void DERBaseElement::update_fl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	assert(false); // Implement in derived...
}

void DERBaseElement::update_DfxDv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
								   const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0)
{
	int nv = (int) this->m_vvinds.size();

	this->m_mDfxD0.resize(this->m_numDOFx, 3*nv);
	this->m_mDfxD0.setZero(); // Init. from zero

	dVector vx0New = vx0;
	dVector va0New = va0;
	dVector vt0New = vt0;
	vector<Frame> vFr0New = vFr0;
	vector<Frame> vFm0New = vFm0;

	this->m_vf0.resize(3*nv);

	for (int i = 0; i < nv; ++i)
	{
		int offset = 3 * this->m_vvinds[i];

		for (int d = 0; d < 3; ++d)
		{
			// +
			vx0New[offset + d] += 1e-6;
			update_frames(i, vx0, va0, vt0, vFr0, vFm0, vt0New, vFr0New, vFm0New);
			update_Rest(vx0New, va0New, vt0New, vFr0New, vFm0New);
			update_Energy(vxx, vax, vtx, vFrx, vFmx);
			VectorXd vfp = this->getLocalForce();

			// -
			vx0New[offset + d] -= 2e-6;
			update_frames(i, vx0, va0, vt0, vFr0, vFm0, vt0New, vFr0New, vFm0New);
			update_Rest(vx0New, va0New, vt0New, vFr0New, vFm0New);
			update_Energy(vxx, vax, vtx, vFrx, vFmx);
			VectorXd vfm = this->getLocalForce();

			// Estimate
			VectorXd vfe = (vfm - vfp)/2e-6;

			for (int k = 0; k < this->m_numDOFx; ++k) // Set
				this->m_mDfxD0(k, offset + d) = this->m_vf0(k);

			vx0New[offset + d] += 1e-6;
		}
	}
}

void DERBaseElement::update_DfxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	int ne = (int) this->m_veinds.size();

	this->m_mDfxDr.resize(this->m_numDOFx, ne);
	this->m_mDfxDr.setZero(); // Init. from zero

	for (int p = 0; p < ne; ++p)
	{
		double rw = this->m_vpmat[p]->getWRadius();
		double rh = this->m_vpmat[p]->getHRadius();
		assert(isApprox(rw, rh, EPS_APPROX));

		// +
		this->m_vpmat[p]->setWRadius(rw + 1e-6);
		this->m_vpmat[p]->setHRadius(rh + 1e-6);
		this->update_Force(vx, va, vt, vFr, vFm);
		VectorXd vfp = this->getLocalForce();

		// -
		this->m_vpmat[p]->setWRadius(rw - 1e-6);
		this->m_vpmat[p]->setHRadius(rh - 1e-6);
		this->update_Force(vx, va, vt, vFr, vFm);
		VectorXd vfm = this->getLocalForce();

		VectorXd vJcol = (vfp - vfm)/(2e-6);
		for (int i = 0; i < this->m_numDOFx; ++i)
			this->m_mDfxDr(i, p) = vJcol[i];

		this->m_vpmat[p]->setWRadius(rw);
		this->m_vpmat[p]->setHRadius(rh);
	}
}

void DERBaseElement::update_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	int ne = (int) this->m_veinds.size();

	this->m_mDfxDl.resize(this->m_numDOFx, ne);
	this->m_mDfxDl.setZero(); // Init. from zero

	for (int p = 0; p < ne; ++p)
	{
		// +
		this->m_vl0(p) += 1e-6;
		this->update_Force(vx, va, vt, vFr, vFm);
		VectorXd vfp = this->getLocalForce();

		// -
		this->m_vl0(p) -= 2e-6;
		this->update_Force(vx, va, vt, vFr, vFm);
		VectorXd vfm = this->getLocalForce();

		VectorXd vJcol = (vfp - vfm) / (2e-6);
		for (int i = 0; i < this->m_numDOFx; ++i)
			this->m_mDfxDl(i, p) = vJcol[i];

		this->m_vl0(p) += 1e-6;
	}
}

void DERBaseElement::add_fv(VectorXd& vf0v) const
{
	int nv = (int) this->m_vvinds.size();

	for (int j = 0; j < 3 * nv; ++j)
		vf0v(j) = this->m_vf0v(0);
}

void DERBaseElement::add_fl(VectorXd& vfl) const
{
	for (int j = 0; j < (int) this->m_veinds.size(); ++j)
		vfl(this->m_veinds[j]) += this->m_vfl(j); // Edges
}

void DERBaseElement::add_fr(VectorXd& vfr) const
{
	for (int j = 0; j < (int) this->m_veinds.size(); ++j)
		vfr(this->m_veinds[j]) += this->m_vfr(j); // Edges
}

void DERBaseElement::add_DfxDv(tVector& vDfxD0v) const
{
	int nv = (int) this->m_vvinds.size();

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < nv; ++j)
		{
			for (int d = 0; d < 3; ++d)
			{
				vDfxD0v.push_back(Triplet<Real>(this->m_vidxx[i], 3*this->m_vvinds[j] + d, this->m_mDfxD0v(i, 3*j + d)));
			}
		}
	}
}

void DERBaseElement::add_DfxDl(tVector& vDfxDl) const
{
	int ne = (int) this->m_veinds.size();

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < ne; ++j)
		{
			vDfxDl.push_back(Triplet<Real>(this->m_vidxx[i], this->m_veinds[j], this->m_mDfxDl(i, j)));
		}
	}
}

void DERBaseElement::add_DfxDr(tVector& vDfxDr) const
{
	int ne = (int) this->m_veinds.size();

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < ne; ++j)
		{
			vDfxDr.push_back(Triplet<Real>(this->m_vidxx[i], 2*this->m_veinds[j] + 0, this->m_mDfxDr(i, 2*j + 0)));
			vDfxDr.push_back(Triplet<Real>(this->m_vidxx[i], 2*this->m_veinds[j] + 1, this->m_mDfxDr(i, 2*j + 1)));
		}
	}
}

void DERBaseElement::test_DfxDv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
								 const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0)
{
	// TODO
}

void DERBaseElement::test_DfxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	// Update analytical solution
	update_DfxDr(vx, va, vt, vFr, vFm); 

	MatrixXd mA = this->getLocal_DfxDr();

	// Update finite difference solution of base class
	DERBaseElement::update_DfxDr(vx, va, vt, vFr, vFm); 
	
	MatrixXd mFD = this->getLocal_DfxDr();

	MatrixXd mDF = (mA - mFD);
	Real realNorm = mFD.norm();
	Real diffNorm = mDF.norm();
	if (realNorm > 1e-9 && diffNorm / realNorm > 1e-6)
		logSimu("[ERROR] Error computing DfxDr: %.9f", diffNorm / realNorm);
}

void DERBaseElement::test_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	// Update analytical solution
	update_DfxDl(vx, va, vt, vFr, vFm);

	MatrixXd mA = this->getLocal_DfxDl();

	// Update finite difference solution of base class
	DERBaseElement::update_DfxDl(vx, va, vt, vFr, vFm);

	MatrixXd mFD = this->getLocal_DfxDl();

	MatrixXd mDF = (mA - mFD);
	Real realNorm = mFD.norm();
	Real diffNorm = mDF.norm();
	if (realNorm > 1e-9 && diffNorm / realNorm > 1e-6)
		logSimu("[ERROR] Error computing DfxDl: %.9f", diffNorm / realNorm);
}