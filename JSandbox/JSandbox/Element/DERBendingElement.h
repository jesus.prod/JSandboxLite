/*=====================================================================================*/
/*!
/file		DERBendingElement.h
/author		jesusprod
/brief		Declaration of DERBendingElement class
*/
/*=====================================================================================*/

#ifndef DER_BENDING_ELEMENT_H
#define DER_BENDING_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/DERBaseElement.h>

class DERBendingElement : public DERBaseElement
{

public:
	DERBendingElement();
	virtual ~DERBendingElement();

	virtual void update_Rest(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Mass(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual void update_Energy(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Force(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Jacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual Real computeEnergy(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1);
	virtual void computeForce(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1, VectorXd& vf);
	virtual void computeJacobian(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1, MatrixXd& mJ);
	virtual void computeRest(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1);

	virtual MatrixXd computeB() const;
	virtual Vector3d computeKB(const Vector3d& e, const Vector3d& f) const;
	virtual Real computeVertexLength(const Vector3d& e, const Vector3d& f) const;
	virtual VectorXd computeKappa(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1) const;
	virtual void computeGradKappa(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1, MatrixXd& gK) const;
	virtual void computeHessKappa(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1, MatrixXd& gK1, MatrixXd& gK2) const;

	void update_fl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	void update_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual Real getIntegrationVolume() const { return this->m_l0; }

	Real getRestVertexLength() const { return this->m_l0; }
	void setRestVertexLength(Real l0) { this->m_l0 = l0; }

	Vector2d getRestKappa() const { return this->m_K0; }
	void setRestKappa(Vector2d K0) { this->m_K0 = K0; }

	Real getConstant() const;

protected:

	MatrixXd m_DeDv; // Transmission
	VectorXd m_K0; // Rest curvature
	Real m_l0; // Rest vertex length 

};

#endif
