/*=====================================================================================*/
/*!
/file		SolidElement.cpp
/author		jesusprod
/brief		Implementation of SolidElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/SolidElement.h>

#include <JSandbox/MathUtils.h>

////////////////////////////////////////////////////////////////////////////////////////////////
// Basic parameterization for simulation

Real SolidElement::getEnergyIntegral() const
{
	return this->m_energy;
}

Real SolidElement::getEnergyDensity() const
{
	return this->m_energy / this->getIntegrationVolume();
}

const VectorXd& SolidElement::getLocalMass() const
{
	return this->m_vmx;
}

const VectorXd& SolidElement::getLocalForce() const
{
	return this->m_vfVal;
}

const VectorXd& SolidElement::getLocalJacobian() const
{
	return this->m_vJVal;
}

void SolidElement::addMass(VectorXd& vm, const vector<bool>* pvFixed) const
{
	if (!this->m_addMass)
		return; // Ignore

	if (pvFixed != NULL)
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
		{
			int idx = this->m_vidxx[i];
			if ((*pvFixed)[idx])
				continue;

			vm(idx) += this->m_vmx(i);
		}
	}
	else
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
			vm(this->m_vidxx[i]) += this->m_vmx(i);
	}
}


void SolidElement::addForce(VectorXd& vf, const vector<bool>* pvFixed) const
{
	if (pvFixed != NULL)
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
		{
			int idx = this->m_vidxx[i];
			if ((*pvFixed)[idx])
				continue;

			vf(idx) += this->m_vfVal(i);
		}
	}
	else
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
			vf(this->m_vidxx[i]) += this->m_vfVal(i);
	}
}

void SolidElement::addJacobian(tVector& vJ, const vector<bool>* pvFixed) const
{
	if (pvFixed != NULL)
	{
		int count = 0;
		for (int i = 0; i < this->m_numDOFx; ++i)
		{
			int idxi = this->m_vidxx[i];
			if ((*pvFixed)[idxi])
			{
				count += i + 1;
				continue;
			}

			for (int j = 0; j <= i; ++j) // Sym
			{
				int idxj = this->m_vidxx[j];
				if ((*pvFixed)[idxj])
				{
					count++;
					continue;
				}

				int r, c;
				if (idxi >= idxj)
				{
					r = idxi;
					c = idxj;
				}
				else
				{
					c = idxi;
					r = idxj;
				}
				vJ.push_back(Triplet<Real>(r, c, this->m_vJVal(count)));
				count++;
			}
		}
	}
	else
	{
		int count = 0;
		for (int i = 0; i < this->m_numDOFx; ++i)
			for (int j = 0; j <= i; ++j) // Sym
			{
			int idxi = this->m_vidxx[i];
			int idxj = this->m_vidxx[j];

			int r, c;
			if (idxi >= idxj)
			{
				r = idxi;
				c = idxj;
			}
			else
			{
				c = idxi;
				r = idxj;
			}
			vJ.push_back(Triplet<Real>(r, c, this->m_vJVal(count)));
			count++;
			}
	}
}


void SolidElement::update_EnergyForce(const VectorXd& vx, const VectorXd& vv)
{
	this->update_Energy(vx, vv);
	this->update_Force(vx, vv);
}

void SolidElement::update_ForceJacobian(const VectorXd& vx, const VectorXd& vv)
{
	this->update_Force(vx, vv);
	this->update_Jacobian(vx, vv);
}

void SolidElement::update_EnergyForceJacobian(const VectorXd& vx, const VectorXd& vv)
{
	this->update_Energy(vx, vv);
	this->update_Force(vx, vv);
	this->update_Jacobian(vx, vv);
}

Real SolidElement::testForceLocal(const VectorXd& vx, const VectorXd& vv)
{
	Real EPS = 1e-6;

	VectorXd vxFD = vx;

	VectorXd vfFD(this->m_numDOFx);
	vfFD.setZero(); // Zero force

	for (int i = 0; i < (int) this->m_vidxx.size(); ++i)
	{
		// Plus
		vxFD(this->m_vidxx[i]) += EPS;
		this->update_Energy(vxFD, vv);
		Real ep = this->getEnergyIntegral();
		vxFD(this->m_vidxx[i]) -= EPS;

		// Minus
		vxFD(this->m_vidxx[i]) -= EPS;
		this->update_Energy(vxFD, vv);
		Real em = this->getEnergyIntegral();
		vxFD(this->m_vidxx[i]) += EPS;

		vfFD[i] = -(ep - em) / (2 * EPS);
	}

	this->update_Force(vx, vv); // Analytical 
	const VectorXd& vfA = this->getLocalForce();

	// Compute difference

	Real FDNorm = vfFD.norm(); // Estimation norm
	Real diffNorm = (vfA - vfFD).norm() / FDNorm;

	this->update_Energy(vx, vv); // Restore

	if (FDNorm >= 1e-9)
		return diffNorm;
	else return 0;
}

Real SolidElement::testJacobianLocal(const VectorXd& vx, const VectorXd& vv)
{
	Real EPS = 1e-6;

	VectorXd vxFD = vx;

	MatrixXd mJFD(this->m_numDOFx, this->m_numDOFx);
	mJFD.setZero(); // Start with zero Jacobian

	for (int i = 0; i < (int) this->m_vidxx.size(); ++i)
	{
		// Plus
		vxFD(this->m_vidxx[i]) += EPS;
		this->update_Force(vxFD, vv);
		VectorXd fp = this->getLocalForce();
		vxFD(this->m_vidxx[i]) -= EPS;

		// Minus
		vxFD(this->m_vidxx[i]) -= EPS;
		this->update_Force(vxFD, vv);
		VectorXd fm = this->getLocalForce();
		vxFD(this->m_vidxx[i]) += EPS;

		VectorXd fDiff = (fp - fm) / (2 * EPS);

		for (int j = 0; j < this->m_numDOFx; ++j)
			mJFD(j, i) = fDiff(j); // Fill matrix
	}

	const VectorXd& vJA = this->getLocalJacobian();
	this->update_Jacobian(vx, vv); // Analytical 

	int count = 0;
	VectorXd vJFD((int) this->m_vJVal.size());
	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j <= i; ++j)
			vJFD(count++) = mJFD(i, j);

	// Compute difference

	Real FDNorm = vJFD.norm(); // Estimation norm
	Real diffNorm = (vJA - vJFD).norm() / FDNorm;

	this->update_Force(vx, vv); // Restore

	if (FDNorm >= 1e-9)
		return diffNorm;
	else return 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Generic parameterization derivatives

const VectorXd& SolidElement::getLocal_fx() const
{
	return this->m_vfx;
}

const VectorXd& SolidElement::getLocal_f0() const
{
	return this->m_vf0;
}

const MatrixXd& SolidElement::getLocal_DfxDx() const
{
	return this->m_mDfxDx;
}

const MatrixXd& SolidElement::getLocal_DfxD0() const
{
	return this->m_mDfxD0;
}

const MatrixXd& SolidElement::getLocal_DmxDx() const
{
	return this->m_mDmxDx;
}

const MatrixXd& SolidElement::getLocal_DmxD0() const
{
	return this->m_mDmxD0;
}

void SolidElement::add_fx(VectorXd& vf, const vector<bool>* pvFixed) const
{
	if (pvFixed != NULL)
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
		{
			int idx = this->m_vidxx[i];
			if ((*pvFixed)[idx])
				continue;

			vf(idx) += this->m_vfx(i);
		}
	}
	else
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
			vf(this->m_vidxx[i]) += this->m_vfx(i);
	}
}

void SolidElement::add_f0(VectorXd& vf, const vector<bool>* pvFixed) const
{
	if (pvFixed != NULL)
	{
		for (int i = 0; i < this->m_numDOF0; ++i)
		{
			int idx = this->m_vidx0[i];
			if ((*pvFixed)[idx])
				continue;

			vf(idx) += this->m_vf0(i);
		}
	}
	else
	{
		for (int i = 0; i < this->m_numDOF0; ++i)
			vf(this->m_vidx0[i]) += this->m_vf0(i);
	}
}

void SolidElement::add_DfxDx(tVector& vJ, const vector<bool>* pvFixed) const
{
	if (pvFixed != NULL)
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
		{
			int idxi = this->m_vidxx[i];
			if ((*pvFixed)[idxi])
				continue;

			for (int j = 0; j < this->m_numDOFx; ++j)
			{
				int idxj = this->m_vidxx[j];
				if ((*pvFixed)[idxj])
					continue;

				vJ.push_back(Triplet<Real>(idxi, idxj, this->m_mDfxDx(i, j)));
			}
		}
	}
	else
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
		{
			int idxi = this->m_vidxx[i];

			for (int j = 0; j < this->m_numDOFx; ++j)
			{
				int idxj = this->m_vidxx[j];

				vJ.push_back(Triplet<Real>(idxi, idxj, this->m_mDfxDx(i, j)));
			}
		}
	}
}

void SolidElement::add_DfxD0(tVector& vJ, const vector<bool>* pvFixed) const
{
	if (pvFixed != NULL)
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
		{
			int idxi = this->m_vidxx[i];
			if ((*pvFixed)[idxi])
				continue;

			for (int j = 0; j < this->m_numDOF0; ++j)
			{
				int idxj = this->m_vidx0[j];
				if ((*pvFixed)[idxj])
					continue;

				vJ.push_back(Triplet<Real>(idxi, idxj, this->m_mDfxD0(i, j)));
			}
		}
	}
	else
	{
		for (int i = 0; i < this->m_numDOFx; ++i)
		{
			int idxi = this->m_vidxx[i];

			for (int j = 0; j < this->m_numDOF0; ++j)
			{
				int idxj = this->m_vidx0[j];

				vJ.push_back(Triplet<Real>(idxi, idxj, this->m_mDfxD0(i, j)));
			}
		}
				
	}
}

void SolidElement::add_DmxDx(tVector& vJ, const vector<bool>* pvFixed) const
{
	if (!this->m_addMass)
		return; // Ignore

	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j < this->m_numDOFx; ++j)
			vJ.push_back(Triplet<Real>(this->m_vidxx[i], this->m_vidxx[j], this->m_mDmxDx(i, j)));
}

void SolidElement::add_DmxD0(tVector& vJ, const vector<bool>* pvFixed) const
{
	if (!this->m_addMass)
		return; // Ignore

	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j < this->m_numDOF0; ++j)
			vJ.push_back(Triplet<Real>(this->m_vidxx[i], this->m_vidx0[j], this->m_mDmxD0(i, j)));
}

Real SolidElement::testDfxDxLocal(const VectorXd& vx, const VectorXd& vX)
{
	Real EPS = 1e-6;

	VectorXd vxFD = vx;

	MatrixXd mJFD(this->m_numDOFx, this->m_numDOFx);
	mJFD.setZero(); // Start with zero Jacobian

	for (int i = 0; i < (int) this->m_vidxx.size(); ++i)
	{
		// Plus
		vxFD(this->m_vidxx[i]) += EPS;
		this->update_Force(vxFD, VectorXd());
		VectorXd fp = this->getLocalForce();
		vxFD(this->m_vidxx[i]) -= EPS;

		// Minus
		vxFD(this->m_vidxx[i]) -= EPS;
		this->update_Force(vxFD, VectorXd());
		VectorXd fm = this->getLocalForce();
		vxFD(this->m_vidxx[i]) += EPS;

		VectorXd fDiff = (fp - fm) / (2 * EPS);

		for (int j = 0; j < this->m_numDOFx; ++j)
			mJFD(j, i) = fDiff(j); // Fill matrix
	}

	this->update_DfxDx(vx, vX); // Analytical 
	const MatrixXd& mJA = this->getLocal_DfxDx();

	// Compute difference

	Real FDNorm = mJFD.norm(); // Estimation norm
	Real diffNorm = (mJA - mJFD).norm() / FDNorm;

	this->update_Force(vx, VectorXd());

	if (FDNorm >= 1e-9)
		return diffNorm;
	else return 0;
}

Real SolidElement::testDfxD0Local(const VectorXd& vx, const VectorXd& vX)
{
	Real EPS = 1e-6;

	VectorXd vXFD = vX;

	MatrixXd mJFD(this->m_numDOFx, this->m_numDOF0);
	mJFD.setZero(); // Start with zero Jacobian

	for (int i = 0; i < (int) this->m_vidx0.size(); ++i)
	{
		// Plus
		vXFD(this->m_vidx0[i]) += EPS;
		this->update_Rest(vXFD);
		this->update_Force(vx, VectorXd());
		VectorXd fp = this->getLocalForce();
		vXFD(this->m_vidx0[i]) -= EPS;

		// Minus
		vXFD(this->m_vidx0[i]) -= EPS;
		this->update_Rest(vXFD);
		this->update_Force(vx, VectorXd());
		VectorXd fm = this->getLocalForce();
		vXFD(this->m_vidx0[i]) += EPS;

		VectorXd fDiff = (fp - fm) / (2 * EPS);

		for (int j = 0; j < this->m_numDOFx; ++j)
			mJFD(j, i) = fDiff(j); // Fill matrix
	}

	this->update_DfxD0(vx, vX); // Analytical 
	const MatrixXd& mJA = this->getLocal_DfxD0();

	// Compute difference

	Real FDNorm = mJFD.norm(); // Estimation norm
	Real diffNorm = (mJA - mJFD).norm() / FDNorm;

	MatrixXd mJDiff = (mJA - mJFD);
	//writeToFile(mJA, "LocalJacobianA.csv");
	//writeToFile(mJFD, "LocalJacobianFD.csv");
	//writeToFile(mJDiff, "LocalJacobianDiff.csv");

	this->update_Rest(vX); // Restore
	this->update_Force(vx, VectorXd());

	if (FDNorm >= 1e-9)
		return diffNorm;
	else return 0;
}