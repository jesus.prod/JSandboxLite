/*=====================================================================================*/
/*!
/file		DERConnectionElement.h
/author		jesusprod
/brief		Implementation of DERConnectionElement.h.
*/
/*=====================================================================================*/

#ifndef DER_CONNECTION_ELEMENT_H
#define DER_CONNECTION_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidElement.h>

typedef struct ConStateRod
{
	ConStateRod()
	{
		this->m_roda = 0;
		this->m_rodt = 0;
		m_vr.setZero();
		m_S = 1.0;
	}

	ConStateRod(const ConStateRod& toCopy)
	{
		this->m_roda = toCopy.m_roda;
		this->m_rodt = toCopy.m_rodt;
		this->m_rodFc = toCopy.m_rodFc;
		this->m_rodFr = toCopy.m_rodFr;
		this->m_vr = toCopy.m_vr;
		this->m_S = toCopy.m_S;
	}

	Real m_S;					// Constant for inversion

	Real m_roda;				// ReferenceR - materialR angle
	Real m_rodt;				// RefereneC - ReferenceR twist
	Frame3d m_rodFc;				// Reference frame C
	Frame3d m_rodFr;				// Reference frame R
	Vector3d m_vr;				// Additional rod point
}
ConStateRod;

typedef struct ConState
{
	ConState()
	{
		this->m_vR.setZero();
		this->m_vc.setZero();
	}

	ConState(const ConState& toCopy)
	{
		this->m_vR = toCopy.m_vR;
		this->m_vc = toCopy.m_vc;
		this->m_vrods = toCopy.m_vrods;
	}

	Vector3d m_vR;
	Vector3d m_vc;

	vector<ConStateRod> m_vrods;
}
ConState;

class DERConnectionElement : public SolidElement
{
public:
	DERConnectionElement();
	~DERConnectionElement();

	const iVector& getVertexIndices() const { return this->m_vvinds; }
	void setNodeIndices(const iVector& vi) { this->m_vvinds = vi; }

	const iVector& getEdgeIndices() const { return this->m_veinds; }
	void setEdgeIndices(const iVector& vi) { this->m_veinds = vi; }

	virtual int getConIndex() const { return this->m_cidx; }
	virtual void setConIndex(int idx) { this->m_cidx = idx; }

	virtual int getRodIndex() const { return this->m_ridx; }
	virtual void setRodIndex(int idx) { this->m_ridx = idx; }

	virtual void update_Rest(const ConState& CS);

	virtual void update_Energy(const ConState& CS);
	virtual void update_Force(const ConState& CS);
	virtual void update_Jacobian(const ConState& CS);

	virtual void update_DfxDp(const ConState& CS);

	virtual void update_Force_Twist(const ConState& CS);
	virtual void update_Energy_Twist(const ConState& CS);

	virtual void update_Force_Bending(const ConState& CS);
	virtual void update_Energy_Bending(const ConState& CS);

	virtual void update_Force_Original(const ConState& CS);
	virtual void update_Energy_Original(const ConState& CS);

	virtual void update_Force_Meta(const ConState& CS);
	virtual void update_Energy_Meta(const ConState& CS);

	virtual void udpate_Force_Test(const ConState& CS);
	virtual void update_Energy_Test(const ConState& CS);

	virtual void update_EnergyForce(const ConState& CS) { this->update_Energy(CS); this->update_Force(CS); }
	virtual void update_ForceJacobian(const ConState& CS) { this->update_Force(CS); this->update_Jacobian(CS); }

	virtual void testEnergyChanging(const ConState& CS);
	virtual void testForceChanging(const ConState& CS);
	virtual Real testForceLocal(const ConState& CS);
	virtual Real testJacobianLocal(const ConState& CS);

	virtual void update_EnergyForceJacobian(const ConState& CS) { this->update_Energy(CS);  this->update_Force(CS); this->update_Jacobian(CS); };

	virtual void update_f0v(const ConState& CS0, const ConState& CSx, const Matrix3d& R0x) { /* TODO */ }
	virtual void update_fr(const ConState& CS) { /* TODO */ }
	virtual void update_fl(const ConState& CS) { /* TODO */ }

	virtual void update_DfxD0v(const ConState& CS0, const ConState& CSx, const Matrix3d& vR0x);
	virtual void update_DfxDr(const ConState& CS);
	virtual void update_DfxDl(const ConState& CS);

	virtual const MatrixXd& getLocal_DfxDp() const { return this->m_mDfxDp; }

	virtual const VectorXd& getLocal_f0v() const { return this->m_vf0v; }
	virtual const VectorXd& getLocal_fr() const { return this->m_vfr; }
	virtual const VectorXd& getLocal_fl() const { return this->m_vfl; }

	virtual const MatrixXd& getLocal_DfxD0v() const { return this->m_mDfxD0v; }
	virtual const MatrixXd& getLocal_DfxDr() const { return this->m_mDfxDr; }
	virtual const MatrixXd& getLocal_DfxDl() const { return this->m_mDfxDl; }

	virtual void add_f0v(VectorXd& vfv) const { /* TODO */ }
	virtual void add_fr(VectorXd& vfr) const { /* TODO */ }
	virtual void add_fl(VectorXd& vfl) const { /* TODO */ }

	virtual void add_DfxD0v(tVector& vDfxDv) const;
	virtual void add_DfxDr(tVector& vDfxDr) const;
	virtual void add_DfxDl(tVector& vDfxDl) const;

	virtual void add_DfxDp(tVector& vDfxDp) const;

	virtual Real getHalfEdgeLength() const { return this->m_hl0; }
	virtual void setHalfEdgeLength(Real hL0) { this->m_hl0 = hL0; }

	virtual Real getIntegrationVolume() const { return this->m_hl0; }

	virtual void update_Mass(const VectorXd& vX) { /* Not a mass holder */ }

	// Default interface is not valid here

	virtual void update_Rest(const VectorXd& vX) { assert(false); }

	virtual void update_Force(const VectorXd& vx, const VectorXd& vv) { assert(false); }
	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv) { assert(false); }
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv) { assert(false); }

	virtual Real testForceLocal(const VectorXd& vx, const VectorXd& vv) { assert(false); return -1.0; }
	virtual Real testJacobianLocal(const VectorXd& vx, const VectorXd& vv) { assert(false); return -1.0; }

	virtual void test_DfxDr(const ConState& CS);
	virtual void test_DfxDl(const ConState& CS);
	virtual void test_DfxD0v(const ConState& CS0, const ConState& CSx, const Matrix3d& R0x);

protected:

	iVector m_vvinds;
	iVector m_veinds;

	int m_cidx;
	int m_ridx;

	double m_hl0;

	VectorXd m_vf0v;
	VectorXd m_vfr;
	VectorXd m_vfl;

	MatrixXd m_mDfxD0v;
	MatrixXd m_mDfxDr;
	MatrixXd m_mDfxDl;

	MatrixXd m_mDfxDp;

	MatrixXd m_mDexDx;
	MatrixXd m_mDevDv;

};

#endif