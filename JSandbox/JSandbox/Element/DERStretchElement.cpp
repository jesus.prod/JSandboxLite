/*=====================================================================================*/
/*!
/file		DERStretchElement.cpp
/author		jesusprod
/brief		Implementation of DERStretchElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/DERStretchElement.h>

DERStretchElement::DERStretchElement() : DERBaseElement(7, 7)
{
	this->m_vpmat.resize(1);
	this->m_vl0.resize(1);
	this->m_vl0.setZero();

	this->m_addMass = true;

	MatrixXd mDexDx(4, 7);
	MatrixXd mDevDv(3, 6);

	mDexDx.setZero();
	mDevDv.setZero();

	mDexDx(3, 6) = 1.0;

	Matrix3d I; I.setIdentity();

	addBlock3x3From(0, 0, I * -1.0, mDexDx);
	addBlock3x3From(0, 3, I, mDexDx);

	addBlock3x3From(0, 0, I * -1.0, mDevDv);
	addBlock3x3From(0, 3, I, mDevDv);

	this->m_mDexDx = mDexDx.sparseView(EPS_POS);
	this->m_mDevDv = mDevDv.sparseView(EPS_POS);

	this->m_regime = Regime::Regular;
}

DERStretchElement::~DERStretchElement()
{
	// Nothing to do here...
}
 
Real DERStretchElement::compute_strain(const dVector& vx, const dVector& vX)
{
	Vector3d x1 = Vector3d(vx[this->m_vidxx[0]], vx[this->m_vidxx[1]], vx[this->m_vidxx[2]]);
	Vector3d x2 = Vector3d(vx[this->m_vidxx[3]], vx[this->m_vidxx[4]], vx[this->m_vidxx[5]]);

	Vector3d X1 = Vector3d(vX[this->m_vidxx[0]], vX[this->m_vidxx[1]], vX[this->m_vidxx[2]]);
	Vector3d X2 = Vector3d(vX[this->m_vidxx[3]], vX[this->m_vidxx[4]], vX[this->m_vidxx[5]]);

	Real L0 = (X1 - X2).norm();
	Real Lx = (x1 - x2).norm();
	return Lx / L0;
}

void DERStretchElement::update_Rest(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Vector3d x1 = Vector3d(vx[this->m_vidxx[0]], vx[this->m_vidxx[1]], vx[this->m_vidxx[2]]);
	Vector3d x2 = Vector3d(vx[this->m_vidxx[3]], vx[this->m_vidxx[4]], vx[this->m_vidxx[5]]);

	this->m_vl0.resize(1); // One edge
	this->m_vl0(0) = (x2 - x1).norm();

	this->m_iv = this->m_vl0[0];
}

void DERStretchElement::update_Mass(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	if (!this->m_addMass)
		return; // Ignore

	double M = M_PI*this->m_vpmat[0]->getWRadius()*
					this->m_vpmat[0]->getHRadius()*
					this->m_vpmat[0]->getDensity()*
					this->m_vl0(0);

	double avgr = 0.5*(this->m_vpmat[0]->getWRadius() + this->m_vpmat[0]->getHRadius());

	double nodeMass = 0.5*M;
	double edgeMass = avgr*M;

	// Set node mass value
	for (int i = 0; i < 6; ++i)
		this->m_vmx(i) = nodeMass;

	// Set edge inertia value
	this->m_vmx(6) = edgeMass;
}

void DERStretchElement::update_Energy(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Vector3d x1;
	Vector3d x2;
	for (int i = 0; i < 3; ++i)
	{
		x1(i) = vx[this->m_vidxx[0 + i]];
		x2(i) = vx[this->m_vidxx[3 + i]];
	}
	
	Vector3d vx12 = x2 - x1;
	const double* x12 = vx12.data();
	//Real L0 = this->m_vl0(0)*this->m_preStrain;

	Real L0 = this->m_vl0(0);
	
	//if (this->m_regime != Regime::Regular)
	//{
	//	if (m_regime == Regime::FreeExt && vx12.norm() >= L0)
	//	{
	//		this->m_energy = 0;
	//		return;
	//	}
	//	if (m_regime == Regime::FreeCom && vx12.norm() <= L0)
	//	{
	//		this->m_energy = 0;
	//		return;
	//	}
	//}
	
	//Real kS = this->computeK();
	
	//Real rh = this->m_vpmat[0]->getHRadius();
	//Real rw = this->m_vpmat[0]->getWRadius();

	//Real E = 0;
	//if (this->m_vpmat[0]->getStretchK() != -1)
	Real E = this->m_vpmat[0]->getStretchK();
	//else E = this->m_vpmat[0]->getYoung();
	
#include "../../Maple/Code/RodEdge3DEnergyBasicOPT.mcg"
	
	this->m_energy = t12*(this->m_iv / this->m_vl0[0]);

	//if (m_energy > 1e-6)
	//{
	//	logSimu("Stretch energy not zero: %f. Rest length: %f. Current length: %f", this->m_energy, L0, vx12.norm());
	//}
}

void DERStretchElement::update_Force(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Vector3d x1;
	Vector3d x2;
	for (int i = 0; i < 3; ++i)
	{
		x1(i) = vx[this->m_vidxx[0 + i]];
		x2(i) = vx[this->m_vidxx[3 + i]];
	}

	Vector3d vx12 = x2 - x1;
	const double* x12 = vx12.data();
	//Real L0 = this->m_vl0(0)*this->m_preStrain;

	Real L0 = this->m_vl0(0);

	//if (this->m_regime != Regime::Regular)
	//{
	//	if (m_regime == Regime::FreeExt && vx12.norm() >= L0)
	//	{
	//		this->m_vfVal.setZero();
	//		return;
	//	}
	//	if (m_regime == Regime::FreeCom && vx12.norm() <= L0)
	//	{
	//		this->m_vfVal.setZero();
	//		return;
	//	}
	//}

	//Real kS = this->computeK();

	//Real rh = this->m_vpmat[0]->getHRadius();
	//Real rw = this->m_vpmat[0]->getWRadius();

	//Real E = 0;
	//if (this->m_vpmat[0]->getStretchK() != -1)
	Real E = this->m_vpmat[0]->getStretchK();
	//else E = this->m_vpmat[0]->getYoung();

	VectorXd vfx(4);
	vfx.setZero();

#include "../../Maple/Code/RodEdge3DForceBasicOPT.mcg"

	this->m_vfVal = this->m_mDexDx.transpose()*vfx*(this->m_iv / this->m_vl0[0]);
}

void DERStretchElement::update_Jacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Vector3d x1;
	Vector3d x2;
	for (int i = 0; i < 3; ++i)
	{
		x1(i) = vx[this->m_vidxx[0 + i]];
		x2(i) = vx[this->m_vidxx[3 + i]];
	}

	Vector3d vx12 = x2 - x1;
	const double* x12 = vx12.data();
	//Real L0 = this->m_vl0(0)*this->m_preStrain;

	Real L0 = this->m_vl0(0);

	//if (this->m_regime != Regime::Regular)
	//{
	//	if (m_regime == Regime::FreeExt && vx12.norm() >= L0)
	//	{
	//		this->m_vJVal.setZero();
	//		return;
	//	}
	//	if (m_regime == Regime::FreeCom && vx12.norm() <= L0)
	//	{
	//		this->m_vJVal.setZero();
	//		return;
	//	}
	//}

	//Real kS = this->computeK();

	//Real rh = this->m_vpmat[0]->getHRadius();
	//Real rw = this->m_vpmat[0]->getWRadius();
	//
	//Real E = 0;
	//if (this->m_vpmat[0]->getStretchK() != -1)
	Real E = this->m_vpmat[0]->getStretchK();
	//else E = this->m_vpmat[0]->getYoung();

	Real mJx[3][3];

#include "../../Maple/Code/RodEdge3DJacobianBasicOPT.mcg"

	// Store force vector

	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", "");

	MatrixXd mJxPart(4, 4);
	mJxPart.setZero();
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; ++j)
			mJxPart(i, j) = mJx[i][j];

	MatrixXd mJxFull = this->m_mDexDx.transpose()*mJxPart*this->m_mDexDx*(this->m_iv / this->m_vl0[0]);

	// Store triangular lower matrix

	this->m_vJVal.setZero();

	int count = 0;
	for (int i = 0; i < 6; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJxFull(i,j);
}

void DERStretchElement::update_DfxDp(const dVector& vx)
{
	Vector3d x1;
	Vector3d x2;
	for (int i = 0; i < 3; ++i)
	{
		x1(i) = vx[this->m_vidxx[0 + i]];
		x2(i) = vx[this->m_vidxx[3 + i]];
	}

	Vector3d vx12 = x2 - x1;
	const double* x12 = vx12.data();
	//Real L0 = this->m_vl0(0)*this->m_preStrain;

	Real L0 = this->m_vl0(0);

	//if (this->m_regime != Regime::Regular)
	//{
	//	if (m_regime == Regime::FreeExt && vx12.norm() >= L0)
	//	{
	//		this->m_vJVal.setZero();
	//		return;
	//	}
	//	if (m_regime == Regime::FreeCom && vx12.norm() <= L0)
	//	{
	//		this->m_vJVal.setZero();
	//		return;
	//	}
	//}

	//Real kS = this->computeK();

	//Real rh = this->m_vpmat[0]->getHRadius();
	//Real rw = this->m_vpmat[0]->getWRadius();
	//
	//Real E = 0;
	//if (this->m_vpmat[0]->getStretchK() != -1)
	Real E = this->m_vpmat[0]->getStretchK();
	//else E = this->m_vpmat[0]->getYoung();

	Real mJp[3][1];

#include "../../Maple/Code/RodEdge3DJacobianParBasicOPT.mcg"

	// Store force vector

	for (int i = 0; i < 3; ++i)
		if (!isfinite(mJp[i][0]))
			logSimu("In %s: Jacobian value is NAN\n", "");

	// Store matrix

	MatrixXd mJpPart(4, 1);
	for (int i = 0; i < 3; i++)
		mJpPart(i, 0) = mJp[i][0];
	mJpPart(3, 0) = 0.0;

	this->m_mDfxDp = this->m_mDexDx.transpose()*mJpPart*(this->m_iv/this->m_vl0[0]);
}

Real DERStretchElement::computeK() const
{
	Real rh = this->m_vpmat[0]->getHRadius();
	Real rw = this->m_vpmat[0]->getWRadius();
	Real E = this->m_vpmat[0]->getYoung();
	Real A = rh*rw*M_PI;
	return E*A;
}

void DERStretchElement::update_Rest(const VectorXd& vX)
{
	Vector3d x1 = Vector3d(vX[this->m_vidxx[0]], vX[this->m_vidxx[1]], vX[this->m_vidxx[2]]);
	Vector3d x2 = Vector3d(vX[this->m_vidxx[3]], vX[this->m_vidxx[4]], vX[this->m_vidxx[5]]);
	this->m_vl0(0) = (x2 - x1).norm();
}

void DERStretchElement::update_Mass(const VectorXd& vX)
{
	if (!this->m_addMass)
		return; // Ignore

	double M = M_PI*this->m_vpmat[0]->getWRadius()*
					this->m_vpmat[0]->getHRadius()*
					this->m_vpmat[0]->getDensity()*
					this->m_vl0(0);

	double avgr = 0.5*(this->m_vpmat[0]->getWRadius() + this->m_vpmat[0]->getHRadius());

	double nodeMass = 0.5*M;
	double edgeMass = avgr*M;

	// Set node mass value
	for (int i = 0; i < 6; ++i)
		this->m_vmx(i) = nodeMass;

	// Set edge inertia value
	this->m_vmx(6) = edgeMass;
}

void DERStretchElement::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	this->update_Energy(toSTL(vx), dVector(), dVector(), vector<Frame>(), vector<Frame>());
}

void DERStretchElement::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	this->update_Force(toSTL(vx), dVector(), dVector(), vector<Frame>(), vector<Frame>());
}

void DERStretchElement::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	this->update_Jacobian(toSTL(vx), dVector(), dVector(), vector<Frame>(), vector<Frame>());
}

void DERStretchElement::update_fv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
								  const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0)
{
	//throw new exception("Not implemented yet");
}

void DERStretchElement::update_fl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	//throw new exception("Not implemented yet");
}

void DERStretchElement::update_fr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	//throw new exception("Not implemented yet");
}

void DERStretchElement::update_DfxDv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
								     const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0)
{
	Vector3d x1;
	Vector3d x2;
	for (int i = 0; i < 3; ++i)
	{
		x1(i) = vxx[this->m_vidxx[0 + i]];
		x2(i) = vxx[this->m_vidxx[3 + i]];
	}

	Vector3d X1;
	Vector3d X2;
	for (int i = 0; i < 3; ++i)
	{
		X1(i) = vx0[this->m_vidxx[0 + i]];
		X2(i) = vx0[this->m_vidxx[3 + i]];
	}

	Vector3d vx12 = x2 - x1;
	Vector3d vX12 = X2 - X1;
	const double* x12 = vx12.data();
	const double* X12 = vX12.data();

	if (this->m_regime != Regime::Regular)
	{
		if (m_regime == Regime::FreeExt && vx12.norm() >= vX12.norm())
		{
			this->m_mDfxD0v.setZero();
			return;
		}
		if (m_regime == Regime::FreeCom && vx12.norm() <= vX12.norm())
		{
			this->m_mDfxD0v.setZero();
			return;
		}
	}

	Real rh = this->m_vpmat[0]->getHRadius();
	Real rw = this->m_vpmat[0]->getWRadius();
	Real E = this->m_vpmat[0]->getYoung();
	
	Real mJ0[3][3];
	
#include "../../Maple/Code/RodEdge3DJacobianRestNodesOPT.mcg"
	
	// Store force vector
	
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			if (!isfinite(mJ0[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", "");
	
	MatrixXd mJ0Part(4, 3);
	mJ0Part.setZero();
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; ++j)
			mJ0Part(i, j) = mJ0[i][j];

	this->m_mDfxD0v = this->m_mDexDx.transpose()*mJ0Part*this->m_mDevDv;
}

void DERStretchElement::update_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Vector3d x1;
	Vector3d x2;
	for (int i = 0; i < 3; ++i)
	{
		x1(i) = vx[this->m_vidxx[0 + i]];
		x2(i) = vx[this->m_vidxx[3 + i]];
	}

	Vector3d vx12 = x2 - x1;
	const double* x12 = vx12.data();
	Real L0 = this->m_vl0(0)*this->m_preStrain;

	if (this->m_regime != Regime::Regular)
	{
		if (m_regime == Regime::FreeExt && vx12.norm() >= L0)
		{
			this->m_mDfxDl.setZero();
			return;
		}
		if (m_regime == Regime::FreeCom && vx12.norm() <= L0)
		{
			this->m_mDfxDl.setZero();
			return;
		}
	}
	
	//Real kS = this->computeK();
	
	Real rh = this->m_vpmat[0]->getHRadius();
	Real rw = this->m_vpmat[0]->getWRadius();
	Real E = this->m_vpmat[0]->getYoung();
	
	Real mJl[3][1];
	
#include "../../Maple/Code/RodEdge3DJacobianLengthOPT.mcg"
	
	// Store force vector
	
	for (int i = 0; i < 3; ++i)
		if (!isfinite(mJl[i][0]))
			logSimu("In %s: Jacobian value is NAN\n", "");
	
	// Store matrix
	
	MatrixXd mJlPart(4, 1);
	for (int i = 0; i < 3; i++)
		mJlPart(i, 0) = mJl[i][0];
	mJlPart(3, 0) = 0.0;

	this->m_mDfxDl = this->m_mDexDx.transpose()*mJlPart;
}

void DERStretchElement::update_DfxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Vector3d x1;
	Vector3d x2;
	for (int i = 0; i < 3; ++i)
	{
		x1(i) = vx[this->m_vidxx[0 + i]];
		x2(i) = vx[this->m_vidxx[3 + i]];
	}

	Vector3d vx12 = x2 - x1;
	const double* x12 = vx12.data();
	Real L0 = this->m_vl0(0)*this->m_preStrain;

	if (this->m_regime != Regime::Regular)
	{
		if (m_regime == Regime::FreeExt && vx12.norm() >= L0)
		{
			this->m_mDfxDr.setZero();
			return;
		}
		if (m_regime == Regime::FreeCom && vx12.norm() <= L0)
		{
			this->m_mDfxDr.setZero();
			return;
		}
	}

	//Real kS = this->computeK();

	Real rh = this->m_vpmat[0]->getHRadius();
	Real rw = this->m_vpmat[0]->getWRadius();
	Real E = this->m_vpmat[0]->getYoung();

	Real mJr[3][2];

#include "../../Maple/Code/RodEdge3DJacobianRadiusOPT.mcg"

	// Store force vector

	for (int i = 0; i < 3; ++i)
		if (!isfinite(mJr[i][0]))
			logSimu("In %s: Jacobian value is NAN\n", "");

	// Store matrix

	MatrixXd mJrPart(4, 2);

	for (int i = 0; i < 3; i++)
	{
		mJrPart(i, 0) = mJr[i][0];
		mJrPart(i, 1) = mJr[i][1];
	}	
	mJrPart(3, 0) = 0.0;
	mJrPart(3, 1) = 0.0;

	this->m_mDfxDr = this->m_mDexDx.transpose()*mJrPart;
}

void DERStretchElement::update_DmxDv(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
									 const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0)
{
	if (!this->m_addMass)
		return; // Ignore

	Vector3d X1 = Vector3d(vx0[this->m_vidxx[0]], vx0[this->m_vidxx[1]], vx0[this->m_vidxx[2]]);
	Vector3d X2 = Vector3d(vx0[this->m_vidxx[3]], vx0[this->m_vidxx[4]], vx0[this->m_vidxx[5]]);

	double rw = this->m_vpmat[0]->getWRadius();
	double rh = this->m_vpmat[0]->getHRadius();
	double rho = this->m_vpmat[0]->getDensity();
	double avgr = 0.5*(rw + rh);

	Vector3d DmxDv = (X2 - X1).normalized()*M_PI*rw*rh*rho;
	Vector3d DmvDv = 0.5*DmxDv;
	Vector3d DmeDv = avgr*DmxDv;

	this->m_mDmxDv.resize(7, 6);
	this->m_mDmxDv.setZero();

	// Derivatives at nodes
	for (int i = 0; i < 6; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			this->m_mDmxDv(i, j + 0) = -DmvDv(j);
			this->m_mDmxDv(i, j + 3) = +DmvDv(j);
		}
	}

	// Derivatives at the edge
	for (int j = 0; j < 3; ++j)
	{
		this->m_mDmxDv(6, j + 0) = -DmeDv(j);
		this->m_mDmxDv(6, j + 3) = +DmeDv(j);
	}
}

void DERStretchElement::update_DmxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	if (!this->m_addMass)
		return; // Ignore

	double rw = this->m_vpmat[0]->getWRadius();
	double rh = this->m_vpmat[0]->getHRadius();
	double rho = this->m_vpmat[0]->getDensity();
	double avgr = 0.5*(rw + rh);

	double DmxDl = M_PI*rw*rh*rho;
	double DmvDl = 0.5*DmxDl;
	double DmeDl = avgr*DmxDl;

	this->m_mDmxDl.resize(7, 1);
	this->m_mDmxDl.setZero();

	// Derivatives at nodes
	for (int i = 0; i < 6; ++i)
		this->m_mDmxDl(i, 0) = DmvDl;

	// Derivatives at the edge
	this->m_mDmxDl(6, 0) = DmeDl;
}

void DERStretchElement::update_DmxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	if (!this->m_addMass)
		return; // Ignore

	double rw = this->m_vpmat[0]->getWRadius();
	double rh = this->m_vpmat[0]->getHRadius();
	double rho = this->m_vpmat[0]->getDensity();
	double avgr = 0.5*(rw + rh);

	double DmxDrw = this->m_vl0(0)*M_PI*rh*rho;
	double DmvDrw = 0.5*DmxDrw;
	double DmeDrw = avgr*DmxDrw;

	double DmxDrh = this->m_vl0(0)*M_PI*rw*rho;
	double DmvDrh = 0.5*DmxDrh;
	double DmeDrh = avgr*DmxDrh;

	this->m_mDmxDr.resize(7, 2);
	this->m_mDmxDr.setZero();

	// Derivatives at nodes
	for (int i = 0; i < 6; ++i)
	{
		this->m_mDmxDr(i, 0) = DmvDrw;
		this->m_mDmxDr(i, 1) = DmvDrh;
	}

	// Derivatives at the edge
	this->m_mDmxDr(6, 0) = DmeDrw;
	this->m_mDmxDr(6, 1) = DmeDrh;
}

void DERStretchElement::add_DmxDv(tVector& vDmxDv) const
{
	if (!this->m_addMass)
		return; // Ignore

	int nv = (int) this->m_vvinds.size();

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < nv; ++j)
		{
			for (int d = 0; d < 3; ++d)
			{
				vDmxDv.push_back(Triplet<Real>(this->m_vidxx[i], 3 * this->m_vvinds[j] + d, this->m_mDmxDv(i, 3 * j + d)));
			}
		}
	}
}

void DERStretchElement::add_DmxDl(tVector& vDmxDl) const
{
	if (!this->m_addMass)
		return; // Ignore

	int ne = (int) this->m_veinds.size();

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < ne; ++j)
		{
			vDmxDl.push_back(Triplet<Real>(this->m_vidxx[i], this->m_veinds[j], this->m_mDmxDl(i, j)));
		}
	}
}

void DERStretchElement::add_DmxDr(tVector& vDmxDr) const
{
	if (!this->m_addMass)
		return; // Ignore

	int ne = (int) this->m_veinds.size();

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < ne; ++j)
		{
			vDmxDr.push_back(Triplet<Real>(this->m_vidxx[i], 2 * this->m_veinds[j] + 0, this->m_mDmxDr(i, 2 * j + 0)));
			vDmxDr.push_back(Triplet<Real>(this->m_vidxx[i], 2 * this->m_veinds[j] + 1, this->m_mDmxDr(i, 2 * j + 1)));
		}
	}
}

void DERStretchElement::add_DfxDp(tVector& vDfxDp) const
{
	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		vDfxDp.push_back(Triplet<Real>(this->m_vidxx[i], 0, this->m_mDfxDp(i, 0)));
	}
}

Real DERStretchElement::testForceLocal(const VectorXd& vx, const VectorXd& vv)
{
	return SolidElement::testForceLocal(vx, vv);
}

Real DERStretchElement::testJacobianLocal(const VectorXd& vx, const VectorXd& vv)
{
	return SolidElement::testJacobianLocal(vx, vv);
}