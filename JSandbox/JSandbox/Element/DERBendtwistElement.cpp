/*=====================================================================================*/
/*!
/file		DERBendTwistElement.cpp
/author		jesusprod
/brief		Implementation of DERBendTwistElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/DERBendTwistElement.h>

// Support operations
#include <JSandbox/Maple/rodriguesRotation.h>
#include <JSandbox/Maple/parallelTransportNormalized.h>
#include <JSandbox/Maple/computeReferenceTwistChange.h>

// Energy, gradient, Hessian
#include <JSandbox/Maple/getBTEnergyOPT.h>
#include <JSandbox/Maple/getBTGradientOPT.h>
#include <JSandbox/Maple/getBTHessianOPT.h>
#include <JSandbox/Maple/getBTParamLengthJacobianOPT.h>
#include <JSandbox/Maple/getBTParamIsoRadiusJacobianOPT.h>
#include <JSandbox/Maple/getBTParamAniRadiusJacobianOPT.h>
#include <JSandbox/Maple/getBTParamRestNodesJacobianOPT.h>

DERBendTwistElement::DERBendTwistElement() : DERBaseElement(11, 11)
{
	this->m_vpmat.resize(2);
	this->m_vl0.resize(2);
	this->m_vl0.setZero();

	MatrixXd mDexDx(8, 11);
	MatrixXd mDevDv(6, 9);

	mDexDx.setZero();
	mDevDv.setZero();

	mDexDx(6, 9) = 1.0;
	mDexDx(7, 10) = 1.0;

	Matrix3d I; I.setIdentity();

	addBlock3x3From(0, 0, I * -1.0, mDexDx);
	addBlock3x3From(3, 3, I * -1.0, mDexDx);
	addBlock3x3From(0, 3, I, mDexDx);
	addBlock3x3From(3, 6, I, mDexDx);

	addBlock3x3From(0, 0, I * -1.0, mDevDv);
	addBlock3x3From(3, 3, I * -1.0, mDevDv);
	addBlock3x3From(0, 3, I, mDevDv);
	addBlock3x3From(3, 6, I, mDevDv);

	this->m_mDexDx = mDexDx.sparseView(EPS_POS);
	this->m_mDevDv = mDevDv.sparseView(EPS_POS);

	this->m_isSplit = false;
}

DERBendTwistElement::~DERBendTwistElement(void)
{
	// Nothing to do here...
}

void DERBendTwistElement::update_Rest(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Vector3d c0 = get3D(this->m_vvinds[0], vx);
	Vector3d c1 = get3D(this->m_vvinds[1], vx);
	Vector3d c2 = get3D(this->m_vvinds[2], vx);
	const Frame3d& f0 = vFm[this->m_veinds[0]];
	const Frame3d& f1 = vFm[this->m_veinds[1]];
	double et = va[this->m_veinds[0]];
	double ft = va[this->m_veinds[1]];
	double rt = vt[this->m_vvinds[1]];
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;

	this->computeRest(e, f, f0, f1, et, ft, rt);
}

void DERBendTwistElement::update_Mass(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	// Does not carry mass
	this->m_vmx.setZero();
}

void DERBendTwistElement::computeRest(const Vector3d &e, const Vector3d &f, const Frame3d &f0, const Frame3d &f1, double et, double ft, double rt)
{
	this->m_vl0.resize(2);
	this->m_vl0(0) = e.norm();
	this->m_vl0(1) = f.norm();
	this->m_K0 = this->computeKappa(e, f, f0, f1);
	this->m_t0 = ft - et + rt;
}

void DERBendTwistElement::update_Energy(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	if (this->m_isSplit)
	{
		this->m_energy = 0;
		return;
	}

	Vector3d c0 = get3D(this->m_vvinds[0], vx);
	Vector3d c1 = get3D(this->m_vvinds[1], vx);
	Vector3d c2 = get3D(this->m_vvinds[2], vx);
	Vector3d e1 = c1 - c0;
	Vector3d e2 = c2 - c1;

	double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	if (this->m_vpmat[0]->getBendingKw() != -1) vyoung[0] = this->m_vpmat[0]->getBendingKw();
	if (this->m_vpmat[1]->getBendingKh() != -1) vyoung[1] = this->m_vpmat[1]->getBendingKh();

	this->m_energy = getBTEnergyOPT(vshear, vyoung, vradw, vradh,
									this->m_vl0(0)*this->m_preStrain,
									this->m_vl0(1)*this->m_preStrain,
									this->m_K0.data(),
									this->m_t0,
									vFr[this->m_veinds[0]].nor.data(), vFr[this->m_veinds[0]].tan.data(),
									vFr[this->m_veinds[1]].nor.data(), vFr[this->m_veinds[1]].tan.data(),
									vt[this->m_vvinds[1]],
									e1.data(), e2.data(),
									va[this->m_veinds[0]], 
									va[this->m_veinds[1]]);
}

void DERBendTwistElement::update_Force(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	if (this->m_isSplit)
	{
		this->m_vfVal.setZero();
		return;
	}

	Vector3d c0 = get3D(this->m_vvinds[0], vx);
	Vector3d c1 = get3D(this->m_vvinds[1], vx);
	Vector3d c2 = get3D(this->m_vvinds[2], vx);
	Vector3d e1 = c1 - c0;
	Vector3d e2 = c2 - c1;

	double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	if (this->m_vpmat[0]->getBendingKw() != -1) vyoung[0] = this->m_vpmat[0]->getBendingKw();
	if (this->m_vpmat[1]->getBendingKh() != -1) vyoung[1] = this->m_vpmat[1]->getBendingKh();

	VectorXd vg(8);

	getBTGradientOPT(vshear, vyoung, vradw, vradh,
					 this->m_vl0(0)*this->m_preStrain,
					 this->m_vl0(1)*this->m_preStrain,
					 this->m_K0.data(),
					 this->m_t0,
					 vFr[this->m_veinds[0]].nor.data(), vFr[this->m_veinds[0]].tan.data(),
					 vFr[this->m_veinds[1]].nor.data(), vFr[this->m_veinds[1]].tan.data(),
					 vt[this->m_vvinds[1]],
					 e1.data(), e2.data(),
					 va[this->m_veinds[0]],
					 va[this->m_veinds[1]],
					 vg.data());

	this->m_vfVal = -this->m_mDexDx.transpose()*vg;
}

void DERBendTwistElement::update_Jacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	if (this->m_isSplit)
	{
		this->m_vJVal.setZero();
		return;
	}

	Vector3d c0 = get3D(this->m_vvinds[0], vx);
	Vector3d c1 = get3D(this->m_vvinds[1], vx);
	Vector3d c2 = get3D(this->m_vvinds[2], vx);
	Vector3d e1 = c1 - c0;
	Vector3d e2 = c2 - c1;

	double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	if (this->m_vpmat[0]->getBendingKw() != -1) vyoung[0] = this->m_vpmat[0]->getBendingKw();
	if (this->m_vpmat[1]->getBendingKh() != -1) vyoung[1] = this->m_vpmat[1]->getBendingKh();

	MatrixXd mH(8, 8);

	getBTHessianOPT(vshear, vyoung, vradw, vradh,
					this->m_vl0(0)*this->m_preStrain,
					this->m_vl0(1)*this->m_preStrain,
					this->m_K0.data(),
					this->m_t0,
					vFr[this->m_veinds[0]].nor.data(), vFr[this->m_veinds[0]].tan.data(),
					vFr[this->m_veinds[1]].nor.data(), vFr[this->m_veinds[1]].tan.data(),
					vt[this->m_vvinds[1]],
					e1.data(), 
					e2.data(),
					va[this->m_veinds[0]],
					va[this->m_veinds[1]],
					mH.data());

	MatrixXd mJFull = -1*this->m_mDexDx.transpose()*mH*this->m_mDexDx;

	int count = 0;
	for (int i = 0; i < 11; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJFull(i, j);
}


void DERBendTwistElement::updateStateForTesting(dVector& vx, dVector& va, dVector& vt, vector<Frame3d>& vFr, vector<Frame3d>& vFm)
{
	Vector3d c0 = get3D(this->m_vvinds[0], vx);
	Vector3d c1 = get3D(this->m_vvinds[1], vx);
	Vector3d c2 = get3D(this->m_vvinds[2], vx);
	Vector3d t0 = (c1 - c0).normalized();
	Vector3d t1 = (c2 - c1).normalized();

	// Update frame 0

	Vector3d n0;
	parallelTransportNormalized(vFr[this->m_veinds[0]].tan.data(), t0.data(), vFr[this->m_veinds[0]].nor.data(), n0.data());
	vFr[this->m_veinds[0]].tan = t0;
	vFr[this->m_veinds[0]].nor = n0;
	vFr[this->m_veinds[0]].bin = t0.cross(n0);

	// Update frame 1

	Vector3d n1;
	parallelTransportNormalized(vFr[this->m_veinds[1]].tan.data(), t1.data(), vFr[this->m_veinds[1]].nor.data(), n1.data());
	vFr[this->m_veinds[1]].tan = t1;
	vFr[this->m_veinds[1]].nor = n1;
	vFr[this->m_veinds[1]].bin = t1.cross(n1);

	// Update ref twist

	vt[this->m_vvinds[1]] += computeReferenceTwistChange(t0.data(), n0.data(), t1.data(), n1.data(), vt[this->m_vvinds[1]]);
}

void DERBendTwistElement::testForceChanging(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Real EPS = 1e-3;

	dVector vxFD = vx;
	dVector vaFD = va;
	dVector vtFD = vt;
	vector<Frame3d> vFrFD = vFr;
	vector<Frame3d> vFmFD = vFm;
	
	// Node Position

	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			// Yes Meta
			vxFD = vx;
			vaFD = va;
			vtFD = vt;
			vFrFD = vFr;
			vFmFD = vFm;
			vxFD[3 * this->m_vvinds[i] + j] += EPS;
			updateStateForTesting(vxFD, vaFD, vtFD, vFrFD, vFmFD);
			this->update_Force(vxFD, vaFD, vtFD, vFrFD, vFmFD);
			VectorXd fYes = this->getLocalForce();

			// No Meta
			vxFD = vx;
			vaFD = va;
			vtFD = vt;
			vFrFD = vFr;
			vFmFD = vFm;
			vxFD[3 * this->m_vvinds[i] + j] += EPS;
			this->update_Force(vxFD, vaFD, vtFD, vFrFD, vFmFD);
			VectorXd fNo = this->getLocalForce();

			logSimu("[INFO] Force difference: %.12f", (fYes - fNo).norm());
		}
	}

	// Angle value

	// Con position
	for (int i = 0; i < 2; ++i)
	{
		// Yes Meta
		vxFD = vx;
		vaFD = va;
		vtFD = vt;
		vFrFD = vFr;
		vFmFD = vFm;
		vaFD[this->m_veinds[i]] += EPS;
		updateStateForTesting(vxFD, vaFD, vtFD, vFrFD, vFmFD);
		this->update_Force(vxFD, vaFD, vtFD, vFrFD, vFmFD);
		VectorXd fYes = this->getLocalForce();

		// Yes Meta
		vxFD = vx;
		vaFD = va;
		vtFD = vt;
		vFrFD = vFr;
		vFmFD = vFm;
		vaFD[this->m_veinds[i]] += EPS;
		this->update_Force(vxFD, vaFD, vtFD, vFrFD, vFmFD);
		VectorXd fNo = this->getLocalForce();

		logSimu("[INFO] Force difference: %.12f", (fYes - fNo).norm());
	}
}

void DERBendTwistElement::testEnergyChanging(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	Real EPS = 1e-3;

	dVector vxFD = vx;
	dVector vaFD = va;
	dVector vtFD = vt;
	vector<Frame3d> vFrFD = vFr;
	vector<Frame3d> vFmFD = vFm;

	// Node Position

	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			// Yes Meta
			vxFD = vx;
			vaFD = va;
			vtFD = vt;
			vFrFD = vFr;
			vFmFD = vFm;
			vxFD[3 * this->m_vvinds[i] + j] += EPS;
			updateStateForTesting(vxFD, vaFD, vtFD, vFrFD, vFmFD);
			this->update_Energy(vxFD, vaFD, vtFD, vFrFD, vFmFD);
			Real eYes = this->getEnergyIntegral();

			// No Meta
			vxFD = vx;
			vaFD = va;
			vtFD = vt;
			vFrFD = vFr;
			vFmFD = vFm;
			vxFD[3 * this->m_vvinds[i] + j] += EPS;
			this->update_Energy(vxFD, vaFD, vtFD, vFrFD, vFmFD);
			Real eNo = this->getEnergyIntegral();

			logSimu("[INFO] Energy difference: %.12f", eYes - eNo);
		}
	}

	// Angle value

	// Con position
	for (int i = 0; i < 2; ++i)
	{
		// Yes Meta
		vxFD = vx;
		vaFD = va;
		vtFD = vt;
		vFrFD = vFr;
		vFmFD = vFm;
		vaFD[this->m_veinds[i]] += EPS;
		updateStateForTesting(vxFD, vaFD, vtFD, vFrFD, vFmFD);
		this->update_Energy(vxFD, vaFD, vtFD, vFrFD, vFmFD);
		Real eYes = this->getEnergyIntegral();

		// Yes Meta
		vxFD = vx;
		vaFD = va;
		vtFD = vt;
		vFrFD = vFr;
		vFmFD = vFm;
		vaFD[this->m_veinds[i]] += EPS;
		this->update_Energy(vxFD, vaFD, vtFD, vFrFD, vFmFD);
		Real eNo = this->getEnergyIntegral();

		logSimu("[INFO] Energy difference: %.12f", eYes - eNo);
	}
}

void DERBendTwistElement::update_f0v(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
									 const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0)
{
	// TODO
}

void DERBendTwistElement::update_fl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	// TODO
}

void DERBendTwistElement::update_fr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	// TODO
}

void DERBendTwistElement::update_DfxD0v(const dVector& vxx, const dVector& vax, const dVector& vtx, const vector<Frame3d>& vFrx, const vector<Frame3d>& vFmx,
									    const dVector& vx0, const dVector& va0, const dVector& vt0, const vector<Frame3d>& vFr0, const vector<Frame3d>& vFm0)
{
	if (this->m_isSplit)
	{
		this->m_mDfxD0v.resize(11, 9);
		this->m_mDfxD0v.setZero();
		return;
	}

	MatrixXd mDfeD0e(6, 8);

	Vector3d x1 = get3D(this->m_vvinds[0], vxx);
	Vector3d x2 = get3D(this->m_vvinds[1], vxx);
	Vector3d x3 = get3D(this->m_vvinds[2], vxx);
	Vector3d e1 = x2 - x1;
	Vector3d e2 = x3 - x2;
	Vector3d X1 = get3D(this->m_vvinds[0], vx0);
	Vector3d X2 = get3D(this->m_vvinds[1], vx0);
	Vector3d X3 = get3D(this->m_vvinds[2], vx0);
	Vector3d E1 = X2 - X1;
	Vector3d E2 = X3 - X2;

	double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	if (this->m_vpmat[0]->getBendingKw() != -1) vyoung[0] = this->m_vpmat[0]->getBendingKw();
	if (this->m_vpmat[1]->getBendingKh() != -1) vyoung[1] = this->m_vpmat[1]->getBendingKh();

	getBTParamRestNodesJacobianOPT(vshear, vyoung, vradw, vradh,
								   vFrx[this->m_veinds[0]].nor.data(), vFrx[this->m_veinds[0]].tan.data(), 
								   vFrx[this->m_veinds[1]].nor.data(), vFrx[this->m_veinds[1]].tan.data(),
								   vtx[this->m_vvinds[1]],
								   e1.data(), 
								   e2.data(),
								   vax[this->m_veinds[0]], 
								   vax[this->m_veinds[1]],
								   vFr0[this->m_veinds[0]].nor.data(), vFr0[this->m_veinds[0]].tan.data(),
								   vFr0[this->m_veinds[1]].nor.data(), vFr0[this->m_veinds[1]].tan.data(),
								   vt0[this->m_vvinds[1]],
								   E1.data(), 
								   E2.data(),
								   va0[this->m_veinds[0]], 
								   va0[this->m_veinds[1]],
								   mDfeD0e.data());

	this->m_mDfxD0v = -this->m_mDexDx.transpose()*mDfeD0e.transpose()*this->m_mDevDv;
}

void DERBendTwistElement::update_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	if (this->m_isSplit)
	{
		this->m_mDfxDl.resize(11, 2);
		this->m_mDfxDl.setZero();
		return;
	}

	MatrixXd mDfeDl(2, 8);

	Vector3d x1 = get3D(this->m_vvinds[0], vx);
	Vector3d x2 = get3D(this->m_vvinds[1], vx);
	Vector3d x3 = get3D(this->m_vvinds[2], vx);
	Vector3d e1 = x2 - x1;
	Vector3d e2 = x3 - x2;

	double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	if (this->m_vpmat[0]->getBendingKw() != -1) vyoung[0] = this->m_vpmat[0]->getBendingKw();
	if (this->m_vpmat[1]->getBendingKh() != -1) vyoung[1] = this->m_vpmat[1]->getBendingKh();

	getBTParamLengthJacobianOPT(vshear, vyoung, vradw, vradh,
								this->m_vl0(0)*this->m_preStrain,
								this->m_vl0(1)*this->m_preStrain,
								this->m_K0.data(),
								this->m_t0,
								vFr[this->m_veinds[0]].nor.data(), vFr[this->m_veinds[0]].tan.data(),
								vFr[this->m_veinds[1]].nor.data(), vFr[this->m_veinds[1]].tan.data(),
								vt[this->m_vvinds[1]],
								e1.data(), 
								e2.data(),
								va[this->m_veinds[0]], 
								va[this->m_veinds[1]],
								mDfeDl.data());

	this->m_mDfxDl = -this->m_mDexDx.transpose()*mDfeDl.transpose();
}

void DERBendTwistElement::update_DfxDr(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm)
{
	if (this->m_isSplit)
	{
		this->m_mDfxDr.resize(11, 4);
		this->m_mDfxDr.setZero();
		return;
	}

	//MatrixXd mDfeDr(4, 8);

	//Vector3d x1 = get3D(this->m_vvinds[0], vx);
	//Vector3d x2 = get3D(this->m_vvinds[1], vx);
	//Vector3d x3 = get3D(this->m_vvinds[2], vx);
	//Vector3d e1 = x2 - x1;
	//Vector3d e2 = x3 - x2;

	//double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	//double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	//double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	//double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	//getBTParamAniRadiusJacobianOPT(vshear, vyoung, vradw, vradh,
	//							   this->m_vl0(0)*this->m_preStrain,
	//							   this->m_vl0(1)*this->m_preStrain,
	//							   this->m_K0.data(),
	//							   this->m_t0,
	//							   vFr[this->m_veinds[0]].nor.data(), vFr[this->m_veinds[0]].tan.data(),
	//							   vFr[this->m_veinds[1]].nor.data(), vFr[this->m_veinds[1]].tan.data(),
	//							   vt[this->m_vvinds[1]],
	//							   e1.data(), 
	//							   e2.data(),
	//							   va[this->m_veinds[0]],
	//							   va[this->m_veinds[1]],
	//							   mDfeDr.data());

	//MatrixXd mDfeDrw(2, 8);
	//MatrixXd mDfeDrh(2, 8);
	//for (int i = 0; i < 8; ++i)
	//	for (int j = 0; j < 2; ++j)
	//	{
	//	mDfeDrw(j, i) = mDfeDr(2*j+0, i);
	//	mDfeDrh(j, i) = mDfeDr(2*j+1, i);
	//	}

	//this->m_mDfxDr = -this->m_mDexDx.transpose()*mDfeDrw.transpose()
	//				 -this->m_mDexDx.transpose()*mDfeDrh.transpose();

	MatrixXd mDfeDr(4, 8);

	Vector3d x1 = get3D(this->m_vvinds[0], vx);
	Vector3d x2 = get3D(this->m_vvinds[1], vx);
	Vector3d x3 = get3D(this->m_vvinds[2], vx);
	Vector3d e1 = x2 - x1;
	Vector3d e2 = x3 - x2;

	double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	if (this->m_vpmat[0]->getBendingKw() != -1) vyoung[0] = this->m_vpmat[0]->getBendingKw();
	if (this->m_vpmat[1]->getBendingKh() != -1) vyoung[1] = this->m_vpmat[1]->getBendingKh();

	getBTParamAniRadiusJacobianOPT(vshear, vyoung, vradw, vradh,
								   this->m_vl0(0)*this->m_preStrain,
								   this->m_vl0(1)*this->m_preStrain,
								   this->m_K0.data(),
								   this->m_t0,
								   vFr[this->m_veinds[0]].nor.data(), vFr[this->m_veinds[0]].tan.data(),
								   vFr[this->m_veinds[1]].nor.data(), vFr[this->m_veinds[1]].tan.data(),
								   vt[this->m_vvinds[1]],
								   e1.data(),
								   e2.data(),
								   va[this->m_veinds[0]],
								   va[this->m_veinds[1]],
								   mDfeDr.data());

	this->m_mDfxDr = -this->m_mDexDx.transpose()*mDfeDr.transpose();
}

Real DERBendTwistElement::computeEnergy(const Vector3d& x1, const Vector3d& x2, const Vector3d& x3, const Frame3d& eF, const Frame3d& fF, double et, double ft, double rt)
{
	//double l0 = this->m_vl0(0)*this->m_preStrain;
	//double l1 = this->m_vl0(1)*this->m_preStrain;
	//double K0[2] = { this->m_K0(0), this->m_K0(1) };
	//double eNor[3] = { eF.nor(0), eF.nor(1), eF.nor(2) };
	//double eTan[3] = { eF.tan(0), eF.tan(1), eF.tan(2) };
	//double fNor[3] = { fF.nor(0), fF.nor(1), fF.nor(2) };
	//double fTan[3] = { fF.tan(0), fF.tan(1), fF.tan(2) };
	//double c1[3] = { x1(0), x1(1), x1(2) };
	//double c2[3] = { x2(0), x2(1), x2(2) };
	//double c3[3] = { x3(0), x3(1), x3(2) };
	//double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	//double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	//double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	//double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	//return getBTEnergy(vshear, vyoung, vradh, vradw, l0, l1, K0, this->m_t0, eNor, eTan, fNor, fTan, rt, c1, c2, c3, et, ft);

	return -1;
}

void DERBendTwistElement::computeForce(const Vector3d& x1, const Vector3d& x2, const Vector3d& x3, const Frame3d& eF, const Frame3d& fF, double et, double ft, double rt, VectorXd& vf)
{
	//double l0 = this->m_vl0(0)*this->m_preStrain;
	//double l1 = this->m_vl0(1)*this->m_preStrain;
	//double K0[2] = { this->m_K0(0), this->m_K0(1) };
	//double eNor[3] = { eF.nor(0), eF.nor(1), eF.nor(2) };
	//double eTan[3] = { eF.tan(0), eF.tan(1), eF.tan(2) };
	//double fNor[3] = { fF.nor(0), fF.nor(1), fF.nor(2) };
	//double fTan[3] = { fF.tan(0), fF.tan(1), fF.tan(2) };
	//double c1[3] = { x1(0), x1(1), x1(2) };
	//double c2[3] = { x2(0), x2(1), x2(2) };
	//double c3[3] = { x3(0), x3(1), x3(2) };
	//double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	//double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	//double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	//double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	//double vgradient[11];

	//getBTGradient(vshear, vyoung, vradh, vradw, l0, l1, K0, this->m_t0, eNor, eTan, fNor, fTan, rt, c1, c2, c3, et, ft, vgradient);

	//vf.resize(11);
	//for (int i = 0; i < 11; ++i)
	//	vf[i] = -vgradient[i];
}

void DERBendTwistElement::computeJacobian(const Vector3d& x1, const Vector3d& x2, const Vector3d& x3, const Frame3d& eF, const Frame3d& fF, double et, double ft, double rt, MatrixXd& mJ)
{
	//double l0 = this->m_vl0(0)*this->m_preStrain;
	//double l1 = this->m_vl0(1)*this->m_preStrain;
	//double K0[2] = { this->m_K0(0), this->m_K0(1) };
	//double eNor[3] = { eF.nor(0), eF.nor(1), eF.nor(2) };
	//double eTan[3] = { eF.tan(0), eF.tan(1), eF.tan(2) };
	//double fNor[3] = { fF.nor(0), fF.nor(1), fF.nor(2) };
	//double fTan[3] = { fF.tan(0), fF.tan(1), fF.tan(2) };
	//double c1[3] = { x1(0), x1(1), x1(2) };
	//double c2[3] = { x2(0), x2(1), x2(2) };
	//double c3[3] = { x3(0), x3(1), x3(2) };
	//double vshear[2] = { this->m_vpmat[0]->getShear(), this->m_vpmat[1]->getShear() };
	//double vyoung[2] = { this->m_vpmat[0]->getYoung(), this->m_vpmat[1]->getYoung() };
	//double vradh[2] = { this->m_vpmat[0]->getHRadius(), this->m_vpmat[1]->getHRadius() };
	//double vradw[2] = { this->m_vpmat[0]->getWRadius(), this->m_vpmat[1]->getWRadius() };

	//double vhessian[121];

	//getBTHessian(vshear, vyoung, vradh, vradw, l0, l1, K0, this->m_t0, eNor, eTan, fNor, fTan, rt, c1, c2, c3, et, ft, vhessian);

	//int count = 0;
	//mJ.resize(11, 11);
	//for (int i = 0; i < 11; ++i)
	//	for (int j = 0; j < 11; ++j)
	//		mJ(i,j) = -vhessian[count++];
}

Vector3d DERBendTwistElement::computeKB(const Vector3d& e, const Vector3d& f) const
{
	Vector3d t0 = e.normalized();
	Vector3d t1 = f.normalized();
	return (t0.cross(t1)) * 2 * (1 / (1 + t0.dot(t1)));
}

Real DERBendTwistElement::computeVertexLength(const Vector3d& e, const Vector3d& f) const
{
	return (e.norm() + f.norm())*0.5;
}

VectorXd DERBendTwistElement::computeKappa(const Vector3d& e, const Vector3d& f, const Frame3d& f0, const Frame3d& f1) const
{
	Vector3d KB = this->computeKB(e, f);
	Vector3d m1e = f0.bin;
	Vector3d m1f = f1.bin;
	Vector3d m2e = f0.nor;
	Vector3d m2f = f1.nor;
	VectorXd kappa(2);
	kappa[0] = (m1e + m1f).dot(KB) * 0.5;
	kappa[1] = (m2e + m2f).dot(KB) * -0.5;
	return kappa;
}

Real DERBendTwistElement::computeBendingStiffness() const
{
	MatrixXd B(2, 2);

	B.setZero();
	Real Y = 0.5*(this->m_vpmat[0]->getYoung() + this->m_vpmat[1]->getYoung());
	Real w = 0.5*(this->m_vpmat[0]->getWRadius() + this->m_vpmat[1]->getWRadius());
	Real h = 0.5*(this->m_vpmat[0]->getHRadius() + this->m_vpmat[1]->getHRadius());
	Real A = M_PI * w * h;
	B(0, 0) = w * w;
	B(1, 1) = h * h;
	B = (0.25 * Y * A) * B;

	return B(0,0); // TODO improve model to account for anisotropic effects
}

Real DERBendTwistElement::computeTwistStiffness() const
{
	Real B = 0;

	Real S = 0.5*(this->m_vpmat[0]->getShear() + this->m_vpmat[1]->getShear());
	Real w = 0.5*(this->m_vpmat[0]->getWRadius() + this->m_vpmat[1]->getWRadius());
	Real h = 0.5*(this->m_vpmat[0]->getHRadius() + this->m_vpmat[1]->getHRadius());
	Real A = M_PI * w * h;
	B = 0.25 * S * A * (w * w + h * h);

	return B;
}