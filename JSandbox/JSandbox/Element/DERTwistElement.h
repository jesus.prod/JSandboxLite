/*=====================================================================================*/
/*!
/file		DERTwistElement.h
/author		jesusprod
/brief		Implementation of DERTwistElement.h.
*/
/*=====================================================================================*/

#ifndef DER_TWIST_ELEMENT_H
#define DER_TWIST_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/DERBaseElement.h>

class DERTwistElement : public DERBaseElement
{
public:
	DERTwistElement();
	~DERTwistElement();

	virtual void update_Rest(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Mass(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual void update_Energy(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Force(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	virtual void update_Jacobian(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual Real computeEnergy(Real d0, Real d1, Real rt);
	virtual void computeForce(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt, VectorXd& vf);
	virtual void computeJacobian(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt, MatrixXd& mJ);
	virtual void computeRest(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt);

	virtual Real computeB() const;
	virtual Vector3d computeKB(const Vector3d& e, const Vector3d& f) const;
	virtual void computeGradTwist(const Vector3d& e, const Vector3d& f, VectorXd& gT) const;
	virtual void computeHessTwist(const Vector3d& e, const Vector3d& f, MatrixXd& hT) const;
	virtual Real computeVertexLength(const Vector3d& e, const Vector3d& f) const;
	virtual Real computeVertexTwist(Real d0, Real d1, Real rt) const;

	void update_fl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);
	void update_DfxDl(const dVector& vx, const dVector& va, const dVector& vt, const vector<Frame3d>& vFr, const vector<Frame3d>& vFm);

	virtual Real getIntegrationVolume() const { return this->m_l0; }

	Real getRestVertexLength() const { return this->m_l0; }
	void setRestVertexLength(Real l0) { this->m_l0 = l0; }

	Real getRestVertexTwist() const { return this->m_t0; }
	void setRestVertexTwist(Real t0) { this->m_t0 = t0; }

	Real getConstant() const;

protected:

	MatrixXd m_DeDv; // Transmission
	Real m_l0; // Rest vertex length 
	Real m_t0; // Rest vertex twist

};

#endif