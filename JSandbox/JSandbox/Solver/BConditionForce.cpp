/*=====================================================================================*/
/*!
/file		BConditionForce.cpp
/author		jesusprod
/brief		Implementation of BConditionForce.h
*/
/*=====================================================================================*/

#include <JSandbox/Solver/BConditionForce.h>

BConditionForce::BConditionForce()
{
	this->m_maxStep = 1;
	this->m_curStep = 1;
}

BConditionForce::~BConditionForce()
{
	// Nothing to do here...
}

Real BConditionForce::getEnergy(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv) const
{
	int N = pModel->getNumSimDOF_x();

	assert((int)vx.size() == N);
	assert((int)vv.size() == N);

	const VectorXd& vgOri = pModel->getGravity();

	VectorXd vfApplied(N);
	vfApplied.setZero();

	pModel->setGravity(this->m_vf);
	pModel->add_vgravity(vfApplied);

	vfApplied *= this->getLoadingFactor();

	double energy = 0;
	for (int i = 0; i < N; ++i)
		energy -= vfApplied(i)*vx(i);

	pModel->setGravity(vgOri);

	return energy;
}

void BConditionForce::addForce(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const
{
	int N = pModel->getNumSimDOF_x();

	assert((int)vx.size() == N);
	assert((int)vv.size() == N);
	assert((int)vf.size() == N);

	const VectorXd& vgOri = pModel->getGravity();

	VectorXd vfApplied(N);
	vfApplied.setZero();

	pModel->setGravity(this->m_vf);
	pModel->add_vgravity(vfApplied);

	vf += vfApplied*this->getLoadingFactor();

	pModel->setGravity(vgOri);
}