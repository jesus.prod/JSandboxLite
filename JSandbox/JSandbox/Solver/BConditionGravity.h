/*=====================================================================================*/
/*!
/file		BConditionGravity.h
/author		jesusprod
/brief		Gravity boundary condition.
*/
/*=====================================================================================*/

#ifndef BCONDITION_GRAVITY_H
#define BCONDITION_GRAVITY_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

#include <JSandbox/Solver/BCondition.h>

#include <JSandbox/Model/SolidModel.h>

class   BConditionGravity : public BCondition
{
public:

	BConditionGravity();

	virtual ~BConditionGravity();

	// <BCondition>

	virtual Real getEnergy(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv) const;
	virtual void addForce(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const;

	// </BCondition>

	virtual void setGravity(const VectorXd& vg) { this->m_vg = vg; }
	virtual const VectorXd& getGravity() const { return this->m_vg; }

	VectorXd m_vg;

};

#endif