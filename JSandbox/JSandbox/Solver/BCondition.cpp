/*=====================================================================================*/
/*! 
/file		BCondition.cpp
/author		jesusprod
/brief		Implementation of BCondition.h
 */
/*=====================================================================================*/

#include <JSandbox/Solver/BCondition.h>

BCondition::BCondition()
{
	this->m_curStep = 0;
	this->m_maxStep = 1;
	this->m_reversed = false;
}

BCondition::~BCondition()
{
	// Nothing to do here...
}

Real BCondition::getEnergy(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv) const
{
	return 0.0;
}

void BCondition::addForce(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const
{
	// Default
}

void BCondition::addJacobian(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, tVector& vJ) const
{
	// Default
}

int BCondition::getNumNonZeros_Jacobian() const
{
	return 0;
}

void BCondition::constrain(SolidModel* pModel) const
{
	// Default
}

void BCondition::constrain(SolidModel* pModel, VectorXd& vf) const
{
	// Default
}

void BCondition::constrain(SolidModel* pModel, tVector& vJ) const
{
	// Default
}

int BCondition::advanceBoundary()
{
	if (this->m_curStep < this->m_maxStep)
		this->m_curStep++; // Advance BC
	return this->m_curStep;
}

int BCondition::getCurrentStep() const
{
	return this->m_curStep;
}

void BCondition::setCurrentStep(int s)
{
	assert(s >= 0);
	m_curStep = s;

	if (m_curStep > m_maxStep)
		m_curStep = m_maxStep;
}

int BCondition::getMaximumStep() const
{
	return m_maxStep;
}

void BCondition::setMaximumStep(int s)
{
	assert(s >= 0);
	m_maxStep = s;
}

bool BCondition::isFullyLoaded() const
{
	return m_curStep == m_maxStep;
}

Real BCondition::getLoadingFactor() const
{
	Real alpha = (Real)m_curStep/(Real)m_maxStep;

	// Return reverse?

	if (this->m_reversed)
		return 1 - alpha;
	else return alpha;
}