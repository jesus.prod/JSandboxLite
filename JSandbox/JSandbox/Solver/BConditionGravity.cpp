/*=====================================================================================*/
/*!
/file		BConditionGravity.cpp
/author		jesusprod
/brief		Implementation of BConditionGravity.h
*/
/*=====================================================================================*/

#include <JSandbox/Solver/BConditionGravity.h>

BConditionGravity::BConditionGravity()
{
	this->m_maxStep = 1;
	this->m_curStep = 1;
}

BConditionGravity::~BConditionGravity()
{
	// Nothing to do here...
}

Real BConditionGravity::getEnergy(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv) const
{
	int N = pModel->getNumSimDOF_x();

	assert((int)vx.size() == N);
	assert((int)vv.size() == N);

	VectorXd vf(N);
	vf.setZero();

	pModel->addGravity(this->m_vg, vf, NULL);

	double energy = 0;

	for (int i = 0; i < N; ++i)
		energy -= vf(i)*vx(i);

	return energy;
}

void BConditionGravity::addForce(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const
{
	int N = pModel->getNumSimDOF_x();

	assert((int)vx.size() == N);
	assert((int)vv.size() == N);
	assert((int)vf.size() == N);

	pModel->addGravity(this->m_vg, vf, NULL);
}