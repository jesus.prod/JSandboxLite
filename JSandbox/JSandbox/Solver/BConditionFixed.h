/*=====================================================================================*/
/*!
/file		BConditionFixed.h
/author		jesusprod
/brief		Fixed values boundary condition.
*/
/*=====================================================================================*/

#ifndef BCONDITION_FIXED_H
#define BCONDITION_FIXED_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

#include <JSandbox/Solver/BCondition.h>
#include <JSandbox/Model/SolidModel.h>

class   BConditionFixed : public BCondition
{

public:

	BConditionFixed();

	virtual ~BConditionFixed();

	// <BCondition>

	virtual void constrain(SolidModel* pModel) const;
	virtual void constrain(SolidModel* pModel, VectorXd& vf) const;
	virtual void constrain(SolidModel* pModel, tVector& vJ) const;

	// </BCondition>

	virtual int getNumber() const { return (int) this->m_vidx.size(); }

	virtual VectorXi getIndices() const { return this->m_vidx; }
	virtual VectorXd getValues() const { return this->m_vval; }

	virtual void setIndices(const VectorXi& vi) { this->m_vidx = vi; }
	virtual void setValues(const VectorXd& vd) { this->m_vval = vd; }

	virtual void setIsDefault(bool def) { this->m_default = def; }
	virtual bool getIsDefault() const { return this->m_default; }

	virtual void initializeIndividual(const iVector& vidx);
	virtual void initializeNodal(int n, const iVector& vidx);

	virtual void updateStencil(SolidModel* pModel);

protected:

	VectorXi m_vidx;
	VectorXd m_vval;
	bool m_default;

	bVector m_vstencil;

};

#endif