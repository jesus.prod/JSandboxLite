/*=====================================================================================*/
/*! 
/file		SolverGenericDynSta.cpp
/author		jesusprod
/brief		Implementation of SolverGenericDynSta.h
 */
/*=====================================================================================*/

#include <JSandbox/Solver/SolverGenericDynSta.h>

SolverGenericDynSta::SolverGenericDynSta() : 
m_fixResTimer(10, "FIX_SOL_RES"),
m_fixMatTimer(10, "FIX_SOL_MAT")
{ 
	// Nothing to do here

	m_fixResTimer.initialize();
	m_fixMatTimer.initialize();
}

SolverGenericDynSta::~SolverGenericDynSta()
{ 
	// Nothing to do here
}

Real SolverGenericDynSta::getPotential(const VectorXd& vx, const VectorXd& vv)
{
	// Compute U(x) = 1/2 * h^2 * a^T*M*a + V_int(x) + V_ext(x)
	// Here, V_int(x) and V_ext(x) are escalar functions such that
	// dV_int/dx = f_int and dV_ext/dx = f_ext. This potentia can 
	// serve as a function to minimize in order to solve the 
	// dynamic/quasistatic problems:
	//
	// dU(x)/dx = Ma - f_int - f_ext = 0

	Real potential = 0;
	potential += this->m_pSM->getEnergy();
	potential += this->getBoundaryEnergy(vx, vv);

	if (!this->m_isStatic) // Add dynamic contribution
	{
		Real dt2 = this->m_dt*this->m_dt;
		VectorXd va = (vx - this->m_vx)/dt2 - this->m_vv/this->m_dt;
		potential += dt2 * 0.5 * va.dot((va.cwiseProduct(this->m_vm)));
	}

	return potential;
}

void SolverGenericDynSta::getResidual(const VectorXd& vx, const VectorXd& vv, VectorXd& vrhs)
{
	m_updateResTimer.restart();

	int N = this->m_pSM->getNumSimDOF_x();

	vrhs.resize(N);
	vrhs.setZero();

	//this->m_pSM->testForceLocal();
	//this->m_pSM->testForceGlobal();

	this->m_pSM->addForce(vrhs, NULL);  
	this->addBoundaryForce(vx, vv, vrhs);

	if (!this->m_isStatic)
	{
		Real dt2 = this->m_dt*this->m_dt;
		VectorXd va = (vx - this->m_vx)/dt2 - this->m_vv/this->m_dt;
		vrhs -= va.cwiseProduct(this->m_vm); // Add dynamic part M*a
	}

	m_fixResTimer.restart();

	this->constrainBoundary(vrhs);

	m_fixResTimer.stopStoreLog();

	m_updateResTimer.stopStoreLog();
}

void SolverGenericDynSta::getMatrix(const VectorXd& vx, const VectorXd& vv, MatrixSd& mA)
{
	int N = this->m_pSM->getNumSimDOF_x();

	this->m_updateMatTimer.restart();

	this->m_vA.clear(); // Clear elements and reserve enough memory
	this->m_vA.reserve(this->m_pSM->getNumNonZeros_Jacobian() + N);

	//this->m_pSM->testJacobianLocal();
	//this->m_pSM->testJacobianGlobal();

	this->m_pSM->addJacobian(this->m_vA, NULL);
	this->addBoundaryJacobian(vx, vv, this->m_vA);

	if (!this->m_isStatic)
	{
		Real dt2i = 1/(this->m_dt*this->m_dt);
		for (int i = 0; i < this->m_N; ++i) // Add inertia contribution
			this->m_vA.push_back(Triplet<Real>(i, i, -this->m_vm(i)*dt2i));
	}

	m_fixMatTimer.restart();

	this->constrainBoundary(this->m_vA);

	m_fixMatTimer.stopStoreLog();

	mA.setFromTriplets(this->m_vA.begin(), this->m_vA.end());
	mA.makeCompressed(); // Sym. lower triangular & compressed

	mA *= -1; // Ma - Fint - Fext = 0

	this->m_updateMatTimer.stopStoreLog();
}