/*=====================================================================================*/
/*!
/file		BConditionForce.h
/author		jesusprod
/brief		Force boundary condition.
*/
/*=====================================================================================*/

#ifndef BCONDITION_FORCE_H
#define BCONDITION_FORCE_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

#include <JSandbox/Solver/BCondition.h>

#include <JSandbox/Model/SolidModel.h>

class   BConditionForce : public BCondition
{
public:

	BConditionForce();

	virtual ~BConditionForce();

	// <BCondition>

	virtual Real getEnergy(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv) const;
	virtual void addForce(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const;

	// </BCondition>

	virtual void setForce(const VectorXd& vg) { this->m_vf = vg; }
	virtual const VectorXd& getForce() const { return this->m_vf; }

	VectorXd m_vf;

};

#endif