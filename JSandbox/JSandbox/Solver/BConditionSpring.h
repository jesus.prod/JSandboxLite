/*=====================================================================================*/
/*!
/file		BConditionSpring.h
/author		jesusprod
/brief		Spring values boundary condition.
*/
/*=====================================================================================*/

#ifndef BCONDITION_SPRING_H
#define BCONDITION_SPRING_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

#include <JSandbox/Solver/BCondition.h>
#include <JSandbox/Model/SolidModel.h>

class   BConditionSpring : public BCondition
{

public:

	BConditionSpring();

	virtual ~BConditionSpring();

	// <BCondition>

	virtual Real getEnergy(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv) const;
	virtual void addForce(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const;
	virtual void addJacobian(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, tVector& vt) const;

	// </BCondition>

	virtual int getNumber() const { return (int) this->m_vidx.size(); }

	virtual VectorXi getIndices() const { return this->m_vidx; }
	virtual VectorXd getValues() const { return this->m_vval; }
	virtual Real getStiffness() const { return this->m_K; }

	virtual void setIndices(const VectorXi& vi) { this->m_vidx = vi; }
	virtual void setValues(const VectorXd& vd) { this->m_vval = vd; }
	virtual void setStiffness(Real K) { this->m_K = K; }

protected:

	VectorXi m_vidx;
	VectorXd m_vval;
	Real m_K;

};

#endif