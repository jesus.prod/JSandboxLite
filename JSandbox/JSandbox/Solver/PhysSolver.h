/*=====================================================================================*/
/*!
/file		PhysSolver.h
/author		jesusprod
/brief		Basic abstract implementation of a physics solver. All created solvers
should inherit from this class. It manages several common tasks: management
of boundary conditions, solving the Sequential Quadratic Programming Problem,
etc. Derived class must just implement the creation of system matrix and rhs,
vector, or in such case, reimplement solve() function.
*/
/*=====================================================================================*/

#ifndef PHYS_SOLVER_H
#define PHYS_SOLVER_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Solver/BCondition.h>
#include <JSandbox/Model/SolidModel.h>
#include <JSandbox/CustomTimer.h>

enum BCIndex
{
	BCFixed = 0,
	BCSpring = 1,
	BCStretch = 2
};

class   PhysSolver
{
public:

	enum LinearSolver
	{
		Cholesky = 0,
		Conjugate = 1,
	};

	enum StepSolver
	{
		StaticNewton = 0,
		FullImplicitEuler = 1,
		SemiImplicitEuler = 2,
		SymplecticEuler = 3
	};

	PhysSolver();
	virtual ~PhysSolver();

	virtual const SolidModel* getModel() const { return this->m_pSM; }
	virtual void setModel(SolidModel* pSM) { this->m_pSM = pSM; }

	virtual void clearBConditions();
	vector<BCondition*>& getBCList();
	virtual int getBConditionNumber() const;
	virtual void addBCondition(BCondition *pBC);
	virtual BCondition* getBCondition(int i);

	/*! Computes the complete list of fixed indices
	*	given the set of solver boundary conditions.
	*/
	iVector getFixedIndices() const;

	virtual void setup();

	virtual bool solveStaticEquilibrium();

	virtual bool step();
	virtual bool solve();

	virtual Real getPotential(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void getResidual(const VectorXd& vx, const VectorXd& vv, VectorXd& vR) = 0;
	virtual void getMatrix(const VectorXd& vx, const VectorXd& vv, MatrixSd& mA) = 0;

	virtual void advanceBoundary();
	virtual bool isFullyLoaded() const;
	virtual void constrainBoundary() const;
	virtual void constrainBoundary(VectorXd& vf) const;
	virtual void constrainBoundary(tVector& vJ) const;
	virtual Real getBoundaryEnergy(const VectorXd& vx, const VectorXd& vv) const;
	virtual void addBoundaryForce(const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const;
	virtual void addBoundaryJacobian(const VectorXd& vx, const VectorXd& vv, tVector& vJ) const;

	virtual bool hasMaximumStepSize() const { return this->m_hasMaxStep; }
	virtual void hasMaximumStepSize(bool has) { this->m_hasMaxStep = has; }

	virtual Real getMaximumStepSize() const { return this->m_maxStep; }
	virtual void setMaximumStepSize(Real ss) { this->m_maxStep = ss; }

	virtual bool isReady_Setup() const { return this->m_isReady_Setup; }

	virtual bool getIsStatic() const { return this->m_isStatic; }
	virtual void setIsStatic(bool sta) { this->m_isStatic = sta; }

	virtual bool getIsNonLin() const { return this->m_isNonLin; }
	virtual void setIsNonLin(bool nl) { this->m_isNonLin = nl; }

	virtual Real getSolverError() const { return this->m_solverMaxError; }
	virtual void setSolverError(Real e) { this->m_solverMaxError = e; }

	virtual LinearSolver getLinearSolver() const { return this->m_linearSolver; }
	virtual void setLinearSolver(LinearSolver ls) { this->m_linearSolver = ls; }

	virtual Real getLinearError() const { return this->m_linearMaxError; }
	virtual void setLinearError(Real lE) { this->m_linearMaxError = lE; }

	virtual bool getUseRegularize() const { return this->m_useRegularize; }
	virtual void setUseRegularize(bool reg) { this->m_useRegularize = reg; }

	virtual bool getUseLineSearch() const { return this->m_useLineSearch; }
	virtual void setUseLineSearch(bool bLS) { this->m_useLineSearch = bLS; }

	virtual int getLineSearchMaxIters() const { return this->m_lsMaxIters; }
	virtual void setLineSearchMaxIters(int maxit) { this->m_lsMaxIters = maxit; }

	virtual int getRegularizeMaxIters() const { return this->m_regMaxIters; }
	virtual void setRegularizeMaxIters(int maxit) { this->m_regMaxIters = maxit; }

	virtual int getSolverMaxIters() const { return this->m_solverMaxIters; }
	virtual void setSolverMaxIters(int maxit) { this->m_solverMaxIters = maxit; }

	virtual int getLinearMaxIters() const { return this->m_linearMaxIters; }
	virtual void setLinearMaxIters(int maxit) { this->m_linearMaxIters = maxit; }

	virtual Real getRegularizeFactor() const { return this->m_rFactor; }
	virtual void setRegularizeFactor(Real rC) { this->m_rFactor = rC; }

	virtual Real getLineSearchFactor() const { return this->m_lsBeta; }
	virtual void setLineSearchFactor(Real lsB) { this->m_lsBeta = lsB; }

	virtual Real getTimeStep() const { return this->m_dt; }
	virtual void setTimeStep(Real dt) { this->m_dt = dt; }

	virtual int getTimeStepMaxBisects() const { return this->m_dtBisec; }
	virtual void setTimeStepMaxBisects(int dt) { this->m_dtBisec = dt; }

	virtual Real getFramerate() const { return this->m_fr; }
	virtual void setFrameRate(Real fr) { this->m_fr = fr; }

	static bool SolveLinearSystem(MatrixSd& mA, const VectorXd& vb, VectorXd& vx, LinearSolver solver = LinearSolver::Cholesky, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);
	static bool SolveLinearSystem_Cholesky(MatrixSd& mA, const VectorXd& vb, VectorXd& vx, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);
	static bool SolveLinearSystem_Conjugate(MatrixSd& mA, const VectorXd& vb, VectorXd& vx, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);

	static bool SolveLinearSystem(MatrixSd& mA, const MatrixSd& mB, MatrixSd& mX, LinearSolver solver = LinearSolver::Cholesky, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);
	static bool SolveLinearSystem_Cholesky(MatrixSd& mA, const MatrixSd& mB, MatrixSd& mX, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);
	static bool SolveLinearSystem_Conjugate(MatrixSd& mA, const MatrixSd& mB, MatrixSd& mX, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);

	static bool SolveLinearSystem(MatrixSd& mA, const MatrixXd& mB, MatrixXd& mX, LinearSolver solver = LinearSolver::Cholesky, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);
	static bool SolveLinearSystem_Cholesky(MatrixSd& mA, const MatrixXd& mB, MatrixXd& mX, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);
	static bool SolveLinearSystem_Conjugate(MatrixSd& mA, const MatrixXd& mB, MatrixXd& mX, int regIter = 25, int maxIter = 1000, Real maxError = 1e-9);

protected:

	SolidModel* m_pSM;					// Solid model to solve
	vector<BCondition*> m_vpBC;			// Boundary conditions

	tVector m_vA;						// Jacobian matrix entries
	LinearSolver m_linearSolver;		// Solver for linear system

	int m_N;							// Number of degrees-of-freedom

	bool m_isStatic;
	bool m_isNonLin;

	Real m_dt;						// Time-step
	Real m_fr;						// Framerate
	Real m_rFactor;					// Factor for regularization
	Real m_lsBeta;					// Factor for the line-search
	Real m_solverMaxError;			// Physics solver convergence error
	Real m_linearMaxError;			// Linear solver convergence error

	int m_lsMaxIters;				// Maximum line-search bisections
	int m_regMaxIters;				// Maximum matrix regularizations
	int m_solverMaxIters;			// Maximum physics solver iterations
	int m_linearMaxIters;			// Maximum linear solver iterations
	int m_dtBisec;

	bool m_hasMaxStep;
	double m_maxStep;

	bool m_useRegularize;
	bool m_useLineSearch;

	bool m_isReady_Setup;

	VectorXd m_vm;					// Cached mass-vector
	VectorXd m_vx;					// Cached current positions
	VectorXd m_vv;					// Cached current velocities
	VectorXd m_vxPre;				// Cached previous positions (for integrators such as Verlet, collisions, etc.)
	VectorXd m_vvPre;				// Cached previous velocities (for integrations such as Verlet, collisions, etc.)

	CustomTimer m_linearTimer;
	CustomTimer m_solverTimer;
	CustomTimer m_updateResTimer;
	CustomTimer m_updateMatTimer;

};

#endif