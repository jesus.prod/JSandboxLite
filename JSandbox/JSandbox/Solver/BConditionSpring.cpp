/*=====================================================================================*/
/*!
/file		BConditionSpring.cpp
/author		jesusprod
/brief		Implementation of BConditionSpring.h
*/
/*=====================================================================================*/

#include <JSandbox/Solver/BConditionSpring.h>

BConditionSpring::BConditionSpring()
{
	// Nothing to do here...

	this->m_maxStep = 1;
	this->m_curStep = 1;
}

BConditionSpring::~BConditionSpring()
{
	// Nothing to do here...
}

Real BConditionSpring::getEnergy(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv) const
{
	double alpha = (Real)this->m_curStep / (Real)this->m_maxStep;

	const iVector& vsimDOF = pModel->getRawToSim_x();

	Real energy = 0;

	for (int i = 0; i < this->getNumber(); ++i)
	{
		int idx = vsimDOF[this->m_vidx(i)];

		if (idx == -1)
			continue;

		Real D = alpha*(m_vval(i) - vx(idx));

		energy += 0.5*this->m_K*D*D;
	}

	return energy;
}

void BConditionSpring::addForce(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const
{
	double alpha = (Real)this->m_curStep / (Real)this->m_maxStep;

	const iVector& vsimDOF = pModel->getRawToSim_x();

	for (int i = 0; i < this->getNumber(); ++i)
	{
		int idx = vsimDOF[this->m_vidx(i)];

		if (idx == -1)
			continue;

		Real D = alpha*(m_vval(i) - vx(idx));

		vf(idx) += this->m_K*D;
	}
}

void BConditionSpring::addJacobian(SolidModel* pModel, const VectorXd& vx, const VectorXd& vv, tVector& vt) const
{
	double alpha = (Real)this->m_curStep / (Real)this->m_maxStep;

	const iVector& vsimDOF = pModel->getRawToSim_x();

	for (int i = 0; i < this->getNumber(); ++i)
	{
		int idx = vsimDOF[this->m_vidx(i)];

		if (idx == -1)
			continue;

		vt.push_back(Triplet<Real>(idx, idx, -this->m_K));
	}
}
