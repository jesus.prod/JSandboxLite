/*=====================================================================================*/
/*!
/file		PhysSolver.cpp
/author		jesusprod
/brief		Implementation of PhysSolver.h
*/
/*=====================================================================================*/

#include <JSandbox/Solver/PhysSolver.h>

#ifdef _OPENMP 
#include <omp.h>
#endif

#include <JSandbox/Solver/BConditionFixed.h>

PhysSolver::PhysSolver() :
m_solverTimer(10, "TOTAL_SOL"),
m_linearTimer(10, "LINEAR_SOL"),
m_updateMatTimer(10, "UPD_SOL_MAT"),
m_updateResTimer(10, "UPD_SOL_RES")
{
	// Might be NULL
	this->m_pSM = NULL;

	// Set default parameters
	this->m_solverMaxError = 1e-6;
	this->m_linearMaxError = 1e-6;
	this->m_useRegularize = true;
	this->m_useLineSearch = true;
	this->m_lsMaxIters = 10;
	this->m_regMaxIters = 10;
	this->m_solverMaxIters = 10;
	this->m_linearMaxIters = 1000;
	this->m_lsBeta = 0.5;
	this->m_dt = 0.005;

	this->m_hasMaxStep = false;
	this->m_maxStep = HUGE_VAL;

	this->m_isReady_Setup = false;

	m_solverTimer.initialize();
	m_linearTimer.initialize();
	m_updateMatTimer.initialize();
	m_updateResTimer.initialize();
}

PhysSolver::~PhysSolver()
{
	this->clearBConditions();
}

void PhysSolver::setup()
{
	// Model should be initialized
	assert(this->m_pSM != NULL);

	// DOF
	this->m_N = this->m_pSM->getNumSimDOF_x();
	this->m_vxPre = this->m_pSM->getPositions_x();
	this->m_vvPre = this->m_pSM->getVelocities_x();
	this->m_vx = this->m_vxPre;
	this->m_vv = this->m_vvPre;

	this->m_isReady_Setup = true;
}

void PhysSolver::advanceBoundary()
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->advanceBoundary();
}

bool PhysSolver::isFullyLoaded() const
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		if (!this->m_vpBC[i]->isFullyLoaded())
			return false;
	return true;
}

void PhysSolver::clearBConditions()
{
	for (int i = 0; i < (int) this->m_vpBC.size(); ++i)
		delete this->m_vpBC[i]; // Free allocated memory
	this->m_vpBC.clear();

	this->m_isReady_Setup = false;
}

vector<BCondition*>& PhysSolver::getBCList() { return this->m_vpBC; }

int PhysSolver::getBConditionNumber() const { return (int) this->m_vpBC.size(); }

void PhysSolver::addBCondition(BCondition *pBC) { assert(pBC != NULL); this->m_vpBC.push_back(pBC); this->m_isReady_Setup = false; }

BCondition *PhysSolver::getBCondition(int i) { assert(i >= 0 && i < this->m_vpBC.size()); return this->m_vpBC[i]; this->m_isReady_Setup = false; }

iVector PhysSolver::getFixedIndices() const
{
	iVector vfixed;

	int numBC = (int) this->m_vpBC.size();
	for (int i = 0; i < numBC; ++i) // Check
	{
		BConditionFixed* pBCFixed = dynamic_cast<BConditionFixed*>(this->m_vpBC[i]);
		if (pBCFixed != NULL)
		{
			VectorXi vidx = pBCFixed->getIndices();
			for (int j = 0; j < (int)vidx.size(); ++j)
				vfixed.push_back(vidx(j)); // Append DOF
		}
	}

	return vfixed;
}

bool PhysSolver::solveStaticEquilibrium()
{
	this->constrainBoundary();

	// At least one
	this->solve();

	VectorXd vf;
	this->getResidual(
		this->m_pSM->getPositions_x(),
		this->m_pSM->getVelocities_x(),
		vf);
	Real vfNorm = vf.norm();

	int itCur = 0;
	int itMax = 1000;

	while (!this->isFullyLoaded() || vfNorm > this->m_solverMaxError)
	{
		if (!this->solve())
		{
			logSimu("[FAILURE] Impossible to perform equilibrium step");

			break; // Out
		}

		// Compute the new force residual

		this->getResidual(
			this->m_pSM->getPositions_x(),
			this->m_pSM->getVelocities_x(),
			vf);
		vfNorm = vf.norm();

		// Check if maximum iterations

		itCur += this->m_solverMaxIters;

		if (itCur >= itMax)
		{
			logSimu("[FAILURE] Maximum projection iterations reached");

			break; // Out
		}
	}

	return vfNorm < this->m_solverMaxError;
}

bool PhysSolver::step()
{
	this->advanceBoundary(); // Advance boundary conditions: on step stronger

	//this->m_pSM->testForceLocal();
	//this->m_pSM->testForceGlobal();
	//this->m_pSM->testJacobianLocal();
	//this->m_pSM->testJacobianGlobal();

	bool result = false;

	if (!this->m_isStatic)
	{
		// Initialize mass vector as it will be needed

		this->m_vm.resize(this->m_pSM->getNumSimDOF_x());
		this->m_vm.setZero(); // Init. from zero vector
		this->m_pSM->add_Mass(this->m_vm);

		// Select time-step and go!

		Real dtStart = this->m_dt;

		if (this->m_fr == 0)
		{
			Real dt0 = this->m_dt;
			this->m_dt = 2 * dt0;
			int bn = 0;
			do
			{
				bn++; // Bisection
				this->m_dt *= 0.5;

				this->m_solverTimer.restart();
				result = this->solve();
				this->m_solverTimer.stopStoreLog();

			} while (!result && bn <= this->m_dtBisec);
		}
		else
		{
			Real T = 1.0 / this->m_fr;
			Real t = 0;
			while (abs(t - T) > 1e-6f)
			{
				Real dt0 = this->m_dt;
				if (t + dt0 > T)
					dt0 = T - t;

				this->m_dt = 2 * dt0;
				int bn = 0;
				do
				{
					bn++; // Bisection
					this->m_dt *= 0.5;

					this->m_solverTimer.restart();
					result = this->solve();
					this->m_solverTimer.stopStoreLog();
				} 
				while (!result && bn <= this->m_dtBisec);

				t += this->m_dt;
			}
		}

		this->m_dt = dtStart;
	}
	else
	{
		this->m_solverTimer.restart();
		this->solve();
		this->m_solverTimer.stopStoreLog();
	}

	return result;
}

bool PhysSolver::solve()
{
	//bool meta = true;

	//if (this->m_isStatic)
	//{
	//	logSimu("[INFO] Non-linear static solve with %u steps", m_solverMaxIters);
	//}
	//else
	//{
	//	logSimu("[INFO] Non-linear dynamic solve with time-step %.9f", this->m_dt);
	//}

	int Ni = this->m_pSM->getNumSimDOF_x();

	//this->m_pSM->testForceGlobal();
	//this->m_pSM->testJacobianGlobal();

	MatrixSd mAi(Ni, Ni);
	VectorXd vRi(Ni);
	VectorXd vdx(Ni);
	mAi.setZero();
	vRi.setZero();
	vdx.setZero();
	Real Ui = 0.0;
	Real Ri = 0.0;

	// Get initial model state (unconstrained) 
	this->m_vx = this->m_pSM->getPositions_x();
	this->m_vv = this->m_pSM->getVelocities_x();

	// Constrain  the state according to the specified
	// boundary conditions. This might imply some nodes
	// movement, for instance.
	this->constrainBoundary();

	// Get initial model state (constrained) 
	VectorXd vxi = this->m_pSM->getPositions_x();
	VectorXd vvi = this->m_pSM->getVelocities_x();

	if (!this->m_isStatic)
	{
		// Compute initial guess
		vxi = vxi + this->m_dt*vvi;

		// Update model state to guess
		this->m_pSM->setPositions_x(vxi);
		this->m_pSM->setVelocities_x(vvi);
	}

	//VectorXd vresConvergence(this->m_solverMaxIters + 1);
	//VectorXd vpotConvergence(this->m_solverMaxIters + 1);

	int i;
	bool solved = true;
	for (i = 0; i < this->m_solverMaxIters; ++i)
	{
		if (i == 0)
		{
			this->getResidual(vxi, vvi, vRi);
			Ui = this->getPotential(vxi, vvi);
			Ri = vRi.norm();

			//vpotConvergence(i) = Ui;
			//vresConvergence(i) = Ri;

			logSimu("[INFO] Starting solve with Res.: %.9f, Pot.: %.9f", Ri, Ui);
		}
		else
		{
			logSimu("[INFO] Starting step with Res.: %.9f, Pot.: %.9f", Ri, Ui);
		}

		if (Ri <= this->m_solverMaxError)
		{
			break; // Out
		}

		// Get the matrix of the step
		this->getMatrix(vxi, vvi, mAi);

		if (!this->SolveLinearSystem(mAi, vRi, vdx))
		{
			solved = false;
			break; // Out
		}

		if (this->m_hasMaxStep)
		{
			Real stepSize = vdx.norm();

			// Cap the size of the step
			if (stepSize > this->m_maxStep)
			{
				vdx = vdx.normalized();
				vdx *= this->m_maxStep;
			}
		}

		// Push the current state

		pSolidState pStateLS = this->m_pSM->getState_x()->clone();

		this->m_lsBeta = 0.5;

		Real stepSize = 0.0;
		VectorXd vxl = vxi;
		VectorXd vvl = vvi;
		Real Rl = Ri;
		Real Ul = Ui;

		// Line-search in dx direction

		int l;
		for (l = 0; l < this->m_lsMaxIters; ++l)
		{
			stepSize = vdx.norm();

			// Step vx
			vxi += vdx;

			if (!this->m_isStatic) // Update dynamics
				vvi = (vxi - this->m_vx) / this->m_dt;

			// Update model state to new state

			//this->m_pSM->setState_x(pStateLS);

			this->m_pSM->setPositions_x(vxi);
			this->m_pSM->setVelocities_x(vvi);
			vxi = this->m_pSM->getPositions_x();
			vvi = this->m_pSM->getVelocities_x();
			Ui = this->getPotential(vxi, vvi);
			this->getResidual(vxi, vvi, vRi);
			Ri = vRi.norm();

			//logSimu("[INFO] LS alpha %f: ( %.12f , %.12f )", pow(0.5, l), Ui, Ri);

			bool improved = Ui <= Ul;

			if (Ui < Ul)
			{
				//logSimu("[INFO] Line-search %u: Improved ENERGY", l);
			}

			if (Ri < Rl)
			{
				//logSimu("[INFO] Line-search %u: Improved GRADIENT", l);
			}

			// Improved?
			if (improved)
			{
				break;
			}

			// Bisection / restore
			vdx *= this->m_lsBeta;
			vxi = vxl;
			vvi = vvl;

			// Roll-back model state

			this->m_pSM->setState_x(pStateLS);
			//this->m_pSM->setPositions_x(vxi);
			//this->m_pSM->setVelocities_x(vvi);
		}

		if (l == this->m_lsMaxIters)
		{
			//writeToFile(mAi, "jacobianNotConverged.csv", true);

			logSimu("[FAILURE] %u bis. Step: %.9f. Res.: %.9f. Pot.: %.9f", l, stepSize, Ri, Ui);

			solved = false;
			break; // Error
		}
		else
		{
			logSimu("[SUCCESS] %u bis. Step: %.9f. Res.: %.9f. Pot.: %.9f", l, stepSize, Ri, Ui);

			//vpotConvergence(i + 1) = Ui;
			//vresConvergence(i + 1) = Ri;
		}
	}

	if (solved)
	{
		//logSimu("[SUCCESS] %u iterations. Res.: %.9f. Pot.: %.9f", i, Ri, Ui);

		this->m_vxPre = this->m_vx;
		this->m_vvPre = this->m_vv;
		this->m_vx = vxi;
		this->m_vv = vvi;
	}
	else
	{
		//logSimu("[FAILURE]%u iterations. Res.: %.9f. Pot.: %.9f", i, Ri, Ui);

		this->m_vx = vxi;
		this->m_vv = vvi;
	}

	//writeToFile(vpotConvergence, "potConvergence.csv");
	//writeToFile(vresConvergence, "resConvergence.csv");

	return solved;
}

bool PhysSolver::SolveLinearSystem(MatrixSd& mA, const VectorXd& vb, VectorXd& vx, LinearSolver linear, int regIter, int maxIters, Real maxError)
{
	assert(mA.cols() == mA.rows());
	assert(mA.cols() == vb.size());

	switch (linear)
	{
	case LinearSolver::Cholesky:
		return PhysSolver::SolveLinearSystem_Cholesky(mA, vb, vx, regIter, maxError);
		break;
	case LinearSolver::Conjugate:
		return PhysSolver::SolveLinearSystem_Conjugate(mA, vb, vx, regIter, maxError);
		break;
	}

	return false;
}

bool PhysSolver::SolveLinearSystem(MatrixSd& mA, const MatrixSd& mB, MatrixSd& mX, LinearSolver linear, int regIter, int maxIters, Real maxError)
{
	assert(mA.cols() == mA.rows());
	assert(mA.cols() == mB.rows());

	switch (linear)
	{
	case LinearSolver::Cholesky:
		return PhysSolver::SolveLinearSystem_Cholesky(mA, mB, mX, regIter, maxError);
		break;
	case LinearSolver::Conjugate:
		return PhysSolver::SolveLinearSystem_Conjugate(mA, mB, mX, regIter, maxError);
		break;
	}

	return false;
}

bool PhysSolver::SolveLinearSystem(MatrixSd& mA, const MatrixXd& mB, MatrixXd& mX, LinearSolver linear, int regIter, int maxIters, Real maxError)
{
	assert(mA.cols() == mA.rows());
	assert(mA.cols() == mB.rows());

	switch (linear)
	{
	case LinearSolver::Cholesky:
		return PhysSolver::SolveLinearSystem_Cholesky(mA, mB, mX, regIter, maxError);
		break;
	case LinearSolver::Conjugate:
		return PhysSolver::SolveLinearSystem_Conjugate(mA, mB, mX, regIter, maxError);
		break;
	}

	return false;
}

bool PhysSolver::SolveLinearSystem_Cholesky(MatrixSd& mA, const VectorXd& vb, VectorXd& vx, int regIter, int maxIters, Real maxError)
{
	int N = (int) vb.size();

	Real normA = mA.norm();
	Real sinThres = min(1e-6, max(1e-9, 1e-9*normA));
	Real regThres = min(1e-3, max(1e-6, 1e-6*normA));

	//logSimu("[INFO] Singularity threshold: %.9f\n", sinThres);

	MatrixSd mI(N, N);
	tVector vT(N);
	for (int i = 0; i < N; ++i)
		vT[i] = Triplet<Real>(i, i, 1.0);
	mI.setFromTriplets(vT.begin(), vT.end());
	mI.makeCompressed();

	// Solve the specified definite positive
	// linear system. Regularize the matrix
	// if needed to create a DP system.

	int i = 0;
	for (i = 0; i < regIter; ++i)
	{
		if (i != 0)
		{
			//logSimu("[INFO] Regularizing system, iteration %u", i);

			mA += mI*regThres*pow(10, i - 1);
		}

		SimplicialLDLT<MatrixSd> solver;

		solver.compute(mA);

		// Check triangulation

		if (solver.info() != Success)
		{
			//logSimu("[FAILURE] Linear solve: error during triangulation");
			continue; // Iterate again
		}

		// Check singularity

		bool jumpSin = false;
		VectorXd vd = solver.vectorD();
		for (int j = 0; j < N; ++j)
		{
			if (isApprox(vd(j), 0.0, sinThres))
			{
				//logSimu("[FAILURE] Linear solve: singular matrix, value: %.9f", vd(j));
				jumpSin = true;
				break;
			}
		}

		if (jumpSin)
			continue;

		vx = solver.solve(vb);

		// Check calculation

		if (solver.info() != Success)
		{
			//logSimu("[FAILURE] Linear solve: error during calculation");
			continue; // Iterate again
		}

		// Check indefiniteness

		double dot = vb.dot(vx);
		if (dot < 0.0)
		{
			//logSimu("[FAILURE] Linear solve: indefinite matrix, dot: %.9f", dot);
			continue; // Iterate again
		}

		// Check exact solution

		VectorXd vbTest = mA.selfadjointView<Lower>()*vx;
		double absError = (vb - vbTest).norm();
		double relError = absError / vb.norm();
		if (absError > maxError && relError > maxError)
		{
			//logSimu("[FAILURE] Linear solve: inexact solution, error: %.9f", relError);
			continue; // Iterate again
		}

		logSimu("[SUCCESS] Linear solve: solved using Cholesky after %i regularization steps", i);

		return true;
	}

	return false;
}

bool PhysSolver::SolveLinearSystem_Conjugate(MatrixSd& mA, const VectorXd& vb, VectorXd& vx, int regIter, int maxIters, Real maxError)
{
	int N = (int)vb.size();

	Real normA = mA.norm();
	Real sinThres = min(1e-6, max(1e-9, 1e-9*normA));
	Real regThres = min(1e-3, max(1e-6, 1e-6*normA));

	//logSimu("[INFO] Singularity threshold: %.9f\n", sinThres);

	MatrixSd mI(N, N);
	tVector vT(N);
	for (int i = 0; i < N; ++i)
		vT[i] = Triplet<Real>(i, i, 1.0);
	mI.setFromTriplets(vT.begin(), vT.end());
	mI.makeCompressed();

	// Solve the specified definite positive
	// linear system. Regularize the matrix
	// if needed to create a DP system.

	int i = 0;
	for (i = 0; i < regIter; ++i)
	{
		if (i != 0)
		{
			//logSimu("[INFO] Regularizing system, iteration %u", i);

			mA += mI*regThres*pow(10, i - 1);
		}

		ConjugateGradient<MatrixSd> solver;
		solver.setMaxIterations(maxIters);
		solver.setTolerance(maxError);

		solver.compute(mA);

		// Check computation

		if (solver.info() != Success)
		{
			//logSimu("[FAILURE] Linear solve: error during computation");
			continue; // Iterate again
		}

		vx = solver.solve(vb);

		// Check calculation

		if (solver.info() != Success)
		{
			//logSimu("[FAILURE] Linear solve: error during calculation");
			continue; // Iterate again
		}

		// Check indefiniteness

		double dot = vb.dot(vx);
		if (dot < 0.0)
		{
			//logSimu("[FAILURE] Linear solve: indefinite matrix, dot: %.9f", dot);
			continue; // Iterate again
		}

		// Check exact solution

		VectorXd vbTest = mA.selfadjointView<Lower>()*vx;
		double absError = (vb - vbTest).norm();
		double relError = absError / vb.norm();
		if (absError > maxError && relError > maxError)
		{
			//logSimu("[FAILURE] Linear solve: inexact solution, error: %.9f", relError);
			continue; // Iterate again
		}

		logSimu("[SUCCESS] Linear solve: solved using Conjugate Gradient after %i regularization steps", i);

		return true;
	}

	return false;
}

bool PhysSolver::SolveLinearSystem_Cholesky(MatrixSd& mA, const MatrixSd& mB, MatrixSd& mX, int regIter, int maxIter, Real maxError)
{
	int N = mB.rows();
	int M = mB.cols();

	assert(N == mA.rows());

	Real normA = mA.norm();
	Real sinThres = min(1e-6, max(1e-9, 1e-9*normA));
	Real regThres = min(1e-3, max(1e-6, 1e-6*normA));

	//logSimu("[INFO] Singularity threshold: %.9f\n", sinThres);

	MatrixSd mI(N, N);
	tVector vT(N);
	for (int i = 0; i < N; ++i)
		vT[i] = Triplet<Real>(i, i, 1.0);
	mI.setFromTriplets(vT.begin(), vT.end());
	mI.makeCompressed();

	// Solve the specified definite positive
	// linear system. Regularize the matrix
	// if needed to create a DP system.

	for (int i = 0; i < regIter; ++i)
	{
		if (i != 0)
		{
			logSimu("[INFO] Regularizing system, iteration %u", i);

			mA += mI*regThres*pow(10, i - 1);
		}

		SimplicialLDLT<MatrixSd> solver;

		solver.compute(mA);

		// Check triangulation

		if (solver.info() != Success)
		{
			logSimu("[FAILURE] Linear solve: error during triangulation");
			continue; // Iterate again
		}

		bool jumpSin = false;
		VectorXd vd = solver.vectorD();
		for (int j = 0; j < N; ++j)
		{
			if (isApprox(vd(j), 0.0, sinThres))
			{
				logSimu("[FAILURE] Linear solve: singular matrix, value: %.9f", vd(j));
				jumpSin = true;
				break;
			}
		}

		if (jumpSin)
			continue;

		mX = solver.solve(mB);
	
		// Check calculation

		if (solver.info() != Success)
		{
			logSimu("[FAILURE] Linear solve: error during calculation");
			continue; // Iterate again
		}

		// Check indefiniteness

		bool jumpInd = false;
		for (int j = 0; j < mB.cols(); ++j)
		{
			double dot = mB.col(j).dot(mX.col(j));
			if (dot < 0.0)
			{
				logSimu("[FAILURE] Linear solve: indefinite matrix, dot: %.9f", dot);
				jumpInd = true;
				break;
			}
		}

		if (jumpInd)
			continue;

		// Check exact solution

		MatrixSd mBTest = mA.selfadjointView<Lower>()*mX;
		Real absError = (mB - mBTest).norm();
		Real relError = absError / mB.norm();
		if (absError > maxError && relError > maxError)
		{
			logSimu("[FAILURE] Linear solve: inexact solution, error: %.9f", relError);
			continue; // Iterate again
		}

		logSimu("[SUCCESS] Linear solve: solved using Simplicial Cholesky");

		return true;
	}

	return false;
}

bool PhysSolver::SolveLinearSystem_Conjugate(MatrixSd& mA, const MatrixSd& mB, MatrixSd& mX, int regIter, int maxIters, Real maxError)
{
	int N = mA.cols();

	Real normA = mA.norm();
	Real sinThres = min(1e-6, max(1e-9, 1e-9*normA));
	Real regThres = min(1e-3, max(1e-6, 1e-6*normA));

	//logSimu("[INFO] Singularity threshold: %.9f\n", sinThres);
	
	MatrixSd mI(N, N);
	tVector vT(N);
	for (int i = 0; i < N; ++i)
		vT[i] = Triplet<Real>(i, i, 1.0);
	mI.setFromTriplets(vT.begin(), vT.end());
	mI.makeCompressed();

	// Solve the specified definite positive
	// linear system. Regularize the matrix
	// if needed to create a DP system.

	for (int i = 0; i < regIter; ++i)
	{
		if (i != 0)
		{
			logSimu("[INFO] Regularizing system, iteration %u", i);

			mA += mI*regThres*pow(10, i - 1);
		}

		ConjugateGradient<MatrixSd> solver;
		solver.setMaxIterations(maxIters);
		solver.setTolerance(maxError);

		solver.compute(mA);

		// Check computation

		if (solver.info() != Success)
		{
			logSimu("[FAILURE] Linear solve: error during triangulation");
			continue; // Iterate again
		}

		mX = solver.solve(mB);

		// Check calculation

		if (solver.info() != Success)
		{
			logSimu("[FAILURE] Linear solve: error during calculation");
			continue; // Iterate again
		}

		// Check indefiniteness

		bool jumpInd = false;
		for (int j = 0; j < mB.cols(); ++j)
		{
			double dot = mB.col(j).dot(mX.col(j));
			if (dot < 0.0)
			{
				logSimu("[FAILURE] Linear solve: indefinite matrix, dot: %.9f", dot);
				jumpInd = true;
				break;
			}
		}

		if (jumpInd)
			continue;

		// Check exact solution

		MatrixSd mBTest = mA.selfadjointView<Lower>()*mX;
		Real absError = (mB - mBTest).norm();
		Real relError = absError / mB.norm();
		if (absError > maxError && relError > maxError)
		{
			logSimu("[FAILURE] Linear solve: inexact solution, error: %.9f", relError);
			continue; // Iterate again
		}

		logSimu("[SUCCESS] Linear solve: solved using Conjugate Gradient");

		return true;
	}

	return false;

}

bool PhysSolver::SolveLinearSystem_Cholesky(MatrixSd& mA, const MatrixXd& mB, MatrixXd& mX, int regIter, int maxIter, Real maxError)
{
	int N = mB.rows();
	int M = mB.cols();

	assert(N == mA.rows());

	Real normA = mA.norm();
	Real sinThres = min(1e-6, max(1e-9, 1e-9*normA));
	Real regThres = min(1e-3, max(1e-6, 1e-6*normA));

	//logSimu("[INFO] Singularity threshold: %.9f\n", sinThres);

	MatrixSd mI(N, N);
	tVector vT(N);
	for (int i = 0; i < N; ++i)
		vT[i] = Triplet<Real>(i, i, 1.0);
	mI.setFromTriplets(vT.begin(), vT.end());
	mI.makeCompressed();

	// Solve the specified definite positive
	// linear system. Regularize the matrix
	// if needed to create a DP system.

	for (int i = 0; i < regIter; ++i)
	{
		if (i != 0)
		{
			logSimu("[INFO] Regularizing system, iteration %u", i);

			mA += mI*regThres*pow(10, i - 1);
		}

		SimplicialLDLT<MatrixSd> solver;

		solver.compute(mA);

		// Check triangulation

		if (solver.info() != Success)
		{
			logSimu("[FAILURE] Linear solve: error during triangulation");
			continue; // Iterate again
		}

		bool jumpSin = false;
		VectorXd vd = solver.vectorD();
		for (int j = 0; j < N; ++j)
		{
			if (isApprox(vd(j), 0.0, sinThres))
			{
				logSimu("[FAILURE] Linear solve: singular matrix, value: %.9f", vd(j));
				jumpSin = true;
				break;
			}
		}

		if (jumpSin)
			continue;

		mX = solver.solve(mB);

		// Check calculation

		if (solver.info() != Success)
		{
			logSimu("[FAILURE] Linear solve: error during calculation");
			continue; // Iterate again
		}

		// Check indefiniteness

		bool jumpInd = false;
		for (int j = 0; j < mB.cols(); ++j)
		{
			double dot = mB.col(j).dot(mX.col(j));
			if (dot < 0.0)
			{
				logSimu("[FAILURE] Linear solve: indefinite matrix, dot: %.9f", dot);
				jumpInd = true;
				break;
			}
		}

		if (jumpInd)
			continue;

		// Check exact solution

		MatrixXd mBTest = mA.selfadjointView<Lower>()*mX;
		Real absError = (mB - mBTest).norm();
		Real relError = absError / mB.norm();
		if (absError > maxError && relError > maxError)
		{
			logSimu("[FAILURE] Linear solve: inexact solution, error: %.9f", relError);
			continue; // Iterate again
		}

		logSimu("[SUCCESS] Linear solve: solved using Simplicial Cholesky");

		return true;
	}

	return false;
}

bool PhysSolver::SolveLinearSystem_Conjugate(MatrixSd& mA, const MatrixXd& mB, MatrixXd& mX, int regIter, int maxIters, Real maxError)
{
	int N = mA.cols();

	Real normA = mA.norm();
	Real sinThres = min(1e-6, max(1e-9, 1e-9*normA));
	Real regThres = min(1e-3, max(1e-6, 1e-6*normA));

	//logSimu("[INFO] Singularity threshold: %.9f\n", sinThres);

	MatrixSd mI(N, N);
	tVector vT(N);
	for (int i = 0; i < N; ++i)
		vT[i] = Triplet<Real>(i, i, 1.0);
	mI.setFromTriplets(vT.begin(), vT.end());
	mI.makeCompressed();

	// Solve the specified definite positive
	// linear system. Regularize the matrix
	// if needed to create a DP system.

	for (int i = 0; i < regIter; ++i)
	{
		if (i != 0)
		{
			logSimu("[INFO] Regularizing system, iteration %u", i);

			mA += mI*regThres*pow(10, i - 1);
		}

		ConjugateGradient<MatrixSd> solver;
		solver.setMaxIterations(maxIters);
		solver.setTolerance(maxError);

		solver.compute(mA);

		// Check computation

		if (solver.info() != Success)
		{
			logSimu("[FAILURE] Linear solve: error during triangulation");
			continue; // Iterate again
		}

		mX = solver.solve(mB);

		// Check calculation

		if (solver.info() != Success)
		{
			logSimu("[FAILURE] Linear solve: error during calculation");
			continue; // Iterate again
		}

		// Check indefiniteness

		bool jumpInd = false;
		for (int j = 0; j < mB.cols(); ++j)
		{
			double dot = mB.col(j).dot(mX.col(j));
			if (dot < 0.0)
			{
				logSimu("[FAILURE] Linear solve: indefinite matrix, dot: %.9f", dot);
				jumpInd = true;
				break;
			}
		}

		if (jumpInd)
			continue;

		// Check exact solution

		MatrixXd mBTest = mA.selfadjointView<Lower>()*mX;
		Real absError = (mB - mBTest).norm();
		Real relError = absError / mB.norm();
		if (absError > maxError && relError > maxError)
		{
			logSimu("[FAILURE] Linear solve: inexact solution, error: %.9f", relError);
			continue; // Iterate again
		}

		logSimu("[SUCCESS] Linear solve: solved using Conjugate Gradient");

		return true;
	}

	return false;

}

void PhysSolver::constrainBoundary() const
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->constrain(this->m_pSM);
}

void PhysSolver::constrainBoundary(VectorXd& vf) const
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->constrain(this->m_pSM, vf);
}

void PhysSolver::constrainBoundary(tVector& vJ) const
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->constrain(this->m_pSM, vJ);
}

Real PhysSolver::getBoundaryEnergy(const VectorXd& vx, const VectorXd& vv) const
{
	int N = this->m_pSM->getNumSimDOF_x();

	assert(vx.size() == N);
	assert(vv.size() == N);

	double energy = 0;

	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		energy += this->m_vpBC[i]->getEnergy(this->m_pSM, vx, vv);

	return energy;
}

void PhysSolver::addBoundaryForce(const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const
{
	int N = this->m_pSM->getNumSimDOF_x();

	assert(vx.size() == N);
	assert(vv.size() == N);
	assert(vf.size() == N);

	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->addForce(this->m_pSM, vx, vv, vf);
}

void PhysSolver::addBoundaryJacobian(const VectorXd& vx, const VectorXd& vv, tVector& vJ) const
{
	int N = this->m_pSM->getNumSimDOF_x();

	assert(vx.size() == N);
	assert(vv.size() == N);

	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->addJacobian(this->m_pSM, vx, vv, vJ);
}