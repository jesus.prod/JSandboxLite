/*=====================================================================================*/
/*!
/file		BConditionFixed.cpp
/author		jesusprod
/brief		Implementation of BConditionFixed.h
*/
/*=====================================================================================*/

#include <JSandbox/Solver/BConditionFixed.h>

BConditionFixed::BConditionFixed()
{
	// Nothing to do here...

	this->m_maxStep = 1;
	this->m_curStep = 1;
}

BConditionFixed::~BConditionFixed()
{
	// Nothing to do here...
}

void BConditionFixed::constrain(SolidModel* pModel) const
{
	double alpha = (Real)this->m_curStep / (Real)this->m_maxStep;

	VectorXd vx = pModel->getPositions_x();
	VectorXd vv = pModel->getVelocities_x();

	const iVector& vsimDOF = pModel->getRawToSim_x();

	for (int i = 0; i < (int)this->m_vidx.size(); ++i)
	{
		if (vsimDOF[this->m_vidx(i)] == -1)
			continue; // No simulated DOF

		vv(vsimDOF[this->m_vidx(i)]) = 0.0; 

		if (!this->m_default) // Move values a proportion of the distance according to the load step
			vx(vsimDOF[this->m_vidx(i)]) += alpha*(this->m_vval(i) - vx(vsimDOF[this->m_vidx(i)]));
	}

	pModel->setPositions_x(vx);

	logSimu("[INFO] Constraint applied: Fixed point alpha %f", alpha);
}

void BConditionFixed::constrain(SolidModel* pModel, VectorXd& vf) const
{
	const iVector& vsimDOF = pModel->getRawToSim_x();

	for (int i = 0; i < (int)this->m_vidx.size(); ++i)
	{
		if (vsimDOF[this->m_vidx(i)] == -1)
			continue; // No simulated DOF

		vf(vsimDOF[this->m_vidx(i)]) = 0.0; // No force
	}
}

void BConditionFixed::constrain(SolidModel* pModel, tVector& vJ) const
{
	int nEntries = (int) vJ.size();
	for (int i = 0; i < nEntries; ++i)
	{
		Triplet<Real>& t = vJ[i];
		int r = t.row();
		int c = t.col();

		if (m_vstencil[r] || m_vstencil[c]) 
			vJ[i] = Triplet<Real>(r, c, 0.0); 
	}

	for (int i = 0; i < pModel->getNumSimDOF_x(); ++i)
		if (this->m_vstencil[i]) // Add diagonal ones
			vJ.push_back(Triplet<Real>(i, i, -1.0));
}

void BConditionFixed::initializeIndividual(const iVector& vidx)
{
	int N = (int)vidx.size();
	this->m_vidx.resize(N);

	for (int i = 0; i < N; ++i)
		this->m_vidx[i] = vidx[i];

	this->m_maxStep = 1;
	this->m_curStep = 0;
}

void BConditionFixed::initializeNodal(int n, const iVector& vidx)
{
	int nv = (int)vidx.size();
	int N = n*nv;
	this->m_vidx.resize(N);

	for (int i = 0; i < nv; ++i)
	{
		int offsetN = n*i;
		int offsetn = n*vidx[i];
		for (int d = 0; d < n; ++d)
			this->m_vidx[offsetN + d] = offsetn + d;
	}

	this->m_maxStep = 1;
	this->m_curStep = 0;
}

void BConditionFixed::updateStencil(SolidModel* pModel)
{
	const iVector& vsimDOF = pModel->getRawToSim_x();

	int nsimDOF = pModel->getNumSimDOF_x();

	this->m_vstencil.assign(nsimDOF, false);

	for (int i = 0; i < (int) this->m_vidx.size(); ++i)
	{
		if (vsimDOF[this->m_vidx(i)] == -1)
			continue; // Not simulated DOF

		this->m_vstencil[vsimDOF[this->m_vidx(i)]] = true;
	}
		
}