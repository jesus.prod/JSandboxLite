/*=====================================================================================*/
/*!
/file		RodMesh.h
/author		jesusprod
/brief		Declaration of RodMesh class (mesh of framed linear curves).
*/
/*=====================================================================================*/

#ifndef ROD_MESH_H
#define ROD_MESH_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/Rod.h>

#include <set>

struct Con
{
	set<pair<int, int>> m_srods;

	bool operator==(const Con& other) const
	{
		return this->m_srods == other.m_srods;
	}

	bool operator!=(const Con& other) const
	{
		return this->m_srods != other.m_srods;
	}
};

class RodMesh
{

public:

	RodMesh();
	RodMesh(const RodMesh& toCopy);

	RodMesh(const vector<Rod>& vr, const vector<string>& vp = vector<string>());
	void init(const vector<Rod>& vr, const vector<string>& vp = vector<string>());

	RodMesh(const vector<Rod>& vr, const vector<Con>& vc, const vector<Matrix3d>& vR = vector<Matrix3d>(), const vector<string>& vp = vector<string>());
	void init(const vector<Rod>& vr, const vector<Con>& vc, const vector<Matrix3d>& vR = vector<Matrix3d>(), const vector<string>& vp = vector<string>());

	void checkConsistency();

	void clearContent();
	void updateCache();
	
	void computeDistanceMatrix_Topology(MatrixXd& mD, const string& pname = "");
	void computeDistanceMatrix_Cartesian(MatrixXd& mD, const string& pname = "");
	void computeDistanceMatrix_RodBased(MatrixXd& mD, const string& pname = "");

	dVector getRanges(const string& pname = "") const;

	int getNumProp() const { return (int) this->m_vprops.size(); }
	const vector<string>& getProperties() const { return this->m_vprops; }

	void getPoints(dVector& vpoints, const string& pname = "") const;
	void setPoints(const dVector& vpoints, const string& pname = "");

	void getMeshData(dVector& vnodes, iVector& vedges, const string& pname = "") const;

	static void InitializeFramesWorldAligned(RodMesh& src, const string& pnamesrc = "");
	static bool InitializeFramesFromRotatedTangents(RodMesh& src, const vector<vector<Vector3d>>& vF, const string& pnamesrc = "");
	static void InitializeFramesFromRotatedRodMesh(RodMesh& src, RodMesh& dst, const string& pnamesrc = "", const string& pnamedst = "");

	int getNumRod() const { return (int) this->m_vprods.at(this->m_vprops.front()).size(); }
	const vector<Rod>& getRods(const string& pname = "") const { return this->m_vprods.at(pname); }

	int getNumCon() const { return (int) this->m_vcons.size(); }
	const vector<Con>& getCons() const { return this->m_vcons; }

	void clearRods();
	void removeRod(int i, bool merge = false);
	void setRod(int i, const Rod& rod, const string& pname, bool setCon = true);
	void setRods(const vector<Rod>& vrods, const string& pname, bool setCon = true);

	bool addRod(const Rod& rod, Real tol, const string& pname, iVector& vaddRods, iVector& vdelRods, bool intersect = true);

	bool weld_BinaryConnection2Rod(int nodeIdx, const string& pname, iVector& vaddRods, iVector& vdelRods);
	bool split_Rod2BinaryConnection(int nodeIdx, const string& pname, iVector& vaddRods, iVector& vdelRods);

	bool weld_Connection(int nodeIdx0, int nodeIdx1, const string& pname, iVector& vaddRods, iVector& vdelRods);
	bool split_Connection(int nodeIdx0, int nodeIdx1, const string& pname, iVector& vaddRods, iVector& vdelRods);

	bool duplicateRod(int i, iVector& vaddRods, iVector&);
	bool joinRods(int i, int j, iVector& vaddRods, iVector&);

	bool isConnection(int idx) const;
	int getRodForNode(int idx) const;
	int getConForNode(int idx) const;

	const Con& getCon(int i) const { assert(i >= 0 && i < this->getNumCon()); return this->m_vcons[i]; }

	Rod& getRod(int i, const string& pname = "");
	const Rod& getRod(int i, const string& pname = "") const;
	Vector3d getConPoint(int i, const string& pname = "") const;

	void setConPoint(int i, const Vector3d& p, const string& pname = "");

	const Matrix3d& getRot(int i, const string& pname = "") const;
	void setRot(int i, const Matrix3d& vn, const string& pname = "");

	const pair<int, int>& getRodCons(int i) const { assert(i >= 0 && i < getNumRod()); return this->m_vrod2con[i]; }

	int getMeshNodeForOffset(int idxOffset) const;
	int getMeshEdgeForOffset(int idxOffset) const;

	void getRodNodeForMeshNode(int idx, int& rodIdx, int& nodeIdx) const;
	void getRodEdgeForMeshEdge(int idx, int& rodIdx, int& edgeIdx) const;

	void getMeshNodeForRodNode(int idx, int rodIdx, int& nodeIdx) const;
	void getMeshEdgeForRodEdge(int idx, int rodIdx, int& nodeIdx) const;

	const iVector& getConEdges(int i) const { assert(i >= 0 && i < getNumCon()); return this->m_vmapConEdge[i]; }
	const iVector& getConNodes(int i) const { assert(i >= 0 && i < getNumCon()); return this->m_vmapConNode[i]; }

	int getNumNode() const { int count = 0; for (int i = 0; i < this->getNumRod(); ++i) count += this->m_vprods.at("")[i].getNumNode(); return count; }
	int getNumEdge() const { int count = 0; for (int i = 0; i < this->getNumRod(); ++i) count += this->m_vprods.at("")[i].getNumEdge(); return count; }

	void setPoint(int i, const Vector3d& p, const string& pname = "");
	void setRadius(int i, const Vector2d& r, const string& pname = "");
	void setFrame(int i, const Frame3d& F, const string& pname = "");
	Vector3d getPoint(int i, const string& pname = "") const;
	Vector2d getRadius(int i, const string& pname = "") const;
	Frame3d getFrame(int i, const string& pname = "") const;

	const vector<pair<int, int>>& getRod2Con() const { return this->m_vrod2con; }
	const vector<iVector>& getSplitToWholeMap() const { return this->m_vsplitToWhole; }
	const vector<iVector>& getWholeToSplitMap() const { return this->m_vwholeToSplit; }

	void resampleRods();

protected:

	void removeCon(int i);
	void disconnectRod(int i);
	void connectRod(int i, Real tol, const string& pname = "");

	vector<Con> m_vcons;
	vector<string> m_vprops;
	map<string, vector<Rod>> m_vprods;
	map<string, vector<Matrix3d>> m_vprots;

	iVector m_rodEdgeCount;
	iVector m_rodNodeCount;

	// Metadata 
	vector<pair<int, int>> m_vrod2con;
	vector<iVector> m_vsplitToWhole;
	vector<iVector> m_vwholeToSplit;
	vector<iVector> m_vmapConEdge;
	vector<iVector> m_vmapConNode;

};

#endif
