/*=====================================================================================*/
/*! 
/file		Curve.cpp
/author		jesusprod
/brief		Implementation of Curve.h
 */
/*=====================================================================================*/

#include <JSandbox/Geometry/Curve.h>

Curve::Curve()
{
	this->m_nv = 0;
	this->m_ne = 0;
	this->m_isClosed = false;
}

Curve::Curve(const dVector& vx, bool isClosed)
{
	this->initialize(vx, isClosed);
}

Curve::Curve(const Curve& toCopy)
{
	this->initialize(toCopy.m_vx, toCopy.m_isClosed); 
}

void Curve::initialize(const dVector& vx, bool isClosed)
{
	assert(vx.size()%3 == 0);

	if (vx.empty())
	{
		this->m_nv = 0;
		this->m_ne = 0;
		this->m_isClosed = false;
	}

	this->m_vx = vx;
	this->m_nv = (int)vx.size()/3;
	this->m_isClosed = isClosed;

	if (this->m_isClosed)
		this->m_ne = this->m_nv;
	else this->m_ne = this->m_nv - 1;
}

void Curve::resample(double tol, const Vector3d& tanIni, const Vector3d& tanEnd)
{
	vector<Vector3d> vpOut;

	vector<Vector3d> vpIn(this->m_nv);
	for (int i = 0; i < this->m_nv; ++i)
		vpIn[i] = get3D(i, this->m_vx);

	// Add closure
	if (this->m_isClosed)
		vpIn.push_back(vpIn.front());

	hermiteInterpolation(vpIn, vpOut, this->m_isClosed, tol, tanIni, tanEnd);

	// Remove closure
	if (this->m_isClosed)
		vpOut.pop_back();

	this->m_nv = (int)vpOut.size();
	this->m_vx.resize(3*this->m_nv);

	// Set coordinates vector
	for (int i = 0; i < this->m_nv; ++i)
		set3D(i, vpOut[i], this->m_vx);

	if (this->m_isClosed)
		this->m_ne = this->m_nv;
	else this->m_ne = this->m_nv - 1;
}

void Curve::resample(int nPoint, const Vector3d& tanIni, const Vector3d& tanEnd)
{
	vector<Vector3d> vpOut;

	vector<Vector3d> vpIn(this->m_nv);
	for (int i = 0; i < this->m_nv; ++i)
		vpIn[i] = get3D(i, this->m_vx);

	// Add closure
	if (this->m_isClosed)
	{
		nPoint++; // Consider extra
		vpIn.push_back(vpIn.front());
	}

	hermiteInterpolation(vpIn, vpOut, this->m_isClosed, nPoint, tanIni, tanEnd);

	// Remove closure
	if (this->m_isClosed) 
		vpOut.pop_back(); 

	this->m_nv = (int)vpOut.size();
	this->m_vx.resize(3*this->m_nv);

	// Set coordinates vector
	for (int i = 0; i < this->m_nv; ++i)
		set3D(i, vpOut[i], this->m_vx);

	if (this->m_isClosed)
		this->m_ne = this->m_nv;
	else this->m_ne = this->m_nv - 1;
}

Vector3d Curve::evaluatePoint(const ModelPoint& mp) const
{
	assert(mp.getNumDim() == 3);

	if (mp.isExplicit())
	{
		return this->getPosition(mp.getPointIdx());
	}
	else
	{
		assert(mp.getNumSup() == 2);

		int v0 = mp.getIndices()[0];
		int v1 = mp.getIndices()[1];
		
		// Check if it's OK
		if (!this->m_isClosed)
		{
			assert(v1 != 0);
			assert(v0 <= v1);
		}

		Vector3d p0 = this->getPosition(v0);
		Vector3d p1 = this->getPosition(v1);
		return p0*mp.getWeights()[0] + p1*mp.getWeights()[1];
	}
}

void Curve::getPointEmbedding(const Vector3d& op, Real maxD, PointPro& pP) const
{
	dVector vs = getNodeParameters();

	pP.m_D = HUGE_VAL;
	pP.m_oriPoint = op;
	pP.m_valid = false;

	// Check vertex projections

	for (int i = 0; i < this->m_nv; ++i)
	{
		Vector3d pp = this->getPosition(i);
		Real D = (pp - op).squaredNorm();
		if (D < pP.m_D)
		{
			pP.m_D = D;
			pP.m_proPoint = pp;
			pP.m_valid = (D < maxD);
			pP.m_point = ModelPoint(3, i);
		}
	}

	// Check edge projections

	for (int i = 0; i < this->m_ne; ++i)
	{
		int pi, ni;
		getEdgeNeighborNodes(i, pi, ni);
		Vector3d pv = this->getPosition(pi);
		Vector3d nv = this->getPosition(ni);

		Vector3d e = nv - pv;
		Vector3d r = op - pv;
		Vector3d en = e.normalized();

		// Project onto the edge vector
		Vector3d pp = pv + en.dot(r)*en; 

		double s = -1;

		if (abs(e.x()) > 0.0) s = (op.x() - pv.x()) / e.x();
		else if (abs(e.y()) > 0.0) s = (op.y() - pv.y()) / e.y();
		else if (abs(e.z()) > 0.0) s = (op.z() - pv.z()) / e.z();
		else assert(false); // What�s going on here?

		if (s >= 0 && s <= 1.0)
		{
			Real D = (pp - op).squaredNorm();
			if (D < pP.m_D)
			{
				pP.m_D = D;
				pP.m_proPoint = pp;
				pP.m_valid = (D < maxD);

				iVector vindices(2);
				dVector vweights(2);
				vindices.push_back(pi);
				vindices.push_back(ni);
				vweights.push_back(1-s);
				vweights.push_back(s);

				pP.m_point = ModelPoint(3, vindices, vweights);
			}
		}
	}

	// TODO: This might be optimized using some kind
	// of accelerating structure to look up positions.
}

void Curve::getPointsEmbedding(const vector<Vector3d>& vop, Real maxD, vector<PointPro>& vpP) const
{
	int np = (int) vop.size();
	
	// Project all
	vpP.resize(np);
	for (int i = 0; i < np; ++i)
		this->getPointEmbedding(vop[i], maxD, vpP[i]);

	// TODO: This might be optimized using some kind
	// of accelerating structure to look up positions.
}

bool Curve::projectPoint(const Eigen::Vector3d& opoint, Eigen::Vector3d& ppoint, Real& D, Real& t, Real tol) const
{
	dVector vs = getNodeParameters();

	Real mint = HUGE_VAL;
	Real minD = HUGE_VAL;
	Vector3d minppoint;

	// Check vertex projections

	for (int i = 0; i < this->m_nv; ++i)
	{
		ppoint = this->getPosition(i);
		D = (ppoint - opoint).norm();
		if (D < minD)
		{
			minD = D;
			mint = vs[i];
			minppoint = ppoint;
		}
	}

	Real L = this->getLength();

	// Check edge projections

	for (int i = 0; i < this->m_ne; ++i)
	{
		int pi, ni;
		getEdgeNeighborNodes(i, pi, ni);
		Vector3d pv = this->getPosition(pi);
		Vector3d nv = this->getPosition(ni);

		Vector3d e = nv - pv;
		Vector3d r = opoint - pv;
		Vector3d en = e.normalized();
		ppoint = pv + en.dot(r)*en; // Project

		double s = -1;

		if (abs(e.x()) > 0.0) s = (opoint.x() - pv.x()) / e.x();
		else if (abs(e.y()) > 0.0) s = (opoint.y() - pv.y()) / e.y();
		else if (abs(e.z()) > 0.0) s = (opoint.z() - pv.z()) / e.z();
		else assert(false); // What�s going on here?

		if (s >= 0 && s <= 1.0)
		{ 
			D = (ppoint - opoint).norm();
			if (D < minD)
			{
				minD = D;
				minppoint = ppoint;
				if (this->m_isClosed && i == m_ne - 1)
					mint = t = vs[pi] + (L - vs[pi])*s;
				else mint = t = vs[pi] + (vs[ni] - vs[pi])*s;
			}
		}
	}

	t = mint;
	D = minD;
	ppoint = minppoint;

	return minD <= tol;
}

Eigen::Vector3d Curve::samplePosition(Real t, int& vi, int& ei, Real tol) const
{
	dVector vs = getNodeParameters();
	Real avgLength = this->getLength();
	avgLength /= this->getNumEdge();

	vi = -1;
	ei = -1;

	int minIdx = -1;
	Real minDist = HUGE_VAL;
	for (int i = 0; i < this->m_nv; ++i)
	{
		Real D = abs(vs[i] - t);
		if (D < tol*avgLength)
		{
			if (D < minDist)
			{
				minIdx = i;
				minDist = D;
			}
		}
	}

	if (minIdx != -1)
	{
		vi = minIdx;
		return this->getPosition(minIdx);
	}

	Real L = this->getLength();

	for (int i = 0; i < this->m_ne; ++i)
	{
		int pi, ni;
		getEdgeNeighborNodes(i, pi, ni);

		if (this->m_isClosed && i == m_ne - 1)
		{
			if (t >= vs[pi] && t <= L)
			{
				ei = i; // Set out sampled edge
				vi = -1; // Set out vertex null

				Real srange = L - vs[pi];
				Vector3d pv = this->getPosition(pi);
				Vector3d nv = this->getPosition(ni);
				Real wp = (t - vs[pi]) / srange;
				Real wn = (L - t) / srange;
				return wp*nv + wn*pv;
			}
		}
		else
		{
			if (t >= vs[pi] && t <= vs[ni])
			{
				ei = i; // Set out sampled edge
				vi = -1; // Set out vertex null

				Real srange = vs[ni] - vs[pi];
				Vector3d pv = this->getPosition(pi);
				Vector3d nv = this->getPosition(ni);
				Real wp = (t - vs[pi]) / srange;
				Real wn = (vs[ni] - t) / srange;
				return wp*nv + wn*pv;
			}
		}
	}

	return Eigen::Vector3d();
}

void Curve::remove(int i) // Remove vertex i
{
	dVector::const_iterator begin = this->m_vx.begin();
	this->m_vx.erase(begin + 3*i, begin + 3*i + 2);
	this->m_nv--;
	this->m_ne--;
}

void Curve::insertBack(const Vector3d& x) // Insert new vertex at end
{
	this->m_vx.insert(this->m_vx.end(), 3, 0);
	set3D(this->m_nv, x, this->m_vx);
	this->m_nv++;
	this->m_ne++;
}

void Curve::insertFront(const Vector3d& x) // Insert new vertex at beginning
{
	this->m_vx.insert(this->m_vx.begin(), 3, 0);
	set3D(0, x, this->m_vx);
	this->m_nv++;
	this->m_ne++;
}

int Curve::insertAtNode(int i, const Vector3d& x) // Insert new vertex at position
{
	validateNodeIndex(i);

	this->m_vx.insert(this->m_vx.begin() + 3*i, 3, 0);
	set3D(i, x, this->m_vx);
	this->m_nv++;
	this->m_ne++;

	return i;
}

int Curve::insertAtEdge(int ei, const Vector3d& x) // Insert new vertex at edge
{
	validateEdgeIndex(ei);

	int pv, nv;
	this->getEdgeNeighborNodes(ei, pv, nv);

	if (nv == 0) 
		nv = this->m_nv; // Insert vertex at curve end
	this->m_vx.insert(this->m_vx.begin() + 3*nv, 3, 0);
	set3D(nv, x, this->m_vx);
	this->m_nv++;
	this->m_ne++;

	return nv;
}

void Curve::getNodeNeighborEdges(int vi, int &pi, int &ni) const
{
	validateNodeIndex(vi);

	if (this->m_isClosed)
	{
		ni = vi;
		if (vi != 0)
			pi = vi - 1;
		else pi = m_ne - 1;
	}
	else
	{
		pi = -1;
		ni = -1;

		if (vi == m_nv - 1)
			pi = vi - 1;
		else if (vi == 0)
			ni = 0;
		else
		{
			pi = vi - 1;
			ni = vi;
		}
	}
}

void Curve::getNodeNeighborNodes(int vi, int &pi, int &ni) const
{
	validateNodeIndex(vi);

	if (this->m_isClosed)
	{
		if (vi != 0)
			pi = vi - 1;
		else pi = m_nv - 1;

		if (vi != m_nv - 1)
			ni = vi + 1;
		else ni = 0;
	}
	else
	{
		pi = -1;
		ni = -1;

		if (vi == m_nv - 1)
			pi = vi - 1;
		else if (vi == 0)
			ni = vi + 1;
		else
		{
			pi = vi - 1;
			ni = vi + 1;
		}
	}
}

void Curve::getEdgeNeighborNodes(int ei, int &pi, int &ni) const
{
	validateEdgeIndex(ei);

	if (this->m_isClosed)
	{
		pi = ei;
		if (ei != m_ne -1)
			ni = ei + 1;
		else ni = 0;
	}
	else
	{
		ni = ei + 1;
		pi = ei;
	}
}

void Curve::getEdgeNeighborEdges(int ei, int &pi, int &ni) const
{
	validateEdgeIndex(ei);

	if (this->m_isClosed)
	{
		if (ei != 0)
			pi = ei - 1;
		else ei = m_ne - 1;

		if (ei != m_ne - 1)
			ni = ei + 1;
		else ni = 0;
	}
	else
	{
		pi = -1;
		ni = -1;

		if (ei == m_ne - 1)
			pi = ei - 1;
		else if (ei == 0)
			ni = ei + 1;
		else
		{
			pi = ei - 1;
			ni = ei + 1;
		}
	}
}

int Curve::getNodeCircularIndex(int vi) const
{
	validateNodeIndex(vi);

	if (this->m_isClosed)
	{
		if (vi >= 0)
			return vi%m_nv;
		else return m_nv + vi%m_nv;
	}
	else if (vi < 0 || vi >= m_nv)
		return -1; // No circular
	
	return vi;
}

int Curve::getEdgeCircularIndex(int ei) const
{
	validateEdgeIndex(ei);

	if (this->m_isClosed)
	{
		if (ei >= 0)
			return ei%m_ne;
		else return m_ne + ei%m_ne;
	}
	else if (ei < 0 || ei >= m_ne)
		return -1; // No circular
	
	return ei;
}

dVector Curve::getNodeParameters() const
{
	dVector vs(this->m_nv);

	vs[0] = 0;
	double lengthT = 0.0;
	Vector3d prevPos = this->getPosition(0);
	for (int i = 1; i < this->m_nv; ++i)
	{
		Vector3d p = this->getPosition(i);
		lengthT += (p - prevPos).norm();
		vs[i] = lengthT;
		prevPos = p;
	}

	return vs;
}

dVector Curve::getEdgeParameters() const
{
	dVector vs(this->m_ne);

	vs[0] = 0;
	double lengthT = 0.0;
	double prevLength = this->getEdge(0).norm();
	for (int i = 1; i < this->m_ne; ++i)
	{
		lengthT += prevLength/2;
		prevLength = this->getEdge(i).norm();
		lengthT += prevLength/2;
		vs[i] = lengthT;
	}

	return vs;
}



dVector Curve::getEdge01Parametrization() const
{
	dVector vs(this->m_ne);

	// 0, 1\n, 2\n, 3\n, 4\n, 5\n, 6\n, ..., n\n
	double inc = 1.0 / (double) (this->m_ne - 1);
	for (int i = 0; i < this->m_ne; ++i)
		vs[i] = i * inc;

	return vs;
}

dVector Curve::getVertex01Parametrization() const
{
	dVector vs(this->m_nv);

	// 0, 1\n, 2\n, 3\n, 4\n, 5\n, 6\n, ..., n\n
	double inc = 1.0 / (double) (this->m_nv - 1);
	for (int i = 0; i < this->m_nv; ++i)
		vs[i] = i * inc;

	return vs;
}

double Curve::getLength() const
{
	if (this->m_nv < 2)
		return 0; // Null

	double length = 0;

	Vector3d pPrev = get3D(0, this->m_vx);
	for (int i = 1; i < this->m_nv; ++i)
	{
		Vector3d p = get3D(i, this->m_vx);
		length += (p - pPrev).norm();
		pPrev = p;
	}

	if (this->m_isClosed) // Add distance between first and last points of the curve
		length += (get3D(0, this->m_vx) - get3D(this->m_nv - 1, this->m_vx)).norm();

	return length;
}

void Curve::getEdgesLength(dVector& vs) const
{
	vs.resize(this->m_ne);
	for (int i = 0; i < this->m_ne; ++i)
		vs[i] = this->getEdge(i).norm();
}

Vector3d Curve::getEdge(int ei) const
{
	validateEdgeIndex(ei);

	int pv, nv;
	getEdgeNeighborNodes(ei, pv, nv);
	return get3D(nv, this->m_vx) - get3D(pv, this->m_vx);
}

Vector3d Curve::getEdgeTangent(int ei) const
{
	return getEdge(ei).normalized();
}

Vector3d Curve::getNodeTangent(int vi) const
{
	validateNodeIndex(vi);

	int pe, ne;
	getNodeNeighborEdges(vi, pe, ne);
	return (getEdge(pe) + getEdge(ne)).normalized();
}

Vector3d Curve::getCurvature(int vi) const
{
	validateNodeIndex(vi);

	int pe, ne;
	getNodeNeighborEdges(vi, pe, ne);
	return computeCurvatureBinormal(getEdge(pe), getEdge(ne));
}

void Curve::Split(const Curve& cin, const vector<SplitPoint>& vpoints, vector<Curve>& vcurves)
{
	vector<SplitPoint> vpointsSorted = vpoints;

	std::sort(vpointsSorted.begin(), 
			  vpointsSorted.end());

	// Add surrounding split points
	int ne = cin.getNumEdge();

	SplitPoint iniReal = vpointsSorted[0]; // Get original
	SplitPoint endReal = vpointsSorted[vpoints.size() - 1];

	SplitPoint ini;
	ini.m_edgeIdx = 0;
	ini.m_edgeMu = 0.0;

	SplitPoint end;
	end.m_edgeMu = 1.0;
	end.m_edgeIdx = ne - 1;

	bool cutStart = true;
	if (iniReal.m_edgeIdx != ini.m_edgeIdx || abs(iniReal.m_edgeMu - ini.m_edgeMu) > EPS_POS)
	{
		vpointsSorted.insert(vpointsSorted.begin(), ini);
		cutStart = false;
	}

	bool cutEnd = true;
	if (endReal.m_edgeIdx != end.m_edgeIdx || abs(endReal.m_edgeMu - end.m_edgeMu) > EPS_POS)
	{
		vpointsSorted.insert(vpointsSorted.end(), end);
		cutEnd = false;
	}

	bool cutExtreme = cutStart || cutEnd;

	vcurves.clear();
	int nc = (int)vpointsSorted.size();
	// Get intermediate splitted curves
	for (int i = 1; i < nc - 2; i++)
	{
		vcurves.push_back(Curve());
		Curve::Split(cin, vpointsSorted[i], vpointsSorted[i + 1], vcurves.back());
	}

	Curve first;
	Curve last;
	Curve::Split(cin, vpointsSorted[0], vpointsSorted[1], first);
	Curve::Split(cin, vpointsSorted[nc - 2], vpointsSorted[nc - 1], last);

	if (cin.getIsClosed() && !cutExtreme)
	{
		vcurves.push_back(Curve()); // Join first - last
		Curve::Merge(last, first, vcurves.back());
	}
	else
	{
		vcurves.push_back(first);
		if (vpointsSorted.size() > 2)
			vcurves.push_back(last);
	}
}

void Curve::Split(const Curve& cin, const SplitPoint& sp0, const SplitPoint& sp1, Curve& cout)
{
	assert(sp0.m_edgeIdx < sp1.m_edgeIdx || (sp0.m_edgeIdx == sp1.m_edgeIdx && sp0.m_edgeMu < sp1.m_edgeMu));

	Curve toSplit(cin.m_vx, false);
	if (cin.m_isClosed) // Repeat first index
		toSplit.insertBack(cin.getPosition(0));

	int inc = 0;

	// Get index 0
	int ci0 = -1;
	if (sp0.m_edgeMu > EPS_POS && sp0.m_edgeMu < 1 - EPS_POS)
	{
		int pvi, nvi;
		cin.getEdgeNeighborNodes(sp0.m_edgeIdx, pvi, nvi);
		Vector3d A = get3D(pvi, toSplit.m_vx);
		Vector3d B = get3D(nvi, toSplit.m_vx);

		Vector3d P = A + (B - A)*sp0.m_edgeMu; // Add
		ci0 = toSplit.insertAtEdge(sp0.m_edgeIdx, P);

		inc++; // Vertex added
	}
	else
	{
		int pvi, nvi;
		toSplit.getEdgeNeighborNodes(sp0.m_edgeIdx, pvi, nvi);
		ci0 = (sp0.m_edgeMu > EPS_POS) ? nvi : pvi; // Next or previous
	}

	// Get index 1
	int ci1 = -1;
	if (sp1.m_edgeMu > EPS_POS && sp1.m_edgeMu < 1 - EPS_POS)
	{
		int pvi, nvi;
		cin.getEdgeNeighborNodes(sp1.m_edgeIdx, pvi, nvi);
		Vector3d A = get3D(pvi + inc, toSplit.m_vx);
		Vector3d B = get3D(nvi + inc, toSplit.m_vx);
		Vector3d P = A + (B - A)*sp1.m_edgeMu; // Add

		ci1 = toSplit.insertAtEdge(sp1.m_edgeIdx + inc, P);
	}
	else
	{
		int pvi, nvi;
		toSplit.getEdgeNeighborNodes(sp1.m_edgeIdx + inc, pvi, nvi);
		ci1 = (sp1.m_edgeMu > EPS_POS) ? nvi : pvi; // Next or previous
	}

	int nv = ci1 - ci0 + 1;
	dVector vx(nv); // Get new set of point
	getSubvector(3 * ci0, 3 * nv, toSplit.m_vx, vx);
	cout.initialize(vx, false); // Always open
}

void Curve::Merge(const Curve& cin0, const Curve& cin1, Curve& cout)
{
	int n1 = cin1.m_nv;
	int n0 = cin0.m_nv - 1;

	dVector vx0;
	dVector vx1;
	getSubvector(0, 3 * n0, cin0.m_vx, vx0);
	getSubvector(0, 3 * n1, cin1.m_vx, vx1);

	dVector vx(3 * (n0 + n1));
	setSubvector(0, 3 * n0, vx0, vx);
	setSubvector(3 * n0, 3 * n1, vx1, vx);

	cout.initialize(vx, false);
}

bool Curve::Intersect(const vector<Curve>& vcs, vector<vector<SplitPoint>>& vsps, Real tolerance)
{
	int nc = (int) vcs.size();

	bool intersected = false;

	vsps.clear();
	vsps.resize(nc);
	for (int i = 0; i < nc; i++)
		vsps[i].clear(); // Restart

	for (int c0 = 0; c0 < nc; c0++)
	{
		const Curve& curve0 = vcs[c0];

		for (int c1 = c0 + 1; c1 < nc; c1++)
		{
			const Curve& curve1 = vcs[c1]; 

			vector<vector<SplitPoint>> vspPair(2);

			if (Curve::Intersect(curve0, curve1, vspPair, tolerance))
			{
				vector<SplitPoint>& vsp0 = vspPair[0];
				vector<SplitPoint>& vsp1 = vspPair[1];

				for (int i = 0; i < (int) vsp0.size(); i++)
				{
					SplitPoint& sp0 = vsp0[i];

					bool alreadyAdded = false;
					for (int j = 0; j < (int) vsps[c0].size(); j++)
					{
						SplitPoint& sp0Added = vsps[c0][j];
						if ((sp0.m_edgeIdx == sp0Added.m_edgeIdx && abs(sp0.m_edgeMu - sp0Added.m_edgeMu) < EPS_APPROX) || // Same intersection added
							(sp0.m_edgeIdx == sp0Added.m_edgeIdx + 1 && sp0.m_edgeMu < EPS_APPROX && (1 - sp0Added.m_edgeMu) < EPS_APPROX)) // In next
						{
							alreadyAdded = true;
							break; // Out of for
						}
					}

					if (alreadyAdded) continue; // Get out of here

					vsps[c0].push_back(sp0);
				}

				for (int i = 0; i < (int) vsp1.size(); i++)
				{
					SplitPoint& sp1 = vsp1[i];

					bool alreadyAdded = false;
					for (int j = 0; j < (int) vsps[c1].size(); j++)
					{
						SplitPoint& sp1Added = vsps[c1][j];
						if ((sp1.m_edgeIdx == sp1Added.m_edgeIdx && abs(sp1.m_edgeMu - sp1Added.m_edgeMu) < EPS_APPROX) || // Same intersection added
							(sp1.m_edgeIdx == sp1Added.m_edgeIdx + 1 && sp1.m_edgeMu < EPS_APPROX && (1 - sp1Added.m_edgeMu) < EPS_APPROX)) // In next
						{
							alreadyAdded = true;
							break; // Out of for
						}
					}

					if (alreadyAdded) continue; // Get out of here

					vsps[c1].push_back(sp1);
				}
			}
		}
	}

	return intersected;
}

bool Curve::Intersect(const Curve& cin0, const Curve& cin1, vector<vector<SplitPoint>>& vsps, Real tolerance)
{
	vector<SplitPoint>& vsp0 = vsps[0];
	vector<SplitPoint>& vsp1 = vsps[1];

	Real absTol0 = tolerance*cin0.getLength();
	Real absTol1 = tolerance*cin1.getLength();
	Real absTol = max(absTol0, absTol1);

	//int nv0 = cin0.getNumNode();
	//int nv1 = cin1.getNumNode();
	int ne0 = cin0.getNumEdge();
	int ne1 = cin1.getNumEdge(); 
	//bool intersected = false;

	vector<SplitPair> vsplitPair;

	for (int e0 = 0; e0 < ne0; e0++)
	{
		int pi0, ni0;
		cin0.getEdgeNeighborNodes(e0, pi0, ni0);
		Vector3d a0 = cin0.getPosition(pi0);
		Vector3d b0 = cin0.getPosition(ni0);

		Real minDist = HUGE_VAL;
		SplitPoint minsp0;
		SplitPoint minsp1;

		for (int e1 = 0; e1 < ne1; e1++)
		{
			int pi1, ni1; 
			cin1.getEdgeNeighborNodes(e1, pi1, ni1);
			Vector3d a1 = cin1.getPosition(pi1);
			Vector3d b1 = cin1.getPosition(ni1);
					
			double mu0, mu1;
			if (!lineLineIntersect(toSTL(a0), toSTL(b0), 
								   toSTL(a1), toSTL(b1), 
								   mu0, mu1, absTol))
				continue; // Too far to be intersected

			// Compute distance

			Vector3d p0 = a0*(1 - mu0) + b0*(mu0);
			Vector3d p1 = a1*(1 - mu1) + b1*(mu1);
			Real dist = (p0 - p1).squaredNorm();
			if (dist < minDist)
			{
				minDist = dist;
				minsp0.m_edgeIdx = e0;
				minsp1.m_edgeIdx = e1;
				minsp0.m_edgeMu = mu0;
				minsp1.m_edgeMu = mu1;
			}
		}

		if (minDist != HUGE_VAL)
		{
			SplitPair sp;
			sp.m_sp0 = minsp0;
			sp.m_sp1 = minsp1;
			sp.m_distance = minDist;
			vsplitPair.push_back(sp);
		}
	}

	//bool isCycle0 = cin0.getIsClosed() || ((cin0.getPosition(0) - cin0.getPosition(nv0 - 1)).norm() < EPS_POS);
	//bool isCycle1 = cin1.getIsClosed() || ((cin1.getPosition(0) - cin1.getPosition(nv1 - 1)).norm() < EPS_POS);

	int numSplitPair = (int) vsplitPair.size();

	bVector vtoDelete(numSplitPair, false);

	for (int i = 0; i < numSplitPair; ++i)
	{
		if (vtoDelete[i])
			continue;

		SplitPair& pair0 = vsplitPair[i];

		for (int j = i + 1; j < numSplitPair; ++j)
		{
			if (vtoDelete[j])
				continue;

			SplitPair& pair1 = vsplitPair[j];

			bool conflict = false;

			conflict = conflict || pair0.m_sp0.m_edgeIdx == pair1.m_sp0.m_edgeIdx;
			conflict = conflict || pair0.m_sp0.m_edgeIdx == pair1.m_sp0.m_edgeIdx - 1;
			conflict = conflict || pair0.m_sp0.m_edgeIdx == pair1.m_sp0.m_edgeIdx + 1;

			conflict = conflict || pair0.m_sp1.m_edgeIdx == pair1.m_sp1.m_edgeIdx;
			conflict = conflict || pair0.m_sp1.m_edgeIdx == pair1.m_sp1.m_edgeIdx - 1;
			conflict = conflict || pair0.m_sp1.m_edgeIdx == pair1.m_sp1.m_edgeIdx + 1;
		
			if (conflict)
			{
				if (pair0.m_distance < pair1.m_distance)
				{
					vtoDelete[j] = true;
				}
				else
				{
					vtoDelete[i] = true;
				}
			}
		}
	}

	for (int i = numSplitPair - 1; i >= 0; --i)
		if (vtoDelete[i])
			vsplitPair.erase(vsplitPair.begin() + i);

	numSplitPair = (int) vsplitPair.size();
	for (int i = 0; i < numSplitPair; ++i)
	{
		vsp0.push_back(vsplitPair[i].m_sp0);
		vsp1.push_back(vsplitPair[i].m_sp1);
	}

	assert(vsp0.size() == vsp1.size());

	return (vsp0.size() > 0) && (vsp1.size() > 0);

	//sort(vsp0.begin(), vsp0.end());
	//sort(vsp1.begin(), vsp1.end());
	//vsp0.erase(unique(vsp0.begin(), vsp0.end()), vsp0.end());
	//vsp1.erase(unique(vsp1.begin(), vsp1.end()), vsp1.end());

	//bVector vtoDelete0(vsp0.size(), false);
	//bVector vtoDelete1(vsp1.size(), false);

	//bool isCycle0 = cin0.getIsClosed() || ((cin0.getPosition(0) - cin0.getPosition(nv0 - 1)).norm() < EPS_POS);
	//bool isCycle1 = cin1.getIsClosed() || ((cin1.getPosition(0) - cin1.getPosition(nv1 - 1)).norm() < EPS_POS);

	//int nsp0 = (int)vsp0.size();
	//for (int i = 1; i < nsp0; ++i)
	//{
	//	if (vsp0[i - 1].m_edgeIdx == vsp0[i].m_edgeIdx && abs(vsp0[i - 1].m_edgeMu - vsp0[i].m_edgeMu) < 10*absTol)
	//		vtoDelete0[i] = true;

	//	if (vsp0[i - 1].m_edgeIdx + 1 == vsp0[i].m_edgeIdx && 1 - vsp0[i - 1].m_edgeMu + vsp0[i].m_edgeMu < 10*absTol)
	//		vtoDelete0[i] = true;
	//}

	//if (vsp0.size() >= 2 && isCycle0)
	//	if (vsp0.front().m_edgeIdx == 0 && vsp0.back().m_edgeIdx == ne0 - 1 && 1 - vsp0.back().m_edgeMu + vsp0.front().m_edgeMu < 10*absTol)
	//		vtoDelete0[0] = true;

	//int nsp1 = (int)vsp1.size();
	//for (int i = 1; i < nsp1; ++i)
	//{
	//	if (vsp1[i - 1].m_edgeIdx == vsp1[i].m_edgeIdx && abs(vsp1[i - 1].m_edgeMu - vsp1[i].m_edgeMu) < 10*absTol)
	//		vtoDelete1[i] = true;

	//	if (vsp1[i - 1].m_edgeIdx + 1 == vsp1[i].m_edgeIdx && 1 - vsp1[i - 1].m_edgeMu + vsp1[i].m_edgeMu < 10*absTol)
	//		vtoDelete1[i] = true;
	//}

	//if (vsp1.size() >= 2 && isCycle1)
	//	if (vsp1.front().m_edgeIdx == 0 && vsp1.back().m_edgeIdx == ne1 - 1 && 1 - vsp1.back().m_edgeMu + vsp1.front().m_edgeMu < 10*absTol)
	//		vtoDelete1[0] = true;

	//for (int i = nsp0 - 1; i >= 0; --i)
	//	if (vtoDelete0[i])
	//		vsp0.erase(vsp0.begin() + i);

	//for (int i = nsp1 - 1; i >= 0; --i)
	//	if (vtoDelete1[i])
	//		vsp1.erase(vsp1.begin() + i);

	//assert(vsp0.size() == vsp1.size());

	//return (vsp0.size() > 0) && (vsp1.size() > 0);
}

void Curve::FramePTForward(const Frame3d& f0, const Curve& curve, vector<Frame3d>& frames)
{
	assert(frames.size() == curve.getNumEdge());

	int ne = curve.getNumEdge();

	frames[0] = f0;
	for (int i = 1; i < ne; i++)
		frames[i] = parallelTransport(frames[i - 1], curve.getEdgeTangent(i));
}

void Curve::FramePTBackward(const Frame3d& fn, const Curve& curve, vector<Frame3d>& frames)
{
	assert(frames.size() == curve.getNumEdge());

	int ne = curve.getNumEdge();

	frames[ne - 1] = fn;
	for (int i = ne - 2; i >= 0; i--)
		frames[i] = parallelTransport(frames[i + 1], curve.getEdgeTangent(i));
}