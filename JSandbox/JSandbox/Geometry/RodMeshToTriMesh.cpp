///*=====================================================================================*/
///*!
///file		RodMeshToTriMesh.cpp
///author		jesusprod
///brief		Implementation of RodMeshToTriMesh
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Geometry/RodMeshToTriMesh.h>
//
//#include <JSandbox/Geometry/TriMesh.h>
//#include <JSandbox/Geometry/RodMesh.h>
//
//RodMeshToTriMesh::RodMeshToTriMesh()
//{
//	this->m_paramLengthResample = true;
//	
//	this->m_paramLengthUseSubdiv = true;
//	this->m_paramSectionUseSubdiv = true;
//
//	this->m_paramLengthDetail = 0.01f;
//	this->m_paramLengthSubdiv = 10;
//
//	this->m_paramSectionDetail = 0.01f;
//	this->m_paramSectionSubdiv = 10;
//
//	this->m_initialized = false;
//}
//
//RodMeshToTriMesh::~RodMeshToTriMesh()
//{
//	// Nothing to do here...
//}
//
//void RodMeshToTriMesh::create(const RodMesh& rodMesh, TriMesh& triMesh)
//{
//	this->m_pRodMesh = &rodMesh;
//
//	this->createMeshes();
//	this->createMerged();
//
//	this->m_initialized = true;
//
//	triMesh = TriMesh(this->m_merged_vx, this->m_merged_vi);
//}
//
//void RodMeshToTriMesh::update(const RodMesh& rodMesh, const dVector& vrodColor, bool offset, TriMesh& triMesh, dVector& vtriColor)
//{
//	assert(this->m_initialized);
//
//	this->m_pRodMesh = &rodMesh;
//
//	this->updateMeshes(vrodColor, offset);
//	this->updateMerged(vrodColor, offset);
//
//	triMesh.setPoints(this->m_merged_vx);
//
//	// Add color
//	if (!vrodColor.empty())
//		vtriColor = this->m_merged_vc;
//}
//
//bool RodMeshToTriMesh::createTriMesh(const RodMesh& rodMesh, TriMesh& triMesh, string& strError)
//{
//	this->m_pRodMesh = &rodMesh;
//
//	this->createMeshes();
//	this->createMerged();
//	this->updateMeshes();
//	this->updateMerged();
//
//	// Copy mesh to output
//	
//	triMesh.init(this->m_merged_vx, this->m_merged_vi);
//
//	return true;
//}
//
//void RodMeshToTriMesh::createSquareSection(vector<Vector2d>& vsecPoints)
//{
//	vsecPoints.resize(4);
//
//	// Initialize section points
//
//	vsecPoints[0].x() = -1;
//	vsecPoints[0].y() = -1;
//
//	vsecPoints[1].x() = -1;
//	vsecPoints[1].y() = 1;
//
//	vsecPoints[2].x() = 1;
//	vsecPoints[2].y() = 1;
//
//	vsecPoints[3].x() = 1;
//	vsecPoints[3].y() = -1;
//}
//
//void RodMeshToTriMesh::createEllipseSection(vector<Vector2d>& vsecPoints)
//{
//	int sec_nv = -1;
//
//	if (this->m_paramSectionUseSubdiv)
//		sec_nv = (int) this->m_paramSectionSubdiv;
//	else sec_nv = (int) ceil(2*M_PI/m_paramSectionDetail);
//
//	vsecPoints.resize(sec_nv);
//
//	double da = (2 * M_PI)/sec_nv;
//
//	dVector sec_va(sec_nv);
//	for (int i = 0; i < sec_nv; i++)
//		sec_va[i] = i*da; // Angles
//
//	// Initialize section points
//
//	for (int s = 0; s < sec_nv; s++)
//	{
//		Vector2d p;
//		p.x() = cos(sec_va[s]);
//		p.y() = sin(sec_va[s]);
//		vsecPoints[s] = p;
//	}
//}
//
//void RodMeshToTriMesh::createMeshes()
//{
//	const RodMesh& rodMesh = *this->m_pRodMesh;
//
//	int nr = rodMesh.getNumRod();
//	int nc = rodMesh.getNumCon();
//
//	//if (!this->m_addConnectionCaps)
//	//{
//		//this->m_vcur_nv.resize(nr + nc);
//		//this->m_vsec_nv.resize(nr + nc);
//		//this->m_vmeshes_nv.resize(nr + nc);
//		//this->m_vmeshes_ni.resize(nr + nc);
//		//this->m_vmeshes_vi.resize(nr + nc);
//		//this->m_vmeshes_vx.resize(nr + nc);
//		//this->m_vmeshes_vc.resize(nr + nc);
//	//}
//	//else
//	//{
//		this->m_vcur_nv.resize(3*nr);
//		this->m_vsec_nv.resize(3*nr);
//		this->m_vmeshes_nv.resize(3*nr);
//		this->m_vmeshes_ni.resize(3*nr);
//		this->m_vmeshes_vx.resize(3*nr);
//		this->m_vmeshes_vi.resize(3*nr);
//		this->m_vmeshes_vc.resize(3*nr);
//	//}
//
//	this->m_vrodInt.resize(nr);
//
//	for (int r = 0; r < nr; ++r)
//	{
//		int& cur_nv = this->m_vcur_nv[r];
//		int& sec_nv = this->m_vsec_nv[r];
//		int& mesh_nv = this->m_vmeshes_nv[r];
//		int& mesh_ni = this->m_vmeshes_ni[r];
//		dVector& mesh_vx = this->m_vmeshes_vx[r];
//		dVector& mesh_vc = this->m_vmeshes_vc[r];
//		iVector& mesh_vi = this->m_vmeshes_vi[r];
//		int count = 0;
//
//		if (rodMesh.getRod(r).getRadius()[0].isZero(1e-6))
//		{
//			cur_nv = 0;
//			sec_nv = 0;
//			mesh_nv = 0;
//			mesh_ni = 0;
//			mesh_vc.clear();
//			mesh_vx.clear();
//			mesh_vi.clear();
//			continue;
//		}
//
//		Rod rod = rodMesh.getRod(r);
//
//		if (this->m_paramLengthResample)
//		{
//			// Length interpolation
//
//			if (this->m_paramLengthUseSubdiv)
//			{
//				if (!rod.getCurve().getIsClosed()) 
//					rod.resample(m_paramLengthSubdiv);
//				else rod.resample(m_paramLengthSubdiv + 1);
//			}
//			else
//			{
//				rod.resample(m_paramLengthDetail);
//			}
//		}
//
//		cur_nv = this->m_vrodInt[r] = rod.getNumNode();
//
//		// Initialize section
//
//		vector<Vector2d> vsecPoints;
//		switch (this->m_sectionType)
//		{
//		case SectionType::Ellipse:
//			this->createEllipseSection(vsecPoints);
//			break;
//		case SectionType::Square:
//			this->createSquareSection(vsecPoints);
//			break;
//		default:
//			throw new exception("Should not be here...");
//		}
//
//		sec_nv = (int)vsecPoints.size();
//
//		// Initialize values vectors
//
//		if (!rod.getCurve().getIsClosed())
//			mesh_nv = sec_nv*cur_nv + 2;
//		else mesh_nv = sec_nv*cur_nv;
//		mesh_vx.resize(3*mesh_nv);
//		mesh_vc.resize(mesh_nv);
//		mesh_vi.clear();
//
//		// Initialize indices
//
//		count = 0;
//
//		int iniOffset = 0;
//		
//		if (!rod.getCurve().getIsClosed())
//		{
//			iniOffset = 1;
//
//			// First fan
//			for (int k = 0; k < sec_nv - 1; k++)
//			{
//				// Triangle 1 (inverse)
//				mesh_vi.push_back(2 + k);
//				mesh_vi.push_back(1 + k);
//				mesh_vi.push_back(0);
//
//				count += 3;
//			}
//
//			////// Triangle 1 (inverse) 
//			////mesh_vi.push_back(1);
//			////mesh_vi.push_back(sec_nv);
//			////mesh_vi.push_back(0);
//
//			////count += 3;
//		}
//		else
//		{
//			iniOffset = 0;
//		}
//
//		for (int k = 0; k < cur_nv - 1; k++)
//		{
//			int offset = iniOffset + k*sec_nv; // Offset
//
//			for (int s = 0; s < sec_nv - 1; s++)
//			{
//				// Triangle 1
//				mesh_vi.push_back(offset + s);
//				mesh_vi.push_back(offset + s + 1);
//				mesh_vi.push_back(offset + s + sec_nv);
//
//				// Triangle 2
//				mesh_vi.push_back(offset + s + sec_nv);
//				mesh_vi.push_back(offset + s + 1);
//				mesh_vi.push_back(offset + s + sec_nv + 1);
//
//				count += 6;
//			}
//
//			// Close strip triangle 1
//			mesh_vi.push_back(offset + sec_nv - 1);
//			mesh_vi.push_back(offset);
//			mesh_vi.push_back(offset + 2 * sec_nv - 1);
//
//			// Close strip triangle 2
//			mesh_vi.push_back(offset + 2 * sec_nv - 1);
//			mesh_vi.push_back(offset);
//			mesh_vi.push_back(offset + sec_nv);
//
//			count += 6;
//		}
//
//		int offset = iniOffset + sec_nv * cur_nv;
//
//		if (!rod.getCurve().getIsClosed())
//		{
//			// Last fan
//			for (int k = 0; k < sec_nv - 1; k++)
//			{
//				// Triangle 1
//				mesh_vi.push_back(offset);
//				mesh_vi.push_back(offset - sec_nv + k);
//				mesh_vi.push_back(offset - sec_nv + k + 1);
//
//				count += 3;
//			}
//
//			//// Triangle 1
//			//mesh_vi.push_back(offset);
//			//mesh_vi.push_back(offset - 1);
//			//mesh_vi.push_back(offset - sec_nv);
//		
//			//count += 3;
//		}
//		else
//		{
//			for (int s = 0; s < sec_nv - 1; s++)
//			{
//				// Triangle 1
//				mesh_vi.push_back(offset - sec_nv + s);
//				mesh_vi.push_back(offset - sec_nv + s + 1);
//				mesh_vi.push_back(s);
//
//				// Triangle 2
//				mesh_vi.push_back(s);
//				mesh_vi.push_back(offset - sec_nv + s + 1);
//				mesh_vi.push_back(s + 1);
//
//				count += 6;
//			}
//
//			// Close strip triangle 1
//			mesh_vi.push_back(offset - 1);
//			mesh_vi.push_back(offset - sec_nv);
//			mesh_vi.push_back(0);
//
//			// Close strip triangle 2
//			mesh_vi.push_back(offset - 1);
//			mesh_vi.push_back(0);
//			mesh_vi.push_back(sec_nv - 1);
//
//			count += 6;
//		}
//
//		mesh_ni = count;
//	}
//
//	// Add connections
//
//	int nconi = (int) this->m_connectionCaps.n_faces() * 3;
//	int nconv = (int) this->m_connectionCaps.n_vertices();
//
//	bVector vconMesh(nc, false);
//
//	for (int i = 0; i < nr; ++i)
//	{
//		pair<int, int> rodCons = this->m_pRodMesh->getRodCons(i);
//
//		if (rodCons.first != -1 && !vconMesh[rodCons.first])
//		{
//			int offset = nr + 2 * i + 0;
//			this->m_vmeshes_ni[offset] = nconi;
//			this->m_vmeshes_nv[offset] = nconv;
//			this->m_connectionCaps.getIndices(this->m_vmeshes_vi[offset]);
//			this->m_connectionCaps.getPoints(this->m_vmeshes_vx[offset]);
//			this->m_vmeshes_vc[offset].resize(nconv);
//
//			vconMesh[rodCons.first] = true;
//		}
//		else
//		{
//			int offset = nr + 2 * i + 0;
//			this->m_vmeshes_ni[offset] = 0;
//			this->m_vmeshes_nv[offset] = 0;
//			this->m_vmeshes_vi[offset].clear();
//			this->m_vmeshes_vx[offset].clear();
//			this->m_vmeshes_vc[offset].clear();
//		}
//
//		if (rodCons.second != -1 && !vconMesh[rodCons.second])
//		{
//			int offset = nr + 2 * i + 1;
//			this->m_vmeshes_ni[offset] = nconi;
//			this->m_vmeshes_nv[offset] = nconv;
//			this->m_connectionCaps.getIndices(this->m_vmeshes_vi[offset]);
//			this->m_connectionCaps.getPoints(this->m_vmeshes_vx[offset]);
//			this->m_vmeshes_vc[offset].resize(nconv);
//
//			vconMesh[rodCons.second] = true;
//		}
//		else
//		{
//			int offset = nr + 2 * i + 1;
//			this->m_vmeshes_ni[offset] = 0;
//			this->m_vmeshes_nv[offset] = 0;
//			this->m_vmeshes_vi[offset].clear();
//			this->m_vmeshes_vx[offset].clear();
//			this->m_vmeshes_vc[offset].clear();
//		}
//	}
//
//	//if (this->m_addConnectionCaps)
//	//{
//	//
//	//}
//}
//
//void computeRotation(const Frame3d& F0, const Frame3d& F1, Matrix3d& R)
//{
//	Matrix3d mQ;
//	mQ.setZero();
//
//	Matrix3d mA;
//	mA.setZero();
//
//	mQ += F0.tan*F0.tan.transpose() +
//		  F0.nor*F0.nor.transpose() +
//		  F0.bin*F0.bin.transpose();
//
//	mA += F1.tan*F0.tan.transpose() +
//		  F1.nor*F0.nor.transpose() +
//		  F1.bin*F0.bin.transpose();
//
//	Matrix3d S;
//	Matrix3d B = mA * mQ.inverse();
//	if (computePolarDecomposition(B, R, S, EPS_POS) < 0.0)
//	{
//		R.setIdentity();
//		S.setIdentity();
//	}
//}
//
//void RodMeshToTriMesh::updateMeshes(const dVector& vcolors, bool offset)
//{
//	const RodMesh& rodMesh = *this->m_pRodMesh;
//
//	int nr = rodMesh.getNumRod();
//	int nc = rodMesh.getNumCon();
//
//	int nrCount = 0;
//
//	for (int r = 0; r < nr; ++r)
//	{
//		if (rodMesh.getRod(r).getRadius()[0].isZero(1e-9))
//		{
//			continue;
//		}
//
//		const int& cur_nv = this->m_vcur_nv[r];
//		const int& sec_nv = this->m_vsec_nv[r];
//		dVector& mesh_vx = this->m_vmeshes_vx[r];
//		dVector& mesh_vc = this->m_vmeshes_vc[r];
//
//		int count = 0;
//
//		Rod rod = rodMesh.getRod(r);
//
//		dVector vsRodRaw;
//		rod.getCurve().getEdgesLength(vsRodRaw);
//		int nvRodRaw = rod.getCurve().getNumNode();
//		dVector vnodeParRaw = rod.getCurve().getNodeParameters();
//
//		rod.resample(this->m_vrodInt[r]);
//
//		dVector vsRodInt;
//		rod.getCurve().getEdgesLength(vsRodInt);
//		int nvRodInt = rod.getCurve().getNumNode();
//		dVector vnodeParInt = rod.getCurve().getNodeParameters();
//
//		bVector vsplitsInt(nvRodInt, false);
//
//		if (!this->m_vsplits.empty())
//		{
//			for (int i = 0; i < (int) this->m_vsplits[r].size(); ++i)
//			{
//				Real splitParRaw = vnodeParRaw[this->m_vsplits[r][i]];
//				
//				int minInt;
//				Real minD = HUGE_VAL;
//				for (int k = 0; k < nvRodInt; ++k)
//				{
//					Real D = abs(splitParRaw - vnodeParInt[k]);
//					if (D < minD)
//					{
//						minD = D;
//						minInt = k;
//					}
//				}
//
//				vsplitsInt[minInt] = true;
//			}
//		}
//
//		dVector vcolorsRod(nvRodRaw);
//		dVector vcolorsInt(nvRodInt);
//
//		if (!vcolors.empty())
//		{
//			getSubvector(nrCount, nvRodRaw, vcolors, vcolorsRod);
//
//			vector<Vector2d> vcolRod(nvRodRaw);
//			vector<Vector2d> vcolInt(nvRodInt);
//
//			for (int j = 0; j < nvRodRaw; ++j) // Prepare interpolation
//				vcolRod[j] = Vector2d(vcolorsRod[j], vcolorsRod[j]);
//
//			hermiteInterpolation(vcolRod, vsRodRaw, vcolInt, false, nvRodInt);
//			
//			for (int j = 0; j < nvRodInt; ++j)
//				vcolorsInt[j] = vcolInt[j].x();
//		}
//
//		const Curve& curve = rod.getCurve();
//
//		vector<Frame> vFNew;
//		vector<Vector2d> vrNew;
//		rod.getNodeFrames(vFNew);
//		rod.getNodeRadius(vrNew);
//
//		// Section interpolation
//
//		vector<Vector2d> vsecPoints;
//		switch (this->m_sectionType)
//		{
//		case SectionType::Ellipse:
//			this->createEllipseSection(vsecPoints);
//			break;
//		case SectionType::Square:
//			this->createSquareSection(vsecPoints);
//			break;
//		default:
//			throw new exception("Should not be here...");
//		}
//
//		// Update values
//
//		if (!rod.getCurve().getIsClosed())
//		{
//			if (!vcolors.empty())
//				mesh_vc[count] = vcolorsInt.front();
//			set3D(count++, curve.getPosition(0), mesh_vx);
//		}
//		
//		int rodMeshCount = 0;
//
//		for (int k = 0; k < cur_nv; k++)
//		{
//			Frame3d Fk = vFNew[k];
//			Vector2d rk = vrNew[k];
//			Vector3d ck = curve.getPosition(k);
//
//			// Leave a mark!
//			if (vsplitsInt[k])
//				rk.y() = 0.0;
//
//			for (int s = 0; s < sec_nv; s++)
//			{
//				// Get section displacement
//				Vector2d d = vsecPoints[s];
//
//				if (offset)
//				{
//					set3D(count,
//						ck +
//						Fk.bin*d.x()*rk[0] +
//						Fk.nor*d.y()*rk[1] +
//						Fk.nor*rk[1],
//						mesh_vx);
//				}
//				else
//				{
//					set3D(count,
//						ck +
//						Fk.bin*d.x()*rk[0] +
//						Fk.nor*d.y()*rk[1],
//						mesh_vx);
//				}
//
//				if (!vcolors.empty())
//					mesh_vc[count] = vcolorsInt[rodMeshCount];
//
//				count++;
//			}
//
//			rodMeshCount++;
//		}
//
//		if (!rod.getCurve().getIsClosed())
//		{
//			if (!vcolors.empty())
//				mesh_vc[count] = vcolorsInt.back();
//			set3D(count++, curve.getPosition(cur_nv - 1), mesh_vx);
//		}
//
//		nrCount += nvRodRaw;
//	}
//
//	Matrix3d Rb;
//
//	Frame3d F0;
//	F0.tan = Vector3d(1.0, 0.0, 0.0);
//	F0.nor = Vector3d(0.0, 0.0, 1.0);
//	F0.bin = F0.tan.cross(F0.nor);
//
//	Frame3d F1;
//	F1.tan = Vector3d(1.0, 0.0, 0.0);
//	F1.nor = Vector3d(0.0, 1.0, 0.0);
//	F1.bin = F1.tan.cross(F1.nor);
//
//	computeRotation(F0, F1, Rb);
//
//	bVector vconMesh(nc, false);
//
//	for (int i = 0; i < nr; ++i)
//	{
//		pair<int, int> rodCons = this->m_pRodMesh->getRodCons(i);
//
//		const Rod& rod = this->m_pRodMesh->getRod(i);
//
//		int nv = rod.getNumNode();
//		int ne = rod.getNumEdge();
//
//		if (rodCons.first != -1 && !vconMesh[rodCons.first])
//		{
//			Matrix3d R = this->m_pRodMesh->getRot(rodCons.first);
//			if (R.isZero(1e-6))
//				R.setIdentity();
//			Matrix3d S; S.setZero();
//			Real rw = rod.getRadius(0).x();
//			Real rh = rod.getRadius(0).y();
//			Vector3d t = rod.getPosition(0);
//			Vector3d c0 = Vector3d(0.0, 0.0, -0.5);
//			S(0, 0) = (2 * rw);
//			S(1, 1) = (2 * rw);
//			S(2, 2) = (2 * rh);
//			Vector3d c1;
//			if (offset)
//				c1 = R*Rb*S*Vector3d(0.0, 0.0, 0.5);
//			else c1 = R*Rb*S*Vector3d(0.0, 0.0, 0.0);
//
//			// Transform the points
//			int off = nr + 2 * i + 0;
//			this->m_connectionCaps.getPoints(this->m_vmeshes_vx[off]);
//			for (int j = 0; j < this->m_vmeshes_nv[off]; ++j)
//				set3D(j, t + c1 + R*Rb*S*(get3D(j, this->m_vmeshes_vx[off]) + c0), this->m_vmeshes_vx[off]);
//
//			if (!vcolors.empty())
//				for (int j = 0; j < m_vmeshes_nv[off]; ++j)
//					this->m_vmeshes_vc[off][j] = 0.0;
//
//			vconMesh[rodCons.first] = true;
//		}
//
//		if (rodCons.second != -1 && !vconMesh[rodCons.second])
//		{
//			Matrix3d R = this->m_pRodMesh->getRot(rodCons.second);
//			if (R.isZero(1e-6))
//				R.setIdentity();
//			Matrix3d S; S.setZero();
//			Real rw = rod.getRadius(ne-1).x();
//			Real rh = rod.getRadius(ne-1).y();
//			Vector3d t = rod.getPosition(nv-1);
//			Vector3d c0 = Vector3d(0.0, 0.0, -0.5);
//			S(0, 0) = (2 * rw);
//			S(1, 1) = (2 * rw);
//			S(2, 2) = (2 * rh);
//			Vector3d c1;
//			if (offset)
//				c1 = R*Rb*S*Vector3d(0.0, 0.0, 0.5);
//			else c1 = R*Rb*S*Vector3d(0.0, 0.0, 0.0);
//
//			// Transform
//			int off = nr + 2 * i + 1;
//			this->m_connectionCaps.getPoints(this->m_vmeshes_vx[off]);
//			for (int j = 0; j < this->m_vmeshes_nv[off]; ++j)
//				set3D(j, t + c1 + R*Rb*S*(get3D(j, this->m_vmeshes_vx[off]) + c0), this->m_vmeshes_vx[off]);
//
//			if (!vcolors.empty())
//			for (int j = 0; j < m_vmeshes_nv[off]; ++j)
//				this->m_vmeshes_vc[off][j] = 0.0;
//
//			vconMesh[rodCons.second] = true;
//		}
//	}
//}
//
//void RodMeshToTriMesh::createMerged()
//{
//	int nr = 0;
//
//	if (!this->m_addConnectionCaps)
//	{
//		nr = 1*this->m_pRodMesh->getNumRod();
//	}
//	else
//	{
//		nr = 3*this->m_pRodMesh->getNumRod();
//	}
//
//	this->m_merged_ni = 0;
//	this->m_merged_nv = 0;
//	for (int i = 0; i < nr; i++)
//	{
//		this->m_merged_ni += this->m_vmeshes_ni[i];
//		this->m_merged_nv += this->m_vmeshes_nv[i];
//	}
//
//	int bufInd = 0;
//	int bufVer = 0;
//	this->m_merged_vi.resize(this->m_merged_ni);
//	this->m_merged_vx.resize(3 * this->m_merged_nv);
//	this->m_merged_vc.resize(this->m_merged_nv);
//	for (int i = 0; i < nr; i++)
//	{
//		int nInds = this->m_vmeshes_ni[i];
//		int nVerts = this->m_vmeshes_nv[i];
//
//		for (int j = 0; j < nInds; j++) // Initialize considering previous
//			this->m_merged_vi[bufInd + j] = bufVer + this->m_vmeshes_vi[i][j];
//
//		bufInd += nInds;
//		bufVer += nVerts;
//	}
//}
//
//void RodMeshToTriMesh::updateMerged(const dVector& vcolors, bool offset)
//{
//	int nr = 0;
//
//	if (!this->m_addConnectionCaps)
//	{
//		nr = 1 * this->m_pRodMesh->getNumRod();
//	}
//	else
//	{
//		nr = 3 * this->m_pRodMesh->getNumRod();
//	}
//
//	int bufVer = 0;
//	for (int i = 0; i < nr; i++)
//	{
//		int nVerts = this->m_vmeshes_nv[i];
//
//		for (int j = 0; j < nVerts; j++) // Initialize considering previous
//		{
//			set3D(bufVer + j, get3D(j, m_vmeshes_vx[i]), m_merged_vx);
//			this->m_merged_vc[bufVer + j] = this->m_vmeshes_vc[i][j];
//		}
//
//		bufVer += nVerts;
//	}
//}