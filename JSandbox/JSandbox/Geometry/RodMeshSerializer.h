/*=====================================================================================*/
/*!
/file		RodMeshSerializer.h
/author		jesusprod
/brief		Declaration of RodMeshSerializer to serialize/deserialize Rod Meshes.
*/
/*=====================================================================================*/

#ifndef ROD_MESH_SERIALIZER_H
#define ROD_MESH_SERIALIZER_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/RodMesh.h>

class RodMeshSerializer
{
	static const string ROD_COUNT;

	static const string IS_LOOP;
	static const string NODE_COUNT;
	static const string EDGE_COUNT;

	static const string POS_SET;
	static const string NOR_SET;
	static const string RAD_SET;

public:

	RodMeshSerializer() {}
	~RodMeshSerializer() {}

	bool serialize(const string& fileName, const RodMesh& rodMesh, string& strError) const;
	bool deserialize(const string& fileName, RodMesh& rodMesh, string& strError) const;

};

#endif
