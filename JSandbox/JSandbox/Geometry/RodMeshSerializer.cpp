/*=====================================================================================*/
/*!
/file		RodMeshSerializer.cpp
/author		jesusprod
/brief		Implementation of RodMeshSerializer.h
*/
/*=====================================================================================*/

#include <fstream>

#include <JSandbox/Geometry/RodMeshSerializer.h>

const string RodMeshSerializer::ROD_COUNT = "#RODCOUNT";

const string RodMeshSerializer::IS_LOOP = "#ISLOOP";
const string RodMeshSerializer::NODE_COUNT = "#NODECOUNT";
const string RodMeshSerializer::EDGE_COUNT = "#EDGECOUNT";

const string RodMeshSerializer::POS_SET = "#POSSET";
const string RodMeshSerializer::NOR_SET = "#NORSET";
const string RodMeshSerializer::RAD_SET = "#RADSET";

bool RodMeshSerializer::serialize(const string& fileName, const RodMesh& rodMesh, string& strError) const
{
	int nr = rodMesh.getNumRod();

	ofstream outfile;
	outfile.open(fileName.c_str(), ios::out | ios::trunc);
	if (!outfile.is_open()) // Ensure the file name exists
	{
		strError = "Cannot open the file stream";

		outfile.close();
		return false;
	}

	outfile.precision(12);

	// Write individual rod data

	outfile << endl << RodMeshSerializer::ROD_COUNT << " " << nr;

	for (int i = 0; i < nr; ++i)
	{
		const Rod& rod = rodMesh.getRod(i);
		int nume = rod.getNumEdge();
		int numn = rod.getNumNode();

		const dVector& vpos = rod.getPositions();
		const vector<Frame>& vF = rod.getFrames();
		const vector<Vector2d>& vr = rod.getRadius();

		// Write counts

		outfile << endl << endl;

		outfile << endl << RodMeshSerializer::NODE_COUNT << " " << numn;

		outfile << endl << RodMeshSerializer::EDGE_COUNT << " " << nume;

		// Write loop

		outfile << endl << endl;

		outfile << endl << RodMeshSerializer::IS_LOOP << " " << rod.getCurve().getIsClosed();

		// Write centerline

		outfile << endl << endl;

		outfile << endl << RodMeshSerializer::POS_SET;

		for (int n = 0; n < numn; ++n)
		{
			int offset = 3 * n;
			outfile << endl << vpos[offset + 0];
			outfile << endl << vpos[offset + 1];
			outfile << endl << vpos[offset + 2];
		}

		// Write edge normals

		outfile << endl << endl;

		outfile << endl << RodMeshSerializer::NOR_SET;

		for (int e = 0; e < nume; ++e)
		{
			outfile << endl << vF[e].nor(0);
			outfile << endl << vF[e].nor(1);
			outfile << endl << vF[e].nor(2);
		}
		
		// Write edge radii

		outfile << endl << endl;

		outfile << endl << RodMeshSerializer::RAD_SET;

		for (int e = 0; e < nume; ++e)
		{
			outfile << endl << vr[e].x();
			outfile << endl << vr[e].y();
		}

	}

	// Write connection data
	
	// TODO: Include conncection data in the rod mesh

	outfile.close();
	return true;
}

bool RodMeshSerializer::deserialize(const string& fileName, RodMesh& rodMesh, string& strError) const
{
	ifstream infile;
	infile.open(fileName.c_str());
	if (!infile.is_open())
	{
		strError = "Cannot open the file stream";

		infile.close();
		return false;
	}

	infile.precision(12);

	string nextLine;

	// Read individual rod data

	infile >> nextLine;
	if (nextLine.compare(RodMeshSerializer::ROD_COUNT))
	{
		strError = "Expected rod count value";
		infile.close();
		return false;
	}

	int nr;
	infile >> nr;

	vector<Rod> vrods(nr);

	for (int r = 0; r < nr; ++r)
	{
		// Read counts

		infile >> nextLine;
		if (nextLine.compare(RodMeshSerializer::NODE_COUNT))
		{
			strError = "Expected node count value";

			infile.close();
			return false;
		}

		int numn;
		infile >> numn;

		infile >> nextLine;
		if (nextLine.compare(RodMeshSerializer::EDGE_COUNT))
		{
			strError = "Expected edge count value";

			infile.close();
			return false;
		}

		int nume;
		infile >> nume;

		// Read loop

		infile >> nextLine;
		if (nextLine.compare(RodMeshSerializer::IS_LOOP))
		{
			strError = "Expected loop value";

			infile.close();
			return false;
		}

		int isLoop;
		infile >> isLoop;
		if (isLoop != 0 && isLoop != 1)
		{
			strError = "Loop must only be 0 or 1";

			infile.close();
			return false;
		}

		// Read centerline

		int nvalues = 3*numn;
		dVector vpos(nvalues);

		infile >> nextLine;
		if (nextLine.compare(RodMeshSerializer::POS_SET))
		{
			infile.close();
			return false;
		}

		for (int i = 0; i < nvalues; ++i)
		{
			double p;
			infile >> p;
			vpos[i] = p;
		}

		// Read edge normals

		vector<Vector3d> vnor(nume);

		infile >> nextLine;
		if (nextLine.compare(RodMeshSerializer::NOR_SET))
		{
			infile.close();
			return false;
		}

		for (int i = 0; i < nume; ++i)
		{
			double x,y,z;
			infile >> x;
			infile >> y;
			infile >> z;
			vnor[i] = Vector3d(x, y, z);
		}

		// Read edge radii

		vector<Vector2d> vr(nume);

		infile >> nextLine;
		if (nextLine.compare(RodMeshSerializer::RAD_SET))
		{
			infile.close();
			return false;
		}

		for (int i = 0; i < nume; ++i)
		{
			double r0, r1;
			infile >> r0;
			infile >> r1;
			vr[i] = Vector2d(r0, r1);
		}

		// Build rod

		Rod& rod = vrods[r];

		vector<Frame> vF(nume);
		Curve curve(vpos, isLoop);
		
		for (int e = 0; e < nume; ++e)
		{
			vF[e].tan = curve.getEdgeTangent(e);

			vF[e].nor = vnor[e];
			vF[e].bin = vF[e].tan.cross(vnor[e]).normalized();
			vF[e].nor = vF[e].bin.cross(vF[e].tan); // Correct
		}

		rod.initialize(curve, vF, vr);
	}

	// Read connection data

	// TODO: Include conncection data in the rod mesh

	rodMesh.init(vrods); // Initialize rod mesh

	infile.close();
	return true;
}