///*=====================================================================================*/
///*!
///file		RodMeshToTriMesh.h
///author		jesusprod
//*/
///*=====================================================================================*/
//
//#ifndef RODMESH_TO_TRIMESH_H
//#define RODMESH_TO_TRIMESH_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/Geometry/TriMesh.h>
//#include <JSandbox/Geometry/RodMesh.h>
//
//class RodMeshToTriMesh
//{
//
//public:
//	enum SectionType
//	{
//		Square,
//		Ellipse
//	};
//
//public:
//
//	RodMeshToTriMesh();
//	virtual ~RodMeshToTriMesh();
//
//	void create(const RodMesh& rodMesh, TriMesh& triMesh);
//	void update(const RodMesh& rodMesh, const dVector& vrodColor, bool offset, TriMesh& triMesh, dVector& triColor);
//
//	virtual bool createTriMesh(const RodMesh& rodMesh, TriMesh& triMesh, string& strError);
//
//	virtual void setSplitPoints(const vector<iVector>& vi) { this->m_vsplits = vi; }
//	virtual const vector<iVector> getSplitPoints() const { return this->m_vsplits; }
//
//protected:
//
//	virtual void createSquareSection(vector<Vector2d>& vsecPoints);
//	virtual void createEllipseSection(vector<Vector2d>& vsecPoints);
//
//	void createMeshes();
//	void createMerged();
//	void updateMeshes(const dVector& vcolors = dVector(), bool offset = false);
//	void updateMerged(const dVector& vcolors = dVector(), bool offset = false);
//
//	bool getIsInitialized() const { return this->m_initialized; }
//
//public:
//
//	// Simulation parameters
//
//	bool m_paramLengthResample;
//
//	bool m_paramLengthUseSubdiv;
//	float m_paramLengthDetail;
//	int m_paramLengthSubdiv;
//
//	bool m_paramSectionUseSubdiv;
//	float m_paramSectionDetail;
//	int m_paramSectionSubdiv;
//
//	bool m_addConnectionCaps;
//	TriMesh m_connectionCaps;
//
//	SectionType m_sectionType;
//
//protected:
//
//	vector<iVector> m_vsplits;
//
//	bool m_initialized;
//
//	// Internal variables
//
//	const RodMesh* m_pRodMesh;
//
//	// Cach� variables
//
//	iVector m_vrodInt;
//
//	iVector m_vcur_nv;
//	iVector m_vsec_nv;
//	iVector m_vmeshes_nv;
//	iVector m_vmeshes_ni;
//	vector<dVector> m_vmeshes_vc;
//	vector<dVector> m_vmeshes_vx;
//	vector<iVector> m_vmeshes_vi;
//
//	int m_merged_nv;
//	int m_merged_ni;
//	dVector m_merged_vx;
//	dVector m_merged_vc;
//	iVector m_merged_vi;
//
//};
//
//#endif