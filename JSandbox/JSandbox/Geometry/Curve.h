/*=====================================================================================*/
/*! 
/file		Curve.h
/author		jesusprod
/brief		Declaration of Curve class (linear curve).
*/
/*=====================================================================================*/

#ifndef CURVE_H
#define CURVE_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

typedef struct SplitPoint
{
	int m_edgeIdx;
	double m_edgeMu;

	bool operator == (const SplitPoint& other) const
	{
		// Same indes
		if (this->m_edgeIdx == other.m_edgeIdx)
		{
			return isApprox(this->m_edgeMu, other.m_edgeMu, EPS_POS);
		}
		else
		{
			if (this->m_edgeIdx == other.m_edgeIdx + 1)
			{
				return isApprox(this->m_edgeMu, 0.0, EPS_POS) &&
					   isApprox(other.m_edgeMu, 1.0, EPS_POS);
			}
			if (this->m_edgeIdx == other.m_edgeIdx - 1)
			{
				return isApprox(this->m_edgeMu, 1.0, EPS_POS) &&
					   isApprox(other.m_edgeMu, 0.0, EPS_POS);
			}
		}

		return false;
	};

	bool operator != (const SplitPoint& other) const
	{
		return !(*this == other);
	};

	bool operator < (const SplitPoint& other) const
	{
		if (*this == other)
			return false;

		if ((this->m_edgeIdx < other.m_edgeIdx) || (this->m_edgeIdx == other.m_edgeIdx && this->m_edgeMu < other.m_edgeMu))
			return true;

		return false;
	}

} SplitPoint;

typedef struct SplitPair
{
	SplitPoint m_sp0;
	SplitPoint m_sp1;
	double m_distance;

} SplitPair;

class Curve
{

public:

	Curve();
	Curve(const dVector& vx, bool isClosed = false);
	void initialize(const dVector& vx, bool isClosed = false);

	Curve(const Curve& toCopy);

	void resample(double tol, const Vector3d& tanIni = Vector3d::Zero(), const Vector3d& tanEnd = Vector3d::Zero());
	void resample(int nPoint, const Vector3d& tanIni = Vector3d::Zero(), const Vector3d& tanEnd = Vector3d::Zero());

	void remove(int i);
	void insertBack(const Vector3d& x);
	void insertFront(const Vector3d& x);
	int insertAtNode(int i, const Vector3d& x);
	int insertAtEdge(int i, const Vector3d& x); 

	Eigen::Vector3d samplePosition(Real t, int& vi, int& ei, Real tol = 0.25) const;

	void setIsClosed(bool is) { this->m_isClosed = is; }
	bool getIsClosed() const { return this->m_isClosed; } 

	void getEdgeNeighborEdges(int ei, int& pi, int& ni) const;
	void getNodeNeighborEdges(int vi, int& pi, int& ni) const;
	void getEdgeNeighborNodes(int ei, int& pi, int& ni) const;
	void getNodeNeighborNodes(int vi, int& pi, int& ni) const;
	
	int getEdgeCircularIndex(int ei) const;
	int getNodeCircularIndex(int vi) const;
	
	double getLength() const;

	void getEdgesLength(dVector& vs) const;

	int getNumEdge() const { return this->m_ne; }
	int getNumNode() const { return this->m_nv; }

	const dVector& getPositions() const { return this->m_vx; }
	void setPositions(const dVector& vx) { validateSize(vx); this->m_vx = vx; }
	Eigen::Vector3d getPosition(int i) const { validateNodeIndex(i); return get3D(i, this->m_vx); }
	void setPosition(int i, const Eigen::Vector3d& x) { validateNodeIndex(i); set3D(i, x, this->m_vx); }

	Vector3d evaluatePoint(const ModelPoint& mp) const;
	void getPointEmbedding(const Vector3d& p, Real maxD, PointPro& pP) const;
	void getPointsEmbedding(const vector<Vector3d>& vp, Real maxD, vector<PointPro>& vpP) const;

	bool projectPoint(const Eigen::Vector3d& opoint, Eigen::Vector3d& ppoint, Real& D, Real& t, Real tol) const;
	
	Eigen::Vector3d getEdge(int i) const;
	Eigen::Vector3d getEdgeTangent(int i) const;
	Eigen::Vector3d getNodeTangent(int i) const;
	Eigen::Vector3d getCurvature(int i) const;
	
	dVector getEdgeParameters() const;
	dVector getNodeParameters() const;

	dVector getEdge01Parametrization() const;
	dVector getVertex01Parametrization() const;
		
	// Utilities

	static void Merge(const Curve& cin0, const Curve& cin1, Curve& cout);
	static void Split(const Curve& cin0, const vector<SplitPoint>& vpoints, vector<Curve>& vcurves);
	static void Split(const Curve& cin0, const SplitPoint& sp0, const SplitPoint& sp1, Curve& cout);

	static bool Intersect(const vector<Curve>& vcs, vector<vector<SplitPoint>>& vsps, Real tolerance = EPS_POS);
	static bool Intersect(const Curve& cin0, const Curve& cin, vector<vector<SplitPoint>>& vsps, Real tolerance = EPS_POS);

	static void FramePTForward(const Frame3d& f0, const Curve& curve, vector<Frame3d>& frames);
	static void FramePTBackward(const Frame3d& fn, const Curve& curve, vector<Frame3d>& frames);
	
	void validateNodeIndex(int i) const { assert(i >= 0 && i < this->m_nv); }
	void validateEdgeIndex(int i) const { assert(i >= 0 && i < this->m_ne); }
	void validateSize(const dVector& v) const { assert(v.size() == 3 * this->m_nv); }

public:

	int m_ne;					// N edges
	int m_nv;					// N vertices
	dVector m_vx;				// Coordinates
	bool m_isClosed;			// Is a closed?

};

#endif
