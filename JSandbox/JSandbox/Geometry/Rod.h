/*=====================================================================================*/
/*!
/file		Rod.h
/author		jesusprod
/brief		Declaration of Rod class (framed linear curve).
*/
/*=====================================================================================*/

#ifndef ROD_H
#define ROD_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/Geometry/Curve.h>

class Rod
{
public:

	Rod();
	Rod(const Curve& curve, const vector<Frame3d>& vF = vector<Frame>(), const vector<Vector2d>& = vector<Vector2d>());
	Rod(const Rod& toCopy);

	void initialize(const Curve& curve, const vector<Frame3d>& vF = vector<Frame>(), const vector<Vector2d>& vr = vector<Vector2d>());

	void resample(int nPoint);
	void resample(double tol);

	void remove(int i);
	void insertBack(const Vector3d& x);
	void insertFront(const Vector3d& x);
	int insertAtNode(int i, const Vector3d& x);
	int insertAtEdge(int i, const Vector3d& x);

	void initialFrames();
	void readaptFrames();

	void reverse();

	int getNumNode() const { return this->getCurve().getNumNode(); }
	int getNumEdge() const { return this->getCurve().getNumEdge(); }

	Curve& getCurve() { return this->m_center; }
	const Curve& getCurve() const { return this->m_center; }

	Vector3d getPosition(int i) const { return m_center.getPosition(i); }
	void setPosition(int i, const Vector3d& x) { m_center.setPosition(i, x); }
	const dVector& getPositions() const { return this->m_center.getPositions(); }
	void setPositions(const dVector& vx) { validateSizeNode((int) vx.size()); this->m_center.setPositions(vx); }

	const vector<Frame>& getFrames() const { return this->m_vF; }
	void setFrames(const vector<Frame3d>& vF) { validateSizeEdge((int) vF.size()); this->m_vF = vF; }
	const Frame3d& getFrame(int i) const { m_center.validateEdgeIndex(i); return this->m_vF[i]; }
	void setFrame(int i, const Frame3d& F) { m_center.validateEdgeIndex(i); this->m_vF[i] = F; }

	const vector<Vector2d>& getRadius() const { return this->m_vr; }
	void setRadius(const vector<Vector2d>& vr) { validateSizeEdge((int)vr.size()); this->m_vr = vr; }
	const Vector2d& getRadius(int i) const { m_center.validateEdgeIndex(i); return this->m_vr[i]; }
	void setRadius(int i, const Vector2d& r) { m_center.validateEdgeIndex(i); this->m_vr[i] = r; }

	void getNodeFrames(vector<Frame3d>& vF) const;
	void getNodeRadius(vector<Vector2d>& vr) const;

	static void MergeRods(const Rod& rin0, const Rod& rin1, Rod& rout);
	static void SplitRod(const Rod& rin, const vector<SplitPoint>& vpoints, vector<Rod>& vrods);
	static void SplitRod(const Rod& rin, const SplitPoint& sp0, const SplitPoint& sp1, Rod& rout);

	void validateSizeNode(int s) const { assert(s == 3*this->m_center.getNumNode()); }
	void validateSizeEdge(int s) const { assert(s == this->m_center.getNumEdge()); }

protected:

	Curve m_center;
	vector<Frame> m_vF;
	vector<Vector2d> m_vr;

};

#endif
