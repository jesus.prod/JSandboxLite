/*=====================================================================================*/
/*!
/file		RodMesh.cpp
/author		jesusprod
/brief		Implementation of RodMesh.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/RodMesh.h>

RodMesh::RodMesh()
{
	this->init(vector<Rod>());
}

RodMesh::RodMesh(const RodMesh& toCopy)
{
	this->m_vcons = toCopy.m_vcons;
	this->m_vprops = toCopy.m_vprops;
	this->m_vprods = toCopy.m_vprods;
	this->m_vprots = toCopy.m_vprots;
	this->m_vrod2con = toCopy.m_vrod2con;
	this->m_rodEdgeCount = toCopy.m_rodEdgeCount;
	this->m_rodNodeCount = toCopy.m_rodNodeCount;
	this->m_vsplitToWhole = toCopy.m_vsplitToWhole;
	this->m_vwholeToSplit = toCopy.m_vwholeToSplit;
	this->m_vmapConEdge = toCopy.m_vmapConEdge;
	this->m_vmapConNode = toCopy.m_vmapConNode;
}

RodMesh::RodMesh(const vector<Rod>& vr, const vector<string>& vp)
{
	this->init(vr, vp);
}

RodMesh::RodMesh(const vector<Rod>& vr, const vector<Con>& vc, const vector<Matrix3d>& vR, const vector<string>& vp)
{
	this->init(vr, vc, vR, vp);
}

void RodMesh::clearContent()
{
	int np = (int) this->m_vprops.size();
	for (int i = 0; i < np; ++i)
		this->m_vprods[this->m_vprops[i]].clear();

	this->m_vcons.clear();

	this->m_vrod2con.clear();

	this->m_rodEdgeCount.clear();
	this->m_rodEdgeCount.clear();

	this->m_vsplitToWhole.clear();
	this->m_vwholeToSplit.clear();

	this->m_vmapConEdge.clear();
	this->m_vmapConNode.clear();
}

void RodMesh::init(const vector<Rod>& vr, const vector<string>& vp)
{
	this->clearContent();

	Real tol = 1e-3;

	// Initialize properties

	this->m_vprops = vp;
	if (this->m_vprops.empty())
		this->m_vprops.push_back("");

	for (int i = 0; i < (int) this->m_vprops.size(); ++i)
		this->m_vprods[this->m_vprops[i]] = vector<Rod>();

	// Initialize content

	int nr = (int)vr.size();
	for (int i = 0; i < nr; ++i)
	{
		iVector vaddRod;
		iVector vdelRod;
		this->addRod(vr[i], tol, this->m_vprops[0], vaddRod, vdelRod);
	}

	// Update metadata

	this->checkConsistency();
	this->updateCache();
}

void RodMesh::init(const vector<Rod>& vr, const vector<Con>& vc, const vector<Matrix3d>& vR, const vector<string>& vp)
{
	this->clearContent();

	// Initialize properties

	this->m_vprops = vp;
	if (this->m_vprops.empty())
		this->m_vprops.push_back("");

	for (int i = 0; i < (int) this->m_vprops.size(); ++i)
	{
		this->m_vprods[this->m_vprops[i]] = vector<Rod>();
		this->m_vprots[this->m_vprops[i]] = vector<Matrix3d>();
	}

	// Initialize content

	for (int i = 0; i < (int) this->m_vprops.size(); ++i)
	{
		this->m_vprods[this->m_vprops[i]] = vr;

		if (vR.size() == vc.size())
		{
			this->m_vprots[this->m_vprops[i]] = vR;
		}
		else
		{
			this->m_vprots[this->m_vprops[i]].resize(vc.size());
			for (int j = 0; j < (int)vc.size(); ++j) // Init.
				this->m_vprots[this->m_vprops[i]][j].setZero();
		}
	}

	this->m_vcons = vc;

	// Update metadata

	this->checkConsistency();
	this->updateCache();
}

void RodMesh::checkConsistency()
{
	for (int i = 0; i < this->getNumCon(); ++i)
	{
		set<pair<int, int>>::iterator s_end = this->getCon(i).m_srods.end();
		set<pair<int, int>>::iterator s_begin = this->getCon(i).m_srods.begin();
		for (set<pair<int, int>>::iterator s_it = s_begin; s_it != s_end; s_it++)
		{
			if ((*s_it).second != 0 && (*s_it).second != 1)
			{
				logSimu("[WARNING] Invalid side identifier at rod mesh consistency check: %d", (*s_it).second);
			}
		}

		Vector3d centerPoint = this->getConPoint(i);

		for (set<pair<int, int>>::iterator it = this->m_vcons[i].m_srods.begin(); it != this->m_vcons[i].m_srods.end(); ++it)
		{
			if (it->first < 0 || it->first >= m_vprods.at("").size())
				continue;

			const Rod& rod = this->m_vprods.at("")[it->first];
			int nv = rod.getNumNode();
			if (it->second == 0)
			{
				Real dist = (rod.getPosition(0) - centerPoint).norm();
				if (dist > EPS_APPROX)
					logSimu("[WARNING] Connection nodes not consistent");
			}
			if (it->second == 1)
			{
				Real dist = (rod.getPosition(nv-1) - centerPoint).norm();
				if (dist > EPS_APPROX)
					logSimu("[WARNING] Connection nodes not consistent");
			}
		}
	}
}

void RodMesh::updateCache()
{
	string prop = this->m_vprops[0];

	// Count elements

	this->m_rodEdgeCount.clear();
	this->m_rodNodeCount.clear();

	int nr = this->getNumRod();
	int nc = this->getNumCon();

	int countNode = 0;
	int countEdge = 0;
	for (int i = 0; i < nr; ++i)
	{
		countNode += this->m_vprods[prop][i].getNumNode();
		countEdge += this->m_vprods[prop][i].getNumEdge();
		this->m_rodEdgeCount.push_back(countEdge);
		this->m_rodNodeCount.push_back(countNode);
	}

	// Update maps

	this->m_vrod2con.clear();
	this->m_vsplitToWhole.clear();
	this->m_vwholeToSplit.clear();

	this->m_vsplitToWhole.resize(countNode);

	// Connnection maps

	this->m_vrod2con.resize(nr);
	for (int i = 0; i < nr; ++i)
	{
		this->m_vrod2con[i].first = -1;
		this->m_vrod2con[i].second = -1;
	}

	for (int i = 0; i < nc; ++i)
	{
		set<pair<int, int>>::iterator cur_it = this->m_vcons[i].m_srods.begin();
		set<pair<int, int>>::iterator end_it = this->m_vcons[i].m_srods.end();
		for (; cur_it != end_it; ++cur_it)
		{
			if (cur_it->second == 0) // Initial extreme
			{
				this->m_vrod2con[cur_it->first].first = i;
			}
			else if (cur_it->second == 1) // Ending extreme
			{
				this->m_vrod2con[cur_it->first].second = i;
			}
			else
			{
				logSimu("[WARNING] Invalid side identifier at rod mesh cache update: %d", cur_it->second);
			}
		}
	}

	// Initialize connection nodes

	int wholeCount = 0;

	for (int i = 0; i < nc; ++i)
	{
		set<pair<int, int>>::iterator cur_it = this->m_vcons[i].m_srods.begin();
		set<pair<int, int>>::iterator end_it = this->m_vcons[i].m_srods.end();
		for (; cur_it != end_it; ++cur_it)
		{
			int rodIdx = cur_it->first;
			int nodIdx = (cur_it->second == 0) ? 0 : this->getRod(rodIdx, prop).getNumNode() - 1;
			int idx = -1;
			this->getMeshNodeForRodNode(nodIdx, rodIdx, idx);
			this->m_vsplitToWhole[idx].push_back(wholeCount);
		}

		wholeCount++;
	}

	// Initialize non-connection nodes

	for (int i = 0; i < nr; ++i)
	{
		const pair<int, int>& rodCons = this->m_vrod2con[i];

		int nv = this->getRod(i, prop).getNumNode();
		int iniIdx = (rodCons.first != -1)? 1 : 0;
		int endIdx = (rodCons.second != -1)? nv-1 : nv;

		for (int j = iniIdx; j < endIdx; ++j)
		{
			int rodIdx = i;
			int nodIdx = j;
			int idx = -1;
			this->getMeshNodeForRodNode(nodIdx, rodIdx, idx);
			this->m_vsplitToWhole[idx].push_back(wholeCount);
			wholeCount++;
		}
	}

	// Create inverse map

	this->m_vwholeToSplit.resize(wholeCount);

	for (int i = 0; i < countNode; ++i)
	{
		assert((int) this->m_vsplitToWhole[i].size() == 1);

		int wholeIdx = this->m_vsplitToWhole[i][0];
		this->m_vwholeToSplit[wholeIdx].push_back(i);
	}

	int edgeCount = 0;
	int nodeCount = 0;

	iVector vedgeIni(this->getNumRod());
	iVector vedgeEnd(this->getNumRod());
	vector<iVector> vnodeIni(this->getNumRod());
	vector<iVector> vnodeEnd(this->getNumRod());

	for (int i = 0; i < this->getNumRod(); ++i)
	{
		vedgeIni[i] = edgeCount;
		vnodeIni[i].push_back(nodeCount + 0);
		vnodeIni[i].push_back(nodeCount + 1);

		nodeCount += this->getRod(i, prop).getNumNode();
		edgeCount += this->getRod(i, prop).getNumEdge();

		vedgeEnd[i] = edgeCount - 1;
		vnodeEnd[i].push_back(nodeCount - 1);
		vnodeEnd[i].push_back(nodeCount - 2);
	}

	this->m_vmapConEdge.resize(nc);
	this->m_vmapConNode.resize(nc);

	for (int i = 0; i < this->getNumCon(); ++i)
	{
		int kEdge = 0;
		int kNode = 0;
		const Con& con = this->getCon(i);
		int nrcon = (int) con.m_srods.size();
		this->m_vmapConEdge[i].resize(1 * nrcon);
		this->m_vmapConNode[i].resize(2 * nrcon);
		set<pair<int, int>>::const_iterator itCur = con.m_srods.begin();
		set<pair<int, int>>::const_iterator itEnd = con.m_srods.end();
		for (; itCur != itEnd; ++itCur)
		{
			if (itCur->second == 0)
			{
				this->m_vmapConEdge[i][kEdge++] = vedgeIni[itCur->first];
				this->m_vmapConNode[i][kNode++] = vnodeIni[itCur->first][0];
				this->m_vmapConNode[i][kNode++] = vnodeIni[itCur->first][1];
			}
			if (itCur->second == 1)
			{
				this->m_vmapConEdge[i][kEdge++] = vedgeEnd[itCur->first];
				this->m_vmapConNode[i][kNode++] = vnodeEnd[itCur->first][0];
				this->m_vmapConNode[i][kNode++] = vnodeEnd[itCur->first][1];
			}
		}
	}
}

void RodMesh::InitializeFramesWorldAligned(RodMesh& src, const string& pnamesrc)
{
	int nr = src.getNumRod();
	int nc = src.getNumCon();
	
	for (int i = 0; i < nc; ++i)
	{
		src.m_vprots[pnamesrc][i].setIdentity();
	}

	for (int i = 0; i < nr; ++i)
	{
		Rod& rod = src.m_vprods[pnamesrc][i];

		int ne = rod.getNumEdge();
		for (int j = 0; j < ne; ++j)
		{
			Frame F;
			F.nor = Vector3d(0.0, 1.0, 0.0);
			F.tan = rod.getCurve().getEdgeTangent(j);
			F.bin = F.tan.cross(F.nor).normalized();
			F.nor = F.bin.cross(F.tan).normalized();
			rod.setFrame(j, F);
		}
	}
}

void RodMesh::InitializeFramesFromRotatedRodMesh(RodMesh& src, RodMesh& dst, const string& pnamesrc, const string& pnamedst)
{
	assert(src.getNumCon() == dst.getNumCon());
	assert(src.getNumRod() == dst.getNumRod());

	int nc = src.getNumCon();
	int nr = src.getNumRod();

	vector<Matrix3d> vconR(nc);

	// Compute connection rotation

	for (int i = 0; i < nc; ++i)
	{
		const Con& con = src.m_vcons[i];
		set<pair<int, int>>::iterator it_end = con.m_srods.end();
		set<pair<int, int>>::iterator it_cur = con.m_srods.begin();

		Matrix3d mQ;
		mQ.setZero();

		Matrix3d mA;
		mA.setZero();

		int nr = (int) con.m_srods.size();
		vector<Frame> vF0(nr);
		vector<Frame> vFx(nr);

		int count = 0;
		for (; it_cur != it_end; ++it_cur)
		{
			Frame3d Fx;
			Frame3d F0;

			if (it_cur->second == 0)
			{
				vF0[count] = src.getRod(it_cur->first, pnamesrc).getFrames().front();
				vFx[count] = dst.getRod(it_cur->first, pnamedst).getFrames().front();
			}
			if (it_cur->second == 1)
			{
				vF0[count] = src.getRod(it_cur->first, pnamesrc).getFrames().back();
				vFx[count] = dst.getRod(it_cur->first, pnamedst).getFrames().back();
			}
			count++;
		}

		for (int j = 0; j < nr; ++j)
		{
			mQ += vF0[j].tan*vF0[j].tan.transpose();
			mA += vFx[j].tan*vF0[j].tan.transpose();
		}

		if (isApprox(mQ.determinant(), 0.0, 1e-6))
		{
			for (int ii = 0; ii < nr; ++ii)
			for (int jj = ii + 1; jj < nr; ++jj)
			{
				if (isApprox(abs(vFx[ii].tan.dot(vFx[jj].tan)), 1.0, 1e-6)) continue;
				if (isApprox(abs(vF0[ii].tan.dot(vF0[jj].tan)), 1.0, 1e-6)) continue;

				Vector3d norCx = vFx[ii].tan.cross(vFx[jj].tan);
				Vector3d norC0 = vF0[ii].tan.cross(vF0[jj].tan);
				if (norC0.dot(vF0[ii].nor) < 0.0 &&
					norC0.dot(vF0[jj].nor) < 0.0)
				{
					norCx *= -1;
					norC0 *= -1;
				}
				Vector3d norC00 = norC0 - vF0[ii].tan*(norC0.dot(vF0[ii].tan));
				Vector3d norC0x = norCx - vFx[ii].tan*(norCx.dot(vFx[ii].tan));
				Vector3d norC10 = norC0 - vF0[jj].tan*(norC0.dot(vF0[jj].tan));
				Vector3d norC1x = norCx - vFx[jj].tan*(norCx.dot(vFx[jj].tan));
				mQ += norC00*norC00.transpose();
				mA += norC0x*norC00.transpose();
				mQ += norC10*norC10.transpose();
				mA += norC1x*norC10.transpose();
			}
		}

		if (isApprox(mQ.determinant(), 0.0, 1e-6))
		{
			logSimu("[WARNING] Couldn't compute proper connection rotation");
			return;
		}

		Matrix3d B = mA * mQ.inverse();
		Matrix3d R;
		Matrix3d S;
		if (computePolarDecomposition(B, R, S, EPS_POS) < 0.0)
		{
			R.setIdentity();
			S.setIdentity();
		}

		vconR[i] = R;
	}

	// Compute rod frames

	for (int i = 0; i < nr; ++i)
	{
		Rod& rod0 = src.getRod(i, pnamesrc);
		Rod& rod1 = dst.getRod(i, pnamedst);

		assert(rod0.getNumEdge() == rod1.getNumEdge());

		int ne = rod0.getNumEdge();

		const vector<Frame3d>& vF0 = rod0.getFrames();
		const vector<Frame3d>& vF1 = rod1.getFrames();

		vector<Frame3d> vF0ext(2);
		vF0ext[0] = vF0.front();
		vF0ext[1] = vF0.back();

		vector<Frame3d> vF1ext(2);
		vF1ext[0] = vF1.front();
		vF1ext[1] = vF1.back();

		if (src.m_vrod2con[i].first != -1 && src.m_vrod2con[i].second != -1)
		{
			const Curve& curve1 = rod1.getCurve();

			vF1ext[0] = parallelTransport(frameRotation(vF0ext[0], vconR[src.m_vrod2con[i].first]), vF1ext[0].tan);
			vF1ext[1] = parallelTransport(frameRotation(vF0ext[1], vconR[src.m_vrod2con[i].second]), vF1ext[1].tan);

			vector<Frame> vFPT(ne);
			vector<Frame> vF1N(ne);

			Curve::FramePTForward(vF1ext[0], curve1, vFPT);
			Vector3d refVec = vF1ext[1].tan;
			Vector3d conNor = vF1ext[1].nor;
			Vector3d rodNor = vFPT.back().nor;

			Real S = (rodNor.cross(conNor).dot(refVec) < 0) ? -1 : 1;
			Real cosa = rodNor.dot(conNor);
			if (cosa > 1.0) 
				cosa = 1.0;
			if (cosa < -1.0) 
				cosa = -1.0;
			Real angle = S*acos(cosa);
			Real inc = angle/(ne - 1);

			dVector vanew(ne);
			for (int j = 0; j < ne; ++j)
				vanew[j] = inc*j; // Uniform

			frameRotation(vFPT, vanew, vF1N);

			rod1.setFrames(vF1N);
			rod1.readaptFrames();
		}
		else if (src.m_vrod2con[i].first != -1 && src.m_vrod2con[i].second == -1)
		{
			vector<Frame> vFPT(ne);
			vF1ext[0] = frameRotation(vF0ext[0], vconR[src.m_vrod2con[i].first]);
			Curve::FramePTForward(vF1ext[0], rod1.getCurve(), vFPT);
			rod1.setFrames(vFPT);
		}
		else if (src.m_vrod2con[i].first == -1 && src.m_vrod2con[i].second != -1)
		{
			vector<Frame> vFPT(ne);
			vF1ext[1] = frameRotation(vF0ext[1], vconR[src.m_vrod2con[i].second]);
			Curve::FramePTBackward(vF1ext[1], rod1.getCurve(), vFPT);
			rod1.setFrames(vFPT);
		}
	}
}

bool RodMesh::InitializeFramesFromRotatedTangents(RodMesh& src, const vector<vector<Vector3d>>& vt, const string& pnamesrc)
{
	int nc = src.getNumCon();
	int nr = src.getNumRod();

	vector<Matrix3d> vconR(src.getNumCon());

	vector<Vector3d> vrodTan0(nr);
	vector<Vector3d> vrodTan1(nr);

	// Compute connection rotation

	for (int i = 0; i < nc; ++i)
	{
		const Con& con = src.m_vcons[i];
		set<pair<int, int>>::iterator it_end = con.m_srods.end();
		set<pair<int, int>>::iterator it_cur = con.m_srods.begin();

		Matrix3d mQ;
		mQ.setZero();

		Matrix3d mA;
		mA.setZero();

		int nr = (int)con.m_srods.size();
		vector<Vector3d> vt0(nr);
		vector<Vector3d> vtx(nr);

		int count = 0;
		for (; it_cur != it_end; ++it_cur)
		{
			if (it_cur->second == 0)
			{
				vtx[count] = src.getRod(it_cur->first, pnamesrc).getFrames().front().tan;
				vt0[count] = vrodTan0[it_cur->first] = vt[i][count];
			}
			if (it_cur->second == 1)
			{
				vtx[count] = src.getRod(it_cur->first, pnamesrc).getFrames().back().tan;
				vt0[count] = vrodTan1[it_cur->first] = vt[i][count];
			}
			count++;
		}

		for (int j = 0; j < nr; ++j)
		{
			mQ += vt0[j]*vt0[j].transpose();
			mA += vtx[j]*vt0[j].transpose();
		}

		if (isApprox(mQ.determinant(), 0.0, 1e-6))
		{
			logSimu("[INFO] Not enough tangents or planar connection %i. Adding normals", i);
			
			for (int ii = 0; ii < nr; ++ii)
				for (int jj = ii + 1; jj < nr; ++jj)
				{
					if (isApprox(abs(vtx[ii].dot(vtx[jj])), 1.0, 1e-6)) continue;
					if (isApprox(abs(vt0[ii].dot(vt0[jj])), 1.0, 1e-6)) continue;

					Vector3d norCx = vtx[ii].cross(vtx[jj]);
					Vector3d norC0 = vt0[ii].cross(vt0[jj]);

					mQ += norC0*norC0.transpose();
					mA += norCx*norC0.transpose();
				}
		}

		if (isApprox(mQ.determinant(), 0.0, 1e-6))
		{
			logSimu("[WARNING] Couldn't compute proper connection rotation");
			return false;
		}

		Matrix3d B = mA * mQ.inverse();
		Matrix3d R;
		Matrix3d S;
		if (computePolarDecomposition(B, R, S, EPS_POS) < 0.0)
		{
			R.setIdentity();
			S.setIdentity();
		}

		vconR[i] = R;
	}

	// Compute rod frames

	for (int i = 0; i < nr; ++i)
	{
		Rod& rod = src.getRod(i, pnamesrc);

		int ne = rod.getNumEdge();

		const vector<Frame3d>& vF = rod.getFrames();

		Frame3d f0;
		f0.tan = vrodTan0[i];
		f0.nor = Vector3d(0, 0, 1);
		f0.bin = f0.tan.cross(f0.nor);

		Frame3d f1;
		f1.tan = vrodTan1[i];
		f1.nor = Vector3d(0, 0, 1);
		f1.bin = f1.tan.cross(f1.nor);

		Frame3d f0New;
		Frame3d f1New;

		if (src.m_vrod2con[i].first != -1 && src.m_vrod2con[i].second != -1)
		{
			Matrix3d rot0 = vconR[src.m_vrod2con[i].first];
			Matrix3d rot1 = vconR[src.m_vrod2con[i].second];

			const Curve& curve = rod.getCurve();

			f0New = parallelTransport(frameRotation(f0, rot0), rod.getFrames().front().tan);
			f1New = parallelTransport(frameRotation(f1, rot1), rod.getFrames().back().tan);

			if (ne == 1)
			{
				rod.setFrame(0, f0New);
			}
			else if (ne == 2)
			{
				rod.setFrame(0, f0New);
				rod.setFrame(1, f0New);
			}
			else
			{
				vector<Frame> vFPT(ne);
				vector<Frame> vFRot(ne);

				Curve::FramePTForward(f0New, curve, vFPT);
				Vector3d refVec = f1New.tan;
				Vector3d conNor = f1New.nor;
				Vector3d rodNor = vFPT.back().nor;

				Real S = (rodNor.cross(conNor).dot(refVec) < 0) ? -1 : 1;
				Real cosa = rodNor.dot(conNor);
				if (cosa > 1.0)
					cosa = 1.0;
				if (cosa < -1.0)
					cosa = -1.0;
				Real angle = S*acos(cosa);
				Real inc = angle / (ne - 1);

				dVector vanew(ne);
				for (int j = 0; j < ne; ++j)
					vanew[j] = inc*j; // Uniform

				frameRotation(vFPT, vanew, vFRot);

				rod.setFrames(vFRot);
				rod.readaptFrames();
			}
		}
		else if (src.m_vrod2con[i].first != -1 && src.m_vrod2con[i].second == -1)
		{
			vector<Frame> vFPT(ne);
			f0New = parallelTransport(frameRotation(f0, vconR[src.m_vrod2con[i].first]), rod.getFrames().front().tan);
			Curve::FramePTForward(f0New, rod.getCurve(), vFPT);
			rod.setFrames(vFPT);
			rod.readaptFrames();
		}
		else if (src.m_vrod2con[i].first == -1 && src.m_vrod2con[i].second != -1)
		{
			vector<Frame> vFPT(ne);
			f1New = parallelTransport(frameRotation(f1, vconR[src.m_vrod2con[i].second]), rod.getFrames().back().tan);
			Curve::FramePTBackward(f1New, rod.getCurve(), vFPT);
			rod.setFrames(vFPT);
			rod.readaptFrames();
		}
	}

	return true;
}

void RodMesh::clearRods()
{
	this->init(vector<Rod>(), vector<Con>());
}

void RodMesh::removeRod(int rodi, bool remerge)
{
	assert(rodi >= 0 && rodi < this->getNumRod());

	// Disconnect and erase

	this->disconnectRod(rodi);

	for (int i = 0; i < (int) this->m_vprops.size(); ++i)
	{
		this->m_vprods[this->m_vprops[i]].erase(this->m_vprods[this->m_vprops[i]].begin() + rodi);
	}

	this->m_vrod2con.erase(this->m_vrod2con.begin() + rodi);

	int nc = this->getNumCon();

	// Shift references to rods

	for (int j = 0; j < nc; ++j)
	{
		vector<pair<int, int>> vtoRem;
		vector<pair<int, int>> vtoAdd;

		for (set<pair<int, int>>::iterator s_it = this->m_vcons[j].m_srods.begin(); s_it != this->m_vcons[j].m_srods.end(); ++s_it)
		{
			if ((*s_it).first > rodi)
			{
				pair<int, int> toRem = *s_it;
				pair<int, int> toAdd = toRem;
				toAdd.first--;

				vtoRem.push_back(toRem);
				vtoAdd.push_back(toAdd);
			}
		}

		for (int i = 0; i < (int)vtoRem.size(); ++i)
			this->m_vcons[j].m_srods.erase(vtoRem[i]);

		for (int i = 0; i < (int)vtoAdd.size(); ++i)
			this->m_vcons[j].m_srods.insert(vtoAdd[i]);
	}

	iVector vdelete;

	vdelete.push_back(rodi);

	//if (remerge)
	//{
	//	// Check remaining connections

	//	for (int i = 0; i < nc; ++i)
	//	{
	//		if (this->m_vcons[i].m_srods.size() != 2)
	//			continue; // Only interested in pairs

	//		vector<pair<int, int>> vrods;
	//		set<pair<int, int>>::iterator
	//			itEnd = this->m_vcons[i].m_srods.end(),
	//			itCur = this->m_vcons[i].m_srods.begin();
	//		for (; itCur != itEnd; ++itCur)
	//			vrods.push_back(*itCur);

	//		bool merge = false;
	//		bool rever = false;

	//		for (int i = 0; i < (int) this->m_vprops.size(); ++i)
	//		{
	//			Vector3d tan0;
	//			if (vrods[0].second == 0)
	//				tan0 = this->getRod(vrods[0].first, this->m_vprops[i]).getFrames().front().tan;
	//			else tan0 = -this->getRod(vrods[0].first, this->m_vprops[i]).getFrames().back().tan;

	//			Vector3d tan1;
	//			if (vrods[1].second == 0)
	//				tan1 = this->getRod(vrods[1].first, this->m_vprops[i]).getFrames().front().tan;
	//			else tan1 = -this->getRod(vrods[1].first, this->m_vprops[i]).getFrames().back().tan;

	//			if (tan0.dot(tan1) < 0.0)
	//			{
	//				tan1 *= -1;

	//				if (acos(tan0.dot(tan1)) < M_PI/10)
	//				{
	//					merge = true;
	//					rever = true;
	//					break;
	//				}
	//			}
	//			else
	//			{
	//				if (acos(tan0.dot(tan1)) < M_PI/10)
	//				{
	//					merge = true;
	//					rever = false;
	//					break;
	//				}
	//			}
	//		}

	//		if (merge)
	//		{
	//			int nr = this->getNumRod();

	//			vector<Rod> vnewRods((int) this->m_vprops.size());
	//			for (int i = 0; i < (int) this->m_vprops.size(); ++i)
	//			{
	//				Rod& newRod = vnewRods[i];
	//				Rod rod0 = this->getRod(vrods[0].first, this->m_vprops[i]);
	//				Rod rod1 = this->getRod(vrods[1].first, this->m_vprops[i]);
	//				if (rever)
	//					rod1.reverse();
	//				Rod::MergeRods(rod0, rod1, newRod);
	//				this->m_vprods[m_vprops[i]].push_back(newRod);
	//				this->m_vrod2con.push_back(pair<int, int>(-1, -1));
	//				this->connectRod(nr, 1e-3, this->m_vprops[i]);
	//			}

	//			vdelete.push_back(vrods[0].first);
	//			vdelete.push_back(vrods[1].first);
	//		}
	//	}

	//	int ndelete = (int)vdelete.size();
	//	sort(vdelete.begin(), vdelete.end());
	//	for (int i = ndelete - 1; i >= 0; --i)
	//		this->removeRod(vdelete[i], false);
	//}

	this->updateCache();

	//return vdelete;
}

bool RodMesh::addRod(const Rod& newrod, Real tol, const string& pname, iVector& vaddRods, iVector& vdelRods, bool intersect)
{
	vector<Rod>& vrods = this->m_vprods[pname];
	int nr = (int) this->m_vrod2con.size();
	int np = (int) this->m_vprops.size();
	bVector vtoDelete(nr, false);

	// Prepara new input rods

	pair<int, int> link = pair<int, int>(-1, -1);

	vector<Rod> vinputSet;
	vinputSet.reserve(nr);

	vinputSet.push_back(newrod);

	// Prepare new mesh rods

	vector<vector<Rod>> vmeshSet(np);
	for (int i = 0; i < np; ++i)
		vmeshSet[i].reserve(2*nr);


	if (intersect)
	{
		// Intersect with every other
		for (int i = 0; i < nr; ++i)
		{
			const Rod& rod_mesh = vrods[i];

			vector<Rod> vinputSet_i = vinputSet;
			int nnew = (int)vinputSet_i.size();

			// Clear new set
			vinputSet.clear();
		
			vector<SplitPoint> vspmesh;

			for (int j = 0; j < nnew; ++j)
			{
				const Rod& rod_input = vinputSet_i[j];
				vector<vector<SplitPoint>> vsp(2);

				if (Curve::Intersect(rod_input.getCurve(), rod_mesh.getCurve(), vsp, tol))
				{
					assert(vsp[0].size() == vsp[1].size());
				
					// Splitting!

					pair<int, int> link = pair<int, int>(-1, -1);

					vector<Rod> vinputSplitted;
					Rod::SplitRod(rod_input, vsp[0], vinputSplitted);
					Real minL0 = rod_input.getCurve().getLength()*tol;
					iVector vvalid;
					for (int s = 0; s < (int)vinputSplitted.size(); ++s)
						if (vinputSplitted[s].getCurve().getLength() > minL0)
							vvalid.push_back(s);
					if (vvalid.size() > 0)
					{
						for (int s = 0; s < (int)vvalid.size(); ++s)
							vinputSet.push_back(vinputSplitted[vvalid[s]]);
					}
					else
					{
						vinputSet.push_back(rod_input);
					}
	
					vspmesh.insert(vspmesh.end(), vsp[1].begin(), vsp[1].end());
				}
				else
				{
					vinputSet.push_back(rod_input);
				}
			}

			if (!vspmesh.empty())
			{
				for (int k = 0; k < np; ++k)
				{
					const Rod& rod_mesh = this->m_vprods[this->m_vprops[k]][i];

					vector<Rod> vmeshSplitted;
					Rod::SplitRod(rod_mesh, vspmesh, vmeshSplitted);
					Real minL1 = rod_mesh.getCurve().getLength()*tol;
					iVector vvalid;
					for (int s = 0; s < (int)vmeshSplitted.size(); ++s)
						if (vmeshSplitted[s].getCurve().getLength() > minL1)
							vvalid.push_back(s);
					if (vvalid.size() > 1)
					{
						for (int s = 0; s < (int) vvalid.size(); ++s)
							vmeshSet[k].push_back(vmeshSplitted[vvalid[s]]);

						vtoDelete[i] = true;
					}
				}
			}
		}
	}

	vdelRods.clear();
	vaddRods.clear();

	int delCount = 0;

	// Delete old deprecated rods
	for (int i = 0; i < nr; ++i)
		if (vtoDelete[i])
		{
		vdelRods.push_back(i);
		this->removeRod(i - delCount);
		delCount++; // Used as offset
		}

	// Add/connect resulting mesh rods
	int nrMesh = (int) vmeshSet[0].size();
	for (int i = 0; i < nrMesh; ++i)
	{
		this->m_vrod2con.push_back(link);

		for (int j = 0; j < np; ++j)
		{
			//vmeshSet[j][i].resample(vmeshSet[j][i].getNumNode());
			m_vprods[this->m_vprops[j]].push_back(vmeshSet[j][i]);
		}

		int idx = (int) this->m_vprods[pname].size() - 1;
		this->connectRod(idx, tol, pname);
		vaddRods.push_back(idx);

		//this->connectRod((int) this->m_vprods[pname].size() - 1, tol, pname);
	}

	// Add/ connect resulting input rods
	int nrInput = (int) vinputSet.size();
	for (int i = 0; i < nrInput; ++i)
	{
		this->m_vrod2con.push_back(link);

		vinputSet[i].resample(vinputSet[i].getNumNode());

		for (int j = 0; j < np; ++j)
		{
			m_vprods[this->m_vprops[j]].push_back(vinputSet[i]);
		}

		int idx = (int) this->m_vprods[pname].size() - 1;
		this->connectRod(idx, tol, pname);
		vaddRods.push_back(idx);
	}

	this->updateCache();

	return !vaddRods.empty();
}

void RodMesh::setRods(const vector<Rod>& vrods, const string& pname, bool setCon)
{
	assert((int)vrods.size() == this->getNumRod());

	if (find(this->m_vprops.begin(), this->m_vprops.end(), pname) == this->m_vprops.end())
		this->m_vprops.push_back(pname); // Add the new property if it was not a property

	this->m_vprods[pname] = vrods;

	this->updateCache();
}

void RodMesh::connectRod(int i, Real tol, const string& pname)
{
	assert(i >= 0 && i < this->getNumRod());

	const vector<Rod>& vrods = this->m_vprods[pname];

	int nv = vrods[i].getNumNode();
	Vector3d p0 = vrods[i].getCurve().getPosition(0);
	Vector3d p1 = vrods[i].getCurve().getPosition(nv-1);

	Real absTol0 = vrods[i].getCurve().getLength()*tol;

	int nr = this->getNumRod();

	for (int j = 0; j < nr; ++j)
	{
		if (i == j) continue;

		int nvo = vrods[j].getNumNode();
		Vector3d p0o = vrods[j].getCurve().getPosition(0);
		Vector3d p1o = vrods[j].getCurve().getPosition(nvo-1);
		
		Real absTol1 = vrods[j].getCurve().getLength()*tol;

		Real absTol = min(absTol0, absTol1);

		if ((p0 - p0o).norm() < absTol)
		{
			int cidx = this->m_vrod2con[j].first;
			if (cidx == -1)
			{
				this->m_vcons.push_back(Con()); // New
				cidx = (int) this->m_vcons.size() - 1;

				for (int k = 0; k < (int) this->m_vprops.size(); ++k)
					this->m_vprots[this->m_vprops[k]].push_back(Matrix3d::Identity());

				this->m_vcons[cidx].m_srods.insert(pair<int, int>(i, 0));
				this->m_vcons[cidx].m_srods.insert(pair<int, int>(j, 0));
				this->m_vrod2con[i].first = cidx;
				this->m_vrod2con[j].first = cidx;
			}
			else
			{
				this->m_vcons[cidx].m_srods.insert(pair<int, int>(i, 0));
				this->m_vrod2con[i].first = cidx;
			}

			break; // Connected
		}
		
		if ((p0 - p1o).norm() < absTol)
		{
			int cidx = this->m_vrod2con[j].second;
			if (cidx == -1)
			{
				this->m_vcons.push_back(Con()); // New
				cidx = (int) this->m_vcons.size() - 1;

				for (int k = 0; k < (int) this->m_vprops.size(); ++k)
					this->m_vprots[this->m_vprops[k]].push_back(Matrix3d::Identity());

				this->m_vcons[cidx].m_srods.insert(pair<int, int>(i, 0));
				this->m_vcons[cidx].m_srods.insert(pair<int, int>(j, 1));
				this->m_vrod2con[i].first = cidx;
				this->m_vrod2con[j].second = cidx;
			}
			else
			{
				this->m_vcons[cidx].m_srods.insert(pair<int, int>(i, 0));
				this->m_vrod2con[i].first = cidx;
			}

			break; // Connected
		}
	}

	for (int j = 0; j < nr; ++j)
	{
		if (i == j) continue;

		int nvo = vrods[j].getNumNode();
		Vector3d p0o = vrods[j].getCurve().getPosition(0);
		Vector3d p1o = vrods[j].getCurve().getPosition(nvo - 1);

		Real absTol1 = vrods[j].getCurve().getLength()*tol;

		Real absTol = min(absTol0, absTol1);

		if ((p1 - p0o).norm() < absTol)
		{
			int cidx = this->m_vrod2con[j].first;
			if (cidx == -1)
			{
				this->m_vcons.push_back(Con()); // New
				cidx = (int) this->m_vcons.size() - 1;

				for (int k = 0; k < (int) this->m_vprops.size(); ++k)
					this->m_vprots[this->m_vprops[k]].push_back(Matrix3d::Identity());

				this->m_vcons[cidx].m_srods.insert(pair<int, int>(i, 1));
				this->m_vcons[cidx].m_srods.insert(pair<int, int>(j, 0));
				this->m_vrod2con[i].second = cidx;
				this->m_vrod2con[j].first = cidx;
			}
			else
			{
				this->m_vcons[cidx].m_srods.insert(pair<int, int>(i, 1));
				this->m_vrod2con[i].second = cidx;
			}

			break; // Connected
		}

		if ((p1 - p1o).norm() < absTol)
		{
			int cidx = this->m_vrod2con[j].second;
			if (cidx == -1)
			{
				this->m_vcons.push_back(Con()); // New
				cidx = (int) this->m_vcons.size() - 1;

				for (int k = 0; k < (int) this->m_vprops.size(); ++k)
					this->m_vprots[this->m_vprops[k]].push_back(Matrix3d::Identity());

				this->m_vcons[cidx].m_srods.insert(pair<int, int>(i, 1));
				this->m_vcons[cidx].m_srods.insert(pair<int, int>(j, 1));
				this->m_vrod2con[i].second = cidx;
				this->m_vrod2con[j].second = cidx;
			}
			else
			{
				this->m_vcons[cidx].m_srods.insert(pair<int, int>(i, 1));
				this->m_vrod2con[i].second = cidx;
			}

			break; // Connected
		}
	}
}

void RodMesh::disconnectRod(int i)
{
	assert(i >= 0 && i < this->getNumRod());
	
	int conn0 = this->m_vrod2con[i].first;
	int conn1 = this->m_vrod2con[i].second;

	// Check if both extremes are conincident

	if (conn0 == conn1)
	{
		int con = conn0;
		if (con != -1)
		{
			this->m_vrod2con[i].first = -1;
			this->m_vrod2con[i].second = -1;
			this->m_vcons[con].m_srods.erase(pair<int, int>(i, 0));
			this->m_vcons[con].m_srods.erase(pair<int, int>(i, 1));
			if (this->m_vcons[con].m_srods.size() <= 1)
			{
				this->removeCon(con);
			}
			return;
		}
		else return;
	}

	// Disconnect rod at starting point

	if (conn0 != -1)
	{
		this->m_vrod2con[i].first = -1;

		this->m_vcons[conn0].m_srods.erase(pair<int, int>(i, 0));
		if (this->m_vcons[conn0].m_srods.size() <= 1)
		{
			this->removeCon(conn0);
			if (conn0 < conn1)
				conn1 -= 1;
		}
	}

	// Disconnect rod at finishing point

	if (conn1 != -1)
	{
		this->m_vrod2con[i].second = -1;

		this->m_vcons[conn1].m_srods.erase(pair<int, int>(i, 1));
		if (this->m_vcons[conn1].m_srods.size() <= 1)
		{
			this->removeCon(conn1); 
			if (conn1 < conn0)
				conn0 -= 1;
		}
	}
}

void RodMesh::removeCon(int i)
{
	assert(i >= 0 && i < this->getNumCon());

	for (int j = 0; j < this->getNumRod(); ++j)
	{
		if (this->m_vrod2con[j].first == i) 
			this->m_vrod2con[j].first = -1;
		if (this->m_vrod2con[j].second == i) 
			this->m_vrod2con[j].second = -1;

		if (this->m_vrod2con[j].first > i) 
			this->m_vrod2con[j].first--;
		if (this->m_vrod2con[j].second > i) 
			this->m_vrod2con[j].second--;
	}

	this->m_vcons.erase(this->m_vcons.begin() + i);
}


void RodMesh::setRod(int i, const Rod& rod, const string& pname, bool setCon)
{
	assert(i >= 0 && i < this->getNumRod());

	int nv = this->m_vprods[""][i].getNumNode();
	int ne = this->m_vprods[""][i].getNumEdge();

	this->m_vprods[pname][i] = rod;

	if (setCon)
	{
		if (this->m_vrod2con[i].first != -1)
		{
			this->setConPoint(this->m_vrod2con[i].first, rod.getPosition(0), pname);
		}
		if (this->m_vrod2con[i].second != -1)
		{
			this->setConPoint(this->m_vrod2con[i].second, rod.getPosition(nv - 1), pname);
		}
	}

	if (rod.getNumNode() != nv || rod.getNumEdge() != ne)
		this->updateCache(); // Update cache if needed
}

void RodMesh::getRodNodeForMeshNode(int idx, int& rodIdx, int& nodeIdx) const
{
	int offset = 0;
	int nr = this->getNumRod();
	for (int i = 0; i < nr; ++i)
	{
		if (idx < this->m_rodNodeCount[i])
		{
			nodeIdx = idx - offset;
			rodIdx = i; // Found rod
			return;
		}

		offset = this->m_rodNodeCount[i];
	}

	assert(false);
}

void RodMesh::getRodEdgeForMeshEdge(int idx, int& rodIdx, int& edgeIdx) const
{
	int offset = 0;
	int nr = this->getNumRod();
	for (int i = 0; i < nr; ++i)
	{
		if (idx < this->m_rodEdgeCount[i])
		{
			edgeIdx = idx - offset;
			rodIdx = i; // Found rod
			return;
		}

		offset = this->m_rodEdgeCount[i];
	}

	assert(false);
}

void RodMesh::getMeshNodeForRodNode(int idx, int rodIdx, int& nodeIdx) const
{
	assert(rodIdx >= 0 && rodIdx < this->getNumRod());

	if (rodIdx == 0)
	{
		nodeIdx = idx;
		return; // Done
	}

	nodeIdx = this->m_rodNodeCount[rodIdx - 1] + idx;

	return;
}

void RodMesh::getMeshEdgeForRodEdge(int idx, int rodIdx, int& edgeIdx) const
{
	assert(rodIdx >= 0 && rodIdx < this->getNumRod());

	if (rodIdx == 0)
	{
		edgeIdx = idx;
		return; // Done
	}

	edgeIdx = this->m_rodEdgeCount[rodIdx - 1] + idx;

	return;
}

Rod& RodMesh::getRod(int i, const string& pname)
{
	assert(i >= 0 && i < getNumRod());
	return this->m_vprods.at(pname)[i];
}

const Rod& RodMesh::getRod(int i, const string& pname) const
{
	assert(i >= 0 && i < getNumRod());
	return this->m_vprods.at(pname)[i];
}

Vector3d RodMesh::getConPoint(int i, const string& pname) const
{
	assert(i >= 0 && i < getNumCon());
	
	Vector3d center;
	center.setZero();

	int nr = 0;

	for (set<pair<int, int>>::iterator it = this->m_vcons[i].m_srods.begin(); it != this->m_vcons[i].m_srods.end(); ++it)
	{
		if (it->first < 0 || it->first >= m_vprods.at(pname).size())
			continue;

		const Rod& rod = this->m_vprods.at(pname)[it->first];
		int nv = rod.getNumNode();
		if (it->second == 0)
		{
			center += rod.getPosition(0);
		}
		if (it->second == 1)
		{
			center += rod.getPosition(nv-1);
		}

		nr++;
	}

	return center/nr;
}

void RodMesh::setConPoint(int i, const Vector3d& p, const string& pname)
{
	assert(i >= 0 && i < getNumCon());

	int nr = 0;

	for (set<pair<int, int>>::iterator it = this->m_vcons[i].m_srods.begin(); it != this->m_vcons[i].m_srods.end(); ++it)
	{
		if (it->first < 0 || it->first >= m_vprods.at(pname).size())
			continue;

		Rod& rod = this->m_vprods.at(pname)[it->first];
		int nv = rod.getNumNode();
		if (it->second == 0)
		{
			rod.setPosition(0, p);
		}
		if (it->second == 1)
		{
			rod.setPosition(nv-1, p);
		}

		rod.readaptFrames();

		nr++;
	}
}


const Matrix3d& RodMesh::getRot(int i, const string& pname) const
{
	assert(i >= 0 && i < getNumCon());

	return this->m_vprots.at(pname)[i];
}

void RodMesh::setRot(int i, const Matrix3d& vn, const string& pname)
{
	assert(i >= 0 && i < getNumCon());

	this->m_vprots.at(pname)[i] = vn;
}

void RodMesh::setPoint(int idx, const Vector3d& p, const string& pname)
{
	int rodIdx;
	int nodIdx;
	this->getRodNodeForMeshNode(idx, rodIdx, nodIdx);
	this->getRod(rodIdx, pname).setPosition(nodIdx, p);
}

void RodMesh::setRadius(int idx, const Vector2d& r, const string& pname)
{
	int rodIdx;
	int edgIdx;
	this->getRodEdgeForMeshEdge(idx, rodIdx, edgIdx);
	this->getRod(rodIdx, pname).setRadius(edgIdx, r);
}

void RodMesh::setFrame(int idx, const Frame3d& F, const string& pname)
{
	int rodIdx;
	int edgIdx;
	this->getRodEdgeForMeshEdge(idx, rodIdx, edgIdx);
	this->getRod(rodIdx, pname).setFrame(edgIdx, F);
}

Vector3d RodMesh::getPoint(int idx, const string& pname) const
{
	int rodIdx;
	int nodIdx;
	this->getRodNodeForMeshNode(idx, rodIdx, nodIdx);
	return this->getRod(rodIdx, pname).getPosition(nodIdx);

	assert(false);
	return Vector3d();
}

Vector2d RodMesh::getRadius(int idx, const string& pname) const
{
	int rodIdx;
	int edgIdx;
	this->getRodEdgeForMeshEdge(idx, rodIdx, edgIdx);
	return this->getRod(rodIdx, pname).getRadius(edgIdx);

	assert(false);
	return Vector2d();
}

Frame3d RodMesh::getFrame(int idx, const string& pname) const
{
	int rodIdx;
	int edgIdx;
	this->getRodEdgeForMeshEdge(idx, rodIdx, edgIdx);
	return this->getRod(rodIdx, pname).getFrame(edgIdx);

	assert(false);
	return Frame3d();
}

int RodMesh::getMeshNodeForOffset(int idxOffset) const
{
	// TODO

	return -1;
}

int RodMesh::getMeshEdgeForOffset(int idxOffset) const
{
	// TODO

	return -1;
}

dVector RodMesh::getRanges(const string& pname) const
{
	dVector vpos;
	iVector vidx;
	this->getMeshData(vpos, vidx);

	int nv = (int) vpos.size()/3;

	dVector vextremes(6);
	vextremes[0] = HUGE_VAL;
	vextremes[2] = HUGE_VAL;
	vextremes[4] = HUGE_VAL;
	vextremes[1] = -HUGE_VAL;
	vextremes[3] = -HUGE_VAL;
	vextremes[5] = -HUGE_VAL;
	for (int i = 0; i < nv; ++i)
	{
		int offset = 3 * i;
		if (vpos[offset + 0] < vextremes[0]) vextremes[0] = vpos[offset + 0];
		if (vpos[offset + 1] < vextremes[2]) vextremes[2] = vpos[offset + 1];
		if (vpos[offset + 2] < vextremes[4]) vextremes[4] = vpos[offset + 2];
		if (vpos[offset + 0] > vextremes[1]) vextremes[1] = vpos[offset + 0];
		if (vpos[offset + 1] > vextremes[3]) vextremes[3] = vpos[offset + 1];
		if (vpos[offset + 2] > vextremes[5]) vextremes[5] = vpos[offset + 2];
	}
	dVector vranges(3);
	vranges[0] = vextremes[1] - vextremes[0];
	vranges[1] = vextremes[3] - vextremes[2];
	vranges[2] = vextremes[5] - vextremes[4];

	return vranges;
}

void RodMesh::getMeshData(dVector& vnodes, iVector& vedges, const string& pname) const
{
	const vector<iVector>& vwhole2SplitMap = getWholeToSplitMap();
	const vector<iVector>& vsplit2WholeMap = getSplitToWholeMap();

	int nvWhole = (int)vwhole2SplitMap.size();

	int neWhole = getNumEdge();

	vnodes.resize(3 * nvWhole);
	vedges.resize(2 * neWhole);

	for (int i = 0; i < nvWhole; ++i)
	{
		int splitNode = vwhole2SplitMap[i][0];
		int ridx;
		int nidx;
		getRodNodeForMeshNode(splitNode, ridx, nidx);

		Vector3d node = getRod(ridx, pname).getPosition(nidx);
		vnodes[3 * i + 0] = node.x();
		vnodes[3 * i + 1] = node.y();
		vnodes[3 * i + 2] = node.z();
	}
	for (int i = 0; i < neWhole; ++i)
	{
		int ridx;
		int eidx;
		getRodEdgeForMeshEdge(i, ridx, eidx);
		int pn, nn;
		getRod(ridx).getCurve().getEdgeNeighborNodes(eidx, pn, nn);
		int pnSplit, nnSplit;
		getMeshNodeForRodNode(pn, ridx, pnSplit);
		getMeshNodeForRodNode(nn, ridx, nnSplit);
		vedges[2 * i + 0] = vsplit2WholeMap[pnSplit][0];
		vedges[2 * i + 1] = vsplit2WholeMap[nnSplit][0];
	}
}

void RodMesh::getPoints(dVector& vpoints, const string& pname) const
{
	int nv = this->getNumNode();

	vpoints.resize(3*nv);

	for (int i = 0; i < nv; ++i)
	{
		set3D(i, this->getPoint(i, pname), vpoints);
	}
}

void RodMesh::setPoints(const dVector& vpoints, const string& pname)
{
	int nv = this->getNumNode();

	assert(3*nv == (int)vpoints.size());

	for (int i = 0; i < nv; ++i)
	{
		this->setPoint(i, get3D(i, vpoints), pname);
	}
}


void RodMesh::resampleRods()
{
	for (int i = 0; i < (int) this->m_vprops.size(); ++i)
		for (int j = 0; j < this->getNumRod(); ++j)
		{
		Rod& rod = this->m_vprods[this->m_vprops[i]][j];
		rod.resample(rod.getNumNode()); // Resample rod
		}
}

void RodMesh::computeDistanceMatrix_Cartesian(MatrixXd& mD, const string& pname)
{
	int nv = this->getNumNode();

	mD.resize(nv, nv);

	for (int i = 0; i < nv; ++i)
	{
		mD(i, i) = 0;

		Vector3d p0 = this->getPoint(i);
		for (int j = i + 1; j < nv; ++j)
		{
			Vector3d p1 = this->getPoint(j);
			Real distance = (p0 - p1).norm();
			mD(i, j) = distance;
			mD(j, i) = distance;
		}
	}
}

void RodMesh::computeDistanceMatrix_Topology(MatrixXd& mD, const string& pname)
{
	int nv = this->getNumNode();
	int ne = this->getNumEdge();
	//int nc = this->getNumCon();

	// Cache edge map and lengths

	dVector vedgeLenghts(ne);
	vector<pair<int,int>> ve2v;

	for (int i = 0; i < ne; ++i)
	{
		int rodIdx, edgeIdx, pv, nv;
		this->getRodEdgeForMeshEdge(i, rodIdx, edgeIdx);
		this->getRod(rodIdx).getCurve().getEdgeNeighborNodes(edgeIdx, pv, nv);

		int pnodeIdx;
		int nnodeIdx;
		this->getMeshNodeForRodNode(pv, rodIdx, pnodeIdx);
		this->getMeshNodeForRodNode(nv, rodIdx, nnodeIdx);
		ve2v.push_back(pair<int, int>(pnodeIdx, nnodeIdx));
		vedgeLenghts[i] = (this->getPoint(pnodeIdx) - this->getPoint(nnodeIdx)).norm();
	}

	mD.resize(nv, nv);

	// Initialize lengths
	mD.setConstant(HUGE_VAL);

	for (int i = 0; i < nv; ++i)
	{
		mD(i, i) = 0.0;
	}

	for (int i = 0; i < ne; ++i)
	{
		mD(ve2v[i].first, ve2v[i].second) = vedgeLenghts[i];
		mD(ve2v[i].second, ve2v[i].first) = vedgeLenghts[i];
	}

	for (int i = 0; i < nv; ++i)
	{
		iVector& vsplit = this->m_vwholeToSplit[this->m_vsplitToWhole[i][0]];
		int nvsplit = (int) vsplit.size();
		for (int j = 0; j < nvsplit; ++j)
		{
			mD(i, vsplit[j]) = 0;
			mD(vsplit[j], i) = 0;
		}
	}

	for (int k = 0; k < nv; ++k)
	{
		for (int i = 0; i < nv; ++i)
		{
			for (int j = 0; j < nv; ++j)
			{
				double oldDistance = mD(i, j);
				double newDistance = mD(i, k) + mD(k, j);
				if (oldDistance > newDistance)
					mD(i, j) = newDistance;
			}
		}
	}
}

void RodMesh::computeDistanceMatrix_RodBased(MatrixXd& mD, const string& pname)
{
	this->computeDistanceMatrix_Topology(mD, pname);

	int nr = this->getNumRod();

	for (int r0 = 0; r0 < nr; ++r0)
	{
		Rod& rod0 = this->getRod(r0);
		int nvrod0 = rod0.getNumNode();

		for (int v0 = 0; v0 < nvrod0; ++v0)
		{
			int nodeIdx0;
			this->getMeshNodeForRodNode(v0, r0, nodeIdx0);

			set<int> vr;
			int wholeIdx = this->m_vsplitToWhole[nodeIdx0][0];
			if (this->m_vwholeToSplit[wholeIdx].size() != 1)
			{
				int nc = this->getNumCon();
				for (int i = 0; i < nc; ++i)
				{
					if (find(this->m_vmapConNode[i].begin(), this->m_vmapConNode[i].end(), nodeIdx0) != this->m_vmapConNode[i].end())
					{
						set<pair<int, int>>::iterator it_end = this->m_vcons[i].m_srods.end();
						set<pair<int, int>>::iterator it_cur = this->m_vcons[i].m_srods.begin();
						for (; it_cur != it_end; ++it_cur)
							vr.insert(it_cur->first);
					}
				}
			}

			for (int r1 = 0; r1 < nr; ++r1)
			{
				if (r0 == r1)
					continue;

				if (vr.find(r1) != vr.end())
					continue;

				Rod& rod1 = this->getRod(r1);
				int nvrod1 = rod1.getNumNode();

				for (int v1 = 0; v1 < nvrod1; ++v1)
				{
					int nodeIdx1;
					this->getMeshNodeForRodNode(v1, r1, nodeIdx1);
					mD(nodeIdx0, nodeIdx1) = HUGE_VAL;
				}
			}
		}
	}
}

bool RodMesh::joinRods(int i, int j, iVector& vaddRods, iVector& vdelRods)
{
	//throw new exception("Not implemented yet...");

	return true;
}

bool RodMesh::duplicateRod(int i, iVector& vaddRods, iVector& vdelRods)
{
	int nr = this->getNumRod();
	if (i < 0 || i >= nr)
		return false;

	Rod& rodRaw = this->getRod(i);

	int nv = rodRaw.getCurve().getNumNode();
	Real L = rodRaw.getCurve().getLength();
	Real mL = 0.05*L;

	Rod rodCopy = rodRaw;

	if (rodRaw.getCurve().getIsClosed())
	{
		for (int i = 0; i < nv; ++i)
		{
			int pe, ne;
			rodRaw.getCurve().getNodeNeighborEdges(i, pe, ne);
			Vector3d bin = 
				rodRaw.getFrame(pe).bin +
				rodRaw.getFrame(ne).bin;
			bin.normalize();

			rodRaw.setPosition(i, rodRaw.getPosition(i) + mL*bin);
			rodCopy.setPosition(i, rodRaw.getPosition(i) - mL*bin);
		}
	}
	else
	{
		for (int i = 1; i < nv - 1; ++i)
		{
			int pe, ne;
			rodRaw.getCurve().getNodeNeighborEdges(i, pe, ne);
			Vector3d bin =
				rodRaw.getFrame(pe).bin +
				rodRaw.getFrame(ne).bin;
			bin.normalize();

			Real mean = nv / 2.0;

			Real alpha = (mean - abs(i - mean)) / mean;

			rodRaw.setPosition(i, rodRaw.getPosition(i) + alpha*mL*bin);
			rodCopy.setPosition(i, rodRaw.getPosition(i) - alpha*mL*bin);
		}
	}

	rodRaw.readaptFrames();
	rodCopy.readaptFrames();

	this->setRod(i, rodRaw, "");

	this->addRod(rodCopy, 0.01, "", vaddRods, vdelRods, false);

	return true;
}

bool RodMesh::weld_BinaryConnection2Rod(int nodeIdx, const string& pname, iVector& vaddRods, iVector& vdelRods)
{
	int conIdx = this->getConForNode(nodeIdx);
	if (conIdx == -1)
		return false;

	Con& con = this->m_vcons[conIdx];
	if ((int) con.m_srods.size() != 2)
		return false;  // Not binary
	
	iVector vextreme;

	set<pair<int, int>>::iterator
		it_end = con.m_srods.end(),
		it_cur = con.m_srods.begin();
	for (; it_cur != it_end; ++it_cur)
	{
		vdelRods.push_back(it_cur->first);
		vextreme.push_back(it_cur->second);
	}
		
	Rod rod0 = this->getRod(vdelRods[0], pname);
	Rod rod1 = this->getRod(vdelRods[1], pname);
	sort(vdelRods.begin(), vdelRods.end());
	this->removeRod(vdelRods[1]);
	this->removeRod(vdelRods[0]);

	if (vextreme[0] == 0 && vextreme[1] == 0)
		rod0.reverse();
	if (vextreme[0] == 1 && vextreme[1] == 1)
		rod1.reverse();
	if (vextreme[0] == 0 && vextreme[1] == 1)
	{
		rod0.reverse();
		rod1.reverse();
	}
	if (vextreme[0] == 1 && vextreme[1] == 0)
		; // Do nothing

	iVector vaddedRods;
	iVector vdeletedRods;

	Rod rodMerge;
	Rod::MergeRods(rod0, rod1, rodMerge);
	this->addRod(rodMerge, 0.01, pname, vaddedRods, vdeletedRods, false);

	assert(vaddedRods.size()==1);
	assert(vdeletedRods.empty());

	vaddRods = vaddedRods;

	return true;
}

bool RodMesh::split_Rod2BinaryConnection(int nodeIdx, const string& pname, iVector& vaddRods, iVector& vdelRods)
{
	int rodIdx, index;
	this->getRodNodeForMeshNode(nodeIdx, rodIdx, index);

	const Rod& rod = this->getRod(rodIdx);
	const Curve& curve = rod.getCurve();

	if (curve.getIsClosed())
	{
		int pe, ne;
		rod.getCurve().getNodeNeighborEdges(index, pe, ne);
		vector<SplitPoint> vsp(1);
		vector<Rod> vsplitRod;
		vsp[0].m_edgeIdx = pe;
		vsp[0].m_edgeMu = 1.0;
		Rod::SplitRod(rod, vsp, vsplitRod);
		assert((int)vsplitRod.size() == 1);

		// Delete rod

		this->removeRod(rodIdx);
		vdelRods.push_back(rodIdx);

		// Add new rods

		iVector vaddRods0;
		iVector vdelRods0;
		this->addRod(vsplitRod[0], 0.01, "", vaddRods0, vdelRods0, false);
		vaddRods.push_back(vaddRods0[0]);
	}
	else
	{
		int nv = curve.getNumNode();
		if (index == 0 || index == nv - 1)
			return false; // Not at extremes

		int pe, ne;
		rod.getCurve().getNodeNeighborEdges(index, pe, ne);
		vector<SplitPoint> vsp(1);
		vector<Rod> vsplitRod;
		vsp[0].m_edgeIdx = pe;
		vsp[0].m_edgeMu = 1.0;
		Rod::SplitRod(rod, vsp, vsplitRod);
		assert((int) vsplitRod.size() == 2);

		// Delete rod

		this->removeRod(rodIdx);
		vdelRods.push_back(rodIdx);

		// Add new rods

		iVector vaddRods0;
		iVector vaddRods1;
		iVector vdelRods0;
		iVector vdelRods1;
		this->addRod(vsplitRod[0], 0.01, "", vaddRods0, vdelRods0, false);
		this->addRod(vsplitRod[1], 0.01, "", vaddRods1, vdelRods1, false);
		vaddRods.push_back(vaddRods0[0]);
		vaddRods.push_back(vaddRods1[0]);
	
		return true;
	}

	return false;
}

bool RodMesh::weld_Connection(int nodeIdx0, int nodeIdx1, const string& pname, iVector& vaddRods, iVector& vdelRods)
{
	// Get con selection

	int conIdx0 = getConForNode(nodeIdx0);
	Vector3d cp0 = this->getConPoint(conIdx0);

	// Get con selection

	int conIdx1 = getConForNode(nodeIdx1);
	Vector3d cp1 = this->getConPoint(conIdx1);

	// Merge connections

	Vector3d cpm = 0.5*(cp0 + cp1);

	Con* pMergedCon;

	if (conIdx0 < conIdx1)
	{
		pMergedCon = &this->m_vcons[conIdx0];

		set<pair<int, int>>::const_iterator
			it_end = this->m_vcons[conIdx1].m_srods.end(),
			it_cur = this->m_vcons[conIdx1].m_srods.begin();
		for (; it_cur != it_end; ++it_cur)
		{
			this->m_vcons[conIdx0].m_srods.insert(*it_cur);
		}
		this->removeCon(conIdx1);
	}
	else
	{
		pMergedCon = &this->m_vcons[conIdx1];

		set<pair<int, int>>::const_iterator
			it_end = this->m_vcons[conIdx0].m_srods.end(),
			it_cur = this->m_vcons[conIdx0].m_srods.begin();
		for (; it_cur != it_end; ++it_cur)
		{
			this->m_vcons[conIdx1].m_srods.insert(*it_cur);
		}
		this->removeCon(conIdx0);
	}

	this->updateCache();

	// Move rods

	set<pair<int, int>>::const_iterator
		it_end = pMergedCon->m_srods.end(),
		it_cur = pMergedCon->m_srods.begin();
	for (; it_cur != it_end; ++it_cur)
	{
		int ri = it_cur->first;
		int si = it_cur->second;
		Rod& rod = this->getRod(ri, pname);
		int nv = rod.getCurve().getNumNode();

		if (si == 0)
		{
			Vector3d rp0 = rod.getPosition(0);
			for (int j = 0; j < nv - 1; ++j)
			{
				Vector3d rp = rod.getPosition(j);
				Real alpha = (Real)(nv - 1 - j) / (Real)(nv - 1);
				rod.setPosition(j, rp + (cpm - rp0)*alpha);
			}
		}
		else
		{
			Vector3d rp1 = rod.getPosition(nv-1);
			for (int j = nv - 1; j >= 1; --j)
			{
				Vector3d rp = rod.getPosition(j);
				Real alpha = (Real)j / (Real)(nv - 1);
				rod.setPosition(j, rp + (cpm - rp1)*alpha);
			}
		}

		rod.readaptFrames();

		this->setRod(ri, rod, pname, false);
	}

	// Merge duplicated rods

	vector<pair<int,int>> vdupRods;

	int count0 = 0;

	set<pair<int, int>>::const_iterator
		it_end0 = pMergedCon->m_srods.end(),
		it_cur0 = pMergedCon->m_srods.begin();
	for (; it_cur0 != it_end0; ++it_cur0, count0++)
	{
		pair<int, int> cons0 = this->getRodCons(it_cur0->first);
		if (cons0.first == -1 || cons0.second == -1) continue;

		int count1 = count0;

		set<pair<int, int>>::const_iterator
			it_end1 = pMergedCon->m_srods.end(),
			it_cur1 = pMergedCon->m_srods.begin();
		for (it_cur1 = it_cur0; it_cur1 != it_end1; ++it_cur1, count1++)
		{
			if (count0 == count1)
				continue; // Jump

			pair<int, int> cons1 = this->getRodCons(it_cur1->first);
			if (cons1.first == -1 || cons1.second == -1) continue;

			if (cons0.first == cons1.first && cons0.second == cons1.second)
			{
				vdupRods.push_back(pair<int, int>(it_cur0->first, it_cur1->first));
				continue;
			}

			if (cons0.first == cons1.second && cons0.second == cons1.first)
			{
				vdupRods.push_back(pair<int, int>(it_cur0->first, it_cur1->first));
				continue;
			}
		}
	}

	vdelRods.clear();

	for (int i = 0; i < (int)vdupRods.size(); ++i)
	{
		Rod& rod0 = this->getRod(vdupRods[i].first, pname);
		Rod& rod1 = this->getRod(vdupRods[i].second, pname);
		int nv0 = rod0.getNumNode();
		int nv1 = rod1.getNumNode();
		int nv = 0.5*(nv0 + nv1);
		rod0.resample(nv);
		rod1.resample(nv);
		rod0.readaptFrames();
		rod0.readaptFrames();
		for (int j = 0; j < nv; ++j)
		{
			rod0.setPosition(j, 0.5*(rod0.getPosition(j) + rod1.getPosition(j)));
		}
		rod0.readaptFrames();

		vdelRods.push_back(vdupRods[i].second);
	}

	// Remove rods

	sort(vdelRods.begin(), vdelRods.end());

	int nd = (int)vdelRods.size();
	for (int i = nd - 1; i >= 0; --i)
		this->removeRod(vdelRods[i]);


	return true;
}

bool RodMesh::split_Connection(int nodeIdx0, int nodeIdx1, const string& pname, iVector& vaddRods, iVector& vdelRods)
{
	// Get con selection

	int conIdx = getConForNode(nodeIdx0);
	Vector3d cp = this->getConPoint(conIdx);

	// Get rod selection
	
	int dummy;
	int rodIdx;
	this->getRodNodeForMeshNode(nodeIdx1, rodIdx, dummy);

	// Duplicate the rod

	this->addRod(this->getRod(rodIdx, pname), 0.01, "", vaddRods, vdelRods, false);

	Vector3d bin;
	if (this->getRodCons(rodIdx).first == conIdx)
		bin = this->getRod(rodIdx).getFrames().front().bin;
	if (this->getRodCons(rodIdx).second == conIdx)
		bin = this->getRod(rodIdx).getFrames().back().bin;

	int dupIdx0 = rodIdx;
	int dupIdx1 = vaddRods[0];

	// Duplicate connection 

	this->m_vcons.push_back(this->m_vcons[conIdx]);
	for (int i = 0; i < (int) this->m_vprots.size(); ++i)
	{
		const string& prop = this->m_vprops[i];
		Matrix3d R = m_vprots[prop][conIdx];
		this->m_vprots[prop].push_back(R);
	}
	
	Con& con0 = this->m_vcons[conIdx];
	Con& con1 = this->m_vcons.back();

	// Partition rods

	vector<pair<int,int>> vrodSet0;
	vector<pair<int, int>> vrodSet1;
	
	set<pair<int, int>>::const_iterator
		it_end = this->m_vcons[conIdx].m_srods.end(),
		it_cur = this->m_vcons[conIdx].m_srods.begin();
	for (; it_cur != it_end; ++it_cur)
	{
		int ri = it_cur->first;
		int si = it_cur->second;
		
		if (ri == dupIdx0)
		{
			vrodSet0.push_back(*it_cur);
			continue;
		}

		if (ri == dupIdx1)
		{
			vrodSet1.push_back(*it_cur);
			continue;
		}
		
		Rod& rod = this->getRod(ri, pname);
		int nv = rod.getCurve().getNumNode();
		Vector3d rp;
		if (si == 0)
			rp = rod.getPosition(1);
		else rp = rod.getPosition(nv-2);

		if ((rp - cp).dot(bin) >= 0)
			vrodSet0.push_back(*it_cur);
		else vrodSet1.push_back(*it_cur);
	}

	con0.m_srods.clear();
	con1.m_srods.clear();
	for (int i = 0; i < (int) vrodSet0.size(); ++i)
		con0.m_srods.insert(vrodSet0[i]);
	for (int i = 0; i < (int) vrodSet1.size(); ++i)
		con1.m_srods.insert(vrodSet1[i]);

	this->updateCache();

	// Separate rods

	Real L = this->getRod(dupIdx0).getCurve().getLength();

	for (int i = 0; i < (int) vrodSet0.size(); ++i)
	{
		int ri = vrodSet0[i].first;
		int si = vrodSet0[i].second;
		Rod& rod = this->getRod(ri, pname);
		int nv = rod.getCurve().getNumNode();
		if (si == 0)
		{
			for (int j = 0; j < nv - 1; ++j)
			{
				Vector3d rp = rod.getPosition(j);
				Real alpha = (Real)(nv - 1 - j) / (Real)(nv - 1);
				rod.setPosition(j, rp + alpha*0.15*L*bin);
			}
		}
		else
		{
			for (int j = nv-1; j >= 1; --j)
			{
				Vector3d rp = rod.getPosition(j);
				Real alpha = (Real) j / (Real)(nv - 1);
				rod.setPosition(j, rp + alpha*0.15*L*bin);
			}
		}

		rod.readaptFrames();

		this->setRod(ri, rod, pname, false);
	}

	for (int i = 0; i < (int)vrodSet1.size(); ++i)
	{
		int ri = vrodSet1[i].first;
		int si = vrodSet1[i].second;
		Rod& rod = this->getRod(ri, pname);
		int nv = rod.getCurve().getNumNode();
		if (si == 0)
		{
			for (int j = 0; j < nv - 1; ++j)
			{
				Vector3d rp = rod.getPosition(j);
				Real alpha = (Real)(nv - 1 - j) / (Real)(nv - 1);
				rod.setPosition(j, rp - alpha*0.15*L*bin);
			}
		}
		else
		{
			for (int j = nv - 1; j >= 1; --j)
			{
				Vector3d rp = rod.getPosition(j);
				Real alpha = (Real)j / (Real)(nv - 1);
				rod.setPosition(j, rp - alpha*0.15*L*bin);
			}
		}
		rod.readaptFrames();

		this->setRod(ri, rod, pname, false);
	}

	return true;
}

bool RodMesh::isConnection(int idx) const
{
	return this->m_vwholeToSplit[this->m_vsplitToWhole[idx][0]].size() != 1;
}

int RodMesh::getRodForNode(int idx) const
{
	int rodIdx, nodeIdx;
	this->getRodNodeForMeshNode(idx, rodIdx, nodeIdx);
	return rodIdx;
}

int RodMesh::getConForNode(int idx) const
{
	for (int i = 0; i < this->getNumCon(); ++i)
	{
		if (find(this->m_vmapConNode[i].begin(), this->m_vmapConNode[i].end(), idx) != this->m_vmapConNode[i].end())
			return i;
	}
	
	return -1;
}