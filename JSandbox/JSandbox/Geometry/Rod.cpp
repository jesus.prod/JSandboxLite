/*=====================================================================================*/
/*!
/file		Rod.cpp
/author		jesusprod
/brief		Implementation of Rod.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/Rod.h>

Rod::Rod()
{
	// Nothing to do here...
}

Rod::Rod(const Curve& curve, const vector<Frame3d>& vF, const vector<Vector2d>& vr)
{
	this->initialize(curve, vF, vr);
}

Rod::Rod(const Rod& toCopy)
{
	this->initialize(toCopy.getCurve(), toCopy.getFrames(), toCopy.getRadius());
}

void Rod::initialize(const Curve& curve, const vector<Frame3d>& vF, const vector<Vector2d>& vr)
{
	this->m_center = curve;

	int ne = this->m_center.getNumEdge();

	if ((int) vF.size() != ne)
	{
		this->initialFrames();
	}
	else
	{
		this->m_vF = vF;

		//// Readapt just in case
		//this->readaptFrames();
	}

	if ((int)vr.size() != ne)
	{
		this->m_vr.resize(ne);
		for (int i = 0; i < ne; ++i)
			this->m_vr[i] = Vector2d(0.01, 0.01);
	}
	else this->m_vr = vr;
}

void Rod::resample(int nPoint)
{
	int ne = this->m_center.getNumEdge();

	dVector vs; // Get current lengths
	this->m_center.getEdgesLength(vs);

	dVector vse(ne - 1);
	for (int i = 0; i < ne - 1; ++i)
		vse[i] = 0.5*(vs[i] + vs[i + 1]);

	// Resample centerline first
	this->m_center.resample(nPoint);

	vector<Frame> vFnew;
	vector<Vector2d> vrnew;

	int nenew = this->m_center.getNumEdge();

	if (ne != 1)
	{
		hermiteInterpolation(this->m_vF, vse, vFnew, false, nenew);
		hermiteInterpolation(this->m_vr, vse, vrnew, false, nenew);
	}
	else
	{
		vFnew.resize(nenew, this->m_vF[0]);
		vrnew.resize(nenew, this->m_vr[0]);
	}

	this->m_vF = vFnew;
	this->m_vr = vrnew;
}

void Rod::resample(double tol)
{
	int ne = this->m_center.getNumEdge();

	dVector vs; // Get current lengths
	this->m_center.getEdgesLength(vs);

	dVector vse(ne - 1);
	for (int i = 0; i < ne - 1; ++i)
		vse[i] = 0.5*(vs[i] + vs[i + 1]);

	// Resample centerline first
	this->m_center.resample(tol);

	vector<Frame> vFnew;
	vector<Vector2d> vrnew;

	int nenew = this->m_center.getNumEdge();

	if (ne != 1)
	{
		hermiteInterpolation(this->m_vF, vse, vFnew, false, nenew);
		hermiteInterpolation(this->m_vr, vse, vrnew, false, nenew);
	}
	else
	{
		vFnew.resize(nenew, this->m_vF[0]);
		vrnew.resize(nenew, this->m_vr[0]);
	}

	this->m_vF = vFnew;
	this->m_vr = vrnew;
}

void Rod::remove(int i) // Remove vertex i
{
	int pe, ne;
	this->m_center.getNodeNeighborEdges(i, pe, ne);
	if (pe == -1) // First
	{
		this->m_vF.erase(this->m_vF.begin());
		this->m_vr.erase(this->m_vr.begin());
		this->m_center.remove(i);
	}
	if (ne == -1)
	{
		this->m_vF.erase(this->m_vF.end() - 1);
		this->m_vr.erase(this->m_vr.end() - 1);
		this->m_center.remove(i);
	}

	Vector3d nmean;
	nmean.setZero();
	Vector2d rmean;
	rmean.setZero();
	nmean += this->getFrame(pe).nor;
	nmean += this->getFrame(ne).nor;
	rmean += this->getRadius(pe);
	rmean += this->getRadius(ne);
	nmean /= 2;
	rmean /= 2;

	this->m_center.remove(i);

	Frame3d Fmean;
	Fmean.tan = this->m_center.getEdgeTangent(pe);
	Fmean.nor = nmean;
	Fmean.bin = Fmean.tan.cross(Fmean.nor).normalized();
	Fmean.nor = Fmean.bin.cross(Fmean.tan).normalized();

	this->m_vF.erase(this->m_vF.begin() + ne);
	this->m_vr.erase(this->m_vr.begin() + ne);
	this->m_vr[pe] = rmean;
	this->m_vF[pe] = Fmean;
}

void Rod::insertBack(const Vector3d& x) // Insert new vertex at end
{
	this->m_center.insertBack(x);
	this->m_vr.push_back(this->m_vr.back());
	this->m_vF.push_back(parallelTransport(this->m_vF.back(), m_center.getEdgeTangent(m_center.getNumEdge()-1)));
}

void Rod::insertFront(const Vector3d& x) // Insert new vertex at beginning
{
	this->m_center.insertFront(x);
	this->m_vr.insert(this->m_vr.begin(), this->m_vr.front());
	this->m_vF.insert(this->m_vF.begin(), parallelTransport(this->m_vF.front(), m_center.getEdgeTangent(0)));
}

int Rod::insertAtNode(int i, const Vector3d& x) // Insert new vertex at position
{
	int nv = this->m_center.getNumNode();

	if (i == 0)
	{
		this->insertFront(x);
		return 0;
	}

	if (i == nv)
	{
		this->insertBack(x);
		return nv;
	}
	
	this->m_center.insertAtNode(i, x);
	this->m_vF.insert(this->m_vF.begin() + i - 1, Frame3d());
	this->m_vF[i - 1] =  parallelTransport(this->m_vF[i - 1], m_center.getEdgeTangent(i - 1));
	this->m_vF[i] = parallelTransport(this->m_vF[i - 1], m_center.getEdgeTangent(i));
	this->m_vr.insert(this->m_vr.begin() + i - 1, this->m_vr[i - 1]);

	return i;
}

int Rod::insertAtEdge(int ei, const Vector3d& x) // Insert new vertex at edge
{
	int vi = this->m_center.insertAtEdge(ei, x);
	this->m_vr.insert(this->m_vr.begin() + ei, this->m_vr[ei]);
	this->m_vF.insert(this->m_vF.begin() + ei, this->m_vF[ei]);
	return vi;
}

void Rod::SplitRod(const Rod& rin, const vector<SplitPoint>& vpoints, vector<Rod>& vrods)
{
	vector<SplitPoint> vpointsSorted = vpoints;
	std::sort(vpointsSorted.begin(), vpointsSorted.end());

	// Add surrounding split points

	int ne = rin.getNumEdge();

	SplitPoint iniReal = vpointsSorted.front();
	SplitPoint endReal = vpointsSorted.back();

	SplitPoint ini;
	ini.m_edgeIdx = 0;
	ini.m_edgeMu = 0.0;

	SplitPoint end;
	end.m_edgeMu = 1.0;
	end.m_edgeIdx = ne - 1;

	bool cutStart = true;
	if (iniReal.m_edgeIdx != ini.m_edgeIdx || abs(iniReal.m_edgeMu - ini.m_edgeMu) > EPS_POS)
	{
		vpointsSorted.insert(vpointsSorted.begin(), ini);
		cutStart = false;
	}

	bool cutEnd = true;
	if (endReal.m_edgeIdx != end.m_edgeIdx || abs(endReal.m_edgeMu - end.m_edgeMu) > EPS_POS)
	{
		vpointsSorted.insert(vpointsSorted.end(), end);
		cutEnd = false;
	}

	int nc = (int)vpointsSorted.size();
	bool cutExtreme = cutStart || cutEnd;

	vrods.clear();

	// Get intermediate splitted curves
	for (int i = 1; i < nc - 2; i++)
	{
		vrods.push_back(Rod());
		Rod::SplitRod(rin, vpointsSorted[i], vpointsSorted[i + 1], vrods.back());
	}

	Rod first;
	Rod last;
	Rod::SplitRod(rin, vpointsSorted[0], vpointsSorted[1], first);
	Rod::SplitRod(rin, vpointsSorted[nc - 2], vpointsSorted[nc - 1], last);

	if (rin.getCurve().getIsClosed() && !cutExtreme)
	{
		vrods.push_back(Rod()); // Join first-last
		Rod::MergeRods(last, first, vrods.back());
	}
	else
	{
		vrods.push_back(first);
		if (vpointsSorted.size() > 2)
			vrods.push_back(last);
	}
}

void Rod::SplitRod(const Rod& rin, const SplitPoint& sp0, const SplitPoint& sp1, Rod& rout)
{
	assert(sp0.m_edgeIdx < sp1.m_edgeIdx || (sp0.m_edgeIdx == sp1.m_edgeIdx && sp0.m_edgeMu < sp1.m_edgeMu));

	const Curve& cin = rin.getCurve();

	Curve toSplit(cin.m_vx, false);
	if (cin.m_isClosed) // Repeat first index
		toSplit.insertBack(cin.getPosition(0));

	int inc = 0;

	vector<Frame> toSplitF = rin.getFrames();
	vector<Vector2d> toSplitR = rin.getRadius();

	// Get index 0
	int ci0 = -1;
	if (sp0.m_edgeMu > EPS_POS && sp0.m_edgeMu < 1 - EPS_POS)
	{
		int pvi, nvi;
		cin.getEdgeNeighborNodes(sp0.m_edgeIdx, pvi, nvi);
		Vector3d A = get3D(pvi, toSplit.m_vx);
		Vector3d B = get3D(nvi, toSplit.m_vx);

		Vector3d P = A + (B - A)*sp0.m_edgeMu; // Add
		ci0 = toSplit.insertAtEdge(sp0.m_edgeIdx, P);

		toSplitF.insert(toSplitF.begin() + sp0.m_edgeIdx, toSplitF[sp0.m_edgeIdx]);
		toSplitR.insert(toSplitR.begin() + sp0.m_edgeIdx, toSplitR[sp0.m_edgeIdx]);

		inc++; // Vertex added
	}
	else
	{
		int pvi, nvi;
		toSplit.getEdgeNeighborNodes(sp0.m_edgeIdx, pvi, nvi);
		ci0 = (sp0.m_edgeMu > EPS_POS) ? nvi : pvi; // Next or previous
	}

	// Get index 1
	int ci1 = -1;
	if (sp1.m_edgeMu > EPS_POS && sp1.m_edgeMu < 1 - EPS_POS)
	{
		int pvi, nvi;
		cin.getEdgeNeighborNodes(sp1.m_edgeIdx, pvi, nvi);
		Vector3d A = get3D(pvi + inc, toSplit.m_vx);
		Vector3d B = get3D(nvi + inc, toSplit.m_vx);
		Vector3d P = A + (B - A)*sp1.m_edgeMu; // Add

		ci1 = toSplit.insertAtEdge(sp1.m_edgeIdx + inc, P);

		toSplitF.insert(toSplitF.begin() + sp0.m_edgeIdx + inc, toSplitF[sp0.m_edgeIdx]);
		toSplitR.insert(toSplitR.begin() + sp0.m_edgeIdx + inc, toSplitR[sp0.m_edgeIdx]);
	}
	else
	{
		int pvi, nvi;
		toSplit.getEdgeNeighborNodes(sp1.m_edgeIdx + inc, pvi, nvi);
		ci1 = (sp1.m_edgeMu > EPS_POS) ? nvi : pvi; // Next or previous
	}

	int ne = ci1-ci0;
	int nv = ne + 1;

	dVector vx(nv);
	vector<Frame> vF(ne);
	vector<Vector2d> vR(ne);
	for (int i = 0; i < ne; ++i)
	{
		vF[i] = toSplitF[ci0 + i];
		vR[i] = toSplitR[ci0 + i];
	}
	getSubvector(3 * ci0, 3 * nv, toSplit.getPositions(), vx);

	rout.initialize(Curve(vx, false), vF, vR);
}

void Rod::MergeRods(const Rod& rin0, const Rod& rin1, Rod& rout)
{
	int n1 = rin1.getNumNode();
	int n0 = rin0.getNumNode() - 1;
	//int ne = rin1.getNumEdge() +
	//		   rin0.getNumEdge();

	Vector3d p0 = rin0.getPosition(0);
	Vector3d p1 = rin1.getPosition(n1-1);
	//bool isClosed = (p0 - p1).norm() < 1e-3;

	dVector vx0;
	dVector vx1;
	getSubvector(0, 3 * n0, rin0.getCurve().getPositions(), vx0);
	getSubvector(0, 3 * n1, rin1.getCurve().getPositions(), vx1);

	dVector vx(3 * (n0 + n1));
	setSubvector(0, 3 * n0, vx0, vx);
	setSubvector(3 * n0, 3 * n1, vx1, vx);

	vector<Frame> vF;
	vF.insert(vF.end(), rin0.getFrames().begin(), rin0.getFrames().end());
	vF.insert(vF.end(), rin1.getFrames().begin(), rin1.getFrames().end());

	vector<Vector2d> vR;
	vR.insert(vR.end(), rin0.getRadius().begin(), rin0.getRadius().end());
	vR.insert(vR.end(), rin1.getRadius().begin(), rin1.getRadius().end());

	rout.initialize(Curve(vx, false), vF, vR);
}

void Rod::getNodeFrames(vector<Frame3d>& vF) const
{
	// TODO: Optimize this to avoid surrounding

	int numn = this->m_center.getNumNode();

	if (this->m_center.getIsClosed())
	{
		vF.resize(numn);
		for (int i = 0; i < numn; ++i)
		{
			int pe, ne;
			this->m_center.getNodeNeighborEdges(i, pe, ne);
			vF[i].tan = 0.5*(this->m_vF[pe].tan + this->m_vF[ne].tan);
			vF[i].nor = 0.5*(this->m_vF[pe].nor + this->m_vF[ne].nor);
			vF[i].bin = 0.5*(this->m_vF[pe].bin + this->m_vF[ne].bin);

			// Ensure orthonormal
			vF[i].tan.normalize();
			vF[i].bin = vF[i].tan.cross(vF[i].nor).normalized();
			vF[i].nor = vF[i].bin.cross(vF[i].tan).normalized();
		}
	}
	else
	{
		vF.resize(numn);
		vF[0] = this->m_vF[0];
		vF[numn - 1] = this->m_vF[numn - 2];

		for (int i = 1; i < numn - 1; ++i)
		{
			int pe, ne;
			this->m_center.getNodeNeighborEdges(i, pe, ne);
			vF[i].tan = 0.5*(this->m_vF[pe].tan + this->m_vF[ne].tan);
			vF[i].nor = 0.5*(this->m_vF[pe].nor + this->m_vF[ne].nor);
			vF[i].bin = 0.5*(this->m_vF[pe].bin + this->m_vF[ne].bin);

			// Ensure orthonormal
			vF[i].tan.normalize();
			vF[i].bin = vF[i].tan.cross(vF[i].nor).normalized();
			vF[i].nor = vF[i].bin.cross(vF[i].tan).normalized();
		}
	}
}

void Rod::getNodeRadius(vector<Vector2d>& vr) const
{
	// TODO: Optimize this to avoid surrounding

	int numn = this->m_center.getNumNode();

	if (this->m_center.getIsClosed())
	{
		vr.resize(numn);
		for (int i = 0; i < numn; ++i)
		{
			int pe, ne;
			m_center.getNodeNeighborEdges(i, pe, ne);
			vr[i] = 0.5*(this->m_vr[pe] + this->m_vr[ne]);
		}
	}
	else
	{
		vr.resize(numn);
		vr[0] = this->m_vr[0];
		vr[numn - 1] = this->m_vr[numn - 2];

		for (int i = 1; i < numn - 1; ++i)
		{
			int pe, ne;
			m_center.getNodeNeighborEdges(i, pe, ne);
			vr[i] = 0.5*(this->m_vr[pe] + this->m_vr[ne]);
		}
	}
}

void Rod::initialFrames()
{
	// Initialize frames to twist-free

	Vector3d r(0.0, 0.0, 1.0);
	this->m_vF.resize(this->getNumEdge());
	Vector3d t = this->m_center.getEdgeTangent(0);

	if (isApprox(abs(r.dot(t)), 1.0, EPS_POS))
	{
		r = Vector3d((double)rand() / (double)RAND_MAX,
					 (double)rand() / (double)RAND_MAX,
					 (double)rand() / (double)RAND_MAX);
	}

	Vector3d b = t.cross(r).normalized();
	Vector3d n = b.cross(t).normalized();

	Frame3d F;
	F.tan = t;
	F.bin = b;
	F.nor = n;
	Curve::FramePTForward(F, this->m_center, this->m_vF);
}

void Rod::readaptFrames()
{
	// Ensure all frames are readapted

	int ne = this->getNumEdge();
	for (int i = 0; i < ne; ++i)
	{
		this->m_vF[i] = parallelTransport(this->m_vF[i], this->m_center.getEdgeTangent(i));
	}
}

void Rod::reverse()
{
	int nv = this->getNumNode();
	int ne = this->getNumEdge();

	dVector vposnew;
	vector<Vector2d> vrnew;
	vector<Frame3d> vFnew;
	vposnew.resize(3*nv);
	vrnew.reserve(ne);
	vFnew.reserve(ne);
	
	const dVector& vpos = this->m_center.getPositions();

	for (int i = nv - 1; i >= 0; --i)
	{
		set3D(i, get3D(nv - 1 - i, vpos), vposnew);
	}

	for (int i = ne - 1; i >= 0; --i)
	{
		vrnew.push_back(this->m_vr[i]);
		vFnew.push_back(this->m_vF[i]);
	}
	
	// Reverse the frames
	for (int i = 0; i < ne; ++i)
		vFnew[i] = vFnew[i].reverse();

	this->m_center = Curve(vposnew, this->m_center.getIsClosed());
	this->m_vr = vrnew;
	this->m_vF = vFnew;
}