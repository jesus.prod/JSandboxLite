/*=====================================================================================*/
/*!
\file		Curve3DAngleElement.cpp
\author		jesusprod
\brief		Implementation of Curve3DAngleElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/Curve3DAngleElement.h>

Curve3DAngleElement::Curve3DAngleElement(int dim0) : NodalSolidElement(3, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);
}

Curve3DAngleElement::~Curve3DAngleElement()
{
	// Nothing to do here
}

void Curve3DAngleElement::update_Rest(const VectorXd& vX)
{
	Vector3d x0;
	Vector3d x1;
	Vector3d x2;

	if (this->m_numDim_0 == 2)
	{
		x0 = Vector3d(vX(this->m_vidx0[0]), vX(this->m_vidx0[1]), 0.0);
		x1 = Vector3d(vX(this->m_vidx0[2]), vX(this->m_vidx0[3]), 0.0);
		x2 = Vector3d(vX(this->m_vidx0[4]), vX(this->m_vidx0[5]), 0.0);
	}
	else if (this->m_numDim_0 == 3)
	{
		x0 = Vector3d(vX(this->m_vidx0[0]), vX(this->m_vidx0[1]), vX(this->m_vidx0[2]));
		x1 = Vector3d(vX(this->m_vidx0[3]), vX(this->m_vidx0[4]), vX(this->m_vidx0[5]));
		x2 = Vector3d(vX(this->m_vidx0[6]), vX(this->m_vidx0[7]), vX(this->m_vidx0[8]));
	}
	else assert(false); // Should not be here!

	Vector3d n0 = (x1 - x0);
	Vector3d n1 = (x2 - x1);
	double n0len = n0.norm();
	double n1len = n1.norm();
	this->m_vL0 = 0.5*(n0len + n1len);

	n0 /= n0len;
	n1 /= n1len;
	this->m_cosTh0 = n0.dot(n1);

//	{
//#include "../../Maple/Code/Curve3DAngleStretchRatio.mcg"
//
//		this->m_cosTh0 = t36;
//	}
}

void Curve3DAngleElement::update_Mass(const VectorXd& vX)
{
	// Does not carry mass
	this->m_vmx.setZero();
}

void Curve3DAngleElement::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	Vector3d x0(vx(this->m_vidxx[0]), vx(this->m_vidxx[1]), vx(this->m_vidxx[2]));
	Vector3d x1(vx(this->m_vidxx[3]), vx(this->m_vidxx[4]), vx(this->m_vidxx[5]));
	Vector3d x2(vx(this->m_vidxx[6]), vx(this->m_vidxx[7]), vx(this->m_vidxx[8]));

	// --------------------------------------------------

	Real kB = m_vpmat[0]->getBendingK();
	Real cosTh0 = this->m_cosTh0;
	//Real r0 = this->m_cosTh0;
	Real vL0 = this->m_vL0;

	{
#include "../../Maple/Code/Curve3DAngleEnergy.mcg"

		this->m_energy = t50;
	}

//	{
//#include "../../Maple/Code/Curve3DAngleStretchEnergy.mcg"
//	
//		this->m_energy = t41;
//	}
}

void Curve3DAngleElement::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	Vector3d x0(vx(this->m_vidxx[0]), vx(this->m_vidxx[1]), vx(this->m_vidxx[2]));
	Vector3d x1(vx(this->m_vidxx[3]), vx(this->m_vidxx[4]), vx(this->m_vidxx[5]));
	Vector3d x2(vx(this->m_vidxx[6]), vx(this->m_vidxx[7]), vx(this->m_vidxx[8]));

	// --------------------------------------------------

	Real kB = m_vpmat[0]->getBendingK();
	Real cosTh0 = this->m_cosTh0;
	//Real r0 = this->m_cosTh0;
	Real vL0 = this->m_vL0;

	dVector vfx(9);

	{
#include "../../Maple/Code/Curve3DAngleForce.mcg"
	}

//	{
//#include "../../Maple/Code/Curve3DAngleStretchForce.mcg"
//	}

	for (int i = 0; i < 9; ++i)
		if (!isfinite(vfx[i]))
			logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 9; ++i)
		this->m_vfVal(i) = vfx[i];
}

void Curve3DAngleElement::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	Vector3d x0(vx(this->m_vidxx[0]), vx(this->m_vidxx[1]), vx(this->m_vidxx[2]));
	Vector3d x1(vx(this->m_vidxx[3]), vx(this->m_vidxx[4]), vx(this->m_vidxx[5]));
	Vector3d x2(vx(this->m_vidxx[6]), vx(this->m_vidxx[7]), vx(this->m_vidxx[8]));

	// --------------------------------------------------

	Real kB = m_vpmat[0]->getBendingK();
	Real cosTh0 = this->m_cosTh0;
	//Real r0 = this->m_cosTh0;
	Real vL0 = this->m_vL0;

	double mJx[9][9];

	{
#include "../../Maple/Code/Curve3DAngleJacobian.mcg"
	}

//	{
//#include "../../Maple/Code/Curve3DAngleStretchJacobian.mcg"
//	}

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}

void Curve3DAngleElement::update_DfxDx(const VectorXd& vx, const VectorXd& vX)
{
	Vector3d x0(vx(this->m_vidxx[0]), vx(this->m_vidxx[1]), vx(this->m_vidxx[2]));
	Vector3d x1(vx(this->m_vidxx[3]), vx(this->m_vidxx[4]), vx(this->m_vidxx[5]));
	Vector3d x2(vx(this->m_vidxx[6]), vx(this->m_vidxx[7]), vx(this->m_vidxx[8]));

	Vector3d X0;
	Vector3d X1;
	Vector3d X2;
	if (this->m_numDim_0 == 2)
	{
		X0 = Vector3d(vX(this->m_vidx0[0]), vX(this->m_vidx0[1]), 0.0);
		X1 = Vector3d(vX(this->m_vidx0[2]), vX(this->m_vidx0[3]), 0.0);
		X2 = Vector3d(vX(this->m_vidx0[4]), vX(this->m_vidx0[5]), 0.0);
	}
	else if (this->m_numDim_0 == 3)
	{
		X0 = Vector3d(vX(this->m_vidx0[0]), vX(this->m_vidx0[1]), this->m_vidx0[2]);
		X1 = Vector3d(vX(this->m_vidx0[3]), vX(this->m_vidx0[4]), this->m_vidx0[5]);
		X2 = Vector3d(vX(this->m_vidx0[6]), vX(this->m_vidx0[7]), this->m_vidx0[8]);
	}

	Real kB = m_vpmat[0]->getBendingK();
	Real C = 1.0;

	double mDfDx[9][9];

#include "../../Maple/Code/Curve3DAngleRest_DfDx.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mDfDx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j < this->m_numDOFx; ++j)
			this->m_mDfxDx(i, j) = mDfDx[i][j];
}

void Curve3DAngleElement::update_DfxD0(const VectorXd& vx, const VectorXd& vX)
{
	Vector3d x0(vx(this->m_vidxx[0]), vx(this->m_vidxx[1]), vx(this->m_vidxx[2]));
	Vector3d x1(vx(this->m_vidxx[3]), vx(this->m_vidxx[4]), vx(this->m_vidxx[5]));
	Vector3d x2(vx(this->m_vidxx[6]), vx(this->m_vidxx[7]), vx(this->m_vidxx[8]));

	Vector3d X0;
	Vector3d X1;
	Vector3d X2;
	if (this->m_numDim_0 == 2)
	{
		X0 = Vector3d(vX(this->m_vidx0[0]), vX(this->m_vidx0[1]), 0.0);
		X1 = Vector3d(vX(this->m_vidx0[2]), vX(this->m_vidx0[3]), 0.0);
		X2 = Vector3d(vX(this->m_vidx0[4]), vX(this->m_vidx0[5]), 0.0);
	}
	else if (this->m_numDim_0 == 3)
	{
		X0 = Vector3d(vX(this->m_vidx0[0]), vX(this->m_vidx0[1]), this->m_vidx0[2]);
		X1 = Vector3d(vX(this->m_vidx0[3]), vX(this->m_vidx0[4]), this->m_vidx0[5]);
		X2 = Vector3d(vX(this->m_vidx0[6]), vX(this->m_vidx0[7]), this->m_vidx0[8]);
	}

	Real kB = m_vpmat[0]->getBendingK();
	Real C = 1.0;

	double mDfD0[9][9];

#include "../../Maple/Code/Curve3DAngleRest_DfD0.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mDfD0[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_0; ++k)
			{
				this->m_mDfxD0(i, this->m_numDim_0*j + k) = mDfD0[i][this->m_numDim_x*j + k];
			}
		}
	}
}