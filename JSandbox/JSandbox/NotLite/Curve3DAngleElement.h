/*=====================================================================================*/
/*!
\file		Curve3DAngleElement.h
\brief		jesusprod
\brief		Blah, blah, blah, TODO.
*/
/*=====================================================================================*/

#ifndef CURVE3D_ANGLE_ELEMENT_H
#define CURVE3D_ANGLE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>

#include <JSandbox/Element/NodalSolidElement.h>

class JSANDBOX_EXPORT Curve3DAngleElement : public NodalSolidElement
{
public:
	Curve3DAngleElement(int dim0 = 3);
	~Curve3DAngleElement();

	virtual void update_Rest(const VectorXd& vX);
	virtual void update_Mass(const VectorXd& vX);

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual void update_DfxDx(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DfxD0(const VectorXd& vx, const VectorXd& vX);

	virtual void update_DmxD0(const VectorXd& vx, const VectorXd& vX) { }

	virtual Real getIntegrationVolume() const { return m_vL0; }

	virtual Real getVertexArea() const { return this->m_vL0; }
	virtual void setVertexArea(Real vL) { this->m_vL0 = vL; }

	virtual Real getRestAngle() const { return this->m_cosTh0; }
	virtual void setRestAngle(Real ang) { this->m_cosTh0 = ang; }

protected:
	Real m_cosTh0;
	Real m_vL0;

};

#endif