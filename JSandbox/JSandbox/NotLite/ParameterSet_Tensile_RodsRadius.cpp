/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_RodsRadius.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_RodsRadius.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_RodsRadius.h>

ParameterSet_Tensile_RodsRadius::ParameterSet_Tensile_RodsRadius(int midx) : ParameterSet("TENRODSRADIUS")
{
	assert(midx >= 0);
	this->m_midx = midx;

	this->m_isAni = true;
}

ParameterSet_Tensile_RodsRadius::~ParameterSet_Tensile_RodsRadius()
{
	// Nothing to do here
}

void ParameterSet_Tensile_RodsRadius::getParameters(VectorXd& vp) const
{
	this->m_pTSModel->getRodMeshModel(this->m_midx)->getParam_RadiusRod(vp);

	if (!this->m_isAni)
	{
		throw new exception("Not implemented yet...");
	}

	assert((int)vp.size() == this->m_np);
}

void ParameterSet_Tensile_RodsRadius::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	VectorXd vpB = vp;
	this->limitParameters(vpB);
	this->m_pTSModel->getRodMeshModel(this->m_midx)->setParam_RadiusRod(vpB);

	if (!this->m_isAni)
	{
		throw new exception("Not implemented yet...");
	}
}

bool ParameterSet_Tensile_RodsRadius::isReady_fp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_frr();

	return true;
}

bool ParameterSet_Tensile_RodsRadius::isReady_DfDp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_DfxDrr();

	return true;
}

void ParameterSet_Tensile_RodsRadius::get_fp(VectorXd& vfp) const
{
	this->m_pTSModel->getRodMeshModel(this->m_midx)->add_frr(vfp);

	if (!this->m_isAni)
	{
		throw new exception("Not implemented yet...");
	}
}

void ParameterSet_Tensile_RodsRadius::get_DfDp(tVector& vDfDp_t) const
{
	// TODO: Convert to isotropic 

	tVector vDfDp_i;
	vDfDp_i.reserve(this->m_pTSModel->getRodMeshModel(this->m_midx)->getNumNonZeros_DfxDrr());
	vDfDp_t.reserve(this->m_pTSModel->getRodMeshModel(this->m_midx)->getNumNonZeros_DfxDrr());
	this->m_pTSModel->getRodMeshModel(this->m_midx)->add_DfxDrr(vDfDp_i);
	int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);
	this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_i, vDfDp_t);

	if (!this->m_isAni)
	{
		throw new exception("Not implemented yet...");
	}
}

void ParameterSet_Tensile_RodsRadius::setup()
{
	assert(this->m_pModel != NULL);
	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	this->m_np = (int) this->m_pTSModel->getRodMeshModel(this->m_midx)->getSimMesh_0().getNumRod();

	// Anisotropic?
	if (this->m_isAni) 
		this->m_np *= 2;

	this->updateBoundsVector();
}