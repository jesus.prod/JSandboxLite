/*=====================================================================================*/
/*!
\file		SurfaceModel.h
\author		jesusprod
\brief		Custom made model for generic deformable surfaces. It provides the necessary
			functions to create a discretization using different surface elements. The
			current implementation is restricted to triangle meshes.
*/
/*=====================================================================================*/

#ifndef SURFACE_MODEL_H
#define SURFACE_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/TriMesh.h>

#include <JSandbox/Model/NodalSolidModel.h>

class JSANDBOX_EXPORT SurfaceModel : public NodalSolidModel
{
public:

	// Deformation model

	enum Model
	{
		QuadArea = 0,					// Simple area preserving energy, no bending or stretch energy
		MS_QuadArea = 1,				// Simple area preserving energy with mass-spring stretch, no bending
		DS_MS_QuadArea = 2,				// Discrete-shells bending + mass-spring stretch + area preserving energy
		DS_CSTStretch = 3,				// Discrete-shells bending + FEM-based constant straint triangles
	};

public:
	SurfaceModel();
	virtual ~SurfaceModel();

	/* From Solid Model */

	virtual void setup();

	virtual void deprecateDeformation();
	virtual void deprecateDiscretization();

	/* From Solid Model */

	virtual void configureModel(SurfaceModel::Model DM);
	virtual void configureMesh(const TriMesh& pSurfMesh);

	virtual TriMesh& getSimMesh() { if (!this->m_isReady_Mesh) this->update_SimMeshFromState(); return *(this->m_pSimMesh); }

	virtual void remeshDelaunay(const pVector& vnodesExtI, 
								const iVector& vedgesExtI, 
								pVector& vnodesExtO,
								int numV = -1, 
								Real minA = 0.0, 
								Real internalMaxL = HUGE_VAL, 
								Real boundaryMaxL = HUGE_VAL)
	{ throw new exception("[ERROR] Not implemented yet"); }

	virtual void setRemeshInternalTolerance(Real tol) { this->m_remeshTolI = tol; }
	virtual Real getRemeshInternalTolerance() const { return this->m_remeshTolI; }

	virtual void setRemeshBoundaryTolerance(Real tol) { this->m_remeshTolI = tol; }
	virtual Real getRemeshBoundaryTolerance() const { return this->m_remeshTolI; }

	/* Harmonic related */

	virtual int getNumNonZeros_DfxDb0() { if (this->m_nNZ_DfxDb0 == -1) precompute_DfxDb0(); return this->m_nNZ_DfxDb0; }
	virtual int getNumNonZeros_DfxDc0() { if (this->m_nNZ_DfxDc0 == -1) precompute_DfxDc0(); return this->m_nNZ_DfxDc0; }
	virtual int getNumNonZeros_DfxDs() { if (this->m_nNZ_DfxDs == -1) precompute_DfxDs(); return this->m_nNZ_DfxDs; }
	virtual int getNumNonZeros_DfsDs() { if (this->m_nNZ_DfsDs == -1) precompute_DfsDs(); return this->m_nNZ_DfsDs; }

	virtual void configureHarmonicRest(bool useIt, const iVector& vcouplings);

	virtual const iVector& getCouplingNodes() const { return this->m_vcouplingList; }
	virtual const iVector& getInternalNodes() const { return this->m_vinternalList; }
	virtual const iVector& getBoundaryNodes() const { return this->m_vboundaryList; }

	virtual const iVector& getFull2BoundaryMap() const { return this->m_vfull2BoundaryMap; }
	virtual const iVector& getFull2CouplingMap() const { return this->m_vfull2CouplingMap; }
	virtual const iVector& getFull2InternalMap() const { return this->m_vfull2InternalMap; }

	const SparseMatrixXd& getInternalSelectionMatrix() const { return this->m_mSi; }
	const SparseMatrixXd& getBoundarySelectionMatrix() const { return this->m_mSb; }
	const SparseMatrixXd& getCouplingSelectionMatrix() const { return this->m_mSc; }

	const SparseMatrixXd& getBoundaryLaplacianMap() const { return this->m_mATemp; }
	const SparseMatrixXd& getCouplingLaplacianMap() const { return this->m_mBTemp; }

	virtual void setParam_CouplingRest(const VectorXd& vc0);
	virtual void getParam_CouplingRest(VectorXd& vc0) const;

	virtual void setParam_BoundaryRest(const VectorXd& vb0);
	virtual void getParam_BoundaryRest(VectorXd& vb0) const;

	virtual void setParam_Stretch(const VectorXd& vs);
	virtual void getParam_Stretch(VectorXd& vs) const;

	virtual bool isReady_fb0() { return this->isReady_f0(); }
	virtual bool isReady_fc0() { return this->isReady_f0(); }

	virtual bool isReady_DfxDb0() { return this->isReady_DfxD0(); }
	virtual bool isReady_DfxDc0() { return this->isReady_DfxD0(); }

	virtual bool isReady_fs() { return this->m_isReady_fs; }
	virtual bool isReady_Dfsds() { return this->m_isReady_DfsDs; }
	virtual bool isReady_DfxDs() { return this->m_isReady_DfxDs; }

	virtual void add_fb0(VectorXd& vfb0);
	virtual void add_fc0(VectorXd& vfc0);
	virtual void add_DfxDb0(tVector& vDfxDb0);
	virtual void add_DfxDc0(tVector& vDfxDc0);

	virtual void update_fs();
	virtual void update_DfsDs();
	virtual void update_DfxDs();

	virtual void add_fs(VectorXd& vfs);
	virtual void add_DfsDs(tVector& vDfsDs);
	virtual void add_DfxDs(tVector& vDfxDs);

	virtual Real get_Energy_Bending();
	virtual void add_Force_Bending(VectorXd& vfx);
	virtual void add_DfxDx_Bending(tVector& vDfxDx);

	virtual void add_fs_Bending(VectorXd& vfs);
	virtual void add_DfsDs_Bending(tVector& vDfsDs);
	virtual void add_DfxDs_Bending(tVector& vDfxDs);

	virtual const vector<SolidElement*>& getStretchElements() const { return this->m_vpStretchEles; }
	virtual const vector<SolidElement*>& getBendingElements() const { return this->m_vpBendingEles; }

	virtual void update_StateFromSimMesh();
	virtual void update_SimMeshFromState();

	virtual bool getParam_Dummy() const;
	virtual void setParam_Dummy(bool s);

	void onDiscretizationChanged(const iVector& vnewIdxRaw);

	__event	void discretizationChanged(SolidModel* pthis, const iVector& vnewIdxRaw);

protected:
	
	/* From Solid Model */

	virtual void freeElements();

	/* From Solid Model */

	virtual void freeMesh();
	virtual void initializeElements_QuadArea();
	virtual void initializeElements_MassSpring();
	virtual void initializeElements_CSTStretch();
	virtual void initializeElements_Bending();

	virtual void initializeMaterials();
	virtual void initializeHarmonicBoundary();
	//virtual void initializeHarmonicCoupling();
	virtual void precompute_DfxDb0();
	virtual void precompute_DfxDc0();
	virtual void precompute_DfxDs();
	virtual void precompute_DfsDs();

protected:
	Model m_DM;
	
	bool m_isDummy;

	TriMesh* m_pSimMesh;

	int m_nNZ_DfxDb0;
	int m_nNZ_DfxDc0;
	int m_nNZ_DfsDs;
	int m_nNZ_DfxDs;

	bool m_isReady_fs;
	bool m_isReady_DfsDs;
	bool m_isReady_DfxDs;

	iVector m_vboundaryList;
	iVector m_vcouplingList;
	iVector m_vinternalList;

	iVector m_vfull2BoundaryMap;
	iVector m_vfull2CouplingMap;
	iVector m_vfull2InternalMap;

	SparseMatrixXd m_mATemp;
	SparseMatrixXd m_mBTemp;

	SparseMatrixXd m_mSb;
	SparseMatrixXd m_mSc;
	SparseMatrixXd m_mSi;

	SparseMatrixXd m_mD0Db0; // Due to boundary vertices
	SparseMatrixXd m_mD0Dc0; // Due to coupling vertices

	//// TO DEPRECATE

	//int m_nNZ_DfxDh0;

	//iVector m_vharmonicMasList;
	//iVector m_vharmonicSlaList;

	//iVector m_vfull2MasMap;
	//iVector m_vfull2SlaMap;
	//iVector m_vmas2FullMap;
	//iVector m_vsla2FullMap;

	//SparseMatrixXd m_mD0Dh0;

	//VectorXd m_vh0;
	//VectorXd m_vx0;

	//// TO DEPRECATE

	Real m_remeshTolI;
	Real m_remeshTolB;

	vector<SolidElement*> m_vpStretchEles;
	vector<SolidElement*> m_vpBendingEles;

};

#endif