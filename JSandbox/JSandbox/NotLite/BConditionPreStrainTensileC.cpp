/*=====================================================================================*/
/*!
\file		BConditionPreStrainTensileC.cpp
\author		jesusprod
\brief		Implementation of BConditionPreStrainTensileC
/*=====================================================================================*/

#include <JSandbox/Solver/BConditionPreStrainTensileC.h>

#include <JSandbox/Model/DERModel.h>
#include <JSandbox/Model/RodMeshModel.h>
#include <JSandbox/Model/TensileStructureModel.h>

BConditionPreStrainTensileC::BConditionPreStrainTensileC()
{
	this->m_factor = 1.0;
	this->m_maxStep = 1;
	this->m_curStep = 1;

	this->m_rodIndex = -1;
}

BConditionPreStrainTensileC::~BConditionPreStrainTensileC()
{
	// Nothing to do here...
}

void BConditionPreStrainTensileC::constrain(SolidModel* pModel) const
{
	double factor = 1.0 - (1.0 - this->m_factor)*this->getLoadingFactor();
	TensileStructureModel* pTSModel = dynamic_cast<TensileStructureModel*>(pModel);

	RodMeshModel* pRMModel = pTSModel->getRodMeshModel(0);
	assert(this->m_rodIndex >= 0 && this->m_rodIndex < pRMModel->getNumRod());
	DERModel* pDERModel = dynamic_cast<DERModel*>(pRMModel->getModel(this->m_rodIndex));

	int numEle = pDERModel->getNumEle();

	// PreStrain elemetns
	for (int i = 0; i < numEle; ++i)
		pDERModel->getElement(i)->setPreStrain(factor);

	logSimu("[TRACE] Constraint applied: Rodmesh pre-strain factor %f\n", factor);
}