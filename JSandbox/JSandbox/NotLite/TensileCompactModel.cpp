/*=====================================================================================*/
/*!
\file		TensileCompactModel.cpp
\author		jesusprod
\brief		Implementation of TensileCompactModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/TensileCompactModel.h>

#include <JSandbox/Element/CSTElementStVKOrthotropic.h>

#include <omp.h> // MP support

TensileCompactModel::TensileCompactModel()
{
	// Nothing to do here...
}

TensileCompactModel::~TensileCompactModel()
{
	this->freeMembranes();

	logSimu("[TRACE] Destroying TensileCompactModel: %d", this);
}

void TensileCompactModel::setup()
{
	RodMeshModel::setup();

	int numM = (int) this->m_vpMeshes.size();

	this->m_vJBStrip.resize(numM);
	this->m_vxBStrip.resize(numM);
	this->m_vXBStrip.resize(numM);
	this->m_mHBStrip.resize(numM);
	this->m_vpMembraneEles.resize(numM);

	for (int i = 0; i < numM; ++i)
	{
		// Reset

		this->m_vJBStrip[i].clear();
		this->m_vxBStrip[i].setZero();
		this->m_vXBStrip[i].setZero();
		this->m_mHBStrip[i].setZero();

		int numV = this->m_vpMeshes[i]->getNumNode();
		int numB = (int) this->m_vpBoundIdx[i]->size();

		// Build mesh to mesh strip map

		// NOTE: First numB nodes of the strip
		// are the mesh boundary nodes. The rest
		// of the nodes correspond to neighbors.

		int numS = numB;

		iVector vmapMesh2Strip(numV, -1);

		for (int j = 0; j < numB; ++j)
			vmapMesh2Strip[m_vpBoundIdx[i]->at(j)] = j;

		for (int j = 0; j < numB; ++j)
		{
			TriMesh::VertexHandle vh = this->m_vpMeshes[i]->vertex_handle(m_vpBoundIdx[i]->at(j));

			vector<TriMesh::VertexHandle> vvh;
			m_vpMeshes[i]->getVertexVertices(vh, vvh);
			for (int k = 0; k < (int)vvh.size(); ++k)
			{
				if (vmapMesh2Strip[vvh[k].idx()] != -1)
					continue; // Already added to strip

				vmapMesh2Strip[vvh[k].idx()] = numS++;
			}
		}

		// Build strip laplacian matrix

		SparseMatrixXd mHMesh;

		this->m_vpMeshes[i]->computeBoundaryToMinimalMap_LaplacianRegular(mHMesh, VProperty::POS0);

		tVector vHMesh;
		toTriplets(mHMesh, vHMesh);
		int numCoeff = (int)vHMesh.size();

		// Select strip indices and map

		tVector vHStrip;
		vHStrip.reserve(numCoeff);
		for (int j = 0; j < numCoeff; ++j)
		{
			const Triplet<Real>& t = vHMesh[j];

			int meshNodeIdx = t.row() / 3;
			int boundNodeIdx = t.col() / 3;

			if (vmapMesh2Strip[meshNodeIdx] == -1)
				continue; // Not boundary strip node

			int stripNodeIdxSim = vmapMesh2Strip[meshNodeIdx];
			int rodMeshNode = (*m_vpBoundMap[i])[boundNodeIdx];
			iVector rodMeshNodeIdxRaw;
			iVector rodMeshNodeIdxSim;
			rodMeshNodeIdxRaw = getNodeRawDOF(rodMeshNode);
			mapRaw2Sim(rodMeshNodeIdxRaw, rodMeshNodeIdxSim);
			
	
			for (int k = 0; k < 3; ++k)
			{
				vHStrip.push_back(Triplet<Real>(
					3*stripNodeIdxSim+k,
					rodMeshNodeIdxSim[k],
					t.value()));
			}
		}

		// Create Laplacian strip matrix

		SparseMatrixXd mH(3*numS, this->getNumSimDOF_x());
		mH.setFromTriplets(vHStrip.begin(), vHStrip.end());
		mH.makeCompressed();

		this->m_mHBStrip[i] = mH.toDense();

		// Create membrane elements

		for (TriMesh::FaceIter it = this->m_vpMeshes[i]->faces_begin(); it != this->m_vpMeshes[i]->faces_end(); ++it)
		{
			if (!m_vpMeshes[i]->is_boundary(*it, true))
				continue; // This is not a boundary face

			iVector vinds(3);
			vector<TriMesh::VertexHandle> vvh;
			m_vpMeshes[i]->getFaceVertices(*it, vvh);
			vinds[0] = vmapMesh2Strip[vvh[0].idx()];
			vinds[1] = vmapMesh2Strip[vvh[1].idx()];
			vinds[2] = vmapMesh2Strip[vvh[2].idx()];

			NodalSolidElement* pEle = new CSTElementStVKOrthotropic(3);
			pEle->setNodeIndices0(vinds);
			pEle->setNodeIndicesx(vinds);
			pEle->setMaterial(&this->m_matMembrane);

			this->m_vpMembraneEles[i].push_back(pEle);
		}
	}

	this->updateMembranePositions_x(this->m_state_x->m_vx);
	this->updateMembranePositions_X(this->m_state_0->m_vx);

	logSimu("[TRACE] Initialized TensileCompactModel. Raw: %d, Sim: %d", this->m_nrawDOF, this->m_nsimDOF_x);
}

void TensileCompactModel::getParam_Stretch(VectorXd& vs) const
{
	vs.resize(1);

	vs(0) = this->m_vpMembraneEles[0][0]->getPreStrain();
}

void TensileCompactModel::setParam_Stretch(const VectorXd& vs)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert(vs.size() == 1);

	for (int i = 0; i < (int) this->m_vpMeshes.size(); ++i)
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->setPreStrain(vs(0)); // Set it!

	this->deprecateUndeformed();
}

void TensileCompactModel::update_Rest()
{
	RodMeshModel::update_Rest();

	int numM = (int) this->m_vpMeshes.size();

#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
	for (int i = 0; i < numM; ++i)
	{
#ifdef OMP_RESTINIT
#pragma omp parallel for
#endif
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->update_Rest(this->m_vXBStrip[i]);
	}
}

void TensileCompactModel::update_Energy()
{
	RodMeshModel::update_Energy();

	int numM = (int) this->m_vpMeshes.size();

	VectorXd vv;

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < numM; ++i)
	{
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->update_Energy(this->m_vxBStrip[i], vv);
	}
}

void TensileCompactModel::update_Force()
{
	RodMeshModel::update_Force();

	int numM = (int) this->m_vpMeshes.size();

	VectorXd vv;

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < numM; ++i)
	{
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->update_Force(this->m_vxBStrip[i], vv);
	}
}

void TensileCompactModel::update_Jacobian()
{
	RodMeshModel::update_Jacobian();

	int numM = (int) this->m_vpMeshes.size();

	VectorXd vv;

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < numM; ++i)
	{
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->update_Jacobian(this->m_vxBStrip[i], vv);
	}
}

void TensileCompactModel::update_EnergyForce()
{
	RodMeshModel::update_EnergyForce();

	int numM = (int) this->m_vpMeshes.size();

	VectorXd vv;

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < numM; ++i)
	{
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->update_EnergyForce(this->m_vxBStrip[i], vv);
	}
}

void TensileCompactModel::update_ForceJacobian()
{
	RodMeshModel::update_ForceJacobian();

	int numM = (int) this->m_vpMeshes.size();

	VectorXd vv;

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < numM; ++i)
	{
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->update_ForceJacobian(this->m_vxBStrip[i], vv);
	}
}

void TensileCompactModel::update_EnergyForceJacobian()
{
	RodMeshModel::update_EnergyForceJacobian();

	int numM = (int) this->m_vpMeshes.size();

	VectorXd vv;

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < numM; ++i)
	{
#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->update_EnergyForceJacobian(this->m_vxBStrip[i], vv);
	}
}

Real TensileCompactModel::getEnergy()
{
	Real energy = 0;

	energy = RodMeshModel::getEnergy();

	int numM = (int) this->m_vpMeshes.size();

	for (int i = 0; i < numM; ++i)
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			energy += this->m_vpMembraneEles[i][j]->getEnergyIntegral();

	return energy;
}

void TensileCompactModel::addForce(VectorXd& vf, const vector<bool>* pvFixed)
{
	RodMeshModel::addForce(vf);

	int numM = (int) this->m_vpMeshes.size();

	for (int i = 0; i < numM; ++i)
	{
		int numDOF = (int) this->m_vxBStrip[i].size();

		VectorXd vfMembrane(numDOF);
		vfMembrane.setZero();
		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->addForce(vfMembrane, NULL);

		CustomTimer multiTimer(1, "FOR_MULTI");

		multiTimer.initialize();
		multiTimer.restart();

		vf += this->m_mHBStrip[i].transpose()*vfMembrane;

		multiTimer.stopStoreLog();
	}
}

void TensileCompactModel::addJacobian(tVector& vJ, const vector<bool>* pvFixed)
{
	RodMeshModel::addJacobian(vJ);

	int numM = (int) this->m_vpMeshes.size();

	for (int i = 0; i < numM; ++i)
	{
		this->m_vJBStrip[i].clear();

		for (int j = 0; j < (int) this->m_vpMembraneEles[i].size(); ++j)
			this->m_vpMembraneEles[i][j]->addJacobian(m_vJBStrip[i], NULL);

		CustomTimer setupTimer(1, "JAC_SETUP");
		CustomTimer multiTimer(1, "JAC_MULTI");

		setupTimer.initialize();
		setupTimer.restart();

		int memNumDOF = (int) this->m_vxBStrip[i].size();
		SparseMatrixXd mJMembraneT(memNumDOF, memNumDOF);
		mJMembraneT.setFromTriplets(m_vJBStrip[i].begin(),
			m_vJBStrip[i].end());
		mJMembraneT.makeCompressed();
		SparseMatrixXd mJMembraneC = mJMembraneT.selfadjointView<Lower>();

		setupTimer.stopStoreLog();

		//MatrixXd mJMDense = mJMembraneC.toDense();

		multiTimer.initialize();
		multiTimer.restart();

		MatrixXd mtoAddDense = m_mHBStrip[i].transpose()*mJMembraneC*m_mHBStrip[i];

		multiTimer.stopStoreLog();

		SparseMatrixXd mtoAdd = mtoAddDense.sparseView(1e-9);

		tVector vtoAdd;
		toTriplets(mtoAdd, vtoAdd);
		vJ.insert(vJ.end(), vtoAdd.begin(), vtoAdd.end());
	}
}


void TensileCompactModel::freeElements()
{
	RodMeshModel::freeElements();

	for (int i = 0; i < this->m_vpMembraneEles.size(); ++i)
		for (int j = 0; j < this->m_vpMembraneEles[i].size(); ++j)
			delete this->m_vpMembraneEles[i][j];
	this->m_vpMembraneEles.clear();
}

void TensileCompactModel::freeMembranes()
{
	int numM = (int) this->m_vpMeshes.size();

	for (int i = 0; i < numM; ++i)
	{
		delete this->m_vpMeshes[i];
		delete this->m_vpBoundIdx[i];
		delete this->m_vpBoundMap[i];
	}
	this->m_vpMeshes.clear();
	this->m_vpBoundIdx.clear();
	this->m_vpBoundMap.clear();
}

void TensileCompactModel::updateMembranePositions_x(const VectorXd& vx)
{
	for (int i = 0; i < (int) this->m_vpMeshes.size(); ++i)
		this->m_vxBStrip[i] = this->m_mHBStrip[i] * vx;
}

void TensileCompactModel::updateMembranePositions_X(const VectorXd& vX)
{
	for (int i = 0; i < (int) this->m_vpMeshes.size(); ++i)
		this->m_vXBStrip[i] = this->m_mHBStrip[i] * vX;
}

void TensileCompactModel::configureMembranes(
	const vector<TriMesh*>& vpMeshes,
	const vector<iVector*>& vpBoundIdx,
	const vector<iVector*>& vpBoundMap)
{
	assert(vpMeshes.size() == vpBoundIdx.size());
	assert(vpMeshes.size() == vpBoundMap.size());

	this->freeMembranes();

	int numM = (int)vpMeshes.size();

	this->m_vpMeshes.resize(numM);
	this->m_vpBoundIdx.resize(numM);
	this->m_vpBoundMap.resize(numM);
	for (int i = 0; i < numM; ++i)
	{
		assert(vpBoundIdx[i]->size() == vpBoundMap[i]->size());

		this->m_vpMeshes[i] = new TriMesh(*vpMeshes[i]);
		this->m_vpBoundIdx[i] = new iVector(*vpBoundIdx[i]);
		this->m_vpBoundMap[i] = new iVector(*vpBoundMap[i]);
	}

	this->deprecateConfiguration();
}

void TensileCompactModel::configureMaterialMembrane(const SolidMaterial& mat)
{
	this->m_matMembrane = mat;

	this->deprecateConfiguration();
}
