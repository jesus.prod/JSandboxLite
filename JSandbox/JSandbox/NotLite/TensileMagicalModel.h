/*=====================================================================================*/
/*!
\file		TensileMagicalModel.h
\author		jesusprod
\brief		Not enougth time to write a proper description.
*/
/*=====================================================================================*/

#ifndef TENSILE_MAGICAL_MODEL_H
#define TENSILE_MAGICAL_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/TriMesh.h>
#include <JSandbox/Geometry/RodMesh.h>

#include <JSandbox/Model/CurveMesh3DModel.h>

class JSANDBOX_EXPORT TensileMagicalModel : public CurveMesh3DModel
{

public:
	TensileMagicalModel();
	virtual ~TensileMagicalModel();

	/* From Solid Model */

	virtual void setup();

	virtual void update_Rest();
	virtual void update_Energy();
	virtual void update_Force();
	virtual void update_Jacobian();
	virtual void update_EnergyForce();
	virtual void update_ForceJacobian();
	virtual void update_EnergyForceJacobian();

	virtual Real getEnergy();
	virtual void addForce(VectorXd& vf, const vector<bool>* pvFixed = NULL);
	virtual void addJacobian(tVector& vJ, const vector<bool>* pvFixed = NULL);

	/* From Solid Model */

	virtual void configureMembranes(const vector<TriMesh*>& vpMeshes, 
									const vector<iVector*>& vpBoundIdx, 
									const vector<iVector*>& vpBoundMap);

	virtual void configureMaterialMembrane(const SolidMaterial& mat);

	virtual void setParam_Stretch(const VectorXd& vs);
	virtual void getParam_Stretch(VectorXd& vs) const;

protected:

	virtual void updateMembranePositions_x(const VectorXd& vx);
	virtual void updateMembranePositions_X(const VectorXd& vX);

	/* From Solid Model */

	virtual void freeElements();

	/* From Solid Model */

	virtual void freeMembranes();

protected:

	vector<TriMesh*> m_vpMeshes;
	vector<iVector*> m_vpBoundIdx;
	vector<iVector*> m_vpBoundMap;

	vector<tVector> m_vJBStrip;
	vector<VectorXd> m_vxBStrip;
	vector<VectorXd> m_vXBStrip;
	vector<SparseMatrixXd> m_mHBStrip;

	SolidMaterial m_matMembrane;

	vector<vector<SolidElement*>> m_vpMembraneEles;

};

#endif