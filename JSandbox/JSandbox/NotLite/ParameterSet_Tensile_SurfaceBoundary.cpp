/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_SurfaceBoundary.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_SurfaceBoundary.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_SurfaceBoundary.h>

ParameterSet_Tensile_SurfaceBoundary::ParameterSet_Tensile_SurfaceBoundary() : ParameterSet("TENSURFBOUNDARY")
{
	// Nothing to do here
}

ParameterSet_Tensile_SurfaceBoundary::~ParameterSet_Tensile_SurfaceBoundary()
{
	// Nothing to do here
}

void ParameterSet_Tensile_SurfaceBoundary::getParameters(VectorXd& vp) const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	vp.resize(this->m_np);

	int count = 0;
	for (int i = 0; i < numSurfaces; ++i)
	{
		SurfaceModel* pSurfModel = this->m_pTSModel->getSurfaceModel(i);

		VectorXd vb0_i;
		pSurfModel->getParam_BoundaryRest(vb0_i);
		for (int j = 0; j < (int)vb0_i.size(); ++j)
			vp(count++) = vb0_i(j); // Set subset
	}
}

void ParameterSet_Tensile_SurfaceBoundary::setParameters(const VectorXd& vp)
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	assert((int)vp.size() == this->m_np);

	int count = 0;
	for (int i = 0; i < numSurfaces; ++i)
	{
		SurfaceModel* pSurfModel = this->m_pTSModel->getSurfaceModel(i);
		int numB_i = (int)pSurfModel->getBoundaryNodes().size();
		int numD_i = pSurfModel->getNumSimDim_0();

		VectorXd vc0_i(numB_i*numD_i);
		for (int j = 0; j < numB_i; ++j)
			for (int d = 0; d < numD_i; ++d)
				vc0_i(j*numD_i + d) = vp(count++);

		pSurfModel->setParam_BoundaryRest(vc0_i);
	}
}

bool ParameterSet_Tensile_SurfaceBoundary::isReady_fp() const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	bool ready = true;
	for (int i = 0; i < numSurfaces; ++i)
		ready &= this->m_pTSModel->getSurfaceModel(i)->isReady_fb0();

	return ready;
}

bool ParameterSet_Tensile_SurfaceBoundary::isReady_DfDp() const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	bool ready = true;
	for (int i = 0; i < numSurfaces; ++i)
		ready &= this->m_pTSModel->getSurfaceModel(i)->isReady_DfxDb0();

	return ready;
}

void ParameterSet_Tensile_SurfaceBoundary::get_fp(VectorXd& vfp) const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	vfp.resize(this->m_np);

	int count = 0;
	for (int i = 0; i < numSurfaces; ++i)
	{
		SurfaceModel* pSurfModel = this->m_pTSModel->getSurfaceModel(i);

		VectorXd vfp_i;
		pSurfModel->add_fb0(vfp_i);
		for (int j = 0; j < (int)vfp_i.size(); ++j)
			vfp(count++) = vfp_i(j); // Set subset
	}
}

void ParameterSet_Tensile_SurfaceBoundary::get_DfDp(tVector& vDFDp) const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	int coefficientSum = 0;
	for (int i = 0; i < numSurfaces; ++i)
		coefficientSum += this->m_pTSModel->getSurfaceModel(i)->getNumNonZeros_DfxDb0();

	vDFDp.reserve(coefficientSum);

	int boundaryCount = 0;
	for (int i = 0; i < numSurfaces; ++i)
	{
		SurfaceModel* pSurfModel = this->m_pTSModel->getSurfaceModel(i);
		int numB_i = (int) pSurfModel->getBoundaryNodes().size();
		int numD_i = pSurfModel->getNumSimDim_0();

		// Local Jacob.
		tVector vDFDpi;

		vDFDpi.reserve(pSurfModel->getNumNonZeros_DfxDb0());
		pSurfModel->add_DfxDb0(vDFDpi); // Forces Jacobian

		int ncoeff = (int) vDFDpi.size();
		for (int j = 0; j < ncoeff; ++j) // Concatenate this coefficients with the rest of the surfaces
			vDFDpi[j] = Triplet<Real>(vDFDpi[j].row(), vDFDpi[j].col() + boundaryCount, vDFDpi[j].value());

		boundaryCount += numB_i*numD_i;

		this->m_pTSModel->addLocal2Global_Row(i, vDFDpi, vDFDp);
	}
}

void ParameterSet_Tensile_SurfaceBoundary::setup()
{
	assert(this->m_pModel != NULL);

	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();
	this->m_np = 0;

	for (int i = 0; i < numSurfaces; ++i)
	{
		VectorXd vc0_i;
		this->m_pTSModel->getSurfaceModel(i)->getParam_BoundaryRest(vc0_i);
		this->m_np += (int)vc0_i.size(); // Add to surface coupling DOF
	}

	this->updateBoundsVector();
}