/*=====================================================================================*/
/*!
\file		SurfaceRemesherCutter.h
\author		jesusprod
\brief		Implementation of a surface remesher that cuts a surface.

*/
/*=====================================================================================*/


#ifndef SURFACE_REMESHER_CUT_NEW_H
#define SURFACE_REMESHER_CUT_NEW_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/Geometry/SurfaceRemesherNew.h>

class JSANDBOX_EXPORT SurfaceRemesherCutterNew : public SurfaceRemesherNew
{
public:

	SurfaceRemesherCutterNew();
	virtual ~SurfaceRemesherCutterNew();

	virtual bool remesh(TriMesh* pMesh, const BVHTree* pEdgeTree = NULL, const BVHTree* pFaceTree = NULL);

	virtual void setCutEdges(const iVector& ve) { this->m_vcutEdges = ve; }

	virtual const iVector& getCutEdges() const { return this->m_vcutEdges; }
	virtual const iVector& getCutVerts() const { return this->m_vcutVerts; }

	virtual const vector<map<TriMesh::VertexHandle, TriMesh::VertexHandle>>& getDuplicatesMap() const { return this->m_vdupsMap; }

protected:

	iVector m_vcutEdges;
	iVector m_vcutVerts;
	vector<map<TriMesh::VertexHandle, TriMesh::VertexHandle>> m_vdupsMap;


};

#endif