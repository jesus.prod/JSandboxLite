/*=====================================================================================*/
/*!
\file		CSTElementStVKTrueOthotropic.h
\author		jesusprod
\brief		Constant strain triangle element with orthotropic St. VK constitutive model.
*/
/*=====================================================================================*/

#ifndef CST_ELEMENT_ORTHO_STVK_O_H
#define CST_ELEMENT_ORTHO_STVK_O_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/CSTElement.h>

class CSTElementStVKTrueOrthotropic : public CSTElement
{
public:
	CSTElementStVKTrueOrthotropic(int dim0);
	virtual ~CSTElementStVKTrueOrthotropic();

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual void update_DfxDx(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DfxD0(const VectorXd& vx, const VectorXd& vX);

};

#endif