/*=====================================================================================*/
/*!
\file		CurveMesh2DModel.h
\author		jesusprod
\brief		Not enougth time to write a proper description.
*/
/*=====================================================================================*/

#ifndef CURVE_MESH_2D_MODEL_H
#define CURVE_MESH_2D_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/RodMesh.h>

#include <JSandbox/Model/NodalSolidModel.h>

class JSANDBOX_EXPORT CurveMesh2DModel : public NodalSolidModel
{

public:
	CurveMesh2DModel();
	virtual ~CurveMesh2DModel();

	/* From Solid Model */

	virtual void setup();

	virtual void update_SimDOF();

	/* From Solid Model */

	virtual void configureRestDimensionNumber(int dim);
	virtual void configureMesh(const RodMesh& mesh_0, const RodMesh& mesh_x);
	virtual void configureMaterials(const SolidMaterial& compMat, const SolidMaterial& exteMat);

	virtual RodMesh& getSimMesh_x() { if (!this->m_isReady_Mesh) this->update_SimMeshFromState(); return *(this->m_pSimMesh_x); }
	virtual RodMesh& getSimMesh_0() { if (!this->m_isReady_Mesh) this->update_SimMeshFromState(); return *(this->m_pSimMesh_0); }

	virtual const vector<SolidElement*>& getStretchElements() const { return this->m_vpStretchEles; }
	virtual const vector<SolidElement*>& getBendingElements() const { return this->m_vpBendingEles; }

	virtual void update_StateFromSimMesh();
	virtual void update_SimMeshFromState();
	
	virtual void getParam_LengthEdge(VectorXd& vl);
	virtual void setParam_LengthEdge(const VectorXd& vl);

protected:

	/* From Solid Model */

	virtual void freeElements();

	/* From Solid Model */

	virtual void freeMesh();
	virtual void initializeElements_Stretch();
	virtual void initializeElements_Bending();
	virtual void initializeMaterials();

protected:

	int m_dim0;

	RodMesh* m_pSimMesh_0;
	RodMesh* m_pSimMesh_x;

	vector<SolidElement*> m_vpStretchEles;
	vector<SolidElement*> m_vpBendingEles;

	SolidMaterial m_matComp;
	SolidMaterial m_matExte;

};

#endif