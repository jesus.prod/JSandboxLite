/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_SurfaceRest.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_SurfaceRest.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_SurfaceRest.h>

ParameterSet_Tensile_SurfaceRest::ParameterSet_Tensile_SurfaceRest() : ParameterSet("TENSURFACEREST")
{
	// Nothing to do here
}

ParameterSet_Tensile_SurfaceRest::~ParameterSet_Tensile_SurfaceRest()
{
	// Nothing to do here
}

void ParameterSet_Tensile_SurfaceRest::getParameters(VectorXd& vp) const
{
	//vp = this->m_pSurfModel->getPositions_0();

	assert((int)vp.size() == this->m_np);
}

void ParameterSet_Tensile_SurfaceRest::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	this->m_pTSModel->updateState0_Surface(0, vp);
}

bool ParameterSet_Tensile_SurfaceRest::isReady_DfDp() const
{
	return this->m_pSurfModel->isReady_DfxD0();
}

void ParameterSet_Tensile_SurfaceRest::get_DfDp(tVector& vDFDp) const
{
	//this->m_pTSModel->test_DfxDX_Surf();

	//this->m_pSurfModel->testDfxD0Local();
	//this->m_pSurfModel->testDfxD0Global();

	vDFDp.reserve(this->m_pTSModel->getNumNonZeros_DfxDX_Surface(0));
	this->m_pTSModel->add_DfxDX_Surface(0, vDFDp); // Forces Jacobian
}

void ParameterSet_Tensile_SurfaceRest::setup()
{
	assert(this->m_pModel != NULL);

	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);

	assert(this->m_pTSModel != NULL);

	this->m_pSurfModel = this->m_pTSModel->getSurfaceModel(0);

	this->m_np = this->m_pSurfModel->getNumSimDOF_0();

	this->updateBoundsVector();
}