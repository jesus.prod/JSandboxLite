/*=====================================================================================*/
/*!
\file		CurveMesh3DModel.h
\author		jesusprod
\brief		Not enougth time to write a proper description.
*/
/*=====================================================================================*/

#ifndef CURVE_MESH_3D_MODEL_H
#define CURVE_MESH_3D_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/RodMesh.h>

#include <JSandbox/Model/NodalSolidModel.h>

class JSANDBOX_EXPORT CurveMesh3DModel : public NodalSolidModel
{

public:
	CurveMesh3DModel();
	virtual ~CurveMesh3DModel();

	/* From Solid Model */

	virtual void setup();

	virtual void update_SimDOF();

	/* From Solid Model */

	virtual void configureMesh(const RodMesh& mesh_0, const RodMesh& mesh_x);
	virtual void configureMaterialStretch(const SolidMaterial& mat);
	virtual void configureMaterialBending(const SolidMaterial& mat);

	virtual RodMesh& getSimMesh_x() { if (!this->m_isReady_Mesh) this->update_SimMeshFromState(); return *(this->m_pSimMesh_x); }
	virtual RodMesh& getSimMesh_0() { if (!this->m_isReady_Mesh) this->update_SimMeshFromState(); return *(this->m_pSimMesh_0); }

	virtual const vector<SolidElement*>& getStretchElements() const { return this->m_vpStretchEles; }
	virtual const vector<SolidElement*>& getBendingElements() const { return this->m_vpBendingEles; }

	virtual void update_StateFromSimMesh();
	virtual void update_SimMeshFromState();

	virtual void getParam_LengthEdge(VectorXd& vl);
	virtual void setParam_LengthEdge(const VectorXd& vl);

protected:

	/* From Solid Model */

	virtual void freeElements();

	/* From Solid Model */

	virtual void freeMesh();
	virtual void initializeElements_Stretch();
	virtual void initializeElements_Bending();

protected:

	RodMesh* m_pSimMesh_0;
	RodMesh* m_pSimMesh_x;

	SolidMaterial m_matBending;
	SolidMaterial m_matStretch;

	vector<SolidElement*> m_vpStretchEles;
	vector<SolidElement*> m_vpBendingEles;

};

#endif