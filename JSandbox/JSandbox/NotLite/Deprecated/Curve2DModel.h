/*=====================================================================================*/
/*!
\file		Curve2DModel.h
\author		jesusprod
\brief		Custom made model for patch boundaries. Mainly a rod in 2D.
*/
/*=====================================================================================*/

#ifndef PATCH_BOUNDARY_MODEL_H
#define PATCH_BOUNDARY_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/NodalSolidModel.h>

class JSANDBOX_EXPORT Curve2DModel : public NodalSolidModel
{

public:
	Curve2DModel();
	virtual ~Curve2DModel();

	virtual void configureCurve(const dVector& vpos);

	virtual dVector* getRestAngles() const { return this->m_pvrestAng; }
	virtual void setRestAngles(dVector* pva) { this->m_pvrestAng = pva; }

	virtual dVector* getRestLengths() const { return this->m_pvrestLen; }
	virtual void setRestLengths(dVector* pvl) { this->m_pvrestLen = pvl; }

	virtual void updateRest();

protected:
	virtual void initializeElements();

protected:

	dVector m_vrawPos;
	dVector* m_pvrestAng;	// Rest angles
	dVector* m_pvrestLen;	// Rest lengths

};

#endif