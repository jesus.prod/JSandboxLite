/*=====================================================================================*/
/*! 
\file		DSEdgeElement.cpp
\author		jesusprod
\brief		Implementation of DSEdgeElement.h
*/
/*=====================================================================================*/

#include <JSandbox/DSEdgeElement.h>

DSEdgeElement::DSEdgeElement(int dim0) : NodalSolidElement(2, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);
}

DSEdgeElement::~DSEdgeElement() 
{
	// Nothing to do here
}

void DSEdgeElement::updateRest(const VectorXd& vX)
{
	if (this->m_numDim0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
		this->m_L0 = (x1 - x0).norm();
	}
	else if (this->m_numDim0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
		this->m_L0 = (x1 - x0).norm();
	}
	else assert(false);
}

void DSEdgeElement::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3+i]);
	}
	double kS = m_vpmat[0]->getStretchK();
	double L = this->m_L0*this->m_preStrain;;
	
//#include "../../Maple/Code/DSEdgeEnergy.mcg"

	//this->m_energy = t21;
}

void DSEdgeElement::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3+i]);
	}
	double kS = m_vpmat[0]->getStretchK();
	double L = this->m_L0*this->m_preStrain;

	double vf6[6];
	
//#include "../../Maple/Code/DSEdgeForce.mcg"

	for(int i = 0; i < 6; ++i)	
		if (!isfinite(vf6[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
	
	// Store force vector

	for(int i = 0; i < 6; ++i)	
		this->m_vfVal(i) = vf6[i];
}

void DSEdgeElement::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3+i]);
	}
	double kS = m_vpmat[0]->getStretchK();
	double L = this->m_L0*this->m_preStrain;

	double mJ[6][6];

//#include "../../Maple/Code/DSEdgeJacobian.mcg"

	for(int i = 0; i < 6; ++i)
		for (int j = 0; j < 6; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 6; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ[i][j];
}