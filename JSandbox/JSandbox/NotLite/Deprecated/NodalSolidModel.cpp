/*=====================================================================================*/
/*! 
\file		NodalSolidModel.cpp
\author		jesusprod
\brief		Implementation of NodalSolidElement.h
 */
/*=====================================================================================*/

#include <JSandbox/NodalSolidModel.h>

void NodalSolidModel::visualizeEnergy(dVector& vcol)
{
	if (!this->m_isEnergyReady)
		this->updateEnergy();

	dVector vnodeE(this->m_numNodeSim, 0.0);
	for (int i = 0; i < this->m_numEle; ++i)
	{
		NodalSolidElement* pEle = static_cast<NodalSolidElement*>(this->m_vpEles[i]);

		double ed =  pEle->getEnergyDensity(); // Split
		const iVector& vnodeIdx = pEle->getNodeIndices();
		for (int n = 0; n < pEle->getNumNodeSim(); ++n)
			vnodeE[vnodeIdx[n]] += ed;
	}

	double minE = HUGE_VAL; 
	double maxE = -HUGE_VAL;
	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		if (vnodeE[i] > maxE) maxE = vnodeE[i];
		if (vnodeE[i] < minE) minE = vnodeE[i];
	}
	double range = maxE - minE;

	vcol.resize(3*this->m_numNodeSim);
	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		dVector col = computeHeatColorRGB((vnodeE[i] - minE)/range);
		int offset = 3*i;
		for (int i = 0; i < 3; ++i)
			vcol[offset + i] = col[i];
	}
}

void NodalSolidModel::visualizeForce(dVector& vcol)
{
	if (!this->m_isForceReady)
		this->updateForce();

	dVector vnodeF(this->m_numNodeSim, 0.0);
	for (int i = 0; i < this->m_numEle; ++i)
	{
		NodalSolidElement* pEle = static_cast<NodalSolidElement*>(this->m_vpEles[i]);

		const VectorXd& vf =  pEle->getLocalForce();
		const iVector& vnodeIdx = pEle->getNodeIndices();
		for (int n = 0; n < pEle->getNumNodeSim(); ++n)
			vnodeF[vnodeIdx[n]] += getBlock3x1(n, vf).norm();
	}

	double minE = HUGE_VAL; 
	double maxE = -HUGE_VAL;
	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		if (vnodeF[i] > maxE) maxE = vnodeF[i];
		if (vnodeF[i] < minE) minE = vnodeF[i];
	}
	double range = maxE - minE;

	vcol.resize(3*this->m_numNodeSim);
	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		dVector col = computeHeatColorRGB((vnodeF[i] - minE)/range);
		int offset = 3*i;
		for (int i = 0; i < 3; ++i)
			vcol[offset + i] = col[i];
	}
}
