///*=====================================================================================*/
///*!
//\file		DSAreaSlideElement.cpp
//\author		jesusprod
//\brief		Implementation of DSAreaSlideElement.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/DSAreaSlideElement.h>
//
//DSAreaSlideElement::DSAreaSlideElement(int) : NodalSolidElement(3, 5)
//{
//	// Nothing to do here
//}
//
//DSAreaSlideElement::~DSAreaSlideElement()
//{
//	// Nothing to do here
//}
//
//void DSAreaSlideElement::updateEnergy(const VectorXd& vx, const VectorXd& vv)
//{
////	vector<double> x1(3);
////	vector<double> x2(3);
////	vector<double> x3(3);
////	vector<double> X1(2);
////	vector<double> X2(3);
////	vector<double> X3(3);
////	for (int i = 0; i < 3; ++i)
////	{
////		x1[i] = vx(this->m_vidx[i]);
////		x2[i] = vx(this->m_vidx[3 + i]);
////		x3[i] = vx(this->m_vidx[6 + i]);
////		X1[i] = vx(this->m_vidx[9 + i]);
////		X2[i] = vx(this->m_vidx[12 + i]);
////		X3[i] = vx(this->m_vidx[15 + i]);
////	}
////	double kA = m_vpmat[0]->getAreaK();
////
////	// Workaround, copy the UV coordinates
////	vector<double> u1(2), u2(2), u3(2);
////	for (int i = 0; i < 2; ++i)
////	{
////		u1[i] = X1[i];
////		u2[i] = X2[i];
////		u3[i] = X3[i];
////	}
////
////#include "../../Maple/Code/DSAreaSlideEnergy.mcg"
//
//	//this->m_energy = t144;
//}
//
//void DSAreaSlideElement::updateForce(const VectorXd& vx, const VectorXd& vv)
//{
////	vector<double> x1(3);
////	vector<double> x2(3);
////	vector<double> x3(3);
////	vector<double> X1(2);
////	vector<double> X2(3);
////	vector<double> X3(3);
////	for (int i = 0; i < 3; ++i)
////	{
////		x1[i] = vx(this->m_vidx[i]);
////		x2[i] = vx(this->m_vidx[3 + i]);
////		x3[i] = vx(this->m_vidx[6 + i]);
////		X1[i] = vx(this->m_vidx[9 + i]);
////		X2[i] = vx(this->m_vidx[12 + i]);
////		X3[i] = vx(this->m_vidx[15 + i]);
////	}
////	double kA = m_vpmat[0]->getAreaK();
////
////	// Workaround, copy the UV coordinates
////	vector<double> u1(2), u2(2), u3(2);
////	for (int i = 0; i < 2; ++i)
////	{
////		u1[i] = X1[i];
////		u2[i] = X2[i];
////		u3[i] = X3[i];
////	}
////	double vf15[9];
////
////#include "../../Maple/Code/DSAreaSlideForce.mcg"
//
//	//// Store force vector
//
//	//for (int i = 0; i < 9; ++i)
//	//	this->m_vfVal(i) = vf15[i];
//
//	//for (int i = 0; i < 3; ++i)
//	//{
//	//	this->m_vfVal(9 + 3*i + 0) = vf15[9 + 2*i + 0];
//	//	this->m_vfVal(9 + 3*i + 1) = vf15[9 + 2*i + 1];
//	//	this->m_vfVal(9 + 3*i + 2) = 0.0;
//	//}
//}
//
//void DSAreaSlideElement::updateJacobian(const VectorXd& vx, const VectorXd& vv)
//{
////	vector<double> x1(3);
////	vector<double> x2(3);
////	vector<double> x3(3);
////	vector<double> X1(2);
////	vector<double> X2(3);
////	vector<double> X3(3);
////	for (int i = 0; i < 3; ++i)
////	{
////		x1[i] = vx(this->m_vidx[i]);
////		x2[i] = vx(this->m_vidx[3 + i]);
////		x3[i] = vx(this->m_vidx[6 + i]);
////		X1[i] = vx(this->m_vidx[9 + i]);
////		X2[i] = vx(this->m_vidx[12 + i]);
////		X3[i] = vx(this->m_vidx[15 + i]);
////	}
////	double kA = m_vpmat[0]->getAreaK();
////
////	// Workaround, copy the UV coordinates
////	vector<double> u1(2), u2(2), u3(2);
////	for (int i = 0; i < 2; ++i)
////	{
////		u1[i] = X1[i];
////		u2[i] = X2[i];
////		u3[i] = X3[i];
////	}
////	double mJ[15][15];
////
////#include "../../Maple/Code/DSAreaSlideJacobian.mcg"
//
//	//// Store triangular lower matrix
//
//	//int count = 0;
//	//for (int i = 0; i < 9; ++i)
//	//	for (int j = 0; j <= i; ++j)
//	//		this->m_vJVal(count++) = mJ[i][j];
//}