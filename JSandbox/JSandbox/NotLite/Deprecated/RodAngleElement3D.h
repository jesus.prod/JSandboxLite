/*=====================================================================================*/
/*!
\file		RodAngleElement3D.h
\author		jesusprod
\brief		Rod angle preserving energy in 3D. It uses SolidMaterial bending K.
*/
/*=====================================================================================*/

#ifndef ROD_ANGLE_ELEMENT_3D_H
#define ROD_ANGLE_ELEMENT_3D_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>

class RodAngleElement3D : public NodalSolidElement
{
public:
	RodAngleElement3D(int dim0);
	~RodAngleElement3D();

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getVoronoiLength() const { return this->m_vL0; }
	virtual void setVoronoiLength(Real vl0) { this->m_vL0 = vl0; }

	virtual Real getIntegrationVolume() const { return this->m_vL0; }

protected:
	Real m_vL0; // Vertex lenght
	Real m_e0norm0;
	Real m_e1norm0;
};

#endif