/*=====================================================================================*/
/*! 
\file		FEMElementLinearCorot.cpp
\author		jesusprod
\brief		Implementation of FEMElementLinearCorot.h
 */
/*=====================================================================================*/

#include <JSandbox/FEMElementLinearCorot.h>

 

Real FEMElementLinearCorot::computeEnergyDensity(const MatrixXd& mF) const
{
	// Psi(F) = m*(F-R):(F-R) + 0.5*l*tr^2(R^T*F - I)
	// where R is the rotational part of F, in this
	// case implemented using orthogonalization

	MatrixXd mR; // Rotation
	orthogonalize_GS(mF, mR);

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	

	MatrixXd I = MatrixXd::Identity(this->m_numDim, this->m_numDim);
	
	MatrixXd mFR = (mF - mR);
	MatrixXd mRTF = (mR.transpose()*mF - I);
	double trFR = (mFR.transpose()*mFR).trace();
	double trRTF = mRTF.trace();

	return lame1*trFR + 0.5*lame2*trRTF*trRTF;
}

void FEMElementLinearCorot::computePiolaKirchhoffFirst(const MatrixXd& mF, MatrixXd& mP) const
{
	//// P(F) = 2*m*(F - R) + l*tr(R^T*F - I)*R

	MatrixXd mR; // Rotation
	orthogonalize_GS(mF, mR);

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	

	MatrixXd I = MatrixXd::Identity(this->m_numDim, this->m_numDim);

	mP = 2*lame1*(mF - mR) + lame2*(mR.transpose()*mF - I).trace()*mR;
}

void FEMElementLinearCorot::computePiolaKirchhoffFirstDerivative(const MatrixXd& mF, const vector<MatrixXd>& vDFDx, vector<MatrixXd>& vDPDx) const
{
	assert(mF.rows() == this->m_numDim);
	assert(mF.cols() == this->m_numDim);
	assert((int) vDFDx.size() == this->m_nrawDOF);

	MatrixXd I = MatrixXd::Identity(this->m_numDim, this->m_numDim);

	MatrixXd mR; // Rotation
	orthogonalize_GS(mF, mR);

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	
	
	vDPDx.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_nrawDOF; ++i) // Component derivatives: nv * nd
		vDPDx[i] = 2*lame1*vDFDx[i] + lame2*(mR.transpose()*vDFDx[i]).trace()*mR;
}