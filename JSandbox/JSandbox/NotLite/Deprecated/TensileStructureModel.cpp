/*=====================================================================================*/
/*!
\file		TensileStructureModel.cpp
\author		jesusprod
\brief		Implementation of TensileStructureModel.h
*/
/*=====================================================================================*/

#include <JSandbox/TensileStructureModel.h>
#include <JSandbox/DSEdgeElement.h>
#include <JSandbox/DSAreaElement.h>
#include <JSandbox/DSHingeElement.h>
#include <JSandbox/RodEdgeElement3D.h>
#include <JSandbox/RodAngleElement3D.h>
#include <JSandbox/EmbeddedRodBendElement3D.h>
#include <JSandbox/EmbeddedRodStretchElement3D.h>
#include <JSandbox/Geometry/SurfaceRemesherContour.h>
#include <JSandbox/Geometry/SurfaceRemesherCutter.h>
#include <JSandbox/Geometry/TriMeshParametrizer.h>

TensileStructureModel::TensileStructureModel()
{
	this->m_pRawMesh = NULL;
	this->m_pSimMesh = NULL;
}

TensileStructureModel::~TensileStructureModel()
{
	if (this->m_pRawMesh != NULL)
		delete this->m_pRawMesh;

	if (this->m_pSimMesh != NULL)
		delete this->m_pSimMesh;
}


void TensileStructureModel::configureSurface(const dVector& vpos, const iVector& vidx)
{
	vector<string> vSimProp;

	delete this->m_pRawMesh;
	this->m_pRawMesh = new TriMesh();
	vSimProp.push_back(VPropSim::POSE);
	vSimProp.push_back(VPropSim::VELE);
	vSimProp.push_back(VPropSim::POS0);
	this->m_pRawMesh->init(vpos, vidx, vSimProp);
	this->m_vrawPos = vpos;
	this->m_vrawIdx = vidx;
}

void TensileStructureModel::configureSeamsCurves(const vector<vector<Vector3d>>& vcurs)
{
	this->m_vrawSeamsCurs = vcurs;
}

void TensileStructureModel::configureStructCurves(const vector<vector<Vector3d>>& vcurs)
{
	this->m_vrawStructCurs = vcurs;
}

void TensileStructureModel::configureInternalRopes(const vector<pair<Vector3d, Vector3d>>& vpairs)
{
	this->m_vrawInternalRopes = vpairs;
}

void TensileStructureModel::configureExternalRopes(const vector<pair<Vector3d, Vector3d>>& vpairs)
{
	this->m_vrawExternalRopes = vpairs;
}

void TensileStructureModel::configureRopesCurves(const vector<pair<Vector3d, Vector3d>>& vpairs)
{
	this->m_vrawRopes = vpairs;
}

void TensileStructureModel::configureStructCurves(const vector<Curve>& vcurve, vector<dVector> vfix)
{
	assert((int)vcurve.size() == (int)vfix.size());

	this->m_vrawCoupledRods.clear();
	int nCurves = (int)vcurve.size();

	for (int iCurve = 0; iCurve < nCurves; ++iCurve)
	{
		CoupledRod rod; 
		rod.m_vfixAL = vfix[iCurve];
		rod.m_rawCurve = vcurve[iCurve];
		this->m_vrawCoupledRods.push_back(rod);
	}
}

void TensileStructureModel::preSetupActions()
{	
	delete this->m_pSimMesh; // Initialize trimesh
	this->m_pSimMesh = new TriMesh(*this->m_pRawMesh);

	this->m_vseamsLoops.clear();
	this->m_vseamsEdges.clear();
	this->m_vseamsVerts.clear();

	this->m_vstructLoops.clear();
	this->m_vstructEdges.clear();
	this->m_vstructVerts.clear();

	// No seam curves edges

	this->m_vseamsEdges.clear();
	this->m_vseamsVerts.clear();

	// Build vertices maps

	this->m_vmapPatches2Single.clear();
	this->m_vmapSingle2Patches.clear();
	this->m_mapPatchEdges.clear();
	 
	//initializeMeshSingle3D();
	//initializeMeshPatchesEmbedded3D();
	//initializeMeshPatchesEmbedded2D();
	//initializeMeshPatchesEmbedded3D_10();
}

void TensileStructureModel::initializeElements()
{
	//initializeElementsSingle3D();
	//initializeElementsPatchesEmbedded3D();
	//initializeElementsPatchesEmbedded2D();
	//initializeElementsPatchesEmbedded3D_10();
}

void TensileStructureModel::updateSimMesh()
{
	this->updateMeshState();
	//updateSimMeshSingle3D();
	//updateSimMeshPatchesEmbedded3D();
	//updateSimMeshPatchesEmbedded2D();
	//updateSimMeshPatchesEmbedded3D_10();
}

void TensileStructureModel::configureMaterial(SolidMaterial* pMatSurf, SolidMaterial* pMatStruct, SolidMaterial* pMatRopes)
{
	this->m_pStructMaterial = pMatStruct;
	this->m_pRopesMaterial = pMatRopes;
	this->m_pSurfMaterial = pMatSurf;
}

void TensileStructureModel::testForceLocal()
{
	//for (int i = 0; i < (int) this->m_vpDSStretchEles.size(); ++i)
	//{
	//	Real error = this->m_vpDSStretchEles[i]->testForceLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Stretch Ele. Force test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpDSAreaEles.size(); ++i)
	//{
	//	Real error = this->m_vpDSAreaEles[i]->testForceLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Area Ele. Force test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpDSEdgeEles.size(); ++i)
	//{
	//	Real error = this->m_vpDSEdgeEles[i]->testForceLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Edge Ele. Force test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpDSBendingEles.size(); ++i)
	//{
	//	Real error = this->m_vpDSBendingEles[i]->testForceLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Hinge Ele. Force test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpRodStretchEles.size(); ++i)
	//{
	//	Real error = this->m_vpRodStretchEles[i]->testForceLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Rod Stretch Ele. Force test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpRodBendingEles.size(); ++i)
	//{
	//	Real error = this->m_vpRodBendingEles[i]->testForceLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Rod Bend Ele. Force test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpRodTwistEles.size(); ++i)
	//{
	//	Real error = this->m_vpRodTwistEles[i]->testForceLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Rod Twist Ele. Force test error: %.9f", error);
	//}
}

void TensileStructureModel::testJacobianLocal()
{
	//for (int i = 0; i < (int) this->m_vpDSStretchEles.size(); ++i)
	//{
	//	Real error = this->m_vpDSStretchEles[i]->testJacobianLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Stretch Ele. Jacobian test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpDSAreaEles.size(); ++i)
	//{
	//	Real error = this->m_vpDSAreaEles[i]->testJacobianLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Area Ele. Jacobian test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpDSEdgeEles.size(); ++i)
	//{
	//	Real error = this->m_vpDSEdgeEles[i]->testJacobianLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Edge Ele. Jacobian test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpDSBendingEles.size(); ++i)
	//{
	//	Real error = this->m_vpDSBendingEles[i]->testJacobianLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Hinge Ele. Jacobian test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpRodStretchEles.size(); ++i)
	//{
	//	Real error = this->m_vpRodStretchEles[i]->testJacobianLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Rod Stretch Ele. Jacobian test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpRodBendingEles.size(); ++i)
	//{
	//	Real error = this->m_vpRodBendingEles[i]->testJacobianLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Rod Bend Ele. Jacobian test error: %.9f", error);
	//}

	//for (int i = 0; i < (int) this->m_vpRodTwistEles.size(); ++i)
	//{
	//	Real error = this->m_vpRodTwistEles[i]->testJacobianLocal(this->m_vx, this->m_vv);
	//	if (error > 1e-6) qDebug("[ERROR] Rod Twist Ele. Jacobian test error: %.9f", error);
	//}
}

void TensileStructureModel::setup()
{
	delete this->m_pSimMesh; // Initialize trimesh
	this->m_pSimMesh = new TriMesh(*this->m_pRawMesh);

	this->m_vseamsLoops.clear();
	this->m_vseamsEdges.clear();
	this->m_vseamsVerts.clear();

	initializeMeshStructure();
	initializeMeshElements();
	initializeMeshMaterials();
	initializeMeshDOF();

	this->m_vpRodModels.clear(); // Rebuild models
	int nRod = (int) this->m_vrawCoupledRods.size();
	for (int iRod = 0; iRod < nRod; ++iRod)
	{
		DERSlideModel* pRod = new DERSlideModel();

		const RodNodeList& rawNodeList = m_vrawCoupledRods[iRod].m_nodeList;
		bool isClosed = m_vrawCoupledRods[iRod].m_rawCurve.getIsClosed();
		
		pRod->configureNodeList(rawNodeList, isClosed);
		pRod->configureMaterial(*m_pStructMaterial, true);
		
		pRod->setup();

		this->m_vpRodModels.push_back(pRod);
	}

	this->updateSimulationDOF();

	this->m_vx.resize(this->m_nrawDOF);
	this->m_vv.resize(this->m_nrawDOF);

	// Update mesh state

	int numDOFMesh = this->m_numDOFMesh;

	for (int iDOF = 0; iDOF < numDOFMesh; iDOF++)
	{
		this->m_vx(this->m_vlocToGlo[0][iDOF]) = this->m_vxMesh(iDOF);
		this->m_vv(this->m_vlocToGlo[0][iDOF]) = this->m_vvMesh(iDOF);
	}

	for (int iRod = 0; iRod < nRod; iRod++)
	{
		int numDOFRod = this->m_vpRodModels[iRod]->getRawDOFNumber();
		VectorXd vxRod = this->m_vpRodModels[iRod]->getPositions();
		VectorXd vvRod = this->m_vpRodModels[iRod]->getVelocities();;
		for (int iDOF = 0; iDOF < numDOFRod; iDOF++)
		{
			this->m_vx(this->m_vlocToGlo[iRod + 1][iDOF]) = vxRod(iDOF);
			this->m_vv(this->m_vlocToGlo[iRod + 1][iDOF]) = vvRod(iDOF);
		}
	}

	this->m_vX = this->m_vx;

	this->updateRest();
	this->updateMass();

	this->m_isRestReady = true;
	this->m_isMeshReady = false;
	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

void TensileStructureModel::pushState()
{
	SolidModel::pushState();

	this->m_vxMeshStack.push_back(this->m_vxMesh);
	this->m_vvMeshStack.push_back(this->m_vvMesh);

	if ((int) this->m_vxMeshStack.size() > this->m_maxStackSize)
	{
		this->m_vxMeshStack.erase(this->m_vxMeshStack.begin());
		this->m_vvMeshStack.erase(this->m_vvMeshStack.begin());
	}

	int nRod = (int) m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; ++iRod)
		this->m_vpRodModels[iRod]->pushState();
}

void TensileStructureModel::popState()
{
	SolidModel::popState();

	this->m_vxMesh = this->m_vxMeshStack.back();
	this->m_vvMesh = this->m_vvMeshStack.back();
	this->m_vxMeshStack.pop_back();
	this->m_vvMeshStack.pop_back();

	int nRod = (int) m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; ++iRod)
		this->m_vpRodModels[iRod]->popState();

	this->updateSimulationDOF();

	this->updateMass();

	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

void TensileStructureModel::peekState(int i)
{
	SolidModel::peekState(i);

	int size = (int) this->m_vxMeshStack.size();
	if (i > size - 1)
	{
		qDebug("[WARN] In %s: acceding to unavailable stack state: %d\n", i);
		logSimu("[WARN] In %s: acceding to unavailable stack state: %d\n", i);

		this->m_vxMesh = this->m_vxMeshStack.front();
		this->m_vvMesh = this->m_vvMeshStack.front();
	}
	else
	{
		this->m_vxMesh = this->m_vxMeshStack[size - 1 - i];
		this->m_vvMesh = this->m_vvMeshStack[size - 1 - i];
	}

	int nRod = (int) m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; ++iRod)
		this->m_vpRodModels[iRod]->peekState(i);

	this->updateSimulationDOF();

	this->updateMass();

	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

void TensileStructureModel::updateMeshState()
{
	if (this->m_isMeshReady)
		return; // Updated

	this->m_pSimMesh->setPoints(toSTL(this->m_vXMesh), VPropSim::POS0);
	this->m_pSimMesh->setPoints(toSTL(this->m_vxMesh), VPropSim::POSE);
	this->m_pSimMesh->setPoints(toSTL(this->m_vvMesh), VPropSim::VELE);

	this->m_isMeshReady = true;
}

void TensileStructureModel::initializeMeshStructure()
{
	// Remesh for struct ----------------------------------------------

	double structTol = 0.0;
	for (TriMesh::EdgeIter e_it = this->m_pSimMesh->edges_begin(); e_it != this->m_pSimMesh->edges_end(); ++e_it)
		structTol += this->m_pSimMesh->calc_edge_length(*e_it); // Estimate length of structure edges for resample
	structTol /= this->m_pSimMesh->n_edges();

	int nStrCur = (int) this->m_vrawCoupledRods.size();
	for (int iStrCur = 0; iStrCur < nStrCur; iStrCur++)
	{
		Curve curve = this->m_vrawCoupledRods[iStrCur].m_rawCurve;
		dVector vfixAL = this->m_vrawCoupledRods[iStrCur].m_vfixAL;

		curve.resample(4); // Resample structure curve

		vector<RodNode> vnodes;
		int nVertices = curve.getNodeNumber();
		dVector vverAL = curve.getVertexParameters();
		for (int i = 0; i < nVertices; ++i) // Regular
		{
			vnodes.push_back(RodNode());
			vnodes.back().m_posAL = vverAL[i];
			vnodes.back().m_vx = curve.getPosition(i);
			vnodes.back().m_isSliding = false; // Fixed
		}

		SurfaceRemesherContour structContourRemesher;

		assert((int) vfixAL.size() % 2 == 0);
		int nInt = (int) vfixAL.size() / 2;
		double length = curve.getLength();

		for (int iInt = 0; iInt < nInt; ++iInt)
		{
			Real t0 = vfixAL[2*iInt + 0];
			Real t1 = vfixAL[2*iInt + 1];

			vector<Vector3d> vcurNodesRaw;
			vector<Vector3d> vcurNodesRes;
			if (isApprox(t0, t1, 1e-6))
			{
				int vi, ei;
				vcurNodesRaw.push_back(curve.samplePosition(t0*length, vi, ei));
			}
			else
			{
				int vi, ei;
				vcurNodesRaw.push_back(curve.samplePosition(t0*length, vi, ei));
				vcurNodesRaw.push_back(curve.samplePosition(t1*length, vi, ei));
			}

			hermiteInterpolation(vcurNodesRaw, vcurNodesRes, false, 0.5*structTol);
			//structContourRemesher.addContourCurve(vcurNodesRes); // Add to remesher
		}

		RemeshOP structContourOP = structContourRemesher.remesh(this->m_pSimMesh);
		if (!structContourOP.m_OK)
			return;

		delete this->m_pSimMesh; // Exchange mesh
		this->m_pSimMesh = structContourOP.m_pMesh;

		int nAddedV = (int) structContourOP.m_vAddVert.size();
		for (int iAddedV = 0; iAddedV < nAddedV; ++iAddedV)
		{
			MeshTraits::ID vid = structContourOP.m_vAddVert[iAddedV]; // Global ID
			Vector3d point = m_pSimMesh->getPoint(m_pSimMesh->getVertHandle(vid));

			//Real t;
			//Real D;
			//Vector3d ppoint;
			//if (!curve.projectPoint(point, ppoint, t))
			//{
			//	qDebug("[ERROR] Unexpected error");
			//	exit(-1); // Critical error: out
			//}

			//vnodes.push_back(RodNode());
			//vnodes.back().m_posAL = t;
			//vnodes.back().m_vx = ppoint;
			//vnodes.back().m_isSliding = true;
		}

		RodNodeList::SortNodeList_AL(vnodes); // Sort arclenght
		m_vrawCoupledRods[iStrCur].m_nodeList.setNodes(vnodes);	
	}

	// Initialize couplings

	this->m_vcouplings.clear();

	int nRawRod = (int) this->m_vrawCoupledRods.size();
	for (int iRawRod = 0; iRawRod < nRawRod; ++iRawRod)
	{
		RodNodeList& nodeList = m_vrawCoupledRods[iRawRod].m_nodeList;

		int nNode = nodeList.getNumNodes();
		vector<RodNode>& vnodes = nodeList.getNodes();
		for (int iNode = 0; iNode < nNode; ++iNode)
		{
			RodNode& rn = vnodes[iNode];
			if (!rn.m_isSliding)
				continue;

			double D;
			TriMesh::VertexHandle vh = this->m_pSimMesh->getClosestVertex(rn.m_vx, D);
			for (int ii = 0; ii < 3; ++ii)
			{
				DOFCoupling dc;
				dc.m_model0 = 0;
				dc.m_idx0 = 3*vh.idx() + ii;
				dc.m_model1 = iRawRod + 1;
				dc.m_idx1 = 3*iNode + ii;

				this->m_vcouplings.push_back(dc);
			}
		}
	}
}

void TensileStructureModel::initializeMeshElements()
{
	this->freeElements();

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());

	// Initialize area preserving elements

	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		CSTElement* pEle = new CSTElement(3);

		// Set indices
		vector<int> vinds0(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds0[v] = (*fv_it).idx();
			++fv_it;
		}
		pEle->setNodeIndices(vinds0);
		pEle->setNodeIndices0(vinds0);
		

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSStretchEles.push_back(pEle);
	}

	// Initialize discrete-shells bending elements 

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		TriMesh::EdgeHandle eh = *e_it;

		if (this->m_pSimMesh->is_boundary(eh))
			continue; // Ignore boundaries

		DSHingeElement* pEle = new DSHingeElement(3);

		// Set indices
		vector<int> vinds0(4);
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);

		for (int i = 0; i < 4; i++)
			vinds0[i] = m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx();
		pEle->setNodeIndices(vinds0);
		pEle->setNodeIndices0(vinds0);
		
		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSBendingEles.push_back(pEle);
	}

	this->m_numEle = (int) this->m_vpEles.size();
}

void TensileStructureModel::initializeMeshMaterials()
{
	for (int i = 0; i < (int) this->m_vpDSStretchEles.size(); ++i)
		this->m_vpDSStretchEles[i]->setMaterial(this->m_pSurfMaterial);

	for (int i = 0; i < (int) this->m_vpDSBendingEles.size(); ++i)
		this->m_vpDSBendingEles[i]->setMaterial(this->m_pSurfMaterial);
}

void TensileStructureModel::initializeMeshDOF()
{
	this->m_numDOFMesh = (int) this->m_pSimMesh->n_vertices();

	this->m_numDOFMesh *= 3;
	dVector vx(m_numDOFMesh);

	m_pSimMesh->getPoints(vx);
	this->m_vxMesh = toEigen(vx);
	this->m_vXMesh = toEigen(vx);
	this->m_vvMesh = toEigen(vx);
	this->m_vvMesh.setZero();
}

void TensileStructureModel::updateSimulationDOF()
{
	int nRod = (int) this->m_vpRodModels.size();

	// Update local-to-global

	this->m_vlocToGlo.resize(nRod + 1);

	int gloIdx = 0;

	// Surface model

	this->m_vlocToGlo[0].clear();
	this->m_vlocToGlo[0].resize(this->m_numDOFMesh);
	for (int iDOF = 0; iDOF < this->m_numDOFMesh; ++iDOF)
		this->m_vlocToGlo[0][iDOF] = gloIdx++; // Append

	// Rod models

	for (int iRod = 0; iRod < nRod; ++iRod)
	{
		iVector& locToGlo = this->m_vlocToGlo[iRod + 1];

		vector<DOFCoupling> vcouplings = this->getModelCouplings(iRod + 1);
		const iVector& vDOF = this->m_vpRodModels[iRod]->getSimDOF();
		int nDOF = this->m_vpRodModels[iRod]->getRawDOFNumber();
		locToGlo.clear();
		locToGlo.resize(nDOF, -1);
		
		// Copy coupled degrees-of-freedom
		int nCoup = (int) vcouplings.size();
		for (int iCoup = 0; iCoup < nCoup; ++iCoup)
		{
			DOFCoupling& coup = vcouplings[iCoup];
			if (coup.m_model0 == iRod + 1 && coup.m_model1 < coup.m_model0) // Model 0
				locToGlo[vDOF[coup.m_idx0]] = m_vlocToGlo[coup.m_model1][coup.m_idx1];
			else if (coup.m_model1 == iRod + 1 && coup.m_model0 < coup.m_model1) // Model 1
				locToGlo[vDOF[coup.m_idx1]] = m_vlocToGlo[coup.m_model0][coup.m_idx0];
		}

		// Initialize remaining degrees-of-freedom
		for (int iDOF = 0; iDOF < nDOF; ++iDOF)
		{
			if (locToGlo[iDOF] != -1)
				continue; // Already 

			locToGlo[iDOF] = gloIdx++;
		}
	}

	this->m_nrawDOF = gloIdx; // Total DOF

	this->m_as.resize(this->m_nrawDOF, true);

	for (int iRod = 0; iRod < nRod; ++iRod)
	{
		const iVector& locToGlo = this->m_vlocToGlo[iRod + 1];

		int nDOF = this->m_vpRodModels[iRod]->getRawDOFNumber();
		const iVector& vDOF = this->m_vpRodModels[iRod]->getSimDOF();
		const bVector& vas = this->m_vpRodModels[iRod]->getActiveStencil();

		for (int iDOF = 0; iDOF < nDOF; ++iDOF) // Init.
			this->m_as[locToGlo[vDOF[iDOF]]] = vas[iDOF];
	}
}

vector<DOFCoupling> TensileStructureModel::getModelCouplings(int i)
{
	vector<DOFCoupling> vout;

	int nCoup = (int) this->m_vcouplings.size();
	for (int iCoup = 0; iCoup < nCoup; ++iCoup)
	{
		DOFCoupling& coup = this->m_vcouplings[iCoup];
		if (coup.m_model0 == i || coup.m_model1 == i)
			vout.push_back(coup);
	}

	return vout;
}

vector<DOFCoupling> TensileStructureModel::getModelModelCouplings(int i, int j)
{
	vector<DOFCoupling> vout;

	int nCoup = (int) this->m_vcouplings.size();
	for (int iCoup = 0; iCoup < nCoup; ++iCoup)
	{
		DOFCoupling& coup = this->m_vcouplings[iCoup];
		if ((coup.m_model0 == i && coup.m_model1 == j) ||
			(coup.m_model0 == j && coup.m_model1 == i))
			vout.push_back(coup);
	}

	return vout;
}

void TensileStructureModel::posSetupActions()
{
	this->m_isMeshReady = false;
}

void TensileStructureModel::initializeMaterials()
{
	//for (int i = 0; i < (int) this->m_vpDSAreaEles.size(); ++i)
	//	this->m_vpDSAreaEles[i]->setMaterial(this->m_pSurfMaterial);

	//for (int i = 0; i < (int) this->m_vpDSEdgeEles.size(); ++i)
	//	this->m_vpDSEdgeEles[i]->setMaterial(this->m_pSurfMaterial);

	for (int i = 0; i < (int) this->m_vpDSStretchEles.size(); ++i)
		this->m_vpDSStretchEles[i]->setMaterial(this->m_pSurfMaterial);

	for (int i = 0; i < (int) this->m_vpDSBendingEles.size(); ++i)
		this->m_vpDSBendingEles[i]->setMaterial(this->m_pSurfMaterial);

	for (int i = 0; i < (int) this->m_vpRodStretchEles.size(); ++i)
		this->m_vpRodStretchEles[i]->setMaterial(this->m_pStructMaterial);

	for (int i = 0; i < (int) this->m_vpRodBendingEles.size(); ++i)
		this->m_vpRodBendingEles[i]->setMaterial(this->m_pStructMaterial);

	for (int i = 0; i < (int) this->m_vpRopeStretchEles.size(); ++i)
		this->m_vpRopeStretchEles[i]->setMaterial(this->m_pRopesMaterial);

	for (int i = 0; i < (int) this->m_vpRopeHooks.size(); ++i)
		this->m_vpRopeHooks[i]->setMaterial(this->m_pRopesMaterial);

	for (int i = 0; i < (int) this->m_vpRopeDoubleHooks.size(); ++i)
		this->m_vpRopeDoubleHooks[i]->setMaterial(this->m_pRopesMaterial);

	//for (int i = 0; i < (int) this->m_vpRodBendingEles.size(); ++i)
	//	for (int j = 0; j < (int) this->m_vpRodBendingEles[i].size(); ++j)
	//		this->m_vpRodBendingEles[i][j]->setMaterial(this->m_pStructMaterial);

	//for (int i = 0; i < (int) this->m_vpRodTwistEles.size(); ++i)
	//	for (int j = 0; j < (int) this->m_vpRodTwistEles[i].size(); ++j)
	//		this->m_vpRodTwistEles[i][j]->setMaterial(this->m_pStructMaterial);
}

void TensileStructureModel::updateStatex(const VectorXd& vx, const VectorXd& vv)
{
	int nRod = (int) this->m_vpRodModels.size();

	this->m_vx = vx;
	this->m_vv = vv;

	// Set mesh state

	int numDOFMesh = m_numDOFMesh;
	this->m_vxMesh.resize(numDOFMesh);
	this->m_vvMesh.resize(numDOFMesh);
	for (int iDOF = 0; iDOF < numDOFMesh; iDOF++)
	{
		this->m_vxMesh(iDOF) = this->m_vx(this->m_vlocToGlo[0][iDOF]);
		this->m_vvMesh(iDOF) = this->m_vv(this->m_vlocToGlo[0][iDOF]);
	}

	// Set rods state

	for (int iRod = 0; iRod < nRod; iRod++)
	{
		int numDOFRod = this->m_vpRodModels[iRod]->getRawDOFNumber(); 
		VectorXd vxRod(numDOFRod);
		VectorXd vvRod(numDOFRod);
		for (int iDOF = 0; iDOF < numDOFRod; iDOF++)
		{
			vxRod(iDOF) = this->m_vx(this->m_vlocToGlo[iRod + 1][iDOF]);
			vvRod(iDOF) = this->m_vv(this->m_vlocToGlo[iRod + 1][iDOF]);
		}

		this->m_vpRodModels[iRod]->updateStatex(vxRod, vvRod);
	}

	// Update simulation DOF

	this->updateSimulationDOF();

	this->m_vx.resize(this->m_nrawDOF);
	this->m_vv.resize(this->m_nrawDOF);

	// Update mesh state

	for (int iDOF = 0; iDOF < numDOFMesh; iDOF++)
	{
		this->m_vx(this->m_vlocToGlo[0][iDOF]) = this->m_vxMesh(iDOF);
		this->m_vv(this->m_vlocToGlo[0][iDOF]) = this->m_vvMesh(iDOF);
	}

	for (int iRod = 0; iRod < nRod; iRod++)
	{
		int numDOFRod = this->m_vpRodModels[iRod]->getRawDOFNumber();
		VectorXd vxRod = this->m_vpRodModels[iRod]->getPositions();
		VectorXd vvRod = this->m_vpRodModels[iRod]->getVelocities();;
		for (int iDOF = 0; iDOF < numDOFRod; iDOF++)
		{
			this->m_vx(this->m_vlocToGlo[iRod + 1][iDOF]) = vxRod(iDOF);
			this->m_vv(this->m_vlocToGlo[iRod + 1][iDOF]) = vvRod(iDOF);
		}
	}

	this->updateMass();

	// Update rods state

	this->m_isMeshReady = false;
	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

void TensileStructureModel::updateMass()
{
	this->m_vm.resize(this->m_nrawDOF);
	this->m_vm.setZero(); // Set zero

	this->m_gs.resize(this->m_nrawDOF, true);

	// Mesh mass

	int numDOFMesh = this->m_numDOFMesh;

	VectorXd vmMesh(numDOFMesh);
	vmMesh.setZero(); // Set zero
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->addMass(vmMesh);

	for (int iDOF = 0; iDOF < numDOFMesh; iDOF++) // Global
		m_vm(this->m_vlocToGlo[0][iDOF]) += vmMesh(iDOF);

	// Rods mass

	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
	{
		int numDOFRod = m_vpRodModels[iRod]->getRawDOFNumber();

		VectorXd vmRod = m_vpRodModels[iRod]->getLumpedMassVector();

		for (int iDOF = 0; iDOF < numDOFRod; iDOF++) // Global
			m_vm(this->m_vlocToGlo[iRod + 1][iDOF]) += vmRod(iDOF);
	}
	
	for (int iDOF = 0; iDOF < numDOFMesh; iDOF++)
		m_gs[this->m_vlocToGlo[0][iDOF]] = true;

	for (int iRod = 0; iRod < nRod; iRod++)
	{
		int numDOFRod = m_vpRodModels[iRod]->getRawDOFNumber();
		bVector gsRod = m_vpRodModels[iRod]->getGravityStencil();
		for (int iDOF = 0; iDOF < numDOFRod; iDOF++) // Global
			m_gs[this->m_vlocToGlo[iRod + 1][iDOF]] = gsRod[iDOF];
	}

	//this->m_mm.clear();

	//int numDOFMesh = this->m_numDOFMesh;

	//VectorXd vmMesh(numDOFMesh);
	//vmMesh.setZero(); // Set zero
	//for (int i = 0; i < this->m_numEle; ++i)
	//	this->m_vpEles[i]->addMass(vmMesh);

	//for (int iDOF = 0; iDOF < numDOFMesh; iDOF++) // Add lumped mass
	//	this->m_mm.push_back(Triplet<Real>(iDOF, iDOF, vmMesh[iDOF]));

	//int nRod = (int)m_vpRodModels.size();
	//for (int iRod = 0; iRod < nRod; iRod++)
	//{
	//	const tVector& vMRod = m_vpRodModels[iRod]->getSparseMassMatrix();

	//	int nCoeff = (int)vMRod.size(); // Map to global
	//	for (int iCoeff = 0; iCoeff < nCoeff; iCoeff++)
	//	{
	//		Real val = vMRod[iCoeff].value();

	//		int row = vMRod[iCoeff].row();
	//		int col = vMRod[iCoeff].col();
	//		int irow = this->m_vlocToGlo[iRod + 1][row];
	//		int icol = this->m_vlocToGlo[iRod + 1][col];
	//		if (irow >= icol)
	//		{
	//			row = irow;
	//			col = icol;
	//		}
	//		else
	//		{
	//			row = icol;
	//			col = irow;
	//		}

	//		this->m_mm.push_back(Triplet<Real>(row, col, val));
	//	}
	//}
}

//void TensileStructureModel::updateStatex(const VectorXd& vx, const VectorXd& vv)
//{
//
//
//	SolidModel::updateStatex(vx, vv);
//
//	//this->updateRodsCenter();
//	//this->updateRodsAngles();
//	//this->updateRodsReferenceFrame();
//	//this->updateRodsReferenceTwist();
//	//this->updateRodsMaterialFrame();
//
//	this->m_isMeshReady = false;
//}

void TensileStructureModel::updateState0(const VectorXd& vX, const VectorXd& vV)
{
	SolidModel::updateState0(vX, vV);
	this->m_isMeshReady = false;
}

void TensileStructureModel::updateEnergy()
{
	if (!this->m_isRestReady)
		this->updateRest();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateEnergy(this->m_vx, this->m_vv);

	int nRod = (int) m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
		m_vpRodModels[iRod]->updateEnergy();

	//for (int i = 0; i < this->m_nr; ++i)
	//{
	//	const dVector& vx = this->m_vrodCenter[i].getPositions();
	//	for (int j = 0; j < (int) this->m_vpRodBendingEles[i].size(); ++j)
	//	{
	//		this->m_vpRodBendingEles[i][j]->updateEnergy(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodTwistEles[i][j]->updateEnergy(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//	}
	//}

	this->m_isEnergyReady = true;
}

void TensileStructureModel::updateForce()
{
	if (!this->m_isRestReady)
		this->updateRest();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateForce(this->m_vx, this->m_vv);

	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
		m_vpRodModels[iRod]->updateForce();

	//for (int i = 0; i < this->m_nr; ++i)
	//{
	//	const dVector& vx = this->m_vrodCenter[i].getPositions();
	//	for (int j = 0; j < (int) this->m_vpRodBendingEles[i].size(); ++j)
	//	{
	//		this->m_vpRodBendingEles[i][j]->updateForce(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodTwistEles[i][j]->updateForce(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//	}
	//}

	this->m_isForceReady = true;
}

void TensileStructureModel::updateJacobian()
{
	if (!this->m_isRestReady)
		this->updateRest();

	this->testJacobianLocal();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateJacobian(this->m_vx, this->m_vv);

	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
		m_vpRodModels[iRod]->updateJacobian();

	//for (int i = 0; i < this->m_nr; ++i)
	//{
	//	const dVector& vx = this->m_vrodCenter[i].getPositions();
	//	for (int j = 0; j < (int) this->m_vpRodBendingEles[i].size(); ++j)
	//	{
	//		this->m_vpRodBendingEles[i][j]->updateJacobian(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodTwistEles[i][j]->updateJacobian(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//	}
	//}

	this->m_isJacobianReady = true;
}

void TensileStructureModel::updateEnergyForce()
{
	if (!this->m_isRestReady)
		this->updateRest();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateEnergyForce(this->m_vx, this->m_vv);

	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
		m_vpRodModels[iRod]->updateEnergyForce();

	//for (int i = 0; i < this->m_nr; ++i)
	//{
	//	const dVector& vx = this->m_vrodCenter[i].getPositions();
	//	for (int j = 0; j < (int) this->m_vpRodBendingEles[i].size(); ++j)
	//	{
	//		this->m_vpRodBendingEles[i][j]->updateEnergy(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodBendingEles[i][j]->updateForce(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodTwistEles[i][j]->updateEnergy(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//		this->m_vpRodTwistEles[i][j]->updateForce(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//	}
	//}

	this->m_isEnergyReady = true;
}

void TensileStructureModel::updateForceJacobian()
{
	if (!this->m_isRestReady)
		this->updateRest();

	this->testForceLocal();
	this->testJacobianLocal();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateForceJacobian(this->m_vx, this->m_vv);

	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
		m_vpRodModels[iRod]->updateForceJacobian();

	//for (int i = 0; i < this->m_nr; ++i)
	//{
	//	const dVector& vx = this->m_vrodCenter[i].getPositions();
	//	for (int j = 0; j < (int) this->m_vpRodBendingEles[i].size(); ++j)
	//	{
	//		this->m_vpRodBendingEles[i][j]->updateForce(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodBendingEles[i][j]->updateJacobian(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodTwistEles[i][j]->updateForce(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//		this->m_vpRodTwistEles[i][j]->updateJacobian(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//	}
	//}

	this->m_isForceReady = true;
	this->m_isJacobianReady = true;
}

void TensileStructureModel::updateEnergyForceJacobian()
{
	if (!this->m_isRestReady)
		this->updateRest();

	this->testForceLocal();
	this->testJacobianLocal();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateEnergyForceJacobian(this->m_vx, this->m_vv);

	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
		m_vpRodModels[iRod]->updateEnergyForceJacobian();

	//for (int i = 0; i < this->m_nr; ++i)
	//{
	//	const dVector& vx = this->m_vrodCenter[i].getPositions();
	//	for (int j = 0; j < (int) this->m_vpRodBendingEles[i].size(); ++j)
	//	{
	//		this->m_vpRodBendingEles[i][j]->updateEnergy(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodBendingEles[i][j]->updateForce(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodBendingEles[i][j]->updateJacobian(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
	//		this->m_vpRodTwistEles[i][j]->updateEnergy(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//		this->m_vpRodTwistEles[i][j]->updateForce(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//		this->m_vpRodTwistEles[i][j]->updateJacobian(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
	//	}
	//}

	this->m_isEnergyReady = true;
	this->m_isForceReady = true;
	this->m_isJacobianReady = true;
}

Real TensileStructureModel::getEnergy()
{
	if (!this->m_isEnergyReady)
		this->updateEnergy();

	double energy = 0;

	for (int i = 0; i < this->m_numEle; ++i)
		energy += this->m_vpEles[i]->getEnergy();

	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
		energy += m_vpRodModels[iRod]->getEnergy();

	return energy;
}

void TensileStructureModel::addForce(VectorXd& vf, const vector<bool>* pvFixed)
{
	if (!this->m_isForceReady)
		this->updateForce();

	int numDOFMesh = this->m_numDOFMesh;

	VectorXd vfMesh(numDOFMesh);
	vfMesh.setZero(); // Set zero
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->addForce(vfMesh);

	for (int iDOF = 0; iDOF < numDOFMesh; iDOF++)
		vf(this->m_vlocToGlo[0][iDOF]) += vfMesh(iDOF);
		
	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
	{
		int numDOFRod = m_vpRodModels[iRod]->getRawDOFNumber();

		VectorXd vfRod(numDOFRod);
		vfRod.setZero(); // Set zero
		m_vpRodModels[iRod]->addForce(vfRod);

		for (int iDOF = 0; iDOF < numDOFRod; iDOF++)
			vf(this->m_vlocToGlo[iRod + 1][iDOF]) += vfRod(iDOF);
	}
}

void TensileStructureModel::addJacobian(tVector& vJ, const vector<bool>* pvFixed)
{
	if (!this->m_isJacobianReady)
		this->updateJacobian();

	tVector vJMesh; // Local
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->addJacobian(vJMesh);

	int nCoeff = (int) vJMesh.size(); // To global
	for (int iCoeff = 0; iCoeff < nCoeff; iCoeff++)
	{
		Real val = vJMesh[iCoeff].value();

		int row = vJMesh[iCoeff].row();
		int col = vJMesh[iCoeff].col();
		int irow = this->m_vlocToGlo[0][row];
		int icol = this->m_vlocToGlo[0][col];
		if (irow >= icol)
		{
			row = irow;
			col = icol;
		}
		else
		{
			row = icol;
			col = irow;
		}

		vJ.push_back(Triplet<Real>(row, col, val));
	}

	int nRod = (int)m_vpRodModels.size();
	for (int iRod = 0; iRod < nRod; iRod++)
	{
		tVector vJRod; // Local
		m_vpRodModels[iRod]->addJacobian(vJRod);

		int nCoeff = (int)vJRod.size(); // Map to global
		for (int iCoeff = 0; iCoeff < nCoeff; iCoeff++)
		{
			Real val = vJRod[iCoeff].value();

			int row = vJRod[iCoeff].row();
			int col = vJRod[iCoeff].col();
			int irow = this->m_vlocToGlo[iRod + 1][row]; 
			int icol = this->m_vlocToGlo[iRod + 1][col];
			if (irow >= icol)
			{
				row = irow;
				col = icol;
			}
			else
			{
				row = icol;
				col = irow;
			}

			vJ.push_back(Triplet<Real>(row, col, val));
		}
	}
}

void TensileStructureModel::getRopesPositions(dVector& vx) const
{
	for (int i = 0; i < (int) this->m_vRopes.size(); ++i)
	{
		const Rope& rope = this->m_vRopes[i];

		int nvr = (int)rope.m_vnodes.size();
		int ini = (rope.m_embeddedIni) ? 1 : 0;
		int end = (rope.m_embeddedEnd) ? nvr - 1 : nvr;

		for (int j = ini; j < end; ++j) // Regular nodes
			set3D(rope.m_vidx[j], rope.m_vnodes[j], vx);
	}
}

void TensileStructureModel::setRopesPositions(const dVector& vx)
{
	for (int i = 0; i < (int) this->m_vRopes.size(); ++i)
	{
		Rope& rope = this->m_vRopes[i];

		int nvr = (int)rope.m_vnodes.size();
		int ini = (rope.m_embeddedIni) ? 1 : 0;
		int end = (rope.m_embeddedEnd) ? nvr - 1 : nvr;

		for (int j = ini; j < end; ++j) // Regular nodes
			rope.m_vnodes[j] = get3D(rope.m_vidx[j], vx);

		if (rope.m_embeddedIni)
		{
			Vector3d x0i = get3D(rope.m_ini.m_vidx[0], vx);
			Vector3d x1i = get3D(rope.m_ini.m_vidx[1], vx);
			Vector3d x2i = get3D(rope.m_ini.m_vidx[2], vx);
			Vector3d vini = x0i*rope.m_ini.m_vwei[0] +
							x1i*rope.m_ini.m_vwei[1] +
							x2i*rope.m_ini.m_vwei[2];
			rope.m_vnodes.front() = vini;
		}

		if (rope.m_embeddedEnd)
		{
			Vector3d x0i = get3D(rope.m_end.m_vidx[0], vx);
			Vector3d x1i = get3D(rope.m_end.m_vidx[1], vx);
			Vector3d x2i = get3D(rope.m_end.m_vidx[2], vx);
			Vector3d vEnd = x0i*rope.m_end.m_vwei[0] +
							x1i*rope.m_end.m_vwei[1] +
							x2i*rope.m_end.m_vwei[2];
			rope.m_vnodes.back() = vEnd;
		}
	}
}

void TensileStructureModel::updateRest()
{

#ifdef OMP_RESTINIT
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateRest(this->m_vXMesh);

	this->updateMass();

	this->m_isRestReady = true;
	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

//void TensileStructureModel::updateRest()
//{
//
//#ifdef OMP_RESTINIT
//#pragma omp parallel
//#endif
//	for (int i = 0; i < this->m_numEle; ++i)
//		this->m_vpEles[i]->updateRest(this->m_vX);
//
//	int count = 0;
//	for (int i = 0; i < (int) this->m_vstructLoops.size(); ++i)
//	{
//		int ne = -1;
//		if (this->m_vstructLoops[i])
//			ne = (int) this->m_vstructNodes[i].size();
//		else ne = (int) this->m_vstructNodes[i].size() - 1;
//
//		for (int j = 0; j < ne; ++j)
//		{
//			EmbeddedRodStretchElement3D* pEle = this->m_vpRodStretchEles[count++];
//			pEle->setRestLength(pEle->getRestLength()*this->m_vstructPreStrain[i]);
//		}
//	}
//
//	for (int i = 0; i < (int) this->m_vRopes.size(); ++i)
//	{
//		for (int j = 0; j < (int) this->m_vRopes[i].m_vhookEles.size(); ++j)
//		{
//			EmbeddedPartialRodStretchElement3D* pEle = this->m_vpRopeHooks[this->m_vRopes[i].m_vhookEles[j]];
//			pEle->setRestLength(pEle->getRestLength()*this->m_vropesPreStrain[i]); // Pre-stretch the rope
//		}
//	}
//
//	for (int i = 0; i < (int) this->m_vRopes.size(); ++i)
//	{
//		for (int j = 0; j < (int) this->m_vRopes[i].m_vdhookEles.size(); ++j)
//		{
//			EmbeddedRodStretchElement3D* pEle = this->m_vpRopeDoubleHooks[this->m_vRopes[i].m_vdhookEles[j]];
//			pEle->setRestLength(pEle->getRestLength()*this->m_vropesPreStrain[i]); // Pre-stretch the rope
//		}
//	}
//
//	//this->updateRodsCenter();
//	//this->updateRodsAngles();
//
//	//for (int i = 0; i < this->m_nr; ++i)
//	//{
//	//	Curve& curve = this->m_vrodCenter[i];
//	//	Vector3d t = curve.getEdgeTangent(0);
//	//	Vector3d u(0.0, 1.0, 0.0);
//	//	Vector3d b = u.cross(t).normalized();
//	//	Vector3d n = t.cross(b).normalized();
//
//	//	Frame ref0;
//	//	ref0.tan = t;
//	//	ref0.nor = n;
//	//	ref0.bin = b;
//
//	//	vector<Frame> vrefFrames(curve.getEdgeNumber());
//	//	Curve::FramePTForward(ref0, curve, vrefFrames);
//	//	for (int j = 0; j < curve.getEdgeNumber(); ++j)
//	//	{
//	//		this->m_vrodAngle[i][j] = 0.0;
//	//		this->m_vrefFrames[i][j] = vrefFrames[j];
//	//		this->m_vmatFrames[i][j] = vrefFrames[j];
//	//	}
//	//	for (int j = 0; j < curve.getNodeNumber(); ++j)
//	//	{
//	//		this->m_vrodTwist[i][j] = 0.0;
//	//	}
//
//	//	const dVector& vx = curve.getPositions();
//	//	for (int j = 0; j < (int) this->m_vpRodBendingEles[i].size(); ++j)
//	//	{
//	//		this->m_vpRodBendingEles[i][j]->updateRest(vx, this->m_vrodAngle[i], this->m_vmatFrames[i]);
//	//		this->m_vpRodTwistEles[i][j]->updateRest(vx, this->m_vrodAngle[i], this->m_vmatFrames[i], this->m_vrodTwist[i]);
//	//	}
//	//}
//
//	//for (int i = 0; i < (int) this->m_vpRodStretchEles.size(); ++i)
//	//{
//	//	EmbeddedRodStretchElement3D* pEle = this->m_vpRodStretchEles[i];
//	//	const iVector& vnodeInd0 = pEle->getNodeIndices0();
//	//	const dVector& vnodebar0 = pEle->getEmbeddingCoordinates();
//	//	Vector3d p0 = vnodebar0[0] * get3D(vnodeInd0[0], this->m_vrodsDesignPos) + vnodebar0[1] * get3D(vnodeInd0[1], this->m_vrodsDesignPos) + vnodebar0[2] * get3D(vnodeInd0[2], this->m_vrodsDesignPos);
//	//	Vector3d p1 = vnodebar0[3] * get3D(vnodeInd0[3], this->m_vrodsDesignPos) + vnodebar0[4] * get3D(vnodeInd0[4], this->m_vrodsDesignPos) + vnodebar0[5] * get3D(vnodeInd0[5], this->m_vrodsDesignPos);
//	//	pEle->setRestLength((p1 - p0).norm()); 
//	//}
//
//	//for (int i = 0; i < (int) this->m_vpRodBendingEles.size(); ++i)
//	//{
//	//	EmbeddedRodBendElement3D* pEle = this->m_vpRodBendingEles[i];
//	//	const iVector& vnodeInd0 = pEle->getNodeIndices0();
//	//	const dVector& vnodebar0 = pEle->getEmbeddingCoordinates();
//	//	Vector3d p0 = vnodebar0[0] * get3D(vnodeInd0[0], this->m_vrodsDesignPos) + vnodebar0[1] * get3D(vnodeInd0[1], this->m_vrodsDesignPos) + vnodebar0[2] * get3D(vnodeInd0[2], this->m_vrodsDesignPos);
//	//	Vector3d p1 = vnodebar0[3] * get3D(vnodeInd0[3], this->m_vrodsDesignPos) + vnodebar0[4] * get3D(vnodeInd0[4], this->m_vrodsDesignPos) + vnodebar0[5] * get3D(vnodeInd0[5], this->m_vrodsDesignPos);
//	//	Vector3d p2 = vnodebar0[6] * get3D(vnodeInd0[6], this->m_vrodsDesignPos) + vnodebar0[7] * get3D(vnodeInd0[7], this->m_vrodsDesignPos) + vnodebar0[8] * get3D(vnodeInd0[8], this->m_vrodsDesignPos);
//	//	pEle->setVoronoiLength(0.5*((p1 - p0).norm() + (p2 - p1).norm()));
//	//}
//
//	//	VectorXd vXmod = this->m_vX; // For aligning boundaries together
//	//	for (int i = this->m_boundaryStretchEleStartIdx; i < this->m_numEle; ++i)
//	//	{
//	//		SolidElement* pEle = this->m_vpEles[i];
//	//		const iVector vidx0 = pEle->getIndices0();
//	//
//	//		// Compute transformation
//	//
//	//		TriMesh::EdgeHandle eh0 = this->m_pSimMesh->edge_handle(this->m_pSimMesh->halfedge_handle(this->m_pSimMesh->vertex_handle(vidx0[0])));
//	//		TriMesh::EdgeHandle eh1 = this->m_pSimMesh->edge_handle(this->m_pSimMesh->halfedge_handle(this->m_pSimMesh->vertex_handle(vidx0[1])));
//	//		vector<TriMesh::VertexHandle> vvh0, vvh1;
//	//		this->m_pSimMesh->getEdgeVertices(eh0, vvh0);
//	//		this->m_pSimMesh->getEdgeVertices(eh1, vvh1);
//	//		Vector2d a0 = getBlock2x1(vvh0[0].idx(), this->m_vX);
//	//		Vector2d b0 = getBlock2x1(vvh0[1].idx(), this->m_vX);
//	//		Vector2d a1 = getBlock2x1(vvh1[0].idx(), this->m_vX);
//	//		Vector2d b1 = getBlock2x1(vvh1[1].idx(), this->m_vX);
//	//		Vector2d e0;
//	//		Vector2d e1;
//	//		Vector2d tv;
//	//		if (this->m_vmapPatches2Single[vvh0[0].idx()] == this->m_vmapPatches2Single[vvh1[0].idx()])
//	//		{
//	//			e0 = b0 - a0;
//	//			e1 = b1 - a1;
//	//			tv = a0 - a1;
//	//		}
//	//		else
//	//		{
//	//			e0 = b0 - a0;
//	//			e1 = a1 - b1;
//	//			tv = a0 - b1;
//	//		}
//	//		double ang = atan2(e1[1] - e0[1],
//	//						   e1[0] - e0[0]);
//	//		Matrix2d Tt = computeTranslate(tv);
//	//		Matrix2d Tr = computeRotation(ang);
//	//
//	//		// Transform point
//	//
//	//		TriMesh::VertexHandle vh = this->m_pSimMesh->vertex_handle(vidx0[2]);
//	//		setBlock2x1(vh.idx(), Tr*Tt*getBlock2x1(vh.idx(), this->m_vX), vXmod);
//	//	}
//	//
//	//#ifdef OMP_RESTINIT
//	//#pragma omp parallel
//	//#endif
//	//	for (int i = 0; i < this->m_boundaryStretchEleStartIdx; ++i)
//	//		this->m_vpEles[i]->updateRest(this->m_vX); // Int
//	//
//	//#ifdef OMP_RESTINIT
//	//#pragma omp parallel
//	//#endif
//	//	for (int i = this->m_boundaryStretchEleStartIdx; i < this->m_numEle; ++i)
//	//		this->m_vpEles[i]->updateRest(vXmod); // Across patch elements
//
//	// Create mass vector and initialize it
//	this->m_vm = VectorXd(this->m_nrawDOF);
//	this->m_vm.setZero();
//	for (int i = 0; i < this->m_numEle; ++i)
//		this->m_vpEles[i]->addMass(this->m_vm);
//
//	//double w = this->m_pStructMaterial->getWRadius();
//	//double h = this->m_pStructMaterial->getHRadius();
//	//double d = this->m_pStructMaterial->getDensity();
//	//double r = 0.5*(w + h);
//	//double A = w*h*M_PI;
//	//for (int i = 0; i < (int)m_vpRodStretchEles.size(); ++i)
//	//{
//	//	double l = this->m_vpRodStretchEles[i]->getRestLength();
//	//	int ei = this->m_vpRodStretchEles[i]->getEdgeIndex();
//	//	this->m_vm[ei] += d*l*A*r*r*0.5;
//	//}
//
//	this->m_isRestReady = true;
//	this->m_isForceReady = false;
//	this->m_isEnergyReady = false;
//	this->m_isJacobianReady = false;
//}

void TensileStructureModel::initializeMeshSingle3D()
{
	// Remesh for structure ----------------------------------------------

	logSimu("\n[INFO] In %s: Contouring mesh for structure", __FUNCTION__);

	SurfaceRemesherContour structContourRemesher;

	int nStructCur = (int) this->m_vrawStructCurs.size();
	this->m_vseamsLoops.resize(nStructCur); // Are loops?

	logSimu("\n[INFO] In %s: %d rods found", __FUNCTION__, nStructCur);

	for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
	{
		if ((this->m_vrawStructCurs[iStructCur].front() - this->m_vrawStructCurs[iStructCur].back()).norm() < EPS_APPROX)
			this->m_vstructLoops.push_back(true);
		else this->m_vstructLoops.push_back(false);
		//structContourRemesher.addContourCurve(this->m_vrawStructCurs[iStructCur]);
	}

	RemeshOP structContourOP = structContourRemesher.remesh(this->m_pSimMesh);
	if (!structContourOP.m_OK)
		return;

	// Get struct curves edges

	this->m_vstructEdges.resize(nStructCur);
	this->m_vstructVerts.resize(nStructCur);
	for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
	{
		//this->m_vstructEdges[iStructCur] = structContourRemesher.getSortedContourEdges(iStructCur);
		//this->m_vstructVerts[iStructCur] = structContourRemesher.getSortedContourVerts(iStructCur);
	}

	delete this->m_pSimMesh; // Exchange mesh
	this->m_pSimMesh = structContourOP.m_pMesh;

	// Mesh creation ----------------------------------------------

	dVector vx3D;
	this->m_pSimMesh->getPoints(vx3D);
	this->m_pSimMesh->setPoints(vx3D, VPropSim::POSE);
	this->m_pSimMesh->setPoints(vx3D, VPropSim::POS0);
}

void TensileStructureModel::initializeMeshPatchesEmbedded3D()
{
	// Remesh for seams ----------------------------------------------

	logSimu("\n[INFO] In %s: Contouring mesh for seams", __FUNCTION__);

	SurfaceRemesherContour seamContourRemesher;

	int nSeamCur = (int) this->m_vrawSeamsCurs.size();

	logSimu("\n[INFO] In %s: %d seams found", __FUNCTION__, nSeamCur);

	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur) // Add lines
	{
		const Vector3d& vIni = this->m_vrawSeamsCurs[iSeamCur].front();
		const Vector3d& vFin = this->m_vrawSeamsCurs[iSeamCur].back();
		if ((vIni - vFin).norm() < EPS_APPROX)
			this->m_vseamsLoops.push_back(true);
		else this->m_vseamsLoops.push_back(false);

		//seamContourRemesher.addContourCurve(this->m_vrawSeamsCurs[iSeamCur]);
	}

	RemeshOP seamContourOP = seamContourRemesher.remesh(this->m_pSimMesh);
	if (!seamContourOP.m_OK)
		return;

	// Get seam curves edges

	this->m_vseamsEdges.resize(nSeamCur);
	this->m_vseamsVerts.resize(nSeamCur);
	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur)
	{
		//this->m_vseamsEdges[iSeamCur] = seamContourRemesher.getSortedContourEdges(iSeamCur);
		//this->m_vseamsVerts[iSeamCur] = seamContourRemesher.getSortedContourVerts(iSeamCur);
	}

	delete this->m_pSimMesh; // Exchange mesh
	this->m_pSimMesh = seamContourOP.m_pMesh;

	logSimu("\n[INFO] In %s: Cutting mesh for seams", __FUNCTION__);

	vector<MeshTraits::ID> vseamsEdges;
	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur) // Append all edges in a single vector for cutting
		vseamsEdges.insert(vseamsEdges.end(), m_vseamsEdges[iSeamCur].begin(), m_vseamsEdges[iSeamCur].end());

	std::sort(vseamsEdges.begin(), vseamsEdges.end()); // Remove duplicated edges from vector
	vseamsEdges.erase(std::unique(vseamsEdges.begin(), vseamsEdges.end()), vseamsEdges.end());

	logSimu("\n[INFO] In %s: %d seam edges found", __FUNCTION__, (int)vseamsEdges.size());

	if ((int)vseamsEdges.size() > 0)
	{
		SurfaceRemesherCutter seamCutterRemesher;
		seamCutterRemesher.setCutEdges(vseamsEdges);

		RemeshOP seamCutterOP = seamCutterRemesher.remesh(this->m_pSimMesh);
		if (!seamCutterOP.m_OK)
			return;

		delete this->m_pSimMesh; // Exchange mesh
		this->m_pSimMesh = seamCutterOP.m_pMesh;
	}

	// Build vertices maps

	this->m_pSimMesh->getSplittedMeshMaps(this->m_vmapPatches2Single,
		this->m_vmapSingle2Patches,
		this->m_mapPatchEdges);

	logSimu("\n[INFO] In %s: %d Seam edges mapped", __FUNCTION__, (int)m_mapPatchEdges.size());
	logSimu("\n[INFO] In %s: %d Seam vertices mapped", __FUNCTION__, (int)m_vmapSingle2Patches.size());

	// Find structure embedding

	double structTol = 0.0;
	for (TriMesh::EdgeIter e_it = this->m_pSimMesh->edges_begin(); e_it != this->m_pSimMesh->edges_end(); ++e_it)
		structTol += this->m_pSimMesh->calc_edge_length(*e_it);
	structTol /= this->m_pSimMesh->n_edges();

	int nStructCur = (int) this->m_vrawStructCurs.size();

	for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
	{
		const Vector3d& vIni = this->m_vrawStructCurs[iStructCur].front();
		const Vector3d& vFin = this->m_vrawStructCurs[iStructCur].back();
		if ((vIni - vFin).norm() < structTol*1e-3)
			this->m_vstructLoops.push_back(true);
		else this->m_vstructLoops.push_back(false);
	}

	vector<Vector3d> vresStructCloud;
	vector<vector<Vector3d>> vresStructCurs;
	for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
	{
		vector<Vector3d> vresStructCur;
		const vector<Vector3d>& vrawStructCur = m_vrawStructCurs[iStructCur];
		hermiteInterpolation(vrawStructCur, vresStructCur, m_vstructLoops[iStructCur], 0.5*structTol);
		vresStructCloud.insert(vresStructCloud.end(), vresStructCur.begin(), vresStructCur.end());
		vresStructCurs.push_back(vresStructCur);
	}

	BVHTree meshTree(0);
	meshTree.initializeMeshFace(this->m_pSimMesh);
	int nStructCloud = (int)vresStructCloud.size();
	if (nStructCloud != 0)
	{
		BVHTree	pointCloudTree(1); // BVH tree of points
		pointCloudTree.initializeCloud(vresStructCloud);

		vector<BVHClosePrimitivePair> vcolls; // Get potential projections on mesh
		BVHTree::testRecursive(&meshTree, &pointCloudTree, 0, 0, vcolls, structTol);

		vector<EmbeddedPoint> vstructCloudPro(nStructCloud);
		vector<double> vminimumDistance(nStructCloud);
		for (int i = 0; i < nStructCloud; ++i)
			vminimumDistance[i] = HUGE_VAL;

		int nPotColl = (int)vcolls.size(); // Project points
		for (int iPotColl = 0; iPotColl < nPotColl; iPotColl++)
		{
			int faceIdx = vcolls[iPotColl].m_prim0.m_index;
			int pointIdx = vcolls[iPotColl].m_prim1.m_index;

			Vector3d point = vresStructCloud[pointIdx];
			vector<TriMesh::VertexHandle> vvh; // Get mesh face vertices handle
			m_pSimMesh->getFaceVertices(m_pSimMesh->face_handle(faceIdx), vvh);
			Vector3d v0 = m_pSimMesh->getPoint(vvh[0]);
			Vector3d v1 = m_pSimMesh->getPoint(vvh[1]);
			Vector3d v2 = m_pSimMesh->getPoint(vvh[2]);

			double l0, l1;
			double distance = sqrDistanceVertexTriangle(point, v0, v1, v2, &l0, &l1);
			if (distance >= 0.0 && distance < structTol) // Consider closest projection
			{
				Vector3d pp = v0*(1 - l0 - l1) + v1*l0 + v2*l1;
				double D = (pp - point).squaredNorm(); // Dist

				if (vminimumDistance[pointIdx] <= D)
					continue; // Already found closer
				vminimumDistance[pointIdx] = D;

				vstructCloudPro[pointIdx].m_vidx.clear();
				vstructCloudPro[pointIdx].m_vwei.clear();
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[0].idx());
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[1].idx());
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[2].idx());
				vstructCloudPro[pointIdx].m_vwei.push_back(1 - l0 - l1);
				vstructCloudPro[pointIdx].m_vwei.push_back(l0);
				vstructCloudPro[pointIdx].m_vwei.push_back(l1);
			}
		}

		int count = 0;

		// Structure

		this->m_vstructNodes.resize(nStructCur);
		for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
		{
			int nCurPoint = (int)vresStructCurs[iStructCur].size();

			this->m_vstructNodes[iStructCur].resize(nCurPoint);
			for (int iCurPoint = 0; iCurPoint < nCurPoint; ++iCurPoint)
				this->m_vstructNodes[iStructCur][iCurPoint] = vstructCloudPro[count++];

			if (this->m_vstructLoops[iStructCur]) // Remove the last point of the loop structure
				this->m_vstructNodes[iStructCur].erase(this->m_vstructNodes[iStructCur].begin());
		}
	}

	// Create ropes

	int nRopes = (int) this->m_vrawRopes.size();
	for (int iRope = 0; iRope < nRopes; ++iRope)
	{
		const Vector3d& vIni = this->m_vrawRopes[iRope].first;
		const Vector3d& vFin = this->m_vrawRopes[iRope].second;
		vector<Vector3d> vRopeRaw;
		vRopeRaw.push_back(vIni);
		vRopeRaw.push_back(vFin);

		vector<bool> vembedded(2, 0.0);
		vector<Vector3d> vextremes = vRopeRaw;
		vector<EmbeddedPoint> vembeddedPoints(2);

		// Interpolate
		this->m_vRopes.push_back(Rope());
		vector<Vector3d>& vRopeInt = this->m_vRopes.back().m_vnodes;
		hermiteInterpolation(vRopeRaw, vRopeInt, false, 2); // Simple 

		for (int j = 0; j < 2; ++j)
		{
			BVHTree	pointTree(1);
			vector<Vector3d> vpoints;
			vpoints.push_back(vextremes[j]);
			pointTree.initializeCloud(vpoints);

			vector<BVHClosePrimitivePair> vcolls; // Get potential projections on mesh
			BVHTree::testRecursive(&meshTree, &pointTree, 0, 0, vcolls, structTol / 10);

			double minDistance = HUGE_VAL;

			int nPotColl = (int)vcolls.size(); // Project points
			for (int iPotColl = 0; iPotColl < nPotColl; iPotColl++)
			{
				int faceIdx = vcolls[iPotColl].m_prim0.m_index;

				vector<TriMesh::VertexHandle> vvh; // Get mesh face vertices handle
				m_pSimMesh->getFaceVertices(m_pSimMesh->face_handle(faceIdx), vvh);
				Vector3d v0 = m_pSimMesh->getPoint(vvh[0]);
				Vector3d v1 = m_pSimMesh->getPoint(vvh[1]);
				Vector3d v2 = m_pSimMesh->getPoint(vvh[2]);

				double l0, l1;
				double distance = sqrDistanceVertexTriangle(vpoints[0], v0, v1, v2, &l0, &l1);
				if (distance >= 0.0 && distance < structTol) // Consider closest projection
				{
					Vector3d pp = v0*(1 - l0 - l1) + v1*l0 + v2*l1;
					double D = (pp - vpoints[0]).squaredNorm(); // Dist

					if (minDistance <= D)
						continue; // Found
					minDistance = D;

					vembedded[j] = true;
					vembeddedPoints[j].m_vidx.clear();
					vembeddedPoints[j].m_vwei.clear();
					vembeddedPoints[j].m_vidx.push_back(vvh[0].idx());
					vembeddedPoints[j].m_vidx.push_back(vvh[1].idx());
					vembeddedPoints[j].m_vidx.push_back(vvh[2].idx());
					vembeddedPoints[j].m_vwei.push_back(1 - l0 - l1);
					vembeddedPoints[j].m_vwei.push_back(l0);
					vembeddedPoints[j].m_vwei.push_back(l1);
				}
			}
		}

		this->m_vRopes.back().m_embeddedIni = vembedded[0];
		this->m_vRopes.back().m_embeddedEnd = vembedded[1];
		if (vembedded[0]) this->m_vRopes.back().m_ini = vembeddedPoints[0];
		if (vembedded[1]) this->m_vRopes.back().m_end = vembeddedPoints[1];
	}

	// Mesh creation ----------------------------------------------

	dVector vx3D;
	this->m_pSimMesh->getPoints(vx3D);
	this->m_pSimMesh->setPoints(vx3D, VPropSim::POSE);
	this->m_pSimMesh->setPoints(vx3D, VPropSim::POS0);
}

void TensileStructureModel::initializeMeshPatchesEmbedded2D()
{
	vector<vector<TriMesh::EdgeHandle>> vehs;
	vector<vector<TriMesh::VertexHandle>> vvhs;
	m_pSimMesh->getSortedBoundaryLoopsVertices(vehs, vvhs);

	// Remesh for seams ----------------------------------------------

	logSimu("\n[INFO] In %s: Contouring mesh for seams", __FUNCTION__);

	SurfaceRemesherContour seamContourRemesher;

	int nSeamCur = (int) this->m_vrawSeamsCurs.size();

	logSimu("\n[INFO] In %s: %d seams found", __FUNCTION__, nSeamCur);

	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur) // Add lines
	{
		const Vector3d& vIni = this->m_vrawSeamsCurs[iSeamCur].front();
		const Vector3d& vFin = this->m_vrawSeamsCurs[iSeamCur].back();
		if ((vIni - vFin).norm() < EPS_APPROX)
			this->m_vseamsLoops.push_back(true);
		else this->m_vseamsLoops.push_back(false);

		//seamContourRemesher.addContourCurve(this->m_vrawSeamsCurs[iSeamCur]);
	}

	RemeshOP seamContourOP = seamContourRemesher.remesh(this->m_pSimMesh);
	if (!seamContourOP.m_OK)
		return;

	// Get seam curves edges

	this->m_vseamsEdges.resize(nSeamCur);
	this->m_vseamsVerts.resize(nSeamCur);
	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur)
	{
		//this->m_vseamsEdges[iSeamCur] = seamContourRemesher.getSortedContourEdges(iSeamCur);
		//this->m_vseamsVerts[iSeamCur] = seamContourRemesher.getSortedContourVerts(iSeamCur);
	}

	delete this->m_pSimMesh; // Exchange mesh
	this->m_pSimMesh = seamContourOP.m_pMesh;

	logSimu("\n[INFO] In %s: Cutting mesh for seams", __FUNCTION__);

	vector<MeshTraits::ID> vseamsEdges;
	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur) // Append all edges in a single vector for cutting
		vseamsEdges.insert(vseamsEdges.end(), m_vseamsEdges[iSeamCur].begin(), m_vseamsEdges[iSeamCur].end());

	std::sort(vseamsEdges.begin(), vseamsEdges.end()); // Remove duplicated edges from vector
	vseamsEdges.erase(std::unique(vseamsEdges.begin(), vseamsEdges.end()), vseamsEdges.end());

	logSimu("\n[INFO] In %s: %d seam edges found", __FUNCTION__, (int)vseamsEdges.size());

	if ((int)vseamsEdges.size() > 0)
	{
		SurfaceRemesherCutter seamCutterRemesher;
		seamCutterRemesher.setCutEdges(vseamsEdges);

		RemeshOP seamCutterOP = seamCutterRemesher.remesh(this->m_pSimMesh);
		if (!seamCutterOP.m_OK)
			return;

		delete this->m_pSimMesh; // Exchange mesh
		this->m_pSimMesh = seamCutterOP.m_pMesh;
	}

	// Build vertices maps

	this->m_pSimMesh->getSplittedMeshMaps(this->m_vmapPatches2Single,
		this->m_vmapSingle2Patches,
		this->m_mapPatchEdges);

	logSimu("\n[INFO] In %s: %d Seam edges mapped", __FUNCTION__, (int)m_mapPatchEdges.size());
	logSimu("\n[INFO] In %s: %d Seam vertices mapped", __FUNCTION__, (int)m_vmapSingle2Patches.size());

	// Find structure embedding

	double structTol = 0.0;
	for (TriMesh::EdgeIter e_it = this->m_pSimMesh->edges_begin(); e_it != this->m_pSimMesh->edges_end(); ++e_it)
		structTol += this->m_pSimMesh->calc_edge_length(*e_it);
	structTol /= this->m_pSimMesh->n_edges();

	int nStructCur = (int) this->m_vrawStructCurs.size();

	for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
	{
		const Vector3d& vIni = this->m_vrawStructCurs[iStructCur].front();
		const Vector3d& vFin = this->m_vrawStructCurs[iStructCur].back();
		if ((vIni - vFin).norm() < structTol*1e-3)
			this->m_vstructLoops.push_back(true);
		else this->m_vstructLoops.push_back(false);
	}

	vector<Vector3d> vresStructCloud;
	vector<vector<Vector3d>> vresStructCurs;
	for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
	{
		vector<Vector3d> vresStructCur;
		const vector<Vector3d>& vrawStructCur = m_vrawStructCurs[iStructCur];
		hermiteInterpolation(vrawStructCur, vresStructCur, m_vstructLoops[iStructCur], 10 * structTol);
		vresStructCloud.insert(vresStructCloud.end(), vresStructCur.begin(), vresStructCur.end());
		vresStructCurs.push_back(vresStructCur);
	}

	int nStructCloud = (int)vresStructCloud.size();
	if (nStructCloud != 0)
	{
		BVHTree meshTree(0);
		BVHTree	pointCloudTree(1);
		pointCloudTree.initializeCloud(vresStructCloud);
		meshTree.initializeMeshFace(this->m_pSimMesh);

		vector<BVHClosePrimitivePair> vcolls; // Get potential projections on mesh
		BVHTree::testRecursive(&meshTree, &pointCloudTree, 0, 0, vcolls, structTol);

		vector<EmbeddedPoint> vstructCloudPro(nStructCloud);
		vector<double> vminimumDistance(nStructCloud);
		for (int i = 0; i < nStructCloud; ++i)
			vminimumDistance[i] = HUGE_VAL;

		int nPotColl = (int)vcolls.size(); // Project points
		for (int iPotColl = 0; iPotColl < nPotColl; iPotColl++)
		{
			int faceIdx = vcolls[iPotColl].m_prim0.m_index;
			int pointIdx = vcolls[iPotColl].m_prim1.m_index;

			Vector3d point = vresStructCloud[pointIdx];
			vector<TriMesh::VertexHandle> vvh; // Get mesh face vertices handle
			m_pSimMesh->getFaceVertices(m_pSimMesh->face_handle(faceIdx), vvh);
			Vector3d v0 = m_pSimMesh->getPoint(vvh[0]);
			Vector3d v1 = m_pSimMesh->getPoint(vvh[1]);
			Vector3d v2 = m_pSimMesh->getPoint(vvh[2]);

			double l0, l1;
			double distance = sqrDistanceVertexTriangle(point, v0, v1, v2, &l0, &l1);
			if (distance >= 0.0 && distance < structTol) // Consider closest projection
			{
				Vector3d pp = v0*(1 - l0 - l1) + v1*l0 + v2*l1;
				double D = (pp - point).squaredNorm(); // Dist

				if (vminimumDistance[pointIdx] <= D)
					continue; // Already found closer
				vminimumDistance[pointIdx] = D;

				vstructCloudPro[pointIdx].m_vidx.clear();
				vstructCloudPro[pointIdx].m_vwei.clear();
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[0].idx());
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[1].idx());
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[2].idx());
				vstructCloudPro[pointIdx].m_vwei.push_back(1 - l0 - l1);
				vstructCloudPro[pointIdx].m_vwei.push_back(l0);
				vstructCloudPro[pointIdx].m_vwei.push_back(l1);
			}
		}

		int count = 0;
		this->m_vstructNodes.resize(nStructCur);
		for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
		{
			int nCurPoint = (int)vresStructCurs[iStructCur].size();

			this->m_vstructNodes[iStructCur].resize(nCurPoint);
			for (int iCurPoint = 0; iCurPoint < nCurPoint; ++iCurPoint)
				this->m_vstructNodes[iStructCur][iCurPoint] = vstructCloudPro[count++];

			if (this->m_vstructLoops[iStructCur]) // Remove the last point of the loop
				this->m_vstructNodes[iStructCur].erase(this->m_vstructNodes[iStructCur].begin());
		}
	}

	// Mesh creation ----------------------------------------------

	//  Parametrize surface

	TriMeshParametrizer patchParametrizer;
	patchParametrizer.setCurveAngleK(1.0);
	patchParametrizer.setCurveLengthK(1.0);
	patchParametrizer.setCurveSolverError(1e-9);

	dVector vX3D;
	dVector vX2D;
	patchParametrizer.parametrizePatches(this->m_pSimMesh, vX2D);
	to3D(vX2D, vX3D); // Store 2D rest state in the XY 3D plane

	dVector vx3D;
	this->m_pSimMesh->getPoints(vx3D);
	this->m_pSimMesh->setPoints(vx3D, VPropSim::POSE);
	this->m_pSimMesh->setPoints(vX3D, VPropSim::POS0);
}

void TensileStructureModel::initializeMeshPatchesEmbedded3D_10()
{
	// Remesh for seams ----------------------------------------------

	logSimu("\n[INFO] In %s: Contouring mesh for seams", __FUNCTION__);

	SurfaceRemesherContour seamContourRemesher;

	int nSeamCur = (int) this->m_vrawSeamsCurs.size();

	logSimu("\n[INFO] In %s: %d seams found", __FUNCTION__, nSeamCur);

	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur) // Add lines
	{
		const Vector3d& vIni = this->m_vrawSeamsCurs[iSeamCur].front();
		const Vector3d& vFin = this->m_vrawSeamsCurs[iSeamCur].back();
		if ((vIni - vFin).norm() < EPS_APPROX)
			this->m_vseamsLoops.push_back(true);
		else this->m_vseamsLoops.push_back(false);

		//seamContourRemesher.addContourCurve(this->m_vrawSeamsCurs[iSeamCur]);
	}

	RemeshOP seamContourOP = seamContourRemesher.remesh(this->m_pSimMesh);
	if (!seamContourOP.m_OK)
		return;

	// Get seam curves edges

	this->m_vseamsEdges.resize(nSeamCur);
	this->m_vseamsVerts.resize(nSeamCur);
	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur)
	{
		//this->m_vseamsEdges[iSeamCur] = seamContourRemesher.getSortedContourEdges(iSeamCur);
		//this->m_vseamsVerts[iSeamCur] = seamContourRemesher.getSortedContourVerts(iSeamCur);
	}

	delete this->m_pSimMesh; // Exchange mesh
	this->m_pSimMesh = seamContourOP.m_pMesh;

	logSimu("\n[INFO] In %s: Cutting mesh for seams", __FUNCTION__);

	vector<MeshTraits::ID> vseamsEdges;
	for (int iSeamCur = 0; iSeamCur < nSeamCur; ++iSeamCur) // Append all edges in a single vector for cutting
		vseamsEdges.insert(vseamsEdges.end(), m_vseamsEdges[iSeamCur].begin(), m_vseamsEdges[iSeamCur].end());

	std::sort(vseamsEdges.begin(), vseamsEdges.end()); // Remove duplicated edges from vector
	vseamsEdges.erase(std::unique(vseamsEdges.begin(), vseamsEdges.end()), vseamsEdges.end());

	logSimu("\n[INFO] In %s: %d seam edges found", __FUNCTION__, (int)vseamsEdges.size());

	if ((int)vseamsEdges.size() > 0)
	{
		SurfaceRemesherCutter seamCutterRemesher;
		seamCutterRemesher.setCutEdges(vseamsEdges);

		RemeshOP seamCutterOP = seamCutterRemesher.remesh(this->m_pSimMesh);
		if (!seamCutterOP.m_OK)
			return;

		delete this->m_pSimMesh; // Exchange mesh
		this->m_pSimMesh = seamCutterOP.m_pMesh;
	}

	// Build vertices maps

	this->m_pSimMesh->getSplittedMeshMaps(this->m_vmapPatches2Single,
		this->m_vmapSingle2Patches,
		this->m_mapPatchEdges);

	logSimu("\n[INFO] In %s: %d Seam edges mapped", __FUNCTION__, (int)m_mapPatchEdges.size());
	logSimu("\n[INFO] In %s: %d Seam vertices mapped", __FUNCTION__, (int)m_vmapSingle2Patches.size());

	// Find structure embedding

	double structTol = 0.0;
	for (TriMesh::EdgeIter e_it = this->m_pSimMesh->edges_begin(); e_it != this->m_pSimMesh->edges_end(); ++e_it)
		structTol += this->m_pSimMesh->calc_edge_length(*e_it);
	structTol /= this->m_pSimMesh->n_edges();

	int nStructCur = (int) this->m_vrawStructCurs.size();

	for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
	{
		const Vector3d& vIni = this->m_vrawStructCurs[iStructCur].front();
		const Vector3d& vFin = this->m_vrawStructCurs[iStructCur].back();
		if ((vIni - vFin).norm() < structTol*1e-3)
			this->m_vstructLoops.push_back(true);
		else this->m_vstructLoops.push_back(false);
	}

	vector<Vector3d> vresStructCloud;
	vector<vector<Vector3d>> vresStructCurs;
	for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
	{
		vector<Vector3d> vresStructCur;
		const vector<Vector3d>& vrawStructCur = m_vrawStructCurs[iStructCur];
		hermiteInterpolation(vrawStructCur, vresStructCur, m_vstructLoops[iStructCur], 0.5*structTol);
		vresStructCloud.insert(vresStructCloud.end(), vresStructCur.begin(), vresStructCur.end());
		vresStructCurs.push_back(vresStructCur);
	}

	int nStructCloud = (int)vresStructCloud.size();
	if (nStructCloud != 0)
	{
		BVHTree meshTree(0);
		BVHTree	pointCloudTree(1);
		pointCloudTree.initializeCloud(vresStructCloud);
		meshTree.initializeMeshFace(this->m_pSimMesh);

		vector<BVHClosePrimitivePair> vcolls; // Get potential projections on mesh
		BVHTree::testRecursive(&meshTree, &pointCloudTree, 0, 0, vcolls, structTol);

		vector<EmbeddedPoint> vstructCloudPro(nStructCloud);
		vector<double> vminimumDistance(nStructCloud);
		for (int i = 0; i < nStructCloud; ++i)
			vminimumDistance[i] = HUGE_VAL;

		int nPotColl = (int)vcolls.size(); // Project points
		for (int iPotColl = 0; iPotColl < nPotColl; iPotColl++)
		{
			int faceIdx = vcolls[iPotColl].m_prim0.m_index;
			int pointIdx = vcolls[iPotColl].m_prim1.m_index;

			Vector3d point = vresStructCloud[pointIdx];
			vector<TriMesh::VertexHandle> vvh; // Get mesh face vertices handle
			m_pSimMesh->getFaceVertices(m_pSimMesh->face_handle(faceIdx), vvh);
			Vector3d v0 = m_pSimMesh->getPoint(vvh[0]);
			Vector3d v1 = m_pSimMesh->getPoint(vvh[1]);
			Vector3d v2 = m_pSimMesh->getPoint(vvh[2]);

			double l0, l1;
			double distance = sqrDistanceVertexTriangle(point, v0, v1, v2, &l0, &l1);
			if (distance >= 0.0 && distance < structTol) // Consider closest projection
			{
				Vector3d pp = v0*(1 - l0 - l1) + v1*l0 + v2*l1;
				double D = (pp - point).squaredNorm(); // Dist

				if (vminimumDistance[pointIdx] <= D)
					continue; // Already found closer
				vminimumDistance[pointIdx] = D;

				vstructCloudPro[pointIdx].m_vidx.clear();
				vstructCloudPro[pointIdx].m_vwei.clear();
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[0].idx());
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[1].idx());
				vstructCloudPro[pointIdx].m_vidx.push_back(vvh[2].idx());
				vstructCloudPro[pointIdx].m_vwei.push_back(1 - l0 - l1);
				vstructCloudPro[pointIdx].m_vwei.push_back(l0);
				vstructCloudPro[pointIdx].m_vwei.push_back(l1);
			}
		}

		int count = 0;
		this->m_vstructNodes.resize(nStructCur);
		for (int iStructCur = 0; iStructCur < nStructCur; ++iStructCur)
		{
			int nCurPoint = (int)vresStructCurs[iStructCur].size();

			this->m_vstructNodes[iStructCur].resize(nCurPoint);
			for (int iCurPoint = 0; iCurPoint < nCurPoint; ++iCurPoint)
				this->m_vstructNodes[iStructCur][iCurPoint] = vstructCloudPro[count++];

			if (this->m_vstructLoops[iStructCur]) // Remove the last point of the loop
				this->m_vstructNodes[iStructCur].erase(this->m_vstructNodes[iStructCur].begin());
		}
	}

	// Mesh creation ----------------------------------------------

	dVector vx3D;
	this->m_pSimMesh->getPoints(vx3D);
	this->m_pSimMesh->setPoints(vx3D, VPropSim::POSE);
	this->m_pSimMesh->setPoints(vx3D, VPropSim::POS0);
}

void TensileStructureModel::updateSimMeshSingle3D()
{
	if (this->m_isMeshReady)
		return; // Updated

	this->m_pSimMesh->setPoints(toSTL(this->m_vX), VPropSim::POS0);
	this->m_pSimMesh->setPoints(toSTL(this->m_vx), VPropSim::POSE);
	this->m_pSimMesh->setPoints(toSTL(this->m_vv), VPropSim::VELE);

	this->m_isMeshReady = true;
}

void TensileStructureModel::updateSimMeshPatchesEmbedded3D()
{
	if (this->m_isMeshReady)
		return; // Updated

	dVector vxt = toSTL(this->m_vx);
	dVector vvt = toSTL(this->m_vv);

	setRopesPositions(vxt);

	dVector vxSingle;
	dVector vvSingle;
	getSubvector(0, 3 * this->m_numMeshNodes_x, vxt, vxSingle);
	getSubvector(0, 3 * this->m_numMeshNodes_x, vvt, vvSingle);
	dVector vxPatches(3 * this->m_numMeshNodes_x);
	dVector vvPatches(3 * this->m_numMeshNodes_x);
	applyIndexMap(this->m_vmapSingle2Patches, vxSingle, 3, vxPatches);
	applyIndexMap(this->m_vmapSingle2Patches, vvSingle, 3, vvPatches);
	this->m_pSimMesh->setPoints(vxPatches, VPropSim::POSE);
	this->m_pSimMesh->setPoints(vvPatches, VPropSim::VELE);

	dVector vXPatches;
	dVector vXt = toSTL(this->m_vX);
	getSubvector(0, 3 * this->m_numMeshNodes_X, vXt, vXPatches);
	this->m_pSimMesh->setPoints(vXPatches, VPropSim::POS0);

	this->m_isMeshReady = true;
}

void TensileStructureModel::updateSimMeshPatchesEmbedded2D()
{
	if (this->m_isMeshReady)
		return; // Updated

	dVector vxSingle = toSTL(this->m_vx);
	dVector vvSingle = toSTL(this->m_vv);
	dVector vxPatches(3 * this->m_numNodeSim0);
	dVector vvPatches(3 * this->m_numNodeSim0);
	applyIndexMap(this->m_vmapSingle2Patches, vxSingle, 3, vxPatches);
	applyIndexMap(this->m_vmapSingle2Patches, vvSingle, 3, vvPatches);
	this->m_pSimMesh->setPoints(vxPatches, VPropSim::POSE);
	this->m_pSimMesh->setPoints(vvPatches, VPropSim::VELE);

	dVector vX2D = toSTL(this->m_vX);
	dVector vX3D;
	to3D(vX2D, vX3D);
	this->m_pSimMesh->setPoints(vX3D, VPropSim::POS0);

	this->m_isMeshReady = true;
}

void TensileStructureModel::updateSimMeshPatchesEmbedded3D_10()
{
	if (this->m_isMeshReady)
		return; // Updated

	dVector vx = toSTL(this->m_vx);
	dVector vv = toSTL(this->m_vv);
	dVector vxSingle(3 * this->m_numNodeSim);
	dVector vvSingle(3 * this->m_numNodeSim);
	getSubvector(0, 3 * this->m_numNodeSim, vx, vxSingle);
	getSubvector(0, 3 * this->m_numNodeSim, vv, vvSingle);
	dVector vxPatches(3 * this->m_numNodeSim0);
	dVector vvPatches(3 * this->m_numNodeSim0);
	applyIndexMap(this->m_vmapSingle2Patches, vxSingle, 3, vxPatches);
	applyIndexMap(this->m_vmapSingle2Patches, vvSingle, 3, vvPatches);
	this->m_pSimMesh->setPoints(vxPatches, VPropSim::POSE);
	this->m_pSimMesh->setPoints(vvPatches, VPropSim::VELE);

	dVector vX = toSTL(this->m_vX);
	dVector vXPatches(3 * this->m_numNodeSim0);
	getSubvector(0, 3 * this->m_numNodeSim0, vX, vXPatches);
	this->m_pSimMesh->setPoints(vXPatches, VPropSim::POS0);

	this->m_isMeshReady = true;
}

void TensileStructureModel::initializeElementsSingle3D()
{
	this->freeElements();

	int nf = (int) this->m_pSimMesh->n_faces();
	int ne = (int) this->m_pSimMesh->n_edges();
	int nv = (int) this->m_pSimMesh->n_vertices();

	this->m_numEle = nf; // Area elements
	this->m_numEle += ne; // Length elements
	this->m_numEle += ne; // Hinge elements

	int nr = (int) this->m_vstructVerts.size();

	for (int r = 0; r < nr; ++r)
	{
		int nre = (int) this->m_vstructEdges[r].size();

		if (this->m_vstructLoops[r])
			this->m_numEle += nre - 1;
		else this->m_numEle += nre;
		this->m_numEle += nre;
	}

	this->m_vpEles.reserve(this->m_numEle); // Estimation

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());

	// Initialize area preserving elements

	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		DSAreaElement* pEle = new DSAreaElement(3);

		// Set indices
		vector<int> vinds(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds[v] = (*fv_it).idx();
			++fv_it;
		}
		pEle->setNodeIndices0(vinds);
		pEle->setNodeIndices(vinds);

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSAreaEles.push_back(pEle);
	}

	// Initialize stretch mass-spring elements

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		TriMesh::EdgeHandle eh = *e_it;

		DSEdgeElement* pEle = new DSEdgeElement(3);

		// Set indices
		vector<int> vinds(2);
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);

		vinds[0] = m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx();
		vinds[1] = m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx();
		pEle->setNodeIndices0(vinds);
		pEle->setNodeIndices(vinds);

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSEdgeEles.push_back(pEle);
	}

	// Initialize discrete-shells bending elements 

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		TriMesh::EdgeHandle eh = *e_it;

		if (this->m_pSimMesh->is_boundary(eh))
			continue; // Ignore boundaries

		DSHingeElement* pEle = new DSHingeElement(3);

		// Set indices
		vector<int> vinds(4);
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);

		for (int i = 0; i < 4; i++)
			vinds[i] = m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx();
		pEle->setNodeIndices0(vinds);
		pEle->setNodeIndices(vinds);

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSBendingEles.push_back(pEle);
	}

	//// Initialize rod edge elements

	//for (int i = 0; i < nr; ++i)
	//{
	//	int nrv = (int) this->m_vstructVerts[i].size();
	//	int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
	//	for (int j = 0; j < end; ++j)
	//	{
	//		int i0 = j; // Loop?
	//		int i1 = (j + 1) % nrv;

	//		RodEdgeElement3D* pEle = new RodEdgeElement3D(3);
	//		vector<int> vinds(2);
	//		vinds[0] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i0]).idx();
	//		vinds[1] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i1]).idx();

	//		pEle->setNodeIndices0(vinds);
	//		pEle->setNodeIndices(vinds);

	//		// Add element
	//		this->m_vpEles.push_back(pEle);
	//		this->m_vpRodStretchEles.push_back(pEle);
	//	}
	//}

	//// Initialize rod angle elements

	//for (int i = 0; i < nr; ++i)
	//{
	//	int ini = (this->m_vstructLoops[i]) ? 0 : 1;
	//	int nrv = (int) this->m_vstructVerts[i].size();
	//	int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
	//	for (int j = ini; j < end; ++j)
	//	{
	//		int i1 = j;
	//		int i0 = (j - 1)%nrv;
	//		int i2 = (j + 1)%nrv;
	//		if (i0 < 0) i0 += nrv;

	//		RodAngleElement3D* pEle = new RodAngleElement3D(3);
	//		vector<int> vinds(3);
	//		vinds[0] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i0]).idx();
	//		vinds[1] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i1]).idx();
	//		vinds[2] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i2]).idx();
	//		pEle->setNodeIndices0(vinds);
	//		pEle->setNodeIndices(vinds);

	//		// Add element 
	//		this->m_vpEles.push_back(pEle);
	//		this->m_vpRodBendingEles.push_back(pEle);
	//	}
	//}

	this->m_numEle = (int) this->m_vpEles.size();

	// Initialize rest state vector 

	this->m_numDim0 = this->m_numDim = 3;
	this->m_numNodeSim0 = this->m_numNodeSim = nv;
	this->m_nrawDOF0 = this->m_nrawDOF = 3 * nv;

	dVector vx;
	dVector vX;
	this->m_pSimMesh->getPoints(vx, VPropSim::POSE);
	this->m_pSimMesh->getPoints(vX, VPropSim::POS0);
	this->m_vX = toEigen(vX);
	this->m_vx = toEigen(vx);
	this->m_vv.resize(this->m_nrawDOF);
	this->m_vv.setZero(); // From zero
}

void TensileStructureModel::initializeElementsPatchesEmbedded3D()
{
	this->freeElements();

	int nf = (int) this->m_pSimMesh->n_faces();
	int ne0 = (int) this->m_pSimMesh->n_edges();
	int nv0 = (int) this->m_pSimMesh->n_vertices();
	int nv = (int) this->m_vmapSingle2Patches.size();

	this->m_numEle = nf; // Area elements
	this->m_numEle += ne0; // Length elements
	this->m_numEle += ne0; // Hinge elements

	int nr = (int) this->m_vstructNodes.size();

	for (int r = 0; r < nr; ++r)
	{
		int nre = (int) this->m_vstructNodes[r].size();

		if (this->m_vstructLoops[r])
			this->m_numEle += nre - 1;
		else this->m_numEle += nre;
		this->m_numEle += nre;
	}

	this->m_vpEles.reserve(this->m_numEle); // Estimation

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());

	//// Initialize area preserving elements

	//for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	//{
	//	DSAreaElement* pEle = new DSAreaElement(3);

	//	// Set indices
	//	vector<int> vinds0(3);
	//	TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
	//	for (int v = 0; v < 3; ++v)
	//	{
	//		vinds0[v] = (*fv_it).idx();
	//		++fv_it;
	//	}
	//	pEle->setNodeIndices0(vinds0);
	//	
	//	vector<int> vinds;
	//	transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
	//	pEle->setNodeIndices(vinds); // Map global simulation indices

	//	// Add element
	//	this->m_vpEles.push_back(pEle);
	//	this->m_vpDSAreaEles.push_back(pEle);
	//}

	//// Initialize stretch mass-spring elements

	//for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	//{
	//	TriMesh::EdgeHandle eh = *e_it;

	//	DSEdgeElement* pEle = new DSEdgeElement(3);

	//	// Set indices
	//	vector<int> vinds0(2);
	//	std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
	//	vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
	//	vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);

	//	vinds0[0] = m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx();
	//	vinds0[1] = m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx();
	//	pEle->setNodeIndices0(vinds0);

	//	vector<int> vinds;
	//	transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
	//	pEle->setNodeIndices(vinds); // Map global simulation indices

	//	// Add element
	//	this->m_vpEles.push_back(pEle);
	//	this->m_vpDSEdgeEles.push_back(pEle);
	//}

	// Initialize area preserving elements

	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		CSTElement* pEle = new CSTElement(3);

		// Set indices
		vector<int> vinds0(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds0[v] = (*fv_it).idx();
			++fv_it;
		}
		pEle->setNodeIndices0(vinds0);

		vector<int> vinds;
		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
		pEle->setNodeIndices(vinds); // Map global simulation indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSStretchEles.push_back(pEle);
	}

	// Initialize discrete-shells bending elements 

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		TriMesh::EdgeHandle eh = *e_it;

		if (this->m_pSimMesh->is_boundary(eh))
			continue; // Ignore boundaries

		DSHingeElement* pEle = new DSHingeElement(3);

		// Set indices
		vector<int> vinds0(4);
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);

		for (int i = 0; i < 4; i++)
			vinds0[i] = m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx();
		pEle->setNodeIndices0(vinds0);

		vector<int> vinds;
		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
		pEle->setNodeIndices(vinds); // Map global simulation indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSBendingEles.push_back(pEle);
	}

	// Initialize rod edge elements

	for (int i = 0; i < nr; ++i)
	{
		int nrv = (int) this->m_vstructNodes[i].size();
		int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
		for (int j = 0; j < end; ++j)
		{
			int i0 = j; // Loop?
			int i1 = (j + 1) % nrv;

			EmbeddedRodStretchElement3D* pEle = new EmbeddedRodStretchElement3D(3);

			dVector vbars;
			iVector vinds;
			iVector vinds0;
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(this->m_vstructNodes[i][i0].m_vidx[ii]);
				vbars.push_back(this->m_vstructNodes[i][i0].m_vwei[ii]);
			}
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(this->m_vstructNodes[i][i1].m_vidx[ii]);
				vbars.push_back(this->m_vstructNodes[i][i1].m_vwei[ii]);
			}

			pEle->setEmbCoordinates(vbars);
			pEle->setNodeIndices0(vinds0);

			transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
			pEle->setNodeIndices(vinds); // Map global simulation indices

			// Add element
			this->m_vpEles.push_back(pEle);
			this->m_vpRodStretchEles.push_back(pEle);
		}
	}

	// Initialize rod angle elements

	for (int i = 0; i < nr; ++i)
	{
		int ini = (this->m_vstructLoops[i]) ? 0 : 1;
		int nrv = (int) this->m_vstructNodes[i].size();
		int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
		for (int j = ini; j < end; ++j)
		{
			int i1 = j;
			int i0 = (j - 1) % nrv;
			int i2 = (j + 1) % nrv;
			if (i0 < 0) i0 += nrv;

			EmbeddedRodBendElement3D* pEle = new EmbeddedRodBendElement3D(3);

			dVector vbars;
			iVector vinds;
			iVector vinds0;
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(this->m_vstructNodes[i][i0].m_vidx[ii]);
				vbars.push_back(this->m_vstructNodes[i][i0].m_vwei[ii]);
			}
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(this->m_vstructNodes[i][i1].m_vidx[ii]);
				vbars.push_back(this->m_vstructNodes[i][i1].m_vwei[ii]);
			}
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(this->m_vstructNodes[i][i2].m_vidx[ii]);
				vbars.push_back(this->m_vstructNodes[i][i2].m_vwei[ii]);
			}

			pEle->setEmbCoordinates(vbars);
			pEle->setNodeIndices0(vinds0);

			transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
			pEle->setNodeIndices(vinds); // Map global simulation indices

			// Add element 
			this->m_vpEles.push_back(pEle);
			this->m_vpRodBendingEles.push_back(pEle);
		}
	}

	int nRopes = (int) this->m_vRopes.size();

	int idxOffset0 = nv0;

	// Initialize rope elements

	for (int i = 0; i < nRopes; ++i)
	{
		Rope& rope = this->m_vRopes[i];
		int nvr = (int)rope.m_vnodes.size();

		// Indices
		rope.m_vidx.resize(nvr);
		int ini = (rope.m_embeddedIni) ? 1 : 0;
		int end = (rope.m_embeddedEnd) ? nvr - 1 : nvr;
		if (rope.m_embeddedIni) rope.m_vidx.front() = -1;
		if (rope.m_embeddedEnd) rope.m_vidx.back() = -1;
		for (int v = ini; v < end; ++v) // Indices
			rope.m_vidx[v] = idxOffset0++;

		// Non-hook elements
		for (int e = ini; e < end - 1; ++e)
		{
			RodEdgeElement3D* pEle = new RodEdgeElement3D(3);

			iVector vinds0;
			vinds0.push_back(rope.m_vidx[e]);
			vinds0.push_back(rope.m_vidx[e + 1]);

			iVector vinds = vinds0;
			pEle->setNodeIndices(vinds);
			pEle->setNodeIndices0(vinds0);

			// Add element 
			this->m_vpEles.push_back(pEle);
			this->m_vpRopeStretchEles.push_back(pEle);
		}

		// Hook elements

		if (rope.m_embeddedIni && rope.m_embeddedEnd && (int)rope.m_vnodes.size() == 2)
		{
			EmbeddedRodStretchElement3D* pEle = new EmbeddedRodStretchElement3D(3);

			dVector vbars;
			iVector vinds0;
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(rope.m_ini.m_vidx[ii]);
				vbars.push_back(rope.m_ini.m_vwei[ii]);
			}
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(rope.m_end.m_vidx[ii]);
				vbars.push_back(rope.m_end.m_vwei[ii]);
			}

			iVector vinds = vinds0;
			pEle->setNodeIndices(vinds);
			pEle->setNodeIndices0(vinds0);
			pEle->setEmbCoordinates(vbars);

			// Add element  
			this->m_vpEles.push_back(pEle);
			this->m_vpRopeDoubleHooks.push_back(pEle);
			rope.m_vdhookEles.push_back((int)m_vpRopeDoubleHooks.size() - 1);
		}
		else
		{
			if (rope.m_embeddedIni)
			{
				EmbeddedPartialRodStretchElement3D* pEle = new EmbeddedPartialRodStretchElement3D(3, 3);

				dVector vbars;
				iVector vinds0;
				EmbeddedPoint& eini = rope.m_ini;
				for (int ii = 0; ii < 3; ++ii)
				{
					vinds0.push_back(eini.m_vidx[ii]);
					vbars.push_back(eini.m_vwei[ii]);
				}
				vinds0.push_back(rope.m_vidx[1]);

				vbars.push_back(1.0);

				iVector vinds = vinds0;
				pEle->setNodeIndices(vinds);
				pEle->setNodeIndices0(vinds0);
				pEle->setEmbCoordinates(vbars);

				// Add element 
				this->m_vpEles.push_back(pEle);
				this->m_vpRopeHooks.push_back(pEle);
				rope.m_vhookEles.push_back((int)m_vpRopeHooks.size() - 1);
			}

			if (rope.m_embeddedEnd)
			{
				EmbeddedPartialRodStretchElement3D* pEle = new EmbeddedPartialRodStretchElement3D(3, 3);

				dVector vbars;
				iVector vinds0;
				EmbeddedPoint& eend = rope.m_end;
				for (int ii = 0; ii < 3; ++ii)
				{
					vinds0.push_back(eend.m_vidx[ii]);
					vbars.push_back(eend.m_vwei[ii]);
				}
				vinds0.push_back(rope.m_vidx[rope.m_vidx.size() - 2]);

				vbars.push_back(1.0);

				iVector vinds = vinds0;
				pEle->setNodeIndices(vinds);
				pEle->setNodeIndices0(vinds0);
				pEle->setEmbCoordinates(vbars);

				// Add element 
				this->m_vpEles.push_back(pEle);
				this->m_vpRopeHooks.push_back(pEle);
				rope.m_vhookEles.push_back((int)m_vpRopeHooks.size() - 1);
			}
		}
	}

	this->m_numEle = (int) this->m_vpEles.size();

	this->m_numMeshNodes_x = nv;
	this->m_numMeshNodes_X = nv0;

	// Initialize rest state vector 

	this->m_numDim0 = 3;
	this->m_numNodeSim0 = idxOffset0;
	this->m_nrawDOF0 = 3 * idxOffset0;

	this->m_numDim = 3;
	this->m_numNodeSim = idxOffset0;
	this->m_nrawDOF = 3 * idxOffset0;

	dVector vXmPat;
	dVector vxmPat;
	dVector vxmSin(3 * nv);
	this->m_pSimMesh->getPoints(vXmPat, VPropSim::POS0);
	this->m_pSimMesh->getPoints(vxmPat, VPropSim::POSE);
	applyIndexMap(m_vmapPatches2Single, vxmPat, 3, vxmSin);

	dVector vx(this->m_nrawDOF);
	dVector vX(this->m_nrawDOF0);
	setSubvector(0, 3 * m_numMeshNodes_X, vxmSin, vx);
	setSubvector(0, 3 * m_numMeshNodes_x, vXmPat, vX);
	this->getRopesPositions(vx);
	this->getRopesPositions(vX);
	this->m_vX = toEigen(vX);
	this->m_vx = toEigen(vx);

	this->m_vv.resize(this->m_nrawDOF);
	this->m_vv.setZero(); // From zero

	this->m_vrodsDesignPos = vxmPat;
}

void TensileStructureModel::initializeElementsPatchesEmbedded2D()
{
	this->freeElements();

	int nf = (int) this->m_pSimMesh->n_faces();
	int ne0 = (int) this->m_pSimMesh->n_edges();
	int nv0 = (int) this->m_pSimMesh->n_vertices();
	int nv = (int) this->m_vmapSingle2Patches.size();

	this->m_numEle = nf; // Area elements
	this->m_numEle += ne0; // Length elements
	this->m_numEle += ne0; // Hinge elements

	int nr = (int) this->m_vstructNodes.size();

	for (int r = 0; r < nr; ++r)
	{
		int nre = (int) this->m_vstructNodes[r].size();

		if (this->m_vstructLoops[r])
			this->m_numEle += nre - 1;
		else this->m_numEle += nre;
		this->m_numEle += nre;
	}

	this->m_vpEles.reserve(this->m_numEle); // Estimation

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());

	// Initialize area preserving elements

	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		DSAreaElement* pEle = new DSAreaElement(2);

		// Set indices
		vector<int> vinds0(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds0[v] = (*fv_it).idx();
			++fv_it;
		}
		pEle->setNodeIndices0(vinds0);

		vector<int> vinds;
		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
		pEle->setNodeIndices(vinds); // Map global simulation indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSAreaEles.push_back(pEle);
	}

	// Initialize stretch mass-spring elements

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		TriMesh::EdgeHandle eh = *e_it;

		DSEdgeElement* pEle = new DSEdgeElement(2);

		// Set indices
		vector<int> vinds0(2);
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);

		vinds0[0] = m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx();
		vinds0[1] = m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx();
		pEle->setNodeIndices0(vinds0);

		vector<int> vinds;
		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
		pEle->setNodeIndices(vinds); // Map global simulation indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSEdgeEles.push_back(pEle);
	}

	// Initialize discrete-shells bending elements 

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		TriMesh::EdgeHandle eh = *e_it;

		if (this->m_pSimMesh->is_boundary(eh))
			continue; // Ignore boundaries

		DSHingeElement* pEle = new DSHingeElement(2);

		// Set indices
		vector<int> vinds0(4);
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);

		for (int i = 0; i < 4; i++)
			vinds0[i] = m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx();
		pEle->setNodeIndices0(vinds0);

		vector<int> vinds;
		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
		pEle->setNodeIndices(vinds); // Map global simulation indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSBendingEles.push_back(pEle);
	}

	//// Initialize rod edge elements

	//for (int i = 0; i < nr; ++i)
	//{
	//	int nrv = (int) this->m_vstructNodes[i].size();
	//	int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
	//	for (int j = 0; j < end; ++j)
	//	{
	//		int i0 = j; // Loop?
	//		int i1 = (j + 1) % nrv;

	//		EmbeddedRodStretchElement3D* pEle = new EmbeddedRodStretchElement3D(2);

	//		dVector vbars;
	//		iVector vinds;
	//		iVector vinds0;
	//		for (int ii = 0; ii < 3; ++ii)
	//		{
	//			vinds0.push_back(this->m_vstructNodes[i][i0].m_vidx[ii]);
	//			vbars.push_back(this->m_vstructNodes[i][i0].m_vwei[ii]);
	//		}
	//		for (int ii = 0; ii < 3; ++ii)
	//		{
	//			vinds0.push_back(this->m_vstructNodes[i][i1].m_vidx[ii]);
	//			vbars.push_back(this->m_vstructNodes[i][i1].m_vwei[ii]);
	//		}

	//		pEle->setEmbCoordinates(vbars);
	//		pEle->setNodeIndices0(vinds0);

	//		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
	//		pEle->setNodeIndices(vinds); // Map global simulation indices

	//		// Add element
	//		this->m_vpEles.push_back(pEle);
	//		this->m_vpRodStretchEles.push_back(pEle);
	//	}
	//}

	//// Initialize rod angle elements

	//for (int i = 0; i < nr; ++i)
	//{
	//	int ini = (this->m_vstructLoops[i]) ? 0 : 1;
	//	int nrv = (int) this->m_vstructNodes[i].size();
	//	int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
	//	for (int j = ini; j < end; ++j)
	//	{
	//		int i1 = j;
	//		int i0 = (j - 1) % nrv;
	//		int i2 = (j + 1) % nrv;
	//		if (i0 < 0) i0 += nrv;

	//		EmbeddedRodBendElement3D* pEle = new EmbeddedRodBendElement3D(2);

	//		dVector vbars;
	//		iVector vinds;
	//		iVector vinds0;
	//		for (int ii = 0; ii < 3; ++ii)
	//		{
	//			vinds0.push_back(this->m_vstructNodes[i][i0].m_vidx[ii]);
	//			vbars.push_back(this->m_vstructNodes[i][i0].m_vwei[ii]);
	//		}
	//		for (int ii = 0; ii < 3; ++ii)
	//		{
	//			vinds0.push_back(this->m_vstructNodes[i][i1].m_vidx[ii]);
	//			vbars.push_back(this->m_vstructNodes[i][i1].m_vwei[ii]);
	//		}
	//		for (int ii = 0; ii < 3; ++ii)
	//		{
	//			vinds0.push_back(this->m_vstructNodes[i][i2].m_vidx[ii]);
	//			vbars.push_back(this->m_vstructNodes[i][i2].m_vwei[ii]);
	//		}

	//		pEle->setEmbCoordinates(vbars);
	//		pEle->setNodeIndices0(vinds0);

	//		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
	//		pEle->setNodeIndices(vinds); // Map global simulation indices

	//		// Add element 
	//		this->m_vpEles.push_back(pEle);
	//		this->m_vpRodBendingEles.push_back(pEle);
	//	}
	//}

	this->m_numEle = (int) this->m_vpEles.size();

	// Initialize rest state vector 

	this->m_numDim0 = 2;
	this->m_numNodeSim0 = nv0;
	this->m_nrawDOF0 = 2 * nv0;

	this->m_numDim = 3;
	this->m_numNodeSim = nv;
	this->m_nrawDOF = 3 * nv;

	dVector vX3D;
	this->m_pSimMesh->getPoints(vX3D, VPropSim::POS0);

	dVector vX2D;
	to2D(vX3D, vX2D);
	this->m_vX = toEigen(vX2D);

	dVector vxPatches;
	dVector vxSingle(3 * nv);
	this->m_pSimMesh->getPoints(vxPatches, VPropSim::POSE);
	applyIndexMap(m_vmapPatches2Single, vxPatches, 3, vxSingle);
	this->m_vx = toEigen(vxSingle); // Transform it to Eigen

	this->m_vv.resize(this->m_nrawDOF);
	this->m_vv.setZero(); // From zero

	this->m_vrodsDesignPos = vxPatches;
}

void TensileStructureModel::initializeElementsPatchesEmbedded3D_10()
{
	this->freeElements();

	int nf = (int) this->m_pSimMesh->n_faces();
	int ne0 = (int) this->m_pSimMesh->n_edges();
	int nv0 = (int) this->m_pSimMesh->n_vertices();
	int nv = (int) this->m_vmapSingle2Patches.size();

	this->m_numEle = nf; // Area elements
	this->m_numEle += ne0; // Length elements
	this->m_numEle += ne0; // Hinge elements

	this->m_nr = (int) this->m_vstructNodes.size();

	for (int r = 0; r < m_nr; ++r)
	{
		int nre = (int) this->m_vstructNodes[r].size();

		if (this->m_vstructLoops[r])
			this->m_numEle += 2 * (nre - 1);
		else this->m_numEle += 2 * nre;
		this->m_numEle += nre;
	}

	this->m_vpEles.reserve(this->m_numEle); // Estimation

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());

	// Initialize area preserving elements

	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		DSAreaElement* pEle = new DSAreaElement(3);

		// Set indices
		vector<int> vinds0(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds0[v] = (*fv_it).idx();
			++fv_it;
		}
		pEle->setNodeIndices0(vinds0);

		vector<int> vinds;
		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
		pEle->setNodeIndices(vinds); // Map global simulation indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSAreaEles.push_back(pEle);
	}

	// Initialize stretch mass-spring elements

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		TriMesh::EdgeHandle eh = *e_it;

		DSEdgeElement* pEle = new DSEdgeElement(3);

		// Set indices
		vector<int> vinds0(2);
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);

		vinds0[0] = m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx();
		vinds0[1] = m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx();
		pEle->setNodeIndices0(vinds0);

		vector<int> vinds;
		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
		pEle->setNodeIndices(vinds); // Map global simulation indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSEdgeEles.push_back(pEle);
	}

	// Initialize discrete-shells bending elements 

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		TriMesh::EdgeHandle eh = *e_it;

		if (this->m_pSimMesh->is_boundary(eh))
			continue; // Ignore boundaries

		DSHingeElement* pEle = new DSHingeElement(3);

		// Set indices
		vector<int> vinds0(4);
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);

		for (int i = 0; i < 4; i++)
			vinds0[i] = m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx();
		pEle->setNodeIndices0(vinds0);

		vector<int> vinds;
		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
		pEle->setNodeIndices(vinds); // Map global simulation indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpDSBendingEles.push_back(pEle);
	}

	// Initialize rod stretch elements

	int dofOffset = 3 * nv;

	for (int i = 0; i < this->m_nr; ++i)
	{
		int nrv = (int) this->m_vstructNodes[i].size();
		int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
		for (int j = 0; j < end; ++j)
		{
			int i0 = j; // Loop?
			int i1 = (j + 1) % nrv;

			EmbeddedRodStretchElement3D* pEle = new EmbeddedRodStretchElement3D(3);

			dVector vbars;
			iVector vinds;
			iVector vinds0;
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(this->m_vstructNodes[i][i0].m_vidx[ii]);
				vbars.push_back(this->m_vstructNodes[i][i0].m_vwei[ii]);
			}
			for (int ii = 0; ii < 3; ++ii)
			{
				vinds0.push_back(this->m_vstructNodes[i][i1].m_vidx[ii]);
				vbars.push_back(this->m_vstructNodes[i][i1].m_vwei[ii]);
			}

			pEle->setEdgeIndex(dofOffset++);

			pEle->setEmbCoordinates(vbars);
			pEle->setNodeIndices0(vinds0);

			transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
			pEle->setNodeIndices(vinds); // Map global simulation indices

			// Add element
			this->m_vpEles.push_back(pEle);
			this->m_vpRodStretchEles.push_back(pEle);
		}
	}

	this->m_nvDOF = 3 * nv;
	this->m_naDOF = 0;
	this->m_vrodCenter.resize(this->m_nr);
	this->m_vrodAngle.resize(this->m_nr);
	this->m_vrodTwist.resize(this->m_nr);
	this->m_vrefFrames.resize(this->m_nr);
	this->m_vmatFrames.resize(this->m_nr);
	for (int i = 0; i < this->m_nr; ++i)
	{
		int nv = (int) this->m_vstructNodes[i].size();
		bool isClosed = this->m_vstructLoops[i];

		dVector vx(3 * nv, 0.0);
		m_vrodCenter[i].initialize(vx, isClosed);
		int ne = m_vrodCenter[i].getEdgeNumber();
		this->m_vrodAngle[i].resize(ne, 0.0);
		this->m_vrodTwist[i].resize(nv, 0.0);
		this->m_vrefFrames[i].resize(ne);
		this->m_vmatFrames[i].resize(ne);
		this->m_naDOF += ne;
	}

	//// Initialize rod elements

	//dofOffset = 3*nv;

	//this->m_vpRodBendingEles.resize(this->m_nr);
	//this->m_vpRodTwistEles.resize(this->m_nr);

	//for (int i = 0; i < this->m_nr; ++i)
	//{
	//	Curve& curve = m_vrodCenter[i];
	//	bool isClosed = curve.getIsClosed();
	//	int nrv = curve.getNodeNumber();
	//	int nre = curve.getEdgeNumber();

	//	int ini = isClosed? 0 : 1;
	//	int end = isClosed? nrv : nrv - 1;
	//	for (int j = ini; j < end; ++j)
	//	{
	//		int pv, nv;
	//		int pe, ne;
	//		curve.getVertexSurroundingEdges(j, pe, ne);
	//		curve.getVertexSurroundingVertices(j, pv, nv);

	//		int i0 = pv, i1 = j, i2 = nv;

	//		iVector vidx;
	//		vidx.push_back(i0);
	//		vidx.push_back(i1);
	//		vidx.push_back(i2);

	//		iVector eidx;
	//		eidx.push_back(pe);
	//		eidx.push_back(ne);

	//		EmbeddedDERRodBendElement* pBendEle = new EmbeddedDERRodBendElement();
	//		EmbeddedDERRodTwistElement* pTwistEle = new EmbeddedDERRodTwistElement();

	//		dVector vbars;
	//		iVector vnodeinds;
	//		iVector vnodeinds0;
	//		for (int ii = 0; ii < 3; ++ii)
	//		{
	//			vnodeinds0.push_back(this->m_vstructNodes[i][i0].m_vidx[ii]);
	//			vbars.push_back(this->m_vstructNodes[i][i0].m_vwei[ii]);
	//		}
	//		for (int ii = 0; ii < 3; ++ii)
	//		{
	//			vnodeinds0.push_back(this->m_vstructNodes[i][i1].m_vidx[ii]);
	//			vbars.push_back(this->m_vstructNodes[i][i1].m_vwei[ii]);
	//		}
	//		for (int ii = 0; ii < 3; ++ii)
	//		{
	//			vnodeinds0.push_back(this->m_vstructNodes[i][i2].m_vidx[ii]);
	//			vbars.push_back(this->m_vstructNodes[i][i2].m_vwei[ii]);
	//		}

	//		transformIndex(this->m_vmapPatches2Single, vnodeinds0, 0, vnodeinds);

	//		iVector vinds;
	//		iVector vinds0;
	//		for (int k0 = 0; k0 < 9; ++k0)
	//		{
	//			for (int k1 = 0; k1 < 3; ++k1)
	//			{
	//				vinds.push_back(3*vnodeinds[k0] + k1);
	//				vinds0.push_back(3*vnodeinds0[k0] + k1);
	//			}
	//		}

	//		pTwistEle->setEmbCoordinates(vbars);
	//		pBendEle->setEmbCoordinates(vbars);

	//		vinds0.push_back(dofOffset + pe);
	//		vinds0.push_back(dofOffset + ne);
	//		vinds.push_back(dofOffset + pe);
	//		vinds.push_back(dofOffset + ne);
	//	
	//		pTwistEle->setIndices0(vinds0);
	//		pBendEle->setIndices0(vinds0);
	//		pTwistEle->setIndices(vinds);
	//		pBendEle->setIndices(vinds);

	//		pBendEle->setVIndices(vidx);
	//		pBendEle->setEIndices(eidx);
	//		pTwistEle->setVIndices(vidx);
	//		pTwistEle->setEIndices(eidx);

	//		// Add element to the element list of the rod
	//		this->m_vpRodBendingEles[i].push_back(pBendEle);
	//		this->m_vpRodTwistEles[i].push_back(pTwistEle);
	//		this->m_vpEles.push_back(pBendEle);
	//		this->m_vpEles.push_back(pTwistEle);
	//	}

	//	dofOffset += nre;
	//}

	this->m_numEle = (int) this->m_vpEles.size();

	// Initialize rest state vector 

	this->m_numDim0 = 3;
	this->m_numNodeSim0 = nv0;
	this->m_nrawDOF0 = 3 * nv0 + this->m_naDOF;

	this->m_numDim = 3;
	this->m_numNodeSim = nv;
	this->m_nrawDOF = 3 * nv + this->m_naDOF;

	dVector vXnodes(3 * nv0), vxnodesP(3 * nv0), vxnodes(3 * nv);
	this->m_pSimMesh->getPoints(vXnodes, VPropSim::POS0);
	this->m_pSimMesh->getPoints(vxnodesP, VPropSim::POSE);
	applyIndexMap(m_vmapPatches2Single, vxnodesP, 3, vxnodes);

	dVector vX(this->m_nrawDOF0, 0.0);
	setSubvector(0, 3 * nv0, vXnodes, vX);
	this->m_vX = toEigen(vX); // Eigen

	dVector vx(this->m_nrawDOF, 0.0);
	setSubvector(0, 3 * nv, vxnodes, vx);
	this->m_vx = toEigen(vx); // Eigen

	this->m_vv.resize(this->m_nrawDOF);
	this->m_vv.setZero(); // From zero

	this->m_vrodsDesignPos = vxnodesP;
}


void TensileStructureModel::updateRodsCenter()
{
	for (int i = 0; i < this->m_nr; ++i)
	{
		Curve& curve = this->m_vrodCenter[i];

		int nv = curve.getNodeNumber();
		dVector vx(3 * nv); // Update vertex
		for (int j = 0; j < nv; ++j)
		{
			EmbeddedPoint& ep = this->m_vstructNodes[i][j];
			Vector3d p = ep.m_vwei[0] * getBlock3x1(ep.m_vidx[0], this->m_vx) +
				ep.m_vwei[1] * getBlock3x1(ep.m_vidx[1], this->m_vx) +
				ep.m_vwei[2] * getBlock3x1(ep.m_vidx[2], this->m_vx);
			set3D(j, p, vx);
		}

		curve.setPositions(vx);
	}
}

void TensileStructureModel::updateRodsAngles()
{
	int count = this->m_nvDOF; // Vertices first

	for (int i = 0; i < this->m_nr; ++i)
	{
		Curve& curve = this->m_vrodCenter[i];

		int ne = curve.getEdgeNumber();
		for (int j = 0; j < ne; ++j) // Update angles
			this->m_vrodAngle[i][j] = this->m_vx[count++];
	}

	assert(count == this->m_nrawDOF);
}

void TensileStructureModel::updateRodsReferenceFrame()
{
	for (int i = 0; i < this->m_nr; ++i)
	{
		Curve& curve = this->m_vrodCenter[i];

		// Parallel transport 
		int ne = curve.getEdgeNumber();
		for (int j = 0; j < ne; j++)
		{
			Frame oldFrame = this->m_vrefFrames[i][j];

			Vector3d e0 = oldFrame.tan;
			Vector3d e1 = curve.getEdge(j).normalized();
			this->m_vrefFrames[i][j] = framePT(oldFrame, e0, e1);
		}
	}
}

void TensileStructureModel::updateRodsReferenceTwist()
{
	for (int i = 0; i < this->m_nr; ++i)
	{
		Curve& curve = this->m_vrodCenter[i];

		// Parallel transport 
		int nv = curve.getNodeNumber();
		for (int j = 0; j < nv; j++)
		{
			if (j == 0 || j == nv - 1)
			{
				if (!curve.getIsClosed())
				{
					this->m_vrodTwist[i][j] = 0.0;
					continue; // No extreme twist
				}
			}

			int pe, ne;
			curve.getVertexSurroundingEdges(j, pe, ne);
			Frame F0 = this->m_vrefFrames[i][pe];
			Frame F1 = this->m_vrefFrames[i][ne];
			Frame F0t = framePT(F0, F0.tan, F1.tan);
			this->m_vrodTwist[i][j] = signedAngle(F0t.bin, F1.bin, F1.tan, this->m_vrodTwist[i][j]);
		}
	}
}

void TensileStructureModel::updateRodsMaterialFrame()
{
	for (int i = 0; i < this->m_nr; ++i)
	{
		Curve& curve = this->m_vrodCenter[i];

		// Parallel transport 
		int ne = curve.getEdgeNumber();
		for (int j = 0; j < ne; j++)
			this->m_vmatFrames[i][j] = frameRotation(this->m_vrefFrames[i][j], this->m_vrodAngle[i][j]);
	}
}


//void TensileStructureModel::initializeElementsPatchesEmbedded3D()
//{
//	this->freeElements();
//
//	int nf = (int) this->m_pSimMesh->n_faces();
//	int ne0 = (int) this->m_pSimMesh->n_edges();
//	int nv0 = (int) this->m_pSimMesh->n_vertices();
//	int nv = (int) this->m_vmapSingle2Patches.size();
//
//	this->m_numEle = nf; // Area elements
//	this->m_numEle += ne0; // Length elements
//	this->m_numEle += ne0; // Hinge elements
//
//	int nr = (int) this->m_vstructVerts.size();
//
//	for (int r = 0; r < nr; ++r)
//	{
//		int nre = (int) this->m_vstructEdges[r].size();
//
//		if (this->m_vstructLoops[r])
//			this->m_numEle += nre - 1;
//		else this->m_numEle += nre;
//		this->m_numEle += nre;
//	}
//
//	this->m_vpEles.reserve(this->m_numEle); // Estimation
//
//	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
//	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());
//
//	// Initialize area preserving elements
//
//	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
//	{
//		DSAreaElement* pEle = new DSAreaElement(3);
//
//		// Set indices
//		vector<int> vinds0(3);
//		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
//		for (int v = 0; v < 3; ++v)
//		{
//			vinds0[v] = (*fv_it).idx();
//			++fv_it;
//		}
//		pEle->setNodeIndices0(vinds0);
//
//		vector<int> vinds;
//		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
//		pEle->setNodeIndices(vinds); // Map global simulation indices
//
//		// Add element
//		this->m_vpEles.push_back(pEle);
//		this->m_vpDSAreaEles.push_back(pEle);
//	}
//
//	// Initialize stretch mass-spring elements
//
//	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
//	{
//		TriMesh::EdgeHandle eh = *e_it;
//
//		DSEdgeElement* pEle = new DSEdgeElement(3);
//
//		// Set indices
//		vector<int> vinds0(2);
//		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
//		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
//		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
//
//		vinds0[0] = m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx();
//		vinds0[1] = m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx();
//		pEle->setNodeIndices0(vinds0);
//
//		vector<int> vinds;
//		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
//		pEle->setNodeIndices(vinds); // Map global simulation indices
//
//		// Add element
//		this->m_vpEles.push_back(pEle);
//		this->m_vpDSEdgeEles.push_back(pEle);
//	}
//
//	// Initialize discrete-shells bending elements 
//
//	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
//	{
//		TriMesh::EdgeHandle eh = *e_it;
//
//		if (this->m_pSimMesh->is_boundary(eh))
//			continue; // Ignore boundaries
//
//		DSHingeElement* pEle = new DSHingeElement(3);
//
//		// Set indices
//		vector<int> vinds0(4);
//		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
//		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
//		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
//		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
//		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);
//
//		for (int i = 0; i < 4; i++)
//			vinds0[i] = m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx();
//		pEle->setNodeIndices0(vinds0);
//
//		vector<int> vinds;
//		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
//		pEle->setNodeIndices(vinds); // Map global simulation indices
//
//		// Add element
//		this->m_vpEles.push_back(pEle);
//		this->m_vpDSBendingEles.push_back(pEle);
//	}
//
//	//// Initialize rod edge elements
//
//	//for (int i = 0; i < nr; ++i)
//	//{
//	//	int nrv = (int) this->m_vstructVerts[i].size();
//	//	int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
//	//	for (int j = 0; j < end; ++j)
//	//	{
//	//		int i0 = j; // Loop?
//	//		int i1 = (j + 1) % nrv;
//
//	//		RodEdgeElement3D* pEle = new RodEdgeElement3D(3);
//	//		vector<int> vinds0(2);
//	//		vinds0[0] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i0]).idx();
//	//		vinds0[1] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i1]).idx();
//	//		pEle->setNodeIndices0(vinds0);
//
//	//		vector<int> vinds;
//	//		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
//	//		pEle->setNodeIndices(vinds); // Map global simulation indices
//
//	//		// Add element
//	//		this->m_vpEles.push_back(pEle);
//	//		this->m_vpRodStretchEles.push_back(pEle);
//	//	}
//	//}
//
//	//// Initialize rod angle elements
//
//	//for (int i = 0; i < nr; ++i)
//	//{
//	//	int ini = (this->m_vstructLoops[i]) ? 0 : 1;
//	//	int nrv = (int) this->m_vstructVerts[i].size();
//	//	int end = (this->m_vstructLoops[i]) ? nrv : nrv - 1;
//	//	for (int j = ini; j < end; ++j)
//	//	{
//	//		int i1 = j;
//	//		int i0 = (j - 1) % nrv;
//	//		int i2 = (j + 1) % nrv;
//	//		if (i0 < 0) i0 += nrv;
//
//	//		RodAngleElement3D* pEle = new RodAngleElement3D(3);
//	//		vector<int> vinds0(3);
//	//		vinds0[0] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i0]).idx();
//	//		vinds0[1] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i1]).idx();
//	//		vinds0[2] = this->m_pSimMesh->getVertHandle(this->m_vstructVerts[i][i2]).idx();
//	//		pEle->setNodeIndices0(vinds0);
//
//	//		vector<int> vinds;
//	//		transformIndex(this->m_vmapPatches2Single, vinds0, 0, vinds);
//	//		pEle->setNodeIndices(vinds); // Map global simulation indices
//
//	//		// Add element 
//	//		this->m_vpEles.push_back(pEle);
//	//		this->m_vpRodBendingEles.push_back(pEle);
//	//	}
//	//}
//
//	this->m_numEle = (int) this->m_vpEles.size();
//
//	// Initialize rest state vector 
//
//	this->m_numDim0 = 3;
//	this->m_numNodeSim0 = nv0;
//	this->m_nrawDOF0 = 3 * nv0;
//
//	this->m_numDim = 3;
//	this->m_numNodeSim = nv;
//	this->m_nrawDOF = 3 * nv;
//
//	dVector vX;
//	this->m_pSimMesh->getPoints(vX, VPropSim::POS0);
//	this->m_vX = toEigen(vX); // Transform it to Eigen
//
//	dVector vxPatches;
//	dVector vxSingle(3 * nv);
//	this->m_pSimMesh->getPoints(vxPatches, VPropSim::POSE);
//	applyIndexMap(m_vmapPatches2Single, vxPatches, 3, vxSingle);
//	this->m_vx = toEigen(vxSingle); // Transform it to Eigen
//
//	this->m_vv.resize(this->m_nrawDOF);
//	this->m_vv.setZero(); // From zero
//}