/*=====================================================================================*/
/*! 
\file		FEMElementLinear.cpp
\author		jesusprod
\brief		Implementation of FEMElementLinear.h
 */
/*=====================================================================================*/

#include <JSandbox/FEMElementLinear.h>

Real FEMElementLinear::computeEnergyDensity(const MatrixXd& mF) const
{
	assert(mF.rows() == this->m_numDim);
	assert(mF.cols() == this->m_numDim);

	//	Psi(e) = m*(e:e) + 0.5*l*tr^2(e)
	//	where e:e is the Frobenious product
	//	Sum_i Sum_j {e_ij*e_ij} = tr(e^T*e)

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	

	MatrixXd me; // Cauchy strain
	computeCauchyStrain(mF, me);

	double tre = me.trace();
	double treTe = (me.transpose()*me).trace();
	return treTe*lame1 + 0.5*lame2*tre*tre;
}

void FEMElementLinear::computePiolaKirchhoffFirst(const MatrixXd& mF, MatrixXd& mP) const
{
	assert(mF.rows() == this->m_numDim);
	assert(mF.cols() == this->m_numDim);

	// P(e) = 2*m*e + l*tr(e)*I => P(F) = m*(F + F^T - 2I) + l*tr(F - I)*I

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	

	MatrixXd me; // Cauchy strain
	computeCauchyStrain(mF, me);

	mP = me*2*lame1 + MatrixXd::Identity(m_numDim, m_numDim)*lame2*me.trace();
}

void FEMElementLinear::computePiolaKirchhoffFirstDerivative(const MatrixXd& mF, const vector<MatrixXd>& vDFDx, vector<MatrixXd>& vDPDx) const
{
	assert(mF.rows() == this->m_numDim);
	assert(mF.cols() == this->m_numDim);
	assert((int) vDFDx.size() == this->m_nrawDOF);

	MatrixXd mI = MatrixXd::Identity(this->m_numDim, this->m_numDim);

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	
	
	vDPDx.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_nrawDOF; ++i) // Component derivatives: nv * nd
		vDPDx[i] = lame1*(vDFDx[i] + vDFDx[i].transpose()) + mI*lame2*vDFDx[i].trace();
}