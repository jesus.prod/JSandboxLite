/*=====================================================================================*/
/*!
\file		RodEdgeElemente3D.h
\author		jesusprod
\brief		Rod length preserving energy in 3D. It uses SolidMaterial stretch K.
*/
/*=====================================================================================*/

#ifndef ROD_EDGE_ELEMENT_3D_H
#define ROD_EDGE_ELEMENT_3D_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>

class RodEdgeElement3D : public NodalSolidElement
{
public:
	RodEdgeElement3D(int dim0);
	~RodEdgeElement3D();

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getIntegrationVolume() const { return this->m_L0; }

	virtual Real getRestLength() const { return this->m_L0; }
	virtual void setRestLength(Real L0) { this->m_L0 = L0; }

	virtual double computeB() const;

protected:
	Real m_L0; // Rest length
};

#endif