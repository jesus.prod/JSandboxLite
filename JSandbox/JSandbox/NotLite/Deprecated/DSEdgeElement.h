/*=====================================================================================*/
/*! 
\file		DSEdgeElement.h
\author		jesusprod,bthomasz
\brief		Basic implementation of a discrete-shell edge element. Adapted from bthomasz
			implementation for Phys3D DeformablesSurface plugin. This provides a simple
			non-linear energy density for edge length preserving. It uses SolidMaterial
			stretch K.
*/
/*=====================================================================================*/

#ifndef DS_EDGE_ELEMENT_H
#define DS_EDGE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>

class DSEdgeElement : public NodalSolidElement
{
public:
	DSEdgeElement(int dim0);
	~DSEdgeElement();

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getIntegrationVolume() const { return this->m_L0; }

	virtual Real getRestLength() const { return this->m_L0; }

protected:
	Real m_L0; // Rest length
};

#endif