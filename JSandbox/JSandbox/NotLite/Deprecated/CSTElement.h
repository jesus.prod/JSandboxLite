/*=====================================================================================*/
/*! 
\file		CSTElement.h
\author		jesusprod,bthomasz
\brief		Basic implementation of Constant Strain Triangle element. Adapted from bthomasz
			implementation for Phys3D DeformablesSurface plugin. This provides a continuous
			based strain-stress response for diverse models. These use Lame coefficients as 
			parameters.
*/
/*=====================================================================================*/

// TODO: This class is still not working, do not use it until this message is removed

#ifndef CST_ELEMENT_H
#define CST_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>
 
typedef Real DNDx[3][2];

class CSTElement : public NodalSolidElement
{
public:
	CSTElement(int d0);
	virtual ~CSTElement();

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getRestArea() const { return this->m_A0; }

	virtual Real compute_Area_2D(const vector<Vector2d>& vx) const;
	virtual Real compute_Area_3D(const vector<Vector3d>& vx) const;
	virtual void compute_DNDx_2D(const vector<Vector2d>& vx, DNDx& DNDx) const;
	virtual void compute_DNDx_3D(const vector<Vector3d>& vx, DNDx& DNDx) const;
	
	virtual Real getIntegrationVolume() const { return this->m_A0; }

	virtual Real getThickness() const { return this->m_t0; }
	virtual void setThickness(Real t0) { this->m_t0 = t0; }

protected:	

	Real m_A0;		// Rest area	
	Real m_t0;		// Thinckness

	DNDx m_dNdx;	// DN/Dx

};

#endif