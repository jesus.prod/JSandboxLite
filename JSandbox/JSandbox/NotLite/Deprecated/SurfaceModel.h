/*=====================================================================================*/
/*! 
\file		SurfaceModel.h
\author		jesusprod
\brief		Custom made model for generic deformable surfaces. It provides the necessary 
			functions to create a discretization using different surface elements. 
			The current implementation is restricted to triangle meshes.
 */
/*=====================================================================================*/

#ifndef SURFACE_MODEL_H
#define SURFACE_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/Geometry/TriMesh.h>

#include <JSandbox/NodalSolidModel.h>
#include <JSandbox/Geometry/SurfaceRemesher.h>

// Interdependency
class SurfaceRemesher;

class JSANDBOX_EXPORT SurfaceModel : public NodalSolidModel
{
public:

	// Discretization element
	
	enum Element
	{
		Tri1,		// Linear triangle
		Quad2,		// Bilinear quad
	};

	// Deformation model

	enum Model
	{
		QuadArea = 0,					// Simple area preserving energy, no bending or stretch energy
		MS_QuadArea = 1,				// Simple area preserving energy with mass-spring stretch, no bending
		DS_MS_QuadArea = 2,				// Discrete-shells bending + mass-spring stretch + area preserving energy
		DS_FEMLinearCorot = 3,			// Discrete-shells bending + FEM-based linear co-rotational
		DS_FEMMooneyRivlin = 4,			// Discrete-shells bending + FEM-based Mooney Rivlin 
		DS_FEMNeoHookean = 5,			// Discrete-shells bending + FEM-based Neo-Hookean
		DS_FEMOgden = 6,				// Discrete-shells bending + FEM-based Ogden 
		DS_FEMStVK = 7					// Discrete-shells bending + FEM-based StVK
	};

public:
	SurfaceModel();
	virtual ~SurfaceModel();

	virtual void configureDeformationModel(SurfaceModel::Model DM);
	virtual void configureDiscretization(const dVector& vp, const iVector& vi, SurfaceModel::Element DE);

	virtual TriMesh* getSimMesh() { return this->m_pSimMesh; }

	virtual void updateSimMesh();
	
	virtual void updateStatex(const VectorXd& vx, const VectorXd& vv);
	virtual void updateState0(const VectorXd& vX, const VectorXd& vV);

	virtual SurfaceRemesher* getDynamicRemesher(int i) const { assert(i >= 0 && i < (int)this->m_vDynRemesher.size()); return this->m_vDynRemesher[i]; }
	virtual SurfaceRemesher* getStaticRemesher(int i) const { assert(i >= 0 && i < (int)this->m_vStaRemesher.size()); return this->m_vStaRemesher[i]; }
	virtual void addDynamicRemesher(SurfaceRemesher* pRemesher) { assert(pRemesher != NULL); this->m_vDynRemesher.push_back(pRemesher); }
	virtual void addStaticRemesher(SurfaceRemesher* pRemesher){ assert(pRemesher != NULL); this->m_vStaRemesher.push_back(pRemesher); }

protected:
	virtual void preSetupActions();
	virtual void posSetupActions();
	virtual void initializeElements();

private:
	virtual void initializeElements_QuadArea();
	virtual void initializeElements_MS_QuadArea();
	virtual void initializeElements_DS_MS_QuadArea();
	virtual void initializeElements_DS_FEM(); 

protected:
	Model m_DM;
	Element m_DE;

	dVector m_vrawPos;
	iVector m_vrawIdx;
	
	bool m_isMeshReady;
	TriMesh* m_pSimMesh;

	vector<SurfaceRemesher*> m_vStaRemesher; // Remeshers to be called upon initialization
	vector<SurfaceRemesher*> m_vDynRemesher; // Remeshers to be called dynamically whenever

};

#endif