/*=====================================================================================*/
/*! 
\file		SolverGenericDynSta.h
\author		jesusprod
\brief		Generic implementation of a basic solver for both dynamic and static problems.

*/
/*=====================================================================================*/

#ifndef SOLVER_GENERIC_DYNAMIC_STATIC_H
#define SOLVER_GENERIC_DYNAMIC_STATIC_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/PhysSolver.h>
#include <JSandbox/CustomTimer.h>
 
class JSANDBOX_EXPORT SolverGenericDynSta : public PhysSolver
{

public: 
	SolverGenericDynSta(SolidModel *pSM);

	virtual ~SolverGenericDynSta();

	virtual Real getPotential(const VectorXd& vx, const VectorXd& vv);
	virtual void getResidual(const VectorXd& vx, const VectorXd& vv, VectorXd& vrhs);
	virtual void getMatrix(const VectorXd& vx, const VectorXd& vv, SparseMatrixXd& mA);

};

#endif