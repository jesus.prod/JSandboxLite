/*=====================================================================================*/
/*!
\file		BasicSlideBendingElement.cpp
\author		jesusprod
\brief		Implementation of BasicSlideBendingElement.h
*/
/*=====================================================================================*/

#include <JSandbox/BasicSlideBendingElement.h>

BasicSlideBendingElement::BasicSlideBendingElement() : DERSlideElement(1, 1)
{
	// Nothign to do here...
}

BasicSlideBendingElement::~BasicSlideBendingElement()
{
	// Nothing to do here...
}

Real BasicSlideBendingElement::computeEnergy(const Vector3d& x0, const Vector3d& x1, const Vector3d& x2, Real t0, Real t1, Real t2)
{
	double vL0 = 0.5*((t1 - t0)+(t2 - t1));
	double kB = this->computeB(); // Stiffness

//#include "../../Maple/Code/RodAngle3DEnergy.mcg"

	//return t78;

	return -1.0;
}

void BasicSlideBendingElement::computeForce(const Vector3d& x0, const Vector3d& x1, const Vector3d& x2, Real t0, Real t1, Real t2, VectorXd& vfOut)
{
	double vL0 = 0.5*((t1 - t0) + (t2 - t1));
	double kB = this->computeB(); // Stiffness

	double vf9[9];

//#include "../../Maple/Code/RodAngle3DForce.mcg"

	for (int i = 0; i < 9; ++i)
		if (!isfinite(vf9[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	vfOut.resize(9);
	for (int i = 0; i < 9; ++i)
		vfOut(i) = vf9[i]; // Copy
}

void BasicSlideBendingElement::computeJacobian(const Vector3d& x0, const Vector3d& x1, const Vector3d& x2, Real t0, Real t1, Real t2, MatrixXd& mJOut)
{
	double vL0 = 0.5*((t1 - t0) + (t2 - t1));
	double kB = this->computeB(); // Stiffness

	double mJ[9][9];

//#include "../../Maple/Code/RodAngle3DJacobian.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	mJOut.resize(9, 9);
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			mJOut(i, j) = mJ[i][j];
}

Real BasicSlideBendingElement::computeB() const
{
	MatrixXd B(2, 2);

	B.setZero();
	double Y = this->m_vpmat[0]->getYoung();
	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double A = M_PI * w * h;
	B(0, 0) = w * w;
	B(1, 1) = h * h;
	B = (0.25 * Y * A) * B;

	return B.trace();
}