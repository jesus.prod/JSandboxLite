/*=====================================================================================*/
/*!
\file		Curve2DModel.cpp
\author		jesusprod
\brief		Implementation of Curve2DModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Curve2DModel.h>
#include <JSandbox/RodEdgeElement2D.h>
#include <JSandbox/RodAngleElement2D.h>

Curve2DModel::Curve2DModel()
{
	// Nothing to do here

	this->m_pvrestAng = NULL;
	this->m_pvrestAng = NULL;
}

Curve2DModel::~Curve2DModel()
{
	// Nothing to do here
}

void Curve2DModel::configureCurve(const dVector& vpos)
{
	assert((int)vpos.size() >= 2 && (int)vpos.size() % 2 == 0);

	this->m_vrawPos = vpos; // Inititalize raw boundary positions
}

void Curve2DModel::initializeElements()
{
	int N = (int)this->m_vrawPos.size();
	int nv = N / 2;
	this->m_numNodeSim = nv;
	this->m_numEle = 2 * nv;
	this->m_numDim = this->m_numDim0 = 2;
	this->m_nrawDOF = this->m_nrawDOF0 = N;

	this->m_vpEles.reserve(this->m_numEle);

	// Initialize elements

	// Length

	for (int i = 0; i < nv; ++i)
	{
		int i0 = i;
		int i1 = (i + 1)%nv;

		iVector vinds;
		vinds.push_back(i0);
		vinds.push_back(i1);
		RodEdgeElement2D* pEle = new RodEdgeElement2D();
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}

	// Angle

	for (int i = 0; i < nv; ++i)
	{
		int i1 = i;
		int i0 = (i - 1) % nv;
		int i2 = (i + 1) % nv; 
		if (i0 < 0) // At 0 
			i0 = nv + i0;

		iVector vinds;
		vinds.push_back(i0);
		vinds.push_back(i1);
		vinds.push_back(i2);
		RodAngleElement2D* pEle = new RodAngleElement2D();
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}

	// Initialize state vectors

	this->m_vx.resize(this->m_nrawDOF);
	this->m_vv.resize(this->m_nrawDOF);
	this->m_vX.resize(this->m_nrawDOF); 

	for (int i = 0; i < this->m_nrawDOF; ++i)
	{
		this->m_vx(i) = this->m_vrawPos[i];
		this->m_vX(i) = this->m_vrawPos[i]; 
	}
	this->m_vv.setZero(); 
}

void Curve2DModel::updateRest()
{
	NodalSolidModel::updateRest(); // Base

	if (this->m_pvrestLen)
	{
		assert(this->m_numNodeSim == (int) this->m_pvrestAng->size());
		for (int i = 0; i < this->m_numNodeSim; ++i)
			dynamic_cast<RodEdgeElement2D*>(this->m_vpEles[i])->setRestLength(this->m_pvrestLen->at(i));
	} 

	if (this->m_pvrestAng)
	{
		assert(this->m_numNodeSim == (int) this->m_pvrestLen->size());
		for (int i = this->m_numNodeSim; i < this->m_numEle; ++i)
			dynamic_cast<RodAngleElement2D*>(this->m_vpEles[i])->setRestAngle(this->m_pvrestAng->at(i - this->m_numNodeSim));
	}
}  