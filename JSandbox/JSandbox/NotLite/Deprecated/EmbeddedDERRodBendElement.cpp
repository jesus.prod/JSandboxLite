/*=====================================================================================*/
/*!
\file		EmbeddedDERRodBendElement.cpp
\author		jesusprod
\brief		Implementation of EmbeddedDERRodBendElement.h
*/
/*=====================================================================================*/

#include <JSandbox/EmbeddedDERRodBendElement.h>

EmbeddedDERRodBendElement::EmbeddedDERRodBendElement() : SolidElement(29, 29)
{
	this->m_DeDv.resize(8, 11);
	this->m_DvDq.resize(11, 29);

	this->m_DeDv.setZero();
	this->m_DvDq.setZero();

	this->m_DeDv(3, 9) = 1.0;
	this->m_DeDv(7, 10) = 1.0;

	Matrix3d I;
	I.setIdentity();
	addBlock3x3From(0, 0, I * -1.0, this->m_DeDv);
	addBlock3x3From(4, 3, I * -1.0, this->m_DeDv);
	addBlock3x3From(0, 3, I, this->m_DeDv);
	addBlock3x3From(4, 6, I, this->m_DeDv);
}

EmbeddedDERRodBendElement::~EmbeddedDERRodBendElement(void)
{
	// Nothing to do here...
}

const dVector& EmbeddedDERRodBendElement::getEmbCoordinates() const
{
	return this->m_vembCor;
}

void EmbeddedDERRodBendElement::setEmbCoordinates(const dVector& vi)
{
	this->m_vembCor = vi;

	this->m_DvDq.resize(11, 29);
	this->m_DvDq.setZero();

	int count = 0;

	for (int i = 0; i < 3; ++i)
	{
		int offseti = 3 * i;

		for (int j = 0; j < 3; ++j)
		{
			int offsetj = 3 * 3 * i + 3 * j;

			for (int k = 0; k < 3; ++k) // Identity matrix scaled by coordinate
				this->m_DvDq(offseti + k, offsetj + k) = this->m_vembCor[count];

			count++;
		}
	}

	this->m_DvDq(9, 27) = 1.0;
	this->m_DvDq(10, 28) = 1.0;
}

void EmbeddedDERRodBendElement::updateRest(const dVector& vx, const dVector& va, const vector<Frame>& vF)
{
	Vector3d c0 = get3D(this->m_vinds[0], vx);
	Vector3d c1 = get3D(this->m_vinds[1], vx);
	Vector3d c2 = get3D(this->m_vinds[2], vx);
	const Frame& f0 = vF[this->m_einds[0]];
	const Frame& f1 = vF[this->m_einds[1]];
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;

	this->updateRest(e, f, f0, f1);
}

void EmbeddedDERRodBendElement::updateRest(const Vector3d &e, const Vector3d &f, const Frame &f0, const Frame &f1)
{
	this->m_l0 = this->computeVertexLength(e, f);
	this->m_K0 = this->computeKappa(e, f, f0, f1);
}

void EmbeddedDERRodBendElement::updateEnergy(const dVector& vx, const dVector& va, const vector<Frame>& vF) 
{
	Vector3d c0 = get3D(this->m_vinds[0], vx);
	Vector3d c1 = get3D(this->m_vinds[1], vx);
	Vector3d c2 = get3D(this->m_vinds[2], vx);
	const Frame& f0 = vF[this->m_einds[0]];
	const Frame& f1 = vF[this->m_einds[1]];
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;

	this->m_energy = this->computeEnergy(e, f, f0, f1);
}

void EmbeddedDERRodBendElement::updateForce(const dVector& vx, const dVector& va, const vector<Frame>& vF) 
{
	Vector3d c0 = get3D(this->m_vinds[0], vx);
	Vector3d c1 = get3D(this->m_vinds[1], vx);
	Vector3d c2 = get3D(this->m_vinds[2], vx);
	const Frame& f0 = vF[this->m_einds[0]];
	const Frame& f1 = vF[this->m_einds[1]];
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;

	VectorXd vfe;
	this->computeForce(e, f, f0, f1, vfe);
	this->m_vfVal = this->m_DvDq.transpose() * this->m_DeDv.transpose() * vfe;
}

void EmbeddedDERRodBendElement::updateJacobian(const dVector& vx, const dVector& va, const vector<Frame>& vF)
{
	Vector3d c0 = get3D(this->m_vinds[0], vx);
	Vector3d c1 = get3D(this->m_vinds[1], vx);
	Vector3d c2 = get3D(this->m_vinds[2], vx);
	const Frame& f0 = vF[this->m_einds[0]];
	const Frame& f1 = vF[this->m_einds[1]];
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;

	MatrixXd mJe;
	this->computeJacobian(e, f, f0, f1, mJe);
	MatrixXd mJ = this->m_DvDq.transpose() * this->m_DeDv.transpose() * mJe * this->m_DeDv * this->m_DvDq;

	for (int i = 0; i < 29; ++i)
		for (int j = 0; j < 29; ++j)
		{
		if (i == j)
			continue;

		if (this->m_vidx[i] == this->m_vidx[j])
		{
			mJ(i, i) += mJ(i, j);
			mJ(i, j) = 0.0; // Sum
		}
		}

	int count = 0;
	for (int i = 0; i < 29; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ(i, j);
}

Real EmbeddedDERRodBendElement::computeEnergy(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1)
{
	VectorXd kappa = this->computeKappa(e, f, f0, f1);

	MatrixXd B = this->computeB();
	VectorXd kappaDiff = kappa - m_K0;
	return kappaDiff.dot(B*kappaDiff)*(0.5 / m_l0);;
}

void EmbeddedDERRodBendElement::computeForce(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, VectorXd& vf)
{
	MatrixXd gK(8, 2);
	this->computeGradKappa(e, f, f0, f1, gK);
	VectorXd kappa = this->computeKappa(e, f, f0, f1);
	VectorXd kappaDiff = kappa - m_K0;

	MatrixXd B = this->computeB();
	VectorXd BdotK = B * kappaDiff;
	vf = gK * BdotK * (-1.0 / m_l0);
}

void EmbeddedDERRodBendElement::computeJacobian(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& mJ)
{
	MatrixXd gK(8, 2);
	MatrixXd hK1(8, 8);
	MatrixXd hK2(8, 8);
	this->computeGradKappa(e, f, f0, f1, gK);
	this->computeHessKappa(e, f, f0, f1, hK1, hK2);
	VectorXd kappa = this->computeKappa(e, f, f0, f1);
	double milen = (-1.0 / this->m_l0);
	VectorXd kappaDiff = kappa - m_K0;

	MatrixXd B = this->computeB();
	VectorXd KdotB = B.transpose() * kappaDiff * milen;
	mJ = (gK * B * gK.transpose()) * milen + hK1 * KdotB(0) + hK2 * KdotB(1);
}

void EmbeddedDERRodBendElement::computeGradKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& gK) const
{
	Vector3d te = f0.tan;
	Vector3d tf = f1.tan;
	Vector3d d1e = f0.bin;
	Vector3d d1f = f1.bin;
	Vector3d d2e = f0.nor;
	Vector3d d2f = f1.nor;

	double chi = 1.0 + te.dot(tf);
	double norm_e = e.norm();
	double norm_f = f.norm();
	Vector3d tilde_t = (te + tf) / chi;
	Vector3d tilde_d1 = (d1e + d1f) / chi;
	Vector3d tilde_d2 = (d2e + d2f) / chi;
	VectorXd kappa = this->computeKappa(e, f, f0, f1);
	Vector3d kb = this->computeKB(e, f);
	double kappa1 = kappa[0];
	double kappa2 = kappa[1];

	{
		Vector3d Dkappa1De = (1.0 / norm_e) * (tilde_t * -kappa1 + tf.cross(tilde_d2));
		Vector3d Dkappa1Df = (1.0 / norm_f) * (tilde_t * -kappa1 - te.cross(tilde_d2));
		Vector3d Dkappa2De = (1.0 / norm_e) * (tilde_t * -kappa2 - tf.cross(tilde_d1));
		Vector3d Dkappa2Df = (1.0 / norm_f) * (tilde_t * -kappa2 + te.cross(tilde_d1));

		gK.resize(8, 2);
		gK.setZero();

		for (int i = 0; i < 3; i++)
		{
			gK(0 + i, 0) = Dkappa1De[i];
			gK(0 + i, 1) = Dkappa2De[i];
			gK(4 + i, 0) = Dkappa1Df[i];
			gK(4 + i, 1) = Dkappa2Df[i];
		}

		gK(3, 0) = kb.dot(d1e) * -0.5;
		gK(7, 0) = kb.dot(d1f) * -0.5;
		gK(3, 1) = kb.dot(d2e) * -0.5;
		gK(7, 1) = kb.dot(d2f) * -0.5;
	}
}

void EmbeddedDERRodBendElement::computeHessKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& hK1, MatrixXd& hK2) const
{
	hK1.resize(8, 8);
	hK2.resize(8, 8);
	hK1.setZero();
	hK2.setZero();

	Vector3d te = f0.tan;
	Vector3d tf = f1.tan;
	Vector3d d1e = f0.bin;
	Vector3d d1f = f1.bin;
	Vector3d d2e = f0.nor;
	Vector3d d2f = f1.nor;
	double norm_e = e.norm();
	double norm_f = f.norm();
	double norm2_e = norm_e*norm_e;
	double norm2_f = norm_f*norm_f;
	double chi = 1.0 + te.dot(tf);
	Vector3d tilde_t = (te + tf) / chi;
	Vector3d tilde_d1 = (d1e + d1f) / chi;
	Vector3d tilde_d2 = (d2e + d2f) / chi;
	VectorXd kappa = this->computeKappa(e, f, f0, f1);
	Vector3d kb = this->computeKB(e, f);
	double kappa1 = kappa[0];
	double kappa2 = kappa[1];

	{
		Matrix3d tt_o_tt = outerProduct(tilde_t, tilde_t);
		Matrix3d tf_c_d2t_o_tt = outerProduct(tf.cross(tilde_d2), tilde_t);
		Matrix3d tt_o_tf_c_d2t = tf_c_d2t_o_tt.transpose();
		Matrix3d kb_o_d2e = outerProduct(kb, d2e);
		Matrix3d d2e_o_kb = kb_o_d2e.transpose();

		Matrix3d I;
		I.setIdentity();

		Matrix3d D2kappa1De2 = 1.0 / norm2_e * (2 * kappa1 * tt_o_tt - (tf_c_d2t_o_tt + tt_o_tf_c_d2t)) - kappa1 / (chi
			* norm2_e) * (I - outerProduct(te, te)) + 1.0 / (4.0 * norm2_e) * (kb_o_d2e + d2e_o_kb);

		Matrix3d te_c_d2t_o_tt = outerProduct(te.cross(tilde_d2), tilde_t);
		Matrix3d tt_o_te_c_d2t = te_c_d2t_o_tt.transpose();
		Matrix3d kb_o_d2f = outerProduct(kb, d2f);
		Matrix3d d2f_o_kb = kb_o_d2f.transpose();

		Matrix3d D2kappa1Df2 = 1.0 / norm2_f * (2 * kappa1 * tt_o_tt + (te_c_d2t_o_tt + tt_o_te_c_d2t)) - kappa1 / (chi
			* norm2_f) * (I - outerProduct(tf, tf)) + 1.0 / (4.0 * norm2_f) * (kb_o_d2f + d2f_o_kb);

		Matrix3d D2kappa1DeDf = -kappa1 / (chi * norm_e * norm_f) * (I + outerProduct(te, tf)) + 1.0 / (norm_e * norm_f) * (2
			* kappa1 * tt_o_tt - tf_c_d2t_o_tt + tt_o_te_c_d2t - crossMatrix(tilde_d2));
		Matrix3d D2kappa1DfDe = D2kappa1DeDf.transpose();

		Matrix3d tf_c_d1t_o_tt = outerProduct(tf.cross(tilde_d1), tilde_t);
		Matrix3d tt_o_tf_c_d1t = tf_c_d1t_o_tt.transpose();
		Matrix3d kb_o_d1e = outerProduct(kb, d1e);
		Matrix3d d1e_o_kb = kb_o_d1e.transpose();

		Matrix3d D2kappa2De2 = 1.0 / norm2_e * (2 * kappa2 * tt_o_tt + (tf_c_d1t_o_tt + tt_o_tf_c_d1t)) - kappa2 / (chi
			* norm2_e) * (I - outerProduct(te, te)) - 1.0 / (4.0 * norm2_e) * (kb_o_d1e + d1e_o_kb);

		Matrix3d te_c_d1t_o_tt = outerProduct(te.cross(tilde_d1), tilde_t);
		Matrix3d tt_o_te_c_d1t = te_c_d1t_o_tt.transpose();
		Matrix3d kb_o_d1f = outerProduct(kb, d1f);
		Matrix3d d1f_o_kb = kb_o_d1f.transpose();

		Matrix3d D2kappa2Df2 = 1.0 / norm2_f * (2 * kappa2 * tt_o_tt - (te_c_d1t_o_tt + tt_o_te_c_d1t)) - kappa2 / (chi
			* norm2_f) * (I - outerProduct(tf, tf)) - 1.0 / (4.0 * norm2_f) * (kb_o_d1f + d1f_o_kb);

		Matrix3d D2kappa2DeDf = -kappa2 / (chi * norm_e * norm_f) * (I + outerProduct(te, tf)) + 1.0 / (norm_e * norm_f) * (2
			* kappa2 * tt_o_tt + tf_c_d1t_o_tt - tt_o_te_c_d1t + crossMatrix(tilde_d1));
		Matrix3d D2kappa2DfDe = D2kappa2DeDf.transpose();

		double D2kappa1Dthetae2 = -0.5 * (kb.dot(d2e));
		double D2kappa1Dthetaf2 = -0.5 * (kb.dot(d2f));
		double D2kappa2Dthetae2 = 0.5 * (kb.dot(d1e));
		double D2kappa2Dthetaf2 = 0.5 * (kb.dot(d1f));

		Vector3d D2kappa1DeDthetae = (tilde_t * (kb.dot(d1e)) * 0.5 - tf.cross(d1e) * (1.0 / chi)) * (1.0 / norm_e);
		Vector3d D2kappa1DeDthetaf = (tilde_t * (kb.dot(d1f)) * 0.5 - tf.cross(d1f) * (1.0 / chi)) * (1.0 / norm_e);
		Vector3d D2kappa1DfDthetae = (tilde_t * (kb.dot(d1e)) * 0.5 + te.cross(d1e) * (1.0 / chi)) * (1.0 / norm_f);
		Vector3d D2kappa1DfDthetaf = (tilde_t * (kb.dot(d1f)) * 0.5 + te.cross(d1f) * (1.0 / chi)) * (1.0 / norm_f);
		Vector3d D2kappa2DeDthetae = (tilde_t * (kb.dot(d2e)) * 0.5 - tf.cross(d2e) * (1.0 / chi)) * (1.0 / norm_e);
		Vector3d D2kappa2DeDthetaf = (tilde_t * (kb.dot(d2f)) * 0.5 - tf.cross(d2f) * (1.0 / chi)) * (1.0 / norm_e);
		Vector3d D2kappa2DfDthetae = (tilde_t * (kb.dot(d2e)) * 0.5 + te.cross(d2e) * (1.0 / chi)) * (1.0 / norm_f);
		Vector3d D2kappa2DfDthetaf = (tilde_t * (kb.dot(d2f)) * 0.5 + te.cross(d2f) * (1.0 / chi)) * (1.0 / norm_f);

		// Assembling hK1

		addBlock3x3From(0, 0, D2kappa1De2, hK1);
		addBlock3x3From(4, 4, D2kappa1Df2, hK1);
		addBlock3x3From(0, 4, D2kappa1DeDf, hK1);
		addBlock3x3From(4, 0, D2kappa1DfDe, hK1);

		hK1(3, 3) = D2kappa1Dthetae2;
		hK1(7, 7) = D2kappa1Dthetaf2;

		for (int i = 0; i < 3; i++)
		{
			hK1(0 + i, 3) = hK1(3, 0 + i) = D2kappa1DeDthetae[i];
			hK1(4 + i, 3) = hK1(3, 4 + i) = D2kappa1DfDthetae[i];
			hK1(0 + i, 7) = hK1(7, 0 + i) = D2kappa1DeDthetaf[i];
			hK1(4 + i, 7) = hK1(7, 4 + i) = D2kappa1DfDthetaf[i];
		}

		// Assembling hK2

		addBlock3x3From(0, 0, D2kappa2De2, hK2);
		addBlock3x3From(4, 4, D2kappa2Df2, hK2);
		addBlock3x3From(0, 4, D2kappa2DeDf, hK2);
		addBlock3x3From(4, 0, D2kappa2DfDe, hK2);

		hK2(3, 3) = D2kappa2Dthetae2;
		hK2(7, 7) = D2kappa2Dthetaf2;

		for (int i = 0; i < 3; i++)
		{
			hK2(0 + i, 3) = hK2(3, 0 + i) = D2kappa2DeDthetae[i];
			hK2(4 + i, 3) = hK2(3, 4 + i) = D2kappa2DfDthetae[i];
			hK2(0 + i, 7) = hK2(7, 0 + i) = D2kappa2DeDthetaf[i];
			hK2(4 + i, 7) = hK2(7, 4 + i) = D2kappa2DfDthetaf[i];
		}
	}
}

Vector3d EmbeddedDERRodBendElement::computeKB(const Vector3d& e, const Vector3d& f) const
{
	Vector3d t0 = e.normalized();
	Vector3d t1 = f.normalized();
	return (t0.cross(t1)) * 2 * (1 / (1 + t0.dot(t1)));
}

double EmbeddedDERRodBendElement::computeVertexLength(const Vector3d& e, const Vector3d& f) const
{
	return (e.norm() + f.norm())*0.5;
}

VectorXd EmbeddedDERRodBendElement::computeKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1) const
{
	Vector3d KB = this->computeKB(e, f);
	Vector3d m1e = f0.bin;
	Vector3d m1f = f1.bin;
	Vector3d m2e = f0.nor;
	Vector3d m2f = f1.nor;
	VectorXd kappa(2);
	kappa[0] = (m2e + m2f).dot(KB) * 0.5;
	kappa[1] = (m1e + m1f).dot(KB) * -0.5;
	return kappa;
}

MatrixXd EmbeddedDERRodBendElement::computeB() const
{
	MatrixXd B(2, 2);

	B.setZero();
	double Y = this->m_vpmat[0]->getYoung();
	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double A = M_PI * w * h;
	B(0, 0) = w * w;
	B(1, 1) = h * h;
	B = (0.25 * Y * A) * B;

	return B;
}

double EmbeddedDERRodBendElement::getConstant() const
{
	MatrixXd B = this->computeB(); // Stiff.
	return (0.5*(B(0, 0) + B(1, 1))) / m_l0;
}
