/*=====================================================================================*/
/*!
\file		BasicSlideBendingElement.h
\author		jesusprod
\brief		Implementation of BasicSlideBendingElement.h.
*/
/*=====================================================================================*/

#ifndef BASIC_SLIDE_BENDING_ELEMENT_3D_H
#define BASIC_SLIDE_BENDING_ELEMENT_3D_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/DERSlideElement.h>

class BasicSlideBendingElement : public DERSlideElement
{
public:
	BasicSlideBendingElement();
	~BasicSlideBendingElement();

	virtual Real computeEnergy(const Vector3d& x0, const Vector3d& x1, const Vector3d& x2, Real t0, Real t1, Real t2);
	virtual void computeForce(const Vector3d& x0, const Vector3d& x1, const Vector3d& x2, Real t0, Real t1, Real t2, VectorXd& vf);
	virtual void computeJacobian(const Vector3d& x0, const Vector3d& x1, const Vector3d& x2, Real t0, Real t1, Real t2, MatrixXd& mJ);

	virtual Real computeB() const;

	virtual Real getIntegrationVolume() const { return -1; }

};

#endif