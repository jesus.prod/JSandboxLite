/*=====================================================================================*/
/*! 
\file		FEMElementLinear.h
\author		jesusprod
\brief		Basic linear FEM element. 
				- Deformation metric: Cauchy strain e
				- Parameters: Lam� coefficients (m,l)
				- Energy density: Psi(F) = mu*e*e + 0.5*l + tr^2(e)
				- First Piola-Kirchhoff: P(F) = mu*(F + F^T - 2*I) + l*tr(F - I)*I
 */
/*=====================================================================================*/

#ifndef FEM_MODEL_LINEAR_H
#define FEM_MODEL_LINEAR_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/FEMElement.h>
#include <JSandbox/SolidMaterial.h>

class FEMElementLinear : public FEMElement
{

public:
	FEMElementLinear(int nv, int nd, FEMType D) : FEMElement(nv, nd, D) { }	

	virtual ~FEMElementLinear() { }

	virtual Real computeEnergyDensity(const MatrixXd& mF) const;
	virtual void computePiolaKirchhoffFirst(const MatrixXd& mF, MatrixXd& mP) const;
	virtual void computePiolaKirchhoffFirstDerivative(const MatrixXd& mF, const vector<MatrixXd>& vDFDx, vector<MatrixXd>& vDPDx) const;

};

#endif