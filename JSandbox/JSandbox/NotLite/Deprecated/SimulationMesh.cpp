/*=====================================================================================*/
/*!
\file		SimMesh.h
\author		jesusprod
\brief		Wrapping class for an OpenMesh 3D mesh which allows to specify different
			spaces to store information about the state of a simulation. This make 
			remeshing operations easier as changes in the topology are done
			in a consistent manner.
*/
/*=====================================================================================*/

#ifndef SIMULATION_MESH_H
#define SIMULATION_MESH_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/Geometry/TriMesh.h>

class SimMesh : public TriMesh
{
public:

	enum Property3D
	{
		ELASTICVEL,
		ELASTICPOS,
		PLASTICPOS,
		UNDEFORPOS,
	};
	
	SimMesh(void);
	~SimMesh(void);

	void init(const dVector& vpos, const iVector& vidx);

	void getProperty3D(int idx, Vector3d& p, Property3D P) const;
	void setProperty3D(int idx, const Vector3d& p, Property3D P);

	void getProperties3D(dVector& vp, Property3D P) const;
	void setProperties3D(const dVector& vp, Property3D P);

	virtual void getFaceProperties3D(const FaceHandle& fh, vector<Vector3d>& vfv, Property3D P) const;
	virtual void getEdgeProperties3D(const EdgeHandle& eh, vector<Vector3d>& vfe, Property3D P) const;

protected:

	map<Property3D, OpenMesh::VPropHandleT<Vector3d>> m_mProps3D;

};

#endif