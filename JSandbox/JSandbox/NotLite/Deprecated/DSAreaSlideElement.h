///*=====================================================================================*/
///*!
//\file		DSAreaSlideElement.h
//\author		jesusprod
//\brief		Basic implementation of a discrete-shell area element allowed to slide. This 
//			provides a simple non-linear energy density for area preserving triangles that
//			are allowed to slide in the parametrization plane UV. It relies on Maple auto
//			differentiation tool for force and Jacobian expressions. It uses SolidMaterial 
//			area K as elasticity coefficient.
//*/
///*=====================================================================================*/
//
//#ifndef DS_AREA_SLIDE_ELEMENT_H
//#define DS_AREA_SLIDE_ELEMENT_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//#include <JSandbox/SolidMaterial.h>
//#include <JSandbox/NodalSolidElement.h>
//
//class DSAreaSlideElement : public NodalSolidElement
//{
//public:
//	DSAreaSlideElement(int dim0);
//	~DSAreaSlideElement();
//
//	virtual void updateRest(const VectorXd& vX) { /* Explicit every time */ }
//
//	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
//	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
//	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);
//
//	virtual Real getIntegrationVolume() const { return -1.0; } // Not precomputed
//
//protected:
//
//};
//
//#endif