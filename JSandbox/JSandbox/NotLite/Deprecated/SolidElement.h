/*=====================================================================================*/
/*! 
\file		SolidElement.h
\author		jesusprod
\brief		Basic abstract implementation of SolidElement to inherit from.
			All solid elements should inherit from this abstract class. It
			includes functions for the precomputation and storage of the
			local gradient and hessian and sparsity management.
 */
/*=====================================================================================*/

#ifndef SOLID_ELEMENT_H
#define SOLID_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/SolidMaterial.h>

class SolidElement
{
public:
	SolidElement(int nd, int nd0)
	{
		assert(nd > 0);
		this->m_nrawDOF = nd;
		this->m_nrawDOF0 = nd0;
		this->m_vidx.resize(nd);
		this->m_vidx0.resize(nd0);

		this->m_vfVal.resize(nd);
		this->m_vfVal.setZero();

		this->m_vmass.resize(nd);
		this->m_vmass.setZero();
		
		int ne = (nd*(nd + 1))/2.0;
		this->m_vJVal.resize(ne);
		this->m_vJVal.setZero();

		this->m_preStrain = 0.0; 

		this->m_vpmat.resize(1);
	}

	virtual ~SolidElement() { }

	virtual int getRawDOFNumber() const { return m_nrawDOF; }
	virtual int getRawDOFNumber0() const { return m_nrawDOF0; }

	virtual const iVector& getIndices() const { return this->m_vidx; }
	virtual const iVector& getIndices0() const { return this->m_vidx0; }

	virtual void setIndices(const iVector& vi) { assert((int)vi.size() == m_nrawDOF); this->m_vidx = vi; }
	virtual void setIndices0(const iVector& vi) { assert((int)vi.size() == m_nrawDOF0); this->m_vidx0 = vi; }

	virtual SolidMaterial *getMaterial() const { return this->m_vpmat[0]; }
	virtual void setMaterial(SolidMaterial *pm) { this->m_vpmat[0] = pm; }

	virtual vector<SolidMaterial*> getMaterials() const { return this->m_vpmat; }
	virtual void setMaterials(vector<SolidMaterial*> vp) { this->m_vpmat = vp; }

	virtual Real getPreStrain() const { return this->m_preStrain; }
	virtual void setPreStrain(Real st) { this->m_preStrain = st; }

	virtual void addMass(VectorXd& vm) const;

	virtual Real getEnergy() const;
	virtual Real getEnergyDensity() const;
	virtual const VectorXd& getLocalForce() const;
	virtual const VectorXd& getLocalJacobian() const;
	virtual void addForce(VectorXd& vf, const vector<bool>* pvFixed = NULL) const;
	virtual void addJacobian(tVector& vJ, const vector<bool>* pvFixed = NULL) const;

	virtual Real getIntegrationVolume() const = 0;
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void updateRest(const VectorXd& vX) = 0;

	virtual void updateEnergyForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForceJacobian(const VectorXd& vx, const VectorXd& vv);
	virtual void updateEnergyForceJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual void deallocateSparseEntries(int& n);
	virtual void allocateSparseEntries(int& n);

	virtual Real testForceLocal(const VectorXd& vx, const VectorXd& vv);
	virtual Real testJacobianLocal(const VectorXd& vx, const VectorXd& vv);
	
protected:

	int m_nrawDOF;
	int m_nrawDOF0;

	iVector m_vidx;					// Vector of global indices for element nodes
	iVector m_vidx0;				// Vector of global indices for element rest

	vector<SolidMaterial*> m_vpmat;	// Pointer to material element, not managed

	VectorXd m_vJVal;				// Where to store precomputed hessian values
	VectorXd m_vmass;				// Where to store precomputed lumped mass
	VectorXd m_vfVal;				// Where to store precomputed force
	Real m_energy;

	Real m_preStrain;
};

#endif