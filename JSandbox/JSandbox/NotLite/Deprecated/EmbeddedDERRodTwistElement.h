/*=====================================================================================*/
/*!
\file		EmbeddedDERRodTwistElement.h
\author		jesusprod
\brief		Declaration of EmbeddedDERRodTwistElement class
*/
/*=====================================================================================*/

#ifndef EMBEDDED_DER_ROD_TWIST_ELEMENT_H
#define EMBEDDED_DER_ROD_TWIST_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "MathUtils.h"
#include "SolidElement.h"

class EmbeddedDERRodTwistElement : public SolidElement
{

public:
	EmbeddedDERRodTwistElement();
	virtual ~EmbeddedDERRodTwistElement();

	virtual const dVector& getEmbCoordinates() const;
	virtual void setEmbCoordinates(const dVector& vi);

	virtual void updateEnergy(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt);
	virtual void updateForce(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt);
	virtual void updateJacobian(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt);
	virtual void updateRest(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt);

	virtual Real computeEnergy(Real d0, Real d1, Real rt);
	virtual void computeForce(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt, VectorXd& vf);
	virtual void computeJacobian(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt, MatrixXd& mJ);
	virtual void updateRest(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt);

	virtual double computeB() const;
	virtual Vector3d computeKB(const Vector3d& e, const Vector3d& f) const;
	virtual void computeGradTwist(const Vector3d& e, const Vector3d& f, VectorXd& gT) const;
	virtual void computeHessTwist(const Vector3d& e, const Vector3d& f, MatrixXd& hT) const;
	virtual double computeVertexLength(const Vector3d& e, const Vector3d& f) const;
	virtual double computeVertexTwist(Real d0, Real d1, Real rt) const;

	virtual const iVector& getVIndices() { return this->m_vinds; }
	virtual const iVector& getEIndices() { return this->m_einds; }
	virtual void setVIndices(const iVector& vinds) { this->m_vinds = vinds; }
	virtual void setEIndices(const iVector& einds) { this->m_einds = einds; }

	virtual Real getIntegrationVolume() const { return this->m_l0; }
	
	virtual void updateRest(const VectorXd& vX) { }
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv) { }
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv) { }
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv) { }

	double getRestVertexLength() const { return this->m_l0; }
	void setRestVertexLength(double l0) { this->m_l0 = l0; }

	double getRestVertexTwist() const { return this->m_t0; }
	void setRestVertexTwist(double t0) { this->m_t0 = t0; }

	double getConstant() const;

protected:

	iVector m_vinds;
	iVector m_einds;

	dVector m_vembCor;

	MatrixXd m_DvDq; // Embedding
	MatrixXd m_DeDv; // Transmission
	double m_l0; // Rest vertex length 
	double m_t0; // Rest vertex twist
};

#endif
