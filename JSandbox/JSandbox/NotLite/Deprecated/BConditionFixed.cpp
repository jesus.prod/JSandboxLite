/*=====================================================================================*/
/*!
\file		BConditionFixed.cpp
\author		jesusprod
\brief		Implementation of BConditionFixed.h
*/
/*=====================================================================================*/

#include <JSandbox/BConditionFixed.h>

BConditionFixed::BConditionFixed(SolidModel* pModel) : BCondition(pModel)
{
	// Nothing to do here...
}

BConditionFixed::~BConditionFixed()
{
	// Nothing to do here...
}

void BConditionFixed::constrain(VectorXd& vf) const
{
	for (int i = 0; i < (int)this->m_vidx.size(); ++i)
		vf(this->m_vidx(i)) = 0.0; // No force at fixed
}

void BConditionFixed::constrain(SolidModel* pModel) const
{
	VectorXd vx = pModel->getPositions();
	const VectorXd& vv = pModel->getVelocities();
	for (int i = 0; i < (int)this->m_vidx.size(); ++i)
		vx(this->m_vidx(i)) = this->m_vval(i); // Set
	pModel->updateStatex(vx, vv);
}

void BConditionFixed::constrain(tVector& vJ) const
{
	int N = this->m_pModel->getRawDOFNumber();

	bVector fixedStencil(N, false);
	for (int i = 0; i < (int)this->m_vidx.size(); ++i)
		fixedStencil[this->m_vidx(i)] = true; // Fixed!

	int nEntries = (int) vJ.size();
	for (int i = 0; i < nEntries; ++i)
	{
		Triplet<Real>& t = vJ[i];
		int r = t.row();
		int c = t.col();
		if (r == c && fixedStencil[r])
			vJ[i] = Triplet<Real>(r, c, -1.0);
		else if (fixedStencil[r] || fixedStencil[c]) 
			vJ[i] = Triplet<Real>(r, c, 0.0);
	}
}


void BConditionFixed::initializeIndividual(const iVector& vidx)
{
	int N = (int)vidx.size();
	this->m_vidx.resize(N);
	this->m_vval.resize(N);

	for (int i = 0; i < N; ++i)
		this->m_vidx[i] = vidx[i];

	const VectorXd& vx = this->m_pModel->getPositions();
	for (int i = 0; i < N; ++i) // To current value
		this->m_vval(i) = vx(m_vidx(i));

	this->m_maxStep = 1;
	this->m_curStep = 1;
}

void BConditionFixed::initializeNodal(int n, const iVector& vidx)
{
	int nv = (int)vidx.size();
	int N = n*nv;
	this->m_vidx.resize(N);
	this->m_vval.resize(N);
	
	for (int i = 0; i < nv; ++i)
	{
		int offsetN = n*i;
		int offsetn = n*vidx[i];
		for (int d = 0; d < n; ++d)
			this->m_vidx[offsetN + d] = offsetn + d;
	}
	
	const VectorXd& vx = this->m_pModel->getPositions();
	for (int i = 0; i < N; ++i) // To current value
		this->m_vval(i) = vx(m_vidx(i));

	this->m_maxStep = 1;
	this->m_curStep = 1;
}