/*=====================================================================================*/
/*!
\file		EmbeddedSolidElement.cpp
\author		jesusprod
\brief		Implementation of EmbeddedSolidElement.h
*/
/*=====================================================================================*/

#include <JSandbox/EmbeddedSolidElement.h>


const dVector& EmbeddedSolidElement::getEmbeddingCoordinates() const
{
	return this->m_vembCor;
}

void EmbeddedSolidElement::setEmbCoordinates(const dVector& vcor)
{
	assert((int)vcor.size() == this->m_numNodeSim);
	this->m_vembCor = vcor; // Barycentric or so

	this->m_mDxDv.resize(this->m_numDim*this->m_numEmbNodes, this->m_numDim*this->m_numNodeSim);
	this->m_mDxDv.setZero();

	int count = 0;

	for (int i = 0; i < this->m_numEmbNodes; ++i) 
	{
		int offseti = this->m_numDim*i;

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int offsetj = this->m_numDim*i*this->m_numEmbCoord + this->m_numDim*j;

			for (int k = 0; k < 3; ++k) // Identity matrix scaled by coordinate
				this->m_mDxDv(offseti + k, offsetj + k) = this->m_vembCor[count];

			count++;
		}
	}
}

void EmbeddedSolidElement::addForcesToEmbedding(const dVector& vembFVal)
{
	this->m_vfVal = this->m_mDxDv.transpose()*toEigen(vembFVal);

	//int count = 0;

	//for (int i0 = 0; i0 < this->m_numEmbNodes; ++i0)
	//{
	//	int isourceOffset = this->m_numDim*i0;
	//	int inodeOffset = this->m_numEmbCoord*i0;

	//	for (int i1 = 0; i1 < this->m_numEmbCoord; ++i1)
	//	{
	//		double iwei = this->m_vembCor[inodeOffset + i1];

	//		for (int i2 = 0; i2 < this->m_numDim; ++i2)
	//		{
	//			this->m_vfVal(count++) = iwei*vembFVal[isourceOffset + i2];
	//		}
	//	}
	//}

}

void EmbeddedSolidElement::addJacobianToEmbedding(const vector<dVector>& vembJVal)
{
	int n = this->m_numDim*this->m_numEmbNodes;

	MatrixXd membJVal(n,n);
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			membJVal(i,j) = vembJVal[i][j];

	MatrixXd mJ = this->m_mDxDv.transpose()*membJVal*this->m_mDxDv;
	int N = this->m_numDim*this->m_numEmbCoord*this->m_numEmbNodes;
	
	//int N = this->m_numDim*this->m_numEmbCoord*this->m_numEmbNodes;
	//MatrixXd mJ(N,N);
	//mJ.setZero();

	//for (int i = 0; i < this->m_numEmbNodes; i++)
	//{
	//	dVector coordsi(this->m_numEmbCoord);
	//	for (int k = 0; k < this->m_numEmbCoord; ++k)
	//		coordsi[k] = this->m_vembCor[this->m_numEmbCoord*i + k];

	//	for (int j = 0; j < m_numEmbNodes; j++)
	//	{
	//		dVector coordsj(this->m_numEmbCoord);
	//		for (int k = 0; k < this->m_numEmbCoord; ++k)
	//			coordsj[k] = this->m_vembCor[this->m_numEmbCoord*j + k];

	//		Matrix3d B;
	//		for (int k = 0; k < this->m_numDim; k++) 
	//			for (int l = 0; l < this->m_numDim; l++)
	//				B(k, l) = vembJVal[this->m_numDim*i + k][this->m_numDim*j + l];

	//		for (int k = 0; k < coordsi.size(); k++)
	//			for (int l = 0; l < coordsj.size(); l++)
	//				addBlock3x3(this->m_numEmbCoord*i + k,
	//							this->m_numEmbCoord*j + l,
	//							coordsi[k]*B*coordsj[l],
	//							mJ, false);
	//	}
	//}

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
		{
			if (i == j) 
				continue;

			if (this->m_vidx[i] == this->m_vidx[j])
			{
				mJ(i, i) += mJ(i, j);
				mJ(i, j) = 0.0; // Sum
			}
		}

	int count = 0;
	for (int i = 0; i < N; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ(i,j);
}


void EmbeddedSolidElement::getEmbbededPositions3D0(const VectorXd& vx, vector<Vector3d>& vp)
{
	vp.clear();

	dVector vxSTL = toSTL(vx);

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int nodeOffset = i*this->m_numEmbCoord;

		Vector3d p;
		p.setZero();

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int idxNode = nodeOffset + j;
			Vector3d node = getBlock3x1(this->m_vnodeIdx0[idxNode], vx);
			p += node * this->m_vembCor[idxNode]; // Coordinate weights
		}

		vp.push_back(p);
	}
}

void EmbeddedSolidElement::getEmbbededPositions3D(const VectorXd& vx, vector<Vector3d>& vp)
{
	vp.clear();

	dVector vxSTL = toSTL(vx);

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int nodeOffset = i*this->m_numEmbCoord;

		Vector3d p;
		p.setZero();

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int idxNode = nodeOffset + j;
			Vector3d node = getBlock3x1(this->m_vnodeIdx[idxNode], vx);
			p += node * this->m_vembCor[idxNode]; // Coordinate weights
		}

		vp.push_back(p);
	}
}


void EmbeddedSolidElement::getEmbbededPositions2D0(const VectorXd& vx, vector<Vector2d>& vp)
{
	vp.clear();

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int nodeOffset = i*this->m_numEmbCoord;

		Vector2d p;
		p.setZero();

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int idxNode = nodeOffset + j;
			Vector2d node = getBlock2x1(this->m_vnodeIdx0[idxNode], vx);
			p += node * this->m_vembCor[idxNode]; // Coordinate weights
		}

		vp.push_back(p);
	}
}

void EmbeddedSolidElement::getEmbbededPositions2D(const VectorXd& vx, vector<Vector2d>& vp)
{
	vp.clear();

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int nodeOffset = i*this->m_numEmbCoord;

		Vector2d p;
		p.setZero();

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int idxNode = nodeOffset + j;
			Vector2d node = getBlock2x1(this->m_vnodeIdx[idxNode], vx);
			p += node * this->m_vembCor[idxNode]; // Coordinate weights
		}

		vp.push_back(p);
	}
}