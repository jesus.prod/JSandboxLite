/*=====================================================================================*/
/*! 
\file		FEMElementLinearCorot.h
\author		jesusprod
\brief		Basic co-rotational linear FEM element. 
				- Deformation metric: Cauchy strain e
				- Parameters: Lam� coefficients (m,l)
				- Energy density: Psi(F) = mu*(F - R)*(F - R) + 0.5*l + tr^2(R^T*F - I), with F = RS
				- First Piola-Kirchhoff: P(F) = 2*mu*(F - R) + l*tr(R^T*F - I)*R, with F = RS
 */
/*=====================================================================================*/

#ifndef FEM_MODEL_LINEAR_COROT_H
#define FEM_MODEL_LINEAR_COROT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/FEMElement.h>
#include <JSandbox/SolidMaterial.h>

/*
Current limitations:

- Element rotation is computed using GS orthogonalization of the
  deformation gradient F. Some other methods should be more
  appropriate: polar decomposition, SVD decomposition...

- Element rotation second derivatives are neglected.
*/


class FEMElementLinearCorot : public FEMElement
{
public:
	FEMElementLinearCorot(int nv, int nd, FEMType D) : FEMElement(nv, nd, D) { }	

	virtual ~FEMElementLinearCorot() { }

	virtual Real computeEnergyDensity(const MatrixXd& mF) const;
	virtual void computePiolaKirchhoffFirst(const MatrixXd& mF, MatrixXd& mP) const;
	virtual void computePiolaKirchhoffFirstDerivative(const MatrixXd& mF, const vector<MatrixXd>& vDFDx, vector<MatrixXd>& vDPDx) const;
};

#endif