/*=====================================================================================*/
/*! 
\file		NodalSolidModel.h
\author		jesusprod
\brief		Basic abstract implementation of SolidModel to inherit from.
			All (purely) nodal solid models should inherit from this abstract 
			class. It includes functions for nodes access and management.
 */
/*=====================================================================================*/

#ifndef NODAL_SOLID_MODEL_H
#define NODAL_SOLID_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidModel.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>


class JSANDBOX_EXPORT NodalSolidModel : public SolidModel
{
public:
	NodalSolidModel() {}

	virtual ~NodalSolidModel() {}

	virtual int getNumDim() const { return this->m_numDim; }
	virtual int getNumNodeSim() const { return this->m_numNodeSim; }

	virtual void visualizeEnergy(dVector& vcol);
	virtual void visualizeForce(dVector& vcol);
	
protected:
	
	virtual void initializeElements() = 0;

protected:

	int m_numDim;
	int m_numDim0;

	int m_numNodeSim;
	int m_numNodeSim0;

};

#endif