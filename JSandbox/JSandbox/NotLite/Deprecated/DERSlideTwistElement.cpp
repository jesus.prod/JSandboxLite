/*=====================================================================================*/
/*!
\file		DERSlideTwistElement.cpp
\author		jesusprod
\brief		Implementation of DERSlideTwistElement.h
*/
/*=====================================================================================*/

#include <JSandbox/DERSlideTwistElement.h>

DERSlideTwistElement::DERSlideTwistElement() : DERSlideElement(11, 11)
{
	// Initialize edge-to-vertex matrix

	this->m_DeDv.resize(8, 11);

	this->m_DeDv.setZero();
	this->m_DeDv(3, 9) = 1.0;
	this->m_DeDv(7, 10) = 1.0;

	Matrix3d I;
	I.setIdentity();
	addBlock3x3From(0, 0, I * -1.0, this->m_DeDv);
	addBlock3x3From(4, 3, I * -1.0, this->m_DeDv);
	addBlock3x3From(0, 3, I, this->m_DeDv);
	addBlock3x3From(4, 6, I, this->m_DeDv);

	this->m_vpmat.resize(2);
}

DERSlideTwistElement::~DERSlideTwistElement()
{
	// Nothing to do here...
}

void DERSlideTwistElement::updateRest(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt)
{
	Vector3d c0 = get3D(this->m_vvinds[0], vx);
	Vector3d c1 = get3D(this->m_vvinds[1], vx);
	Vector3d c2 = get3D(this->m_vvinds[2], vx);
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;
	Real d0 = va[this->m_veinds[0]];
	Real d1 = va[this->m_veinds[1]];
	Real rt = vt[this->m_vvinds[1]];

	this->updateRest(e, f, d0, d1, rt);
}

void DERSlideTwistElement::updateRest(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt)
{
	this->m_l0 = this->computeVertexLength(e, f);
	this->m_t0 = this->computeVertexTwist(d0, d1, rt);
}

void DERSlideTwistElement::updateEnergy(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt)
{
	Real d0 = va[this->m_veinds[0]];
	Real d1 = va[this->m_veinds[1]];
	Real rt = vt[this->m_vvinds[1]];

	this->m_energy = this->computeEnergy(d0, d1, rt);
}

void DERSlideTwistElement::updateForce(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt)
{
	Vector3d c0 = get3D(this->m_vvinds[0], vx);
	Vector3d c1 = get3D(this->m_vvinds[1], vx);
	Vector3d c2 = get3D(this->m_vvinds[2], vx);
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;
	Real d0 = va[this->m_veinds[0]];
	Real d1 = va[this->m_veinds[1]];
	Real rt = vt[this->m_vvinds[1]];

	VectorXd vfe;
	this->computeForce(e, f, d0, d1, rt, vfe);
	this->m_vfVal = this->m_DeDv.transpose() * vfe;
}

void DERSlideTwistElement::updateJacobian(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt)
{
	Vector3d c0 = get3D(this->m_vvinds[0], vx);
	Vector3d c1 = get3D(this->m_vvinds[1], vx);
	Vector3d c2 = get3D(this->m_vvinds[2], vx);
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;
	Real d0 = va[this->m_veinds[0]];
	Real d1 = va[this->m_veinds[1]];
	Real rt = vt[this->m_vvinds[1]];

	MatrixXd mJe;
	this->computeJacobian(e, f, d0, d1, rt, mJe);
	MatrixXd mJ = this->m_DeDv.transpose() * mJe * this->m_DeDv;

	int count = 0;
	for (int i = 0; i < 11; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ(i, j);
}

Real DERSlideTwistElement::computeEnergy(Real d0, Real d1, Real rt)
{
	Real t = this->computeVertexTwist(d0, d1, rt);
	Real twistDiff = t - this->m_t0; // Twist def.

	return this->computeB() / (2 * this->m_l0) * twistDiff*twistDiff;
}

void DERSlideTwistElement::computeForce(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt, VectorXd& vf)
{
	VectorXd gT(8);
	this->computeGradTwist(e, f, gT);
	Real t = this->computeVertexTwist(d0, d1, rt);
	Real twistDiff = t - this->m_t0;

	Real kT = this->computeB();

	vf = gT * (kT / this->m_l0) * twistDiff * -1.0;
}

void DERSlideTwistElement::computeJacobian(const Vector3d& e, const Vector3d& f, Real d0, Real d1, Real rt, MatrixXd& mJ)
{
	VectorXd gT(8);
	MatrixXd hT(8, 8);
	this->computeGradTwist(e, f, gT);
	this->computeHessTwist(e, f, hT);
	Real t = this->computeVertexTwist(d0, d1, rt);
	Real twistDiff = t - this->m_t0;
	Real milen = -1.0 / this->m_l0;

	Real kT = this->computeB();

	mJ = hT * twistDiff + (gT*gT.transpose()) * milen * kT;
}

void DERSlideTwistElement::computeGradTwist(const Vector3d& e, const Vector3d& f, VectorXd& gT) const
{
	gT.resize(8);
	gT.setZero();

	Real norm_e = e.norm();
	Real norm_f = f.norm();
	Vector3d kb = this->computeKB(e, f);

	Vector3d dmde = kb * (0.5 / norm_e);
	Vector3d dmdf = kb * (0.5 / norm_f);
	for (int i = 0; i < 3; i++)
	{
		gT[0 + i] = dmde[i];
		gT[4 + i] = dmdf[i];
	}
	gT[3] = -1.0;
	gT[7] = 1.0;
}

void DERSlideTwistElement::computeHessTwist(const Vector3d& e, const Vector3d& f, MatrixXd& hT) const
{
	hT.resize(8, 8);
	hT.setZero();

	Vector3d te = e.normalized();
	Vector3d tf = f.normalized();
	Real norm_e = e.norm();
	Real norm_f = f.norm();
	Real norm_e2 = norm_e*norm_e;
	Real norm_f2 = norm_f*norm_f;
	Real norm_e3 = norm_e2*norm_e;
	Real norm_f3 = norm_f2*norm_f;
	Vector3d kb = this->computeKB(e, f);

	{
		Matrix3d I;
		I.setIdentity();

		Matrix3d DteDe = I * (1.0 / norm_e) - outerProduct(e, e) * (1.0 / norm_e3);
		Matrix3d DtfDf = I * (1.0 / norm_f) - outerProduct(f, f) * (1.0 / norm_f3);

		const Real chi = 1 + te.dot(tf);
		const Vector3d tilde_t = 1.0 / chi * (te + tf);
		const Matrix3d D2mDe2 = -0.25 / norm_e2 * (outerProduct(kb, te + tilde_t) + outerProduct(te + tilde_t, kb));
		const Matrix3d D2mDf2 = -0.25 / norm_f2 * (outerProduct(kb, tf + tilde_t) + outerProduct(tf + tilde_t, kb));
		const Matrix3d D2mDeDf = 0.5 / (norm_e * norm_f) * (2.0 / chi * crossMatrix(te) - outerProduct(kb, tilde_t));
		const Matrix3d D2mDfDe = D2mDeDf.transpose();

		addBlock3x3From(0, 0, D2mDe2, hT);
		addBlock3x3From(4, 4, D2mDf2, hT);
		addBlock3x3From(0, 4, D2mDeDf, hT);
		addBlock3x3From(4, 0, D2mDfDe, hT);
	}
}

Real DERSlideTwistElement::computeVertexLength(const Vector3d& e, const Vector3d& f) const
{
	return (e.norm() + f.norm())*0.5;
}

Real DERSlideTwistElement::computeVertexTwist(Real d0, Real d1, Real rt) const
{
	return rt + d1 - d0;
}

Vector3d DERSlideTwistElement::computeKB(const Vector3d& e, const Vector3d& f) const
{
	Vector3d t0 = e.normalized();
	Vector3d t1 = f.normalized();
	return (t0.cross(t1)) * 2 * (1 / (1 + t0.dot(t1)));
}

Real DERSlideTwistElement::computeB() const
{
	Real B = 0;

	Real S = 0.5*(this->m_vpmat[0]->getShear() + this->m_vpmat[1]->getShear());
	Real w = 0.5*(this->m_vpmat[0]->getWRadius() + this->m_vpmat[1]->getWRadius());
	Real h = 0.5*(this->m_vpmat[0]->getHRadius() + this->m_vpmat[1]->getHRadius());
	Real A = M_PI * w * h;
	B = 0.25 * S * A * (w * w + h * h);

	return B;
}

Real DERSlideTwistElement::getConstant() const
{
	return this->computeB() / this->m_l0; // Stiff.
}