/*=====================================================================================*/
/*!
\file		SurfaceRemesherARC.h
\author		jesusprod
\brief		Implementation of the Adaptative Anisotropic Remeshing mechanism from the paper
			"Adaptive Anisotropic Remeshing for Cloth Simulation", Rahul Narain, Armin Samiii,
			J. O'Brien. SIG2012.
			
*/
/*=====================================================================================*/


#ifndef SURFACE_REMESHER_ARC_H
#define SURFACE_REMESHER_ARC_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/Geometry/SurfaceRemesher.h>
#include <JSandbox/SRSizingElement.h>

class SurfaceRemesherARC : public SurfaceRemesher
{
public:

	SurfaceRemesherARC();
	virtual ~SurfaceRemesherARC();

	virtual const RemeshOP& remesh(TriMesh* pMesh, BVHTree* pTree = NULL);

	virtual void addSizingElement(SRSizingElement* pEle);

protected:

	virtual bool computeSizing();
	//virtual RemeshOP collapseEdges(vector<MeshTraits::ID>& vacset);
	virtual RemeshOP splitEdges(vector<MeshTraits::ID>& vacset);
	virtual RemeshOP flipEdges(vector<MeshTraits::ID>& vacset);

	// Edge collapse --------------------------------------------------------------------------------------------------

	// TODO

	// Edge split -----------------------------------------------------------------------------------------------------

	virtual bool findEdgesToSplit(vector<MeshTraits::ID>& vacset, vector<MeshTraits::ID>& vsplit) const;
	virtual double getSplitMetric(const MeshTraits::ID& eh) const;
	virtual RemeshOP splitEdge(const MeshTraits::ID& eh);

	// Edge flip -----------------------------------------------------------------------------------------------------

	virtual bool findEdgesToFlip(vector<MeshTraits::ID>& vactive, vector<MeshTraits::ID>& vflip) const;
	virtual double getFlipMetric(const MeshTraits::ID& eh) const;
	virtual RemeshOP flipEdge(const MeshTraits::ID& eh);

	// Utitilities ----------------------------------------------------------------------------------------------------

	virtual Vector3d computeMidpoint(const MeshTraits::ID& eh, const string& pname) const;
	
	vector<SurfaceSizing> m_vVerSizing;
	vector<SRSizingElement*> m_vpElements;

};

#endif