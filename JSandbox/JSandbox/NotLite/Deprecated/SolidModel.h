/*=====================================================================================*/
/*! 
\file		SolidModel.h
\author		jesusprod
\brief		Basic abstract implementation of SolidModel to inherit from.
			All solid model should inherit from this abstract class. It
			includes functions for elements and materials management 
			and parallel computation of force and Jacobian.
 */
/*=====================================================================================*/

#ifndef SOLID_MODEL_H
#define SOLID_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidElement.h>
#include <JSandbox/SolidMaterial.h>


class JSANDBOX_EXPORT SolidModel
{
public:
	SolidModel();

	virtual ~SolidModel() { freeElements(); freeMaterials(); }

	virtual int getRawDOFNumber() const { return this->m_nrawDOF; }
	virtual int getNumEle() const { return this->m_numEle; }

	virtual void setup();

	virtual void configureMaterial(bool isHomogeneous, const vector<SolidMaterial*>& vpMats);

	virtual const bVector& getGravityStencil() const { return m_gs; }
	virtual const bVector& getActiveStencil() const { return m_as; }

	virtual void peekState(int i);
	virtual void pushState();
	virtual void popState();

	virtual const tVector& getSparseMassMatrix() const { return this->m_mm; }
	virtual const VectorXd& getLumpedMassVector() const { return this->m_vm; }
	virtual const VectorXd& getPositions() const { return this->m_vx; }
	virtual const VectorXd& getVelocities() const { return this->m_vv; }
	virtual const VectorXd& getPositions0() const { return this->m_vX; }

	virtual bool isMaterialHomogeneous() const { return m_isMatHomo; }
	virtual const SolidMaterial *getMaterial() const { return this->m_vpMats[0]; }
	
	virtual SolidElement* getElement(int i) { assert(i >= 0 && i < this->m_numEle); return this->m_vpEles[i]; }
	virtual SolidMaterial* getMaterial(int i) { assert(i >= 0 && i < this->m_numEle); return this->m_vpMats[i]; }
	
	virtual const SolidElement* getElement(int i) const { assert(i >= 0 && i < this->m_numEle); return this->m_vpEles[i]; }
	virtual const SolidMaterial *getMaterial(int i) const { assert(i >= 0 && i < this->m_numEle); return this->m_vpMats[i]; }
	
	virtual int getNumNonZeros_Jacobian() const { return this->m_nNZ_Jacobian; }

	virtual void updateRest();
	virtual void updateEnergy();
	virtual void updateForce();
	virtual void updateJacobian();
	virtual void updateEnergyForce();
	virtual void updateForceJacobian();
	virtual void updateEnergyForceJacobian();

	virtual void testForceLocal();
	virtual void testJacobianLocal();

	virtual bool isReady_Setup() const { return this->m_isSetupReady; }
	virtual bool isReady_Rest() const { return this->m_isRestReady; }
	virtual bool isReady_Force() const { return this->m_isForceReady; }
	virtual bool isReady_Energy() const { return this->m_isEnergyReady; }
	virtual bool isReady_Jacobian() const { return this->m_isJacobianReady; }
	
	virtual void updateStatex(const VectorXd& vx, const VectorXd& vv);
	virtual void updateState0(const VectorXd& vX, const VectorXd& vV);

	virtual Real getEnergy();
	virtual void addForce(VectorXd& vf, const vector<bool>* pvFixed = NULL);
	virtual void addJacobian(tVector& vJ, const vector<bool>* pvFixed = NULL);

	virtual void visualizeEnergy(dVector& vcol) = 0;
	virtual void visualizeForce(dVector& vcol) = 0;
	
protected:
	
	virtual void freeElements();
	virtual void freeMaterials();
	virtual void initializeElements() = 0;
	virtual void initializeMaterials();
	virtual void precompute_Jacobian();

	virtual void preSetupActions() { /* Nothing to do here */ }
	virtual void posSetupActions() { /* Nothing to do here */ }
	

protected:

	bVector m_gs;
	bVector m_as;

	int m_nrawDOF;
	int m_nrawDOF0;

	int m_numEle; 

	VectorXd m_vX;
	VectorXd m_vV;
	VectorXd m_vx;
	VectorXd m_vv;

	VectorXd m_vm;	

	tVector m_mm;

	int m_maxStackSize;
	vector<VectorXd> m_vxStack;
	vector<VectorXd> m_vvStack;

	bool m_isSetupReady;
	bool m_isForceReady;
	bool m_isEnergyReady;
	bool m_isJacobianReady;
	bool m_isRestReady;
	bool m_isMatHomo;

	vector<SolidMaterial*> m_vpMats;
	vector<SolidElement*> m_vpEles;
	
	int m_nNZ_Jacobian;	

};

#endif