/*=====================================================================================*/
/*!
\file		DERSlideModel.h
\author		jesusprod
\brief		DER10SlideModel.h implementation.
*/
/*=====================================================================================*/

#ifndef DER_SLIDE_MODEL_H
#define DER_SLIDE_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidModel.h>
#include <JSandbox/SolidElement.h>
#include <JSandbox/SolidMaterial.h>

#include <JSandbox/DERSlideStretchElement.h>
#include <JSandbox/DERSlideBendingElement.h>
#include <JSandbox/DERSlideTwistElement.h>

#include <JSandbox/BasicSlideStretchElement.h>
#include <JSandbox/BasicSlideBendingElement.h>

#include <JSandbox/Geometry/Curve.h>

typedef struct RodNode
{
	int m_index;

	iVector m_vsimIdx;
	iVector m_vgloIdx;

	Vector3d m_vx;
	Vector3d m_vv;
	Real m_posAL;
	Real m_velAL;
	bool m_isSliding;
	bool m_isActive;
} RodNode;

typedef struct RodEdge
{
	int m_index;

	iVector m_vsimIdx;
	iVector m_vgloIdx;

	int m_nodePrev;
	int m_nodeNext;

	Frame m_refFrame;
	Frame m_matFrame;
	Real m_matAngle;
} RodEdge;

// Sort nodes with local index

typedef struct LT_RodNode_ID
{
	bool operator()(RodNode rn0, RodNode rn1)
	{
		return rn0.m_index < rn1.m_index;
	}
} LT_RodNode_ID;

// Sort nodes with arc length

typedef struct LT_RodNode_AL
{
	bool operator()(RodNode rn0, RodNode rn1)
	{	
		return (!isApprox(rn0.m_posAL, rn1.m_posAL, 1e-6) && (rn0.m_posAL < rn1.m_posAL)) ||
			   (isApprox(rn0.m_posAL, rn1.m_posAL, 1e-6) && (!rn0.m_isActive && rn1.m_isActive)) ||
			   (isApprox(rn0.m_posAL, rn1.m_posAL, 1e-6) && (!rn0.m_isSliding && rn1.m_isSliding));
	}
} LT_RodNode_AL;


class JSANDBOX_EXPORT RodNodeList
{

public:
	int getNumNodes() const { return (int) this->m_vnodes.size(); }
	void setNodes(const vector<RodNode>& vn) { m_vnodes = vn;  }
	const vector<RodNode>& getNodes() const { return m_vnodes; }
	vector<RodNode>& getNodes() { return m_vnodes; }

	vector<RodNode> getActiveNodes() const
	{
		vector<RodNode> vnodes;
		for (int i = 0; i < (int) this->m_vnodes.size(); ++i)
			if (this->m_vnodes[i].m_isActive) // Add active
				vnodes.push_back(this->m_vnodes[i]);
		return vnodes;
	}

	vector<RodNode> getSlidingNodes() const
	{
		vector<RodNode> vnodes;
		for (int i = 0; i < (int) this->m_vnodes.size(); ++i)
			if (this->m_vnodes[i].m_isSliding) // Add sliding
				vnodes.push_back(this->m_vnodes[i]);
		return vnodes;
	}

	static void SortNodeList_AL(vector<RodNode>& nodeList) 
	{
		sort(nodeList.begin(), nodeList.end(), LT_RodNode_AL());
	}

	static void SortNodeList_ID(vector<RodNode>& nodeList)
	{
		sort(nodeList.begin(), nodeList.end(), LT_RodNode_ID());
	}

protected:
	vector<RodNode> m_vnodes;

};

class JSANDBOX_EXPORT RodEdgeList
{

public:
	int getNumEdges() { return (int) this->m_vedges.size(); }
	void setEdges(const vector<RodEdge>& vn) { m_vedges = vn; }
	const vector<RodEdge>& getEdges() const { return m_vedges; }
	vector<RodEdge>& getEdges() { return m_vedges; }
	
protected:
	vector<RodEdge> m_vedges;

};


class JSANDBOX_EXPORT DERSlideModel : public SolidModel
{
public:
	DERSlideModel();

	virtual ~DERSlideModel();

	virtual void setup();

	virtual void configureNodeList(const RodNodeList& list, bool isClosed);
	virtual void configureMaterial(SolidMaterial mat, bool isHomogeneous);

	virtual void peekState(int i);
	virtual void pushState();
	virtual void popState();

	virtual void updateStatex(const VectorXd& vX, const VectorXd& vV);
	virtual void updateState0(const VectorXd& vX, const VectorXd& vV);

	virtual int getRawDOFNumber() { return this->m_simDOF; }

	virtual iVector getSimDOF() { return this->m_vsimDOF; }

	virtual const RodNodeList& getNodeList() const { return this->m_defoNodeList; }

	virtual void updateMass();
	virtual void updateRest();
	virtual void updateForce();
	virtual void updateEnergy();
	virtual void updateJacobian();
	virtual void updateEnergyForce();
	virtual void updateForceJacobian();
	virtual void updateEnergyForceJacobian();
	
	virtual Real getEnergy();
	virtual void addForce(VectorXd& vf, const vector<bool>* pvFixed = NULL);
	virtual void addJacobian(tVector& vJ, const vector<bool>* pvFixed = NULL);

	virtual void visualizeEnergy(dVector& vcol);
	virtual void visualizeForce(dVector& vcol);

	virtual vector<RodNode> getSlidingNodes() const { return this->m_defoNodeList.getSlidingNodes(); }
	virtual vector<RodNode> getActiveNodes() const { return this->m_defoNodeList.getActiveNodes(); }

	//virtual const dVector& getAngles0() const { return this->m_va0; }
	//virtual const Curve& getCenterline0() const { return this->m_centerline0; }
	//virtual const vector<Frame>& getRefFrames0() const { return this->m_vrefFrame0; }
	//virtual const vector<Frame>& getMatFrames0() const { return this->m_vmatFrame0; }
	//virtual const dVector& getMaterialCoordinates0() const { return this->m_vs0; }

	//virtual const dVector& getAngles() const { return this->m_va; }
	//virtual const Curve& getCenterline() const { return this->m_centerline; }
	//virtual const vector<Frame>& getRefFrames() const { return this->m_vrefFrame; }
	//virtual const vector<Frame>& getMatFrames() const { return this->m_vmatFrame; }
	//virtual const dVector& getMaterialCoordinates() const { return this->m_vs; }

	//virtual int vertexToDOF(int i) const { return 3*i; }
	//virtual int angleToDOF(int i) const { return 3 * m_nv + i; }
	//virtual int materialToDOF(int i) const { return 3 * m_nv + m_na + i; }

protected:

	virtual void updateNodesState(RodNodeList& nodeList);
	virtual void updateActiveNodes(RodNodeList& nodeList);
	
	virtual void updateEdgeStructure(RodNodeList& nodeList, RodEdgeList& edgeList);
	virtual void updateSimulationDOF(RodNodeList& nodeList, RodEdgeList& edgeList);

	virtual void initializeDOFVector();
	virtual void initializeMaterials();

	virtual void initializeElements() {} 

	//virtual void initializeFrames();
	//virtual void precompute_Jacobian();
	//virtual void updateRodsCenter();
	//virtual void updateRodsAngles();
	//virtual void updateMaterialCoordinates();
	//virtual void updateRodsReferenceFrame();
	//virtual void updateRodsReferenceTwist();
	//virtual void updateRodsMaterialFrame();

protected:

	// Raw discretization

	RodNodeList m_rawNodeList;
	double m_rawLength;
	bool m_isClosed;

	// Raw material

	SolidMaterial m_rawMat;
	bool m_isMatHomogeneous;

	// Local storage

	Real m_energy;						// Local stored energy
	VectorXd m_vfVal;					// Local stored force
	tVector m_vJVal;		// Local stored Jacobiam

	vector<RodNodeList> m_vNodeListStack;

	// DOFS

	iVector m_vsimDOF;

	int m_gloDOF;
	int m_simDOF;

	int m_numActive;
	int m_numSliding;

	int m_nActive;
	int m_nSliding;
	int m_numRodNodes;
	int m_numRodEdges;

	RodNodeList m_defoNodeList;
	RodEdgeList m_defoEdgeList;

	RodNodeList m_restNodeList;
	RodEdgeList m_restEdgeList;

	BasicSlideStretchElement* m_pStretchEle;
	BasicSlideBendingElement* m_pBendingEle;


	// To use later

	//Real m_l0;
	//int m_nrawDOF0;
	//int m_nv0;
	//int m_ne0;
	//int m_ns0;

	//int m_nv;
	//int m_ne;
	//int m_ns;

	//dVector m_vArcLen;

	//vector<Frame> m_vrestRefFrames;
	//vector<Frame> m_vrestMatFrames;
	//dVector m_vrestRefTwist;
	//dVector m_vrestMatAngle;

	//vector<Frame> m_vdefoRefFrames;
	//vector<Frame> m_vdefoMatFrames;
	//dVector m_vdefoRefTwist;
	//dVector m_vdefoMatAngle;

	// To deprecate ----------------------

	//Curve m_rawCurve;
	//bVector m_vslide;

	//int m_nv;
	//int m_na;

	//SolidMaterial m_rawMat;

	//int m_numSlide;
	//iVector m_vsimDOF;

	//int m_offsetP;
	//int m_offsetA;
	//int m_offsetS;
	//
	//dVector m_vp;
	//dVector m_va;
	//dVector m_vs;

	//Curve m_centerline;
	//vector<Frame> m_vrefFrame;
	//vector<Frame> m_vmatFrame;
	//dVector m_vt;

	//dVector m_vp0;
	//dVector m_va0;
	//dVector m_vs0;

	//Curve m_centerline0;
	//vector<Frame> m_vrefFrame0;
	//vector<Frame> m_vmatFrame0;
	//dVector m_vt0;

	//int m_numStretchEle;
	//int m_numBendingEle;
	//int m_numTwistEle;

	//vector<DERSlideStretchElement*> m_vpStretchEle;
	//vector<DERSlideBendingElement*> m_vpBendingEle;
	//vector<DERSlideTwistElement*> m_vpTwistEle;

};

#endif

///*=====================================================================================*/
///*!
//\file		DERSlideModel.h
//\author		jesusprod
//\brief		DER10SlideModel.h implementation.
//*/
///*=====================================================================================*/
//
//#ifndef DER_SLIDE_MODEL_H
//#define DER_SLIDE_MODEL_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//#include <JSandbox/SolidModel.h>
//#include <JSandbox/SolidElement.h>
//#include <JSandbox/SolidMaterial.h>
//
//#include <JSandbox/DERSlideStretchElement.h>
//#include <JSandbox/DERSlideBendingElement.h>
//#include <JSandbox/DERSlideTwistElement.h>
//
//#include <JSandbox/Curve.h>
//
//
//class JSANDBOX_EXPORT DERSlideModel : SolidModel
//{
//public:
//	DERSlideModel();
//
//	virtual ~DERSlideModel();
//
//	virtual void setup();
//
//	virtual void configureCurve(const Curve& curve, const bVector& vslide);
//	virtual void configureMaterial(SolidMaterial mat, bool isHomogeneous);
//
//	virtual void updateStatex(const VectorXd& vX, const VectorXd& vV);
//	virtual void updateState0(const VectorXd& vX, const VectorXd& vV);
//
//	virtual void updateMass();
//	virtual void updateRest();
//	virtual void updateForce();
//	virtual void updateEnergy();
//	virtual void updateJacobian();
//	virtual void updateEnergyForce();
//	virtual void updateForceJacobian();
//	virtual void updateEnergyForceJacobian();
//
//	virtual void visualizeEnergy(dVector& vcol);
//	virtual void visualizeForce(dVector& vcol);
//
//	virtual const dVector& getAngles0() const { return this->m_va0; }
//	virtual const Curve& getCenterline0() const { return this->m_centerline0; }
//	virtual const vector<Frame>& getRefFrames0() const { return this->m_vrefFrame0; }
//	virtual const vector<Frame>& getMatFrames0() const { return this->m_vmatFrame0; }
//	virtual const dVector& getMaterialCoordinates0() const { return this->m_vs0; }
//
//	virtual const dVector& getAngles() const { return this->m_va; }
//	virtual const Curve& getCenterline() const { return this->m_centerline; }
//	virtual const vector<Frame>& getRefFrames() const { return this->m_vrefFrame; }
//	virtual const vector<Frame>& getMatFrames() const { return this->m_vmatFrame; }
//	virtual const dVector& getMaterialCoordinates() const { return this->m_vs; }
//
//	virtual int vertexToDOF(int i) const { return 3*i; }
//	virtual int angleToDOF(int i) const { return 3 * m_nv + i; }
//	virtual int materialToDOF(int i) const { return 3 * m_nv + m_na + i; }
//
//protected:
//
//	virtual void initializeDOFS();
//	virtual void initializeFrames();
//	virtual void initializeElements();
//	virtual void initializeMaterials();
//	virtual void precompute_Jacobian();
//
//	virtual void updateRodsCenter();
//	virtual void updateRodsAngles();
//	virtual void updateMaterialCoordinates();
//	virtual void updateRodsReferenceFrame();
//	virtual void updateRodsReferenceTwist();
//	virtual void updateRodsMaterialFrame();
//
//protected:
//
//	Curve m_rawCurve;
//	bVector m_vslide;
//
//	int m_nv;
//	int m_na;
//
//	SolidMaterial m_rawMat;
//
//	int m_numSlide;
//	iVector m_vsimDOF;
//
//	int m_offsetP;
//	int m_offsetA;
//	int m_offsetS;
//	
//	dVector m_vp;
//	dVector m_va;
//	dVector m_vs;
//
//	Curve m_centerline;
//	vector<Frame> m_vrefFrame;
//	vector<Frame> m_vmatFrame;
//	dVector m_vt;
//
//	dVector m_vp0;
//	dVector m_va0;
//	dVector m_vs0;
//
//	Curve m_centerline0;
//	vector<Frame> m_vrefFrame0;
//	vector<Frame> m_vmatFrame0;
//	dVector m_vt0;
//
//	int m_numStretchEle;
//	int m_numBendingEle;
//	int m_numTwistEle;
//
//	vector<DERSlideStretchElement*> m_vpStretchEle;
//	vector<DERSlideBendingElement*> m_vpBendingEle;
//	vector<DERSlideTwistElement*> m_vpTwistEle;
//
//};
//
//#endif