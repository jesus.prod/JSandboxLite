/*=====================================================================================*/
/*! 
\file		DSHingeElement.h
\author		jesusprod,bthomasz
\brief		Basic implementation of a discrete-shell bending element. Adapted from bthomasz
			implementation for Phys3D DeformablesSurface plugin. This provides a simple
			quadratic energy density for angle preserving between pairs of triangles. It 
			relies on Maple auto-differentiation tool for force and Jacobian expressions. 
			It uses SolidMaterial bending K as elasticity coefficient.
*/
/*=====================================================================================*/

#ifndef DS_HINGE_ELEMENT_H
#define DS_HINGE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>

class JSANDBOX_EXPORT DSHingeElement : public NodalSolidElement
{
public:
	DSHingeElement(int dim0);
	~DSHingeElement(); 

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getIntegrationVolume() const { return m_gFac0; }

	virtual Real getRestAngle() const { return this->m_phi0; }
	virtual Real getRestFactor() const { return this->m_gFac0; }

protected:
	Real m_phi0;
	Real m_gFac0;

};

#endif