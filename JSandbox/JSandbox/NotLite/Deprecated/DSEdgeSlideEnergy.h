///*=====================================================================================*/
///*!
//\file		DSEdgeSlideElement.h
//\author		jesusprod
//\brief		Basic implementation of a discrete-shell edge element allowed to slide. This 
//			provides a simple non-linear energy density for edge length preserving. It 
//			uses SolidMaterial stretch K.
//*/
///*=====================================================================================*/
//
//#ifndef DS_EDGE_SLIDE_ELEMENT_H
//#define DS_EDGE_SLIDE_ELEMENT_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//#include <JSandbox/SolidMaterial.h>
//#include <JSandbox/NodalSolidElement.h>
//
//class DSEdgeSlideElement : public NodalSolidElement
//{
//public:
//	DSEdgeSlideElement(int dim0);
//	~DSEdgeSlideElement();
//
//	virtual void updateRest(const VectorXd& vX) { /* Explicity every time */} 
//
//	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
//	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
//	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);
//
//	virtual Real getIntegrationVolume() const { return -1.0; }
//
//protected:
//
//};
//
//#endif