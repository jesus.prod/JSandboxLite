/*=====================================================================================*/
/*!
\file		BConditionPreStrain.h
\author		jesusprod
\brief		Boundary condition to prestrain solid models by modifiying their rest
			strain by a given factor. For prestrain 1.0, the model should behave
			normally.
*/
/*=====================================================================================*/

#ifndef BCONDITION_PRESTRAIN_H
#define BCONDITION_PRESTRAIN_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/BCondition.h>
#include <JSandbox/SolidModel.h>

class JSANDBOX_EXPORT BConditionPreStrain : public BCondition
{
public:

	BConditionPreStrain(SolidModel* pModel);

	virtual ~BConditionPreStrain();

	// <BCondition>

	virtual void constrain(SolidModel* pModel) const;

	// </BCondition>

	virtual void setFactor(Real sf) { this->m_factor = sf; }
	virtual Real getFactor() const { return this->m_factor; }

protected:

	double m_factor;

};

#endif