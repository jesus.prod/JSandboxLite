/*=====================================================================================*/
/*! 
\file		FEMElementNeoHookean.h
\author		jesusprod
\brief		Non-linear Neo-Hookean model
				- Parameters: Lam� coefficients (m,l)
				- Deformation metric: with I1 = tr(F^T*F), J = sqrt(det(F^T*F)) 
				- Energy density: Psi(F) = 0.5*mu*(I1 - 3) - mu*log(J) + 0.5*l*log^2(J)
				- First Piola-Kirchhoff: P(F) = mu*F - mu*F^-T + l*log(J)*F^-T
 */
/*=====================================================================================*/

#ifndef FEM_MODEL_NEO_HOOKEAN_H
#define FEM_MODEL_NEO_HOOKEAN_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/FEMElement.h>
#include <JSandbox/SolidMaterial.h>

class FEMElementNeoHookean : public FEMElement
{
	// TODO: Implement this
};

#endif