/*=====================================================================================*/
/*! 
\file		SolidElement.cpp
\author		jesusprod
\brief		Implementation of SolidElement.h
 */
/*=====================================================================================*/

#include <JSandbox/SolidElement.h>

#include <JSandbox/MathUtils.h>

Real SolidElement::getEnergy() const
{
	return this->m_energy;
}

Real SolidElement::getEnergyDensity() const
{
	return this->m_energy/this->getIntegrationVolume();
}

const VectorXd& SolidElement::getLocalForce() const
{
	return this->m_vfVal;
}

const VectorXd& SolidElement::getLocalJacobian() const
{
	return this->m_vJVal;
} 

void SolidElement::addMass(VectorXd& vm) const
{
	for (int i = 0; i < this->m_nrawDOF; ++i)
		vm(this->m_vidx[i]) += this->m_vmass(i);
}

void SolidElement::addForce(VectorXd& vf, const vector<bool>* pvFixed) const
{
	if (pvFixed != NULL)
	{
		for (int i = 0; i < this->m_nrawDOF; ++i)
		{
			int idx = this->m_vidx[i];
			if ((*pvFixed)[idx])
				continue;

			vf(idx) += this->m_vfVal[i];
		}
	} 
	else
	{
		for (int i = 0; i < this->m_nrawDOF; ++i)
			vf(this->m_vidx[i]) += this->m_vfVal[i];
	}
}

void SolidElement::addJacobian(tVector& vJ, const vector<bool>* pvFixed) const
{
	if (pvFixed != NULL)
	{
		int count = 0;
		for (int i = 0; i < this->m_nrawDOF; ++i) 
		{
			int idxi = this->m_vidx[i];
			if ((*pvFixed)[idxi])
			{
				count+=i+1;
				continue;
			}

			for (int j = 0; j <= i; ++j) // Sym
			{
				int idxj = this->m_vidx[j];
				if ((*pvFixed)[idxj])
				{
					count++;
					continue;
				}

				int r,c;
				if (idxi >= idxj)
				{
					r = idxi;
					c = idxj;
				}
				else
				{ 
					c = idxi;
					r = idxj;
				}
				vJ.push_back(Triplet<Real>(r, c, this->m_vJVal(count)));
				count++;
			}	
		}
	}
	else
	{
		int count = 0;
		for (int i = 0; i < this->m_nrawDOF; ++i) 
			for (int j = 0; j <= i; ++j) // Sym
			{
				int idxi = this->m_vidx[i];
				int idxj = this->m_vidx[j];

				int r,c;
				if (idxi >= idxj)
				{
					r = idxi;
					c = idxj;
				}
				else
				{ 
					c = idxi;
					r = idxj;
				}
				vJ.push_back(Triplet<Real>(r, c, this->m_vJVal(count)));
				count++;
			}	
	}
}

void SolidElement::updateEnergyForce(const VectorXd& vx, const VectorXd& vv)
{
	this->updateEnergy(vx, vv);
	this->updateForce(vx, vv);
}

void SolidElement::updateForceJacobian(const VectorXd& vx, const VectorXd& vv)
{
	this->updateForce(vx, vv);
	this->updateJacobian(vx, vv);
}

void SolidElement::updateEnergyForceJacobian(const VectorXd& vx, const VectorXd& vv)
{
	this->updateEnergy(vx, vv);
	this->updateForce(vx, vv);
	this->updateJacobian(vx, vv);
} 

void SolidElement::deallocateSparseEntries(int& n)
{
	n -= ((this->m_nrawDOF + 1)*this->m_nrawDOF) / 2.0;
}

void SolidElement::allocateSparseEntries(int& n)
{
	n += ((this->m_nrawDOF + 1)*this->m_nrawDOF)/2.0;
}

Real SolidElement::testForceLocal(const VectorXd& vx, const VectorXd& vv)
{
	Real EPS = 1e-6;

	VectorXd vxFD = vx;

	VectorXd vfFD(this->m_nrawDOF);
	vfFD.setZero(); // Zero force

	for (int i = 0; i < (int) this->m_vidx.size(); ++i)
	{
		// Plus
		vxFD(this->m_vidx[i]) += EPS;
		this->updateEnergy(vxFD, vv);
		Real ep = this->getEnergy();
		vxFD(this->m_vidx[i]) -= EPS;

		// Minus
		vxFD(this->m_vidx[i]) -= EPS;
		this->updateEnergy(vxFD, vv);
		Real em = this->getEnergy();
		vxFD(this->m_vidx[i]) += EPS;

		vfFD[i] = -(ep - em)/(2*EPS);
	}

	this->updateForce(vx, vv); // Analytical 
	const VectorXd& vfA = this->getLocalForce();

	dVector vfASTL = toSTL(vfA);
	dVector vfFDSTL = toSTL(vfFD);

	// Compute difference

	VectorXd vfDiff = (vfA - vfFD);
	Real FDNorm = vfFD.norm();
	Real diffNorm = vfDiff.norm() / FDNorm;

	if (FDNorm >= 1e-9)
		return diffNorm;
	else return 0;

	//if (FDNorm > 1e-9 && diffNorm > 1e-5) // Trace to much difference on the force
	//	qDebug("In %s: Difference on force %.9f\n", __FUNCDNAME__, diffNorm);
}

Real SolidElement::testJacobianLocal(const VectorXd& vx, const VectorXd& vv)
{
	Real EPS = 1e-6;

	VectorXd vxFD = vx;

	MatrixXd mJFD(this->m_nrawDOF, this->m_nrawDOF);
	mJFD.setZero(); // Start with zero Jacobian

	for (int i = 0; i < (int) this->m_vidx.size(); ++i)
	{
		// Plus
		vxFD(this->m_vidx[i]) += EPS;
		this->updateForce(vxFD, vv);
		VectorXd fp = this->getLocalForce();
		vxFD(this->m_vidx[i]) -= EPS;

		// Minus
		vxFD(this->m_vidx[i]) -= EPS;
		this->updateForce(vxFD, vv);
		VectorXd fm = this->getLocalForce();
		vxFD(this->m_vidx[i]) += EPS;

		VectorXd fDiff = (fp - fm)/(2*EPS);

		for (int j = 0; j < this->m_nrawDOF; ++j)
			mJFD(j, i) = fDiff(j); // Fill matrix
	}

	this->updateJacobian(vx, vv); // Analytical 
	const VectorXd& vJA = this->getLocalJacobian();

	int count = 0;
	VectorXd vJFD((int) this->m_vJVal.size());
	for (int i = 0; i < this->m_nrawDOF; ++i)
		for (int j = 0; j <= i; ++j)
			vJFD(count++) = mJFD(i,j);

	// Compute difference

	VectorXd vJDiff = (vJA - vJFD);
	Real FDNorm = vJFD.norm();
	Real diffNorm = vJDiff.norm() / FDNorm;

	if (FDNorm >= 1e-9)
		return diffNorm;
	else return 0;

	//if (FDNorm > 1e-9 && diffNorm > 1e-5) // Trace to much difference on the force
	//	qDebug("In %s: Difference on Jacobian %.9f\n", __FUNCDNAME__, diffNorm);
}