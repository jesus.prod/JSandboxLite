/*=====================================================================================*/
/*! 
\file		SurfaceModel.cpp
\author		jesusprod
\brief		Implementation of SurfaceModel.h
 */
/*=====================================================================================*/

#include <JSandbox/SurfaceModel.h>
#include <JSandbox/FEMElement.h>
#include <JSandbox/DSEdgeElement.h>
#include <JSandbox/DSAreaElement.h>
#include <JSandbox/DSHingeElement.h>
#include <JSandbox/FEMElementLinearCorot.h>
#include <JSandbox/FEMElementMooneyRivlin.h>
#include <JSandbox/FEMElementNeoHookean.h>
#include <JSandbox/FEMElementOgden.h>
#include <JSandbox/FEMElementStVK.h>

SurfaceModel::SurfaceModel()
{
	this->m_pSimMesh = NULL;
}

SurfaceModel::~SurfaceModel()
{
	if (this->m_pSimMesh != NULL)
		delete this->m_pSimMesh;

	for (int i = 0; i < (int) this->m_vDynRemesher.size(); ++i)
		delete this->m_vDynRemesher[i];
	this->m_vDynRemesher.clear();

	for (int i = 0; i < (int) this->m_vStaRemesher.size(); ++i)
		delete this->m_vStaRemesher[i];
	this->m_vStaRemesher.clear();
}

void SurfaceModel::configureDeformationModel(Model DM)
{
	this->m_DM = DM;
}

void SurfaceModel::configureDiscretization(const dVector& vpos, const iVector& vidx, SurfaceModel::Element E)
{
	vector<string> vSimProp;

	switch (E)
	{
	case SurfaceModel::Tri1:
		this->m_pSimMesh = new TriMesh();
		vSimProp.push_back(VPropSim::POSE);
		vSimProp.push_back(VPropSim::VELE);
		vSimProp.push_back(VPropSim::POS0);

		this->m_pSimMesh->init(vpos, vidx, vSimProp);
		this->m_vrawPos = vpos;
		this->m_vrawIdx = vidx;
		break;

	case SurfaceModel::Quad2: assert(false); break;

	default: assert(false); // Should not happen
	}
}
	
void SurfaceModel::preSetupActions()
{
	for (int i = 0; i < (int) this->m_vStaRemesher.size(); ++i)
	{
		RemeshOP remeshOP = this->m_vStaRemesher[i]->remesh(this->m_pSimMesh);
		if (remeshOP.m_OK)
		{
			delete this->m_pSimMesh; // Exchange
			this->m_pSimMesh = remeshOP.m_pMesh;
		}
	}

	// Reinitialize raw data

	this->m_pSimMesh->getPoints(this->m_vrawPos);
	this->m_pSimMesh->getIndices(this->m_vrawIdx);
}

void SurfaceModel::posSetupActions()
{
	this->m_isMeshReady = false;
}

void SurfaceModel::initializeElements()
{
	switch (this->m_DM)
	{
	case SurfaceModel::QuadArea: this->initializeElements_QuadArea(); break;

	case SurfaceModel::MS_QuadArea: this->initializeElements_MS_QuadArea(); break;

	case SurfaceModel::DS_MS_QuadArea: this->initializeElements_DS_MS_QuadArea(); break;

	case SurfaceModel::DS_FEMLinearCorot:
	case SurfaceModel::DS_FEMMooneyRivlin: 
	case SurfaceModel::DS_FEMNeoHookean: 
	case SurfaceModel::DS_FEMOgden: 
	case SurfaceModel::DS_FEMStVK: 
		this->initializeElements_DS_FEM(); // Not ready
		break;

	default: assert(false); // Should not happen
	}

	// Initialize state vectors

	this->m_vx.resize(this->m_nrawDOF);
	this->m_vv.resize(this->m_nrawDOF);
	this->m_vX.resize(this->m_nrawDOF);

	for (int i = 0; i < this->m_nrawDOF; ++i)
	{
		this->m_vx(i) = this->m_vrawPos[i];
		this->m_vX(i) = this->m_vrawPos[i];
	}
	this->m_vv.setZero();
}

void SurfaceModel::initializeElements_QuadArea()
{
	int nf = (int) this->m_pSimMesh->n_faces();
	int nv = (int) this->m_pSimMesh->n_vertices();
	this->m_numEle = nf; // Area elements
	this->m_numNodeSim = nv;
	this->m_nrawDOF = 3*nv;
	this->m_numDim = 3;

	this->m_vpEles.reserve(this->m_numEle);

	// Initialize area preserving elements

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		NodalSolidElement* pEle = new DSAreaElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for(int v = 0; v < 3; ++v)
		{
			vinds.push_back((*fv_it).idx());
			++fv_it;
		}
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}
}

void SurfaceModel::initializeElements_MS_QuadArea()
{
	int nf = (int) this->m_pSimMesh->n_faces();
	int ne = (int) this->m_pSimMesh->n_edges();
	int nv = (int) this->m_pSimMesh->n_vertices();
	this->m_numEle = nf; // Area elements
	this->m_numEle += ne; // MS elements
	this->m_numNodeSim = nv;
	this->m_nrawDOF = 3*nv;
	this->m_numDim = 3;

	this->m_vpEles.reserve(this->m_numEle);

	// Initialize stretch mass-spring elements

	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());
	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		NodalSolidElement* pEle = new DSEdgeElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(2);
		TriMesh::EdgeHandle eh = *e_it;
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		
		vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx());
		vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx());
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}

	// Initialize area preserving elements

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		NodalSolidElement* pEle = new DSAreaElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for(int v = 0; v < 3; ++v)
		{
			vinds.push_back((*fv_it).idx());
			++fv_it;
		}
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}
}


void SurfaceModel::initializeElements_DS_MS_QuadArea()
{
	int nf = (int) this->m_pSimMesh->n_faces();
	int ne = (int) this->m_pSimMesh->n_edges();
	int nv = (int) this->m_pSimMesh->n_vertices();
	this->m_numEle = nf; // Area elements
	this->m_numEle += ne; // MS elements
	this->m_numNodeSim = nv;
	this->m_nrawDOF = 3*nv;
	this->m_numDim = 3;

	this->m_vpEles.reserve(this->m_numEle);

	// Initialize stretch mass-spring elements

	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());
	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		NodalSolidElement* pEle = new DSEdgeElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(2);
		TriMesh::EdgeHandle eh = *e_it;
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		
		vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx());
		vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx());
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}

	// Initialize area preserving elements

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		NodalSolidElement* pEle = new DSAreaElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for(int v = 0; v < 3; ++v)
		{
			vinds.push_back((*fv_it).idx());
			++fv_it;
		}
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}

	// Initialize discrete-shells bending elements

	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		NodalSolidElement* pEle = new DSHingeElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(4);
		TriMesh::EdgeHandle eh = *e_it;
		if (m_pSimMesh->is_boundary(eh))
			continue;

		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);
		
		for(int i = 0; i < 4; i++)	
			vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx()); 
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);

		this->m_numEle++;
	}
}


void SurfaceModel::initializeElements_DS_FEM()
{
	int nf = (int) this->m_pSimMesh->n_faces();
	int nv = (int) this->m_pSimMesh->n_vertices();
	this->m_numEle = nf; // Area elements
	this->m_numEle += nf; // DS elements
	this->m_numNodeSim = nv;
	this->m_nrawDOF = 3*nv;
	this->m_numDim = 3;

	this->m_vpEles.reserve(this->m_numEle);

	// Initialize area elements

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		NodalSolidElement* pEle = NULL; 

		// Create element
		switch (this->m_DM)
		{
			case DS_FEMLinearCorot: pEle = new FEMElementLinearCorot(4, 3, FEMElement::Tet1); break;
			//case DS_FEMMooneyRivlin: pEle = new FEMElementMooneyRivlin(4, 3, FEMElement::Tet1); break;
			//case DS_FEMNeoHookean: pEle = new FEMElementNeoHookean(4, 3, FEMElement::Tet1); break;
			//case DS_FEMOgden: pEle = new FEMElementOgden(4, 3, FEMElement::Tet1); break;
			case DS_FEMStVK: pEle = new FEMElementStVK(4, 3, FEMElement::Tet1); break;
			default: assert(false); // Should not happen D:
		}

		// Set indices
		vector<int> vinds;
		vinds.reserve(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for(int v = 0; v < 3; ++v)
		{
			vinds.push_back((*fv_it).idx());
			++fv_it;
		}
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}

	// Initialize bending elements

	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());
	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		NodalSolidElement* pEle = new DSHingeElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(4);
		TriMesh::EdgeHandle eh = *e_it;
		if (m_pSimMesh->is_boundary(eh))
			continue;

		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);
		
		for(int i = 0; i < 4; i++)	
			vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx()); 
		pEle->setNodeIndices(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}
}

void SurfaceModel::updateStatex(const VectorXd& vx, const VectorXd& vv)
{
	SolidModel::updateStatex(vx, vv);
	this->m_isMeshReady = false;
}

void SurfaceModel::updateState0(const VectorXd& vX, const VectorXd& vV)
{
	SolidModel::updateState0(vX, vV);
	this->m_isMeshReady = false;
}

void SurfaceModel::updateSimMesh()
{
	if (this->m_isMeshReady)
		return; // Updated

	dVector xEVec(this->m_nrawDOF);
	for (int i = 0; i < m_nrawDOF; ++i)
		xEVec[i] = this->m_vx(i);

	dVector vEVec(this->m_nrawDOF);
	for (int i = 0; i < m_nrawDOF; ++i)
		vEVec[i] = this->m_vv(i);

	dVector x0Vec(this->m_nrawDOF);
	for (int i = 0; i < m_nrawDOF; ++i)
		x0Vec[i] = this->m_vX(i);

	this->m_pSimMesh->setPoints(xEVec, VPropSim::POSE);
	this->m_pSimMesh->setPoints(vEVec, VPropSim::VELE);
	this->m_pSimMesh->setPoints(x0Vec, VPropSim::POS0);

	this->m_isMeshReady = true;
}