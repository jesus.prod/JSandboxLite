/*=====================================================================================*/
/*! 
\file		FEMElementStVK.h
\author		jesusprod
\brief		Non-linear St. Venant-Kirchhoff model
				- Deformation metric: Green straint E
				- Parameters: Lam� coefficients (m,l)
				- Energy density: Psi(F) = mu*E*E + 0.5*l + tr^2(E)
				- First Piola-Kirchhoff: P(F) = F*[2*mu*E + l*tr(E)*I]
 */
/*=====================================================================================*/

#ifndef FEM_MODEL_STVK_H
#define FEM_MODEL_STVK_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/FEMElement.h>
#include <JSandbox/SolidMaterial.h>

class FEMElementStVK : public FEMElement
{

public:
	FEMElementStVK(int nv, int nd, FEMType D) : FEMElement(nv, nd, D) { }	

	virtual ~FEMElementStVK() { }

	virtual Real computeEnergyDensity(const MatrixXd& mF) const;
	virtual void computePiolaKirchhoffFirst(const MatrixXd& mF, MatrixXd& mP) const;
	virtual void computePiolaKirchhoffFirstDerivative(const MatrixXd& mF, const vector<MatrixXd>& vDFDx, vector<MatrixXd>& vDPDx) const;

};

#endif