/*=====================================================================================*/
/*! 
\file		PhysSolver.cpp
\author		jesusprod
\brief		Implementation of PhysSolver.h
 */
/*=====================================================================================*/

#include <JSandbox/PhysSolver.h>

#include <omp.h> // MP support

 

PhysSolver::PhysSolver(SolidModel *pSM) :
	m_solverTimer(10, "TOTAL_SOLVE"), 
	m_linearTimer(10, "LINEAR_SOLVE"),
	m_updateMatTimer(10, "UPDATE_SOLVER_MAT"),
	m_updateResTimer(10, "UPDATE_SOLVER_RES")
{ 
	// Might be NULL
	this->m_pSM = NULL;
	this->m_pSM = pSM;
			
	// Set default parameters
	this->m_solverMaxError = 1e-6;
	this->m_linearMaxError = 1e-6;
	this->m_useRegularize = true;
	this->m_useLineSearch = true;
	this->m_lsMaxIters = 10;
	this->m_regMaxIters = 10;
	this->m_solverMaxIters = 10;
	this->m_linearMaxIters = 1000;
	this->m_lsBeta = 0.5;
	this->m_dt = 0.005;

	this->m_hasMaxStep = false;
	this->m_maxStep = HUGE_VAL;

	this->m_isSetupReady = false;

	m_solverTimer.initialize(); 
	m_linearTimer.initialize();
	m_updateMatTimer.initialize();
	m_updateResTimer.initialize();
}

PhysSolver::~PhysSolver()
{
	this->clearBConditions();
}

void PhysSolver::setup()
{
	// Model should be initialized
	assert(this->m_pSM != NULL);

	// DOF
	this->m_N = this->m_pSM->getRawDOFNumber();
	this->m_vxPre = this->m_pSM->getPositions();
	this->m_vvPre = this->m_pSM->getVelocities();
	this->m_vx = this->m_vxPre;
	this->m_vv = this->m_vvPre;

	// Mass vector and sparse matrix (TODO: matrix)
	this->m_vm = this->m_pSM->getLumpedMassVector();
	
	// Hessian (TO DEPRECATE)
	this->preallocateHessian();

	this->m_isSetupReady = true;
}

void PhysSolver::advanceBoundary()
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->advanceBoundary();
}

bool PhysSolver::isFullyLoaded() const
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		if (!this->m_vpBC[i]->isFullyLoaded())
			return false;
	return true;
}

void PhysSolver::clearBConditions()
{
	for (int i = 0; i < (int) this->m_vpBC.size(); ++i)
		delete this->m_vpBC[i]; // Free allocated memory
	this->m_vpBC.clear();

	this->m_isSetupReady = false;	
}

int PhysSolver::getBConditionNumber() const { return (int) this->m_vpBC.size(); }

void PhysSolver::addBCondition(BCondition *pBC) { assert(pBC != NULL); this->m_vpBC.push_back(pBC); this->m_isSetupReady = false; }

BCondition *PhysSolver::getBCondition(int i) { assert(i >= 0 && i < this->m_vpBC.size()); return this->m_vpBC[i]; this->m_isSetupReady = false; }

void PhysSolver::preallocateHessian()
{
	// Precompute the minimum number of required 
	// entries to allocate the whole Hessian matrix.
	// These only include those concerning the model
	// and boundary conditions. New entries might be
	// added when handling collisions or constraints

	//int nJ = this->m_pSM->getNumNonZeros_Jacobian();
	//nJ += this->m_N; // For lumped mass matrix addition
	//for (int i = 0; i < (int) this->m_vpBC.size(); ++i)
	//	nJ += this->m_vpBC[i]->getNumNonZeros_Jacobian();
	//this->m_vA.reserve(nJ);
}

bool PhysSolver::solve()
{
	this->advanceBoundary();

	bool result = false;

	if (!this->m_isStatic)
	{
		Real dtStart = this->m_dt;

		if (this->m_fr == 0)
		{
			Real dt0 = this->m_dt;
			this->m_dt = 2*dt0;
			int bn = 0;
			do
			{
				bn++; // Bisection
				this->m_dt *= 0.5;
				
				this->m_solverTimer.restart();

				if (this->m_isNonLin)
					result = this->solve_NonLinear();
				else result = this->solve_Quadratic();

				this->m_solverTimer.stopStoreLog();
			}
			while (!result && bn <= this->m_dtBisects);
		}
		else
		{
			Real T = 1.0/this->m_fr; 
			Real t = 0;
			while (abs(t - T) > 1e-6f)
			{
				Real dt0 = this->m_dt;
				if (t + dt0 > T) 
					dt0 = T - t;

				this->m_dt = 2*dt0;
				int bn = 0;
				do
				{
					bn++; // Bisection
					this->m_dt *= 0.5; 

					this->m_solverTimer.restart();

					if (this->m_isNonLin)
						result = this->solve_NonLinear();
					else result = this->solve_Quadratic();

					this->m_solverTimer.stopStoreLog();
				}
				while (!result && bn <= this->m_dtBisects);
				
				t += this->m_dt;
			}
		}

		this->m_dt = dtStart;
	}
	else 
	{
		this->m_solverTimer.restart();

		if (this->m_isNonLin)
			result = this->solve_NonLinear();
		else result = this->solve_Quadratic();

		this->m_solverTimer.stopStoreLog();
	}

	return result;
}

bool PhysSolver::solve_Quadratic()
{
	if (this->m_isStatic)
		logSimu("\n[INFO] In %s: Non-linear static solve\n", __FUNCTION__);
	else logSimu("\n[INFO] In %s: Non-linear dynamic solve with time-step %.9f\n", __FUNCTION__, this->m_dt);

	VectorXd vRi(this->m_N);
	VectorXd vxi(this->m_N);
	VectorXd vvi(this->m_N);
	VectorXd vdx(this->m_N);
	VectorXd vxl(this->m_N);
	VectorXd vvl(this->m_N);
	Real Rnormi = 0.0;
	Real Rnorml = 0.0;

	bool solved = false;

	vvi = this->m_vv;
	vxi = this->m_vx;
	this->constrainBoundary(this->m_pSM);
	this->m_pSM->updateForceJacobian();
	this->getResidual(vxi, vvi, vRi);
	Rnormi = vRi.norm();

	logSimu("[INFO] In %s: Initial residual: %.9f\n", __FUNCTION__, Rnormi);

	// Solve linear system

	SparseMatrixXd mA(m_N, m_N);
	this->getMatrix(vxi, vvi, mA);
	vRi = -vRi; // System A*dx = -R

	if (!this->solveLinearSystem(mA, vRi, vdx))
	{
		if (this->m_useLineSearch)
		{
			// Line-search in dx direction

			int l;
			vxl = vxi;
			vvl = vvi;
			Rnorml = Rnormi;
			// potenl = poteni;
			for (l = 0; l < this->m_lsMaxIters; ++l)
			{
				// Position/velocity updates

				vxi += vdx;
				if (!this->m_isStatic) // Update dynamics
					vvi = (vxi - this->m_vx)/this->m_dt;

				this->m_pSM->updateStatex(vxi, vvi);
				this->getResidual(vxi, vvi, vRi);
				Rnormi = vRi.norm();
				if (Rnormi < Rnorml)
				{
					solved = true;
					break;
				}
				
				// Bisection / restore
				vdx *= this->m_lsBeta;;
				vxi = vxl;
				vvi = vvl;
			}
			
			if (l == this->m_lsMaxIters)
				logSimu("[ERROR] In %s: NOT improved after %d bisections, residual: %.9f\n", __FUNCTION__, l, Rnormi);
			else logSimu("[GOOD] In %s: improved after %d bisections, residual: %.9f\n", __FUNCTION__, l, Rnormi);
		}
		else
		{
			// Do not perform line search, this way the solution
			// is not guaranteed to reduce the overall error so
			// should be used carefully

			vxi += vdx; // Pos/vel updates
			vvi = (vxi-this->m_vx)/this->m_dt;
			this->m_pSM->updateStatex(vxi, vvi);
			this->constrainBoundary(this->m_pSM);
			this->getResidual(vxi, vvi, vRi);
			Rnormi = vRi.norm();
			solved = true;
		}
	}
	else solved = false;

	if (solved)
	{
		logSimu("[GOOD] In %s: solution improved, residual: %.9f\n", __FUNCTION__, Rnormi);

		this->m_vxPre = this->m_vx;
		this->m_vvPre = this->m_vv;
		this->m_vx = vxi;
		this->m_vv = vvi;
	}
	else
	{
		logSimu("[GOOD] In %s: solution not improved, residual: %.9f\n", __FUNCTION__, Rnormi);

		this->m_pSM->updateStatex(this->m_vx, this->m_vv);
	}

	return solved;
}

int totalIt = 0;

bool PhysSolver::solve_NonLinear()
{

	if (this->m_isStatic)
		logSimu("\n[INFO] In %s: Non-linear static solve\n", __FUNCTION__);
	else logSimu("\n[INFO] In %s: Non-linear dynamic solve with time-step %.9f\n", __FUNCTION__, this->m_dt);

	logSimu("\n[INFO] Iteration %d\n", totalIt++);

	Real Rnormi = 0.0;

	VectorXd vRi;

	VectorXd vxi = this->m_pSM->getPositions();
	VectorXd vvi = this->m_pSM->getVelocities();

	this->m_vx = vxi;
	this->m_vv = vvi;

	vxi = vxi + this->m_dt*vvi; // Initial guess

	this->m_pSM->pushState();
	this->m_pSM->updateStatex(vxi, vvi);
	vxi = this->m_pSM->getPositions();
	vvi = this->m_pSM->getVelocities();

	int i;
	bool solved = true;
	for (i = 0; i < this->m_solverMaxIters; ++i)
	{
		if (i == 0)
		{
			this->constrainBoundary(this->m_pSM); 
			this->m_pSM->updateForceJacobian();	
			this->getResidual(vxi, vvi, vRi);
			Rnormi = vRi.norm();

			logSimu("[INFO] In %s: Starting solve with initial residual: %.9f\n", __FUNCTION__, Rnormi);
		} 
		
		if (Rnormi <= this->m_solverMaxError)
		{
			break; // Out
		}

		// Solve linear system

		int Ni = this->m_pSM->getRawDOFNumber();

		SparseMatrixXd mA(Ni, Ni);
		this->getMatrix(vxi, vvi, mA);
		mA = -mA; // Step dx = -A^-1*R

		//writeToFile(mA, "jacobian.csv", true);

		VectorXd vdx(Ni);
		vdx.setZero();

		if (!this->solveLinearSystem(mA, vRi, vdx))
		{
			solved = false;
			break; // Out
		} 

		if (this->m_hasMaxStep) 
			vdx *= this->m_maxStep;

		VectorXd vxl = vxi; 
		VectorXd vvl = vvi;
		Real Rnorml = Rnormi;

		if (this->m_useLineSearch)
		{
			// Line-search in dx direction

			int l;
			for (l = 0; l < this->m_lsMaxIters; ++l)
			{
				// Position/velocity updates

				vxi += vdx;
				if (!this->m_isStatic) // Update dynamics
					vvi = (vxi - this->m_vx)/this->m_dt;

				this->m_pSM->peekState(0);
				this->m_pSM->updateStatex(vxi, vvi);
				vxi = this->m_pSM->getPositions();
				vvi = this->m_pSM->getVelocities();

				this->constrainBoundary(this->m_pSM);
				this->getResidual(vxi, vvi, vRi);
				Rnormi = vRi.norm();
				if (Rnormi < Rnorml)
				{
					break;
				}

				// Bisection / restore

				vdx *= this->m_lsBeta;
				vxi = vxl;
				vvi = vvl;
			}


			//if (l == 1)
			//{
			//	ostringstream debugMatrixOut;
			//	debugMatrixOut << "jacobianConvergedBad.csv";
			//	writeToFile(-mA, debugMatrixOut.str().c_str(), true);
			//}
			//else if (l == 0)
			//{
			//	ostringstream debugMatrixOut;
			//	debugMatrixOut << "jacobianConvergedGood.csv";
			//	writeToFile(-mA, debugMatrixOut.str().c_str(), true);
			//}
			
			if (l == this->m_lsMaxIters)
			{
				//writeToFile(mA, "jacobianNotConverged.csv", true);

				logSimu("[ERROR] In %s: NOT improved after %d bisections, residual: %.9f\n", __FUNCTION__, l, Rnormi);
				solved = false;
				break; // Error
			}
			else
			{
				logSimu("[GOOD] In %s: improved after %d bisections, residual: %.9f\n", __FUNCTION__, l, Rnormi);
			}
		}
		else
		{
			// Do not perform line search, this way the solution
			// is not guaranteed to reduce the overall error so
			// should be used carefully

			vxi += vdx; // Pos/vel updates
			vvi = (vxi - this->m_vx)/this->m_dt;
			this->m_pSM->updateStatex(vxi, vvi);
			this->constrainBoundary(this->m_pSM);
			this->getResidual(vxi, vvi, vRi);
			Rnormi = vRi.norm();

			if (Rnormi > Rnorml)
			{
				solved = false;
				break; // Error
			}
		}
	}

	if (solved)
	{
		logSimu("[GOOD] In %s: converged after %d iterations, residual: %.9f\n", __FUNCTION__, i, Rnormi);
		
		this->m_vxPre = this->m_vx;
		this->m_vvPre = this->m_vv;
		this->m_vx = vxi;
		this->m_vv = vvi;
	}
	else
	{
		logSimu("[ERROR] In %s: NOT converged after %d iterations, residual: %.9f\n", __FUNCTION__, i, Rnormi);
	
		this->m_pSM->popState();
		this->m_vx = vxi;
		this->m_vv = vvi;
	}

	return solved;
}

bool PhysSolver::solveLinearSystem(SparseMatrixXd& mA, VectorXd& vb, VectorXd& vx)
{
	m_linearTimer.restart();

	// Solve the linear system s.t. regularization
	for (int i = 0; i < this->m_regMaxIters; ++i)
	{
		if (i != 0)
		{
			if (this->m_useRegularize)
			{
				logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);

				// Regularize diagonal
				for (int ii = 0; ii < this->m_N; ++ii)
					mA.coeffRef(ii, ii) += this->m_rFactor*pow(10.0,i);
			}
			else break; // Only one chance, out and return error
		}

		//writeToFile(mA, "LinearSystemMatrix.csv", true);

		if (this->m_lsSolver == PhysSolver::Cholesky)
		{
			SimplicialLDLT<SparseMatrixXd> solver;

			solver.compute(mA);

			if (solver.info() != Success)
				continue; // Iterate again

			vx = solver.solve(vb);

			if (solver.info() != Success) 
				continue; // Iterate again
			 
			VectorXd vbTest = mA.selfadjointView<Lower>()*vx;
			double error = (vb - vbTest).squaredNorm();
			if (error > this->m_linearMaxError)
				continue; // Iterate again

			logSimu("[GOOD] In %s: Linear system solved using Cholesky\n", __FUNCTION__);
		}
		else if (this->m_lsSolver == PhysSolver::Conjugate)
		{
			ConjugateGradient<SparseMatrixXd> solver;

			solver.setTolerance(this->m_linearMaxError);
			solver.setMaxIterations(this->m_linearMaxIters);

			solver.compute(mA);
			
			if (solver.info() != Success)
				continue; // Iterate again

			vx = solver.solve(vb);
			
			if (solver.info() != Success) 
				continue; // Iterate again

			logSimu("[GOOD] In %s: Linear system solved using Conjugate Gradient\n", __FUNCTION__);
		}
		else assert(false); // Should not happen

		m_linearTimer.stopStoreLog();

		return true;
	}

	return false;
}

void PhysSolver::constrainBoundary(SolidModel* pModel) const
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->constrain(pModel); // Constrain
}

void PhysSolver::constrainBoundary(VectorXd& vf) const
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->constrain(vf); // Constrain vector
}

void PhysSolver::constrainBoundary(tVector& vJ) const
{
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->constrain(vJ); // Constrain matrix
}

Real PhysSolver::getBoundaryEnergy(const VectorXd& vx, const VectorXd& vv) const
{
	int N = this->m_pSM->getRawDOFNumber();

	assert(vx.size() == N);
	assert(vv.size() == N);

	double energy = 0;
	
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		energy += this->m_vpBC[i]->getEnergy(vx, vv);

	return energy;
}

void PhysSolver::addBoundaryForce(const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const
{
	int N = this->m_pSM->getRawDOFNumber();

	assert(vx.size() == N);
	assert(vv.size() == N);
	assert(vf.size() == N);
	
	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->addForce(vx, vv, vf);
}

void PhysSolver::addBoundaryJacobian(const VectorXd& vx, const VectorXd& vv, tVector& vJ) const
{
	int N = this->m_pSM->getRawDOFNumber();

	assert(vx.size() == N);
	assert(vv.size() == N);

	for (int i = 0; i < (int)this->m_vpBC.size(); ++i)
		this->m_vpBC[i]->addJacobian(vx, vv, vJ);
}