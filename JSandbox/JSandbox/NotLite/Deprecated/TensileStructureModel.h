/*=====================================================================================*/
/*!
\file		TensileStructureModel.h
\author		jesusprod
\brief		Custom made model for tensile structures.
*/
/*=====================================================================================*/

#ifndef TENSILE_STRUCTURE_MODEL_H
#define TENSILE_STRUCTURE_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/Geometry/TriMesh.h>

#include <JSandbox/Geometry/Curve.h>
#include <JSandbox/CSTElement.h>
#include <JSandbox/DSEdgeElement.h>
#include <JSandbox/DSAreaElement.h>
#include <JSandbox/DSHingeElement.h>
#include <JSandbox/RodEdgeElement3D.h>
#include <JSandbox/RodAngleElement3D.h>
#include <JSandbox/EmbeddedDERRodBendElement.h>
#include <JSandbox/EmbeddedDERRodTwistElement.h>
#include <JSandbox/EmbeddedRodStretchElement3D.h>
#include <JSandbox/EmbeddedPartialRodStretchElement3D.h>
#include <JSandbox/EmbeddedRodBendElement3D.h>
#include <JSandbox/NodalSolidModel.h>
#include <JSandbox/Geometry/SurfaceRemesher.h>
#include <JSandbox/DERSlideModel.h>

//typedef struct EmbeddedPoint
//{
//	iVector m_vidx;	// Indices
//	dVector m_vbar; // Coords.
//
//	EmbeddedPoint()
//	{
//		m_vidx.clear();
//		m_vbar.clear();
//	}
//} 
//EmbeddedPoint;

typedef struct EmbeddedRod
{
	vector<EmbeddedPoint> m_vnodes;

	EmbeddedRod()
	{
		this->m_vnodes.clear();
	}
} EmbeddedRod;

typedef struct Rope
{
	iVector m_vidx; // Indices
	vector<Vector3d> m_vnodes;
	
	bool m_embeddedIni;
	bool m_embeddedEnd;

	EmbeddedPoint m_ini;
	EmbeddedPoint m_end;

	iVector m_vhookEles;
	iVector m_vdhookEles;

	Rope()
	{
		this->m_vnodes.clear();
		m_embeddedIni = false;
		m_embeddedEnd = false;
	}
} Rope;

typedef struct CoupledRod
{
	Curve m_rawCurve;
	dVector m_vfixAL;

	RodNodeList m_nodeList;
} CoupledRod;

typedef struct DOFCoupling
{
	int m_model0;
	int m_model1;
	int m_idx0;
	int m_idx1;
} DOFCoupling;

class JSANDBOX_EXPORT TensileStructureModel : public NodalSolidModel
{
public:

public:
	TensileStructureModel();
	virtual ~TensileStructureModel();

	virtual void configureSurface(const dVector& vp, const iVector& vi);
	virtual void configureSeamsCurves(const vector<vector<Vector3d>>& vcurs);
	virtual void configureStructCurves(const vector<vector<Vector3d>>& vcurs);
	virtual void configureInternalRopes(const vector<pair<Vector3d, Vector3d>>& vropes);
	virtual void configureExternalRopes(const vector<pair<Vector3d, Vector3d>>& vropes);
	virtual void configureRopesCurves(const vector<pair<Vector3d, Vector3d>>& vropes);

	virtual void configureStructCurves(const vector<Curve>& vcurve, vector<dVector> vfix);

	virtual TriMesh* getRawMesh() { return this->m_pRawMesh; }
	virtual TriMesh* getSimMesh() { return this->m_pSimMesh; }

	virtual void configureMaterial(SolidMaterial* pMatSurf, SolidMaterial* pMatStruct, SolidMaterial* pMatRopes);

	virtual void updateSimMesh();

	virtual void peekState(int i);
	virtual void pushState();
	virtual void popState();

	virtual void updateStatex(const VectorXd& vx, const VectorXd& vv);
	virtual void updateState0(const VectorXd& vX, const VectorXd& vV);

	virtual void testForceLocal();
	virtual void testJacobianLocal();

	virtual void updateMass();
	virtual void updateRest();
	virtual void updateForce();
	virtual void updateEnergy();
	virtual void updateJacobian();
	virtual void updateEnergyForce();
	virtual void updateForceJacobian();
	virtual void updateEnergyForceJacobian();

	virtual void setup();

	virtual Real getEnergy();
	virtual void addForce(VectorXd& vf, const vector<bool>* pvFixed = NULL);
	virtual void addJacobian(tVector& vJ, const vector<bool>* pvFixed = NULL);

	virtual const dVector& getStructWeights() const { return this->m_vstructPreStrain; }
	virtual void setStructWeights(const dVector& vw) { this->m_vstructPreStrain = vw; }

	virtual const dVector& getRopesWeights() const { return this->m_vropesPreStrain; }
	virtual void setRopesWeights(const dVector& vw) { this->m_vropesPreStrain = vw; }

	virtual const vector<vector<MeshTraits::ID>>& getStructVerts() const { return this->m_vstructVerts; }
	virtual const vector<vector<MeshTraits::ID>>& getStructEdges() const { return this->m_vstructEdges; }

	virtual const vector<vector<MeshTraits::ID>>& getSeamsVerts() const { return this->m_vseamsVerts; }
	virtual const vector<vector<MeshTraits::ID>>& getSeamsEdges() const { return this->m_vseamsEdges; }

	virtual int getRodModelNumber() const { return (int) this->m_vpRodModels.size(); }
	virtual const DERSlideModel* getRodModel(int i) const { return this->m_vpRodModels[i]; }

	virtual int getNumSurfaceBendingEles() const { return (int) this->m_vpDSBendingEles.size(); }
	virtual int getNumSurfaceStretchEles() const { return (int) this->m_vpDSStretchEles.size(); }
	virtual int getNumRodStretchEles() const { return (int) this->m_vpRodStretchEles.size(); }
	virtual int getNumRodBendingEles() const { return (int) this->m_vpRodBendingEles.size(); }

	virtual EmbeddedRodBendElement3D* getRodBendingEle(int i) { return this->m_vpRodBendingEles[i]; }
	virtual EmbeddedRodStretchElement3D* getRodStretchEle(int i) { return this->m_vpRodStretchEles[i]; }

	virtual int getStructRodNum() { return this->m_nr; }

	//virtual EmbeddedRodStretchElement3D* getRodStretchEle(int i) { return this->m_vpRodStretchEles[i]; }
	//virtual vector<EmbeddedDERRodBendElement*>& getRodBendingEles(int i) { return this->m_vpRodBendingEles[i]; }
	//virtual vector<EmbeddedDERRodTwistElement*>& getRodTwistEles(int i) { return this->m_vpRodTwistEles[i]; }
	
	const vector<vector<EmbeddedPoint>>& getStructPoints() { return this->m_vstructNodes; }
	
	const vector<Rope>& getRopes() { return this->m_vRopes; }

	virtual const vector<iVector>& getMapSingle2Patches() const { return this->m_vmapSingle2Patches; }
	virtual const vector<iVector>& getMapPatches2Single() const { return this->m_vmapPatches2Single; }
	virtual const map<int, int>& getMapPatchEdges() const { return this->m_mapPatchEdges; }

protected:

	virtual void getRopesPositions(dVector& vx) const;
	virtual void setRopesPositions(const dVector& vx);

	virtual void preSetupActions();
	virtual void posSetupActions();
	virtual void initializeElements();
	virtual void initializeMaterials();

	virtual void updateSimMeshSingle3D();
	virtual void initializeMeshSingle3D();
	virtual void initializeElementsSingle3D();

	virtual void updateSimMeshPatchesEmbedded3D();
	virtual void initializeMeshPatchesEmbedded3D();
	virtual void initializeElementsPatchesEmbedded3D();

	virtual void updateSimMeshPatchesEmbedded2D();
	virtual void initializeMeshPatchesEmbedded2D();
	virtual void initializeElementsPatchesEmbedded2D();

	virtual void updateSimMeshPatchesEmbedded3D_10();
	virtual void initializeMeshPatchesEmbedded3D_10();
	virtual void initializeElementsPatchesEmbedded3D_10();

	virtual void updateMeshState();

	virtual void initializeMeshStructure();
	virtual void initializeMeshElements();
	virtual void initializeMeshMaterials();
	virtual void initializeMeshDOF();

	// Temporal

	virtual void updateSimulationDOF();
	virtual vector<DOFCoupling> getModelCouplings(int i);
	virtual vector<DOFCoupling> getModelModelCouplings(int i, int j);

	// Temporal

	virtual void updateRodsCenter();
	virtual void updateRodsAngles();
	virtual void updateRodsReferenceFrame();
	virtual void updateRodsReferenceTwist();
	virtual void updateRodsMaterialFrame();

protected:

	vector<VectorXd> m_vxMeshStack;
	vector<VectorXd> m_vvMeshStack;

	dVector m_vstructPreStrain;
	dVector m_vropesPreStrain;

	// Indices

	VectorXd m_vxMesh;
	VectorXd m_vXMesh;
	VectorXd m_vvMesh;
	int m_numDOFMesh;

	vector<iVector> m_vlocToGlo;
	vector<DOFCoupling> m_vcouplings;

	vector<DERSlideModel*> m_vpRodModels;
	vector<CoupledRod> m_vrawCoupledRods;

	// Raw data

	int m_numMeshNodes_x;
	int m_numMeshNodes_X;

	dVector m_vrawPos;
	iVector m_vrawIdx;
	TriMesh* m_pRawMesh;
	vector<dVector> m_vrawSursPos;
	vector<iVector> m_vrawSursIdx;
	vector<vector<Vector3d>> m_vrawSeamsCurs;
	vector<vector<Vector3d>> m_vrawStructCurs;
	vector<pair<Vector3d, Vector3d>> m_vrawInternalRopes;
	vector<pair<Vector3d, Vector3d>> m_vrawExternalRopes;
	vector<pair<Vector3d, Vector3d>> m_vrawRopes;

	dVector m_vrodsDesignPos;

	// For remeshing issues

	bVector m_vseamsLoops;
	bVector m_vstructLoops;
	vector<vector<MeshTraits::ID>> m_vstructEdges;
	vector<vector<MeshTraits::ID>> m_vstructVerts;
	vector<vector<MeshTraits::ID>> m_vseamsEdges;
	vector<vector<MeshTraits::ID>> m_vseamsVerts;

	// For embedded rods

	vector<vector<EmbeddedPoint>> m_vstructNodes;
	vector<Rope> m_vRopes;

	int m_nr;
	int m_nvDOF;
	int m_naDOF;
	vector<Curve> m_vrodCenter;
	vector<dVector> m_vrodAngle;
	vector<dVector> m_vrodTwist;
	vector<vector<Frame>> m_vrefFrames;
	vector<vector<Frame>> m_vmatFrames;

	// Patches maps

	vector<iVector> m_vmapSingle2Patches;
	vector<iVector> m_vmapPatches2Single;
	map<int, int> m_mapPatchEdges;

	// Simulation mesh

	bool m_isMeshReady;
	TriMesh* m_pSimMesh;

	int m_boundaryStretchEleStartIdx;
	int m_boundaryBendingEleStartIdx;

	SolidMaterial* m_pStructMaterial;
	SolidMaterial* m_pRopesMaterial;
	SolidMaterial* m_pSurfMaterial;

	vector<CSTElement*> m_vpDSStretchEles;
	vector<DSAreaElement*> m_vpDSAreaEles;
	vector<DSEdgeElement*> m_vpDSEdgeEles;
	vector<DSHingeElement*> m_vpDSBendingEles;

	//vector<RodAngleElement3D*> m_vpRodBendingEles;
	//vector<RodEdgeElement3D*> m_vpRodStretchEles;
	
	vector<EmbeddedRodBendElement3D*> m_vpRodBendingEles;
	vector<EmbeddedRodStretchElement3D*> m_vpRodStretchEles;
	
	vector<RodAngleElement3D*> m_vpRopeBendEles;
	vector<RodEdgeElement3D*> m_vpRopeStretchEles;
	
	vector<EmbeddedRodStretchElement3D*> m_vpRopeDoubleHooks;
	vector<EmbeddedPartialRodStretchElement3D*> m_vpRopeHooks;

	//vector<vector<EmbeddedDERRodBendElement*>> m_vpRodBendingEles;
	//vector<vector<EmbeddedDERRodTwistElement*>> m_vpRodTwistEles;
	//vector<EmbeddedRodStretchElement3D*> m_vpRodStretchEles;

};

#endif