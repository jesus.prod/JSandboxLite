/*=====================================================================================*/
/*!
\file		RodEdgeElement2D.cpp
\author		jesusprod
\brief		Implementation of RodEdgeElement2D.h
*/
/*=====================================================================================*/

#include <JSandbox/RodEdgeElement2D.h>

RodEdgeElement2D::RodEdgeElement2D() : NodalSolidElement(2, 2, 2)
{
	// Nothign to do here
}

RodEdgeElement2D::~RodEdgeElement2D()
{
	// Nothing to do here
}

void RodEdgeElement2D::updateRest(const VectorXd& vX)
{
	Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
	Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
	this->m_L0 = (x1 - x0).norm();

	Real nodeMass = this->m_L0*this->m_vpmat[0]->getDensity()/2.0;
	for (int i = 0; i < this->m_nrawDOF; ++i)
		this->m_vmass[i] = nodeMass;
}

void RodEdgeElement2D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[2 + i]);
	}

	double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double kS = m_vpmat[0]->getStretchK();

//#include "../../Maple/Code/RodEdge2DEnergy.mcg"

	//this->m_energy = t17;
}

void RodEdgeElement2D::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[2 + i]);
	}

	double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double kS = m_vpmat[0]->getStretchK();

	double vf4[4];

//#include "../../Maple/Code/RodEdge2DForce.mcg"

	for (int i = 0; i < 4; ++i)
		if (!isfinite(vf4[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 4; ++i)
		this->m_vfVal(i) = vf4[i];
}

void RodEdgeElement2D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{ 
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[2 + i]);
	}

	double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double kS = m_vpmat[0]->getStretchK();

	double mJ[4][4];

//#include "../../Maple/Code/RodEdge2DJacobian.mcg"

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ[i][j];
}