/*=====================================================================================*/
/*! 
\file		FEMElement.h
\author		jesusprod
\brief		Basic abstract implementation of SolidElement for continuous elements.
			All implemented continuous elements should inherit from this abstract
			class; at least, those where discretization fits with an 
			interpolation from n-dimensional node positions.
 */
/*=====================================================================================*/

#ifndef FEM_ELEMENT_H
#define FEM_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>

/*
Current limitations:

- Mass lumping is done by considering the mass of the element
is distributed uniformly among element nodes. This should
depend on the rest configuration of the element.

- Hexahedral elements are assumed to be regular hexahedrons.
*/

class FEMElement : public NodalSolidElement
{

public:
	enum FEMType
	{
		Tet1 = 0,	// Basic linear tetrahedron, 1 quadrature point
		Hex3 = 1,	// Basic cubic hexahedron, 8 quadrature points
	};

public:
	FEMElement(int nv, int nd, FEMType D); 
	virtual ~FEMElement();

	virtual FEMType getType() const { return this->m_type; } // Discretization type
	virtual int getNumDims() const { return this->m_numDim; } // Number of dimensions
	virtual int getNumNodes() const { return this->m_numNodeSim; }	// Number of element nodes
	virtual int getNumQuads() const { return this->m_numQuad; }	// Number of quadrature points
	virtual Real getVolume0() const { return this->m_volume0; }	

	virtual Real getIntegrationVolume() const { return this->m_volume0; }

	// Initialize rest volumen and precomputed parts of F
	virtual void updateRest(const VectorXd& vX);

	// Build [nd x nv] matrix of node positions for the given DOF vector
	virtual void buildNodeMatrix(const VectorXd& vx, MatrixXd& mD) const;	

	// Transform between iso-parametric and material coordinates
	virtual VectorXd toNaturalCoordinates(const VectorXd& vx) const;
	virtual VectorXd toMaterialCoordinates(const VectorXd& vs) const;

	// Compute element volume for the given DOF vector
	virtual Real computeVolume(const MatrixXd& mDs) const;

	// Compute [nv x 1] vector of shape function values
	virtual void computeShapeFunction(int q, VectorXd& vN) const;
	virtual void computeShapeFunction(const VectorXd& s, VectorXd& vN) const;

	// Compute [nv x nd] matrix of shape function derivatives
	virtual void computeShapeDerivative(int q, MatrixXd& mH) const;
	virtual void computeShapeDerivative(const VectorXd& s, MatrixXd& mH) const;

	// Compute [nd x nd] matrix containing the deformation gradient
	virtual void computeDeformationGradient(const MatrixXd& mDs, MatrixXd& mF) const;
	virtual void computeDeformationGradient(int q, const MatrixXd& mDs, MatrixXd& mF) const;

	// Compute [nd x nd x nd] tensor containting deformation gradient derivative
	virtual void computeDeformationGradientDerivative(const MatrixXd& mDs, vector<MatrixXd>& vDFDx) const;
	virtual void computeDeformationGradientDerivative(int q, const MatrixXd& mDs, vector<MatrixXd>& vDFDx) const;

	// Get vectors containting quadrature points and weights integrate element
	virtual void getQuadrature(vector<Real>& vwq, vector<VectorXd>& vsq) const;

	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);
	virtual void updateEnergyForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForceJacobian(const VectorXd& vx, const VectorXd& vv);
	virtual void updateEnergyForceJacobian(const VectorXd& vx, const VectorXd& vv);
	
	// Abstract elements to implement by 
	virtual Real computeEnergyDensity(const MatrixXd& mF) const = 0;
	virtual void computePiolaKirchhoffFirst(const MatrixXd& mF, MatrixXd& mP) const = 0;
	virtual void computePiolaKirchhoffFirstDerivative(const MatrixXd& mF, const vector<MatrixXd>& vDFDx, vector<MatrixXd>& vDPDx) const = 0;
	
	
protected:

	FEMType m_type;				// Disc. type
	int m_numQuad;				// Sample number
	Real m_volume0;				// Volume at rest

	// This vectors store precomputed (Dm*H(s_i))^-1 at 
	// each of the quadrature points used to approximate
	// shape function derivatives

	vector<Real> m_vwq;			// Quadrature weights
	vector<VectorXd> m_vsq;		// Quadrature points

	vector<Real> m_vdetDmH0;		// Precomputed det([dX/ds]q) = dVX/dVs
	vector<MatrixXd> m_vHDmHi0;		// Precomputed Hq*(Dm*Hq)^-1 part of F	
	
};

#endif