/*=====================================================================================*/
/*!
\file		DERSlideStretchElement.h
\author		jesusprod
\brief		Implementation of DERSlideStretchElement.h.
*/
/*=====================================================================================*/

#ifndef DER_SLIDE_STRETCH_ELEMENT_H
#define DER_SLIDE_STRETCH_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/DERSlideElement.h>

class DERSlideStretchElement : public DERSlideElement
{
public:
	DERSlideStretchElement();
	~DERSlideStretchElement();

	virtual void updateRest(const dVector& vx, const dVector& vs);
	virtual void updateEnergy(const dVector& vx, const dVector& vs);
	virtual void updateForce(const dVector& vx, const dVector& vs);
	virtual void updateJacobian(const dVector& vx, const dVector& vs);

	virtual Real getIntegrationVolume() const { return this->m_L0; }

	virtual Real computeK() const;

	virtual Real getRestLength() const { return this->m_L0; }
	virtual void setRestLength(Real L0) { this->m_L0 = L0; }

protected:

	Real m_L0; // Rest length
};

#endif