/*=====================================================================================*/
/*! 
\file		FEMElementStVK.cpp
\author		jesusprod
\brief		Implementation of FEMElementStVK.h
 */
/*=====================================================================================*/

#include <JSandbox/FEMElementStVK.h>

 

Real FEMElementStVK::computeEnergyDensity(const MatrixXd& mF) const
{
	assert(mF.rows() == this->m_numDim);
	assert(mF.cols() == this->m_numDim);

	//	Psi(e) = m*(E:E) + 0.5*l*tr^2(E)
	//	where E:E is the Frobenious product
	//	Sum_i Sum_j {E_ij*E_ij} = tr(E^T*E)

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	

	MatrixXd mE; // Cauchy strain
	computeCauchyStrain(mF, mE);

	double trE = mE.trace();
	double trETE = (mE.transpose()*mE).trace();
	return trETE*lame1 + 0.5*lame2*trE*trE;
}

void FEMElementStVK::computePiolaKirchhoffFirst(const MatrixXd& mF, MatrixXd& mP) const
{
	assert(mF.rows() == this->m_numDim);
	assert(mF.cols() == this->m_numDim);

	// P(E) = F*(2*m*E + l*tr(E)*I)

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	

	MatrixXd mE; // Green strain
	computeGreenStrain(mF, mE);

	mP = mF*(2*lame1*mE + lame2*mE.trace()*MatrixXd::Identity(this->m_numDim, this->m_numDim));
}

void FEMElementStVK::computePiolaKirchhoffFirstDerivative(const MatrixXd& mF, const vector<MatrixXd>& vDFDx, vector<MatrixXd>& vDPDx) const
{
	assert(mF.rows() == this->m_numDim);
	assert(mF.cols() == this->m_numDim);
	assert((int) vDFDx.size() == this->m_nrawDOF);

	MatrixXd mI = MatrixXd::Identity(this->m_numDim, this->m_numDim);

	double lame1 = this->m_vpmat[0]->getLameFirst();
	double lame2 = this->m_vpmat[0]->getLameSecond();	
	
	MatrixXd mE; // Green strain
	computeGreenStrain(mF, mE);

	MatrixXd mT = 2*lame1*mE + lame2*mE.trace()*mI;

	vDPDx.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_nrawDOF; ++i) // Component derivatives: nv * nd
	{
		MatrixXd mDEDx = 0.5*(vDFDx[i].transpose()*mF + mF.transpose()*vDFDx[i]);
		vDPDx[i] = vDFDx[i]*mT + mF*(2*lame1*mDEDx + lame2*mDEDx.trace()*mI);
	}
}