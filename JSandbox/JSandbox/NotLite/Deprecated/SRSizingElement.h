/*=====================================================================================*/
/*!
\file		SRSizingElement.h
\author		jesusprod
\brief		Basic interface for any sizing criteria to be used with a surface model
			ARC remesher. This sizing must have access to the model insights as they
			can be motivated by both geometry and mechanics.
*/
/*=====================================================================================*/

// TODO: This class is still not working, do not use it until this message is removed

#ifndef I_SR_SIZING_ELEMENT_H
#define I_SR_SIZING_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/SurfaceRemesherARC.h>

typedef Matrix3d SurfaceSizing;

class SRSizingElement
{
public:
	virtual void addSizingTensor(const RemeshOP& remeshOP, vector<SurfaceSizing>& vfsizes) = 0;
};

#endif