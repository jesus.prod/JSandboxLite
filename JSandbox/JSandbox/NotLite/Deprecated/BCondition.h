/*=====================================================================================*/
/*! 
\file		BCondition.h
\author		jesusprod
\brief		Basic implementation of a boundary condition. This provides functionality for
			incremental loading of the boundary condition. All boundary conditions should
			derive from this.
*/
/*=====================================================================================*/

#ifndef BCONDITION_H
#define BCONDITION_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidModel.h>

class JSANDBOX_EXPORT BCondition
{
public: 

	BCondition(SolidModel* pModel);

	virtual ~BCondition();

	// From BCondition

	virtual Real getEnergy(const VectorXd& vx, const VectorXd& vv) const;
	virtual void addForce(const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const;
	virtual void addJacobian(const VectorXd& vx, const VectorXd& vv, tVector& vJ) const;

	virtual int getNumNonZeros_Jacobian() const;

	virtual void constrain(VectorXd& vf) const;
	virtual void constrain(SolidModel* pModel) const;
	virtual void constrain(tVector& vJ) const;

	/*! Advance step.
	*/
	virtual int advanceBoundary();

	/*! Get/set the current step of the BC.
	*/
	virtual int getCurrentStep() const;
	virtual void setCurrentStep(int s);

	/*! Get/set the maximum step of the BC.
	*/
	virtual int getMaximumStep() const;
	virtual void setMaximumStep(int s);

	/*! Is the BC fully loaded.
	*/
	virtual bool isFullyLoaded() const;

	/*! Get the current loading factor
	*/
	virtual Real getLoadingFactor() const;

protected:

	int m_maxStep;
	int m_curStep;

	SolidModel* m_pModel;

};

#endif