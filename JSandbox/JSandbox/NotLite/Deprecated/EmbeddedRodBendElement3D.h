/*=====================================================================================*/
/*!
\file		EmbeddedRodBendElement3D.h
\author		jesusprod
\brief		Embedded rod angle preserving energy in 3D. It uses SolidMaterial bending K.
*/
/*=====================================================================================*/

#ifndef EMBEDDED_ROD_BEND_ELEMENT_3D_H
#define EMBEDDED_ROD_BEND_ELEMENT_3D_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/EmbeddedSolidElement.h>

class EmbeddedRodBendElement3D : public EmbeddedSolidElement
{
public:
	EmbeddedRodBendElement3D(int dim0);
	~EmbeddedRodBendElement3D();

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real computeB() const;

	//virtual void compute_mJxx(const VectorXd& vx, const VectorXd& vv, MatrixXd& mJxx);
	//virtual void compute_mJnn(const VectorXd& vx, const VectorXd& vv, MatrixXd& mJnn);
	//virtual void compute_mJxn(const VectorXd& vx, const VectorXd& vv, MatrixXd& mJxn);
	//virtual void compute_mJnx(const VectorXd& vx, const VectorXd& vv, MatrixXd& mJnx);
	//virtual void compute_DnDv(const VectorXd& vx, const VectorXd& vv, MatrixXd& mDnDv);
	//virtual void compute_D2nDv2(const VectorXd& vx, const VectorXd& vv, vector<MatrixXd>& mD2nDv2);

	virtual Real getVoronoiLength() const { return this->m_vL0; }
	virtual void setVoronoiLength(Real vl0) { this->m_vL0 = vl0; }

	virtual Real getIntegrationVolume() const { return this->m_vL0; }

protected:
	Real m_vL0;
	Real m_e0norm0;
	Real m_e1norm0;
	Vector2d m_k0;
};

#endif