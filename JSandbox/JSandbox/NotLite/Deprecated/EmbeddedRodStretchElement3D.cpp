/*=====================================================================================*/
/*!
\file		EmbeddedRodStretchElement3D.cpp
\author		jesusprod
\brief		Implementation of EmbeddedRodStretchElement3D.h
*/
/*=====================================================================================*/

#include <JSandbox/EmbeddedRodStretchElement3D.h>

EmbeddedRodStretchElement3D::EmbeddedRodStretchElement3D(int nX) : EmbeddedSolidElement(2, 3, 3, nX)
{
	assert(nX == 2 || nX == 3);
}

EmbeddedRodStretchElement3D::~EmbeddedRodStretchElement3D()
{
	// Nothing to do here
}

void EmbeddedRodStretchElement3D::updateRest(const VectorXd& vX)
{
	if (this->m_numDim0 == 3)
	{
		vector<Vector3d> vp;
		this->getEmbbededPositions3D0(vX, vp);
		const Vector3d& x0 = vp[0];
		const Vector3d& x1 = vp[1];
		Vector3d e0 = x1 - x0;
		this->m_L0 = e0.norm();
	}
	else if (this->m_numDim0 == 2)
	{
		vector<Vector2d> vp;
		this->getEmbbededPositions2D0(vX, vp);
		const Vector2d& x0 = vp[0];
		const Vector2d& x1 = vp[1];
		Vector2d e0 = x1 - x0;
		this->m_L0 = e0.norm();
	}
	else assert(false);

	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double d = this->m_vpmat[0]->getDensity();
	double nodeMass = 0.5*d*m_L0*w*h*M_PI;

	int count = 0;
 	for (int i = 0; i < this->m_numNodeSim; ++i)
		for (int j = 0; j < this->m_numDim; ++j)
			this->m_vmass[count++] = nodeMass*this->m_vembCor[i];
}

void EmbeddedRodStretchElement3D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x1(3);
	vector<double> x2(3);

	this->getEmbbededPositions3D(vx, vp);

	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vp[0](i);
		x2[i] = vp[1](i);
	}

	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = this->computeB();

//#include "../../Maple/Code/RodEdge3DEnergy.mcg"

	//this->m_energy = t21;
}

void EmbeddedRodStretchElement3D::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x1(3);
	vector<double> x2(3);

	this->getEmbbededPositions3D(vx, vp);

	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vp[0](i);
		x2[i] = vp[1](i);
	}

	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = this->computeB();

	dVector vf6(6);

//#include "../../Maple/Code/RodEdge3DForce.mcg"

	for (int i = 0; i < 6; ++i)
		if (!isfinite(vf6[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);

	this->addForcesToEmbedding(vf6);
}

void EmbeddedRodStretchElement3D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x1(3);
	vector<double> x2(3);

	this->getEmbbededPositions3D(vx, vp);

	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vp[0](i);
		x2[i] = vp[1](i);
	}

	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = this->computeB();

	vector<dVector> mJ(6);
	for (int i = 0; i < 6; ++i)
		mJ[i].resize(6); // Init.

//#include "../../Maple/Code/RodEdge3DJacobian.mcg"

	for (int i = 0; i < 6; ++i)
		for (int j = 0; j < 6; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	this->addJacobianToEmbedding(mJ);
}

double EmbeddedRodStretchElement3D::computeB() const
{
	double Y = this->m_vpmat[0]->getYoung();
	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double A = M_PI * w * h;
	return A*Y;
}