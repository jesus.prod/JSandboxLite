/*=====================================================================================*/
/*!
\file		EmbeddedSolidElement.h
\author		jesusprod
\brief		EmbeddedSolidElement declaration.
*/
/*=====================================================================================*/

#ifndef EMBEDDED_SOLID_ELEMENT_H
#define EMBEDDED_SOLID_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>


class EmbeddedSolidElement : public NodalSolidElement
{
public:
	EmbeddedSolidElement(int nn, int nc, int dx, int dX) : NodalSolidElement(nn*nc, dx, dX)
	{
		this->m_numEmbCoord = nc;
		this->m_numEmbNodes = nn;
	}

	virtual ~EmbeddedSolidElement() { }

	virtual int getEmbeddedNodes() const { return this->m_numEmbNodes;  }
	virtual int getCoordinatesPerNode() const { return m_numEmbCoord; }

	virtual const dVector& getEmbeddingCoordinates() const;
	virtual void setEmbCoordinates(const dVector& vi);

	virtual void addForcesToEmbedding(const dVector& vembFVal);
	virtual void addJacobianToEmbedding(const vector<dVector>& vembJVal);
	virtual void getEmbbededPositions3D0(const VectorXd& vx, vector<Vector3d>& vp);
	virtual void getEmbbededPositions2D0(const VectorXd& vx, vector<Vector2d>& vp);
	virtual void getEmbbededPositions3D(const VectorXd& vx, vector<Vector3d>& vp);
	virtual void getEmbbededPositions2D(const VectorXd& vx, vector<Vector2d>& vp);

	virtual void updateRest(const VectorXd& vX) = 0;
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv) = 0;

protected:

	int m_numEmbCoord;
	int m_numEmbNodes;
	iVector m_vembIdx;
	dVector m_vembCor;

	MatrixXd m_mDxDv;

};

#endif