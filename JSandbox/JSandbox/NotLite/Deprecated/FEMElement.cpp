/*=====================================================================================*/
/*! 
\file		FEMElement.cpp
\author		jesusprod
\brief		Implementation of FEMElement.h
 */
/*=====================================================================================*/

#include <JSandbox/FEMElement.h>

FEMElement::FEMElement(int nv, int nd, FEMType D) : NodalSolidElement(nv, nd, nd)
{
	switch (D)
	{
	case Tet1:
		assert(nv == 4);
		assert(nd == 3);
		this->m_numQuad = 1;
		this->m_type = D;
		break;

	case Hex3:
		assert(nv == 8);
		assert(nd == 3);
		this->m_numQuad = 8;
		this->m_type = D;
		break;

	default: assert(false);
	}
}

FEMElement::~FEMElement() 
{
	// Nothing to do here
}

void FEMElement::updateRest(const VectorXd& vX)
{
	this->getQuadrature(this->m_vwq, this->m_vsq);

	// Deformation
	MatrixXd Dm, Hq;
	this->buildNodeMatrix(vX, Dm);
	this->m_vHDmHi0.resize(this->m_numQuad);
	this->m_vdetDmH0.resize(this->m_numQuad); 
	for (int q = 0; q < this->m_numQuad; ++q)
	{
		// Precompute the part of deformation gradient which remains
		// constant throughout the simulation, that is Hq*(Dm*Hq)^-1
		// where:
		// - Hq: (nv x nd) matrix of shape function derivatives at sq
		// - Dm: (nd x nv) matrix of element node positions at rest
		//
		// We store a matrix of this kind for each quadrature point
		// needed to approximate shape function nodal derivatives 

		this->computeShapeDerivative(this->m_vsq[q], Hq);
		
		MatrixXd DmHq = Dm*Hq;
		this->m_vdetDmH0[q] = DmHq.determinant();
		this->m_vHDmHi0[q] = Hq*(DmHq.inverse()); 
	}

	this->m_volume0 = this->computeVolume(Dm);
}

void FEMElement::buildNodeMatrix(const VectorXd& vx, MatrixXd& mD) const
{
	mD.resize(this->m_numDim, this->m_numNodeSim);
	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim*i;
		for (int d = 0; d < this->m_numDim; ++d)
			mD(d, i) = vx[this->m_vidx[offset + d]];
	}
}

Real FEMElement::computeVolume(const MatrixXd& mDs) const
{
	assert(mDs.cols() == this->m_numNodeSim);
	assert(mDs.rows() == this->m_numDim);

	double vol = 0.0;

	if (this->m_type == Tet1)
	{
		Matrix3d volM;
		Vector3d x0 = mDs.col(0);
		volM.col(0) = mDs.col(1) - x0;
		volM.col(1) = mDs.col(2) - x0;
		volM.col(2) = mDs.col(3) - x0;
		vol = abs(volM.determinant()/6);
	}
	else if (this->m_type == Hex3)
	{
		Matrix3d volM;
		Vector3d x0 = mDs.col(0);
		volM.col(0) = mDs.col(1) - x0;
		volM.col(1) = mDs.col(3) - x0;
		volM.col(2) = mDs.col(4) - x0;
		vol = abs(volM.determinant());
	}
	else assert(false);
	
	return vol;
}

void FEMElement::computeShapeFunction(int q, VectorXd& vN) const
{
	assert(q >= 0 && q < this->m_numQuad);
	this->computeShapeFunction(this->m_vsq[q], vN);
}

void FEMElement::computeShapeFunction(const VectorXd& vs, VectorXd& vN) const
{
	// TODO: Optimize this. Hardcode to avoid calling pow

	if (this->m_type == Tet1)
	{
		assert(vs.size() == 4);
		vN = vs; // N_i(s) = s_i
	}
	else if (this->m_type == Hex3)
	{
		assert(vs.size() == 3);

		vN.resize(this->m_numNodeSim);

		// N(s) = (1.0/8.0) * (1 + (-1)^i s_1) * 
		//					  (1 + (-1)^j s_2) * 
		//					  (1 + (-1)^k s_3)
		for (int i = 1; i <= 2; ++i) 
			for (int j = 1; j <= 2; ++j) 
				for (int k = 1; k <= 2; ++k)
					vN[4*(i-1) + 2*(j-1) + k] = (1.0/8.0) *
												(1 + pow(-1.0, (double)i)*vs[0]) *
												(1 + pow(-1.0, (double)j)*vs[1]) *
												(1 + pow(-1.0, (double)k)*vs[2]);
	}
	else assert(false);
}

void FEMElement::computeShapeDerivative(int q, MatrixXd& mH) const
{
	assert(q >= 0 && q < this->m_numQuad);
	this->computeShapeDerivative(this->m_vsq[q], mH);
}

void FEMElement::computeShapeDerivative(const VectorXd& vs, MatrixXd& mH) const
{
	// TODO: Optimize this. Hardcode to avoid calling pow

	mH.resize(this->m_numNodeSim, this->m_numDim);

	if (this->m_type == Tet1)
	{
		assert(vs.size() == 4);

		mH.row(0) = Vector3d(-1.0, -1.0, -1.0);
		mH.row(1) = Vector3d(1.0, 0.0, 0.0);
		mH.row(2) = Vector3d(0.0, 1.0, 0.0);
		mH.row(3) = Vector3d(0.0, 0.0, 1.0);
	}
	else if (this->m_type == Hex3)
	{
		assert(vs.size() == 3);

		// dN(s)/ds = (1.0/8.0) * 
		//			  [(-1)^i * (1 + (-1)^j s_2) * (1 + (-1)^k s_3),
		//			   (-1)^j * (1 + (-1)^i s_1) * (1 + (-1)^k s_3),
		//			   (-1)^k * (1 + (-1)^i s_1) * (1 + (-1)^j s_2)]
		for (int i = 1; i <= 2; ++i) 
			for (int j = 1; j <= 2; ++j) 
				for (int k = 1; k <= 2; ++k)
				{
					double c0 = (1 + pow(-1.0, (double)i)*vs[0]);
					double c1 = (1 + pow(-1.0, (double)i)*vs[1]);
					double c2 = (1 + pow(-1.0, (double)i)*vs[2]);
					Vector3d t(pow(-1.0, (double)i)*c1*c2,
							   pow(-1.0, (double)j)*c0*c2,
							   pow(-1.0, (double)k)*c0*c1);
					mH.row(4*(i-1) + 2*(j-1) + k) = (1.0/8.0) * t;
				}
	}
	else assert(false);
}

void FEMElement::computeDeformationGradient(const MatrixXd& mDs, MatrixXd& mF) const
{
	mF.setZero(); // Approximate integral
	for (int q = 0; q < this->m_numQuad; ++q)
		mF += this->m_vwq[q] * mDs * this->m_vHDmHi0[q]; 
}

void FEMElement::computeDeformationGradient(int q, const MatrixXd& mDs, MatrixXd& mF) const
{
	assert(q >= 0 && q < this->m_numQuad);
	mF = mDs * this->m_vHDmHi0[q]; // At point q
}

void FEMElement::computeDeformationGradientDerivative(const MatrixXd& mDs, vector<MatrixXd>& vDFDx) const
{
	// TODO: This seems to be constant throughout the simulation, consider precomputing

	assert(mDs.rows() == this->m_numDim);
	assert(mDs.cols() == this->m_numNodeSim);
	vDFDx.resize(this->m_nrawDOF);

	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim*i;
		for (int d = 0; d < this->m_numDim; ++d)
		{
			int index = offset + d; // Store all nd*nv derivatives
			vDFDx[index] = MatrixXd(this->m_numDim, this->m_numDim);
 
			// Integrate element
			vDFDx[index].setZero(); 
			for (int q = 0; q < this->m_numQuad; ++q)
				vDFDx[index].row(d) += this->m_vHDmHi0[q].row(i) * this->m_vwq[q];
		}
	}
}

void FEMElement::computeDeformationGradientDerivative(int q, const MatrixXd& mDs, vector<MatrixXd>& vDFDx) const
{
	// TODO: This seems to be constante throughout the simulation, consider precomputing

	assert(mDs.rows() == this->m_numDim);
	assert(mDs.cols() == this->m_numNodeSim);
	assert(q >= 0 && q < this->m_numQuad);
	vDFDx.resize(this->m_nrawDOF);

	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim*i;
		for (int d = 0; d < this->m_numDim; ++d)
		{
			int index = offset + d; // Store all nd*nv F derivatives
			vDFDx[index] = MatrixXd(this->m_numDim, this->m_numDim);
			vDFDx[index].row(d) == this->m_vHDmHi0[q].row(i);
		}
	}
}

void FEMElement::getQuadrature(vector<Real>& vwq, vector<VectorXd>& vsq) const
{
	if (this->m_type == Tet1)
	{
		// TODO: Make sure of this. Generic formulation using isoparametric
		// elements describes integration of a function f(F) throughout the
		// volume of an element E as:
		//
		// Int_Em { f(F) dX } = Int_Es { f(F) det(dX/ds) ds}
		//
		// Here Em is the element in material space while Es is the ideal
		// element in iso-parametric space; det([dX/ds]q) is the derivative
		// of volume change in material space w.r.t. iso-parametric volume.
		//
		// In general, this is approximated using gaussian quadrature as:
		// Sum_q { f(Fq) det([dX/ds]q) wq}, where [dX/ds]q = (Dm*Hq).
		//
		// For this formulation to be consistent with integrating in
		// material space det([dX/ds]q) wq should be equal to V0 (rest
		// volume of the tetrahedron) and so wq = 1.0/6.0. This is 
		// reasonable since the volume of the ideal tetrahedron 
		// (unit size edges is 1.0/6.0 units.

		vwq.resize(1);
		vsq.resize(1);
		vwq[0] = 1.0/6.0; 
		vsq[0] = Vector4d(0.25, 0.25, 0.25, 0.25);
	}
	else if (this->m_type == Hex3)
	{
		// TODO: Optimize this. Hardcode to avoid calling pow

		double c = 1.0/sqrt(3.0);
		vwq.resize(8);
		vsq.resize(8);
		for (int i = 1; i <= 2; ++i) 
			for (int j = 1; j <= 2; ++j) 
				for (int k = 1; k <= 2; ++k)
				{
					int index = 4*(i-1) + 2*(j-1) + k;
					vwq[index] = 1.0;
					vsq[index] = Vector3d(pow(-1.0, (double)i)*c,
										  pow(-1.0, (double)j)*c,
										  pow(-1.0, (double)k)*c);
				}
	}
	else assert(false);
}

void FEMElement::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	this->m_energy = 0;

	MatrixXd mDs;
	this->buildNodeMatrix(vx, mDs);
	for (int q = 0; q < this->m_numQuad; ++q)
	{
		MatrixXd mFq;
		this->computeDeformationGradient(q, mDs, mFq);	
		Real Psi = this->computeEnergyDensity(mFq);

		this->m_energy += Psi * this->m_vdetDmH0[q] * this->m_vwq[q];
	}
}

void FEMElement::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	MatrixXd mDs; // Node matrix
	this->buildNodeMatrix(vx, mDs);

	// Restart values
	this->m_vfVal.setZero();

	// Approximate force/Jacobian by quadrature
	for (int q = 0; q < this->m_numQuad; ++q)
	{
		MatrixXd mFq, mPq;
		vector<MatrixXd> vmDFDxq, vmDPDxq;
		this->computeDeformationGradient(q, mDs, mFq); // Strain tensor
		this->computePiolaKirchhoffFirst(mFq, mPq); // Stress tensor
			
		MatrixXd Bq = this->m_vHDmHi0[q].transpose() * this->m_vdetDmH0[q] * this->m_vwq[q];
		MatrixXd Gq = -1.0 * mPq * Bq; // Compute all forces simultaneously using closed form

		// Add force to the precomputed vector
		for (int i = 0; i < this->m_numNodeSim; ++i)
		{
			int offset = this->m_numDim*i;
			for (int d = 0; d < this->m_numDim; ++d)
				this->m_vfVal[offset + d] += Gq(d, i);
		}
	}
}

void FEMElement::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	MatrixXd mDs; // Node matrix
	this->buildNodeMatrix(vx, mDs);

	// Restart values
	this->m_vJVal.setZero();
		
	MatrixXd K(this->m_nrawDOF, this->m_nrawDOF);
	K.setZero();

	// Approximate force/Jacobian by quadrature
	for (int q = 0; q < this->m_numQuad; ++q)
	{
		MatrixXd mFq, mPq;
		vector<MatrixXd> vmDFDxq, vmDPDxq;
		this->computeDeformationGradient(q, mDs, mFq); // Strain tensor
		this->computePiolaKirchhoffFirst(mFq, mPq); // Stress tensor
			
		MatrixXd Bq = this->m_vHDmHi0[q].transpose() * this->m_vdetDmH0[q] * this->m_vwq[q];

		// TODO: Optimize this. Right now, the Jacobian of the forces is computed
		// so that derived classes only are required to implement scalar derivatives
		// of the Piola-Kirchhoff stress formula. This results in nd*nv matrices of
		// size [nd x nd] which are assembled as follows:
		//
		// G = -1.0 * P(F) * B^T where B = H*(Dm*H)^-1 * det(Dm*H) =>
		// => fi = - P(F) * bi where bi = col_i(B^T) =>
		// => dfi/dxj = - dP(F)/dxj * bi 
		//
		// This results in the inner product of a third-order tensor DP times a 
		// first-order tensor, that is, a second-order tensor whose components 
		// are: Klm = - [dP(F)/dxj]_lmo * [bi]_o
		//
		// It should be nice to find a closed form for the derivative computation
		// that at the same time minimizes repeating code at derived classes.

		this->computeDeformationGradientDerivative(q, mDs, vmDFDxq); // Strain derivative
		this->computePiolaKirchhoffFirstDerivative(mFq, vmDFDxq, vmDPDxq); // Stress derivative

		for (int i = 0; i < this->m_numNodeSim; ++i)
		{
			VectorXd bi = Bq.col(i);
			for (int j = 0; j <= i; ++j)
			{
				int offset = this->m_numDim*j;
				
				MatrixXd Kij(this->m_numDim, this->m_numDim);
				Kij.setZero();

				// Perform Rank3-Rank1 inner product
				for (int n = 0; n < this->m_numDim; ++n) 
					for (int m = 0; m < this->m_numDim; ++m)
						for (int o = 0; o < this->m_numDim; ++o) 
							Kij(n, m) += vmDPDxq[offset + o](n, m)*bi(o);

				addBlockNxM(this->m_numDim, this->m_numDim, i, j, Kij, K, true);
			}
		}
	}

	int count = 0;
	for (int i = 0; i < this->m_nrawDOF; ++i) 
		for (int j = 0; j <= i; ++j) // Sym
			this->m_vJVal(count++) = K(i,j);
}


void FEMElement::updateEnergyForce(const VectorXd& vx, const VectorXd& vv)
{
	MatrixXd mDs; // Node matrix
	this->buildNodeMatrix(vx, mDs);

	// Restart values
	this->m_energy = 0;
	this->m_vfVal.setZero();

	// Approximate force/Jacobian by quadrature
	for (int q = 0; q < this->m_numQuad; ++q)
	{
		MatrixXd mFq, mPq;
		vector<MatrixXd> vmDFDxq, vmDPDxq;
		this->computeDeformationGradient(q, mDs, mFq); // Strain tensor
		this->computePiolaKirchhoffFirst(mFq, mPq); // Stress tensor

		// ENERGY

		Real Psi = this->computeEnergyDensity(mFq); // Compute energy
		this->m_energy += Psi * this->m_vdetDmH0[q] * this->m_vwq[q];

		// FORCE
			
		MatrixXd Bq = this->m_vHDmHi0[q].transpose() * this->m_vdetDmH0[q] * this->m_vwq[q];
		MatrixXd Gq = -1.0 * mPq * Bq; // Compute all forces simultaneously using closed form

		// Add force to the precomputed vector
		for (int i = 0; i < this->m_numNodeSim; ++i)
		{
			int offset = this->m_numDim*i;
			for (int d = 0; d < this->m_numDim; ++d)
				this->m_vfVal[offset + d] += Gq(d, i);
		}
	}
}

void FEMElement::updateForceJacobian(const VectorXd& vx, const VectorXd& vv)
{
	MatrixXd mDs; // Node matrix
	this->buildNodeMatrix(vx, mDs);

	// Restart values
	this->m_vfVal.setZero();
	this->m_vJVal.setZero();

	MatrixXd K(this->m_nrawDOF, this->m_nrawDOF);
	K.setZero();

	// Approximate force/Jacobian by quadrature
	for (int q = 0; q < this->m_numQuad; ++q)
	{
		MatrixXd mFq, mPq;
		vector<MatrixXd> vmDFDxq, vmDPDxq;
		this->computeDeformationGradient(q, mDs, mFq); // Strain tensor
		this->computePiolaKirchhoffFirst(mFq, mPq); // Stress tensor

		// FORCE

		MatrixXd Bq = this->m_vHDmHi0[q].transpose() * this->m_vdetDmH0[q] * this->m_vwq[q];
		MatrixXd Gq = -1.0 * mPq * Bq; // Compute all forces simultaneously using closed form

		// Add force to the precomputed vector
		for (int i = 0; i < this->m_numNodeSim; ++i)
		{
			int offset = this->m_numDim*i;
			for (int d = 0; d < this->m_numDim; ++d)
				this->m_vfVal[offset + d] += Gq(d, i);
		}

		// JACOBIAN

		this->computeDeformationGradientDerivative(q, mDs, vmDFDxq); // Strain derivative
		this->computePiolaKirchhoffFirstDerivative(mFq, vmDFDxq, vmDPDxq); // Stress derivative

		for (int i = 0; i < this->m_numNodeSim; ++i)
		{
			VectorXd bi = Bq.col(i);
			for (int j = 0; j <= i; ++j)
			{
				int offset = this->m_numDim*j;
				
				MatrixXd Kij(this->m_numDim, this->m_numDim);
				Kij.setZero();

				// Perform Rank3-Rank1 inner product
				for (int n = 0; n < this->m_numDim; ++n) 
					for (int m = 0; m < this->m_numDim; ++m)
						for (int o = 0; o < this->m_numDim; ++o) 
							Kij(n, m) += vmDPDxq[offset + o](n, m)*bi(o);

				addBlockNxM(this->m_numDim, this->m_numDim, i, j, Kij, K, true);
			}
		}
	}

	int count = 0;
	for (int i = 0; i < this->m_nrawDOF; ++i) 
		for (int j = 0; j <= i; ++j) // Sym
			this->m_vJVal(count++) = K(i,j);
}

void FEMElement::updateEnergyForceJacobian(const VectorXd& vx, const VectorXd& vv)
{
	MatrixXd mDs; // Node matrix
	this->buildNodeMatrix(vx, mDs);

	// Restart values
	this->m_energy = 0;
	this->m_vfVal.setZero();
	this->m_vJVal.setZero();

	MatrixXd K(this->m_nrawDOF, this->m_nrawDOF);
	K.setZero();

	// Approximate force/Jacobian by quadrature
	for (int q = 0; q < this->m_numQuad; ++q)
	{
		MatrixXd mFq, mPq;
		vector<MatrixXd> vmDFDxq, vmDPDxq;
		this->computeDeformationGradient(q, mDs, mFq); // Strain tensor
		this->computePiolaKirchhoffFirst(mFq, mPq); // Stress tensor

		// ENERGY

		Real Psi = this->computeEnergyDensity(mFq); // Compute energy
		this->m_energy += Psi * this->m_vdetDmH0[q] * this->m_vwq[q];
			
		// FORCE

		MatrixXd Bq = this->m_vHDmHi0[q].transpose() * this->m_vdetDmH0[q] * this->m_vwq[q];
		MatrixXd Gq = -1.0 * mPq * Bq; // Compute all forces simultaneously using closed form

		// Add force to the precomputed vector
		for (int i = 0; i < this->m_numNodeSim; ++i)
		{
			int offset = this->m_numDim*i;
			for (int d = 0; d < this->m_numDim; ++d)
				this->m_vfVal[offset + d] += Gq(d, i);
		}

		// JACOBIAN

		this->computeDeformationGradientDerivative(q, mDs, vmDFDxq); // Strain derivative
		this->computePiolaKirchhoffFirstDerivative(mFq, vmDFDxq, vmDPDxq); // Stress derivative

		for (int i = 0; i < this->m_numNodeSim; ++i)
		{
			VectorXd bi = Bq.col(i);
			for (int j = 0; j <= i; ++j)
			{
				int offset = this->m_numDim*j;
				
				MatrixXd Kij(this->m_numDim, this->m_numDim);
				Kij.setZero();

				// Perform Rank3-Rank1 inner product
				for (int n = 0; n < this->m_numDim; ++n) 
					for (int m = 0; m < this->m_numDim; ++m)
						for (int o = 0; o < this->m_numDim; ++o) 
							Kij(n, m) += vmDPDxq[offset + o](n, m)*bi(o);

				addBlockNxM(this->m_numDim, this->m_numDim, i, j, Kij, K, true);
			}
		}
	}

	int count = 0;
	for (int i = 0; i < this->m_nrawDOF; ++i) 
		for (int j = 0; j <= i; ++j) // Sym
			this->m_vJVal(count++) = K(i,j);
}

VectorXd FEMElement::toNaturalCoordinates(const VectorXd& vx) const
{
	return VectorXd(); // TODO
}

VectorXd FEMElement::toMaterialCoordinates(const VectorXd& vs) const
{
	return VectorXd(); // TODO
}