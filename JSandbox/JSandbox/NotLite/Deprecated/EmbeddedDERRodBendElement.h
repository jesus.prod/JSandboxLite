/*=====================================================================================*/
/*!
\file		EmbeddedDERRodBendElement.h
\author		jesusprod
\brief		Declaration of EmbeddedDERRodBendElement class
*/
/*=====================================================================================*/

#ifndef EMBEDDED_DER_ROD_BEND_ELEMENT_H
#define EMBEDDED_DER_ROD_BEND_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "MathUtils.h"
#include "SolidElement.h"

class EmbeddedDERRodBendElement : public SolidElement
{

public:
	EmbeddedDERRodBendElement();
	virtual ~EmbeddedDERRodBendElement();

	virtual const dVector& getEmbCoordinates() const;
	virtual void setEmbCoordinates(const dVector& vi);

	virtual void updateEnergy(const dVector& vx, const dVector& va, const vector<Frame>& vF);
	virtual void updateForce(const dVector& vx, const dVector& va, const vector<Frame>& vF);
	virtual void updateJacobian(const dVector& vx, const dVector& va, const vector<Frame>& vF);
	virtual void updateRest(const dVector& vx, const dVector& va, const vector<Frame>& vF);

	virtual Real computeEnergy(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1);
	virtual void computeForce(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, VectorXd& vf);
	virtual void computeJacobian(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& mJ);
	virtual void updateRest(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1);

	virtual MatrixXd computeB() const;
	virtual Vector3d computeKB(const Vector3d& e, const Vector3d& f) const;
	virtual Real computeVertexLength(const Vector3d& e, const Vector3d& f) const;
	virtual VectorXd computeKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1) const;
	virtual void computeGradKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& gK) const;
	virtual void computeHessKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& gK1, MatrixXd& gK2) const;

	virtual const iVector& getVIndices() { return this->m_vinds; }
	virtual const iVector& getEIndices() { return this->m_einds; }
	virtual void setVIndices(const iVector& vinds) { this->m_vinds = vinds; }
	virtual void setEIndices(const iVector& einds) { this->m_einds = einds; }

	virtual Real getIntegrationVolume() const { return this->m_l0; }

	virtual void updateRest(const VectorXd& vX) { }
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv) { }
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv) { }
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv) { }

	double getRestVertexLength() const { return this->m_l0; }
	void setRestVertexLength(double l0) { this->m_l0 = l0; }

	Vector2d getRestKappa() const { return this->m_K0; }
	void setRestKappa(Vector2d K0) { this->m_K0 = K0; }

	Real getConstant() const;

protected:

	iVector m_vinds;
	iVector m_einds;

	dVector m_vembCor;

	MatrixXd m_DvDq; // Embedding
	MatrixXd m_DeDv; // Transmission
	VectorXd m_K0; // Rest curvature
	Real m_l0; // Rest vertex length 

};

#endif
