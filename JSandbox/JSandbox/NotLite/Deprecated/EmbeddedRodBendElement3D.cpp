/*=====================================================================================*/
/*!
\file		EmbeddedRodBendElement3D.cpp
\author		jesusprod
\brief		Implementation of EmbeddedRodBendElement3D.h
*/
/*=====================================================================================*/

#include <JSandbox/EmbeddedRodBendElement3D.h>

EmbeddedRodBendElement3D::EmbeddedRodBendElement3D(int nX) : EmbeddedSolidElement(3, 3, 3, nX)
{
	assert(nX == 2 || nX == 3);
}

EmbeddedRodBendElement3D::~EmbeddedRodBendElement3D()
{
	// Nothing to do here
}

void EmbeddedRodBendElement3D::updateRest(const VectorXd& vX)
{
	if (this->m_numDim0 == 3)
	{
		vector<Vector3d> vp;
		this->getEmbbededPositions3D0(vX, vp);
		const Vector3d& x0 = vp[0];
		const Vector3d& x1 = vp[1];
		const Vector3d& x2 = vp[2];
		Vector3d e0 = x1 - x0;
		Vector3d e1 = x2 - x1;
		this->m_e0norm0 = e0.norm();
		this->m_e1norm0 = e1.norm();
		this->m_vL0 = 0.5*(m_e0norm0 + m_e1norm0);
	}
	else if (this->m_numDim0 == 2)
	{
		vector<Vector2d> vp;
		this->getEmbbededPositions2D0(vX, vp);
		const Vector2d& x0 = vp[0];
		const Vector2d& x1 = vp[1];
		const Vector2d& x2 = vp[2];
		Vector2d e0 = x1 - x0;
		Vector2d e1 = x2 - x1;
		this->m_e0norm0 = e0.norm();
		this->m_e1norm0 = e1.norm();
		this->m_vL0 = 0.5*(m_e0norm0 + m_e1norm0);
	}
	else assert(false);
}

//void EmbeddedRodBendElement3D::updateRest(const VectorXd& vX)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vX, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//	Vector3d e0 = x1 - x0;
//	Vector3d e1 = x2 - x1;
//	this->m_e0norm0 = e0.norm();
//	this->m_e1norm0 = e1.norm();
//	this->m_vL0 = 0.5*(m_e0norm0 + m_e1norm0);
//
//	// Get material frame
//	Vector3d t0 = e0.normalized();
//	Vector3d t1 = e1.normalized();
//	Vector3d v3 = getBlock3x1(this->m_vnodeIdx0[3], vX);
//	Vector3d v4 = getBlock3x1(this->m_vnodeIdx0[4], vX);
//	Vector3d v5 = getBlock3x1(this->m_vnodeIdx0[5], vX);
//	Vector3d te0 = v4 - v3;
//	Vector3d te1 = v5 - v3;
//	Vector3d d0, d1, d2;
//	d0 = 0.5*(t0 + t1);
//	d2 = te0.cross(te1).normalized();
//	d1 = d0.cross(d2).normalized();
//	d2 = d1.cross(d0).normalized();
//
//	Vector2d k;
//
//#include "../../Maple/Code/RodAngle3DRestCurvature.mcg"
//
//	this->m_k0 = k;
//}

//void EmbeddedRodBendElement3D::updateRest(const VectorXd& vX)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vX, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//	Vector3d ve0 = x1 - x0;
//	Vector3d ve1 = x2 - x1;
//	double ve0norm = ve0.norm();
//	double ve1norm = ve1.norm();
//	Vector3d vt0 = ve0*(1.0 / ve0norm);
//	Vector3d vt1 = ve1*(1.0 / ve1norm);
//	Vector3d vt = 0.5*(vt0 + vt1);
//
//	Vector3d v0, v1, v2;
//	v0 = getBlock3x1(this->m_vnodeIdx0[3], vX);
//	v1 = getBlock3x1(this->m_vnodeIdx0[4], vX);
//	v2 = getBlock3x1(this->m_vnodeIdx0[5], vX);
//
//	Vector3d vte0 = v1 - v0;
//	Vector3d vte1 = v2 - v0;
//	Vector3d vn = vte0.cross(vte1).normalized();
//
//	this->m_e0norm0 = ve0norm;
//	this->m_e1norm0 = ve1norm;
//	this->m_vL0 = 0.5*(ve0norm + ve1norm);
//
//	Vector2d K;
//
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_Curvature.mcg"
//
//	this->m_k0 = K;
//}

//void EmbeddedRodBendElement3D::updateRest(const VectorXd& vX)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vX, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//	Vector3d ve0 = x1 - x0;
//	Vector3d ve1 = x2 - x1;
//	double ve0norm = ve0.norm();
//	double ve1norm = ve1.norm();
//	Vector3d vt0 = ve0*(1.0/ve0norm);
//	Vector3d vt1 = ve1*(1.0/ve1norm);
//
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
//	v1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
//	v2 = getBlock3x1(this->m_vnodeIdx0[2], vX);
//	v3 = getBlock3x1(this->m_vnodeIdx0[3], vX);
//	v4 = getBlock3x1(this->m_vnodeIdx0[4], vX);
//	v5 = getBlock3x1(this->m_vnodeIdx0[5], vX);
//	v6 = getBlock3x1(this->m_vnodeIdx0[6], vX);
//	v7 = getBlock3x1(this->m_vnodeIdx0[7], vX);
//	v8 = getBlock3x1(this->m_vnodeIdx0[8], vX);
//	Vector3d n0 = (v1 - v0).cross(v2 - v0).normalized();
//	Vector3d n1 = (v4 - v3).cross(v5 - v3).normalized();
//	Vector3d n2 = (v7 - v6).cross(v8 - v6).normalized();
//
//	this->m_e0norm0 = ve0norm;
//	this->m_e1norm0 = ve1norm;
//	this->m_vL0 = 0.5*(ve0norm + ve1norm);
//
//	Vector2d K;
//
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_Curvature.mcg"
//
//	this->m_k0 = K;
//}

void EmbeddedRodBendElement3D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	
	this->getEmbbededPositions3D(vx, vp);
	
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vp[0](i);
		x1[i] = vp[1](i);
		x2[i] = vp[2](i);
	}
	
	double vL0 = this->m_vL0;
	
	double kB = this->computeB();
	
//#include "../../Maple/Code/RodAngle3DEnergy.mcg"
	
	//this->m_energy = t78;
}

//void EmbeddedRodBendElement3D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	vector<double> x0(3);
//	vector<double> x1(3);
//	vector<double> x2(3);
//	
//	this->getEmbbededPositions3D(vx, vp);
//	
//	for (int i = 0; i < 3; ++i)
//	{
//		x0[i] = vp[0](i);
//		x1[i] = vp[1](i);
//		x2[i] = vp[2](i);
//	}
//
//	// Get material frame
//	Vector3d e0 = vp[1] - vp[0];
//	Vector3d e1 = vp[2] - vp[1];
//	Vector3d t0 = e0.normalized();
//	Vector3d t1 = e1.normalized();
//	Vector3d v3 = getBlock3x1(this->m_vnodeIdx0[3], vx);
//	Vector3d v4 = getBlock3x1(this->m_vnodeIdx0[4], vx);
//	Vector3d v5 = getBlock3x1(this->m_vnodeIdx0[5], vx);
//	Vector3d te0 = v4 - v3;
//	Vector3d te1 = v5 - v3;
//	Vector3d d0, d1, d2;
//	d0 = 0.5*(t0 + t1);
//	d2 = te0.cross(te1).normalized();
//	d1 = d0.cross(d2).normalized();
//	d2 = d1.cross(d0).normalized();
//
//	Vector2d k0 = this->m_k0;
//	double vL0 = this->m_vL0;
//	double kB = m_vpmat[0]->getBendingK();
//	
//#include "../../Maple/Code/RodAngle3DRestEnergy.mcg"
//	
//	this->m_energy = t87;
//}

//void EmbeddedRodBendElement3D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//	Vector3d ve0 = x1 - x0;
//	Vector3d ve1 = x2 - x1;
//	double ve0norm = ve0.norm();
//	double ve1norm = ve1.norm();
//	Vector3d vt0 = ve0*(1.0 / ve0norm);
//	Vector3d vt1 = ve1*(1.0 / ve1norm);
//	Vector3d vt = 0.5*(vt0 + vt1);
//
//	Vector3d v0, v1, v2;
//	v0 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[5], vx);
//
//	Vector3d vte0 = v1 - v0;
//	Vector3d vte1 = v2 - v0;
//	Vector3d vn = vte0.cross(vte1).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = m_vpmat[0]->getBendingK();
//
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_Energy.mcg"
//
//	this->m_energy = t113;
//}

//void EmbeddedRodBendElement3D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//	Vector3d n0 = (v1 - v0).cross(v2 - v0).normalized();
//	Vector3d n1 = (v4 - v3).cross(v5 - v3).normalized();
//	Vector3d n2 = (v7 - v6).cross(v8 - v6).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = m_vpmat[0]->getBendingK();
//
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_Energy.mcg"
//
//	this->m_energy = t226;
//}

void EmbeddedRodBendElement3D::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	
	this->getEmbbededPositions3D(vx, vp);
	
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vp[0](i);
		x1[i] = vp[1](i);
		x2[i] = vp[2](i);
	}
	
	double vL0 = this->m_vL0;

	double kB = this->computeB();
	
	dVector vf9(9);
	
//#include "../../Maple/Code/RodAngle3DForce.mcg"
	
	for (int i = 0; i < 9; ++i)
		if (!isfinite(vf9[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
	
	this->addForcesToEmbedding(vf9);
}

//void EmbeddedRodBendElement3D::updateForce(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	vector<double> x0(3);
//	vector<double> x1(3);
//	vector<double> x2(3);
//
//	this->getEmbbededPositions3D(vx, vp);
//
//	for (int i = 0; i < 3; ++i)
//	{
//		x0[i] = vp[0](i);
//		x1[i] = vp[1](i);
//		x2[i] = vp[2](i);
//	}
//
//	// Get material frame
//	Vector3d e0 = vp[1] - vp[0];
//	Vector3d e1 = vp[2] - vp[1];
//	Vector3d t0 = e0.normalized();
//	Vector3d t1 = e1.normalized();
//	Vector3d v3 = getBlock3x1(this->m_vnodeIdx0[3], vx);
//	Vector3d v4 = getBlock3x1(this->m_vnodeIdx0[4], vx);
//	Vector3d v5 = getBlock3x1(this->m_vnodeIdx0[5], vx);
//	Vector3d te0 = v4 - v3;
//	Vector3d te1 = v5 - v3;
//	Vector3d d0, d1, d2;
//	d0 = 0.5*(t0 + t1);
//	d2 = te0.cross(te1).normalized();
//	d1 = d0.cross(d2).normalized();
//	d2 = d1.cross(d0).normalized();
//
//	Vector2d k0 = this->m_k0;
//	double vL0 = this->m_vL0;
//	double kB = m_vpmat[0]->getBendingK();
//
//	dVector vf9(9);
//
//#include "../../Maple/Code/RodAngle3DRestForce.mcg"
//
//	for (int i = 0; i < 9; ++i)
//		if (!isfinite(vf9[i]))
//			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
//
//	this->addForcesToEmbedding(vf9);
//}

//void EmbeddedRodBendElement3D::updateForce(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//	Vector3d ve0 = x1 - x0;
//	Vector3d ve1 = x2 - x1;
//	double ve0norm = ve0.norm();
//	double ve1norm = ve1.norm();
//	Vector3d vt0 = ve0*(1.0 / ve0norm);
//	Vector3d vt1 = ve1*(1.0 / ve1norm);
//	Vector3d vt = 0.5*(vt0 + vt1);
//
//	Vector3d v0, v1, v2;
//	v0 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[5], vx);
//
//	Vector3d vte0 = v1 - v0;
//	Vector3d vte1 = v2 - v0;
//	Vector3d vn = vte0.cross(vte1).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = m_vpmat[0]->getBendingK();
//
//	dVector vfx(9);
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_ForceX.mcg"
//	
//		for (int i = 0; i < 9; ++i)
//			if (!isfinite(vfx[i]))
//				qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
//	}
//
//	dVector vfn(3);
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_ForceN.mcg"
//
//		for (int i = 0; i < 3; ++i)
//			if (!isfinite(vfn[i]))
//				qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
//	}
//
//	double mDnDv[3][9];
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_DnDv.mcg"
//
//		for (int i = 0; i < 3; ++i)
//			for (int j = 0; j < 9; ++j)
//				if (!isfinite(mDnDv[i][j]))
//					qDebug("In %s: DnDv value is NAN\n", __FUNCDNAME__);
//	}
//
//	// Forces at rod vertices
//
//	VectorXd vfxE = toEigen(vfx);
//
//	// Force at rod normal
//
//	VectorXd vfnE = toEigen(vfn);
//
//	MatrixXd mDnDvE(3,9);
//	for (int i = 0; i < 3; ++i)
//		for (int j = 0; j < 9; ++j)
//			mDnDvE(i, j) = mDnDv[i][j];
//	
//	VectorXd vfxv = m_mDxDv.transpose()*vfxE;
//	VectorXd vfnv = mDnDvE.transpose()*vfnE;
//
//	// Add forces
//	this->m_vfVal = vfxv;
//	for (int i = 0; i < 9; ++i) 
//		this->m_vfVal[9+i] += vfnv[i];
//}

//void EmbeddedRodBendElement3D::updateForce(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//	Vector3d n0 = (v1 - v0).cross(v2 - v0).normalized();
//	Vector3d n1 = (v4 - v3).cross(v5 - v3).normalized();
//	Vector3d n2 = (v7 - v6).cross(v8 - v6).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = m_vpmat[0]->getBendingK();
//
//	dVector vfx(9);
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_ForceX.mcg"
//
//		for (int i = 0; i < 9; ++i)
//			if (!isfinite(vfx[i]))
//				qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
//	}
//
//	dVector vfn(9);
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_ForceN.mcg"
//
//		for (int i = 0; i < 9; ++i)
//			if (!isfinite(vfn[i]))
//				qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
//	}
//
//	VectorXd vfxE = toEigen(vfx);
//	VectorXd vfnE = toEigen(vfn);
//
//	MatrixXd mDnDv;
//	this->compute_DnDv(vx, vv, mDnDv);
//	const MatrixXd& mDxDv = this->m_mDxDv;
//
//	this->m_vfVal.setZero();
//	this->m_vfVal += mDxDv.transpose()*vfxE;
//	this->m_vfVal += mDnDv.transpose()*vfnE;
//}


void EmbeddedRodBendElement3D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);

	this->getEmbbededPositions3D(vx, vp);

	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vp[0](i);
		x1[i] = vp[1](i);
		x2[i] = vp[2](i);
	}

	double vL0 = this->m_vL0;
	//double e0norm0 = this->m_e0norm0;
	//double e1norm0 = this->m_e1norm0;

	double kB = this->computeB();

	vector<dVector> mJ(9);
	for (int i = 0; i < 9; ++i)
		mJ[i].resize(9); // Init.

//#include "../../Maple/Code/RodAngle3DJacobian.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	this->addJacobianToEmbedding(mJ);
}

//void EmbeddedRodBendElement3D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	vector<double> x0(3);
//	vector<double> x1(3);
//	vector<double> x2(3);
//
//	this->getEmbbededPositions3D(vx, vp);
//
//	for (int i = 0; i < 3; ++i)
//	{
//		x0[i] = vp[0](i);
//		x1[i] = vp[1](i);
//		x2[i] = vp[2](i);
//	}
//
//	// Get material frame
//	Vector3d e0 = vp[1] - vp[0];
//	Vector3d e1 = vp[2] - vp[1];
//	Vector3d t0 = e0.normalized();
//	Vector3d t1 = e1.normalized();
//	Vector3d v3 = getBlock3x1(this->m_vnodeIdx0[3], vx);
//	Vector3d v4 = getBlock3x1(this->m_vnodeIdx0[4], vx);
//	Vector3d v5 = getBlock3x1(this->m_vnodeIdx0[5], vx);
//	Vector3d te0 = v4 - v3;
//	Vector3d te1 = v5 - v3;
//	Vector3d d0, d1, d2;
//	d0 = 0.5*(t0 + t1);
//	d2 = te0.cross(te1).normalized();
//	d1 = d0.cross(d2).normalized();
//	d2 = d1.cross(d0).normalized();
//
//	Vector2d k0 = this->m_k0;
//	double vL0 = this->m_vL0;
//	double kB = m_vpmat[0]->getBendingK();
//
//	vector<dVector> mJ(9);
//	for (int i = 0; i < 9; ++i)
//		mJ[i].resize(9); // Init.
//
//#include "../../Maple/Code/RodAngle3DRestJacobian.mcg"
//
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			if (!isfinite(mJ[i][j]))
//				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//
//	this->addJacobianToEmbedding(mJ);
//}

//void EmbeddedRodBendElement3D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//	Vector3d ve0 = x1 - x0;
//	Vector3d ve1 = x2 - x1;
//	double ve0norm = ve0.norm();
//	double ve1norm = ve1.norm();
//	Vector3d vt0 = ve0*(1.0 / ve0norm);
//	Vector3d vt1 = ve1*(1.0 / ve1norm);
//	Vector3d vt = 0.5*(vt0 + vt1);
//
//	Vector3d v0, v1, v2;
//	v0 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[5], vx);
//
//	Vector3d vte0 = v1 - v0;
//	Vector3d vte1 = v2 - v0;
//	Vector3d vn = vte0.cross(vte1).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = m_vpmat[0]->getBendingK();
//
//	dVector vfn(3);
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_ForceN.mcg"
//
//		for (int i = 0; i < 3; ++i)
//			if (!isfinite(vfn[i]))
//				qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
//	}
//
//	VectorXd vfnE = toEigen(vfn);
//

//
//	double mDnDv[3][9];
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_DnDv.mcg"
//
//		for (int i = 0; i < 3; ++i)
//			for (int j = 0; j < 9; ++j)
//				if (!isfinite(mDnDv[i][j]))
//					qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//	}
//
//	MatrixXd mDnDvE(3, 9);
//	for (int i = 0; i < 3; ++i)
//		for (int j = 0; j < 9; ++j)
//			mDnDvE(i, j) = mDnDv[i][j];
//
//	// --------------------------
//
//	double mD2nDv2[3][9][9];
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_D2nDv2.mcg"
//
//		for (int i = 0; i < 3; ++i)
//			for (int j = 0; j < 9; ++j)
//				for (int k = 0; k < 9; ++k)
//					if (!isfinite(mD2nDv2[i][j][k]))
//						qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//	}
//
//	vector<MatrixXd> mD2nDv2E(9);
//	for (int i = 0; i < 9; ++i)
//		mD2nDv2E[i].resize(3, 9);
//
//	for (int k = 0; k < 9; ++k)
//		for (int i = 0; i < 3; ++i)
//			for (int j = 0; j < 9; ++j)
//				mD2nDv2E[k](i, j) = mD2nDv2[i][j][k];
//
//	// --------------------------
//
//	const MatrixXd& mDxDvE = this->m_mDxDv;
//
//	MatrixXd mJ(27, 27);
//
//	MatrixXd B0 = mDxDvE.transpose()*mJxxE*mDxDvE; // 27x27
//	MatrixXd B1 = mDxDvE.transpose()*mJxnE*mDnDvE; // 27x9
//
//	MatrixXd C0 = mDnDvE.transpose()*mJnnE*mDnDvE; // 9x9
//	MatrixXd C1 = mDnDvE.transpose()*mJnxE*mDxDvE; // 9x27
//
//	MatrixXd A0(9, 9);
//	for (int k = 0; k < 9; ++k)
//		A0.col(k) = mD2nDv2E[k].transpose()*vfnE; // 9x9
//
//	mJ = B0;
//
//	for (int i = 0; i < 27; ++i)
//		for (int j = 0; j < 9; ++j)
//		{
//			mJ(i, 9+j) += B1(i, j);
//			mJ(9+j, i) += C1(j, i);
//		}
//
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//		{
//			mJ(9+i, 9+j) += A0(i,j);
//			mJ(9+i, 9+j) += C0(i,j);
//		}
//
//	for (int i = 0; i < 27; ++i)
//		for (int j = 0; j < 27; ++j)
//		{
//			if (i == j)
//				continue;
//
//			if (this->m_vidx[i] == this->m_vidx[j])
//			{
//				mJ(i, i) += mJ(i, j);
//				mJ(i, j) = 0.0; // Sum
//			}
//		}
//
//	// Store triangular lower matrix
//
//	int count = 0;
//	for (int i = 0; i < 27; ++i)
//		for (int j = 0; j <= i; ++j)
//			this->m_vJVal(count++) = mJ(i,j);
//}

//void EmbeddedRodBendElement3D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//	Vector3d n0 = (v1 - v0).cross(v2 - v0).normalized();
//	Vector3d n1 = (v4 - v3).cross(v5 - v3).normalized();
//	Vector3d n2 = (v7 - v6).cross(v8 - v6).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = m_vpmat[0]->getBendingK();
//
//	dVector vfn(9);
//
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_ForceN.mcg"
//
//		for (int i = 0; i < 9; ++i)
//			if (!isfinite(vfn[i]))
//				qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
//	}
//
//	VectorXd vfnE = toEigen(vfn);
//
//	MatrixXd mDnDv;
//	vector<MatrixXd> mD2nDv2;
//	this->compute_DnDv(vx, vv, mDnDv);
//	this->compute_D2nDv2(vx, vv, mD2nDv2);
//	const MatrixXd& mDxDv = this->m_mDxDv;
//
//	MatrixXd mJxx, mJnn, mJxn, mJnx;
//	this->compute_mJxx(vx, vv, mJxx);
//	this->compute_mJnn(vx, vv, mJnn);
//	this->compute_mJxn(vx, vv, mJxn);
//	this->compute_mJnx(vx, vv, mJnx);
//
//	MatrixXd mJ(27, 27);
//	mJ.setZero(); // Init
//
//	mJ += mDxDv.transpose()*mJxx*mDxDv;
//	mJ += mDxDv.transpose()*mJxn*mDnDv;
//	mJ += mDnDv.transpose()*mJnn*mDnDv;
//	mJ += mDnDv.transpose()*mJnx*mDxDv;
//
//	MatrixXd A(27, 27);
//	for (int k = 0; k < 27; ++k)
//		A.col(k) = mD2nDv2[k].transpose()*vfnE;
//
//	mJ += A;
//
//	for (int i = 0; i < 27; ++i)
//	{
//		for (int j = 0; j < 27; ++j)
//		{
//			if (i == j)
//				continue;
//
//			if (this->m_vidx[i] == this->m_vidx[j])
//			{
//				mJ(i, i) += mJ(i, j);
//				mJ(i, j) = 0.0; // Sum
//			}
//		}
//	}
//
//	// Store triangular lower matrix
//
//	int count = 0;
//	for (int i = 0; i < 27; ++i)
//		for (int j = 0; j <= i; ++j)
//			this->m_vJVal(count++) = mJ(i, j);
//}

//void EmbeddedRodBendElement3D::compute_DnDv(const VectorXd& vx, const VectorXd& vv, MatrixXd& mDnDvE)
//{
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//
//	double mDnDv[9][27];
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 27; ++j)
//			mDnDv[i][j] = 0.0; // Init.
//	
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_DnDv.mcg"
//	
//		for (int i = 0; i < 9; ++i)
//			for (int j = 0; j < 27; ++j)
//				if (!isfinite(mDnDv[i][j]))
//					qDebug("In %s: DnDv value is NAN\n", __FUNCDNAME__);
//	}
//
//	mDnDvE.resize(9, 27);
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 27; ++j)
//			mDnDvE(i, j) = mDnDv[i][j];
//}
//
//void EmbeddedRodBendElement3D::compute_D2nDv2(const VectorXd& vx, const VectorXd& vv, vector<MatrixXd>& mD2nDv2E)
//{
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//
//	double mD2nDv2[9][27][27];
//	for (int k = 0; k < 27; ++k)
//		for (int i = 0; i < 9; ++i)
//			for (int j = 0; j < 27; ++j)
//				mD2nDv2[i][j][k] = 0.0; // Init
//	
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_D2nDv2.mcg"
//	
//		for (int i = 0; i < 9; ++i)
//			for (int j = 0; j < 27; ++j)
//				for (int k = 0; k < 27; ++k)
//					if (!isfinite(mD2nDv2[i][j][k]))
//						qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//	}
//	
//	mD2nDv2E.resize(27);
//	for (int i = 0; i < 27; ++i)
//		mD2nDv2E[i].resize(9, 27);
//	
//	for (int k = 0; k < 27; ++k)
//		for (int i = 0; i < 9; ++i)
//			for (int j = 0; j < 27; ++j)
//				mD2nDv2E[k](i, j) = mD2nDv2[i][j][k];
//}
//
//void EmbeddedRodBendElement3D::compute_mJxx(const VectorXd& vx, const VectorXd& vv, MatrixXd& mJxxE)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//	Vector3d n0 = (v1 - v0).cross(v2 - v0).normalized();
//	Vector3d n1 = (v4 - v3).cross(v5 - v3).normalized();
//	Vector3d n2 = (v7 - v6).cross(v8 - v6).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = this->computeB();
//
//	double mJxx[9][9];
//	
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_JacobianXX.mcg"
//	
//		for (int i = 0; i < 9; ++i)
//			for (int j = 0; j < 9; ++j)
//				if (!isfinite(mJxx[i][j]))
//					qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//	}
//	
//	mJxxE.resize(9, 9);
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			mJxxE(i, j) = mJxx[i][j];
//}
//
//void EmbeddedRodBendElement3D::compute_mJnn(const VectorXd& vx, const VectorXd& vv, MatrixXd& mJnnE)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//	Vector3d n0 = (v1 - v0).cross(v2 - v0).normalized();
//	Vector3d n1 = (v4 - v3).cross(v5 - v3).normalized();
//	Vector3d n2 = (v7 - v6).cross(v8 - v6).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = this->computeB();
//
//	double mJnn[9][9];
//	
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_JacobianNN.mcg"
//	
//		for (int i = 0; i < 9; ++i)
//			for (int j = 0; j < 9; ++j)
//				if (!isfinite(mJnn[i][j]))
//					qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//	}
//	
//	mJnnE.resize(9, 9);
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			mJnnE(i, j) = mJnn[i][j];
//}
//
//void EmbeddedRodBendElement3D::compute_mJxn(const VectorXd& vx, const VectorXd& vv, MatrixXd& mJxnE)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//	Vector3d n0 = (v1 - v0).cross(v2 - v0).normalized();
//	Vector3d n1 = (v4 - v3).cross(v5 - v3).normalized();
//	Vector3d n2 = (v7 - v6).cross(v8 - v6).normalized();
//
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = this->computeB();
//
//	double mJxn[9][9];
//	
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_JacobianXN.mcg"
//	
//		for (int i = 0; i < 9; ++i)
//			for (int j = 0; j < 9; ++j)
//				if (!isfinite(mJxn[i][j]))
//					qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//	}
//	
//	mJxnE.resize(9, 9);
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			mJxnE(i, j) = mJxn[i][j];
//}
//
//void EmbeddedRodBendElement3D::compute_mJnx(const VectorXd& vx, const VectorXd& vv, MatrixXd& mJnxE)
//{
//	vector<Vector3d> vp;
//	this->getEmbbededPositions3D0(vx, vp);
//	const Vector3d& x0 = vp[0];
//	const Vector3d& x1 = vp[1];
//	const Vector3d& x2 = vp[2];
//	
//	Vector3d v0, v1, v2, v3, v4, v5, v6, v7, v8;
//	v0 = getBlock3x1(this->m_vnodeIdx[0], vx);
//	v1 = getBlock3x1(this->m_vnodeIdx[1], vx);
//	v2 = getBlock3x1(this->m_vnodeIdx[2], vx);
//	v3 = getBlock3x1(this->m_vnodeIdx[3], vx);
//	v4 = getBlock3x1(this->m_vnodeIdx[4], vx);
//	v5 = getBlock3x1(this->m_vnodeIdx[5], vx);
//	v6 = getBlock3x1(this->m_vnodeIdx[6], vx);
//	v7 = getBlock3x1(this->m_vnodeIdx[7], vx);
//	v8 = getBlock3x1(this->m_vnodeIdx[8], vx);
//	Vector3d n0 = (v1 - v0).cross(v2 - v0).normalized();
//	Vector3d n1 = (v4 - v3).cross(v5 - v3).normalized();
//	Vector3d n2 = (v7 - v6).cross(v8 - v6).normalized();
//	
//	double vL0 = this->m_vL0;
//	Vector2d K_0 = this->m_k0;
//	double kB = this->computeB();
//
//	double mJnx[9][9];
//	
//	{
//#include "../../Maple/Code/EmbeddedRodAngle3DSplit_JacobianNX.mcg"
//	
//		for (int i = 0; i < 9; ++i)
//			for (int j = 0; j < 9; ++j)
//				if (!isfinite(mJnx[i][j]))
//					qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//	}
//	
//	mJnxE.resize(9, 9);
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			mJnxE(i, j) = mJnx[i][j];
//}

Real EmbeddedRodBendElement3D::computeB() const
{
	MatrixXd B(2, 2);

	B.setZero();
	double Y = this->m_vpmat[0]->getYoung();
	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double A = M_PI * w * h;
	B(0, 0) = w * w;
	B(1, 1) = h * h;
	B = (0.25 * Y * A) * B;

	return B.trace();
}