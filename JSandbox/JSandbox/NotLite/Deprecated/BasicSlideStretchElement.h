/*=====================================================================================*/
/*!
\file		BasicSlideStretchElement.h
\author		jesusprod
\brief		Implementation of BasicSlideStretchElement.h.
*/
/*=====================================================================================*/

#ifndef BASIC_SLIDE_STRETCH_ELEMENT_H
#define BASIC_SLIDE_STRETCH_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/DERSlideElement.h>

class BasicSlideStretchElement : public DERSlideElement
{
public:
	BasicSlideStretchElement();
	~BasicSlideStretchElement();

	virtual Real computeEnergy(const Vector3d& x0, const Vector3d& x1, Real t0, Real t1);
	virtual void computeForce(const Vector3d& x0, const Vector3d& x1, Real t0, Real t1, VectorXd& vf);
	virtual void computeJacobian(const Vector3d& x0, const Vector3d& x1, Real t0, Real t1, MatrixXd& mJ);

	virtual Real getIntegrationVolume() const { return -1; }

	virtual Real computeK() const;
};

#endif