/*=====================================================================================*/
/*! 
\file		VolumeModel.h
\author		jesusprod
\brief		Generic 3D FEM model. It provides the necessary functions to create a FEM
			discretization using different elements. The current implementation
			is restricted to linear tetrahedral and cubic hexahedral elements.
 */
/*=====================================================================================*/

#ifndef FEM_MODEL_H
#define FEM_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidModel.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/FEMElement.h>

class JSANDBOX_EXPORT VolumeModel : public SolidModel
{
public:

	// Discretization elements

	enum Element
	{
		Tet1,		// Linear tetrahedron
		Hex3		// Trilinear hexahedron
	};

	// Deformation models

	enum Model
	{
		FEMLinear = 0,			// Linear FEM
		FEMLinearCorot = 1,		// Linear Co-Rotational FEM
		FEMMooneyRivlin = 2,	// Mooney-Rivlin FEM
		FEMNeoHookean = 3,		// Neo-Hookean FEM
		FEMOgden = 4,			// Ogden FEM
		FEMStVK = 5				// St-VK FEM
	};

public:
	VolumeModel() { }

	virtual ~VolumeModel() {}

	virtual void configureDeformationModel(VolumeModel::Model DM);
	virtual void configureDiscretization(const dVector& vp, const iVector& vi, VolumeModel::Element DE);

	virtual void visualizeEnergy(dVector& vcol) { };
	virtual void visualizeForce(dVector& vcol) { };

protected:
	virtual void initializeElements();
	virtual void initializeTetDiscretization();
	virtual void initializeHexDiscretization();



protected:
	Model m_DM;
	Element m_DE;
	dVector m_vx;
	iVector m_vind;

};

#endif