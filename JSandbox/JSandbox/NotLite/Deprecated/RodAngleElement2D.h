/*=====================================================================================*/
/*!
\file		RodAngleElement2D.h
\author		jesusprod
\brief		Rod angle preserving energy in 2D. It uses SolidMaterial bending K.
*/
/*=====================================================================================*/

#ifndef ROD_ANGLE_ELEMENT_2D_H
#define ROD_ANGLE_ELEMENT_2D_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>

class RodAngleElement2D : public NodalSolidElement
{
public:
	RodAngleElement2D();
	~RodAngleElement2D();

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getIntegrationVolume() const { return this->m_vL0; }

	virtual Real getRestAngle() const { return this->m_phi0; }
	virtual void setRestAngle(Real phi0) { this->m_phi0 = phi0; }

protected:
	Real m_phi0; // Rest angle
	Real m_vL0; // Vertex lenght
};

#endif