/*=====================================================================================*/
/*!
\file		BasicSlideStretchElement.cpp
\author		jesusprod
\brief		Implementation of BasicSlideStretchElement.h
*/
/*=====================================================================================*/

#include <JSandbox/BasicSlideStretchElement.h>

BasicSlideStretchElement::BasicSlideStretchElement() : DERSlideElement(1, 1)
{
	// Nothing to do here...
}

BasicSlideStretchElement::~BasicSlideStretchElement()
{
	// Nothing to do here...
}

Real BasicSlideStretchElement::computeEnergy(const Vector3d& x1, const Vector3d& x2, Real s1, Real s2)
{
	Real ks = this->computeK();

//#include "../../Maple/Code/DERSlideStretchEnergy.mcg"
//
//	return t22;
	
	return -1.0;
}

void BasicSlideStretchElement::computeForce(const Vector3d& x1, const Vector3d& x2, Real s1, Real s2, VectorXd& vfOut)
{
	Real ks = this->computeK();

	Real vf[8];

//#include "../../Maple/Code/DERSlideStretchForce.mcg"

	vfOut.resize(8);
	for (int i = 0; i < 8; ++i)
		vfOut(i) = vf[i]; // Copy
}

void BasicSlideStretchElement::computeJacobian(const Vector3d& x1, const Vector3d& x2, Real s1, Real s2, MatrixXd& mJOut)
{

	Real ks = this->computeK();

	Real mJ[8][8];

//#include "../../Maple/Code/DERSlideStretchJacobian.mcg"

	for (int i = 0; i < 8; ++i)
		for (int j = 0; j < 8; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix
	
	mJOut.resize(8, 8);
	for (int i = 0; i < 8; ++i)
		for (int j = 0; j < 8; ++j)
			mJOut(i, j) = mJ[i][j];
}

Real BasicSlideStretchElement::computeK() const
{
	Real rh = this->m_vpmat[0]->getHRadius();
	Real rw = this->m_vpmat[0]->getWRadius();
	Real E = this->m_vpmat[0]->getYoung();
	Real A = rh*rw*M_PI;
	return E*A;
}
