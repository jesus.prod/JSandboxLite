/*=====================================================================================*/
/*!
\file		RodEdgeElement3D.cpp
\author		jesusprod
\brief		Implementation of RodEdgeElement3D.h
*/
/*=====================================================================================*/

#include <JSandbox/RodEdgeElement3D.h>

RodEdgeElement3D::RodEdgeElement3D(int dim0) : NodalSolidElement(2, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);
}

RodEdgeElement3D::~RodEdgeElement3D()
{
	// Nothing to do here
}

void RodEdgeElement3D::updateRest(const VectorXd& vX)
{
	if (this->m_numDim0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
		this->m_L0 = (x1 - x0).norm();
	}
	else if (this->m_numDim0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
		this->m_L0 = (x1 - x0).norm();
	}
	else assert(false);

	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double d = this->m_vpmat[0]->getDensity();
	for (int i = 0; i < this->m_nrawDOF; ++i)
		this->m_vmass[i] = 0.5*d*m_L0*w*h*M_PI;
}

void RodEdgeElement3D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3 + i]);
	}

	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = computeB();

//#include "../../Maple/Code/RodEdge3DEnergy.mcg"

//	this->m_energy = t21;
}

void RodEdgeElement3D::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3 + i]);
	}

	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = computeB();

	double vf6[6];

//#include "../../Maple/Code/RodEdge3DForce.mcg"

	for (int i = 0; i < 6; ++i)
		if (!isfinite(vf6[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 6; ++i)
		this->m_vfVal(i) = vf6[i];
}

void RodEdgeElement3D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3 + i]);
	}

	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = computeB();

	double mJ[6][6];

//#include "../../Maple/Code/RodEdge3DJacobian.mcg"

	for (int i = 0; i < 6; ++i)
		for (int j = 0; j < 6; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 6; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ[i][j];
}

double RodEdgeElement3D::computeB() const
{
	double Y = this->m_vpmat[0]->getYoung();
	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double A = M_PI * w * h;
	return A*Y;
}