/*=====================================================================================*/
/*!
\file		DERSlideElement.h
\author		jesusprod
\brief		Implementation of DERSlideElement.h.
*/
/*=====================================================================================*/

#ifndef DER_SLIDE_ELEMENT_H
#define DER_SLIDE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/SolidElement.h>

class DERSlideElement : public SolidElement
{
public:
	DERSlideElement(int nd, int nd0) : SolidElement(nd, nd0) { }
	~DERSlideElement() { }

	const iVector& getVertexIndices() const { return this->m_vvinds; }
	void setVertexIndices(const iVector& vi) { this->m_vvinds = vi; }

	const iVector& getEdgeIndices() const { return this->m_veinds; }
	void setEdgeIndices(const iVector& vi) { this->m_veinds = vi; }

	// Usual interface is not valid here

	virtual void updateRest(const VectorXd& vX) { assert(false); }
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv) { assert(false); }
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv) { assert(false); }
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv) { assert(false); }

protected:

	iVector m_vvinds;
	iVector m_veinds;
};

#endif