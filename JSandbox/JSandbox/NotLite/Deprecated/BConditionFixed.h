/*=====================================================================================*/
/*!
\file		BConditionFixed.h
\author		jesusprod
\brief		Fixed values boundary condition.
*/
/*=====================================================================================*/

#ifndef BCONDITION_FIXED_H
#define BCONDITION_FIXED_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/BCondition.h>
#include <JSandbox/SolidModel.h>

class JSANDBOX_EXPORT BConditionFixed : public BCondition
{

public:

	BConditionFixed(SolidModel* pModel);

	virtual ~BConditionFixed();

	// <BCondition>

	virtual void constrain(VectorXd& vf) const;
	virtual void constrain(SolidModel* pModel) const;
	virtual void constrain(tVector& vJ) const;

	// </BCondition>

	virtual int getNumber() const { return this->m_vidx.size(); }

	virtual VectorXi getIndices() const { return this->m_vidx; }
	virtual VectorXd getValues() const { return this->m_vval; }
	virtual void setIndices(const VectorXi& vi) { this->m_vidx = vi; }
	virtual void setValues(const VectorXd& vv) { this->m_vval = vv; }

	virtual void initializeIndividual(const iVector& vidx);
	virtual void initializeNodal(int n, const iVector& vidx);

protected:

	VectorXi m_vidx;
	VectorXd m_vval;

};

#endif