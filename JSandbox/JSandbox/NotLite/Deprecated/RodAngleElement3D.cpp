/*=====================================================================================*/
/*!
\file		RodAngleElement3D.cpp
\author		jesusprod
\brief		Implementation of RodAngleElement3D.h
*/
/*=====================================================================================*/

#include <JSandbox/RodAngleElement3D.h>

RodAngleElement3D::RodAngleElement3D(int dim0) : NodalSolidElement(3, 3, dim0)
{
	// Nothign to do here
}

RodAngleElement3D::~RodAngleElement3D()
{
	// Nothing to do here
}

void RodAngleElement3D::updateRest(const VectorXd& vX)
{
	if (this->m_numDim0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
		Vector3d x2 = getBlock3x1(this->m_vnodeIdx0[2], vX);
		Vector3d e0 = x1 - x0;
		Vector3d e1 = x2 - x1;
		this->m_e0norm0 = e0.norm();
		this->m_e1norm0 = e1.norm();
		this->m_vL0 = 0.5*(m_e0norm0 + m_e1norm0);
	}
	else if (this->m_numDim0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
		Vector2d x2 = getBlock2x1(this->m_vnodeIdx0[2], vX);
		Vector2d e0 = x1 - x0;
		Vector2d e1 = x2 - x1;
		this->m_e0norm0 = e0.norm();
		this->m_e1norm0 = e1.norm();
		this->m_vL0 = 0.5*(m_e0norm0 + m_e1norm0);
	}
	else assert(false);
}

void RodAngleElement3D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidx[i]);
		x1[i] = vx(this->m_vidx[3 + i]);
		x2[i] = vx(this->m_vidx[6 + i]);
	}

	double vL0 = this->m_vL0;
	//double e0norm0 = this->m_e0norm0;
	//double e1norm0 = this->m_e1norm0;

	double kB = m_vpmat[0]->getBendingK();

//#include "../../Maple/Code/RodAngle3DEnergy.mcg"

//	this->m_energy = t78;
}

void RodAngleElement3D::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidx[i]);
		x1[i] = vx(this->m_vidx[3 + i]);
		x2[i] = vx(this->m_vidx[6 + i]);
	}

	double vL0 = this->m_vL0;
	//double e0norm0 = this->m_e0norm0;
	//double e1norm0 = this->m_e1norm0;

	double kB = m_vpmat[0]->getBendingK();

	double vf9[9];
	    
//#include "../../Maple/Code/RodAngle3DForce.mcg"

	for (int i = 0; i < 9; ++i)
		if (!isfinite(vf9[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 9; ++i)
		this->m_vfVal(i) = vf9[i];
}

void RodAngleElement3D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i) 
	{
		x0[i] = vx(this->m_vidx[i]);
		x1[i] = vx(this->m_vidx[3 + i]);
		x2[i] = vx(this->m_vidx[6 + i]);
	}

	double vL0 = this->m_vL0;
	//double e0norm0 = this->m_e0norm0;
	//double e1norm0 = this->m_e1norm0;

	double kB = m_vpmat[0]->getBendingK();

	double mJ[9][9];

//#include "../../Maple/Code/RodAngle3DJacobian.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ[i][j];
}