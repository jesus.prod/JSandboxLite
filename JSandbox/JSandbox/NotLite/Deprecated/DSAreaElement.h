/*=====================================================================================*/
/*! 
\file		DSAreaElement.h
\author		jesusprod,bthomasz
\brief		Basic implementation of a discrete-shell area element. Adapted from bthomasz
			implementation for Phys3D DeformablesSurface plugin. This provides a simple
			non-linear energy density for area preserving triangles. It relies on Maple
			auto-differentiation tool for force and Jacobian expressions. It uses 
			SolidMaterial area K as elasticity coefficient.
*/
/*=====================================================================================*/

#ifndef DS_AREA_ELEMENT_H
#define DS_AREA_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>

class DSAreaElement : public NodalSolidElement
{
public:
	DSAreaElement(int dim0);
	~DSAreaElement();

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getIntegrationVolume() const { return this->m_A0; }
	
	virtual Real getRestArea() const { return this->m_A0; }

protected:
	Real m_A0;
};

#endif