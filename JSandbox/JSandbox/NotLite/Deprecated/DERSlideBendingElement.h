/*=====================================================================================*/
/*!
\file		DERSlideBendingElement.h
\author		jesusprod
\brief		Declaration of DERSlideBendingElement class
*/
/*=====================================================================================*/

#ifndef DER_SLIDE_BENDING_ELEMENT_H
#define DER_SLIDE_BENDING_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include "MathUtils.h"
#include "DERSlideElement.h"

class DERSlideBendingElement : public DERSlideElement
{

public:
	DERSlideBendingElement();
	virtual ~DERSlideBendingElement();

	virtual void updateEnergy(const dVector& vx, const dVector& va, const vector<Frame>& vF);
	virtual void updateForce(const dVector& vx, const dVector& va, const vector<Frame>& vF);
	virtual void updateJacobian(const dVector& vx, const dVector& va, const vector<Frame>& vF);
	virtual void updateRest(const dVector& vx, const dVector& va, const vector<Frame>& vF);

	virtual Real computeEnergy(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1);
	virtual void computeForce(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, VectorXd& vf);
	virtual void computeJacobian(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& mJ);
	virtual void updateRest(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1);

	virtual MatrixXd computeB() const;
	virtual Vector3d computeKB(const Vector3d& e, const Vector3d& f) const;
	virtual Real computeVertexLength(const Vector3d& e, const Vector3d& f) const;
	virtual VectorXd computeKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1) const;
	virtual void computeGradKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& gK) const;
	virtual void computeHessKappa(const Vector3d& e, const Vector3d& f, const Frame& f0, const Frame& f1, MatrixXd& gK1, MatrixXd& gK2) const;

	virtual Real getIntegrationVolume() const { return this->m_l0; }

	Real getRestVertexLength() const { return this->m_l0; }
	void setRestVertexLength(Real l0) { this->m_l0 = l0; }

	Vector2d getRestKappa() const { return this->m_K0; }
	void setRestKappa(Vector2d K0) { this->m_K0 = K0; }

	Real getConstant() const;

protected:

	MatrixXd m_DeDv; // Transmission
	VectorXd m_K0; // Rest curvature
	Real m_l0; // Rest vertex length 

};

#endif
