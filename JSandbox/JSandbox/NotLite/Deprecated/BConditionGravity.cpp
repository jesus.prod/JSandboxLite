/*=====================================================================================*/
/*!
\file		BConditionGravity.cpp
\author		jesusprod
\brief		Implementation of BConditionGravity.h
*/
/*=====================================================================================*/

#include <JSandbox/BConditionGravity.h>

BConditionGravity::BConditionGravity(SolidModel* pModel) : BCondition(pModel)
{
	// Nothing to do here...
}

BConditionGravity::~BConditionGravity()
{
	// Nothing to do here...
}

Real BConditionGravity::getEnergy(const VectorXd& vx, const VectorXd& vv) const
{
	int N = this->m_pModel->getRawDOFNumber();

	assert((int)vx.size() == N);
	assert((int)vv.size() == N);

	const bVector& gs = this->m_pModel->getGravityStencil();
	const VectorXd& vm = this->m_pModel->getLumpedMassVector();

	double energy = 0;

	for (int i = 0; i < N; ++i)
	{
		if (!gs[i])
			continue;
		energy -= this->m_vg(i)*vm(i)*vx(i);
	}

	return energy;
}

void BConditionGravity::addForce(const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const
{
	int N = this->m_pModel->getRawDOFNumber();

	assert((int)vx.size() == N);
	assert((int)vv.size() == N);
	assert((int)vf.size() == N);

	const bVector& gs = this->m_pModel->getGravityStencil();
	const VectorXd& vm = this->m_pModel->getLumpedMassVector();

	for (int i = 0; i < N; ++i)
	{
		if (!gs[i])
			continue;
		vf[i] += this->m_vg(i)*vm(i);
	}
}

void BConditionGravity::addJacobian(const VectorXd& vx, const VectorXd& vv, tVector& vJ) const
{
	// Constant force, no Jacobian
}


void BConditionGravity::initializeNodal(int n, const VectorXd& vg)
{
	assert((int)vg.size() == n);
	
	int N = this->m_pModel->getRawDOFNumber();

	this->m_vg.resize(N);
	for (int i = 0; i < N; ++i)
		this->m_vg(i) = vg(i%n);

	this->m_maxStep = 1;
	this->m_curStep = 1;
}