/*=====================================================================================*/
/*!
\file			SurfaceRemesherARC.cpp
\author		jesusprod
\brief		Implementation of SurfaceRemesherARC.h
*/
/*=====================================================================================*/

#include <JSandbox/SurfaceRemesherARC.h>

SurfaceRemesherARC::SurfaceRemesherARC()
{
	// Nothing to do here
}

SurfaceRemesherARC::~SurfaceRemesherARC()
{
	this->freeMemory();
}

const RemeshOP& SurfaceRemesherARC::remesh(TriMesh* pMesh, BVHTree* pTree)
{
	assert(pMesh != NULL); // Invalid call, error

	// Create remesh operation

	this->m_remeshOP = this->createOperation();

	// TODO: Initialize mesh

	bool error = false;

	if (error)
	{
		this->m_remeshOP.m_OK = false;
		this->freeMemory(); // Invalid
	}
	else
	{
		// Prepare output
	}

	return this->m_remeshOP;
}

bool SurfaceRemesherARC::computeSizing()
{
	int nv = (int) m_pMesh->n_vertices();
	int nf = (int) m_pMesh->n_faces();

	// Compute sizing at faces

	vector<SurfaceSizing> vFacSizing(nf);
	for (int i = 0; i < nf; ++i)
		vFacSizing[i].setZero();

	for (int i = 0; i < (int) this->m_vpElements.size(); ++i)
		this->m_vpElements[i]->addSizingTensor(this->m_remeshOP, vFacSizing);

	// Average sizing at vertices

	this->m_vVerSizing.resize(nv);
	for (TriMesh::VertexIter v_it = m_pMesh->vertices_begin(); v_it != m_pMesh->vertices_end(); v_it++)
	{
		TriMesh::VertexHandle vh = *v_it;
		double vertexArea = 0;
		int vidx = vh.idx();

		for (TriMesh::VertexFaceIter vf_it = m_pMesh->vf_begin(vh); vf_it != m_pMesh->vf_end(vh); vf_it++)
		{
			double faceArea = m_pMesh->getFaceArea(*vf_it, VPropSim::POSE);
			this->m_vVerSizing[vidx] += faceArea*vFacSizing[(*vf_it).idx()];
			vertexArea += vertexArea;
		}
		
		this->m_vVerSizing[vidx] /= vertexArea; // Average
	}

	return true;
}

RemeshOP SurfaceRemesherARC::splitEdges(vector<MeshTraits::ID>& vacset)
{
	int ne = (int) m_pMesh->n_edges();

	// Loop for a maximum times equal to the
	// number of edges in the mesh, or until
	// no edges have been splitted.

	RemeshOP OP = this->createOperation();

	bool splitted = false;

	for (int i = 0; i < ne; ++i)
	{
		RemeshOP splitRoundOP = this->createOperation();

		bool splittedRound = false;

		vector<MeshTraits::ID> veid; // To split
		if (findEdgesToSplit(vacset, veid))
		{
			for (int e = 0; e < (int) veid.size(); ++e)
			{
				RemeshOP splitOP = this->splitEdge(veid[e]);
				if (splitOP.m_OK)
				{
					splitRoundOP = this->appendOperations(splitRoundOP, splitOP);
					splittedRound = true;
				}
			}
		}

		splitted |= splittedRound;

		if (!splittedRound)
			break; // No splits
		else
		{
			OP = this->appendOperations(OP, splitRoundOP);
			this->updateActiveSet(vacset, splitRoundOP);

			// Perform flip after split to added edges to fix up any mesh problem
			RemeshOP splitRoundFlipOP = this->flipEdges(splitRoundOP.m_vAddEdge);
			
			if (splitRoundFlipOP.m_OK)
			{
				OP = this->appendOperations(OP, splitRoundFlipOP);
				this->updateActiveSet(vacset, splitRoundFlipOP);
			}
		}
	}

	return OP;
}

bool SurfaceRemesherARC::findEdgesToSplit(vector<MeshTraits::ID>& vacset, vector<MeshTraits::ID>& vsplit) const
{
	vsplit.clear();

	vector<pair<double, MeshTraits::ID>> veidScored;

	for (int i = 0; i < (int)vacset.size(); ++i)
	{
		double s = this->getSplitMetric(vacset[i]);
		veidScored.push_back(make_pair(s, vacset[i]));
	}

	struct DescSort {
		inline bool operator()(const std::pair<double, MeshTraits::ID>& left, 
							   const std::pair<double, MeshTraits::ID>& right) {
			return left.first > right.first;
		}
	} DescSort;

	std::sort(veidScored.begin(), veidScored.end(), DescSort);

	int ne = (int) veidScored.size();

	vsplit.resize(ne);
	for (int i = 0; i < ne; ++i)
		vsplit[i] = veidScored[i].second;

	return !vsplit.empty();
}

double SurfaceRemesherARC::getSplitMetric(const MeshTraits::ID& eid) const
{
	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	// Get vectors

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getEdgeVertices(eh, vvh);

	vector<Vector3d> vv(2);
	vv[0] = m_pMesh->getPoint(vvh[0], VPropSim::POSE);
	vv[1] = m_pMesh->getPoint(vvh[1], VPropSim::POSE);
	Vector3d uij = vv[0] - vv[1];

	// Get sizing average

	Matrix3d M = (this->m_vVerSizing[vvh[0].idx()] + this->m_vVerSizing[vvh[1].idx()])*0.5;

	return (uij.transpose() * M * uij);
}

RemeshOP SurfaceRemesherARC::splitEdge(const MeshTraits::ID& eid)
{
	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	RemeshOP out = this->createOperation();

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getHingeVertices(eh, vvh);

	vector<TriMesh::FaceHandle> vfh;
	m_pMesh->getEdgeFaces(eh, vfh);

	// Remove edge and faces ikj, jli

	m_pMesh->delete_edge(eh, false);
	out.m_vDelEdge.push_back(eid);

	for (int i = 0; i < (int)vfh.size(); ++i)
		out.m_vDelFace.push_back(m_pMesh->getFaceID(vfh[i]));

	// Introduce new vertex in such way that the change in the
	// curvature is minimized at properties for all the spaces

	Vector3d newXE = this->computeMidpoint(eid, VPropSim::POSE);
	Vector3d newXP = this->computeMidpoint(eid, VPropSim::POSP);
	Vector3d newX0 = this->computeMidpoint(eid, VPropSim::POS0);

	// Velocity is interpolated linearly

	Vector3d newVE = (m_pMesh->getPoint(vvh[0], VPropSim::VELE) +
					  m_pMesh->getPoint(vvh[1], VPropSim::VELE))*0.5;

	TriMesh::VertexHandle vh = m_pMesh->add_vertex(MeshTraits::Point(newX0(0), newX0(1), newX0(2)));
	m_pMesh->setPoint(vh, newXE, VPropSim::POSE);
	m_pMesh->setPoint(vh, newXP, VPropSim::POSP);
	m_pMesh->setPoint(vh, newX0, VPropSim::POS0);
	m_pMesh->setPoint(vh, newVE, VPropSim::VELE);
	MeshTraits::ID vid = TriMesh::CreateIdentifier();
	m_pMesh->setVertID(vh, vid);

	// Update sizing to the average

	assert(vh.idx() == (int) this->m_vVerSizing.size());

	SurfaceSizing avgM = (this->m_vVerSizing[vvh[0].idx()] +
						  this->m_vVerSizing[vvh[1].idx()])*0.5;
	this->m_vVerSizing.push_back(avgM);

	// Introduce new faces and edges

	TriMesh::FaceHandle fh0 = m_pMesh->add_face(vh, vvh[0], vvh[2]);
	TriMesh::FaceHandle fh1 = m_pMesh->add_face(vh, vvh[2], vvh[1]);
	MeshTraits::ID fid0 = TriMesh::CreateIdentifier();
	MeshTraits::ID fid1 = TriMesh::CreateIdentifier();
	m_pMesh->setFaceID(fh0, fid0);
	m_pMesh->setFaceID(fh1, fid1);
	out.m_vAddFace.push_back(fid0);
	out.m_vAddFace.push_back(fid1);
	if ((int) vvh.size() == 4)
	{
		TriMesh::FaceHandle fh2 = m_pMesh->add_face(vh, vvh[1], vvh[3]);
		TriMesh::FaceHandle fh3 = m_pMesh->add_face(vh, vvh[3], vvh[0]);
		MeshTraits::ID fid2 = TriMesh::CreateIdentifier();
		MeshTraits::ID fid3 = TriMesh::CreateIdentifier();
		m_pMesh->setFaceID(fh2, fid2);
		m_pMesh->setFaceID(fh3, fid3);
		out.m_vAddFace.push_back(fid2);
		out.m_vAddFace.push_back(fid3);
	}

	for (TriMesh::VertexEdgeIter ve_it = m_pMesh->ve_begin(vh); ve_it != m_pMesh->ve_end(vh); ve_it++)
	{
		MeshTraits::ID eid = TriMesh::CreateIdentifier();
		m_pMesh->setEdgeID(*ve_it, eid);
		out.m_vAddEdge.push_back(eid);
	}

	// Operation OK
	out.m_OK = true;

	return out;
}

RemeshOP SurfaceRemesherARC::flipEdges(vector<MeshTraits::ID>& vacset)
{
	int ne = (int) m_pMesh->n_edges();

	// Loop for a maximum times equal to the
	// number of edges in the mesh, or until
	// no edges have been flipped.

	// Each time some of the edges might be filtered 
	// out if they are not independent from each other.

	RemeshOP OP = this->createOperation();

	bool flipped = false;

	for (int i = 0; i < ne; ++i)
	{
		RemeshOP flipRoundOP = this->createOperation();

		bool flippedRound = false;

		vector<MeshTraits::ID> veid;
		if (findEdgesToFlip(vacset, veid))
		{
			vector<MeshTraits::ID> veidFiltered;
			this->filterIndependent(veid, veidFiltered);
			for (int e = 0; e < (int)veidFiltered.size(); ++e)
			{
				RemeshOP flipOP = this->flipEdge(veidFiltered[e]);
				if (flipOP.m_OK)
				{
					flipRoundOP = this->appendOperations(flipRoundOP, flipOP);
					flippedRound = true;
				}
			}
		}

		flipped |= flippedRound;

		if (!flippedRound)
			break; // No flips
		else
		{
			OP = this->appendOperations(OP, flipRoundOP);
			this->updateActiveSet(vacset, flipRoundOP);
		}
	}

	return OP;
}

bool SurfaceRemesherARC::findEdgesToFlip(vector<MeshTraits::ID>& vacset, vector<MeshTraits::ID>& vflip) const
{
	vflip.clear();
	
	for (int i = 0; i < (int) vacset.size(); ++i)
		if (this->getFlipMetric(vacset[i]) < 0.0)
			vflip.push_back(vacset[i]);

	return !vflip.empty();
}

double SurfaceRemesherARC::getFlipMetric(const  MeshTraits::ID& eid) const
{
	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	if (m_pMesh->is_boundary(eh))
		return 1.0; // No flip

	// Get vectors

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getHingeVertices(eh, vvh);

	vector<Vector3d> vv(4);
	vv[0] = m_pMesh->getPoint(vvh[0], VPropSim::POSE);
	vv[1] = m_pMesh->getPoint(vvh[1], VPropSim::POSE);
	vv[2] = m_pMesh->getPoint(vvh[2], VPropSim::POSE);
	vv[3] = m_pMesh->getPoint(vvh[3], VPropSim::POSE);
	Vector3d ujk = vv[1] - vv[2];
	Vector3d uik = vv[0] - vv[2];
	Vector3d ujl = vv[1] - vv[3];
	Vector3d uil = vv[0] - vv[3];

	// Get ref normals

	Vector3d nikj = (vv[2] - vv[0]).cross(vv[1] - vv[0]).normalized();
	Vector3d nijl = (vv[1] - vv[0]).cross(vv[3] - vv[0]).normalized();

	assert(nikj.dot(nijl) > 0.0); // Both normals should be outwards

	// Get average sizing

	Matrix3d M;
	M.setZero();
	M += this->m_vVerSizing[vvh[0].idx()];
	M += this->m_vVerSizing[vvh[1].idx()];
	M += this->m_vVerSizing[vvh[2].idx()];
	M += this->m_vVerSizing[vvh[3].idx()];
	M *= 0.25;

	// Test

	Vector3d cjkik = ujk.cross(uik);
	Vector3d ciljl = uil.cross(ujl);
	double sigArea0 = cjkik.dot(nikj)*cjkik.norm();
	double sigArea1 = ciljl.dot(nikj)*ciljl.norm();
	double val0 = (uil.transpose() * M * ujl);
	double val1 = (ujk.transpose() * M * uik);

	return sigArea0 * val0 + sigArea1 * val1;
}

RemeshOP SurfaceRemesherARC::flipEdge(const MeshTraits::ID& eid)
{
	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	assert(!m_pMesh->is_boundary(eh));

	RemeshOP out = this->createOperation();

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getHingeVertices(eh, vvh);

	vector<TriMesh::FaceHandle> vfh;
	m_pMesh->getEdgeFaces(eh, vfh);

	// Remove edge and faces ikj, jli

	m_pMesh->delete_edge(eh, false);
	out.m_vDelEdge.push_back(eid);

	for (int i = 0; i < (int)vfh.size(); ++i)
		out.m_vDelFace.push_back(m_pMesh->getFaceID(vfh[i]));

	// Introduce new faces and edges

	TriMesh::FaceHandle fh0 = m_pMesh->add_face(vvh[0], vvh[2], vvh[3]);
	TriMesh::FaceHandle fh1 = m_pMesh->add_face(vvh[2], vvh[3], vvh[2]);
	MeshTraits::ID fid0 = TriMesh::CreateIdentifier();
	MeshTraits::ID fid1 = TriMesh::CreateIdentifier();
	m_pMesh->setFaceID(fh0, fid0);
	m_pMesh->setFaceID(fh1, fid1);
	out.m_vAddFace.push_back(fid0);
	out.m_vAddFace.push_back(fid1);
	for (TriMesh::FaceEdgeIter fe_it = m_pMesh->fe_begin(fh0); fe_it != m_pMesh->fe_end(fh0); fe_it++)
	{
		// Look for the new added edges
		TriMesh::EdgeHandle& feh = *fe_it;

		vector<TriMesh::VertexHandle> vevh;
		m_pMesh->getEdgeVertices(feh, vevh);
		if ((vevh[0] == vvh[2] && vevh[1] == vvh[3]) ||
			(vevh[0] == vvh[2] && vevh[1] == vvh[3]))
		{
			MeshTraits::ID eid = TriMesh::CreateIdentifier();
			m_pMesh->setEdgeID(feh, eid);
			out.m_vAddEdge.push_back(eid);
			break;
		}
	}

	// Operation OK
	out.m_OK = true;

	return out;
}

Vector3d SurfaceRemesherARC::computeMidpoint(const MeshTraits::ID& eid, const string& pname) const
{
	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getHingeVertices(eh, vvh);
	vector<Vector3d> vp;
	vector<Vector3d> vn;
	for (int i = 0; i < (int)vvh.size(); ++i)
	{
		vp.push_back(m_pMesh->getPoint(vvh[i], pname));
		vn.push_back(m_pMesh->getNormal(vvh[i], pname));
	}

	Vector3d x = (vp[1] + vp[0]) * 0.5;
	Matrix3d Ht; Ht.setZero();
	Vector3d gt; gt.setZero();

	for (int i = 0; i < (int)vp.size(); ++i)
	{
		dVector xi;
		dVector ni;
		for (int ii = 0; ii < 3; ++ii)
		{
			xi[ii] += vp[i](ii);
			ni[ii] += vn[i](ii);
		}

		double g[3];
		double H[3][3];
		{
//#include "Maple/Code/CurvatureMinGradient.mcg"
		}
		{
//#include "Maple/Code/CurvatureMinHessian.mcg"
		}

		for (int ii = 0; ii < 3; ++ii)
		{
			gt(ii) += g[ii];
			for (int jj = 0; jj < 3; ++jj)
				Ht(ii, jj) = H[ii][jj];
		}
	}

	// Add regularizer

	Matrix3d D;
	D.setIdentity();
	Ht += D * 0.0001;

	return -Ht.inverse() * gt;
}

void SurfaceRemesherARC::addSizingElement(SRSizingElement* pEle)
{
	assert(pEle != NULL); // Invalid
	this->m_vpElements.push_back(pEle);
}