/*=====================================================================================*/
/*! 
\file			CSTElement.cpp
\author		jesusprod
\brief		Implementation of CSTElement.h
*/
/*=====================================================================================*/

#include <JSandbox/CSTElement.h>

CSTElement::CSTElement(int d0) : NodalSolidElement(3, 3, d0)
{
	this->m_t0 = 1.0;
	this->m_A0 = 0.0;
}

CSTElement::~CSTElement() 
{
	// Nothing to do here
}

void CSTElement::updateRest(const VectorXd& vX)
{
	if (this->m_numDim0 == 3) // 2D rest shape
	{
		vector<Vector3d> vp(3);
		for (int i = 0; i < 3; ++i)
		{
			vp[0](i) = vX(this->m_vidx0[i]);
			vp[1](i) = vX(this->m_vidx0[3 + i]);
			vp[2](i) = vX(this->m_vidx0[6 + i]);
		}
		this->m_A0 = this->compute_Area_3D(vp);
		this->compute_DNDx_3D(vp, this->m_dNdx);
	}
	else if (this->m_numDim0 == 2) // 3D rest shape
	{
		vector<Vector2d> vp(3);
		for (int i = 0; i < 2; ++i)
		{
			vp[0](i) = vX(this->m_vidx0[i]);
			vp[1](i) = vX(this->m_vidx0[2 + i]);
			vp[2](i) = vX(this->m_vidx0[4 + i]);
		}
		this->m_A0 = this->compute_Area_2D(vp);
		this->compute_DNDx_2D(vp, this->m_dNdx);
	}
	else
	{
		assert(false); // Should not be here!
	}
	
	// Add lumped mass at the degrees of freedom
	double t = this->m_vpmat[0]->getThickness();
	double d = this->m_vpmat[0]->getDensity();
	double nodeMass = (this->m_A0*t*d) / 3.0;
	for (int i = 0; i < this->m_nrawDOF; ++i) 
		this->m_vmass[i] = nodeMass;
}

void CSTElement::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3 + i]);
		x3[i] = vx(this->m_vidx[6 + i]);
	}

	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double G = m_vpmat[0]->getShear();

	double& A0 = this->m_A0;
	double& pS = this->m_preStrain;
	const DNDx& dNdx = this->m_dNdx;

//#include "../../Maple/Code/CSTStVKOrthoEnergy.mcg"

	//this->m_energy = t55;
}

void CSTElement::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3 + i]);
		x3[i] = vx(this->m_vidx[6 + i]);
	}

	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double G = m_vpmat[0]->getShear();

	double& A0 = this->m_A0;
	double& pS = this->m_preStrain;
	const DNDx& dNdx = this->m_dNdx;

	double vf[9];

//#include "../../Maple/Code/CSTStVKOrthoForce.mcg"

	for(int i = 0; i < 9; ++i)	
		if (!isfinite(vf[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
	
	// Store force vector

	for(int i = 0; i < 9; ++i)	
		this->m_vfVal(i) = vf[i];
}

void CSTElement::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3 + i]);
		x3[i] = vx(this->m_vidx[6 + i]);
	}

	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double G = m_vpmat[0]->getShear();

	double& A0 = this->m_A0;
	double& pS = this->m_preStrain;
	const DNDx& dNdx = this->m_dNdx;

	double mJ[9][9];

//#include "../../Maple/Code/CSTStVKOrthoJacobian.mcg"

	for(int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ[i][j];
}

Real CSTElement::compute_Area_2D(const vector<Vector2d>& vp) const
{
	return 0.5*(vp[1] - vp[0]).norm()*(vp[2] - vp[0]).norm();
}

void CSTElement::compute_DNDx_2D(const vector<Vector2d>& vp, DNDx& DNDx) const
{
	Vector2d x1_2D(0., 0.), x2_2D(0., 0.), x3_2D(0., 0.);

	Vector2d x12 = vp[1] - vp[0];
	Vector2d x13 = vp[2] - vp[0];

	Vector2d u = x12.normalized();
	Vector2d v = Vector2d(-u[1], u[0]);

	x2_2D(0) = x12.dot(u);
	x3_2D(0) = x13.dot(u);
	x3_2D(1) = x13.dot(v);

	double twiceArea = x1_2D[0] * x2_2D[1] + x2_2D[0] * x3_2D[1] + x3_2D[0] * x1_2D[1]
					 - x1_2D[1] * x2_2D[0] - x2_2D[1] * x3_2D[0] - x3_2D[1] * x1_2D[0];

	DNDx[0][0] = (x2_2D[1] - x3_2D[1]) / twiceArea;
	DNDx[0][1] = (x3_2D[0] - x2_2D[0]) / twiceArea;

	DNDx[1][0] = (x3_2D[1] - x1_2D[1]) / twiceArea;
	DNDx[1][1] = (x1_2D[0] - x3_2D[0]) / twiceArea;

	DNDx[2][0] = (x1_2D[1] - x2_2D[1]) / twiceArea;
	DNDx[2][1] = (x2_2D[0] - x1_2D[0]) / twiceArea;
}


Real CSTElement::compute_Area_3D(const vector<Vector3d>& vp) const
{
	return 0.5*(vp[1] - vp[0]).cross(vp[2] - vp[0]).norm();
}

void CSTElement::compute_DNDx_3D(const vector<Vector3d>& vp, DNDx& DNDx) const
{
	Vector2d x1_2D(0., 0.), x2_2D(0., 0.), x3_2D(0., 0.);

	Vector3d x12 = vp[1] - vp[0];
	Vector3d x13 = vp[2] - vp[0];

	Vector3d n = x12.cross(x13);
	Vector3d w = n.normalized();
	Vector3d u = x12.normalized();
	Vector3d v = w.cross(u);

	x2_2D[0] = x12.dot(u);
	x3_2D[0] = x13.dot(u);
	x3_2D[1] = x13.dot(v);

	double TwiceArea = x1_2D[0] * x2_2D[1] + x2_2D[0] * x3_2D[1] + x3_2D[0] * x1_2D[1]
					 - x1_2D[1] * x2_2D[0] - x2_2D[1] * x3_2D[0] - x3_2D[1] * x1_2D[0];

	DNDx[0][0] = (x2_2D[1] - x3_2D[1]) / TwiceArea;
	DNDx[0][1] = (x3_2D[0] - x2_2D[0]) / TwiceArea;

	DNDx[1][0] = (x3_2D[1] - x1_2D[1]) / TwiceArea;
	DNDx[1][1] = (x1_2D[0] - x3_2D[0]) / TwiceArea;

	DNDx[2][0] = (x1_2D[1] - x2_2D[1]) / TwiceArea;
	DNDx[2][1] = (x2_2D[0] - x1_2D[0]) / TwiceArea;
}
