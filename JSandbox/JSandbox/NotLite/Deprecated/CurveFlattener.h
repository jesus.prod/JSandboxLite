/*=====================================================================================*/
/*!
\file		CurveFlattener.h
\author		skourasm,jesusprod
\brief		Curve flattener.
*/
/*=====================================================================================*/

#ifndef CURVE_FLATTENER_H
#define CURVE_FLATTENER_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

class JSANDBOX_EXPORT CurveFlattener
{

public:
	CurveFlattener(void);
	virtual ~CurveFlattener(void);

	void SetTolGradient(double iTol) { m_tolGradient = iTol; }
	void SetKl(double iKl) { m_Kl = iKl; }
	void SetKa(double iKa) { m_Ka = iKa; }

	bool flattenCurve(const dVector& vposIn, const dVector& vnorIn, dVector& vposOut);

protected:

	void initializeCurve(const dVector& vposIn, dVector& vposOut);

	double m_tolGradient;
	double m_Kl;
	double m_Ka;
};

#endif