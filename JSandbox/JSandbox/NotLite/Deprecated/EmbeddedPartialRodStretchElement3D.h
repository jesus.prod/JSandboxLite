/*=====================================================================================*/
/*!
\file		EmbeddedPartialRodStretchElement3D.h
\author		jesusprod
\brief		Embedded rod length preserving energy in 3D. It uses SolidMaterial stretch K.
*/
/*=====================================================================================*/

#ifndef EMBEDDED_PARTIAL_ROD_STRETCH_ELEMENT_3D_H
#define EMBEDDED_PARTIAL_ROD_STRETCH_ELEMENT_3D_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/NodalSolidElement.h>

class EmbeddedPartialRodStretchElement3D : public NodalSolidElement
{
public:
	EmbeddedPartialRodStretchElement3D(int dx, int dX) : NodalSolidElement(4, dx, dX)
	{
		this->m_numEmbCoord = 3;
		this->m_numEmbNodes = 1;
	}

	virtual ~EmbeddedPartialRodStretchElement3D() { }

	virtual int getEmbeddedNodes() const { return this->m_numEmbNodes; }
	virtual int getCoordinatesPerNode() const { return m_numEmbCoord; }

	virtual const dVector& getEmbeddingCoordinates() const;
	virtual void setEmbCoordinates(const dVector& vi);

	virtual void addForcesToEmbedding(const dVector& vembFVal);
	virtual void addJacobianToEmbedding(const vector<dVector>& vembJVal);
	virtual void getEmbbededPositions3D0(const VectorXd& vx, vector<Vector3d>& vp);
	virtual void getEmbbededPositions2D0(const VectorXd& vx, vector<Vector2d>& vp);
	virtual void getEmbbededPositions3D(const VectorXd& vx, vector<Vector3d>& vp);
	virtual void getEmbbededPositions2D(const VectorXd& vx, vector<Vector2d>& vp);

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual double computeB() const;

	virtual void setEdgeIndex(int ei) { this->m_ei = ei; }
	virtual int getEdgeIndex() const { return this->m_ei; }

	virtual Real getIntegrationVolume() const { return this->m_L0; }

	virtual Real getRestLength() const { return this->m_L0; }
	virtual void setRestLength(Real L0) { this->m_L0 = L0; }

protected:
	Real m_L0; // Rest length

	int m_ei;

	int m_numEmbCoord;
	int m_numEmbNodes;
	iVector m_vembIdx;
	dVector m_vembCor;

	MatrixXd m_mDxDv;

};

#endif