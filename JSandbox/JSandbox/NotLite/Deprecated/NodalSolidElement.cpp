/*=====================================================================================*/
/*! 
\file		NodalSolidElement.cpp
\author		jesusprod
\brief		Implementation of NodalSolidElement.h
 */
/*=====================================================================================*/

#include <JSandbox/NodalSolidElement.h>

const iVector& NodalSolidElement::getNodeIndices() const
{
	return this->m_vnodeIdx;
}

void NodalSolidElement::setNodeIndices(const iVector& vidx)
{
	assert((int) vidx.size() == this->m_numNodeSim);
	
	int count = 0;
	this->m_vnodeIdx = vidx;
	this->m_vidx.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim*vidx[i];
		for (int d = 0; d < this->m_numDim; ++d)
			this->m_vidx[count++] = offset + d;
	}
}

const iVector& NodalSolidElement::getNodeIndices0() const
{
	return this->m_vnodeIdx0;
}

void NodalSolidElement::setNodeIndices0(const iVector& vidx)
{
	assert((int)vidx.size() == this->m_numNodeSim);

	int count = 0;
	this->m_vnodeIdx0 = vidx;
	this->m_vidx0.resize(this->m_nrawDOF0);
	for (int i = 0; i < this->m_numNodeSim; ++i)
	{
		int offset = this->m_numDim0*vidx[i];
		for (int d = 0; d < this->m_numDim0; ++d)
			this->m_vidx0[count++] = offset + d;
	}
}

