///*=====================================================================================*/
///*!
//\file		CurveFlatterner.cpp
//\author		mskouras,jesusprod
//\brief		Implementation of CurveFlattener.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/CurveFlattener.h>
//
//#include <JSandbox/Curve2DModel.h>
//#include <JSandbox/BConditionFixed.h>
//#include <JSandbox/SolverGenericDynSta.h>
//
//CurveFlattener::CurveFlattener()
//{
//	m_Kl = 1.0;
//	m_Ka = 1.0;
//	m_tolGradient = 1.0e-6;
//}
//
//CurveFlattener::~CurveFlattener()
//{
//	// Nothing to do here...
//}
//
//bool CurveFlattener::flattenCurve(const dVector& vposIn,  const dVector& vnorIn, dVector& vposOut)
//{
//	dVector vposIn2D; // Initialize
//	initializeCurve(vposIn, vposIn2D);
//
//	// Compute target lengths/angles
//
//	int nv = (int)vposIn.size() / 3;
//	dVector vang(nv);
//	dVector vlen(nv);
//	for (int i = 0; i < nv; ++i)
//	{
//		int i1 = i;
//		int i0 = (i - 1) % nv;
//		int i2 = (i + 1) % nv;
//		Vector3d nr = get3D(i1, vnorIn);
//		Vector3d x0 = get3D(i0, vposIn);
//		Vector3d x1 = get3D(i1, vposIn);
//		Vector3d x2 = get3D(i2, vposIn);
//		vlen[i] = (x2 - x1).norm();
//		Vector3d v0 = x1 - x0;
//		Vector3d v1 = x2 - x1;
//		v0.normalize();
//		v1.normalize();
//		if (v0.cross(v1).dot(nr) < 0.0)
//			vang[i] = -acos(v0.dot(v1));
//		else vang[i] = acos(v0.dot(v1));
//	}
//
//	// Create deformation model
//
//	Curve2DModel* pModel = new Curve2DModel();
//	
//	vector<SolidMaterial*> vpMat(1);
//	vpMat[0] = new SolidMaterial();
//	vpMat[0]->setAreaK(this->m_Ka);
//	vpMat[0]->setStretchK(this->m_Kl);
//	pModel->configureMaterial(true, vpMat);
//	pModel->configureCurve(vposIn2D, vlen, vang);
//
//	pModel->setup();
//
//	// Create minimizer
//
//	SolverGenericDynSta* pSolver = new SolverGenericDynSta(pModel);
//	pSolver->setIsNonLinear(true);
//	pSolver->setIsStatic(true);
//	pSolver->setSolverError(this->m_tolGradient);
//	pSolver->setLinearError(this->m_tolGradient);
//	pSolver->setSolverMaxIters(1000);
//	pSolver->setLinearMaxIters(1000);
//	pSolver->setRegularizeFactor(1e-3);
//	pSolver->setLineSearchFactor(0.5);
//	pSolver->setRegularizeMaxIters(10);
//	pSolver->setLineSearchMaxIters(10);
//	pSolver->setLinearSolver(PhysSolver::Cholesky);
//	BConditionFixed* pBCFixed = new BConditionFixed(pModel);
//	iVector vfix;
//	vfix.push_back(0);
//	vfix.push_back(1);
//	pBCFixed->initializeNodal(2, vfix);
//	pSolver->addBCondition(pBCFixed);
//
//	pSolver->setup();
//
//	if (pSolver->solve())
//	{
//		vposOut = toSTL(pModel->getPositions());
//
//		delete pSolver;
//		delete pModel;
//
//		return true;
//	}
//	else
//	{
//		delete pSolver;
//		delete pModel;
//
//		return false;
//	}
//}
//
//void CurveFlattener::initializeCurve(const dVector& vin3D, dVector& vout3D)
//{
//	assert((int)vin3D.size() % 3 == 0 && (int)vin3D.size() >= 6);
//
//	dVector vlen;
//	double totLen;
//
//	int nv = (int)vin3D.size();
//	Vector3d prevP = get3D(0, vin3D);
//	for (int i = 1; i <= nv; ++i)
//	{
//		Vector3d P = get3D(i%nv, vin3D);
//		double l = (P - prevP).norm();
//		vlen.push_back(l);
//		totLen += l;
//		prevP = P;
//	}
//
//	double R = totLen / (2 * M_PI);
//
//	vout3D.clear();
//	double angle = 0;
//	for (int i = 0; i < nv; ++i)
//	{
//		vout3D.push_back(R*cos(angle));
//		vout3D.push_back(R*sin(angle));
//		angle += vlen[i]/R;
//	}
//}
