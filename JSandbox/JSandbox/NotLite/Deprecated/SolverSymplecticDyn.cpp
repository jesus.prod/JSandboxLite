/*=====================================================================================*/
/*! 
\file		SolverSymplecticDyn.cpp
\author		jesusprod
\brief		Implementation of SolverSymplecticDyn.h
 */
/*=====================================================================================*/

#include <JSandbox/SolverSymplecticDyn.h>

SolverSymplecticDyn::SolverSymplecticDyn(SolidModel *pSM) : PhysSolver(pSM) 
{
}

SolverSymplecticDyn::~SolverSymplecticDyn() 
{ 
	// Nothing to do here
}

bool SolverSymplecticDyn::solve()
{
	this->m_solverTimer.restart();

	VectorXd vx(this->m_vx);
	VectorXd vv(this->m_vv);
	VectorXd vf(this->m_N);
	vf.setZero();
	this->m_pSM->addForce(vf);
	this->addBoundaryForce(this->m_vx, this->m_vv, vf);
	vv = this->m_vv + vf.cwiseProduct(this->m_vm.cwiseInverse())*this->m_dt;
	vx = this->m_vx + vv*this->m_dt;
	this->m_pSM->updateStatex(vx, vv);
	this->constrainBoundary(this->m_pSM);
	this->m_vxPre = this->m_vx;
	this->m_vvPre = this->m_vv;
	this->m_vx = vx;
	this->m_vv = vv;

	this->m_solverTimer.stopStoreLog();

	return true;
}

