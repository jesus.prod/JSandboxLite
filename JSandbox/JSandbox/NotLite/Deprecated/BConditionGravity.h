/*=====================================================================================*/
/*!
\file		BConditionGravity.h
\author		jesusprod
\brief		Gravity boundary condition.
*/
/*=====================================================================================*/

#ifndef BCONDITION_GRAVITY_H
#define BCONDITION_GRAVITY_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/BCondition.h>
#include <JSandbox/SolidModel.h>

class JSANDBOX_EXPORT BConditionGravity : public BCondition
{
public:

	BConditionGravity(SolidModel* pModel);

	virtual ~BConditionGravity();

	// <BCondition>

	virtual Real getEnergy(const VectorXd& vx, const VectorXd& vv) const;
	virtual void addForce(const VectorXd& vx, const VectorXd& vv, VectorXd& vf) const;
	virtual void addJacobian(const VectorXd& vx, const VectorXd& vv, tVector& vJ) const;

	// </BCondition>

	virtual void initializeNodal(int n, const VectorXd& vg);

protected:

	VectorXd m_vg;

};

#endif