/*=====================================================================================*/
/*!
\file		DERSlideStretchElement.cpp
\author		jesusprod
\brief		Implementation of DERSlideStretchElement.h
*/
/*=====================================================================================*/

#include <JSandbox/DERSlideStretchElement.h>

DERSlideStretchElement::DERSlideStretchElement() : DERSlideElement(6 + 2, 6 + 2)
{
	// Nothing to do here...
}

DERSlideStretchElement::~DERSlideStretchElement()
{
	// Nothing to do here...
}

void DERSlideStretchElement::updateRest(const dVector& vx, const dVector& vs)
{
	Real s1 = vs[this->m_vidx0[0]];
	Real s2 = vs[this->m_vidx0[1]];

	this->m_L0 = s2 - s1;
}

void DERSlideStretchElement::updateEnergy(const dVector& vx, const dVector& vs)
{
	Vector3d x1 = get3D(this->m_vidx[0], vx);
	Vector3d x2 = get3D(this->m_vidx[1], vx);
	Real s1 = vs[this->m_vidx[0]];
	Real s2 = vs[this->m_vidx[1]];

	Real ks = this->computeK();

	//#include "../../Maple/Code/DERSlideStretchEnergy.mcg"

	//this->m_energy = t22;
}

void DERSlideStretchElement::updateForce(const dVector& vx, const dVector& vs)
{
	Vector3d x1 = get3D(this->m_vidx[0], vx);
	Vector3d x2 = get3D(this->m_vidx[1], vx);
	Real s1 = vs[this->m_vidx[0]];
	Real s2 = vs[this->m_vidx[1]];

	Real ks = this->computeK();

	Real vf[8];

//#include "../../Maple/Code/DERSlideStretchForce.mcg"

	// Store force vector

	for (int i = 0; i < 8; ++i)
		this->m_vfVal(i) = vf[i];
}

void DERSlideStretchElement::updateJacobian(const dVector& vx, const dVector& vs)
{
	Vector3d x1 = get3D(this->m_vidx[0], vx);
	Vector3d x2 = get3D(this->m_vidx[1], vx);
	Real s1 = vs[this->m_vidx[0]];
	Real s2 = vs[this->m_vidx[1]];
	
	Real ks = this->computeK();

	Real mJ[8][8];

//#include "../../Maple/Code/DERSlideStretchJacobian.mcg"

	for (int i = 0; i < 8; ++i)
		for (int j = 0; j < 8; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 8; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ[i][j];
}

Real DERSlideStretchElement::computeK() const
{
	Real rh = this->m_vpmat[0]->getHRadius();
	Real rw = this->m_vpmat[0]->getWRadius();
	Real E = this->m_vpmat[0]->getYoung();
	Real A = rh*rw*M_PI;
	return E*A;
}
