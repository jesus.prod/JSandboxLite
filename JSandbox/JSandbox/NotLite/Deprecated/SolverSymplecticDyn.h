/*=====================================================================================*/
/*! 
\file		SolverSymplecticDyn.h
\author		jesusprod
\brief		Simplectic Euler dynamics solver.

*/
/*=====================================================================================*/

#ifndef SYMPLECTIC_EULER_SOLVER_H
#define SYMPLECTIC_EULER_SOLVER_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/PhysSolver.h>
#include <JSandbox/CustomTimer.h>
 
class JSANDBOX_EXPORT SolverSymplecticDyn : public PhysSolver
{

public: 
	SolverSymplecticDyn(SolidModel *pSM);
	virtual ~SolverSymplecticDyn();

	virtual bool solve();
	virtual Real getPotential(const VectorXd& vx, const VectorXd& vv) { return -1.0; }
	virtual void getResidual(const VectorXd& vx, const VectorXd& vv, VectorXd& vrhs) { }
	virtual void getMatrix(const VectorXd& vx, const VectorXd& vv, SparseMatrixXd& mA) { }

};

#endif