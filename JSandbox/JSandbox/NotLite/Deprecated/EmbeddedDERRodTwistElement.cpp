/*=====================================================================================*/
/*!
\file		EmbeddedDERRodTwistElement.cpp
\author		jesusprod
\brief		Implementation of EmbeddedDERRodTwistElement.h
*/
/*=====================================================================================*/

#include <JSandbox/EmbeddedDERRodTwistElement.h>

EmbeddedDERRodTwistElement::EmbeddedDERRodTwistElement() : SolidElement(29, 29)
{
	this->m_DeDv.resize(8, 11);
	this->m_DvDq.resize(11, 29);

	this->m_DeDv.setZero();
	this->m_DvDq.setZero();

	this->m_DeDv(3, 9) = 1.0;
	this->m_DeDv(7, 10) = 1.0;

	Matrix3d I;
	I.setIdentity();
	addBlock3x3From(0, 0, I * -1.0, this->m_DeDv);
	addBlock3x3From(4, 3, I * -1.0, this->m_DeDv);
	addBlock3x3From(0, 3, I, this->m_DeDv);
	addBlock3x3From(4, 6, I, this->m_DeDv);
}

EmbeddedDERRodTwistElement::~EmbeddedDERRodTwistElement()
{ 
	// Nothing to do here...
}

const dVector& EmbeddedDERRodTwistElement::getEmbCoordinates() const
{
	return this->m_vembCor;
}

void EmbeddedDERRodTwistElement::setEmbCoordinates(const dVector& vi)
{
	this->m_vembCor = vi;

	this->m_DvDq.resize(11, 29);
	this->m_DvDq.setZero();

	int count = 0;

	for (int i = 0; i < 3; ++i)
	{
		int offseti = 3 * i;

		for (int j = 0; j < 3; ++j)
		{
			int offsetj = 3 * 3 * i + 3 * j;

			for (int k = 0; k < 3; ++k) // Identity matrix scaled by coordinate
				this->m_DvDq(offseti + k, offsetj + k) = this->m_vembCor[count];

			count++;
		}
	}

	this->m_DvDq(9, 27) = 1.0;
	this->m_DvDq(10, 28) = 1.0;
}

void EmbeddedDERRodTwistElement::updateRest(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt)
{
	Vector3d c0 = get3D(this->m_vinds[0], vx);
	Vector3d c1 = get3D(this->m_vinds[1], vx);
	Vector3d c2 = get3D(this->m_vinds[2], vx);
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;
	double d0 = va[this->m_einds[0]];
	double d1 = va[this->m_einds[1]];
	double rt = vt[this->m_vinds[1]];

	this->updateRest(e, f, d0, d1, rt);
}

void EmbeddedDERRodTwistElement::updateRest(const Vector3d& e, const Vector3d& f, double d0, double d1, double rt)
{
	this->m_l0 = this->computeVertexLength(e, f);
	this->m_t0 = this->computeVertexTwist(d0, d1, rt);
}

void EmbeddedDERRodTwistElement::updateEnergy(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt)
{
	double d0 = va[this->m_einds[0]];
	double d1 = va[this->m_einds[1]];
	double rt = vt[this->m_vinds[1]];

	this->m_energy = this->computeEnergy(d0, d1, rt);
}

void EmbeddedDERRodTwistElement::updateForce(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt)
{
	Vector3d c0 = get3D(this->m_vinds[0], vx);
	Vector3d c1 = get3D(this->m_vinds[1], vx);
	Vector3d c2 = get3D(this->m_vinds[2], vx);
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;
	double d0 = va[this->m_einds[0]];
	double d1 = va[this->m_einds[1]];
	double rt = vt[this->m_vinds[1]];

	VectorXd vfe;
	this->computeForce(e, f, d0, d1, rt, vfe);
	this->m_vfVal = this->m_DvDq.transpose() * this->m_DeDv.transpose() * vfe;
}

void EmbeddedDERRodTwistElement::updateJacobian(const dVector& vx, const dVector& va, const vector<Frame>& vF, const dVector& vt)
{
	Vector3d c0 = get3D(this->m_vinds[0], vx);
	Vector3d c1 = get3D(this->m_vinds[1], vx);
	Vector3d c2 = get3D(this->m_vinds[2], vx);
	Vector3d e = c1 - c0;
	Vector3d f = c2 - c1;
	double d0 = va[this->m_einds[0]];
	double d1 = va[this->m_einds[1]];
	double rt = vt[this->m_vinds[1]];

	MatrixXd mJe;
	this->computeJacobian(e, f, d0, d1, rt, mJe);
	MatrixXd mJ = this->m_DvDq.transpose() * this->m_DeDv.transpose() * mJe * this->m_DeDv * this->m_DvDq;

	for (int i = 0; i < 29; ++i)
		for (int j = 0; j < 29; ++j)
		{
		if (i == j)
			continue;

		if (this->m_vidx[i] == this->m_vidx[j])
		{
			mJ(i, i) += mJ(i, j);
			mJ(i, j) = 0.0; // Sum
		}
		}

	int count = 0;
	for (int i = 0; i < 29; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ(i, j);
}

Real EmbeddedDERRodTwistElement::computeEnergy(double d0, double d1, double rt)
{
	double t = this->computeVertexTwist(d0, d1, rt);
	double twistDiff = t - this->m_t0; // Twist def.

	return this->computeB() / (2 * this->m_l0) * twistDiff*twistDiff;
}

void EmbeddedDERRodTwistElement::computeForce(const Vector3d& e, const Vector3d& f, double d0, double d1, double rt, VectorXd& vf)
{
	VectorXd gT(8);
	this->computeGradTwist(e, f, gT);
	double t = this->computeVertexTwist(d0, d1, rt);
	double twistDiff = t - this->m_t0;

	double kT = this->computeB();

	vf = gT * (kT / this->m_l0) * twistDiff * -1.0;
}

void EmbeddedDERRodTwistElement::computeJacobian(const Vector3d& e, const Vector3d& f, double d0, double d1, double rt, MatrixXd& mJ)
{
	VectorXd gT(8);
	MatrixXd hT(8, 8);
	this->computeGradTwist(e, f, gT);
	this->computeHessTwist(e, f, hT);
	double t = this->computeVertexTwist(d0, d1, rt);
	double twistDiff = t - this->m_t0;
	double milen = -1.0 / this->m_l0;

	double kT = this->computeB();

	mJ = hT * twistDiff + (gT*gT.transpose()) * milen * kT;
}

void EmbeddedDERRodTwistElement::computeGradTwist(const Vector3d& e, const Vector3d& f, VectorXd& gT) const
{
	gT.resize(8);
	gT.setZero();

	double norm_e = e.norm();
	double norm_f = f.norm();
	Vector3d kb = this->computeKB(e, f);

	Vector3d dmde = kb * (0.5 / norm_e);
	Vector3d dmdf = kb * (0.5 / norm_f);
	for (int i = 0; i < 3; i++)
	{
		gT[0 + i] = dmde[i];
		gT[4 + i] = dmdf[i];
	}
	gT[3] = -1.0;
	gT[7] = 1.0;
}

void EmbeddedDERRodTwistElement::computeHessTwist(const Vector3d& e, const Vector3d& f, MatrixXd& hT) const
{
	hT.resize(8, 8);
	hT.setZero();

	Vector3d te = e.normalized();
	Vector3d tf = f.normalized();
	double norm_e = e.norm();
	double norm_f = f.norm();
	double norm_e2 = norm_e*norm_e;
	double norm_f2 = norm_f*norm_f;
	double norm_e3 = norm_e2*norm_e;
	double norm_f3 = norm_f2*norm_f;
	Vector3d kb = this->computeKB(e, f);

	{
		Matrix3d I;
		I.setIdentity();

		Matrix3d DteDe = I * (1.0 / norm_e) - outerProduct(e, e) * (1.0 / norm_e3);
		Matrix3d DtfDf = I * (1.0 / norm_f) - outerProduct(f, f) * (1.0 / norm_f3);

		const double chi = 1 + te.dot(tf);
		const Vector3d tilde_t = 1.0 / chi * (te + tf);
		const Matrix3d D2mDe2 = -0.25 / norm_e2 * (outerProduct(kb, te + tilde_t) + outerProduct(te + tilde_t, kb));
		const Matrix3d D2mDf2 = -0.25 / norm_f2 * (outerProduct(kb, tf + tilde_t) + outerProduct(tf + tilde_t, kb));
		const Matrix3d D2mDeDf = 0.5 / (norm_e * norm_f) * (2.0 / chi * crossMatrix(te) - outerProduct(kb, tilde_t));
		const Matrix3d D2mDfDe = D2mDeDf.transpose();

		addBlock3x3From(0, 0, D2mDe2, hT);
		addBlock3x3From(4, 4, D2mDf2, hT);
		addBlock3x3From(0, 4, D2mDeDf, hT);
		addBlock3x3From(4, 0, D2mDfDe, hT);
	}
}

double EmbeddedDERRodTwistElement::computeVertexLength(const Vector3d& e, const Vector3d& f) const
{
	return (e.norm() + f.norm())*0.5;
}

double EmbeddedDERRodTwistElement::computeVertexTwist(double d0, double d1, double rt) const
{
	return rt + d1 - d0;
}

Vector3d EmbeddedDERRodTwistElement::computeKB(const Vector3d& e, const Vector3d& f) const
{
	Vector3d t0 = e.normalized();
	Vector3d t1 = f.normalized();
	return (t0.cross(t1)) * 2 * (1 / (1 + t0.dot(t1)));
}

double EmbeddedDERRodTwistElement::computeB() const
{
	double B = 0;

	double S = this->m_vpmat[0]->getShear();
	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double A = M_PI * w * h;
	B = 0.25 * S * A * (w * w + h * h);

	return B;
}

double EmbeddedDERRodTwistElement::getConstant() const
{
	return this->computeB()/this->m_l0; // Stiff.
}