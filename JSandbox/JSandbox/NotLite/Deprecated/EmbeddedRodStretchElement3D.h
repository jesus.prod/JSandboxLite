/*=====================================================================================*/
/*!
\file		EmbeddedRodStretchElement3D.h
\author		jesusprod
\brief		Embedded rod length preserving energy in 3D. It uses SolidMaterial stretch K.
*/
/*=====================================================================================*/

#ifndef EMBEDDED_ROD_STRETCH_ELEMENT_3D_H
#define EMBEDDED_ROD_STRETCH_ELEMENT_3D_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidMaterial.h>
#include <JSandbox/EmbeddedSolidElement.h>

class EmbeddedRodStretchElement3D : public EmbeddedSolidElement
{
public:
	EmbeddedRodStretchElement3D(int dim0);
	~EmbeddedRodStretchElement3D();

	virtual void updateRest(const VectorXd& vX);
	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv);
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv);
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv);

	virtual double computeB() const;

	virtual void setEdgeIndex(int ei) { this->m_ei = ei; }
	virtual int getEdgeIndex() const { return this->m_ei; }

	virtual Real getIntegrationVolume() const { return this->m_L0; }

	virtual Real getRestLength() const { return this->m_L0; }
	virtual void setRestLength(Real L0) { this->m_L0 = L0; }

protected:
	Real m_L0; // Rest length

	int m_ei;
};

#endif