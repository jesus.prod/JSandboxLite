/*=====================================================================================*/
/*!
\file		DERSlideModel.cpp
\author		jesusprod
\brief		Implementation of DERSlideModel.cpp
*/
/*=====================================================================================*/

#include <JSandbox/DERSlideModel.h>

DERSlideModel::DERSlideModel()
{
	// Nothing to do here...
}

DERSlideModel::~DERSlideModel()
{
	// Nothing to do here...
}

void DERSlideModel::configureNodeList(const RodNodeList& list, bool isClosed)
{
	this->m_isClosed = isClosed;
	this->m_rawNodeList = list;
}

void DERSlideModel::configureMaterial(SolidMaterial mat, bool isHomogeneous)
{
	this->m_rawMat = mat; // Raw material 
	this->m_isMatHomogeneous = isHomogeneous;
}

void DERSlideModel::setup()
{
	this->initializeDOFVector();
	this->initializeMaterials();

	this->m_pStretchEle = new BasicSlideStretchElement();
	this->m_pBendingEle = new BasicSlideBendingElement();

	vector<SolidMaterial*> vpmats;
	vpmats.push_back(this->m_vpMats[0]);
	vpmats.push_back(this->m_vpMats[1]);
	this->m_pStretchEle->setMaterials(vpmats);
	this->m_pBendingEle->setMaterials(vpmats);

	this->updateMass();

	this->m_isSetupReady = true;
	this->m_isRestReady = true;
	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

void DERSlideModel::initializeDOFVector()
{
	int index = 0;
	int gloIndex = 0;
	int simIndex = 0;

	vector<RodNode>& vnodes = this->m_rawNodeList.getNodes();
	this->m_numRodNodes = this->m_rawNodeList.getNumNodes();

	this->m_rawLength = 0;
	for (int iNode = 0; iNode < this->m_numRodNodes - 1; ++iNode) // Init. length
		this->m_rawLength += (vnodes[iNode].m_vx - vnodes[iNode + 1].m_vx).norm();

	for (int iNode = 0; iNode < this->m_numRodNodes; ++iNode)
	{
		RodNode& rn = vnodes[iNode];

		rn.m_index = index++;

		rn.m_velAL = 0.0; // Zero eulerian
		rn.m_vv = Vector3d(0.0, 0.0, 0.0);

		rn.m_isActive = true;

		for (int ii = 0; ii < 3; ++ii)
		{
			rn.m_vgloIdx.push_back(gloIndex++);
			rn.m_vsimIdx.push_back(simIndex++);
		}
	}

	for (int iNode = 0; iNode < this->m_numRodNodes; ++iNode)
	{
		if (!vnodes[iNode].m_isSliding)
			continue; // Not sliding
		
		vnodes[iNode].m_vgloIdx.push_back(gloIndex++);
		vnodes[iNode].m_vsimIdx.push_back(simIndex++);
	}

	this->m_gloDOF = gloIndex; // Total number of DOF

	this->m_defoNodeList = this->m_rawNodeList;
	this->updateActiveNodes(this->m_defoNodeList);
	this->updateEdgeStructure(this->m_defoNodeList, this->m_defoEdgeList);
	this->updateSimulationDOF(this->m_defoNodeList, this->m_defoEdgeList);

	this->m_restNodeList = this->m_defoNodeList;
	this->m_restEdgeList = this->m_defoEdgeList;
}

void DERSlideModel::initializeMaterials()
{
	int nEdges = this->m_restEdgeList.getNumEdges();

	this->m_vpMats.resize(nEdges);
	if (this->m_isMatHomogeneous)
	{
		SolidMaterial* pMat = new SolidMaterial(m_rawMat);
		for (int iEdge = 0; iEdge < nEdges; ++iEdge)
			m_vpMats[iEdge] = pMat; // Same
	}
	else
	{
		for (int iEdge = 0; iEdge < nEdges; ++iEdge) // Diff.
			m_vpMats[iEdge] = new SolidMaterial(m_rawMat);;
	}
}

void DERSlideModel::updateState0(const VectorXd& vX, const VectorXd& vV)
{
	assert(false); // Should not be called, modify some other way TODO
}

void DERSlideModel::updateStatex(const VectorXd& vx, const VectorXd& vv)
{
	this->m_vx = vx;
	this->m_vv = vv;

	// Update internal structure

	this->updateNodesState(this->m_defoNodeList);
	this->updateActiveNodes(this->m_defoNodeList);

	this->updateEdgeStructure(this->m_defoNodeList, this->m_defoEdgeList);
	this->updateSimulationDOF(this->m_defoNodeList, this->m_defoEdgeList);

	this->updateMass();

	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

void DERSlideModel::updateNodesState(RodNodeList& nodeList)
{
	int nNodes = nodeList.getNumNodes(); // Whole
	vector<RodNode>& vnodes = nodeList.getNodes();

	// Update active Lagrangian

	for (int iNode = 0; iNode < nNodes; ++iNode)
	{
		for (int ii = 0; ii < 3; ++ii)
		{
			vnodes[iNode].m_vx(ii) = this->m_vx(vnodes[iNode].m_vsimIdx[ii]);
			vnodes[iNode].m_vv(ii) = this->m_vv(vnodes[iNode].m_vsimIdx[ii]);
		}
	}

	// Update active Eulerian

	for (int iNode = 0; iNode < nNodes; ++iNode)
	{
		if (!vnodes[iNode].m_isSliding)
			continue; // Not sliding: out

		vnodes[iNode].m_posAL = this->m_vx(vnodes[iNode].m_vsimIdx[3]);
		vnodes[iNode].m_velAL = this->m_vv(vnodes[iNode].m_vsimIdx[3]);
	}

	// Update inactive nodes

	RodNodeList::SortNodeList_AL(vnodes); // Sort

	for (int iNode = 0; iNode < nNodes; ++iNode)
	{
		if (vnodes[iNode].m_isActive)
			continue; // Active: out

		if (iNode == 0)
		{
			vnodes[iNode].m_vx = vnodes[iNode + 1].m_vx;
			vnodes[iNode].m_vv = vnodes[iNode + 1].m_vv;
		}
		else if (iNode == nNodes - 1)
		{
			vnodes[iNode].m_vx = vnodes[iNode - 1].m_vx;
			vnodes[iNode].m_vv = vnodes[iNode - 1].m_vv;
		}
		else
		{
			Real t = vnodes[iNode].m_posAL;
			Real tprev = vnodes[iNode - 1].m_posAL;
			Real tnext = vnodes[iNode + 1].m_posAL;
			Real trange = tnext - tprev;
			Real wprev = (tnext - t) / trange;
			Real wnext = (t - tprev) / trange;
			vnodes[iNode].m_vx = wprev*vnodes[iNode - 1].m_vx + wnext*vnodes[iNode + 1].m_vx;
			vnodes[iNode].m_vv = wprev*vnodes[iNode - 1].m_vv + wnext*vnodes[iNode + 1].m_vv;
		}
	}

	ostringstream ostr;
	for (int iNode = 0; iNode < nNodes; ++iNode)
	{
		ostr << vnodes[iNode].m_posAL << " ";
		ostr << vnodes[iNode].m_velAL << " ";
		ostr << "| ";
	}

	qDebug("[TRACE] Arc-length: %s", ostr.str().c_str());
}

void DERSlideModel::updateActiveNodes(RodNodeList& nodeList)
{
	int nNodes = nodeList.getNumNodes(); // Whole
	vector<RodNode>& vnodes = nodeList.getNodes();

	// Update active set

	for (int iNode = 0; iNode < nNodes; ++iNode)
		vnodes[iNode].m_isActive = true; // Base

	// Update active set

	for (int iNode = 0; iNode < nNodes; ++iNode)
	{
		if (!vnodes[iNode].m_isSliding)
			continue; // Check overlap

		int overlapping = -1;

		for (int jNode = 0; jNode < nNodes; ++jNode)
		{
			// Don't check itself
			if (iNode == jNode)
				continue;

			if (isApprox(vnodes[iNode].m_posAL, vnodes[jNode].m_posAL, 1e-6))
			{
				overlapping = jNode;
				break; // Found node
			}
		}

		if (overlapping == -1)
			continue; // Back

		// The overlapped node is not active
		vnodes[overlapping].m_isActive = false;
	}
}

void DERSlideModel::updateEdgeStructure(RodNodeList& nodeList, RodEdgeList& edgeList)
{
	vector<RodNode>& vactiveNodes = nodeList.getActiveNodes();

	RodNodeList::SortNodeList_AL(vactiveNodes);
	int nActiveNodes = (int) vactiveNodes.size();

	m_numRodEdges = (m_isClosed)? nActiveNodes : nActiveNodes - 1;

	vector<RodEdge> vedges(m_numRodEdges);

	for (int iEdge = 0; iEdge < this->m_numRodEdges; ++iEdge)
	{
		int vNext = (iEdge != nActiveNodes - 1) ? iEdge + 1 : 0;

		RodEdge re;
		re.m_index = iEdge;
		re.m_nodePrev = vactiveNodes[iEdge].m_index;
		re.m_nodeNext = vactiveNodes[vNext].m_index;

		ostringstream ostr;
		ostr << "Edge found";
		ostr << " (" << vactiveNodes[iEdge].m_index << "," << vactiveNodes[vNext].m_index << ")";
		ostr << " : " << (vactiveNodes[vNext].m_vx - vactiveNodes[iEdge].m_vx).norm() << " - ";
		ostr << (vactiveNodes[vNext].m_posAL - vactiveNodes[iEdge].m_posAL) << "\n";
		qDebug(ostr.str().c_str());

		vedges[iEdge] = re;
	}

	edgeList.setEdges(vedges);
}

void DERSlideModel::updateSimulationDOF(RodNodeList& nodeList, RodEdgeList& edgeList)
{
	// Update DOF vector and simulation

	dVector vxSTL;
	dVector vvSTL;

	vector<RodNode>& vnodes = nodeList.getNodes();
	int nNodes = (int)vnodes.size(); // Whole 

	this->m_numActive = 0;
	this->m_numSliding = 0;
	
	this->m_vsimDOF.resize(this->m_gloDOF, -1);
	this->m_gs.resize(this->m_gloDOF, true);
	this->m_as.resize(this->m_gloDOF, true);

	for (int iNode = 0; iNode < nNodes; ++iNode)
	{
		RodNode& rn = vnodes[iNode];

		rn.m_vsimIdx.clear(); // Restart

		for (int ii = 0; ii < 3; ++ii)
		{
			int idx = (int)vxSTL.size();
			rn.m_vsimIdx.push_back(idx);
			vxSTL.push_back(rn.m_vx(ii));
			vvSTL.push_back(rn.m_vv(ii));

			int lastIdx = (int)rn.m_vsimIdx.size() - 1; // Update DOF map
			this->m_vsimDOF[rn.m_vgloIdx[lastIdx]] = rn.m_vsimIdx[lastIdx];

			if (!rn.m_isActive) this->m_as[rn.m_vgloIdx[lastIdx]] = false;
		}

		m_numActive++;
	}

	// Node Eulerian

	for (int iNode = 0; iNode < nNodes; ++iNode)
	{
		RodNode& rn = vnodes[iNode];
		if (!rn.m_isSliding)
			continue;

		int idx = (int)vxSTL.size();
		rn.m_vsimIdx.push_back(idx);
		vxSTL.push_back(rn.m_posAL);
		vvSTL.push_back(rn.m_velAL);

		int lastIdx = (int)rn.m_vsimIdx.size() - 1; // Update DOF map
		this->m_vsimDOF[rn.m_vgloIdx[lastIdx]] = rn.m_vsimIdx[lastIdx];

		if (!rn.m_isActive) this->m_as[rn.m_vgloIdx[lastIdx]] = false;

		this->m_gs[rn.m_vgloIdx[lastIdx]] = false;

		this->m_numSliding++;
	}

	this->m_simDOF = (int)vxSTL.size();

	this->m_vx = toEigen(vxSTL);
	this->m_vv = toEigen(vvSTL);

	//// Node Lagrangian

	//for (int iNode = 0; iNode < nNodes; ++iNode)
	//{
	//	RodNode& rn = vnodes[iNode];
	//	if (!rn.m_isActive)
	//		continue;

	//	rn.m_vsimIdx.clear();

	//	for (int ii = 0; ii < 3; ++ii)
	//	{
	//		int idx = (int)vxSTL.size();
	//		rn.m_vsimIdx.push_back(idx);
	//		vxSTL.push_back(rn.m_vx(ii));
	//		vvSTL.push_back(rn.m_vv(ii));

	//		int lastIdx = (int) rn.m_vsimIdx.size() - 1; // Update DOF map
	//		this->m_vsimDOF[rn.m_vgloIdx[lastIdx]] = rn.m_vsimIdx[lastIdx];
	//	}

	//	m_numActive++;
	//}

	//// Node Eulerian

	//for (int iNode = 0; iNode < nNodes; ++iNode)
	//{
	//	RodNode& rn = vnodes[iNode];

	//	if (!rn.m_isActive)
	//		continue;

	//	if (!rn.m_isSliding)
	//		continue;

	//	int idx = (int)vxSTL.size();
	//	rn.m_vsimIdx.push_back(idx);
	//	vxSTL.push_back(rn.m_posAL);
	//	vvSTL.push_back(rn.m_velAL);

	//	int lastIdx = (int) rn.m_vsimIdx.size() - 1; // Update DOF map
	//	this->m_vsimDOF[rn.m_vgloIdx[lastIdx]] = rn.m_vsimIdx[lastIdx];

	//	this->m_gs[rn.m_vgloIdx[lastIdx]] = false;

	//	this->m_numSliding++;
	//}

	//this->m_simDOF = (int)vxSTL.size();

	//this->m_vx = toEigen(vxSTL);
	//this->m_vv = toEigen(vvSTL);
}

void DERSlideModel::peekState(int i)
{
	SolidModel::peekState(i);

	int size = (int) this->m_vNodeListStack.size();
	if (i > size - 1)
	{
		qDebug("[WARN] In %s: acceding to unavailable stack state: %d\n", i);
		logSimu("[WARN] In %s: acceding to unavailable stack state: %d\n", i);

		this->m_defoNodeList = this->m_vNodeListStack.front();
	}
	else
	{
		this->m_defoNodeList = this->m_vNodeListStack[size - 1 - i];
	}

	this->updateEdgeStructure(this->m_defoNodeList, this->m_defoEdgeList);
	this->updateSimulationDOF(this->m_defoNodeList, this->m_defoEdgeList);

	this->updateMass();

	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

void DERSlideModel::pushState()
{
	SolidModel::pushState();

	this->m_vNodeListStack.push_back(this->m_defoNodeList);

	if ((int) this->m_vNodeListStack.size() > this->m_maxStackSize)
	{
		this->m_vNodeListStack.erase(this->m_vNodeListStack.begin());
	}
}

void DERSlideModel::popState()
{
	SolidModel::popState();

	this->m_defoNodeList = this->m_vNodeListStack.back();
	this->m_vNodeListStack.pop_back(); // Remove head

	this->updateEdgeStructure(this->m_defoNodeList, this->m_defoEdgeList);
	this->updateSimulationDOF(this->m_defoNodeList, this->m_defoEdgeList);

	this->updateMass();

	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;
}

void DERSlideModel::updateRest()
{
	// Nothing to do here
}

void DERSlideModel::updateMass()
{
	vector<RodNode>& vnodes = this->m_defoNodeList.getNodes();
	vector<RodEdge>& vedges = this->m_defoEdgeList.getEdges();
	RodNodeList::SortNodeList_ID(vnodes); // Sort by local index

	this->m_vm.resize(this->m_simDOF);
	this->m_vm.setZero(); // From zero

	this->m_mm.clear();
	
	//  Add mass at lagrangian vertex coords

	for (int i = 0; i < this->m_numRodEdges; ++i)
	{
		RodEdge& re = vedges[i];
		RodNode& node0 = vnodes[re.m_nodePrev];
		RodNode& node1 = vnodes[re.m_nodeNext];

		iVector vidx;
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node0.m_vsimIdx[ii]);
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node1.m_vsimIdx[ii]);
		if (!node0.m_isSliding) vidx.push_back(-1);
		else vidx.push_back(node0.m_vsimIdx[3]);
		if (!node1.m_isSliding) vidx.push_back(-1);
		else vidx.push_back(node1.m_vsimIdx[3]);

		Real ds = node1.m_posAL - node0.m_posAL;
		Vector3d w = (node1.m_vx - node0.m_vx) / ds;

		MatrixXd M(8, 8); M.setZero();
		//addBlock3x3From(0, 0, 2 * Matrix3d::Identity(), M);
		//addBlock3x3From(3, 3, 2 * Matrix3d::Identity(), M);
		//addBlock3x3From(0, 3, Matrix3d::Identity(), M);
		//addBlock3x3From(3, 0, Matrix3d::Identity(), M);
		//addBlockNxM(3, 1, 0, 6, -2 * w, M);
		//addBlockNxM(1, 3, 6, 0, -2 * w, M);
		//addBlockNxM(3, 1, 0, 7, -w, M);
		//addBlockNxM(1, 3, 7, 0, -w, M);
		//addBlockNxM(3, 1, 3, 6, -w, M);
		//addBlockNxM(1, 3, 6, 3, -w, M);
		//addBlockNxM(3, 1, 3, 7, -2 * w, M);
		//addBlockNxM(1, 3, 7, 3, -2 * w, M);
		//M(6, 6) = 2 * w.dot(w);
		//M(7, 7) = 2 * w.dot(w);
		//M(6, 7) = w.dot(w);
		//M(7, 6) = w.dot(w);

		Real rW = this->m_vpMats[0]->getWRadius();
		Real rH = this->m_vpMats[0]->getHRadius();
		Real d = this->m_vpMats[0]->getDensity();
		Real C = (1.0 / 6.0)*ds*d*rW*rH*M_PI;
		M = M*C;

		Vector3d sumLan(C, C, C);
		Real sumEul = 2*w.dot(w)*C;

		for (int ii = 0; ii < 3; ++ii)
		{
			this->m_vm[node0.m_vsimIdx[ii]] += sumLan(ii);
			this->m_vm[node1.m_vsimIdx[ii]] += sumLan(ii);
		}

		if (node0.m_isSliding) this->m_vm[node0.m_vsimIdx[3]] += sumEul;
		if (node1.m_isSliding) this->m_vm[node1.m_vsimIdx[3]] += sumEul;

		for (int ii = 0; ii < 8; ++ii)
		{
			if ((!node0.m_isSliding && ii == 6) ||
				(!node1.m_isSliding && ii == 7))
				continue; // No sliding coord.

			for (int jj = 0; jj <= ii; ++jj)
			{
				if ((!node0.m_isSliding && jj == 6) ||
					(!node1.m_isSliding && jj == 7))
					continue; // No sliding coord.

				int iSimIdx = -1;
				int jSimIdx = -1;
				if (vidx[ii] >= vidx[jj])
				{
					iSimIdx = vidx[ii];
					jSimIdx = vidx[jj];
				}
				else
				{
					iSimIdx = vidx[jj];
					jSimIdx = vidx[ii];
				}

				this->m_mm.push_back(Triplet<Real>(iSimIdx, jSimIdx, M(ii, jj)));
			}
		}
	}
}

void DERSlideModel::updateEnergy()
{
	vector<RodNode>& vnodes = this->m_defoNodeList.getNodes();
	vector<RodEdge>& vedges = this->m_defoEdgeList.getEdges();
	RodNodeList::SortNodeList_ID(vnodes); // Sort by local index

	this->m_energy = 0;

	// Add stretch energy

	for (int i = 0; i < this->m_numRodEdges; ++i)
	{
		RodEdge& re = vedges[i];
		RodNode& node0 = vnodes[re.m_nodePrev];
		RodNode& node1 = vnodes[re.m_nodeNext];

		assert(node0.m_posAL <= node1.m_posAL);
		
		this->m_energy += this->m_pStretchEle->computeEnergy(node0.m_vx, node1.m_vx, node0.m_posAL, node1.m_posAL);
	}

	//// Add bending energy

	//int numEles = (m_isClosed)? m_numRodEdges : m_numRodEdges - 1;

	//for (int iEdge = 0; iEdge < numEles; ++iEdge)
	//{
	//	int iNext = (iEdge != m_numRodEdges - 1) ? iEdge + 1 : 0;

	//	RodEdge& ePrev = vedges[iEdge];
	//	RodEdge& eNext = vedges[iNext];

	//	RodNode& node0 = vnodes[ePrev.m_nodePrev];
	//	RodNode& node1 = vnodes[ePrev.m_nodeNext];
	//	RodNode& node2 = vnodes[eNext.m_nodeNext];

	//	assert(node0.m_posAL <= node1.m_posAL);
	//	assert(node1.m_posAL <= node2.m_posAL);

	//	this->m_energy += this->m_pBendingEle->computeEnergy(node0.m_vx, node1.m_vx, node2.m_vx, node0.m_posAL, node1.m_posAL, node2.m_posAL);
	//}


	this->m_isEnergyReady = true;
}

void DERSlideModel::updateForce()
{
	vector<RodNode>& vnodes = this->m_defoNodeList.getNodes();
	vector<RodEdge>& vedges = this->m_defoEdgeList.getEdges();
	RodNodeList::SortNodeList_ID(vnodes); // Sort by local index

	this->m_vfVal.resize(this->m_simDOF);
	this->m_vfVal.setZero(); // From zero

	// Add stretch energy

	for (int i = 0; i < this->m_numRodEdges; ++i)
	{
		RodEdge& re = vedges[i];
		RodNode& node0 = vnodes[re.m_nodePrev];
		RodNode& node1 = vnodes[re.m_nodeNext];

		assert(node0.m_posAL <= node1.m_posAL);

		iVector vidx;
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node0.m_vsimIdx[ii]);
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node1.m_vsimIdx[ii]);
		if (!node0.m_isSliding) vidx.push_back(-1);
		else vidx.push_back(node0.m_vsimIdx[3]);
		if (!node1.m_isSliding) vidx.push_back(-1);
		else vidx.push_back(node1.m_vsimIdx[3]);

		VectorXd vf;

		this->m_pStretchEle->computeForce(node0.m_vx, node1.m_vx, node0.m_posAL, node1.m_posAL, vf);

		for (int ii = 0; ii < 6; ++ii)
			this->m_vfVal(vidx[ii]) += vf(ii);

		if (node0.m_isSliding) this->m_vfVal(vidx[6]) += vf(6);
		if (node1.m_isSliding) this->m_vfVal(vidx[7]) += vf(7); 
	}

	ostringstream ostr;
	for (int iDOF = 0; iDOF < this->m_simDOF; ++iDOF)
		ostr << this->m_vfVal(iDOF) << "\n"; // Trace force
	qDebug("[TRACE] Stretch force:\n %s", ostr.str().c_str());

	// Add bending energy

	int numEles = (m_isClosed)? m_numRodEdges : m_numRodEdges - 1;

	for (int iEdge = 0; iEdge < numEles; ++iEdge)
	{
		int iNext = (iEdge != m_numRodEdges - 1) ? iEdge + 1 : 0;

		RodEdge& ePrev = vedges[iEdge];
		RodEdge& eNext = vedges[iNext];

		RodNode& node0 = vnodes[ePrev.m_nodePrev];
		RodNode& node1 = vnodes[ePrev.m_nodeNext];
		RodNode& node2 = vnodes[eNext.m_nodeNext];

		assert(node0.m_posAL <= node1.m_posAL);
		assert(node1.m_posAL <= node2.m_posAL);

		iVector vidx;
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node0.m_vsimIdx[ii]);
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node1.m_vsimIdx[ii]);
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node2.m_vsimIdx[ii]);

		VectorXd vf;

		this->m_pBendingEle->computeForce(node0.m_vx, node1.m_vx, node2.m_vx, node0.m_posAL, node1.m_posAL, node2.m_posAL, vf);

		for (int ii = 0; ii < 9; ++ii)
			this->m_vfVal(vidx[ii]) += vf(ii);
	}

	this->m_isForceReady = true;
}

void DERSlideModel::updateJacobian()
{
	vector<RodNode>& vnodes = this->m_defoNodeList.getNodes();
	vector<RodEdge>& vedges = this->m_defoEdgeList.getEdges();
	RodNodeList::SortNodeList_ID(vnodes); // Sort by local index

	this->m_vJVal.clear();

	// Add stretch energy

	for (int i = 0; i < this->m_numRodEdges; ++i)
	{
		RodEdge& re = vedges[i];
		RodNode& node0 = vnodes[re.m_nodePrev];
		RodNode& node1 = vnodes[re.m_nodeNext];

		assert(node0.m_posAL <= node1.m_posAL);

		iVector vidx;
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node0.m_vsimIdx[ii]);
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node1.m_vsimIdx[ii]);
		if (!node0.m_isSliding) vidx.push_back(-1);
		else vidx.push_back(node0.m_vsimIdx[3]);
		if (!node1.m_isSliding) vidx.push_back(-1);
		else vidx.push_back(node1.m_vsimIdx[3]);

		MatrixXd mJ;

		this->m_pStretchEle->computeJacobian(node0.m_vx, node1.m_vx, node0.m_posAL, node1.m_posAL, mJ);

		for (int ii = 0; ii < 8; ++ii)
		{
			if ((!node0.m_isSliding && ii == 6) ||
				(!node1.m_isSliding && ii == 7))
				continue; // No sliding coord.

			for (int jj = 0; jj <= ii; ++jj)
			{
				if ((!node0.m_isSliding && jj == 6) ||
					(!node1.m_isSliding && jj == 7))
					continue; // No sliding coord.

				int iSimIdx = -1;
				int jSimIdx = -1;
				if (vidx[ii] >= vidx[jj])
				{
					iSimIdx = vidx[ii];
					jSimIdx = vidx[jj];
				}
				else
				{
					iSimIdx = vidx[jj];
					jSimIdx = vidx[ii];
				}

				this->m_vJVal.push_back(Triplet<Real>(iSimIdx, jSimIdx, mJ(ii, jj)));
			}
		}
	}

	// Add bending energy

	int numEles = (m_isClosed)? m_numRodEdges : m_numRodEdges - 1;

	for (int iEdge = 0; iEdge < numEles; ++iEdge)
	{
		int iNext = (iEdge != m_numRodEdges - 1) ? iEdge + 1 : 0;

		RodEdge& ePrev = vedges[iEdge];
		RodEdge& eNext = vedges[iNext];

		RodNode& node0 = vnodes[ePrev.m_nodePrev];
		RodNode& node1 = vnodes[ePrev.m_nodeNext];
		RodNode& node2 = vnodes[eNext.m_nodeNext];

		assert(node0.m_posAL <= node1.m_posAL);
		assert(node1.m_posAL <= node2.m_posAL);

		iVector vidx;
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node0.m_vsimIdx[ii]);
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node1.m_vsimIdx[ii]);
		for (int ii = 0; ii < 3; ++ii) vidx.push_back(node2.m_vsimIdx[ii]);

		MatrixXd mJ;

		this->m_pBendingEle->computeJacobian(node0.m_vx, node1.m_vx, node2.m_vx, node0.m_posAL, node1.m_posAL, node2.m_posAL, mJ);

		for (int ii = 0; ii < 9; ++ii)
		{
			for (int jj = 0; jj <= ii; ++jj)
			{
				int iSimIdx = -1;
				int jSimIdx = -1;
				if (vidx[ii] >= vidx[jj])
				{
					iSimIdx = vidx[ii];
					jSimIdx = vidx[jj];
				}
				else
				{
					iSimIdx = vidx[jj];
					jSimIdx = vidx[ii];
				}

				this->m_vJVal.push_back(Triplet<Real>(iSimIdx, jSimIdx, mJ(ii, jj)));
			}
		}
	}

	this->m_isJacobianReady = true;
}

void DERSlideModel::updateEnergyForce()
{
	this->updateEnergy();
	this->updateForce();
}

void DERSlideModel::updateForceJacobian()
{
	this->updateForce();
	this->updateJacobian();
}

void DERSlideModel::updateEnergyForceJacobian()
{
	this->updateEnergy();
	this->updateForce();
	this->updateJacobian();
}

Real DERSlideModel::getEnergy()
{
	if (!this->m_isEnergyReady)
		this->updateEnergy();

	return this->m_energy;
}

void DERSlideModel::addForce(VectorXd& vf, const vector<bool>* pvFixed)
{
	if (!this->m_isForceReady)
		this->updateForce();

	vf = this->m_vfVal;
}

void DERSlideModel::addJacobian(tVector& vJ, const vector<bool>* pvFixed)
{
	if (!this->m_isJacobianReady)
		this->updateJacobian();

	vJ = this->m_vJVal;
}

void DERSlideModel::visualizeEnergy(dVector& vcol)
{
	// TODO
}

void DERSlideModel::visualizeForce(dVector& vcol)
{
	// TODO
}