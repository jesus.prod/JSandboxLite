/*=====================================================================================*/
/*! 
\file		SolidModel.cpp
\author		jesusprod
\brief		Implementation of SolidModel.h
 */
/*=====================================================================================*/

#include <JSandbox/SolidModel.h>

#include <omp.h> // MP support

 

SolidModel::SolidModel()
{
	this->m_isSetupReady = false;
	this->m_isForceReady = false;
	this->m_isRestReady = false;
	this->m_nrawDOF = 0;
	this->m_numEle = 0;

	this->m_maxStackSize = 3;
}

void SolidModel::setup()
{
	this->preSetupActions();

 	this->initializeElements(); 
	this->initializeMaterials();
	this->precompute_Jacobian();
	this->updateRest();

	this->m_isSetupReady = true;
	this->m_isRestReady = true;
	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false;

	this->posSetupActions();

	this->m_gs.resize(this->m_nrawDOF, true);
	this->m_as.resize(this->m_nrawDOF, true);
}

void SolidModel::freeElements()
{
	for (int i = 0; i < this->m_numEle; ++i) 
		delete this->m_vpEles[i]; // Free
	this->m_vpEles.clear();
}

void SolidModel::freeMaterials()
{
	int nm = (int) this->m_vpMats.size();
	for (int i = 0; i <  nm; ++i)
		delete this->m_vpMats[i];
	this->m_vpMats.clear();
}

void SolidModel::initializeMaterials()
{
	if (this->m_isMatHomo)
	{
		assert((int) this->m_vpMats.size() > 0);
		SolidMaterial *pMat = this->m_vpMats[0];

		for (int i = 1; i < (int)this->m_vpMats.size(); ++i)
			delete this->m_vpMats[i]; // Delete setup materials

		this->m_vpMats.resize(1);
		this->m_vpMats[0] = pMat;

		// Initialize elements
		for (int i = 0; i < this->m_numEle; ++i)
			this->m_vpEles[i]->setMaterial(this->m_vpMats[0]);
	}
	else
	{	
		assert((int) this->m_vpMats.size() > 0);

		if ((int) this->m_vpMats.size() == this->m_numEle)
		{
			// Initialize elements
			for (int i = 0; i < this->m_numEle; ++i)
				this->m_vpEles[i]->setMaterial(this->m_vpMats[i]);
		}
		else
		{
			// Not enough elements, pick first one
			SolidMaterial *pMat = this->m_vpMats[0];

			for (int i = 1; i < (int)this->m_vpMats.size(); ++i)
				delete this->m_vpMats[i]; // Delete setup materials

			this->m_vpMats.resize(this->m_numEle);

			this->m_vpMats[0] = pMat;
			this->m_vpEles[0]->setMaterial(pMat);
			for (int i = 1; i < this->m_numEle; ++i)
			{
				SolidMaterial *pMati = new SolidMaterial();
				*pMati = *pMat;
				this->m_vpMats[i] = pMati;
				this->m_vpEles[i]->setMaterial(pMati);
			}
		}
	}
}

void SolidModel::precompute_Jacobian()
{
	this->m_nNZ_Jacobian = 0;

	tVector vt;
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->allocateSparseEntries(m_nNZ_Jacobian);
}

void SolidModel::configureMaterial(bool homo, const vector<SolidMaterial*>& vpMats)
{
	this->m_isMatHomo = homo;
	this->m_vpMats = vpMats;
}

void SolidModel::updateStatex(const VectorXd& vx, const VectorXd& vv)
{
	if (!this->m_isSetupReady)
		this->setup();

	assert(vx.size() == this->m_nrawDOF);
	assert(vv.size() == this->m_nrawDOF);
	this->m_vx = vx;
	this->m_vv = vv;
	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false; 
}

void SolidModel::updateState0(const VectorXd& vX, const VectorXd& vV)
{
	if (!this->m_isSetupReady)
		this->setup();

	assert(vX.size() == this->m_nrawDOF);
	assert(vV.size() == this->m_nrawDOF);

	this->m_vX = vX;
	this->m_vv = vV;
	this->m_isRestReady = false;
	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false; 
}

void SolidModel::pushState()
{
	this->m_vxStack.push_back(this->m_vx);
	this->m_vvStack.push_back(this->m_vv);

	if ((int) this->m_vxStack.size() > this->m_maxStackSize)
	{
		this->m_vxStack.erase(this->m_vxStack.begin());
		this->m_vvStack.erase(this->m_vvStack.begin());
	}
}

void SolidModel::popState()
{
	this->m_vx = this->m_vxStack.back();
	this->m_vv = this->m_vvStack.back();
	this->m_vxStack.pop_back();
	this->m_vvStack.pop_back();
}

void SolidModel::peekState(int i)
{
	int size = (int) this->m_vxStack.size();
	if (i > size - 1)
	{
		qDebug("[WARN] In %s: acceding to unavailable stack state: %d\n", i);
		logSimu("[WARN] In %s: acceding to unavailable stack state: %d\n", i);

		this->m_vx = this->m_vxStack.front();
		this->m_vv = this->m_vvStack.front();
	}
	else
	{
		this->m_vx = this->m_vxStack[size - 1 - i];
		this->m_vv = this->m_vvStack[size - 1 - i];
	}
}

void SolidModel::updateRest()
{
#ifdef OMP_RESTINIT
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateRest(this->m_vX);

	// Create mass vector and initialize it
	this->m_vm = VectorXd(this->m_nrawDOF);
	this->m_vm.setZero();
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->addMass(this->m_vm);

	this->m_isRestReady = true;
	this->m_isForceReady = false;
	this->m_isEnergyReady = false;
	this->m_isJacobianReady = false; 
}

void SolidModel::updateEnergy()
{
	if (!this->m_isRestReady)
		this->updateRest();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateEnergy(this->m_vx, this->m_vv);

	this->m_isEnergyReady = true;
}

void SolidModel::updateForce()
{
	if (!this->m_isRestReady)
		this->updateRest();

	this->testForceLocal();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateForce(this->m_vx, this->m_vv);

	this->m_isForceReady = true;
}

void SolidModel::updateJacobian()
{
	if (!this->m_isRestReady) 
		this->updateRest();

	this->testJacobianLocal();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateJacobian(this->m_vx, this->m_vv);

	this->m_isJacobianReady = true;
}

void SolidModel::updateEnergyForce()
{
	if (!this->m_isRestReady)
		this->updateRest();

	this->testForceLocal();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateEnergyForce(this->m_vx, this->m_vv);

	this->m_isEnergyReady = true;
	
}

void SolidModel::updateForceJacobian()
{
	if (!this->m_isRestReady)
		this->updateRest();

	this->testForceLocal();
	this->testJacobianLocal();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateForceJacobian(this->m_vx, this->m_vv);

	this->m_isForceReady = true;
	this->m_isJacobianReady = true;
} 

void SolidModel::updateEnergyForceJacobian()
{
	if (!this->m_isRestReady)
		this->updateRest();

	this->testForceLocal();
	this->testJacobianLocal();

#ifdef OMP_ENFORJAC
#pragma omp parallel
#endif
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->updateEnergyForceJacobian(this->m_vx, this->m_vv);

	this->m_isEnergyReady = true;
	this->m_isForceReady = true;
	this->m_isJacobianReady = true;
}

void SolidModel::testForceLocal()
{
	for (int i = 0; i < this->m_numEle; ++i)
	{
		Real error = this->m_vpEles[i]->testForceLocal(this->m_vx, this->m_vv);
		if (error > 1e-6) qDebug("[ERROR] Force test error: %.9f", error);
	}
}

void SolidModel::testJacobianLocal()
{
	for (int i = 0; i < this->m_numEle; ++i)
	{
		Real error = this->m_vpEles[i]->testJacobianLocal(this->m_vx, this->m_vv);
		if (error > 1e-6) qDebug("[ERROR] Jacobian test error: %.9f", error);
	}
}

Real SolidModel::getEnergy()
{
	if (!this->m_isEnergyReady)
		this->updateEnergy();

	Real energy = 0;
	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->getEnergy();
	return energy;
}

void SolidModel::addForce(VectorXd& vf, const vector<bool>* pFixed) 
{
	if (!this->m_isForceReady)
		this->updateForce();

	assert(vf.size() == this->m_nrawDOF);

	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->addForce(vf, pFixed);
}

void SolidModel::addJacobian(tVector& vJ, const vector<bool>* pFixed) 
{
	if (!this->m_isJacobianReady)
		this->updateJacobian();

	for (int i = 0; i < this->m_numEle; ++i)
		this->m_vpEles[i]->addJacobian(vJ, pFixed);
}