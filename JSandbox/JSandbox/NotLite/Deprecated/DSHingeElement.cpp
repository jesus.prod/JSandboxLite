/*=====================================================================================*/
/*!
\file		DSHingeElement.cpp
\author		jesusprod
\brief		Implementation of DSHingeElement.h
*/
/*=====================================================================================*/

#include <JSandbox/DSHingeElement.h>

DSHingeElement::DSHingeElement(int dim0) : NodalSolidElement(4, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);
}

DSHingeElement::~DSHingeElement()
{
	// Nothing to do here
}

void DSHingeElement::updateRest(const VectorXd& vX)
{
	vector<double> x0(3, 0.0);
	vector<double> x1(3, 0.0);
	vector<double> x2(3, 0.0);
	vector<double> x3(3, 0.0);
	for (int i = 0; i < this->m_numDim0; ++i)
	{
		x0[i] = vX(this->m_vidx0[i]);
		x1[i] = vX(this->m_vidx0[this->m_numDim0 + i]);
		x2[i] = vX(this->m_vidx0[2*this->m_numDim0 + i]);
		x3[i] = vX(this->m_vidx0[3*this->m_numDim0 + i]);
	}

	double t1 = x1[0] * x1[0];
	double t4 = x0[0] * x0[0];
	double t5 = x1[1] * x1[1];
	double t8 = x0[1] * x0[1];
	double t9 = x1[2] * x1[2];
	double t12 = x0[2] * x0[2];
	double t14 = sqrt(t1 - 2.0*x1[0] * x0[0] + t4 + t5 - 2.0*x1[1] * x0[1] + t8 + t9 - 2.0*x1[2] * x0[2]
		+ t12);
	double t20 = x3[0] * x3[0];
	double t25 = x3[1] * x3[1];
	double t32 = x3[2] * x3[2];
	double t49 = -x1[2] * t8*x3[2] - x1[2] * t4*x3[2] - t20*x1[1] * x0[1] - x1[0] * t12*x3[0] - t25*
		x1[2] * x0[2] - x3[1] * t9*x0[1] - x1[1] * t12*x3[1] - t32*x1[1] * x0[1] - x3[2] * t5*x0[2] - t32*
		x1[0] * x0[0] - x3[2] * t1*x0[2] - t20*x1[2] * x0[2] - x3[0] * t9*x0[0] - x3[0] * t5*x0[0] - x3[0] *
		t8*x1[0];
	double t56 = x1[1] * x0[2];
	double t57 = x1[2] * x0[1];
	double t58 = t56*t57;
	double t59 = x1[2] * x0[0];
	double t60 = x1[0] * x0[2];
	double t61 = t59*t60;
	double t62 = x1[0] * x0[1];
	double t63 = x1[1] * x0[0];
	double t64 = t62*t63;
	double t65 = x3[1] * x1[2];
	double t66 = x3[2] * x1[1];
	double t68 = x3[2] * x0[1];
	double t71 = x3[1] * x0[2];
	double t77 = x3[2] * x1[0];
	double t78 = x3[0] * x1[2];
	double t80 = x3[0] * x0[2];
	double t82 = -x1[1] * t4*x3[1] - t25*x1[0] * x0[0] - x3[1] * t1*x0[1] - t58 - t61 - t64 - t65*t66 +
		t65*t68 + t65*t56 + t71*t57 + t71*t66 - t71*t68 + t57*t66 + t68*t56 - t77*t78 + t77*t80;
	double t86 = x3[2] * x0[0];
	double t97 = x3[0] * x1[1];
	double t98 = x3[1] * x1[0];
	double t101 = x3[1] * x0[0];
	double t106 = x3[0] * x0[1];
	double t117 = t5*t12;
	double t118 = t9*t8;
	double t119 = 2.0*t77*t59 + 2.0*t86*t60 + 2.0*t86*t78 - 2.0*t86*t80 + 2.0*t60*t78 + 2.0*
		t80*t59 - 2.0*t97*t98 + 2.0*t97*t101 + 2.0*t97*t62 + 2.0*t106*t63 + 2.0*t106*t98 - 2.0*t106
		*t101 + 2.0*t63*t98 + 2.0*t101*t62 + t117 + t118;
	double t120 = t9*t4;
	double t121 = t1*t12;
	double t122 = t1*t8;
	double t123 = t5*t4;
	double t136 = t120 + t121 + t122 + t123 + t25*t9 + t25*t12 + t32*t5 + t32*t8 + t32*t1 + t32*t4 + t20
		*t9 + t20*t12 + t20*t5 + t20*t8 + t25*t1 + t25*t4;
	double t139 = sqrt(2.0*t49 + 2.0*t82 + t119 + t136);
	double t140 = 1 / t139;
	double t141 = x3[1] - x0[1];
	double t142 = x1[2] - x0[2];
	double t144 = x3[2] - x0[2];
	double t145 = x1[1] - x0[1];
	double t158 = x2[2] * x2[2];
	double t165 = x2[1] * x2[1];
	double t170 = x2[0] * x2[0];
	double t183 = -t12*x2[0] * x1[0] - t4*x2[2] * x1[2] - t12*x2[1] * x1[1] - t5*x2[2] * x0[2] - x1
		[1] * t158*x0[1] - t8*x2[2] * x1[2] - t9*x2[1] * x0[1] - x1[2] * t165*x0[2] - t9*x2[0] * x0[0] - x1
		[2] * t170*x0[2] - t1*x2[2] * x0[2] - x1[0] * t158*x0[0] - t1*x2[1] * x0[1] - x1[0] * t165*x0[0] -
		x1[0] * t8*x2[0];
	double t190 = x1[1] * x2[2];
	double t191 = x1[2] * x2[1];
	double t194 = x0[2] * x2[1];
	double t196 = x0[1] * x2[2];
	double t202 = x1[2] * x2[0];
	double t203 = x1[0] * x2[2];
	double t206 = -t4*x2[1] * x1[1] - t5*x2[0] * x0[0] - x1[1] * t170*x0[1] - t58 - t61 - t64 - t190*
		t191 + t190*t57 + t190*t194 + t56*t196 + t56*t191 + t196*t191 - t196*t194 + t57*t194 - t202*
		t203 + t202*t60;
	double t208 = x0[0] * x2[2];
	double t211 = x0[2] * x2[0];
	double t222 = x1[0] * x2[1];
	double t223 = x1[1] * x2[0];
	double t228 = x0[1] * x2[0];
	double t231 = x0[0] * x2[1];
	double t242 = 2.0*t202*t208 + 2.0*t59*t211 + 2.0*t59*t203 + 2.0*t211*t203 - 2.0*t211*
		t208 + 2.0*t60*t208 - 2.0*t222*t223 + 2.0*t222*t63 + 2.0*t222*t228 + 2.0*t62*t231 + 2.0*t62
		*t223 + 2.0*t231*t223 - 2.0*t231*t228 + 2.0*t63*t228 + t117 + t118;
	double t255 = t120 + t121 + t122 + t123 + t5*t158 + t8*t158 + t9*t165 + t12*t165 + t9*t170 + t12*
		t170 + t1*t158 + t4*t158 + t1*t165 + t4*t165 + t5*t170 + t8*t170;
	double t258 = sqrt(2.0*t183 + 2.0*t206 + t242 + t255);
	double t259 = 1 / t258;
	double t260 = x2[2] - x0[2];
	double t262 = x2[1] - x0[1];
	double t265 = t259*(t145*t260 - t142*t262);
	double t267 = 0.1E1*t140*(t141*t142 - t144*t145) + 0.1E1*t265;
	double t268 = t267*t267;
	double t269 = x1[0] - x0[0];
	double t271 = x3[0] - x0[0];
	double t276 = x2[0] - x0[0];
	double t280 = t259*(t142*t276 - t269*t260);
	double t282 = 0.1E1*t140*(t144*t269 - t271*t142) + 0.1E1*t280;
	double t283 = t282*t282;
	double t292 = t259*(t269*t262 - t145*t276);
	double t294 = 0.1E1*t140*(t271*t145 - t141*t269) + 0.1E1*t292;
	double t295 = t294*t294;
	double t297 = sqrt(t268 + t283 + t295);
	double t298 = 1 / t297;
	double t299 = t298*t282;
	double t302 = t298*t294;
	double t309 = t298*t267;
	double t332 = atan(0.1E1 / t14*((0.1E1*t299*t292 - 0.1E1*t302*t280)*t269 + (0.1E1*t302
		*t265 - 0.1E1*t309*t292)*t145 + (0.1E1*t309*t280 - 0.1E1*t299*t265)*t142) / (0.1E1*t265
		*t309 + 0.1E1*t280*t299 + 0.1E1*t292*t302));
	m_phi0 = 2.0*t332;
	m_gFac0 = t14;

	this->m_phi0 = 0.0;
}

void DSHingeElement::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	Vector3d x0(vx(this->m_vidx[0]), vx(this->m_vidx[1]), vx(this->m_vidx[2]));
	Vector3d x1(vx(this->m_vidx[3]), vx(this->m_vidx[4]), vx(this->m_vidx[5]));
	Vector3d x2(vx(this->m_vidx[6]), vx(this->m_vidx[7]), vx(this->m_vidx[8]));
	Vector3d x3(vx(this->m_vidx[9]), vx(this->m_vidx[10]), vx(this->m_vidx[11]));

	Vector3d n0 = (x0 - x1).cross(x3 - x1);
	Vector3d n1 = (x2 - x1).cross(x0 - x1);
	double n0len = n0.norm();
	double n1len = n1.norm();

	double length = (x0 - x1).norm();
	double area0 = 0.5*n0len;
	double area1 = 0.5*n1len;
	double cosAngle = (n0.dot(n1)) / (n0len*n1len);
	if (abs(cosAngle) > 1.0) // Prevent NaN from acos 
		cosAngle = (cosAngle >= 0.0) ? 1.0 : -1.0;
	double phi = acos(cosAngle);

	this->m_energy = m_vpmat[0]->getBendingK()*(3.0*length*length) / (area0 + area1)*(phi - m_phi0)*(phi - m_phi0);
}

void DSHingeElement::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidx[i]);
		x1[i] = vx(this->m_vidx[3 + i]);
		x2[i] = vx(this->m_vidx[6 + i]);
		x3[i] = vx(this->m_vidx[9 + i]);
	}

	double kB = m_vpmat[0]->getBendingK();
	double phi0 = this->m_phi0;
	double gFac = m_gFac0;
	dVector vf12(12);

	if (kB == 0)
	{
		for (int i = 0; i < 12; ++i)
			this->m_vfVal(i) = 0.0;
		return;
	}

	double t1 = kB*gFac;
	double t2 = x1[0] * x1[0];
	double t5 = x0[0] * x0[0];
	double t6 = x1[1] * x1[1];
	double t9 = x0[1] * x0[1];
	double t10 = x1[2] * x1[2];
	double t13 = x0[2] * x0[2];
	double t15 = sqrt(t2 - 2.0*x1[0] * x0[0] + t5 + t6 - 2.0*x1[1] * x0[1] + t9 + t10 - 2.0*x1[2] * x0
		[2] + t13);
	double t16 = 1 / t15;
	double t17 = x1[1] * x0[2];
	double t18 = x1[2] * x0[1];
	double t19 = t17*t18;
	double t20 = x1[2] * x0[0];
	double t21 = x1[0] * x0[2];
	double t22 = t20*t21;
	double t23 = x1[0] * x0[1];
	double t24 = x1[1] * x0[0];
	double t25 = t23*t24;
	double t26 = x3[1] * x1[2];
	double t27 = x3[2] * x1[1];
	double t29 = x3[2] * x0[1];
	double t32 = x3[1] * x0[2];
	double t38 = x3[2] * x1[0];
	double t39 = x3[0] * x1[2];
	double t41 = x3[0] * x0[2];
	double t44 = x3[2] * x0[0];
	double t46 = -t19 - t22 - t25 - t26*t27 + t26*t29 + t26*t17 + t32*t18 + t32*t27 - t32*t29 + t18*
		t27 + t29*t17 - t38*t39 + t38*t41 + t38*t20 + t44*t21;
	double t55 = x3[0] * x1[1];
	double t56 = x3[1] * x1[0];
	double t59 = x3[1] * x0[0];
	double t64 = x3[0] * x0[1];
	double t75 = t6*t13;
	double t76 = t10*t9;
	double t77 = t10*t5;
	double t78 = t2*t13;
	double t79 = 2.0*t44*t39 - 2.0*t44*t41 + 2.0*t21*t39 + 2.0*t41*t20 - 2.0*t55*t56 + 2.0*t55
		*t59 + 2.0*t55*t23 + 2.0*t64*t24 + 2.0*t64*t56 - 2.0*t64*t59 + 2.0*t24*t56 + 2.0*t59*t23 +
		t75 + t76 + t77 + t78;
	double t81 = t2*t9;
	double t82 = t6*t5;
	double t83 = x3[1] * x3[1];
	double t86 = x3[2] * x3[2];
	double t91 = x3[0] * x3[0];
	double t104 = t81 + t82 + t83*t10 + t83*t13 + t86*t6 + t86*t9 + t86*t2 + t86*t5 + t91*t10 + t91*
		t13 + t91*t6 + t91*t9 + t83*t2 + t83*t5 - 2.0*t91*x1[1] * x0[1] - 2.0*x1[1] * t13*x3[1];
	double t137 = -x3[1] * t2*x0[1] - x1[2] * t9*x3[2] - t83*x1[2] * x0[2] - x3[1] * t10*x0[1] - t86
		*x1[1] * x0[1] - x3[2] * t6*x0[2] - t86*x1[0] * x0[0] - x3[2] * t2*x0[2] - x1[2] * t5*x3[2] - x1[0]
		* t13*x3[0] - t91*x1[2] * x0[2] - x3[0] * t10*x0[0] - x3[0] * t6*x0[0] - x3[0] * t9*x1[0] - x1[1] *
		t5*x3[1] - t83*x1[0] * x0[0];
	double t140 = sqrt(2.0*t46 + t79 + t104 + 2.0*t137);
	double t141 = 1 / t140;
	double t142 = x3[1] - x0[1];
	double t143 = x1[2] - x0[2];
	double t145 = x3[2] - x0[2];
	double t146 = x1[1] - x0[1];
	double t149 = t141*(t142*t143 - t145*t146);
	double t151 = x1[1] * x2[2];
	double t152 = x1[2] * x2[1];
	double t155 = x0[2] * x2[1];
	double t157 = x0[1] * x2[2];
	double t163 = x1[2] * x2[0];
	double t164 = x1[0] * x2[2];
	double t167 = x0[0] * x2[2];
	double t169 = x0[2] * x2[0];
	double t171 = -t19 - t22 - t25 - t151*t152 + t151*t18 + t151*t155 + t17*t157 + t17*t152 + t157*
		t152 - t157*t155 + t18*t155 - t163*t164 + t163*t21 + t163*t167 + t20*t169;
	double t180 = x1[0] * x2[1];
	double t181 = x1[1] * x2[0];
	double t186 = x0[1] * x2[0];
	double t189 = x0[0] * x2[1];
	double t200 = 2.0*t20*t164 + 2.0*t169*t164 - 2.0*t169*t167 + 2.0*t21*t167 - 2.0*t180*
		t181 + 2.0*t180*t24 + 2.0*t180*t186 + 2.0*t23*t189 + 2.0*t23*t181 + 2.0*t189*t181 - 2.0*
		t189*t186 + 2.0*t24*t186 + t75 + t76 + t77 + t78;
	double t202 = x2[2] * x2[2];
	double t205 = x2[1] * x2[1];
	double t208 = x2[0] * x2[0];
	double t223 = t81 + t82 + t6*t202 + t9*t202 + t10*t205 + t13*t205 + t10*t208 + t13*t208 + t2*
		t202 + t5*t202 + t2*t205 + t5*t205 + t6*t208 + t9*t208 - 2.0*t13*x2[1] * x1[1] - 2.0*t6*x2[2] *
		x0[2];
	double t256 = -x1[1] * t202*x0[1] - t5*x2[2] * x1[2] - t13*x2[0] * x1[0] - t2*x2[2] * x0[2] - t9
		*x2[2] * x1[2] - t10*x2[1] * x0[1] - x1[2] * t205*x0[2] - t10*x2[0] * x0[0] - x1[2] * t208*x0[2] -
		x1[0] * t202*x0[0] - t2*x2[1] * x0[1] - x1[0] * t205*x0[0] - x1[0] * t9*x2[0] - t5*x2[1] * x1[1] -
		t6*x2[0] * x0[0] - x1[1] * t208*x0[1];
	double t259 = sqrt(2.0*t171 + t200 + t223 + 2.0*t256);
	double t260 = 1 / t259;
	double t261 = x2[2] - x0[2];
	double t263 = x2[1] - x0[1];
	double t266 = t260*(t146*t261 - t143*t263);
	double t268 = 0.1E1*t149 + 0.1E1*t266;
	double t269 = t268*t268;
	double t270 = x1[0] - x0[0];
	double t272 = x3[0] - x0[0];
	double t275 = t141*(t145*t270 - t272*t143);
	double t277 = x2[0] - x0[0];
	double t281 = t260*(t143*t277 - t270*t261);
	double t283 = 0.1E1*t275 + 0.1E1*t281;
	double t284 = t283*t283;
	double t288 = t141*(t272*t146 - t142*t270);
	double t293 = t260*(t270*t263 - t146*t277);
	double t295 = 0.1E1*t288 + 0.1E1*t293;
	double t296 = t295*t295;
	double t298 = sqrt(t269 + t284 + t296);
	double t299 = 1 / t298;
	double t300 = t299*t283;
	double t303 = t299*t295;
	double t310 = t299*t268;
	double t333 = atan(0.1E1*t16*((0.1E1*t300*t293 - 0.1E1*t303*t281)*t270 + (0.1E1*t303
		*t266 - 0.1E1*t310*t293)*t146 + (0.1E1*t310*t281 - 0.1E1*t300*t266)*t143) / (0.1E1*t266
		*t310 + 0.1E1*t281*t300 + 0.1E1*t293*t303));
	double t335 = 2.0*t333 - phi0;
	double t336 = t335*t16;
	double t337 = t16*t270;
	double t345 = sqrt(t208 - 2.0*x1[0] * x2[0] + t2 + t205 - 2.0*x1[1] * x2[1] + t6 + t202 - 2.0*x2
		[2] * x1[2] + t10);
	double t346 = 1 / t345;
	double t351 = t16*t146;
	double t356 = t16*t143;
	double t361 = -0.1E1*t337*t346*(x2[0] - x1[0]) - 0.1E1*t351*t346*(x2[1] - x1[1]) - 0.1E1
		*t356*t346*(x2[2] - x1[2]);
	double t362 = t361*t361;
	double t364 = sqrt(0.1E1 - t362);
	double t366 = t361 / t364;
	double t376 = sqrt(t91 - 2.0*x3[0] * x1[0] + t2 + t83 - 2.0*x3[1] * x1[1] + t6 + t86 - 2.0*x1[2] *
		x3[2] + t10);
	double t377 = 1 / t376;
	double t390 = -0.1E1*t337*t377*(x3[0] - x1[0]) - 0.1E1*t351*t377*(x3[1] - x1[1]) - 0.1E1
		*t356*t377*(x3[2] - x1[2]);
	double t391 = t390*t390;
	double t393 = sqrt(0.1E1 - t391);
	double t395 = t390 / t393;
	double t425 = sqrt(t208 - 2.0*x2[0] * x0[0] + t5 + t205 - 2.0*x2[1] * x0[1] + t9 + t202 - 2.0*x2
		[2] * x0[2] + t13);
	double t426 = 1 / t425;
	double t436 = 0.1E1*t337*t426*t277 + 0.1E1*t351*t426*t263 + 0.1E1*t356*t426*t261;
	double t437 = t436*t436;
	double t439 = sqrt(0.1E1 - t437);
	double t441 = t436 / t439;
	double t451 = sqrt(t91 - 2.0*x3[0] * x0[0] + t5 + t83 - 2.0*x3[1] * x0[1] + t9 + t86 - 2.0*x3[2] *
		x0[2] + t13);
	double t452 = 1 / t451;
	double t462 = 0.1E1*t337*t452*t272 + 0.1E1*t351*t452*t142 + 0.1E1*t356*t452*t145;
	double t463 = t462*t462;
	double t465 = sqrt(0.1E1 - t463);
	double t467 = t462 / t465;
	double t490 = t1*t335;
	double t492 = t16*(t441 + t366);
	double t503 = t16*(t467 + t395);
	vf12[0] = -0.1E1*t1*t336*(0.1E1*t366*t266 + 0.1E1*t395*t149);
	vf12[1] = -0.1E1*t1*t336*(0.1E1*t366*t281 + 0.1E1*t395*t275);
	vf12[2] = -0.1E1*t1*t336*(0.1E1*t366*t293 + 0.1E1*t395*t288);
	vf12[3] = -0.1E1*t1*t336*(0.1E1*t441*t266 + 0.1E1*t467*t149);
	vf12[4] = -0.1E1*t1*t336*(0.1E1*t441*t281 + 0.1E1*t467*t275);
	vf12[5] = -0.1E1*t1*t336*(0.1E1*t441*t293 + 0.1E1*t467*t288);
	vf12[6] = 0.1E1*t490*t492*t266;
	vf12[7] = 0.1E1*t490*t492*t281;
	vf12[8] = 0.1E1*t490*t492*t293;
	vf12[9] = 0.1E1*t490*t503*t149;
	vf12[10] = 0.1E1*t490*t503*t275;
	vf12[11] = 0.1E1*t490*t503*t288;

	//for (int i = 0; i < 12; ++i)
	//	if (!FloatHelper::IsFinite(vf12[i]))
	//		qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 12; ++i)
		this->m_vfVal(i) = -vf12[i];
}

void DSHingeElement::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidx[i]);
		x1[i] = vx(this->m_vidx[3 + i]);
		x2[i] = vx(this->m_vidx[6 + i]);
		x3[i] = vx(this->m_vidx[9 + i]);
	}

	double kB = m_vpmat[0]->getBendingK();
	double phi0 = this->m_phi0;
	double gFac = m_gFac0;

	double mJ[12][12];

	if (kB == 0)
	{
		int count = 0;
		for (int i = 0; i < 12; ++i)
			for (int j = 0; j < i; ++j)
				this->m_vJVal(count++) = 0.0;
		return;
	}

	double t1 = kB*gFac;
	double t2 = x1[0] * x1[0];
	double t3 = x1[0] * x0[0];
	double t5 = x0[0] * x0[0];
	double t6 = x1[1] * x1[1];
	double t7 = x1[1] * x0[1];
	double t9 = x0[1] * x0[1];
	double t10 = x1[2] * x1[2];
	double t11 = x1[2] * x0[2];
	double t13 = x0[2] * x0[2];
	double t14 = t2 - 2.0*t3 + t5 + t6 - 2.0*t7 + t9 + t10 - 2.0*t11 + t13;
	double t15 = sqrt(t14);
	double t16 = 1 / t15;
	double t17 = x1[0] * x0[1];
	double t18 = x1[1] * x0[0];
	double t19 = t17*t18;
	double t20 = x3[1] * x0[2];
	double t21 = x3[2] * x0[1];
	double t23 = x3[2] * x1[1];
	double t25 = x1[2] * x0[1];
	double t27 = x3[1] * x1[2];
	double t28 = x1[1] * x0[2];
	double t30 = x3[0] * x3[0];
	double t31 = t30*x1[1];
	double t33 = x1[0] * t13;
	double t35 = x3[1] * x3[1];
	double t36 = t35*x1[2];
	double t38 = x3[1] * t10;
	double t40 = t13*x3[1];
	double t42 = x3[2] * x3[2];
	double t43 = t42*x1[1];
	double t45 = x3[2] * t6;
	double t47 = t42*x1[0];
	double t49 = x3[2] * t2;
	double t51 = t5*x3[2];
	double t53 = -t19 - t20*t21 + t20*t23 + t20*t25 + t27*t28 - t31*x0[1] - t33*x3[0] - t36*x0[2] -
		t38*x0[1] - t40*x1[1] - t43*x0[1] - t45*x0[2] - t47*x0[0] - t49*x0[2] - t51*x1[2];
	double t54 = t30*x1[2];
	double t57 = x3[0] * t10;
	double t60 = x3[0] * t6;
	double t63 = x3[0] * t9;
	double t66 = x1[1] * t5;
	double t69 = t35*x1[0];
	double t72 = x3[1] * t2;
	double t84 = -2.0*t54*x0[2] - 2.0*t57*x0[0] - 2.0*t60*x0[0] - 2.0*t63*x1[0] - 2.0*t66*x3
		[1] - 2.0*t69*x0[0] - 2.0*t72*x0[1] + t42*t9 + t42*t2 + t42*t5 + t30*t10 + t30*t13 + t30*t6 + t30
		*t9 + t35*t2 + t35*t5;
	double t86 = t6*t13;
	double t87 = t10*t9;
	double t88 = t10*t5;
	double t89 = t2*t13;
	double t90 = t2*t9;
	double t91 = t6*t5;
	double t96 = 2.0*t28*t25;
	double t97 = x1[2] * t9;
	double t102 = x3[2] * x1[0];
	double t103 = x1[2] * x0[0];
	double t106 = x3[0] * x0[2];
	double t109 = x3[0] * x1[2];
	double t114 = t86 + t87 + t88 + t89 + t90 + t91 + t35*t10 + t35*t13 + t42*t6 - t96 - 2.0*t97*x3[2] +
		2.0*t25*t23 + 2.0*t102*t103 + 2.0*t102*t106 - 2.0*t102*t109 + 2.0*t21*t28;
	double t115 = x1[0] * x0[2];
	double t116 = t103*t115;
	double t117 = x3[0] * x1[1];
	double t118 = x3[1] * x0[0];
	double t120 = x3[1] * x1[0];
	double t124 = x3[2] * x0[0];
	double t130 = x3[0] * x0[1];
	double t137 = -t116 + t117*t118 - t117*t120 + t106*t103 + t115*t109 - t124*t106 + t124*t109 +
		t124*t115 + t118*t17 + t18*t120 - t130*t118 + t130*t120 + t130*t18 + t117*t17 + t27*t21 - t27*
		t23;
	double t139 = 2.0*t53 + t84 + t114 + 2.0*t137;
	double t140 = sqrt(t139);
	double t141 = 1 / t140;
	double t142 = x3[1] - x0[1];
	double t143 = x1[2] - x0[2];
	double t145 = x3[2] - x0[2];
	double t146 = x1[1] - x0[1];
	double t148 = t142*t143 - t145*t146;
	double t149 = t141*t148;
	double t152 = x2[2] * x2[2];
	double t155 = x2[1] * x2[1];
	double t158 = x2[0] * x2[0];
	double t167 = x1[2] * t5;
	double t170 = x1[1] * t13;
	double t173 = -2.0*t19 + t6*t152 + t9*t152 + t10*t155 + t13*t155 + t10*t158 + t13*t158 + t2*
		t152 + t5*t152 + t2*t155 + t5*t155 + t6*t158 + t9*t158 - 2.0*t167*x2[2] - 2.0*t170*x2[1];
	double t174 = t10*x2[1];
	double t177 = t6*x2[2];
	double t180 = x1[1] * t152;
	double t183 = t9*x2[2];
	double t186 = x1[2] * t155;
	double t189 = t10*x2[0];
	double t192 = x1[2] * t158;
	double t195 = t2*x2[2];
	double t198 = x1[0] * t152;
	double t201 = t2*x2[1];
	double t204 = x1[0] * t155;
	double t207 = x1[0] * t9;
	double t210 = t5*x2[1];
	double t213 = t6*x2[0];
	double t216 = x1[1] * t158;
	double t219 = -2.0*t174*x0[1] - 2.0*t177*x0[2] - 2.0*t180*x0[1] - 2.0*t183*x1[2] - 2.0*
		t186*x0[2] - 2.0*t189*x0[0] - 2.0*t192*x0[2] - 2.0*t195*x0[2] - 2.0*t198*x0[0] - 2.0*t201
		*x0[1] - 2.0*t204*x0[0] - 2.0*t207*x2[0] - 2.0*t210*x1[1] - 2.0*t213*x0[0] - 2.0*t216*x0
		[1] + t86;
	double t221 = t13*x2[0];
	double t224 = x1[1] * x2[2];
	double t227 = x1[2] * x2[1];
	double t230 = x0[2] * x2[1];
	double t233 = x0[1] * x2[2];
	double t242 = x1[2] * x2[0];
	double t243 = x1[0] * x2[2];
	double t248 = t87 + t88 + t89 + t90 + t91 - t96 - 2.0*t221*x1[0] + 2.0*t224*t25 - 2.0*t224*t227 +
		2.0*t224*t230 + 2.0*t28*t233 - 2.0*t233*t230 + 2.0*t233*t227 + 2.0*t28*t227 - 2.0*t242*
		t243 + 2.0*t25*t230;
	double t250 = x0[2] * x2[0];
	double t252 = x0[0] * x2[2];
	double t256 = x1[0] * x2[1];
	double t257 = x1[1] * x2[0];
	double t261 = x0[1] * x2[0];
	double t264 = x0[0] * x2[1];
	double t270 = t242*t115 + t103*t250 + t242*t252 + t103*t243 + t250*t243 - t256*t257 + t115*
		t252 - t250*t252 + t256*t261 + t256*t18 + t17*t264 - t116 - t264*t261 + t264*t257 + t17*t257 +
		t18*t261;
	double t272 = t173 + t219 + t248 + 2.0*t270;
	double t273 = sqrt(t272);
	double t274 = 1 / t273;
	double t275 = x2[2] - x0[2];
	double t277 = x2[1] - x0[1];
	double t279 = t146*t275 - t143*t277;
	double t280 = t274*t279;
	double t282 = 0.1E1*t149 + 0.1E1*t280;
	double t283 = t282*t282;
	double t284 = x1[0] - x0[0];
	double t286 = x3[0] - x0[0];
	double t288 = t145*t284 - t286*t143;
	double t289 = t141*t288;
	double t291 = x2[0] - x0[0];
	double t294 = t143*t291 - t284*t275;
	double t295 = t274*t294;
	double t297 = 0.1E1*t289 + 0.1E1*t295;
	double t298 = t297*t297;
	double t301 = t286*t146 - t142*t284;
	double t302 = t141*t301;
	double t306 = t284*t277 - t146*t291;
	double t307 = t274*t306;
	double t309 = 0.1E1*t302 + 0.1E1*t307;
	double t310 = t309*t309;
	double t312 = sqrt(t283 + t298 + t310);
	double t313 = 1 / t312;
	double t314 = t313*t297;
	double t317 = t313*t309;
	double t324 = t313*t282;
	double t347 = atan(0.1E1*((0.1E1*t314*t307 - 0.1E1*t317*t295)*t284 + (0.1E1*t317*
		t280 - 0.1E1*t324*t307)*t146 + (0.1E1*t324*t295 - 0.1E1*t314*t280)*t143)*t16 / (0.1E1*
		t280*t324 + 0.1E1*t295*t314 + 0.1E1*t307*t317));
	double t349 = 2.0*t347 - phi0;
	double t351 = 1 / t15 / t14;
	double t352 = t16*t284;
	double t353 = x1[0] * x2[0];
	double t355 = x1[1] * x2[1];
	double t357 = x2[2] * x1[2];
	double t359 = t158 - 2.0*t353 + t2 + t155 - 2.0*t355 + t6 + t152 - 2.0*t357 + t10;
	double t360 = sqrt(t359);
	double t361 = 1 / t360;
	double t362 = x2[0] - x1[0];
	double t363 = t361*t362;
	double t366 = t16*t146;
	double t367 = x2[1] - x1[1];
	double t368 = t361*t367;
	double t371 = t16*t143;
	double t372 = x2[2] - x1[2];
	double t373 = t361*t372;
	double t376 = -0.1E1*t352*t363 - 0.1E1*t366*t368 - 0.1E1*t371*t373;
	double t377 = t376*t376;
	double t378 = 0.1E1 - t377;
	double t379 = sqrt(t378);
	double t380 = 1 / t379;
	double t381 = t376*t380;
	double t384 = x3[0] * x1[0];
	double t386 = x3[1] * x1[1];
	double t388 = x1[2] * x3[2];
	double t390 = t30 - 2.0*t384 + t2 + t35 - 2.0*t386 + t6 + t42 - 2.0*t388 + t10;
	double t391 = sqrt(t390);
	double t392 = 1 / t391;
	double t393 = x3[0] - x1[0];
	double t394 = t392*t393;
	double t397 = x3[1] - x1[1];
	double t398 = t392*t397;
	double t401 = x3[2] - x1[2];
	double t402 = t392*t401;
	double t405 = -0.1E1*t352*t394 - 0.1E1*t366*t398 - 0.1E1*t371*t402;
	double t406 = t405*t405;
	double t407 = 0.1E1 - t406;
	double t408 = sqrt(t407);
	double t409 = 1 / t408;
	double t410 = t405*t409;
	double t413 = 0.1E1*t381*t280 + 0.1E1*t410*t149;
	double t414 = t351*t413;
	double t417 = t351*t284;
	double t421 = t16*t361;
	double t423 = 0.1E1*t421*t362;
	double t424 = t351*t146;
	double t428 = t351*t143;
	double t432 = -0.1E1*t417*t363*t284 + t423 - 0.1E1*t424*t368*t284 - 0.1E1*t428*t373*
		t284;
	double t433 = t432*t380;
	double t438 = t377 / t379 / t378;
	double t443 = 1 / t273 / t272;
	double t444 = t443*t279;
	double t445 = t242*x2[2];
	double t446 = t11*x2[0];
	double t447 = x1[2] * x1[0];
	double t448 = t447*x2[2];
	double t449 = t447*x0[2];
	double t450 = t250*x2[2];
	double t451 = t115*x2[2];
	double t452 = t256*x1[1];
	double t453 = t17*x2[1];
	double t454 = t17*x1[1];
	double t455 = t355*x2[0];
	double t456 = x2[1] * x0[1];
	double t457 = t456*x2[0];
	double t458 = t445 + t446 + t448 - t449 - t450 + t451 + t452 + t453 - t454 + t455 - t457;
	double t459 = t7*x2[0];
	double t462 = 2.0*t10*x0[0];
	double t464 = 2.0*t152*x0[0];
	double t466 = 2.0*t155*x0[0];
	double t468 = 2.0*t6*x0[0];
	double t469 = 2.0*t189;
	double t470 = t103*x2[2];
	double t472 = 2.0*t198;
	double t473 = 2.0*t204;
	double t474 = t355*x0[0];
	double t476 = 2.0*t213;
	double t477 = 2.0*t459 + t462 + t464 + t466 + t468 - t469 - 4.0*t470 - t472 - t473 - 4.0*t474 - t476
		;
	double t478 = 2.0*t458 + t477;
	double t479 = t444*t478;
	double t485 = t16*t392;
	double t487 = 0.1E1*t485*t393;
	double t494 = -0.1E1*t417*t394*t284 + t487 - 0.1E1*t424*t398*t284 - 0.1E1*t428*t402*
		t284;
	double t495 = t494*t409;
	double t500 = t406 / t408 / t407;
	double t505 = 1 / t140 / t139;
	double t506 = t505*t148;
	double t507 = t102*x1[2];
	double t508 = t102*x0[2];
	double t509 = x3[2] * x3[0];
	double t510 = t509*x1[2];
	double t511 = t509*x0[2];
	double t512 = t106*x1[2];
	double t513 = t117*x3[1];
	double t514 = t130*x1[1];
	double t515 = t130*x3[1];
	double t516 = t386*x1[0];
	double t517 = -t449 - t454 + t507 + t508 + t510 - t511 + t512 + t513 + t514 - t515 + t516;
	double t518 = t120*x0[1];
	double t521 = 2.0*t42*x0[0];
	double t523 = 2.0*t35*x0[0];
	double t524 = 2.0*t47;
	double t525 = t388*x0[0];
	double t527 = 2.0*t57;
	double t528 = 2.0*t60;
	double t529 = t18*x3[1];
	double t531 = 2.0*t69;
	double t532 = 2.0*t518 + t462 + t468 + t521 + t523 - t524 - 4.0*t525 - t527 - t528 - 4.0*t529 - t531
		;
	double t533 = 2.0*t517 + t532;
	double t534 = t506*t533;
	double t542 = 1 / t14;
	double t543 = t413*t413;
	double t557 = 0.1E1*t421*t367;
	double t561 = -0.1E1*t417*t363*t146 - 0.1E1*t424*t368*t146 + t557 - 0.1E1*t428*t373*
		t146;
	double t562 = t561*t380;
	double t568 = t224*x1[2];
	double t569 = t28*x2[2];
	double t570 = t28*x1[2];
	double t571 = t357*x2[1];
	double t572 = x2[2] * x0[2];
	double t573 = t572*x2[1];
	double t574 = t11*x2[1];
	double t575 = t256*x2[0];
	double t576 = t3*x2[1];
	double t578 = x1[0] * x1[1] * x2[0];
	double t579 = t18*x1[0];
	double t580 = t264*x2[0];
	double t581 = t568 + t569 - t570 + t571 - t573 + t574 + t575 + t576 + t578 - t579 - t580;
	double t582 = t18*x2[0];
	double t585 = 2.0*t152*x0[1];
	double t587 = 2.0*t10*x0[1];
	double t589 = 2.0*t2*x0[1];
	double t591 = 2.0*x0[1] * t158;
	double t592 = 2.0*t180;
	double t593 = t233*x1[2];
	double t595 = 2.0*t174;
	double t596 = 2.0*t201;
	double t597 = t17*x2[0];
	double t599 = 2.0*t216;
	double t600 = 2.0*t582 + t585 + t587 + t589 + t591 - t592 - 4.0*t593 - t595 - t596 - 4.0*t597 - t599
		;
	double t601 = 2.0*t581 + t600;
	double t602 = t444*t601;
	double t605 = -t274*t372;
	double t615 = 0.1E1*t485*t397;
	double t619 = -0.1E1*t417*t394*t146 - 0.1E1*t424*t398*t146 + t615 - 0.1E1*t428*t402*
		t146;
	double t620 = t619*t409;
	double t626 = t27*x3[2];
	double t627 = t20*x1[2];
	double t628 = t20*x3[2];
	double t629 = t388*x1[1];
	double t630 = t23*x0[2];
	double t631 = t117*x1[0];
	double t632 = t117*x0[0];
	double t633 = x3[0] * x3[1];
	double t634 = t633*x1[0];
	double t635 = t633*x0[0];
	double t636 = -t570 - t579 + t626 + t627 - t628 + t629 + t630 + t631 + t632 + t634 - t635;
	double t637 = t118*x1[0];
	double t638 = 2.0*t637;
	double t640 = 2.0*t42*x0[1];
	double t642 = 2.0*t30*x0[1];
	double t643 = 2.0*t38;
	double t644 = t388*x0[1];
	double t646 = 2.0*t43;
	double t647 = 2.0*t31;
	double t648 = t130*x1[0];
	double t650 = 2.0*t72;
	double t651 = t638 + t587 + t589 + t640 + t642 - t643 - 4.0*t644 - t646 - t647 - 4.0*t648 - t650;
	double t652 = 2.0*t636 + t651;
	double t653 = t506*t652;
	double t656 = t141*t401;
	double t664 = t542*t413;
	double t669 = 0.1E1*t381*t295 + 0.1E1*t410*t289;
	double t671 = 0.1E1*t664*t669;
	double t686 = 0.1E1*t421*t372;
	double t687 = -0.1E1*t417*t363*t143 - 0.1E1*t424*t368*t143 - 0.1E1*t428*t373*t143 +
		t686;
	double t688 = t687*t380;
	double t694 = t224*x2[1];
	double t695 = t7*x2[2];
	double t696 = x1[1] * x1[2];
	double t697 = t696*x2[1];
	double t698 = t696*x0[1];
	double t699 = t233*x2[1];
	double t700 = t25*x2[1];
	double t701 = t242*x1[0];
	double t702 = t242*x0[0];
	double t703 = t103*x1[0];
	double t704 = t353*x2[2];
	double t705 = x2[0] * x0[0];
	double t706 = t705*x2[2];
	double t707 = t694 + t695 + t697 - t698 - t699 + t700 + t701 + t702 - t703 + t704 - t706;
	double t708 = t3*x2[2];
	double t711 = 2.0*t6*x0[2];
	double t713 = 2.0*x0[2] * t155;
	double t715 = 2.0*x0[2] * t158;
	double t717 = 2.0*t2*x0[2];
	double t718 = 2.0*t177;
	double t719 = t28*x2[1];
	double t721 = 2.0*t186;
	double t722 = 2.0*t192;
	double t723 = t250*x1[0];
	double t725 = 2.0*t195;
	double t726 = 2.0*t708 + t711 + t713 + t715 + t717 - t718 - 4.0*t719 - t721 - t722 - 4.0*t723 - t725
		;
	double t727 = 2.0*t707 + t726;
	double t728 = t444*t727;
	double t731 = t274*t367;
	double t744 = 0.1E1*t485*t401;
	double t745 = -0.1E1*t417*t394*t143 - 0.1E1*t424*t398*t143 - 0.1E1*t428*t402*t143 +
		t744;
	double t746 = t745*t409;
	double t752 = t27*x1[1];
	double t753 = t27*x0[1];
	double t754 = x3[1] * x3[2];
	double t755 = t754*x1[1];
	double t756 = t754*x0[1];
	double t757 = t21*x1[1];
	double t758 = t102*x3[0];
	double t759 = t124*x1[0];
	double t760 = t124*x3[0];
	double t761 = t384*x1[2];
	double t762 = -t698 - t703 + t752 + t753 + t755 - t756 + t757 + t758 + t759 - t760 + t761;
	double t763 = t109*x0[0];
	double t766 = 2.0*t35*x0[2];
	double t768 = 2.0*t30*x0[2];
	double t769 = 2.0*t36;
	double t770 = t20*x1[1];
	double t772 = 2.0*t45;
	double t773 = 2.0*t49;
	double t774 = t115*x3[0];
	double t776 = 2.0*t54;
	double t777 = 2.0*t763 + t711 + t717 + t766 + t768 - t769 - 4.0*t770 - t772 - t773 - 4.0*t774 - t776
		;
	double t778 = 2.0*t762 + t777;
	double t779 = t506*t778;
	double t782 = -t141*t397;
	double t794 = 0.1E1*t381*t307 + 0.1E1*t410*t302;
	double t796 = 0.1E1*t664*t794;
	double t805 = 1 / t360 / t359;
	double t806 = t805*t362;
	double t811 = 0.1E1*t352*t361;
	double t815 = t805*t367;
	double t822 = t805*t372;
	double t826 = 0.1E1*t417*t363*t284 - t423 - 0.1E1*t352*t806*t362 + t811 + 0.1E1*t424*
		t368*t284 - 0.1E1*t366*t815*t362 + 0.1E1*t428*t373*t284 - 0.1E1*t371*t822*t362;
	double t827 = t826*t380;
	double t833 = t103*x0[2];
	double t835 = x0[2] * x0[0] * x2[2];
	double t836 = x0[1] * x0[0];
	double t837 = t836*x2[1];
	double t838 = -t445 + t446 + t470 - t833 + t450 + t835 - t455 + t474 + t457 + t837 + t459;
	double t839 = t7*x0[0];
	double t840 = 2.0*t839;
	double t841 = 2.0*t33;
	double t842 = 2.0*t207;
	double t843 = 2.0*t221;
	double t847 = 2.0*t9*x2[0];
	double t848 = -t840 + t472 + t841 + t473 + t842 - t843 - 4.0*t451 - t464 - 4.0*t453 - t466 - t847;
	double t849 = 2.0*t838 + t848;
	double t850 = t444*t849;
	double t857 = 1 / t391 / t390;
	double t858 = t857*t393;
	double t863 = 0.1E1*t352*t392;
	double t867 = t857*t397;
	double t874 = t857*t401;
	double t878 = 0.1E1*t417*t394*t284 - t487 - 0.1E1*t352*t858*t393 + t863 + 0.1E1*t424*
		t398*t284 - 0.1E1*t366*t867*t393 + 0.1E1*t428*t402*t284 - 0.1E1*t371*t874*t393;
	double t879 = t878*t409;
	double t885 = t124*x0[2];
	double t886 = -t833 - t839 - t510 + t511 + t525 + t885 + t512 - t513 + t514 + t515 + t529;
	double t888 = 2.0*t118*x0[1];
	double t891 = 2.0*t13*x3[0];
	double t892 = 2.0*t63;
	double t894 = t888 + t841 + t842 + t524 + t531 - t521 - 4.0*t508 - t891 - t892 - t523 - 4.0*t518;
	double t895 = 2.0*t886 + t894;
	double t896 = t506*t895;
	double t907 = t158 - 2.0*t705 + t5 + t155 - 2.0*t456 + t9 + t152 - 2.0*t572 + t13;
	double t908 = sqrt(t907);
	double t909 = 1 / t908;
	double t910 = t909*t291;
	double t913 = t909*t277;
	double t916 = t909*t275;
	double t919 = 0.1E1*t352*t910 + 0.1E1*t366*t913 + 0.1E1*t371*t916;
	double t920 = t919*t919;
	double t921 = 0.1E1 - t920;
	double t922 = sqrt(t921);
	double t923 = 1 / t922;
	double t924 = t919*t923;
	double t931 = x3[2] * x0[2];
	double t933 = t30 - 2.0*x3[0] * x0[0] + t5 + t35 - 2.0*x3[1] * x0[1] + t9 + t42 - 2.0*t931 + t13;
	double t934 = sqrt(t933);
	double t935 = 1 / t934;
	double t936 = t935*t286;
	double t939 = t935*t142;
	double t942 = t935*t145;
	double t945 = 0.1E1*t352*t936 + 0.1E1*t366*t939 + 0.1E1*t371*t942;
	double t946 = t945*t945;
	double t947 = 0.1E1 - t946;
	double t948 = sqrt(t947);
	double t949 = 1 / t948;
	double t950 = t945*t949;
	double t953 = 0.1E1*t924*t280 + 0.1E1*t950*t149;
	double t955 = 0.1E1*t664*t953;
	double t973 = 0.1E1*t366*t361;
	double t980 = 0.1E1*t417*t363*t146 - 0.1E1*t352*t806*t367 + 0.1E1*t424*t368*t146 -
		t557 - 0.1E1*t366*t815*t367 + t973 + 0.1E1*t428*t373*t146 - 0.1E1*t371*t822*t367;
	double t981 = t980*t380;
	double t987 = t233*x0[2];
	double t988 = t11*x0[1];
	double t989 = t3*x0[1];
	double t990 = -t571 + t593 + t573 + t987 + t574 - t988 - t575 + t576 + t597 - t989 + t580;
	double t992 = 2.0*t836*x2[0];
	double t993 = 2.0*t170;
	double t994 = 2.0*t66;
	double t997 = 2.0*t13*x2[1];
	double t998 = 2.0*t210;
	double t1000 = t992 + t592 + t993 + t599 + t994 - 4.0*t569 - t585 - t997 - t998 - 4.0*t582 - t591;
	double t1001 = 2.0*t990 + t1000;
	double t1002 = t444*t1001;
	double t1005 = t274*t275;
	double t1021 = 0.1E1*t366*t392;
	double t1028 = 0.1E1*t417*t394*t146 - 0.1E1*t352*t858*t397 + 0.1E1*t424*t398*t146 -
		t615 - 0.1E1*t366*t867*t397 + t1021 + 0.1E1*t428*t402*t146 - 0.1E1*t371*t874*t397;
	double t1029 = t1028*t409;
	double t1035 = t931*x0[1];
	double t1036 = t130*x0[0];
	double t1037 = -t988 - t989 - t626 + t627 + t628 + t644 + t1035 - t634 + t635 + t648 + t1036;
	double t1038 = 2.0*t40;
	double t1042 = 2.0*t5*x3[1];
	double t1043 = t638 + t993 + t994 + t646 + t647 - t1038 - t640 - 4.0*t630 - t642 - 4.0*t632 - t1042;
	double t1044 = 2.0*t1037 + t1043;
	double t1045 = t506*t1044;
	double t1048 = -t141*t145;
	double t1060 = 0.1E1*t924*t295 + 0.1E1*t950*t289;
	double t1062 = 0.1E1*t664*t1060;
	double t1086 = 0.1E1*t371*t361;
	double t1087 = 0.1E1*t417*t363*t143 - 0.1E1*t352*t806*t372 + 0.1E1*t424*t368*t143
		- 0.1E1*t366*t815*t372 + 0.1E1*t428*t373*t143 - t686 - 0.1E1*t371*t822*t372 + t1086;
	double t1088 = t1087*t380;
	double t1094 = t28*x0[1];
	double t1096 = x0[1] * x0[2] * x2[1];
	double t1097 = t250*x0[0];
	double t1098 = -t694 + t695 + t719 - t1094 + t699 + t1096 - t704 + t723 + t706 + t1097 + t708;
	double t1099 = t115*x0[0];
	double t1100 = 2.0*t1099;
	double t1101 = 2.0*t97;
	double t1102 = 2.0*t167;
	double t1103 = 2.0*t183;
	double t1107 = 2.0*t5*x2[2];
	double t1108 = -t1100 + t721 + t1101 + t722 + t1102 - t1103 - 4.0*t700 - t713 - 4.0*t702 - t715 -
		t1107;
	double t1109 = 2.0*t1098 + t1108;
	double t1110 = t444*t1109;
	double t1113 = -t274*t277;
	double t1135 = 0.1E1*t371*t392;
	double t1136 = 0.1E1*t417*t394*t143 - 0.1E1*t352*t858*t401 + 0.1E1*t424*t398*t143
		- 0.1E1*t366*t867*t401 + 0.1E1*t428*t402*t143 - t744 - 0.1E1*t371*t874*t401 + t1135;
	double t1137 = t1136*t409;
	double t1143 = t20*x0[1];
	double t1144 = -t1094 - t1099 - t755 + t756 + t770 + t1143 + t757 - t758 + t759 + t760 + t774;
	double t1146 = 2.0*t106*x0[0];
	double t1149 = 2.0*t9*x3[2];
	double t1150 = 2.0*t51;
	double t1152 = t1146 + t1101 + t1102 + t769 + t776 - t766 - 4.0*t753 - t1149 - t1150 - t768 - 4.0*
		t763;
	double t1153 = 2.0*t1144 + t1152;
	double t1154 = t506*t1153;
	double t1157 = t141*t142;
	double t1169 = 0.1E1*t924*t307 + 0.1E1*t950*t302;
	double t1171 = 0.1E1*t664*t1169;
	double t1174 = t349*t16;
	double t1184 = 0.1E1*t352*t806*t362 - t811 + 0.1E1*t366*t815*t362 + 0.1E1*t371*t822*
		t362;
	double t1185 = t1184*t380;
	double t1191 = -t448 + t449 + t470 + t833 + t451 - t835 - t452 + t453 + t454 + t474 - t837;
	double t1194 = t840 + t469 + t843 + t476 + t847 - t462 - 4.0*t446 - t841 - t842 - t468 - 4.0*t459;
	double t1195 = 2.0*t1191 + t1194;
	double t1196 = t444*t1195;
	double t1202 = t924 + t381;
	double t1203 = t1202*t274;
	double t1204 = t1203*t279;
	double t1206 = 0.1E1*t664*t1204;
	double t1218 = 0.1E1*t352*t806*t367 + 0.1E1*t366*t815*t367 - t973 + 0.1E1*t371*t822*
		t367;
	double t1219 = t1218*t380;
	double t1225 = -t568 + t569 + t570 + t593 - t987 + t988 - t578 + t579 + t597 + t989 + t582;
	double t1228 = -t992 + t595 + t997 + t596 + t998 - t993 - t587 - 4.0*t574 - t589 - 4.0*t576 - t994;
	double t1229 = 2.0*t1225 + t1228;
	double t1230 = t444*t1229;
	double t1233 = -t274*t143;
	double t1239 = t1203*t294;
	double t1241 = 0.1E1*t664*t1239;
	double t1253 = 0.1E1*t352*t806*t372 + 0.1E1*t366*t815*t372 + 0.1E1*t371*t822*t372 -
		t1086;
	double t1254 = t1253*t380;
	double t1260 = -t697 + t698 + t719 + t1094 + t700 - t1096 - t701 + t702 + t703 + t723 - t1097;
	double t1263 = t1100 + t718 + t1103 + t725 + t1107 - t711 - 4.0*t695 - t1101 - t1102 - t717 - 4.0*
		t708;
	double t1264 = 2.0*t1260 + t1263;
	double t1265 = t444*t1264;
	double t1268 = t274*t146;
	double t1274 = t1203*t306;
	double t1276 = 0.1E1*t664*t1274;
	double t1288 = 0.1E1*t352*t858*t393 - t863 + 0.1E1*t366*t867*t393 + 0.1E1*t371*t874*
		t393;
	double t1289 = t1288*t409;
	double t1295 = -t507 + t508 + t525 - t885 + t449 + t833 - t516 + t529 + t454 + t839 + t518;
	double t1298 = -t888 + t527 + t891 + t528 + t892 - t841 - 4.0*t512 - t462 - 4.0*t514 - t468 - t842;
	double t1299 = 2.0*t1295 + t1298;
	double t1300 = t506*t1299;
	double t1306 = t950 + t410;
	double t1307 = t1306*t141;
	double t1308 = t1307*t148;
	double t1310 = 0.1E1*t664*t1308;
	double t1322 = 0.1E1*t352*t858*t397 + 0.1E1*t366*t867*t397 - t1021 + 0.1E1*t371*t874*
		t397;
	double t1323 = t1322*t409;
	double t1329 = -t629 + t644 + t570 + t988 + t630 - t1035 - t631 + t632 + t648 - t1036 + t579;
	double t1333 = 2.0*t989 + t643 + t1038 + t650 + t1042 - 4.0*t627 - t587 - t993 - t994 - 4.0*t637 -
		t589;
	double t1334 = 2.0*t1329 + t1333;
	double t1335 = t506*t1334;
	double t1338 = t141*t143;
	double t1344 = t1307*t288;
	double t1346 = 0.1E1*t664*t1344;
	double t1358 = 0.1E1*t352*t858*t401 + 0.1E1*t366*t867*t401 + 0.1E1*t371*t874*t401 -
		t1135;
	double t1359 = t1358*t409;
	double t1365 = -t752 + t753 + t770 - t1143 + t698 + t1094 - t761 + t774 + t703 + t1099 + t763;
	double t1368 = -t1146 + t772 + t1149 + t773 + t1150 - t1101 - 4.0*t757 - t711 - 4.0*t759 - t717 -
		t1102;
	double t1369 = 2.0*t1365 + t1368;
	double t1370 = t506*t1369;
	double t1373 = -t141*t146;
	double t1379 = t1307*t301;
	double t1381 = 0.1E1*t664*t1379;
	double t1384 = t351*t669;
	double t1392 = t443*t294;
	double t1393 = t1392*t478;
	double t1396 = t274*t372;
	double t1404 = t505*t288;
	double t1405 = t1404*t533;
	double t1408 = -t141*t401;
	double t1425 = t1392*t601;
	double t1433 = t1404*t652;
	double t1441 = t669*t669;
	double t1453 = t1392*t727;
	double t1456 = -t274*t362;
	double t1464 = t1404*t778;
	double t1467 = t141*t393;
	double t1475 = t542*t669;
	double t1477 = 0.1E1*t1475*t794;
	double t1487 = t1392*t849;
	double t1490 = -t274*t275;
	double t1498 = t1404*t895;
	double t1501 = t141*t145;
	double t1510 = 0.1E1*t1475*t953;
	double t1520 = t1392*t1001;
	double t1528 = t1404*t1044;
	double t1537 = 0.1E1*t1475*t1060;
	double t1547 = t1392*t1109;
	double t1550 = t274*t291;
	double t1558 = t1404*t1153;
	double t1561 = -t141*t286;
	double t1570 = 0.1E1*t1475*t1169;
	double t1578 = t1392*t1195;
	double t1581 = t274*t143;
	double t1588 = 0.1E1*t1475*t1204;
	double t1596 = t1392*t1229;
	double t1603 = 0.1E1*t1475*t1239;
	double t1611 = t1392*t1264;
	double t1614 = -t274*t284;
	double t1621 = 0.1E1*t1475*t1274;
	double t1629 = t1404*t1299;
	double t1632 = -t141*t143;
	double t1639 = 0.1E1*t1475*t1308;
	double t1647 = t1404*t1334;
	double t1654 = 0.1E1*t1475*t1344;
	double t1662 = t1404*t1369;
	double t1665 = t141*t284;
	double t1672 = 0.1E1*t1475*t1379;
	double t1675 = t351*t794;
	double t1683 = t443*t306;
	double t1684 = t1683*t478;
	double t1687 = -t274*t367;
	double t1695 = t505*t301;
	double t1696 = t1695*t533;
	double t1699 = t141*t397;
	double t1716 = t1683*t601;
	double t1719 = t274*t362;
	double t1727 = t1695*t652;
	double t1730 = -t141*t393;
	double t1747 = t1683*t727;
	double t1755 = t1695*t778;
	double t1763 = t794*t794;
	double t1775 = t1683*t849;
	double t1778 = t274*t277;
	double t1786 = t1695*t895;
	double t1789 = -t141*t142;
	double t1797 = t542*t794;
	double t1799 = 0.1E1*t1797*t953;
	double t1809 = t1683*t1001;
	double t1812 = -t274*t291;
	double t1820 = t1695*t1044;
	double t1823 = t141*t286;
	double t1832 = 0.1E1*t1797*t1060;
	double t1842 = t1683*t1109;
	double t1850 = t1695*t1153;
	double t1859 = 0.1E1*t1797*t1169;
	double t1867 = t1683*t1195;
	double t1870 = -t274*t146;
	double t1877 = 0.1E1*t1797*t1204;
	double t1885 = t1683*t1229;
	double t1888 = t274*t284;
	double t1895 = 0.1E1*t1797*t1239;
	double t1903 = t1683*t1264;
	double t1910 = 0.1E1*t1797*t1274;
	double t1918 = t1695*t1299;
	double t1921 = t141*t146;
	double t1928 = 0.1E1*t1797*t1308;
	double t1936 = t1695*t1334;
	double t1939 = -t141*t284;
	double t1946 = 0.1E1*t1797*t1344;
	double t1954 = t1695*t1369;
	double t1961 = 0.1E1*t1797*t1379;
	double t1964 = t351*t953;
	double t1970 = t16*t909;
	double t1972 = 0.1E1*t1970*t291;
	double t1974 = 1 / t908 / t907;
	double t1975 = t1974*t291;
	double t1980 = 0.1E1*t352*t909;
	double t1984 = t1974*t277;
	double t1991 = t1974*t275;
	double t1995 = 0.1E1*t417*t910*t284 - t1972 + 0.1E1*t352*t1975*t291 - t1980 + 0.1E1*t424
		*t913*t284 + 0.1E1*t366*t1984*t291 + 0.1E1*t428*t916*t284 + 0.1E1*t371*t1991*t291;
	double t1996 = t1995*t923;
	double t2001 = t920 / t922 / t921;
	double t2010 = t16*t935;
	double t2012 = 0.1E1*t2010*t286;
	double t2014 = 1 / t934 / t933;
	double t2015 = t2014*t286;
	double t2020 = 0.1E1*t352*t935;
	double t2024 = t2014*t142;
	double t2031 = t2014*t145;
	double t2035 = 0.1E1*t417*t936*t284 - t2012 + 0.1E1*t352*t2015*t286 - t2020 + 0.1E1*t424
		*t939*t284 + 0.1E1*t366*t2024*t286 + 0.1E1*t428*t942*t284 + 0.1E1*t371*t2031*t286;
	double t2036 = t2035*t949;
	double t2041 = t946 / t948 / t947;
	double t2066 = 0.1E1*t1970*t277;
	double t2071 = 0.1E1*t366*t909;
	double t2078 = 0.1E1*t417*t910*t146 + 0.1E1*t352*t1975*t277 + 0.1E1*t424*t913*t146 -
		t2066 + 0.1E1*t366*t1984*t277 - t2071 + 0.1E1*t428*t916*t146 + 0.1E1*t371*t1991*t277;
	double t2079 = t2078*t923;
	double t2099 = 0.1E1*t2010*t142;
	double t2104 = 0.1E1*t366*t935;
	double t2111 = 0.1E1*t417*t936*t146 + 0.1E1*t352*t2015*t142 + 0.1E1*t424*t939*t146 -
		t2099 + 0.1E1*t366*t2024*t142 - t2104 + 0.1E1*t428*t942*t146 + 0.1E1*t371*t2031*t142;
	double t2112 = t2111*t949;
	double t2147 = 0.1E1*t1970*t275;
	double t2152 = 0.1E1*t371*t909;
	double t2153 = 0.1E1*t417*t910*t143 + 0.1E1*t352*t1975*t275 + 0.1E1*t424*t913*t143 +
		0.1E1*t366*t1984*t275 + 0.1E1*t428*t916*t143 - t2147 + 0.1E1*t371*t1991*t275 - t2152;
	double t2154 = t2153*t923;
	double t2180 = 0.1E1*t2010*t145;
	double t2185 = 0.1E1*t371*t935;
	double t2186 = 0.1E1*t417*t936*t143 + 0.1E1*t352*t2015*t145 + 0.1E1*t424*t939*t143 +
		0.1E1*t366*t2024*t145 + 0.1E1*t428*t942*t143 - t2180 + 0.1E1*t371*t2031*t145 - t2185;
	double t2187 = t2186*t949;
	double t2215 = -0.1E1*t417*t910*t284 + t1972 - 0.1E1*t424*t913*t284 - 0.1E1*t428*t916*
		t284;
	double t2216 = t2215*t923;
	double t2233 = -0.1E1*t417*t936*t284 + t2012 - 0.1E1*t424*t939*t284 - 0.1E1*t428*t942*
		t284;
	double t2234 = t2233*t949;
	double t2247 = t953*t953;
	double t2263 = -0.1E1*t417*t910*t146 - 0.1E1*t424*t913*t146 + t2066 - 0.1E1*t428*t916*
		t146;
	double t2264 = t2263*t923;
	double t2283 = -0.1E1*t417*t936*t146 - 0.1E1*t424*t939*t146 + t2099 - 0.1E1*t428*t942*
		t146;
	double t2284 = t2283*t949;
	double t2299 = t542*t953;
	double t2301 = 0.1E1*t2299*t1060;
	double t2315 = -0.1E1*t417*t910*t143 - 0.1E1*t424*t913*t143 - 0.1E1*t428*t916*t143 +
		t2147;
	double t2316 = t2315*t923;
	double t2335 = -0.1E1*t417*t936*t143 - 0.1E1*t424*t939*t143 - 0.1E1*t428*t942*t143 +
		t2180;
	double t2336 = t2335*t949;
	double t2352 = 0.1E1*t2299*t1169;
	double t2364 = -0.1E1*t352*t1975*t291 + t1980 - 0.1E1*t366*t1984*t291 - 0.1E1*t371*
		t1991*t291;
	double t2365 = t2364*t923;
	double t2377 = 0.1E1*t2299*t1204;
	double t2389 = -0.1E1*t352*t1975*t277 - 0.1E1*t366*t1984*t277 + t2071 - 0.1E1*t371*
		t1991*t277;
	double t2390 = t2389*t923;
	double t2404 = 0.1E1*t2299*t1239;
	double t2416 = -0.1E1*t352*t1975*t275 - 0.1E1*t366*t1984*t275 - 0.1E1*t371*t1991*
		t275 + t2152;
	double t2417 = t2416*t923;
	double t2431 = 0.1E1*t2299*t1274;
	double t2443 = -0.1E1*t352*t2015*t286 + t2020 - 0.1E1*t366*t2024*t286 - 0.1E1*t371*
		t2031*t286;
	double t2444 = t2443*t949;
	double t2456 = 0.1E1*t2299*t1308;
	double t2468 = -0.1E1*t352*t2015*t142 - 0.1E1*t366*t2024*t142 + t2104 - 0.1E1*t371*
		t2031*t142;
	double t2469 = t2468*t949;
	double t2483 = 0.1E1*t2299*t1344;
	double t2495 = -0.1E1*t352*t2015*t145 - 0.1E1*t366*t2024*t145 - 0.1E1*t371*t2031*
		t145 + t2185;
	double t2496 = t2495*t949;
	double t2510 = 0.1E1*t2299*t1379;
	double t2513 = t351*t1060;
	double t2639 = t1060*t1060;
	double t2669 = t542*t1060;
	double t2671 = 0.1E1*t2669*t1169;
	double t2687 = 0.1E1*t2669*t1204;
	double t2701 = 0.1E1*t2669*t1239;
	double t2717 = 0.1E1*t2669*t1274;
	double t2733 = 0.1E1*t2669*t1308;
	double t2747 = 0.1E1*t2669*t1344;
	double t2763 = 0.1E1*t2669*t1379;
	double t2766 = t351*t1169;
	double t2919 = t1169*t1169;
	double t2936 = t542*t1169;
	double t2938 = 0.1E1*t2936*t1204;
	double t2954 = 0.1E1*t2936*t1239;
	double t2968 = 0.1E1*t2936*t1274;
	double t2984 = 0.1E1*t2936*t1308;
	double t3000 = 0.1E1*t2936*t1344;
	double t3014 = 0.1E1*t2936*t1379;
	double t3017 = t351*t1202;
	double t3024 = t16*(t1996 + t2001*t1995 + t433 + t438*t432);
	double t3027 = t16*t1202;
	double t3040 = t16*(t2079 + t2001*t2078 + t562 + t438*t561);
	double t3057 = t16*(t2154 + t2001*t2153 + t688 + t438*t687);
	double t3074 = t16*(t2216 + t2001*t2215 + t827 + t438*t826);
	double t3089 = t16*(t2264 + t2001*t2263 + t981 + t438*t980);
	double t3106 = t16*(t2316 + t2001*t2315 + t1088 + t438*t1087);
	double t3120 = t16*(t2365 + t2001*t2364 + t1185 + t438*t1184);
	double t3127 = t1202*t1202;
	double t3128 = t542*t3127;
	double t3129 = 1 / t272;
	double t3130 = t279*t279;
	double t3139 = t16*(t2390 + t2001*t2389 + t1219 + t438*t1218);
	double t3148 = t3129*t279;
	double t3151 = 0.1E1*t3128*t3148*t294;
	double t3157 = t16*(t2417 + t2001*t2416 + t1254 + t438*t1253);
	double t3168 = 0.1E1*t3128*t3148*t306;
	double t3172 = t1*t542*t1202;
	double t3175 = 0.1E1*t3172*t280*t1308;
	double t3178 = 0.1E1*t3172*t280*t1344;
	double t3181 = 0.1E1*t3172*t280*t1379;
	double t3272 = t294*t294;
	double t3289 = 0.1E1*t3128*t3129*t294*t306;
	double t3294 = 0.1E1*t3172*t295*t1308;
	double t3297 = 0.1E1*t3172*t295*t1344;
	double t3300 = 0.1E1*t3172*t295*t1379;
	double t3401 = t306*t306;
	double t3409 = 0.1E1*t3172*t307*t1308;
	double t3412 = 0.1E1*t3172*t307*t1344;
	double t3415 = 0.1E1*t3172*t307*t1379;
	double t3416 = t351*t1306;
	double t3423 = t16*(t2036 + t2041*t2035 + t495 + t500*t494);
	double t3426 = t16*t1306;
	double t3439 = t16*(t2112 + t2041*t2111 + t620 + t500*t619);
	double t3456 = t16*(t2187 + t2041*t2186 + t746 + t500*t745);
	double t3473 = t16*(t2234 + t2041*t2233 + t879 + t500*t878);
	double t3488 = t16*(t2284 + t2041*t2283 + t1029 + t500*t1028);
	double t3505 = t16*(t2336 + t2041*t2335 + t1137 + t500*t1136);
	double t3519 = t16*(t2444 + t2041*t2443 + t1289 + t500*t1288);
	double t3526 = t1306*t1306;
	double t3527 = t542*t3526;
	double t3528 = 1 / t139;
	double t3529 = t148*t148;
	double t3538 = t16*(t2469 + t2041*t2468 + t1323 + t500*t1322);
	double t3547 = t3528*t148;
	double t3550 = 0.1E1*t3527*t3547*t288;
	double t3556 = t16*(t2496 + t2041*t2495 + t1359 + t500*t1358);
	double t3567 = 0.1E1*t3527*t3547*t301;
	double t3660 = t288*t288;
	double t3677 = 0.1E1*t3527*t3528*t288*t301;
	double t3780 = t301*t301;
	mJ[0][0] = t1*(t349*(-0.1E1*t414*t284 - 0.1E1*t16*(0.1E1*t433*t280 + 0.1E1
		*t438*t280*t432 - 0.5*t381*t479 + 0.1E1*t495*t149 + 0.1E1*t500*t149*t494 - 0.5*t410*
		t534)) + 0.1E1*t542*t543);
	mJ[0][1] = t1*(t349*(-0.1E1*t414*t146 - 0.1E1*t16*(0.1E1*t562*t280 + 0.1E1
		*t438*t280*t561 - 0.5*t381*t602 + 0.1E1*t381*t605 + 0.1E1*t620*t149 + 0.1E1*t500*t149*
		t619 - 0.5*t410*t653 + 0.1E1*t410*t656)) + t671);
	mJ[0][2] = t1*(t349*(-0.1E1*t414*t143 - 0.1E1*t16*(0.1E1*t688*t280 + 0.1E1
		*t438*t280*t687 - 0.5*t381*t728 + 0.1E1*t381*t731 + 0.1E1*t746*t149 + 0.1E1*t500*t149*
		t745 - 0.5*t410*t779 + 0.1E1*t410*t782)) + t796);
	mJ[0][3] = t1*(t349*(0.1E1*t414*t284 - 0.1E1*t16*(0.1E1*t827*t280 + 0.1E1*
		t438*t280*t826 - 0.5*t381*t850 + 0.1E1*t879*t149 + 0.1E1*t500*t149*t878 - 0.5*t410*t896
		)) + t955);
	mJ[0][4] = t1*(t349*(0.1E1*t414*t146 - 0.1E1*t16*(0.1E1*t981*t280 + 0.1E1*
		t438*t280*t980 - 0.5*t381*t1002 + 0.1E1*t381*t1005 + 0.1E1*t1029*t149 + 0.1E1*t500*t149
		*t1028 - 0.5*t410*t1045 + 0.1E1*t410*t1048)) + t1062);
	mJ[0][5] = t1*(t349*(0.1E1*t414*t143 - 0.1E1*t16*(0.1E1*t1088*t280 + 0.1E1
		*t438*t280*t1087 - 0.5*t381*t1110 + 0.1E1*t381*t1113 + 0.1E1*t1137*t149 + 0.1E1*t500*
		t149*t1136 - 0.5*t410*t1154 + 0.1E1*t410*t1157)) + t1171);
	mJ[0][6] = t1*(-0.1E1*t1174*(0.1E1*t1185*t280 + 0.1E1*t438*t280*t1184
		- 0.5*t381*t1196) - t1206);
	mJ[0][7] = t1*(-0.1E1*t1174*(0.1E1*t1219*t280 + 0.1E1*t438*t280*t1218
		- 0.5*t381*t1230 + 0.1E1*t381*t1233) - t1241);
	mJ[0][8] = t1*(-0.1E1*t1174*(0.1E1*t1254*t280 + 0.1E1*t438*t280*t1253
		- 0.5*t381*t1265 + 0.1E1*t381*t1268) - t1276);
	mJ[0][9] = t1*(-0.1E1*t1174*(0.1E1*t1289*t149 + 0.1E1*t500*t149*t1288
		- 0.5*t410*t1300) - t1310);
	mJ[0][10] = t1*(-0.1E1*t1174*(0.1E1*t1323*t149 + 0.1E1*t500*t149*t1322
		- 0.5*t410*t1335 + 0.1E1*t410*t1338) - t1346);
	mJ[0][11] = t1*(-0.1E1*t1174*(0.1E1*t1359*t149 + 0.1E1*t500*t149*t1358
		- 0.5*t410*t1370 + 0.1E1*t410*t1373) - t1381);
	mJ[1][0] = t1*(t349*(-0.1E1*t1384*t284 - 0.1E1*t16*(0.1E1*t433*t295 +
		0.1E1*t438*t295*t432 - 0.5*t381*t1393 + 0.1E1*t381*t1396 + 0.1E1*t495*t289 + 0.1E1*t500
		*t289*t494 - 0.5*t410*t1405 + 0.1E1*t410*t1408)) + t671);
	mJ[1][1] = t1*(t349*(-0.1E1*t1384*t146 - 0.1E1*t16*(0.1E1*t562*t295 +
		0.1E1*t438*t295*t561 - 0.5*t381*t1425 + 0.1E1*t620*t289 + 0.1E1*t500*t289*t619 - 0.5*
		t410*t1433)) + 0.1E1*t542*t1441);
	mJ[1][2] = t1*(t349*(-0.1E1*t1384*t143 - 0.1E1*t16*(0.1E1*t688*t295 +
		0.1E1*t438*t295*t687 - 0.5*t381*t1453 + 0.1E1*t381*t1456 + 0.1E1*t746*t289 + 0.1E1*t500
		*t289*t745 - 0.5*t410*t1464 + 0.1E1*t410*t1467)) + t1477);
	mJ[1][3] = t1*(t349*(0.1E1*t1384*t284 - 0.1E1*t16*(0.1E1*t827*t295 + 0.1E1
		*t438*t295*t826 - 0.5*t381*t1487 + 0.1E1*t381*t1490 + 0.1E1*t879*t289 + 0.1E1*t500*t289
		*t878 - 0.5*t410*t1498 + 0.1E1*t410*t1501)) + t1510);
	mJ[1][4] = t1*(t349*(0.1E1*t1384*t146 - 0.1E1*t16*(0.1E1*t981*t295 + 0.1E1
		*t438*t295*t980 - 0.5*t381*t1520 + 0.1E1*t1029*t289 + 0.1E1*t500*t289*t1028 - 0.5*t410*
		t1528)) + t1537);
	mJ[1][5] = t1*(t349*(0.1E1*t1384*t143 - 0.1E1*t16*(0.1E1*t1088*t295 +
		0.1E1*t438*t295*t1087 - 0.5*t381*t1547 + 0.1E1*t381*t1550 + 0.1E1*t1137*t289 + 0.1E1*
		t500*t289*t1136 - 0.5*t410*t1558 + 0.1E1*t410*t1561)) + t1570);
	mJ[1][6] = t1*(-0.1E1*t1174*(0.1E1*t1185*t295 + 0.1E1*t438*t295*t1184
		- 0.5*t381*t1578 + 0.1E1*t381*t1581) - t1588);
	mJ[1][7] = t1*(-0.1E1*t1174*(0.1E1*t1219*t295 + 0.1E1*t438*t295*t1218
		- 0.5*t381*t1596) - t1603);
	mJ[1][8] = t1*(-0.1E1*t1174*(0.1E1*t1254*t295 + 0.1E1*t438*t295*t1253
		- 0.5*t381*t1611 + 0.1E1*t381*t1614) - t1621);
	mJ[1][9] = t1*(-0.1E1*t1174*(0.1E1*t1289*t289 + 0.1E1*t500*t289*t1288
		- 0.5*t410*t1629 + 0.1E1*t410*t1632) - t1639);
	mJ[1][10] = t1*(-0.1E1*t1174*(0.1E1*t1323*t289 + 0.1E1*t500*t289*t1322
		- 0.5*t410*t1647) - t1654);
	mJ[1][11] = t1*(-0.1E1*t1174*(0.1E1*t1359*t289 + 0.1E1*t500*t289*t1358
		- 0.5*t410*t1662 + 0.1E1*t410*t1665) - t1672);
	mJ[2][0] = t1*(t349*(-0.1E1*t1675*t284 - 0.1E1*t16*(0.1E1*t433*t307 +
		0.1E1*t438*t307*t432 - 0.5*t381*t1684 + 0.1E1*t381*t1687 + 0.1E1*t495*t302 + 0.1E1*t500
		*t302*t494 - 0.5*t410*t1696 + 0.1E1*t410*t1699)) + t796);
	mJ[2][1] = t1*(t349*(-0.1E1*t1675*t146 - 0.1E1*t16*(0.1E1*t562*t307 +
		0.1E1*t438*t307*t561 - 0.5*t381*t1716 + 0.1E1*t381*t1719 + 0.1E1*t620*t302 + 0.1E1*t500
		*t302*t619 - 0.5*t410*t1727 + 0.1E1*t410*t1730)) + t1477);
	mJ[2][2] = t1*(t349*(-0.1E1*t1675*t143 - 0.1E1*t16*(0.1E1*t688*t307 +
		0.1E1*t438*t307*t687 - 0.5*t381*t1747 + 0.1E1*t746*t302 + 0.1E1*t500*t302*t745 - 0.5*
		t410*t1755)) + 0.1E1*t542*t1763);
	mJ[2][3] = t1*(t349*(0.1E1*t1675*t284 - 0.1E1*t16*(0.1E1*t827*t307 + 0.1E1
		*t438*t307*t826 - 0.5*t381*t1775 + 0.1E1*t381*t1778 + 0.1E1*t879*t302 + 0.1E1*t500*t302
		*t878 - 0.5*t410*t1786 + 0.1E1*t410*t1789)) + t1799);
	mJ[2][4] = t1*(t349*(0.1E1*t1675*t146 - 0.1E1*t16*(0.1E1*t981*t307 + 0.1E1
		*t438*t307*t980 - 0.5*t381*t1809 + 0.1E1*t381*t1812 + 0.1E1*t1029*t302 + 0.1E1*t500*
		t302*t1028 - 0.5*t410*t1820 + 0.1E1*t410*t1823)) + t1832);
	mJ[2][5] = t1*(t349*(0.1E1*t1675*t143 - 0.1E1*t16*(0.1E1*t1088*t307 +
		0.1E1*t438*t307*t1087 - 0.5*t381*t1842 + 0.1E1*t1137*t302 + 0.1E1*t500*t302*t1136 - 0.5
		*t410*t1850)) + t1859);
	mJ[2][6] = t1*(-0.1E1*t1174*(0.1E1*t1185*t307 + 0.1E1*t438*t307*t1184
		- 0.5*t381*t1867 + 0.1E1*t381*t1870) - t1877);
	mJ[2][7] = t1*(-0.1E1*t1174*(0.1E1*t1219*t307 + 0.1E1*t438*t307*t1218
		- 0.5*t381*t1885 + 0.1E1*t381*t1888) - t1895);
	mJ[2][8] = t1*(-0.1E1*t1174*(0.1E1*t1254*t307 + 0.1E1*t438*t307*t1253
		- 0.5*t381*t1903) - t1910);
	mJ[2][9] = t1*(-0.1E1*t1174*(0.1E1*t1289*t302 + 0.1E1*t500*t302*t1288
		- 0.5*t410*t1918 + 0.1E1*t410*t1921) - t1928);
	mJ[2][10] = t1*(-0.1E1*t1174*(0.1E1*t1323*t302 + 0.1E1*t500*t302*t1322
		- 0.5*t410*t1936 + 0.1E1*t410*t1939) - t1946);
	mJ[2][11] = t1*(-0.1E1*t1174*(0.1E1*t1359*t302 + 0.1E1*t500*t302*t1358
		- 0.5*t410*t1954) - t1961);
	mJ[3][0] = t1*(t349*(-0.1E1*t1964*t284 - 0.1E1*t16*(0.1E1*t1996*t280 +
		0.1E1*t2001*t280*t1995 - 0.5*t924*t479 + 0.1E1*t2036*t149 + 0.1E1*t2041*t149*t2035
		- 0.5*t950*t534)) + t955);
	mJ[3][1] = t1*(t349*(-0.1E1*t1964*t146 - 0.1E1*t16*(0.1E1*t2079*t280 +
		0.1E1*t2001*t280*t2078 - 0.5*t924*t602 + 0.1E1*t924*t605 + 0.1E1*t2112*t149 + 0.1E1*
		t2041*t149*t2111 - 0.5*t950*t653 + 0.1E1*t950*t656)) + t1510);
	mJ[3][2] = t1*(t349*(-0.1E1*t1964*t143 - 0.1E1*t16*(0.1E1*t2154*t280 +
		0.1E1*t2001*t280*t2153 - 0.5*t924*t728 + 0.1E1*t924*t731 + 0.1E1*t2187*t149 + 0.1E1*
		t2041*t149*t2186 - 0.5*t950*t779 + 0.1E1*t950*t782)) + t1799);
	mJ[3][3] = t1*(t349*(0.1E1*t1964*t284 - 0.1E1*t16*(0.1E1*t2216*t280 +
		0.1E1*t2001*t280*t2215 - 0.5*t924*t850 + 0.1E1*t2234*t149 + 0.1E1*t2041*t149*t2233
		- 0.5*t950*t896)) + 0.1E1*t542*t2247);
	mJ[3][4] = t1*(t349*(0.1E1*t1964*t146 - 0.1E1*t16*(0.1E1*t2264*t280 +
		0.1E1*t2001*t280*t2263 - 0.5*t924*t1002 + 0.1E1*t924*t1005 + 0.1E1*t2284*t149 + 0.1E1*
		t2041*t149*t2283 - 0.5*t950*t1045 + 0.1E1*t950*t1048)) + t2301);
	mJ[3][5] = t1*(t349*(0.1E1*t1964*t143 - 0.1E1*t16*(0.1E1*t2316*t280 +
		0.1E1*t2001*t280*t2315 - 0.5*t924*t1110 + 0.1E1*t924*t1113 + 0.1E1*t2336*t149 + 0.1E1*
		t2041*t149*t2335 - 0.5*t950*t1154 + 0.1E1*t950*t1157)) + t2352);
	mJ[3][6] = t1*(-0.1E1*t1174*(0.1E1*t2365*t280 + 0.1E1*t2001*t280*t2364
		- 0.5*t924*t1196) - t2377);
	mJ[3][7] = t1*(-0.1E1*t1174*(0.1E1*t2390*t280 + 0.1E1*t2001*t280*t2389
		- 0.5*t924*t1230 + 0.1E1*t924*t1233) - t2404);
	mJ[3][8] = t1*(-0.1E1*t1174*(0.1E1*t2417*t280 + 0.1E1*t2001*t280*t2416
		- 0.5*t924*t1265 + 0.1E1*t924*t1268) - t2431);
	mJ[3][9] = t1*(-0.1E1*t1174*(0.1E1*t2444*t149 + 0.1E1*t2041*t149*t2443
		- 0.5*t950*t1300) - t2456);
	mJ[3][10] = t1*(-0.1E1*t1174*(0.1E1*t2469*t149 + 0.1E1*t2041*t149*t2468
		- 0.5*t950*t1335 + 0.1E1*t950*t1338) - t2483);
	mJ[3][11] = t1*(-0.1E1*t1174*(0.1E1*t2496*t149 + 0.1E1*t2041*t149*t2495
		- 0.5*t950*t1370 + 0.1E1*t950*t1373) - t2510);
	mJ[4][0] = t1*(t349*(-0.1E1*t2513*t284 - 0.1E1*t16*(0.1E1*t1996*t295 +
		0.1E1*t2001*t295*t1995 - 0.5*t924*t1393 + 0.1E1*t924*t1396 + 0.1E1*t2036*t289 + 0.1E1*
		t2041*t289*t2035 - 0.5*t950*t1405 + 0.1E1*t950*t1408)) + t1062);
	mJ[4][1] = t1*(t349*(-0.1E1*t2513*t146 - 0.1E1*t16*(0.1E1*t2079*t295 +
		0.1E1*t2001*t295*t2078 - 0.5*t924*t1425 + 0.1E1*t2112*t289 + 0.1E1*t2041*t289*t2111
		- 0.5*t950*t1433)) + t1537);
	mJ[4][2] = t1*(t349*(-0.1E1*t2513*t143 - 0.1E1*t16*(0.1E1*t2154*t295 +
		0.1E1*t2001*t295*t2153 - 0.5*t924*t1453 + 0.1E1*t924*t1456 + 0.1E1*t2187*t289 + 0.1E1*
		t2041*t289*t2186 - 0.5*t950*t1464 + 0.1E1*t950*t1467)) + t1832);
	mJ[4][3] = t1*(t349*(0.1E1*t2513*t284 - 0.1E1*t16*(0.1E1*t2216*t295 +
		0.1E1*t2001*t295*t2215 - 0.5*t924*t1487 + 0.1E1*t924*t1490 + 0.1E1*t2234*t289 + 0.1E1*
		t2041*t289*t2233 - 0.5*t950*t1498 + 0.1E1*t950*t1501)) + t2301);
	mJ[4][4] = t1*(t349*(0.1E1*t2513*t146 - 0.1E1*t16*(0.1E1*t2264*t295 +
		0.1E1*t2001*t295*t2263 - 0.5*t924*t1520 + 0.1E1*t2284*t289 + 0.1E1*t2041*t289*t2283
		- 0.5*t950*t1528)) + 0.1E1*t542*t2639);
	mJ[4][5] = t1*(t349*(0.1E1*t2513*t143 - 0.1E1*t16*(0.1E1*t2316*t295 +
		0.1E1*t2001*t295*t2315 - 0.5*t924*t1547 + 0.1E1*t924*t1550 + 0.1E1*t2336*t289 + 0.1E1*
		t2041*t289*t2335 - 0.5*t950*t1558 + 0.1E1*t950*t1561)) + t2671);
	mJ[4][6] = t1*(-0.1E1*t1174*(0.1E1*t2365*t295 + 0.1E1*t2001*t295*t2364
		- 0.5*t924*t1578 + 0.1E1*t924*t1581) - t2687);
	mJ[4][7] = t1*(-0.1E1*t1174*(0.1E1*t2390*t295 + 0.1E1*t2001*t295*t2389
		- 0.5*t924*t1596) - t2701);
	mJ[4][8] = t1*(-0.1E1*t1174*(0.1E1*t2417*t295 + 0.1E1*t2001*t295*t2416
		- 0.5*t924*t1611 + 0.1E1*t924*t1614) - t2717);
	mJ[4][9] = t1*(-0.1E1*t1174*(0.1E1*t2444*t289 + 0.1E1*t2041*t289*t2443
		- 0.5*t950*t1629 + 0.1E1*t950*t1632) - t2733);
	mJ[4][10] = t1*(-0.1E1*t1174*(0.1E1*t2469*t289 + 0.1E1*t2041*t289*t2468
		- 0.5*t950*t1647) - t2747);
	mJ[4][11] = t1*(-0.1E1*t1174*(0.1E1*t2496*t289 + 0.1E1*t2041*t289*t2495
		- 0.5*t950*t1662 + 0.1E1*t950*t1665) - t2763);
	mJ[5][0] = t1*(t349*(-0.1E1*t2766*t284 - 0.1E1*t16*(0.1E1*t1996*t307 +
		0.1E1*t2001*t307*t1995 - 0.5*t924*t1684 + 0.1E1*t924*t1687 + 0.1E1*t2036*t302 + 0.1E1*
		t2041*t302*t2035 - 0.5*t950*t1696 + 0.1E1*t950*t1699)) + t1171);
	mJ[5][1] = t1*(t349*(-0.1E1*t2766*t146 - 0.1E1*t16*(0.1E1*t2079*t307 +
		0.1E1*t2001*t307*t2078 - 0.5*t924*t1716 + 0.1E1*t924*t1719 + 0.1E1*t2112*t302 + 0.1E1*
		t2041*t302*t2111 - 0.5*t950*t1727 + 0.1E1*t950*t1730)) + t1570);
	mJ[5][2] = t1*(t349*(-0.1E1*t2766*t143 - 0.1E1*t16*(0.1E1*t2154*t307 +
		0.1E1*t2001*t307*t2153 - 0.5*t924*t1747 + 0.1E1*t2187*t302 + 0.1E1*t2041*t302*t2186
		- 0.5*t950*t1755)) + t1859);
	mJ[5][3] = t1*(t349*(0.1E1*t2766*t284 - 0.1E1*t16*(0.1E1*t2216*t307 +
		0.1E1*t2001*t307*t2215 - 0.5*t924*t1775 + 0.1E1*t924*t1778 + 0.1E1*t2234*t302 + 0.1E1*
		t2041*t302*t2233 - 0.5*t950*t1786 + 0.1E1*t950*t1789)) + t2352);
	mJ[5][4] = t1*(t349*(0.1E1*t2766*t146 - 0.1E1*t16*(0.1E1*t2264*t307 +
		0.1E1*t2001*t307*t2263 - 0.5*t924*t1809 + 0.1E1*t924*t1812 + 0.1E1*t2284*t302 + 0.1E1*
		t2041*t302*t2283 - 0.5*t950*t1820 + 0.1E1*t950*t1823)) + t2671);
	mJ[5][5] = t1*(t349*(0.1E1*t2766*t143 - 0.1E1*t16*(0.1E1*t2316*t307 +
		0.1E1*t2001*t307*t2315 - 0.5*t924*t1842 + 0.1E1*t2336*t302 + 0.1E1*t2041*t302*t2335
		- 0.5*t950*t1850)) + 0.1E1*t542*t2919);
	mJ[5][6] = t1*(-0.1E1*t1174*(0.1E1*t2365*t307 + 0.1E1*t2001*t307*t2364
		- 0.5*t924*t1867 + 0.1E1*t924*t1870) - t2938);
	mJ[5][7] = t1*(-0.1E1*t1174*(0.1E1*t2390*t307 + 0.1E1*t2001*t307*t2389
		- 0.5*t924*t1885 + 0.1E1*t924*t1888) - t2954);
	mJ[5][8] = t1*(-0.1E1*t1174*(0.1E1*t2417*t307 + 0.1E1*t2001*t307*t2416
		- 0.5*t924*t1903) - t2968);
	mJ[5][9] = t1*(-0.1E1*t1174*(0.1E1*t2444*t302 + 0.1E1*t2041*t302*t2443
		- 0.5*t950*t1918 + 0.1E1*t950*t1921) - t2984);
	mJ[5][10] = t1*(-0.1E1*t1174*(0.1E1*t2469*t302 + 0.1E1*t2041*t302*t2468
		- 0.5*t950*t1936 + 0.1E1*t950*t1939) - t3000);
	mJ[5][11] = t1*(-0.1E1*t1174*(0.1E1*t2496*t302 + 0.1E1*t2041*t302*t2495
		- 0.5*t950*t1954) - t3014);
	mJ[6][0] = t1*(t349*(0.1E1*t3017*t280*t284 + 0.1E1*t3024*t280 - 0.5*t3027*
		t479) - t1206);
	mJ[6][1] = t1*(t349*(0.1E1*t3017*t280*t146 + 0.1E1*t3040*t280 - 0.5*t3027*
		t602 + 0.1E1*t3027*t605) - t1588);
	mJ[6][2] = t1*(t349*(0.1E1*t3017*t280*t143 + 0.1E1*t3057*t280 - 0.5*t3027*
		t728 + 0.1E1*t3027*t731) - t1877);
	mJ[6][3] = t1*(t349*(-0.1E1*t3017*t280*t284 + 0.1E1*t3074*t280 - 0.5*t3027
		*t850) - t2377);
	mJ[6][4] = t1*(t349*(-0.1E1*t3017*t280*t146 + 0.1E1*t3089*t280 - 0.5*t3027
		*t1002 + 0.1E1*t3027*t1005) - t2687);
	mJ[6][5] = t1*(t349*(-0.1E1*t3017*t280*t143 + 0.1E1*t3106*t280 - 0.5*t3027
		*t1110 + 0.1E1*t3027*t1113) - t2938);
	mJ[6][6] = t1*(t349*(0.1E1*t3120*t280 - 0.5*t3027*t1196) + 0.1E1*t3128*
		t3129*t3130);
	mJ[6][7] = t1*(t349*(0.1E1*t3139*t280 - 0.5*t3027*t1230 + 0.1E1*t3027*
		t1233) + t3151);
	mJ[6][8] = t1*(t349*(0.1E1*t3157*t280 - 0.5*t3027*t1265 + 0.1E1*t3027*
		t1268) + t3168);
	mJ[6][9] = t3175;
	mJ[6][10] = t3178;
	mJ[6][11] = t3181;
	mJ[7][0] = t1*(t349*(0.1E1*t3017*t295*t284 + 0.1E1*t3024*t295 - 0.5*t3027*
		t1393 + 0.1E1*t3027*t1396) - t1241);
	mJ[7][1] = t1*(t349*(0.1E1*t3017*t295*t146 + 0.1E1*t3040*t295 - 0.5*t3027*
		t1425) - t1603);
	mJ[7][2] = t1*(t349*(0.1E1*t3017*t295*t143 + 0.1E1*t3057*t295 - 0.5*t3027*
		t1453 + 0.1E1*t3027*t1456) - t1895);
	mJ[7][3] = t1*(t349*(-0.1E1*t3017*t295*t284 + 0.1E1*t3074*t295 - 0.5*t3027
		*t1487 + 0.1E1*t3027*t1490) - t2404);
	mJ[7][4] = t1*(t349*(-0.1E1*t3017*t295*t146 + 0.1E1*t3089*t295 - 0.5*t3027
		*t1520) - t2701);
	mJ[7][5] = t1*(t349*(-0.1E1*t3017*t295*t143 + 0.1E1*t3106*t295 - 0.5*t3027
		*t1547 + 0.1E1*t3027*t1550) - t2954);
	mJ[7][6] = t1*(t349*(0.1E1*t3120*t295 - 0.5*t3027*t1578 + 0.1E1*t3027*
		t1581) + t3151);
	mJ[7][7] = t1*(t349*(0.1E1*t3139*t295 - 0.5*t3027*t1596) + 0.1E1*t3128*
		t3129*t3272);
	mJ[7][8] = t1*(t349*(0.1E1*t3157*t295 - 0.5*t3027*t1611 + 0.1E1*t3027*
		t1614) + t3289);
	mJ[7][9] = t3294;
	mJ[7][10] = t3297;
	mJ[7][11] = t3300;
	mJ[8][0] = t1*(t349*(0.1E1*t3017*t307*t284 + 0.1E1*t3024*t307 - 0.5*t3027*
		t1684 + 0.1E1*t3027*t1687) - t1276);
	mJ[8][1] = t1*(t349*(0.1E1*t3017*t307*t146 + 0.1E1*t3040*t307 - 0.5*t3027*
		t1716 + 0.1E1*t3027*t1719) - t1621);
	mJ[8][2] = t1*(t349*(0.1E1*t3017*t307*t143 + 0.1E1*t3057*t307 - 0.5*t3027*
		t1747) - t1910);
	mJ[8][3] = t1*(t349*(-0.1E1*t3017*t307*t284 + 0.1E1*t3074*t307 - 0.5*t3027
		*t1775 + 0.1E1*t3027*t1778) - t2431);
	mJ[8][4] = t1*(t349*(-0.1E1*t3017*t307*t146 + 0.1E1*t3089*t307 - 0.5*t3027
		*t1809 + 0.1E1*t3027*t1812) - t2717);
	mJ[8][5] = t1*(t349*(-0.1E1*t3017*t307*t143 + 0.1E1*t3106*t307 - 0.5*t3027
		*t1842) - t2968);
	mJ[8][6] = t1*(t349*(0.1E1*t3120*t307 - 0.5*t3027*t1867 + 0.1E1*t3027*
		t1870) + t3168);
	mJ[8][7] = t1*(t349*(0.1E1*t3139*t307 - 0.5*t3027*t1885 + 0.1E1*t3027*
		t1888) + t3289);
	mJ[8][8] = t1*(t349*(0.1E1*t3157*t307 - 0.5*t3027*t1903) + 0.1E1*t3128*
		t3129*t3401);
	mJ[8][9] = t3409;
	mJ[8][10] = t3412;
	mJ[8][11] = t3415;
	mJ[9][0] = t1*(t349*(0.1E1*t3416*t149*t284 + 0.1E1*t3423*t149 - 0.5*t3426*
		t534) - t1310);
	mJ[9][1] = t1*(t349*(0.1E1*t3416*t149*t146 + 0.1E1*t3439*t149 - 0.5*t3426*
		t653 + 0.1E1*t3426*t656) - t1639);
	mJ[9][2] = t1*(t349*(0.1E1*t3416*t149*t143 + 0.1E1*t3456*t149 - 0.5*t3426*
		t779 + 0.1E1*t3426*t782) - t1928);
	mJ[9][3] = t1*(t349*(-0.1E1*t3416*t149*t284 + 0.1E1*t3473*t149 - 0.5*t3426
		*t896) - t2456);
	mJ[9][4] = t1*(t349*(-0.1E1*t3416*t149*t146 + 0.1E1*t3488*t149 - 0.5*t3426
		*t1045 + 0.1E1*t3426*t1048) - t2733);
	mJ[9][5] = t1*(t349*(-0.1E1*t3416*t149*t143 + 0.1E1*t3505*t149 - 0.5*t3426
		*t1154 + 0.1E1*t3426*t1157) - t2984);
	mJ[9][6] = t3175;
	mJ[9][7] = t3294;
	mJ[9][8] = t3409;
	mJ[9][9] = t1*(t349*(0.1E1*t3519*t149 - 0.5*t3426*t1300) + 0.1E1*t3527*
		t3528*t3529);
	mJ[9][10] = t1*(t349*(0.1E1*t3538*t149 - 0.5*t3426*t1335 + 0.1E1*t3426*
		t1338) + t3550);
	mJ[9][11] = t1*(t349*(0.1E1*t3556*t149 - 0.5*t3426*t1370 + 0.1E1*t3426*
		t1373) + t3567);
	mJ[10][0] = t1*(t349*(0.1E1*t3416*t289*t284 + 0.1E1*t3423*t289 - 0.5*t3426
		*t1405 + 0.1E1*t3426*t1408) - t1346);
	mJ[10][1] = t1*(t349*(0.1E1*t3416*t289*t146 + 0.1E1*t3439*t289 - 0.5*t3426
		*t1433) - t1654);
	mJ[10][2] = t1*(t349*(0.1E1*t3416*t289*t143 + 0.1E1*t3456*t289 - 0.5*t3426
		*t1464 + 0.1E1*t3426*t1467) - t1946);
	mJ[10][3] = t1*(t349*(-0.1E1*t3416*t289*t284 + 0.1E1*t3473*t289 - 0.5*
		t3426*t1498 + 0.1E1*t3426*t1501) - t2483);
	mJ[10][4] = t1*(t349*(-0.1E1*t3416*t289*t146 + 0.1E1*t3488*t289 - 0.5*
		t3426*t1528) - t2747);
	mJ[10][5] = t1*(t349*(-0.1E1*t3416*t289*t143 + 0.1E1*t3505*t289 - 0.5*
		t3426*t1558 + 0.1E1*t3426*t1561) - t3000);
	mJ[10][6] = t3178;
	mJ[10][7] = t3297;
	mJ[10][8] = t3412;
	mJ[10][9] = t1*(t349*(0.1E1*t3519*t289 - 0.5*t3426*t1629 + 0.1E1*t3426*
		t1632) + t3550);
	mJ[10][10] = t1*(t349*(0.1E1*t3538*t289 - 0.5*t3426*t1647) + 0.1E1*t3527*
		t3528*t3660);
	mJ[10][11] = t1*(t349*(0.1E1*t3556*t289 - 0.5*t3426*t1662 + 0.1E1*t3426*
		t1665) + t3677);
	mJ[11][0] = t1*(t349*(0.1E1*t3416*t302*t284 + 0.1E1*t3423*t302 - 0.5*t3426
		*t1696 + 0.1E1*t3426*t1699) - t1381);
	mJ[11][1] = t1*(t349*(0.1E1*t3416*t302*t146 + 0.1E1*t3439*t302 - 0.5*t3426
		*t1727 + 0.1E1*t3426*t1730) - t1672);
	mJ[11][2] = t1*(t349*(0.1E1*t3416*t302*t143 + 0.1E1*t3456*t302 - 0.5*t3426
		*t1755) - t1961);
	mJ[11][3] = t1*(t349*(-0.1E1*t3416*t302*t284 + 0.1E1*t3473*t302 - 0.5*
		t3426*t1786 + 0.1E1*t3426*t1789) - t2510);
	mJ[11][4] = t1*(t349*(-0.1E1*t3416*t302*t146 + 0.1E1*t3488*t302 - 0.5*
		t3426*t1820 + 0.1E1*t3426*t1823) - t2763);
	mJ[11][5] = t1*(t349*(-0.1E1*t3416*t302*t143 + 0.1E1*t3505*t302 - 0.5*
		t3426*t1850) - t3014);
	mJ[11][6] = t3181;
	mJ[11][7] = t3300;
	mJ[11][8] = t3415;
	mJ[11][9] = t1*(t349*(0.1E1*t3519*t302 - 0.5*t3426*t1918 + 0.1E1*t3426*
		t1921) + t3567);
	mJ[11][10] = t1*(t349*(0.1E1*t3538*t302 - 0.5*t3426*t1936 + 0.1E1*t3426*
		t1939) + t3677);
	mJ[11][11] = t1*(t349*(0.1E1*t3556*t302 - 0.5*t3426*t1954) + 0.1E1*t3527*
		t3528*t3780);

	//for (int i = 0; i < 12; ++i)
	//	for (int j = 0; j < 12; ++j)
	//		if (!FloatHelper::IsFinite(mJ[i][j]))
	//			qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 12; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = -mJ[i][j];
}