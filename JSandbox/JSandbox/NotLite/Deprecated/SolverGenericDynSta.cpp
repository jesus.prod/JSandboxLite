/*=====================================================================================*/
/*! 
\file		SolverGenericDynSta.cpp
\author		jesusprod
\brief		Implementation of SolverGenericDynSta.h
 */
/*=====================================================================================*/

#include <JSandbox/SolverGenericDynSta.h>

#include <JSandbox/BConditionFixed.h>

SolverGenericDynSta::SolverGenericDynSta(SolidModel *pSM) : PhysSolver(pSM) 
{ 
	this->setIsStatic(false);
}

SolverGenericDynSta::~SolverGenericDynSta()
{ 
	// Nothing to do here
}

Real SolverGenericDynSta::getPotential(const VectorXd& vx, const VectorXd& vv)
{
	// Compute U(x) = - 1/2 * h^2 * a^T*M*a - V_int(x) - V_ext(x)
	// Here, V_int(x) and V_ext(x) are escalar functions such that
	// dV_int/dx = f_int and dV_ext/dx = f_ext. This potentia can 
	// serve as a function to minimize in order to solve the 
	// dynamic/quasistatic problems:
	//
	// dU(x)/dx = f_int + f_ext - Ma = 0

	Real potential = 0;
	potential -= this->m_pSM->getEnergy();
	potential -= this->getBoundaryEnergy(vx, vx);

	if (!this->m_isStatic) // Add dynamic contribution
	{
		Real dt2 = this->m_dt*this->m_dt;
		VectorXd va = (vx - this->m_vx)/dt2 - this->m_vv/this->m_dt;
		potential -= 0.5 * va.dot((va.cwiseProduct(this->m_vm)));
	}

	return potential;
}

void SolverGenericDynSta::getResidual(const VectorXd& vx, const VectorXd& vv, VectorXd& vrhs)
{
	m_updateResTimer.restart();

	vrhs.resize(this->m_pSM->getRawDOFNumber());
	vrhs.setZero(); // From zero vector

	this->m_pSM->addForce(vrhs, NULL);
	this->addBoundaryForce(vx, vv, vrhs);
	//this->m_pSM->addForce(vrhs, &this->m_fixedStencil);

	if (!this->m_isStatic)
	{
		Real dt2 = this->m_dt*this->m_dt;
		VectorXd va = (vx - this->m_vx)/dt2 - this->m_vv/this->m_dt;
		vrhs -= va.cwiseProduct(this->m_vm); // Add dynamic part M*a
	}

	this->constrainBoundary(vrhs);

	iVector fixDOF;
	BConditionFixed bcFixed(this->m_pSM);
	const bVector& vas = m_pSM->getActiveStencil();
	for (int iDOF = 0; iDOF < (int)vas.size(); ++iDOF)
		if (!vas[iDOF]) // Add inactive DOF to vector
			fixDOF.push_back(iDOF);

	bcFixed.constrain(vrhs); // Constrain
	
	m_updateResTimer.stopStoreLog();
}

void SolverGenericDynSta::getMatrix(const VectorXd& vx, const VectorXd& vv, SparseMatrixXd& mA)
{
	assert(mA.cols() == this->m_N && mA.rows() == this->m_N);

	this->m_updateMatTimer.restart();

	this->m_vA.clear();
	this->m_pSM->addJacobian(this->m_vA, NULL);
	this->addBoundaryJacobian(vx, vv, this->m_vA);

	if (!this->m_isStatic)
	{
		Real dt2i = 1/(this->m_dt*this->m_dt);
		for (int i = 0; i < this->m_N; ++i) // Add inertia contribution
			this->m_vA.push_back(Triplet<Real>(i, i, -this->m_vm(i)*dt2i));
	}

	this->constrainBoundary(this->m_vA);

	iVector fixDOF;
	BConditionFixed bcFixed(this->m_pSM);
	const bVector& vas = m_pSM->getActiveStencil();
	for (int iDOF = 0; iDOF < (int)vas.size(); ++iDOF)
		if (!vas[iDOF]) // Add inactive DOF to vector
			fixDOF.push_back(iDOF);

	bcFixed.constrain(this->m_vA); // Constrain

	mA.setFromTriplets(this->m_vA.begin(), this->m_vA.end());
	mA.makeCompressed(); // Sym. lower triangular & compressed

	this->m_updateMatTimer.stopStoreLog();
}