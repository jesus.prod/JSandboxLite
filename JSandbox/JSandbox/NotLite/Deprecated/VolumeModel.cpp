/*=====================================================================================*/
/*! 
\file		VolumeModel.cpp
\author		jesusprod
\brief		Implementation of VolumeModel.h
 */
/*=====================================================================================*/

#include <JSandbox/VolumeModel.h>
#include <JSandbox/FEMElement.h>
#include <JSandbox/FEMElementLinear.h>
#include <JSandbox/FEMElementLinearCorot.h>
#include <JSandbox/FEMElementMooneyRivlin.h>
#include <JSandbox/FEMElementNeoHookean.h>
#include <JSandbox/FEMElementOgden.h>
#include <JSandbox/FEMElementStVK.h>

void VolumeModel::configureDeformationModel(Model CM)
{
	this->m_DM = CM;
}

void VolumeModel::configureDiscretization(const dVector& vp, const iVector& vi, VolumeModel::Element E)
{
	switch (E)
	{
	case VolumeModel::Tet1:
		assert((int) vp.size() % 3 == 0);
		assert((int) vi.size() % 4 == 0);
		this->m_vx = vp;
		this->m_vind = vi;
		this->m_DE = E;
		break;

	case VolumeModel::Hex3:
		assert((int) vp.size() % 3 == 0);
		assert((int) vi.size() % 3 == 0);
		this->m_vx = vp;
		this->m_vind = vi;
		this->m_DE = E;
		break;

	default: assert(false); // Should not happen
	}
}

void VolumeModel::initializeElements()
{
	// Initialize discretization

	switch (this->m_DE)
	{
	case FEMElement::Tet1:
		initializeTetDiscretization();
		break;

	case FEMElement::Hex3: 
		initializeHexDiscretization();
		break;

	default: assert(false);
	}

	// Initialize state vectors

	this->m_vx.resize(this->m_nrawDOF);
	this->m_vv.resize(this->m_nrawDOF);
	this->m_vX.resize(this->m_nrawDOF);

	for (int i = 0; i < this->m_nrawDOF; ++i)
	{
		//this->m_vx(i) = this->m_vx[i];
		//this->m_vX(i) = this->m_vx[i];
	}
	this->m_vv.setZero();
}

void VolumeModel::initializeTetDiscretization()
{
	int nv = (int) this->m_vx.size() / 3;
	int ne = (int) this->m_vind.size() / 4;

	this->m_numEle = ne;
	this->m_nrawDOF = 3*nv;
	this->m_vpEles.reserve(ne);
	for (int i = 0; i < ne; ++i)
	{
		SolidElement *pEle = NULL;

		// Create element
		switch (this->m_DM)
		{
			case FEMLinear: pEle = new FEMElementLinear(4, 3, FEMElement::Tet1); break;
			case FEMLinearCorot: pEle = new FEMElementLinearCorot(4, 3, FEMElement::Tet1); break;
			//case MooneyRivlin: pEle = new FEMElementMooneyRivlin(4, 3, FEMElement::Tet1); break;
			//case NeoHookean: pEle = new FEMElementNeoHookean(4, 3, FEMElement::Tet1); break;
			//case Ogden: pEle = new FEMElementOgden(4, 3, FEMElement::Tet1); break;
			case FEMStVK: pEle = new FEMElementStVK(4, 3, FEMElement::Tet1); break;
		}

		// Set indices
		int offseti = 4*i;
		iVector vinds(12);
		for (int j = 0; j < 4; ++j)
		{
			int offsetj = 3*this->m_vind[offseti+j];
			vinds[3*j] = offsetj;
			vinds[3*j + 1] = offsetj + 1;
			vinds[3*j + 2] = offsetj + 2;
		}

		pEle->setIndices(vinds);

		// Add element to elements vector
		this->m_vpEles.push_back(pEle);
	}
}

void VolumeModel::initializeHexDiscretization()
{
	// TODO: Discretization not available yet
}