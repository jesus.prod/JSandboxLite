/*=====================================================================================*/
/*!
\file		EmbeddedPartialRodStretchElement3D.cpp
\author		jesusprod
\brief		Implementation of EmbeddedPartialRodStretchElement3D.h
*/
/*=====================================================================================*/

#include <JSandbox/EmbeddedPartialRodStretchElement3D.h>


void EmbeddedPartialRodStretchElement3D::updateRest(const VectorXd& vX)
{
	if (this->m_numDim0 == 3)
	{
		vector<Vector3d> vp;
		this->getEmbbededPositions3D0(vX, vp);
		const Vector3d& x0 = vp[0];
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[3], vX);
		Vector3d e0 = x1 - x0;
		this->m_L0 = e0.norm();
	}
	else assert(false);

	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double d = this->m_vpmat[0]->getDensity();
	double nodeMass = 0.5*d*m_L0*w*h*M_PI;

	int count = 0;
	for (int i = 0; i < this->m_numNodeSim; ++i)
		for (int j = 0; j < this->m_numDim; ++j)
			this->m_vmass[count++] = nodeMass*this->m_vembCor[i];
}

void EmbeddedPartialRodStretchElement3D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x1(3);
	vector<double> x2(3);

	this->getEmbbededPositions3D(vx, vp);
	Vector3d vx2 = getBlock3x1(this->m_vnodeIdx[3], vx);

	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vp[0](i);
		x2[i] = vx2(i);
	}
	
	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = computeB();

//#include "../../Maple/Code/RodEdge3DEnergy.mcg"

	//this->m_energy = t21;
}

void EmbeddedPartialRodStretchElement3D::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x1(3);
	vector<double> x2(3);

	this->getEmbbededPositions3D(vx, vp);
	Vector3d vx2 = getBlock3x1(this->m_vnodeIdx[3], vx);

	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vp[0](i);
		x2[i] = vx2(i);
	}

	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = computeB();

	dVector vf6(6);

//#include "../../Maple/Code/RodEdge3DForce.mcg"

	for (int i = 0; i < 6; ++i)
		if (!isfinite(vf6[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);

	this->addForcesToEmbedding(vf6);
}

void EmbeddedPartialRodStretchElement3D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<Vector3d> vp;
	vector<double> x1(3);
	vector<double> x2(3);

	this->getEmbbededPositions3D(vx, vp);
	Vector3d vx2 = getBlock3x1(this->m_vnodeIdx[3], vx);

	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vp[0](i);
		x2[i] = vx2(i);
	}

	//double L = this->m_L0 + this->m_L0*this->m_preStrain;

	double L = this->m_L0;

	double kS = computeB();

	vector<dVector> mJ(6);
	for (int i = 0; i < 6; ++i)
		mJ[i].resize(6); // Init.

//#include "../../Maple/Code/RodEdge3DJacobian.mcg"

	for (int i = 0; i < 6; ++i)
		for (int j = 0; j < 6; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	this->addJacobianToEmbedding(mJ);
}

const dVector& EmbeddedPartialRodStretchElement3D::getEmbeddingCoordinates() const
{
	return this->m_vembCor;
}

void EmbeddedPartialRodStretchElement3D::setEmbCoordinates(const dVector& vcor)
{
	assert((int)vcor.size() == this->m_numNodeSim);
	this->m_vembCor = vcor; // Barycentric or so

	this->m_mDxDv.resize(this->m_numDim*this->m_numEmbNodes + 3, this->m_numDim*this->m_numNodeSim);
	this->m_mDxDv.setZero();

	int count = 0;

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int offseti = this->m_numDim*i;

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int offsetj = this->m_numDim*i*this->m_numEmbCoord + this->m_numDim*j;

			for (int k = 0; k < 3; ++k) // Identity matrix scaled by coordinate
				this->m_mDxDv(offseti + k, offsetj + k) = this->m_vembCor[count];

			count++;
		}
	}

	addBlock3x3From(3, 9, Matrix3d::Identity(), this->m_mDxDv, false);
}

void EmbeddedPartialRodStretchElement3D::addForcesToEmbedding(const dVector& vembFVal)
{
	this->m_vfVal = this->m_mDxDv.transpose()*toEigen(vembFVal);
}

void EmbeddedPartialRodStretchElement3D::addJacobianToEmbedding(const vector<dVector>& vembJVal)
{
	int n = this->m_numDim*this->m_numEmbNodes + 3;

	MatrixXd membJVal(n, n);
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < n; ++j)
			membJVal(i, j) = vembJVal[i][j];

	MatrixXd mJ = this->m_mDxDv.transpose()*membJVal*this->m_mDxDv;
	int N = this->m_numDim*this->m_numEmbCoord*this->m_numEmbNodes;

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
		{
		if (i == j)
			continue;

		if (this->m_vidx[i] == this->m_vidx[j])
		{
			mJ(i, i) += mJ(i, j);
			mJ(i, j) = 0.0; // Sum
		}
		}

	int count = 0;
	for (int i = 0; i < N; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ(i, j);
}


void EmbeddedPartialRodStretchElement3D::getEmbbededPositions3D0(const VectorXd& vx, vector<Vector3d>& vp)
{
	vp.clear();

	dVector vxSTL = toSTL(vx);

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int nodeOffset = i*this->m_numEmbCoord;

		Vector3d p;
		p.setZero();

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int idxNode = nodeOffset + j;
			Vector3d node = getBlock3x1(this->m_vnodeIdx0[idxNode], vx);
			p += node * this->m_vembCor[idxNode]; // Coordinate weights
		}

		vp.push_back(p);
	}
}

void EmbeddedPartialRodStretchElement3D::getEmbbededPositions3D(const VectorXd& vx, vector<Vector3d>& vp)
{
	vp.clear();

	dVector vxSTL = toSTL(vx);

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int nodeOffset = i*this->m_numEmbCoord;

		Vector3d p;
		p.setZero();

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int idxNode = nodeOffset + j;
			Vector3d node = getBlock3x1(this->m_vnodeIdx[idxNode], vx);
			p += node * this->m_vembCor[idxNode]; // Coordinate weights
		}

		vp.push_back(p);
	}
}


void EmbeddedPartialRodStretchElement3D::getEmbbededPositions2D0(const VectorXd& vx, vector<Vector2d>& vp)
{
	vp.clear();

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int nodeOffset = i*this->m_numEmbCoord;

		Vector2d p;
		p.setZero();

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int idxNode = nodeOffset + j;
			Vector2d node = getBlock2x1(this->m_vnodeIdx0[idxNode], vx);
			p += node * this->m_vembCor[idxNode]; // Coordinate weights
		}

		vp.push_back(p);
	}
}

void EmbeddedPartialRodStretchElement3D::getEmbbededPositions2D(const VectorXd& vx, vector<Vector2d>& vp)
{
	vp.clear();

	for (int i = 0; i < this->m_numEmbNodes; ++i)
	{
		int nodeOffset = i*this->m_numEmbCoord;

		Vector2d p;
		p.setZero();

		for (int j = 0; j < this->m_numEmbCoord; ++j)
		{
			int idxNode = nodeOffset + j;
			Vector2d node = getBlock2x1(this->m_vnodeIdx[idxNode], vx);
			p += node * this->m_vembCor[idxNode]; // Coordinate weights
		}

		vp.push_back(p);
	}
}

double EmbeddedPartialRodStretchElement3D::computeB() const
{
	double Y = this->m_vpmat[0]->getYoung();
	double w = this->m_vpmat[0]->getWRadius();
	double h = this->m_vpmat[0]->getHRadius();
	double A = M_PI * w * h;
	return A*Y;
}