/*=====================================================================================*/
/*! 
\file		DSAreaElement.cpp
\author		jesusprod
\brief		Implementation of DSAreaElement.h
*/
/*=====================================================================================*/

#include <JSandbox/DSAreaElement.h>
 
DSAreaElement::DSAreaElement(int dim0) : NodalSolidElement(3, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);
}

DSAreaElement::~DSAreaElement() 
{
	// Nothing to do here
}

void DSAreaElement::updateRest(const VectorXd& vX)
{
	if (this->m_numDim0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
		Vector3d x2 = getBlock3x1(this->m_vnodeIdx0[2], vX);
		this->m_A0 = 0.5*(x2 - x0).cross(x1 - x0).norm();
		assert(this->m_A0 >= 0.0);
	}
	else if (this->m_numDim0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
		Vector2d x2 = getBlock2x1(this->m_vnodeIdx0[2], vX);
		Vector3d x03D(x0.x(), 0.0, x0.y());
		Vector3d x13D(x1.x(), 0.0, x1.y());
		Vector3d x23D(x2.x(), 0.0, x2.y());
		this->m_A0 = 0.5*(x23D - x03D).cross(x13D - x03D).norm();
		assert(this->m_A0 >= 0.0);
	}
	else assert(false);

	Real nodeMass = this->m_A0*this->m_vpmat[0]->getDensity()/3.0;
	for (int i = 0; i < this->m_nrawDOF; ++i)
		this->m_vmass[i] = nodeMass;
}

void DSAreaElement::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3+i]);
		x3[i] = vx(this->m_vidx[6+i]);
	}
	double A0 = this->m_A0*this->m_preStrain;
	double kA = m_vpmat[0]->getAreaK();

//#include "../../Maple/Code/DSAreaEnergy.mcg"

	//this->m_energy = t137;
}

void DSAreaElement::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3+i]);
		x3[i] = vx(this->m_vidx[6+i]);
	}
	double A0 = this->m_A0*this->m_preStrain;
	double kA = m_vpmat[0]->getAreaK();

	double vf9[9];
	
//#include "../../Maple/Code/DSAreaForce.mcg"

	for(int i = 0; i < 9; ++i)	
		if (!isfinite(vf9[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
	
	// Store force vector

	for(int i = 0; i < 9; ++i)	
		this->m_vfVal(i) = vf9[i];
}

void DSAreaElement::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidx[i]);
		x2[i] = vx(this->m_vidx[3+i]);
		x3[i] = vx(this->m_vidx[6+i]);
	}
	double A0 = this->m_A0*this->m_preStrain;
	double kA = m_vpmat[0]->getAreaK();
	double mJ[9][9];

//#include "../../Maple/Code/DSAreaJacobian.mcg"

	for(int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJ[i][j])) 
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ[i][j];
}