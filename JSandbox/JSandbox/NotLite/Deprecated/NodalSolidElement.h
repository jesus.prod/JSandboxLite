/*=====================================================================================*/
/*! 
\file		NodalSolidElement.h
\author		jesusprod
\brief		Basic abstract implementation of SolidElement to inherit from.
			All (purely) nodal solid elements should inherit from this abstract 
			class. It holds information about number of nodes and dimension n.
 */
/*=====================================================================================*/

#ifndef NODAL_SOLID_ELEMENT_H
#define NODAL_SOLID_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/SolidElement.h>
#include <JSandbox/SolidMaterial.h>

class NodalSolidElement : public SolidElement
{
public:
	NodalSolidElement(int n, int d, int d0) : SolidElement(n*d, n*d0)
	{
		assert(n > 1);
		assert(d > 0); 
		assert(d0 > 0); 

		this->m_numDim = d;
		this->m_numDim0 = d0;
		this->m_numNodeSim = n;
		  
		this->m_vnodeIdx0.resize(n);
		this->m_vnodeIdx.resize(n);
	}

	virtual ~NodalSolidElement() { }

	virtual int getNumNodeSim() const { return m_numNodeSim; }

	virtual int getNumDim() const { return m_numDim; }
	virtual int getNumDim0() const { return m_numDim0; }

	// Wrapper for making easier to use it, nodal indices 
	// should be specified and not the Degrees-of-Freedom

	virtual const iVector& getNodeIndices() const;
	virtual void setNodeIndices(const iVector& vi);

	virtual const iVector& getNodeIndices0() const;
	virtual void setNodeIndices0(const iVector& vi);

	virtual void updateEnergy(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void updateForce(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void updateJacobian(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void updateRest(const VectorXd& vX) = 0;
	
protected:

	int m_numNodeSim;

	int m_numDim;
	int m_numDim0;

	iVector m_vnodeIdx;
	iVector m_vnodeIdx0;

};

#endif