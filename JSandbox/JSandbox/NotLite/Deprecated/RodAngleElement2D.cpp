/*=====================================================================================*/
/*!
\file		RodAngleElement2D.cpp
\author		jesusprod
\brief		Implementation of RodAngleElement2D.h
*/
/*=====================================================================================*/

#include <JSandbox/RodAngleElement2D.h>

RodAngleElement2D::RodAngleElement2D() : NodalSolidElement(3, 2, 2)
{
	// Nothign to do here
}

RodAngleElement2D::~RodAngleElement2D()
{
	// Nothing to do here
}

void RodAngleElement2D::updateRest(const VectorXd& vX)
{
	Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
	Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
	Vector2d x2 = getBlock2x1(this->m_vnodeIdx0[2], vX);
	Vector2d x01 = x1 - x0;
	Vector2d x12 = x2 - x1;
	Real x01norm = x01.norm();
	Real x12norm = x12.norm(); 
	this->m_phi0 = atan2(x12[1] - x01[1],
						 x12[0] - x01[0]);
	this->m_vL0 = 0.5*(x01norm + x12norm);
}

void RodAngleElement2D::updateEnergy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x0(2);
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x0[i] = vx(this->m_vidx[i]);
		x1[i] = vx(this->m_vidx[2 + i]);
		x2[i] = vx(this->m_vidx[4 + i]);
	}

	Real C = (((x1[0] - x0[0])*(x2[1] - x1[1]) - (x1[1] - x0[1])*(x2[0] - x1[0])) < 0.0) ? -1.0 : 1.0; // Convexity

	double vL0 = this->m_vL0;

	double phi0 = this->m_phi0  + this->m_phi0*this->m_preStrain;

	double kB = m_vpmat[0]->getBendingK();

//#include "../../Maple/Code/RodAngle2DEnergy.mcg"

	//this->m_energy = t36;

//	Vector3d x0(3);
//	Vector3d x1(3);
//	Vector3d x2(3);
//	for (int i = 0; i < 2; ++i)
//	{
//		x0[i] = vx(this->m_vidx[i]);
//		x1[i] = vx(this->m_vidx[2 + i]);
//		x2[i] = vx(this->m_vidx[4 + i]);
//	}
//
//	x0[2] = 0.0;
//	x1[2] = 0.0;
//	x2[2] = 0.0;
//
//	Vector3d e0 = x1 - x0;
//	Vector3d e1 = x2 - x1;
//	int C = 0;
//	if (e0.cross(e1).dot(Vector3d(0.0, 0.0, 1.0)) > 0.0)
//		C = 1.0;
//	else C = -1.0;
//
//	double vL0 = this->m_vL0;
//	//double e0norm0 = this->m_e0norm0;
//	//double e1norm0 = this->m_e1norm0;
//
//	double kB = m_vpmat[0]->getBendingK();
//
//#include "../../Maple/Code/RodAngle3DEnergy.mcg"
//
//	this->m_energy = t78;
}

void RodAngleElement2D::updateForce(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x0(2);
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x0[i] = vx(this->m_vidx[i]);
		x1[i] = vx(this->m_vidx[2 + i]);
		x2[i] = vx(this->m_vidx[4 + i]);
	}

	Vector2d x0p = getBlock2x1(this->m_vnodeIdx[0], vx);
	Vector2d x1p = getBlock2x1(this->m_vnodeIdx[1], vx);
	Vector2d x2p = getBlock2x1(this->m_vnodeIdx[2], vx);
	Vector2d x01 = x1p - x0p;
	Vector2d x12 = x2p - x1p;
	double cosa = x01.normalized().dot(x12.normalized());
	//double angle0 = atan2(x12[1] - x01[1], x12[0] - x01[0]);  
	//double angle1 = acos(cosa); // For comparison purposes

	if (isApprox(abs(cosa), 1.0, 1.0e-6)) // Colinear
	{
		this->m_vfVal.setZero();
		return; // Special case
	}

	Real C = (((x1[0] - x0[0])*(x2[1] - x1[1]) - (x1[1] - x0[1])*(x2[0] - x1[0])) < 0.0) ? -1.0 : 1.0; // Convexity

	double vL0 = this->m_vL0;
	
	double phi0 = this->m_phi0 + this->m_phi0*this->m_preStrain;

	double kB = m_vpmat[0]->getBendingK();

	double vf6[6];

//#include "../../Maple/Code/RodAngle2DForce.mcg"

	for (int i = 0; i < 6; ++i)
		if (!isfinite(vf6[i]))
			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 6; ++i)
		this->m_vfVal(i) = vf6[i];

//	Vector3d x0(3);
//	Vector3d x1(3);
//	Vector3d x2(3);
//	for (int i = 0; i < 2; ++i)
//	{
//		x0[i] = vx(this->m_vidx[i]);
//		x1[i] = vx(this->m_vidx[2 + i]);
//		x2[i] = vx(this->m_vidx[4 + i]);
//	}
//
//	x0[2] = 0.0;
//	x1[2] = 0.0;
//	x2[2] = 0.0;
//
//	Vector3d e0 = x1 - x0;
//	Vector3d e1 = x2 - x1;
//	int C = 0;
//	if (e0.cross(e1).dot(Vector3d(0.0, 0.0, 1.0)) > 0.0)
//		C = 1.0;
//	else C = -1.0;
//
//
//	double vL0 = this->m_vL0;
//	//double e0norm0 = this->m_e0norm0;
//	//double e1norm0 = this->m_e1norm0;
//
//	double kB = m_vpmat[0]->getBendingK();
//
//	double vf9[9];
//
//#include "../../Maple/Code/RodAngle3DForce.mcg"
//
//	for (int i = 0; i < 9; ++i)
//		if (!isfinite(vf9[i]))
//			qDebug("In %s: Force value is NAN\n", __FUNCDNAME__);
//
//	double vf6[6];
//	for (int i = 0; i < 3; ++i)
//		for (int j = 0; j < 2; ++j)
//			vf6[2*i + j] = C*vf9[3*i + j];
}

void RodAngleElement2D::updateJacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x0(2);
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x0[i] = vx(this->m_vidx[i]);
		x1[i] = vx(this->m_vidx[2 + i]);
		x2[i] = vx(this->m_vidx[4 + i]);
	}

	Vector2d x0p = getBlock2x1(this->m_vnodeIdx[0], vx);
	Vector2d x1p = getBlock2x1(this->m_vnodeIdx[1], vx);
	Vector2d x2p = getBlock2x1(this->m_vnodeIdx[2], vx);
	Vector2d x01 = x1p - x0p;
	Vector2d x12 = x2p - x1p;
	double cosa = x01.normalized().dot(x12.normalized());
	if (isApprox(abs(cosa), 1.0, 1.0e-6)) // Colinear
	{
		this->m_vJVal.setZero();
		return; // Special case
	}

	Real C = (((x1[0] - x0[0])*(x2[1] - x1[1]) - (x1[1] - x0[1])*(x2[0] - x1[0])) < 0.0) ? -1.0 : 1.0; // Convexity

	double vL0 = this->m_vL0;
	
	double phi0 = this->m_phi0 + this->m_phi0*this->m_preStrain;

	double kB = m_vpmat[0]->getBendingK();

	double mJ[6][6];
	 
//#include "../../Maple/Code/RodAngle2DJacobian.mcg"

	for (int i = 0; i < 6; ++i)
		for (int j = 0; j < 6; ++j)
			if (!isfinite(mJ[i][j]))
				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 6; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJ[i][j]; 

//	Vector3d x0(3);
//	Vector3d x1(3);
//	Vector3d x2(3);
//	for (int i = 0; i < 2; ++i)
//	{
//		x0[i] = vx(this->m_vidx[i]);
//		x1[i] = vx(this->m_vidx[2 + i]);
//		x2[i] = vx(this->m_vidx[4 + i]);
//	}
//
//	x0[2] = 0.0;
//	x1[2] = 0.0;
//	x2[2] = 0.0;
//
//	Vector3d e0 = x1 - x0;
//	Vector3d e1 = x2 - x1;
//	int C = 0;
//	if (e0.cross(e1).dot(Vector3d(0.0, 0.0, 1.0)) > 0.0)
//		C = 1.0;
//	else C = -1.0;
//
//
//	double vL0 = this->m_vL0;
//	//double e0norm0 = this->m_e0norm0;
//	//double e1norm0 = this->m_e1norm0;
//
//	double kB = m_vpmat[0]->getBendingK();
//
//	double mJ[9][9];
//
//#include "../../Maple/Code/RodAngle3DJacobian.mcg"
//
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			if (!isfinite(mJ[i][j]))
//				qDebug("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//
//	double mJ6[6][6];
//	for (int i = 0; i < 3; i++)
//		for (int j = 0; j < 3; j++)
//			for (int ii = 0; ii < 2; ii++)
//				for (int jj = 0; jj < 2; jj++)
//				{
//		mJ6[2*i + ii][2*j + jj] = C*mJ[3*i + ii][3*j + jj];
//				}
}