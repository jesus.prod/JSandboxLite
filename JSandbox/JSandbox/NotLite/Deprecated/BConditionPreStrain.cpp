/*=====================================================================================*/
/*!
\file		BConditionPreStrain.cpp
\author		jesusprod
\brief		Implementation of BConditionPreStrain
/*=====================================================================================*/

#include <JSandbox/BConditionPreStrain.h>

BConditionPreStrain::BConditionPreStrain(SolidModel* pModel) : BCondition(pModel)
{
	this->m_factor = 1.0;
}

BConditionPreStrain::~BConditionPreStrain()
{
	// Nothing to do here...
}


void BConditionPreStrain::constrain(SolidModel* pModel) const
{
	double factor = 1.0 - (1.0 - this->m_factor)*this->getLoadingFactor();
	for (int i = 0; i < pModel->getNumEle(); ++i) // Pre-strain
		pModel->getElement(i)->setPreStrain(factor);
}