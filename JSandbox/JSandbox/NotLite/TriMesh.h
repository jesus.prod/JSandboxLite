/*=====================================================================================*/
/*!
\file		TriMesh.h
\author		jesusprod
\brief		Wrapping class of Open Mesh for a simple 3D mesh.
*/
/*=====================================================================================*/

#ifndef TRIMESH_H
#define TRIMESH_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/BVHTree.h> // To search

#include <OpenMesh/Core/IO/MeshIO.hh> // Before kernel
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

#include <JSandbox/Model/ModelCoupling.h>

struct MeshTraits : public OpenMesh::DefaultTraits
{
	// Necessary to be able to delete and modify
	VertexAttributes(OpenMesh::Attributes::Status);
	FaceAttributes(OpenMesh::Attributes::Status);
	EdgeAttributes(OpenMesh::Attributes::Status);

	// Type definitions
	typedef unsigned int ID;
	typedef OpenMesh::Vec3d Point;
	typedef OpenMesh::Vec3d Normal;
};

typedef OpenMesh::TriMesh_ArrayKernel_GeneratorT<MeshTraits>::Mesh BaseMesh;

// Utility vertex property names for simulation purposes

typedef struct VProperty
{
	static const string POSE;	// Position in elastic space
	static const string POS0;	// Position in material space

	static const string VELE;	// Velocity in elastic space
	static const string VEL0;	// Velocity in material space
	static const string POSP;	// Position in plastic space

	static const string TARE;	// Target in elastic space

} VProperty;

class BVHTree;

// Utility vertex property names for geometry purposes

typedef struct VPropGeo
{
	static const string POS3D;	// Position in 3D space
	static const string PAR2D;	// Parametrization in 2D

} VPropGeo;

class TriMesh : public BaseMesh
{
public:
	TriMesh(void);
	TriMesh(const dVector& vpos, const iVector& vidx, const vector<string>& vps = vector<string>());
	void init(const dVector& vpos, const iVector& vidx, const vector<string>& vps = vector<string>());

	void initSphere(Real dimW, Real dimH, const vector<string>& vps = vector<string>());
	void initPlane(Real dimW, Real dimH, const vector<string>& vps = vector<string>());

	virtual ~TriMesh(void);

	TriMesh(const TriMesh& toCopy);

	// Other --------------------------------------------------------------------

	/*! Returns the number of properties.
	*/
	virtual int getNumProp() const;

	/*! Returns the string vector of vertex properties.
	*/
	virtual const vector<string>& getProperties() const;

	/*! Returns whether or not it has a property.
	*/
	virtual bool hasProperty(const string& pname) const;

	virtual int getNumEdge() const { return (int) this->n_edges(); }
	virtual int getNumFace() const { return (int) this->n_faces(); }
	virtual int getNumNode() const { return (int) this->n_vertices(); }

	// Property dependent ------------------------------------------------------

	virtual void scale(Real sX, Real sY, Real sZ, const string& pname = "");
	virtual void rotate(Real rX, Real rY, Real rZ, const string& pname = "");
	virtual void translate(Real tX, Real tY, Real tZ, const string& pname = "");

	virtual void updateBVH_Face(bool clear = true, const string& pname = "");
	virtual void updateBVH_Edge(bool clear = true, const string& pname = "");
	virtual const BVHTree* getBVHTree_Face(const string& pname = "");
	virtual const BVHTree* getBVHTree_Edge(const string& pname = "");
	virtual bool isBVHUpdated_Face(const string& pname = "");
	virtual bool isBVHUpdated_Edge(const string& pname = "");

	virtual Real getAverageEdgeLength(const string& pname = "") const;

	virtual void getNodesMatrix(MatrixXd& V, const string& pname = "") const;
	virtual void getFacesMatrix(MatrixXi& F, const string& pname = "") const;

	virtual void getNormals(dVector& vn, bool flip = false, const string& pname = "") const;

	virtual Real computeSectorAngle(const HalfedgeHandle& hh, const string& pname = "") const;

	virtual Real getFanAngle(const VertexHandle& vh, const string& pname = "") const;
	virtual Real getVoronoiArea(const VertexHandle& vh, const string& pname = "") const;
	virtual Real getMeanCurvature(const VertexHandle& vh, const string& pname = "") const;
	virtual Real getGaussianCurvature(const VertexHandle& vh, const string& pname = "") const;
	virtual Vector3d getMeanCurvatureNormal(const VertexHandle& vh, const string& pname = "") const;
	virtual Vector2d getPrincipalCurvatures(const VertexHandle& vh, const string& pname = "") const;

	virtual void getFanAngles(dVector& va, const string& pname = "") const;
	virtual void getVoronoiAreas(dVector& va, const string& pname = "") const;
	virtual void getMeanCurvatures(dVector& vH, const string& pname = "") const;
	virtual void getGaussianCurvatures(dVector& vK, const string& pname = "") const;
	virtual void getMeanCurvatureNormals(vector<Vector3d>& vh, const string& pname = "") const;
	virtual void getPrincipalCurvatures(vector<Vector2d>& vk, const string& pname = "") const;

	virtual void getVoronoiAreas_LIBIGL(dVector& va, const string& pname = "") const;
	virtual void getMeanCurvatures_LIBIGL_LB(dVector& vH, const string& pname = "") const;
	virtual void getMeanCurvatures_LIBIGL_MK(dVector& vH, const string& pname = "") const;
	virtual void getGaussianCurvatures_LIBIGL(dVector& vK, const string& pname = "") const;
	virtual void getMeanCurvatureNormals_LIBIGL(vector<Vector3d>& vh, const string& pname = "") const;
	virtual void getPrincipalCurvatures_LIBIGL(vector<Vector2d>& vk, const string& pname = "") const;
	virtual void getPrincipalCurvatures_LIBIGL(vector<vector<Vector3d>>& vk, const string& pname = "") const;

	virtual Vector3d getPoint(int i, const string& pname = "") const { return this->getPoint(this->vertex_handle(i), pname); }
	virtual void setPoint(int i, const Vector3d& p, const string& pname = "") { this->setPoint(this->vertex_handle(i), p, pname); }

	/*! Returns a non-constant reference to the specified property of the vertex.
	*/
	virtual MeshTraits::Point& point(VertexHandle vh, const string& pname = "");

	/*! Returns a constant reference to the specified property of the vertex.
	*/
	virtual const MeshTraits::Point& point(VertexHandle vh, const string& pname = "") const;

	/*! Get the value of the specified mesh properties.
	*/
	virtual void getPoints(dVector& vp, const string& pname = "") const;

	/*! Set the value of the specified mesh properties.
	*/
	virtual void setPoints(const dVector& vp, const string& ppname = "");

	/*! Returns the value of the point at the given vertex.
	*/
	virtual Vector3d getPoint(const VertexHandle& vh, const string& pname = "") const;

	/*! Sets the value of the point at the given vertex.
	*/
	virtual void setPoint(const VertexHandle& vh, const Vector3d& p, const string& pname = "");

	/*! Evaluates the specifed geometric point using ModelPoint struct.
	*/
	virtual Vector3d evaluatePoint(const ModelPoint& modelPoint, const string& pname = "") const;

	/*! Returns the area of a given face.
	*/
	virtual double getFaceArea(const FaceHandle& fh, const string& pname = "") const;

	/*! Returns the normal at a given vertex (no unit).
	*/
	virtual Vector3d getNormal(const VertexHandle& vh, const string& pname = "") const;

	/*! Returns the unit normal at a given vertex.
	*/
	virtual Vector3d getUnitNormal(const VertexHandle& vh, const string& pname = "") const;

	/*! Returns the normal at a given face (no unit).
	*/
	virtual Vector3d getFaceNormal(const FaceHandle& fh, const string& pname = "") const;

	/*! Returns the unit normal at a given face.
	*/
	virtual Vector3d getFaceUnitNormal(const FaceHandle& fh, const string& pname = "") const;

	/*! Returns the dihedral angle between the faces of the speicified edge index.
	*/
	virtual double getDihedralAngle(const EdgeHandle& eh, const string& pname = "") const;

	/*! Returns the curvature tensor at a given face considering the adjancent faces to it.
	*/
	virtual Matrix3d getCurvature(const FaceHandle& fh, const string& pname = "") const;

	/*! Returns a vector with the points of the specified face handle.
	*/
	virtual void getFacePoints(const FaceHandle& fh, vector<Vector3d>& vfv, const string& pname = "") const;

	/*! Returns a vector with the points of the specified edge handle.
	*/
	virtual void getEdgePoints(const EdgeHandle& eh, vector<Vector3d>& vfe, const string& pname = "") const;

	/*! Returns a vector with the points forming a hinge at the specified edge or false if it is a boundary.
	*/
	virtual bool getHingePoints(const EdgeHandle& eh, vector<Vector3d>& vv, const string& pname = "") const;

	/*! Returns a vector with values of the AA bounding box sides.
	*/
	virtual dVector getAABBRanges(const string& pname = "") const;

	/*! Returns a vector with values of the AA bounding box.
	*/
	virtual dVector getAABBExtremes(const string& pname = "") const;

	/*! Returns a vector with the AABB considering the conected components.
	*/
	virtual vector<dVector> getAABBExtremesCC(const string& pname = "") const;

	/*! Returns the closest distance between points at the property.
	*/
	virtual Real getClosestVertexDistance(const string& pname = "") const;

	/*! Tests the closest distance between points at property to be higher than a value.
	*/
	virtual void testOverlappedVertices(Real value, const string& pname = "") const;

	/*! Tests the vertices of the mesh to detect isolated.
	*/
	virtual void testIsolatedVertices(const string& pname = "") const;

	/*! Tests the triangles of the mesh to det inverted.
	*/
	virtual void testInvertedTriangles(const string& pname = "") const;

	/*! Returns the closest vertex to the specified one considering a property.
	*/
	virtual VertexHandle getClosestVertex(const Vector3d& p, Real& D, const string& pname = "") const;

	/*! Returns the embedded projection of the specified point over the surface as long as it is no further from a distance.
	*/
	virtual void getPointEmbedding(const Vector3d& p, Real maxD, PointPro& pP, const string& pname = "");

	/*! Returns the embedded projection of the specified cloud of points over the surface as long as it is no further from a distance.
	*/
	virtual void getPointsEmbedding(const vector<Vector3d>& vp, Real maxD, vector<PointPro>& vpP, const string& pname = "");

	/*! Returns lists of the ordered edges and vertices of mesh boundary loops. The resulting boundary curve is splitted into pieces according to the given maximum angle criteria.
	*/
	virtual void getBoundaryComponents_Smooth(vector<vector<vector<EdgeHandle>>>& vehs, vector<vector<vector<VertexHandle>>>& vvhs, Real angle, const string& pname = "") const;

	// Property independent ----------------------------------------------------------

	/*! Returns the list of indices in the mesh.
	*/
	virtual void getIndices(iVector& vidx) const;

	/*!Return the list of vertices colors for CC*/
	virtual void getColorCC(dVector& vcol) const;

	/*! Returns the list of vertices neighbors of the specified vertex.
	*/
	virtual void getVertexVertices(const VertexHandle& vh, vector<VertexHandle>& vv) const;

	/*! Returns the list of edges incident in the specified vertex.
	*/
	virtual void getVertexEdges(const VertexHandle& vh, vector<EdgeHandle>& vv) const;

	/*! Returns the list of faces incident in the specified vertex.
	*/
	virtual void getVertexFaces(const VertexHandle& vh, vector<FaceHandle>& vv) const;

	/*! Returns two vertices forming  the specified edge.
	*/
	virtual void getEdgeVertices(const EdgeHandle& eh, vector<VertexHandle>& vv) const;

	/*! Returns the edge corresponding to a pair of vertices if it exists
	*/
	virtual bool getVerticesEdge(const vector<VertexHandle>& vehv, EdgeHandle& eh) const;

	/*! Returns the faces of the specified edge (2 if it isn't boundary, 1 if it is).
	*/
	virtual void getEdgeFaces(const EdgeHandle& eh, vector<FaceHandle>& vv) const;

	/*! Returns three vertices forming the specified face.
	*/
	virtual void getFaceVertices(const FaceHandle& fh, vector<VertexHandle>& vv) const;

	/*! Returns the face corresponding to a triplet of vertices if it exists
	*/
	virtual bool getVerticesFace(const vector<VertexHandle>& vfvh, FaceHandle& fh) const;

	/*! Returns three edges forming the specified face.
	*/
	virtual void getFaceEdges(const FaceHandle& eh, vector<EdgeHandle>& ve) const;

	/*! Returns the faces around the specified face.
	*/
	virtual void getFaceFaces(const FaceHandle& eh, vector<FaceHandle>& ve) const;

	/*! Returns four vertices forming a hinge at the specified edge. False if boundary.
	*/
	virtual bool getHingeVertices(const EdgeHandle& eh, vector<VertexHandle>& vv) const;

	/*! Returns lists of the unordered edges and vertices of mesh boundaries.
	*/
	virtual void getBoundaryComponents_Simple(vector<EdgeHandle>& vehs, vector<VertexHandle>& vvhs) const;

	/*! Returns lists of the ordered edges and vertices of mesh boundary loops.
	*/
	virtual void getBoundaryComponents_Sorted(vector<vector<EdgeHandle>>& vehs, vector<vector<VertexHandle>>& vvhs) const;

	/*! Returns a partition of mesh faces based on the specified list of edges. If the edge list is a closed cycle then it
	results in a partition of faces in/out the surrounded region. Otherwise, the result of the partition is not determined.
	*/
	virtual void getRegionFacePartition(const vector<EdgeHandle>& veh, vector<FaceHandle>& vfh0, vector<FaceHandle>& vfh1) const;

	/*! Returns a partition of mesh faces and vertices corresponding to connected components.
	*/
	virtual void getPartitionConnectedComponents(vector<vector<FaceHandle>>& vfhs, vector<vector<VertexHandle>>& vvhs) const;

	/*! Returns a partition of mesh faces corresponding to regions separated by specified edges.
	*/
	virtual void getPartitionBoundaryEdgeRegions(const vector<EdgeHandle>& veh, vector<vector<FaceHandle>>& vfhs) const;

	/*! Returns maps relating the components of a splitted mesh, creating a indexation for the set of unique vertices and edges.
	*/
	virtual void getSplittedMeshMaps(vector<iVector>& vmapSplit2Whole, vector<iVector>& vmapWhole2Split, map<int, int>& mapEdges) const;

	virtual void getSortedBoundaryIndices(iVector& vboundList, iVector& vslaveList) const;

	virtual void getSortedBoundaryPoints(dVector& vpos, const string& pname = "") const;
	virtual void setSortedBoundaryPoints(const dVector& vpos, const string& pname = "");

	//virtual void computeBoundaryToMinimalMap_OptimizedCotangent(SparseMatrixXd& mHB, const string& pname = "", int DIM = 3);
	//virtual void computeBoundaryToMinimalMap_OptimizedRegular(SparseMatrixXd& mHB, const string& pname = "", int DIM = 3);
	virtual void computeBoundaryToMinimalMap_LaplacianCotangent(SparseMatrixXd& mHB, const string& pname = "", int DIM = 3);
	virtual void computeBoundaryToMinimalMap_LaplacianRegular(SparseMatrixXd& mHB, const string& pname = "", int DIM = 3);

	//virtual void computeBoundaryToMinimalMap_Optimized(const dVector& vedgeWeights, SparseMatrixXd& mHB, int DIM = 3);
	virtual void computeBoundaryToMinimalMap_Laplacian(const dVector& vedgeWeights, SparseMatrixXd& mHB, int DIM = 3);

	virtual void computeCartesianToDifferentialMap_Graph(SparseMatrixXd& mM, const string& pname = "", int DIM = 3);

	virtual void computeSymmetricLaplacian_Graph(SparseMatrixXd& mL, const string& pname = "");
	virtual void computeSymmetricLaplacian_Cotangent(SparseMatrixXd& mL, const string& pname = "");
	virtual void computeSymmetricLaplacian_MeanValue(SparseMatrixXd& mL, const string& pname = "");
	virtual void computeSymmetricLaplacian_Regular(SparseMatrixXd& mL, const string& pname = "");

	virtual void computeAbsoluteLaplacian_Graph(SparseMatrixXd& mL, const string& pname = "");
	virtual void computeAbsoluteLaplacian_Cotangent(SparseMatrixXd& mL, const string& pname = "");
	virtual void computeAbsoluteLaplacian_MeanValue(SparseMatrixXd& mL, const string& pname = "");
	virtual void computeAbsoluteLaplacian_Regular(SparseMatrixXd& mL, const string& pname = "");

	virtual void computeLaplacianWeights_Graph(dVector& vedgeWeights, const string& pname = "");
	virtual void computeLaplacianWeights_Cotangent(dVector& vedgeWeights, const string& pname = "");
	virtual void computeLaplacianWeights_MeanValue(dVector& vedgeWeights, const string& pname = "");
	virtual void computeLaplacianWeights_Regular(dVector& vedgeWeights, const string& pname = "");

	virtual void computeSymmetricLaplacian(const dVector& vedgeWeights, SparseMatrixXd& mL);

	virtual void computeAbsoluteLaplacian(const dVector& vedgeWeights, const dVector& vnodeAreas, SparseMatrixXd& mL);



	// Access to identifiers --------------------------------

	MeshTraits::ID getVertID(const VertexHandle& vh) const;
	MeshTraits::ID getEdgeID(const EdgeHandle& eh) const;
	MeshTraits::ID getFaceID(const FaceHandle& fh) const;

	VertexHandle getVertHandle(const MeshTraits::ID& id) const;
	EdgeHandle getEdgeHandle(const MeshTraits::ID& id) const;
	FaceHandle getFaceHandle(const MeshTraits::ID& id) const;

	bool hasVertID(const MeshTraits::ID& id) const;
	bool hasEdgeID(const MeshTraits::ID& id) const;
	bool hasFaceID(const MeshTraits::ID& id) const;

	void setVertID(const VertexHandle& vh, const MeshTraits::ID& id);
	void setEdgeID(const EdgeHandle& vh, const MeshTraits::ID& id);
	void setFaceID(const FaceHandle& vh, const MeshTraits::ID& id);

	void updateIdentifiersMap();

	static MeshTraits::ID CreateIdentifier();

	static void BuildSplittedMeshVerticesMap(const TriMesh* pWholeMesh, const TriMesh* pSplittedMesh,
											 const vector<map<MeshTraits::ID, MeshTraits::ID>>& vdupsMap,
											 vector<iVector>& vmapSplitToWhole, vector<iVector>& vmapWholeToSplit);

	static void BuildSplittedMeshEdgesMap(const TriMesh* pSplittedMesh,
										  const vector<iVector>& vmapSplitToWhole,
										  const vector<iVector>& vmapWholeToSplit,
										  map<int, int>& mapBoundaryEdges);

	static void TestEquivalence(const TriMesh* pMesh0, const TriMesh* pMesh1);


	std::map<string, bool> m_mBVHUpdatedFace;
	std::map<string, bool> m_mBVHUpdatedEdge;
	std::map<string, BVHTree*> m_mBVHTreeFace;
	std::map<string, BVHTree*> m_mBVHTreeEdge;

protected:

	vector<string> m_vproperties;

	std::map<MeshTraits::ID, VertexHandle> m_mvertIDs;
	std::map<MeshTraits::ID, EdgeHandle> m_medgeIDs;
	std::map<MeshTraits::ID, FaceHandle> m_mfaceIDs;

	static MeshTraits::ID m_IDCount;

};

#endif