/*=====================================================================================*/
/*!
\file		SurfaceModel.cpp
\author		jesusprod
\brief		Implementation of SurfaceModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/SurfaceModel2DRest.h>

#include <JSandbox/Geometry/CGALConstrainedMesher.h>

#include <JSandbox/Element/DSAreaElement.h>
#include <JSandbox/Element/DSEdgeElement.h>
#include <JSandbox/Element/DSHingeElement.h>
#include <JSandbox/Element/CSTElementStVKTrueOrthotropic.h>
#include <JSandbox/Element/CSTElementStVKOrthotropic.h>
#include <JSandbox/Element/CSTElementStVKIsotropic.h>
#include <JSandbox/Element/CSTElementNH.h>
#include <JSandbox/Element/CSTElementMR.h>

#include <omp.h> // MP support

SurfaceModel2DRest::SurfaceModel2DRest()
{
	// Default plane XZ
	this->m_plane = 1;

	this->deprecateConfiguration();
}

SurfaceModel2DRest::~SurfaceModel2DRest()
{
	logSimu("[TRACE] Destroying SurfaceModel2DRest: %d", this);
}

void SurfaceModel2DRest::setup()
{
	//dVector vx;
	//this->m_pSimMesh->getPoints(vx, VProperty::POSE);
	//this->m_pSimMesh->setPoints(vx, "");
	//m_pSimMesh->getSplittedMeshMaps(this->m_vmapSplit2Whole, this->m_vmapWhole2Split, this->m_mapBoundaryEdges);

	dVector vx0;
	dVector vxx;
	this->m_pSimMesh->getPoints(vxx, VProperty::POSE);
	this->m_pSimMesh->getPoints(vx0, VProperty::POS0);
	this->m_pSimMesh->setPoints(vx0, "");
	m_pSimMesh->getSplittedMeshMaps(this->m_vmapSplit2Whole, this->m_vmapWhole2Split, this->m_mapBoundaryEdges);
	this->m_pSimMesh->setPoints(vxx, "");

	int numvraw = (int) this->m_pSimMesh->n_vertices();
	int numvsim = (int) this->m_vmapWhole2Split.size();

	// DOF

	this->m_numDim_x = 3;
	this->m_numDim_0 = 2;
	this->m_numDimRaw = 3;

	this->m_numNodeRaw = numvraw;
	this->m_numNodeSim = numvsim;

	this->m_nrawDOF = 3 * numvraw;
	this->m_nsimDOF_x = 3 * numvsim;
	this->m_nsimDOF_0 = 2 * numvraw;

	this->m_state_x->m_vx.resize(this->m_nsimDOF_x);
	this->m_state_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vx.resize(this->m_nsimDOF_0);
	this->m_state_0->m_vv.resize(this->m_nsimDOF_0);
	this->m_state_x->m_vv.setZero();
	this->m_state_0->m_vv.setZero();

	dVector vxSTLSplit3D;
	dVector vXSTLSplit3D;
	this->m_pSimMesh->getPoints(vxSTLSplit3D, VProperty::POSE);
	this->m_pSimMesh->getPoints(vXSTLSplit3D, VProperty::POS0);
	dVector vxSTLWhole3D(this->m_nsimDOF_x);
	dVector vXSTLSplit2D(this->m_nsimDOF_0);
	
	// Map from splitted 3D position to the united 3D material position
	applyIndexMap(this->m_vmapSplit2Whole, vxSTLSplit3D, 3, vxSTLWhole3D);

	// Map from splitted 3D coordinates to 2D rest
	to2D(vXSTLSplit3D, vXSTLSplit2D, this->m_plane);

	// Convert to Eigen
	this->m_state_x->m_vx = toEigen(vxSTLWhole3D);
	this->m_state_0->m_vx = toEigen(vXSTLSplit2D); 

	// Initialize elements

	this->freeElements();

	// Initialize stretch elements

	switch (this->m_DM)
	{
	case Model::QuadArea:
		//this->initializeElements_QuadArea();
		break;
	case Model::MS_QuadArea:
		//this->initializeElements_QuadArea();
		this->initializeElements_MassSpring();
		break;
	case Model::DS_MS_QuadArea:
		//this->initializeElements_QuadArea();
		this->initializeElements_MassSpring();
		this->initializeElements_Bending();
		break;
	case Model::DS_CSTStretch:
		this->initializeElements_CSTStretch();
		this->initializeElements_Bending();
		break;
	}

	this->initializeMaterials();
	
	this->m_isReady_Setup = true;
	this->deprecateDiscretization();

	// Update simulation 
	this->update_SimDOF();

	logSimu("[TRACE] Initialized SurfaceModel. Raw: %d, Sim: %d", this->m_nrawDOF, this->m_nsimDOF_x);
}

void SurfaceModel2DRest::setStateToMesh(const TriMesh& mesh)
{
	delete this->m_pSimMesh;
	this->m_pSimMesh = new TriMesh(mesh);
	this->update_StateFromSimMesh();
}

void SurfaceModel2DRest::initializeElements_QuadArea()
{
	// Initialize area preserving elements

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		NodalSolidElement* pEle = new DSAreaElement(2);

		// Set indices
		vector<int> vinds0, vinds;
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds0.push_back((*fv_it).idx());
			++fv_it;
		}
		pEle->setNodeIndices0(vinds0);
		transformIndex(this->m_vmapSplit2Whole, vinds0, 0, vinds);
		pEle->setNodeIndicesx(vinds); // Initialize whole indices

		// Add to elements vector
		this->m_vpEles.push_back(pEle);
		this->m_vpStretchEles.push_back(pEle);
	}
}

void SurfaceModel2DRest::initializeElements_MassSpring()
{
	// Initialize stretch mass-spring elements

	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());
	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		NodalSolidElement* pEle = new DSEdgeElement(2);

		// Set indices
		vector<int> vinds0, vinds;
		TriMesh::EdgeHandle eh = *e_it;
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);

		vinds0.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx());
		vinds0.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx());

		pEle->setNodeIndices0(vinds0);
		transformIndex(this->m_vmapSplit2Whole, vinds0, 0, vinds);
		pEle->setNodeIndicesx(vinds); // Initialize whole indices

		// Add to elements vector
		this->m_vpEles.push_back(pEle);
		this->m_vpStretchEles.push_back(pEle);
	}
}

void SurfaceModel2DRest::initializeElements_CSTStretch()
{
	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		//CSTElement* pEle = new CSTElementStVKTrueOrthotropic(2);
		CSTElement* pEle = new CSTElementStVKOrthotropic(2);
		//CSTElement* pEle = new CSTElementStVKIsotropic(2);
		//CSTElement* pEle = new CSTElementNH(2);
		//CSTElement* pEle = new CSTElementMR(2);

		// Set indices
		vector<int> vinds0(3), vinds(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds0[v] = (*fv_it).idx();
			++fv_it;
		}

		pEle->setNodeIndices0(vinds0);
		transformIndex(this->m_vmapSplit2Whole, vinds0, 0, vinds);
		pEle->setNodeIndicesx(vinds); // Initialize whole indices

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpStretchEles.push_back(pEle);
	}
}

void SurfaceModel2DRest::initializeElements_Bending()
{
	// Initialize discrete-shells bending elements

	// No boundary

	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());
	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		// Set indices
		vector<int> vinds0, vinds;
		TriMesh::EdgeHandle eh = *e_it;
		if (m_pSimMesh->is_boundary(eh))
			continue;

		NodalSolidElement* pEle = new DSHingeElement(2);

		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);

		for (int i = 0; i < 4; i++)
			vinds0.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx());

		pEle->setNodeIndices0(vinds0);
		transformIndex(this->m_vmapSplit2Whole, vinds0, 0, vinds);
		pEle->setNodeIndicesx(vinds); // Initialize whole indices

		// Add to elements vector
		this->m_vpEles.push_back(pEle);
		this->m_vpBendingEles.push_back(pEle);
	}
	
	//// Boundary seams

	//bVector vinitialized(this->m_pSimMesh->n_edges(), false); 
	//map<int, int>::const_iterator cit; // Iterate seam edges

	//for (cit = this->m_mapBoundaryEdges.begin(); cit != this->m_mapBoundaryEdges.end(); cit++)
	//{
	//	int e0Idx = cit->first;
	//	int e1Idx = cit->second;
	//	if (vinitialized[e0Idx] || vinitialized[e1Idx])
	//		continue; // Already initialized, continue

	//	DSHingeElement* pEle = new DSHingeElement(2);

	//	vinitialized[e0Idx] = true;
	//	vinitialized[e1Idx] = true;

	//	TriMesh::EdgeHandle eh0 = this->m_pSimMesh->edge_handle(e0Idx);
	//	TriMesh::EdgeHandle eh1 = this->m_pSimMesh->edge_handle(e1Idx);
	//	vector<TriMesh::EdgeHandle> veh;
	//	veh.push_back(eh0);
	//	veh.push_back(eh1);
	//	this->m_vSeamEleH.push_back(veh);

	//	assert(this->m_pSimMesh->is_boundary(eh0));
	//	assert(this->m_pSimMesh->is_boundary(eh1));

	//	std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
	//	vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh0, 0);
	//	vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh1, 0);
	//	vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
	//	vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);

	//	vector<int> vinds0, vinds;
	//	for (int i = 0; i < 4; i++)
	//		vinds0.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx());

	//	pEle->setNodeIndices0(vinds0);
	//	transformIndex(this->m_vmapSplit2Whole, vinds0, 0, vinds);
	//	pEle->setNodeIndices(vinds); // Initialize whole indices

	//	// Add to elements vector
	//	this->m_vpEles.push_back(pEle);
	//	this->m_vpSeamBendingEles.push_back(pEle);	
	//}
}

void SurfaceModel2DRest::update_Rest()
{
	SurfaceModel::update_Rest();

	// Consider boundary seams bending elements

	int numEle = (int) this->m_vpSeamBendingEles.size();
	for (int i = 0; i < numEle; ++i) // Zero strain...
	{
		// TODO: Check if this expression of the geometric factor is OK

		TriMesh::EdgeHandle eh0 = this->m_vSeamEleH[i][0];
		TriMesh::EdgeHandle eh1 = this->m_vSeamEleH[i][1];
		TriMesh::FaceHandle fh0 = this->m_pSimMesh->face_handle(this->m_pSimMesh->halfedge_handle(eh0, 0));
		TriMesh::FaceHandle fh1 = this->m_pSimMesh->face_handle(this->m_pSimMesh->halfedge_handle(eh1, 0));
		double A0 = this->m_pSimMesh->getFaceArea(fh0, VProperty::POS0);
		double A1 = this->m_pSimMesh->getFaceArea(fh1, VProperty::POS0);
		double Amean = (A0 + A1)*0.5;

		vector<Vector3d> vex0, vex1;
		this->m_pSimMesh->getEdgePoints(eh0, vex0, VProperty::POS0);
		this->m_pSimMesh->getEdgePoints(eh1, vex1, VProperty::POS0);

		// These lengths are supposed to be identical
		// but it might happen that they aren�t so we
		// compute the average here.

		double L0 = (vex0[0] - vex0[1]).norm();
		double L1 = (vex1[0] - vex1[1]).norm();
		double Lmean = 0.5*(L0 + L1);

		// Compute the geometric factor 
		double fac = Lmean*(Amean / 3.0);

		this->m_vpSeamBendingEles[i]->setRestFactor(fac);
		this->m_vpSeamBendingEles[i]->setRestAngle(0.0);
	}
}

void SurfaceModel2DRest::update_SimDOF()
{
	iVector vsimNodeIdx(this->m_numNodeRaw);
	iVector vrawNodeIdx(this->m_numNodeRaw);
	for (int i = 0; i < this->m_numNodeRaw; ++i)
		vrawNodeIdx[i] = i; // Default indexation
	transformIndex(this->m_vmapSplit2Whole, vrawNodeIdx, 0, vsimNodeIdx);

	// RAW -> SIM

	this->m_vraw2sim_x.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_numNodeRaw; ++i)
	{
		for (int j = 0; j < 3; ++j) // Whole, 3D: use built map
			this->m_vraw2sim_x[3*i + j] = 3 * vsimNodeIdx[i] + j;
	}

	this->m_vraw2sim_0.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_numNodeRaw; ++i)
	{
		if (this->m_plane == 1) // No rest Z
		{
			this->m_vraw2sim_0[3 * i + 1] = 2 * i + 0;
			this->m_vraw2sim_0[3 * i + 2] = 2 * i + 1;
			this->m_vraw2sim_0[3 * i + 0] = -1;
		}
		if (this->m_plane == 1) // No rest Y
		{
			this->m_vraw2sim_0[3 * i + 0] = 2 * i + 0;
			this->m_vraw2sim_0[3 * i + 2] = 2 * i + 1;
			this->m_vraw2sim_0[3 * i + 1] = -1;
		}
		if (this->m_plane == 2) // No rest Z
		{
			this->m_vraw2sim_0[3 * i + 0] = 2 * i + 0;
			this->m_vraw2sim_0[3 * i + 1] = 2 * i + 1;
			this->m_vraw2sim_0[3 * i + 2] = -1;
		}
	}
}

void SurfaceModel2DRest::update_StateFromSimMesh()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	dVector vXSplit3D;
	dVector vxSplit3D;
	this->m_pSimMesh->getPoints(vXSplit3D, VProperty::POS0);
	this->m_pSimMesh->getPoints(vxSplit3D, VProperty::POSE);

	dVector vXSplit2D;
	to2D(vXSplit3D, vXSplit2D, this->m_plane);
	dVector vxWhole3D(this->m_nsimDOF_x, 0.0);
	applyIndexMap(this->m_vmapSplit2Whole, vxSplit3D, 3, vxWhole3D);

	toEigen(vxWhole3D, this->m_state_x->m_vx);
	toEigen(vXSplit2D, this->m_state_0->m_vx);
	
	this->m_isReady_Mesh = true;
	this->m_isReady_Rest = false;
	this->deprecateDeformation();
	this->deprecateUndeformed();
}

void SurfaceModel2DRest::update_SimMeshFromState()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mesh)
		return; // Already

	dVector vXSplit3D; // Transform split 2D -> split 3D
	to3D(toSTL(this->m_state_0->m_vx), vXSplit3D, this->m_plane);
	
	dVector vxWhole3D = toSTL(this->m_state_x->m_vx);
	dVector vxSplit3D(this->m_nrawDOF, 0.0);
	applyIndexMap(this->m_vmapWhole2Split, vxWhole3D, 3, vxSplit3D);

	this->m_pSimMesh->setPoints(vXSplit3D, VProperty::POS0);
	this->m_pSimMesh->setPoints(vxSplit3D, VProperty::POSE);

	this->m_isReady_Mesh = true;
}

void SurfaceModel2DRest::getMasterBoundaryPoints_Basic(const TriMesh* pMesh, vector<ModelCurve>& vBCurves)
{
	// Get sorted boundaries for component

	vector<vector<TriMesh::EdgeHandle>> vehsS;
	vector<vector<TriMesh::VertexHandle>> vvhsS;
	pMesh->getBoundaryComponents_Sorted(vehsS, vvhsS);

	assert(vehsS.size() == vvhsS.size());
	int numComponents = (int)vehsS.size();

	vBCurves.resize(numComponents);

	// Build basic boundary points vector

	for (int i = 0; i < numComponents; ++i)
	{
		double lengthT = 0;

		int neB = (int)vehsS[i].size();
		int nvB = (int)vvhsS[i].size();
		assert(nvB == neB); // Closed

		// Initialize points

		vBCurves[i].resize(nvB);

		for (int j = 0; j < nvB; ++j)
		{
			vBCurves[i][j].m_mp = ModelPoint(3, vvhsS[i][j].idx()); // Add point
			vBCurves[i][j].m_vp = pMesh->getPoint(vvhsS[i][j], VProperty::POS0);

			if (j != 0) // If this is not the first vertex, sum the curve length
				lengthT += (vBCurves[i][j].m_vp - vBCurves[i][j - 1].m_vp).norm();

			vBCurves[i][j].m_arcLen = lengthT;
		}

		// Initialize properties: by default the point of
		// the curve is not a master point and the input
		// and output tangents are not equal.

		for (int j = 0; j < nvB; ++j)
		{
			int prevv = (j == 0)? nvB - 1 : j - 1;
			int nextv = (j == nvB - 1) ? 0 : j + 1;
			vBCurves[i][j].m_tanI = (vBCurves[i][j].m_vp - vBCurves[i][prevv].m_vp).normalized();
			vBCurves[i][j].m_tanO = (vBCurves[i][nextv].m_vp - vBCurves[i][j].m_vp).normalized();
			vBCurves[i][j].m_isMasterPoint = false;
		}
	}
}


void SurfaceModel2DRest::addMasterBoundaryPoints_Angle(const TriMesh* pMesh, const Real& maxAngle, ModelCurve& BCurve)
{
	int nvB = (int) BCurve.size();

	for (int j = 0; j < nvB; ++j)
	{
		Real angle = acos(BCurve[j].m_tanI.dot(BCurve[j].m_tanO));

		// Non-smooth boundary: do not
		// correct tangent values since
		// these are already separated

		if (angle > maxAngle)
		{
			BCurve[j].m_isMasterPoint = true;
		}
	}
}

void SurfaceModel2DRest::addMasterBoundaryPoints_Const(const TriMesh* pMesh, const Real& tol, const pVector& vCPoints, ModelCurve& BCurve)
{
	int nvB = (int) BCurve.size();
	int nvC = (int) vCPoints.size();

	for (int i = 0; i < nvC; ++i)
	{
		const ModelPoint& point = vCPoints[i];

		if (point.isExplicit())
		{
			// Look for the actual point
			int idx = point.getPointIdx();

			for (int j = 0; j < nvB; ++j)
			{
				if (BCurve[j].m_mp.getPointIdx() == idx)
				{
					BCurve[j].m_spointers.insert(i);
					if (!BCurve[j].m_isMasterPoint)
					{
						Vector3d tan = (BCurve[j].m_tanI + BCurve[j].m_tanO).normalized();
						BCurve[j].m_tanI = tan;
						BCurve[j].m_tanO = tan;
						BCurve[j].m_isMasterPoint = true;
					}
					break;
				}
			}
		}
		else
		{
			assert(point.getNumSup() == 3);

			// Is on node?
			int idx = -1;

			if (isApprox(point.getWeights()[0], 1.0, tol)) idx = point.getIndices()[0];
			else if (isApprox(point.getWeights()[1], 1.0, tol)) idx = point.getIndices()[1];
			else if (isApprox(point.getWeights()[2], 1.0, tol)) idx = point.getIndices()[2];

			TriMesh::VertexHandle vh = pMesh->vertex_handle(idx);

			if (idx != -1 && pMesh->is_boundary(vh))
			{
				for (int j = 0; j < nvB; ++j)
				{
					if (BCurve[j].m_mp.getPointIdx() == idx)
					{
						BCurve[j].m_spointers.insert(i);
						if (!BCurve[j].m_isMasterPoint)
						{
							Vector3d tan = (BCurve[j].m_tanI + BCurve[j].m_tanO).normalized();
							BCurve[j].m_tanI = tan;
							BCurve[j].m_tanO = tan;
							BCurve[j].m_isMasterPoint = true;
						}
						break;
					}
				}
			}
			else
			{
				iVector vedgeIdx;

				for (int k = 0; k < 3; ++k)
				{
					int k0 = (k + 1) % 3;
					int k1 = (k + 2) % 3;
					if (isApprox(point.getWeights()[k], 0.0, tol))
					{
						vedgeIdx.push_back(point.getIndices()[k0]);
						vedgeIdx.push_back(point.getIndices()[k1]);
						break;
					}
				}

				// Is it an edge?
				if (vedgeIdx.empty())
					continue; // Out

				// Get mesh edge handle

				vector<TriMesh::VertexHandle> vevh;
				vevh.push_back(pMesh->vertex_handle(vedgeIdx[0]));
				vevh.push_back(pMesh->vertex_handle(vedgeIdx[1]));

				TriMesh::EdgeHandle eh;
				if (!pMesh->getVerticesEdge(vevh, eh))
					throw new exception("[ERROR] Something is too bad");

				if (!pMesh->is_boundary(eh))
					continue; // Internal

				// Look for the right position
				int iv0 = -1;
				int iv1 = -1;
				for (int j0 = 0; j0 < nvB; ++j0)
				{
					int j1 = (j0 == nvB - 1)? 0 : j0 + 1;

					int j0Idx = BCurve[j0].m_mp.getPointIdx();
					int j1Idx = BCurve[j1].m_mp.getPointIdx();

					if (j0Idx == vedgeIdx[0] && j1Idx == vedgeIdx[1])
					{
						iv0 = j0;
						iv1 = j1;
						break;
					}
					
					if (j0Idx == vedgeIdx[1] && j1Idx == vedgeIdx[0])
					{
						iv0 = j1;
						iv1 = j0;
						break;
					}
				}

				// Add a new boundary point
				if (iv0 != -1 && iv1 != -1)
				{
					int iex = iv0;

					if (iv0 > iv1 || (iv0 == 0 && iv1 == nvB-1))
					{
						iv0 = iv1;
						iv1 = iex;
					}

					BCurve.push_back(CurvePoint());
					CurvePoint& BP = BCurve.back();

					BP.m_mp = point;
					
					BP.m_vp = pMesh->evaluatePoint(point, VProperty::POS0);

					BP.m_tanI = (BP.m_vp - BCurve[iv0].m_vp).normalized();
					BP.m_tanO = (BCurve[iv1].m_vp - BP.m_vp).normalized();
		
					BP.m_arcLen = BCurve[iv0].m_arcLen + (BP.m_vp - BCurve[iv0].m_vp).norm();

					BP.m_isMasterPoint = true;
					BP.m_spointers.insert(i);
				}
			}
		}
	}
}

void SurfaceModel2DRest::getMasterBoundarySplit(const Real& maxA, const Real& tolI, pVector& vCPoints, vector<vector<ModelCurve>>& vBCurves_S, vector<vector<BoundaryPair>>& vBPairs_S)
{
	if (!this->m_isReady_Mesh)
		this->update_SimMeshFromState();

	// Set the master boundary 
	vector<ModelCurve> vBCurves;

	// Get basic represetantion of boundary curves with master points
	this->getMasterBoundaryPoints_Basic(this->m_pSimMesh, vBCurves);

	int numComponents = (int) vBCurves.size();

	// Add additional master points

	int nvBTotal = 0;

	// Constraint sort boundary curves

	LT_CurvePoint LTC;

	for (int i = 0; i < numComponents; ++i)
	{
		this->addMasterBoundaryPoints_Const(this->m_pSimMesh, tolI, vCPoints, vBCurves[i]); // Constraint points 
		this->addMasterBoundaryPoints_Angle(this->m_pSimMesh, maxA, vBCurves[i]); // Non-smooth points
		sort(vBCurves[i].begin(), vBCurves[i].end(), LTC);
		nvBTotal += (int)vBCurves[i].size();
	}

	// Split at master boundary points

	vBCurves_S.resize(numComponents);
	vBPairs_S.resize(numComponents);

	for (int i = 0; i < numComponents; ++i)
	{
		int nvB = (int)vBCurves[i].size();

		// Select the first master point
		// to start from it the boundary
		// resampling

		int curIndex = -1;
		for (int j = 0; j < nvB; ++j)
			if (vBCurves[i][j].m_isMasterPoint)
			{
			curIndex = j;
			break; // Out
			}

		if (curIndex == -1)
		{
			vBCurves_S[i].push_back(vBCurves[i]); // Copy curve
			vBCurves_S[i].back().front().m_isMasterPoint = true;

			// For convenience we close the loop adding the front point
			vBCurves_S[i].back().push_back(vBCurves_S[i].back().front());

			continue;
		}

		// Split the curve

		// First and last points are always the same

		vBCurves_S[i].push_back(ModelCurve()); // A new curve!
		vBCurves_S[i].back().push_back(vBCurves[i][curIndex]);

		curIndex++;

		for (int j = 1; j < nvB; ++j, curIndex++)
		{
			if (curIndex == nvB)
				curIndex = 0;

			// Add the current point to the current boundary curve
			vBCurves_S[i].back().push_back(vBCurves[i][curIndex]);

			// If this is master, start a new curve
			if (vBCurves[i][curIndex].m_isMasterPoint)
			{
				vBCurves_S[i].push_back(ModelCurve()); // A new curve!
				vBCurves_S[i].back().push_back(vBCurves[i][curIndex]);
			}
		}

		if (curIndex == nvB)
			curIndex = 0;

		vBCurves_S[i].back().push_back(vBCurves[i][curIndex]);
	}

	// Look for pairs of seam curves

	for (int i = 0; i < numComponents; ++i)
		vBPairs_S[i].resize(vBCurves_S[i].size());
}
 
void SurfaceModel2DRest::remeshDelaunay(const pVector& vnodesExtI, const iVector& vedgesExtI, pVector& vnodesExtO, int numV, Real minrAngle, Real relInterMaxL, Real relBoundMaxL)
{
	if (!this->m_isReady_Mesh)
		this->update_SimMeshFromState();

	dVector vranges = this->m_pSimMesh->getAABBRanges(VProperty::POS0);

	Real maxA = M_PI/4;
	
	Real maxRange = -HUGE_VAL;
	for (int i = 0; i < 3; ++i)
		if (maxRange < vranges[i])
			maxRange = vranges[i];
	
	Real tolI = maxRange*1e-2;
	Real internalMaxL = maxRange*relInterMaxL;
	Real boundaryMaxL = maxRange*relBoundMaxL;

	logSimu("[TRACE] Remeshing surface mesh. I. Max. L: %f, B. Max. L: %f", internalMaxL, boundaryMaxL);

	// Here we consider both external and internal constrained points:
	//		External: Those imposed new external constraints  (e.g. couplings).
	//		Internal: Those associated with existing constraints (e.g. fixed).

	pVector vCPoints = vnodesExtI;
	iVector vCEdges = vedgesExtI;
	int nvExtC = (int) vnodesExtI.size();
	int neExtC = (int) vedgesExtI.size()/2;
	int nvModC = (int) this->m_sdcs.size();
	int nvC = nvExtC + nvModC;
	int neC = neExtC;

	vCPoints.insert(vCPoints.end(), this->m_sdcs.begin(), this->m_sdcs.end());

	// Filter external and internal constrained points to 
	// remove constrained points that are overlapping, for
	// instance, a fixed point that is also a coupling.

	iVector vmapDefaultUnique;

	pVector vCPoints_F;
	iVector vCEdges_F;

	vCPoints_F.reserve(nvC);
	vCEdges_F.reserve(2*neC);

	vmapDefaultUnique.resize(nvC, -1);

	int countF = 0;

	for (int i = 0; i < nvC; ++i)
	{
		if (vmapDefaultUnique[i] != -1)
			continue; // Already mapped

		Vector3d point0 = this->m_pSimMesh->evaluatePoint(vCPoints[i], VProperty::POS0);

		// Add point constraint to list
		vmapDefaultUnique[i] = countF++;
		vCPoints_F.push_back(vCPoints[i]);

		for (int j = i + 1; j < nvC; ++j)
		{
			Vector3d point1 = this->m_pSimMesh->evaluatePoint(vCPoints[j], VProperty::POS0);

			if ((point0 - point1).squaredNorm() < EPS_POS)
			{
				logSimu("--");
				logSimu("[WARNING] Overlapping coupling point: %d", i);
				logSimu("--");

				vmapDefaultUnique[j] = vmapDefaultUnique[i];
			}
		}
	}

	// Compute filtered edges

	int nindices = 2*neExtC;
	for (int i = 0; i < nindices; ++i)
		vCEdges_F.push_back(vmapDefaultUnique[vedgesExtI[i]]);

	int nvC_F = (int) vCPoints_F.size();

	// --

	// Get the splitted represetantion of mesh boundary curves. This also
	// considers the new boundary points due to new constrained points that
	// lie in the boundary of the membrane.

	vector<vector<ModelCurve>> vBCurves_S;
	vector<vector<BoundaryPair>> vBPairs_S;

	this->getMasterBoundarySplit(maxA, tolI, vCPoints_F, vBCurves_S, vBPairs_S);

	// Check the number of connected components
	assert(vBCurves_S.size() == vBPairs_S.size());
	int numComponents = (int) vBCurves_S.size();

	// Resample boundary curves

	vector<vector<Curve>> vboundary(numComponents);
	
	for (int i = 0; i < numComponents; ++i)
		vboundary[i].resize(vBCurves_S[i].size());

	for (int i0 = 0; i0 < numComponents; ++i0)
	for (int j0 = 0; j0 < (int) vBCurves_S[i0].size(); ++j0)
	{
		const vector<CurvePoint>& vsplit0 = vBCurves_S[i0][j0];
		
		// Build curve
		
		int nv0 = (int) vsplit0.size();
		dVector vx0(3 * nv0);
		for (int v = 0; v < nv0; ++v)
			set3D(v, vsplit0[v].m_vp, vx0);

		vboundary[i0][j0].initialize(vx0, false);
		vboundary[i0][j0].resample(boundaryMaxL,
									vBCurves_S[i0][j0].front().m_tanO,
									vBCurves_S[i0][j0].back().m_tanI);
	}

	// Prepare remesh input

	int nv = (int) this->m_pSimMesh->n_vertices();
	int ne = (int) this->m_pSimMesh->n_edges();
	dVector vnodesT;
	iVector vedgesT;
	vnodesT.reserve(2 * nv);
	vedgesT.reserve(2 * ne);

	// Add boundaries

	int nvBCount = 0;
	
	// Maps constraints index to new
	iVector vconIdx2NewIdx(nvC_F, -1);
	iVector vconInBoundary(nvC_F, false);

	for (int i = 0; i < numComponents; ++i)
	{
		// Get all curves nodes except last

		int numS = (int) vboundary[i].size();

		int nvBComp = nvBCount;

		for (int j = 0; j < numS; ++j)
		{
			int numV = (int) vboundary[i][j].getNumNode();
			const dVector& vx = vboundary[i][j].getPositions();

			int nvBSplit = nvBCount;

			// Fill map

			set<int> spointers = vBCurves_S[i][j][0].m_spointers;
			set<int>::iterator s_begin = spointers.begin();
			set<int>::iterator s_end = spointers.end();

			for (set<int>::iterator s_it = s_begin; s_it != s_end; ++s_it)
			{
				vconInBoundary[*s_it] = true;
				vconIdx2NewIdx[*s_it] = nvBSplit; 
			}

			// Add boundary vertices and edges

			for (int k = 0; k < numV - 1; ++k)
			{
				int offset = 3 * k;
				vnodesT.push_back(vx[offset + 0]);
				vnodesT.push_back(vx[offset + 2]);

				// Add current vertex to edges
				vedgesT.push_back(nvBCount++);
				vedgesT.push_back(nvBCount);
			}
		}

		vedgesT.back() = nvBComp;
	}

	// Add constraints

	int nvCCount = 0;

	for (int i = 0; i < nvC_F; ++i)
	{
		// If the point has not been added yet, then add it
		if (!vconInBoundary[i])
		{
			Vector2d vp = to2D(this->m_pSimMesh->evaluatePoint(vCPoints_F[i], VProperty::POS0), this->m_plane);
			vconIdx2NewIdx[i] = nvBCount + nvCCount;
			vnodesT.push_back(vp.x());
			vnodesT.push_back(vp.y());
			nvCCount++;
		}
		else
		{
			// Nothing to do, already added
		}
	}

	// Ensure all the constraints were mapped
	assert((int) vconIdx2NewIdx.size() == nvC_F);

	for (int i = 0; i < neExtC; ++i)
	{
		int offset = 2 * i;
		if (vconInBoundary[vCEdges_F[offset + 0]] &&
			vconInBoundary[vCEdges_F[offset + 1]])
			continue; // Both boundary constraints

		vedgesT.push_back(vconIdx2NewIdx[vCEdges_F[offset + 0]]);
		vedgesT.push_back(vconIdx2NewIdx[vCEdges_F[offset + 1]]);
	}

	// Finally remesh considerint constraints
	
	dVector vnodesOut;
	iVector vfacesOut;
	iVector vnewConst;
	string error;
	
	CGALConstrainedMesher mesherCGAL;
	mesherCGAL.setVerticesCount(numV);
	mesherCGAL.setMinimumAngle(0.250);
	mesherCGAL.setMaximumLength(internalMaxL);

	if (!mesherCGAL.createConstrainedMesh(vnodesT, vedgesT, vnodesOut, vfacesOut, vnewConst, error))
		throw new exception("[ERROR] Impossible to create the constrained mesh");  // Invalid meshing

	// Get the 3D coordinates and map
	
	dVector vpos0;
	dVector vposx;
	to3D(vnodesOut, vpos0, this->m_plane);
	
	// Project old rest coordinates
	
	int nvNew = (int)vpos0.size() / 3;
	vector<Vector3d> vcloudNew0(nvNew);
	vector<PointPro> vcloudPro0(nvNew);
	
	for (int i = 0; i < nvNew; ++i)
	{
		int offset = 3 * i;
		vcloudNew0[i] = Vector3d(vpos0[offset + 0],
								 vpos0[offset + 1],
								 vpos0[offset + 2]);
	}
	
	// Get the embedding of the new points in the previous rest mesh.
	// We use here a relatively conservative distance threshold as it
	// is possible that some of the boundary points have been moved.

	//this->m_pSimMesh->getPointsEmbedding(vcloudNew0, boundaryMaxL*1e-3, vcloudPro0, VProperty::POS0);
	this->m_pSimMesh->getPointsEmbedding(vcloudNew0, boundaryMaxL, vcloudPro0, VProperty::POS0);

	// Compute new deformed coordinates
	
	vposx.reserve((int)vpos0.size());
	for (int i = 0; i < nvNew; ++i)
	{
		if (!vcloudPro0[i].m_valid) 
			throw new exception("[ERROR] Invalid point embedding");
		Vector3d newPointx = this->m_pSimMesh->evaluatePoint(vcloudPro0[i].m_point, VProperty::POSE);

		vposx.push_back(newPointx.x());
		vposx.push_back(newPointx.y());
		vposx.push_back(newPointx.z());
	}
	
	vector<string> vprop;
	vprop.push_back(VProperty::POSE);
	vprop.push_back(VProperty::POS0);
	delete this->m_pSimMesh;
	this->m_pSimMesh = new TriMesh(vposx, vfacesOut, vprop);
	this->m_pSimMesh->setPoints(vpos0, VProperty::POS0);
	this->m_pSimMesh->setPoints(vposx, VProperty::POSE);

	// Compute discretization changes map

	// External changes -----------------
	
	vnodesExtO.resize(nvExtC);

	for (int i = 0; i < nvExtC; ++i)
	{
		vnodesExtO[i] = ModelPoint(3, vconIdx2NewIdx[vmapDefaultUnique[i]]);
	}

	// Internal changes -----------------

	int countC = nvExtC;
	iVector vnewIdxRaw;
	vnewIdxRaw.resize(this->m_nrawDOF, -1);
	for (set<ModelPoint>::iterator s_it = this->m_sdcs.begin(); s_it != this->m_sdcs.end(); ++s_it, countC++)
	{
		int offsetNew = 3 * vconIdx2NewIdx[vmapDefaultUnique[countC]];
		int offsetOld = 3 * (*s_it).getPointIdx();
		vnewIdxRaw[offsetOld + 0] = offsetNew + 0;
		vnewIdxRaw[offsetOld + 1] = offsetNew + 1;
		vnewIdxRaw[offsetOld + 2] = offsetNew + 2;
	}
	
	// Send discretization changed signal out
	this->onDiscretizationChanged(vnewIdxRaw);
}

void SurfaceModel2DRest::freeElements()
{
	SurfaceModel::freeElements();

	this->m_vpSeamBendingEles.clear();
}

iVector SurfaceModel2DRest::getAABBIndices(const dVector& vaabb) const
{
	// Compute AABB indices in simulation space... but in the
	// case of this model, simulation space and configuration
	// space might not be the same, so we have to transform.

	iVector vidxSim = NodalSolidModel::getAABBIndices(vaabb);

	int nn = (int)vidxSim.size()/3;
	iVector vsimNodeIdx(nn);
	iVector vrawNodeIdx(nn);
	for (int i = 0; i < nn; ++i)
		vsimNodeIdx[i] = vidxSim[3*i]/3;

	transformIndex(this->m_vmapWhole2Split, vsimNodeIdx, 0, vrawNodeIdx);

	iVector vidxRaw;
	for (int i = 0; i < nn; ++i)
		for (int j = 0; j < 3; ++j)
			vidxRaw.push_back(3*vrawNodeIdx[i] + j);

	return vidxRaw;
}