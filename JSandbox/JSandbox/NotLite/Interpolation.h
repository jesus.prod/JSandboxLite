/*=====================================================================================*/
/*!
\file		Interpolation.h
\author		Jes�s P�rez (jesus.prod@gmail.com)
*/
/*=====================================================================================*/

#ifndef INTERPOLATION_H
#define INTERPOLATION_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

class InterpolatedPoint
{
public:

	InterpolatedPoint()
	{
		this->m_dimension = -1;
		this->m_numSupport = -1;
	}

	InterpolatedPoint(int dimension, int numSupport)
	{
		assert(dimension > 0);
		assert(numSupport > 0);

		this->m_dimension = dimension;
		this->m_numSupport = numSupport;
	}

	virtual ~InterpolatedPoint()
	{
		// Nothing to do
	}

	InterpolatedPoint(const InterpolatedPoint& toCopy)
	{
		this->m_dimension = toCopy.m_dimension;
		this->m_numSupport = toCopy.m_numSupport;

		this->m_vindices = toCopy.m_vindices;
		this->m_vweights = toCopy.m_vweights;
	}

	int getDimension() const { return this->m_dimension; }
	int getNumSupport() const { return this->m_numSupport; }

	iVector& getIndices() { return this->m_vindices; }
	dVector& getWeights() { return this->m_vweights; }

	const iVector& getIndices() const { return this->m_vindices; }
	const dVector& getWeights() const { return this->m_vweights; }

	void setIndices(const iVector& vindices)
	{
		assert((int)vindices.size() == this->m_numSupport);
		this->m_vindices = vindices;
	}

	void setWeights(const dVector& vweights)
	{
		assert((int)vweights.size() == this->m_numSupport);
		this->m_vweights = vweights;
	}

	bool operator == (const InterpolatedPoint& other)
	{
		bool boolAnd = true;
		boolAnd &= this->m_dimension == other.m_dimension;
		boolAnd &= this->m_numSupport == other.m_numSupport;
		boolAnd &= this->m_vindices == other.m_vindices;
		boolAnd &= this->m_vweights == other.m_vweights;
		return boolAnd;
	}

	bool operator != (const InterpolatedPoint& other)
	{
		return !(*this == other);
	}

protected:

	//! The dimension of the point
	int m_dimension;

	//! The number of support points
	int m_numSupport;

	//! Indices of interpolating points
	iVector m_vindices;

	//! Weigths of interpolating points
	dVector m_vweights;

};

#endif