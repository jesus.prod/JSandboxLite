/*=====================================================================================*/
/*!
\file		BoundingVolumen.h
\author		jesusprod,bthomasz
\brief		Basic abstract class of a bounding volume to be used in a BVH. This is 
			adapted to this project from bthomasz implementation for Phys3D Collision 
			Detections Plugins.
*/
/*=====================================================================================*/

#ifndef BOUNDING_VOLUME_HH
#define BOUNDING_VOLUME_HH

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

class JSANDBOX_EXPORT BoundingVolume
{

public:
	BoundingVolume(void);
	virtual ~BoundingVolume(void);

	virtual bool overlaps(const BoundingVolume* bvIn, double tol = 1.0e-9) const = 0;
	virtual void merge(const BoundingVolume* bvIn) = 0;
	virtual void insert(const Vector3d& p) = 0;
	virtual void clear() = 0;

	virtual int getLongestDimension(double* pLength = NULL) const = 0;

	bool isEmpty() const { return m_empty; }

protected:
	bool m_empty;
};

#endif