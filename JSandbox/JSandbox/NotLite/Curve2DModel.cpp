/*=====================================================================================*/
/*!
\file		Curve2DModel.cpp
\author		jesusprod
\brief		Implementation of Curve2DModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/Curve2DModel.h>

#include <omp.h> // MP support

Curve2DModel::Curve2DModel() : NodalSolidModel("C2D")
{
	this->m_pvrestAng = NULL;
	this->m_pvrestLen = NULL;
}

Curve2DModel::~Curve2DModel()
{
	logSimu("[TRACE] Destroying Curve2DModel: %d", this);
}

void Curve2DModel::configureCurve(const dVector& vpos)
{
	assert((int)vpos.size() >= 2 && (int)vpos.size() % 2 == 0);
	this->m_vposRaw = vpos; // Inititalize raw boundary positions

	this->m_isReady_Setup = false;
}

void Curve2DModel::setup()
{
	int numv = ((int) this->m_vposRaw.size())/2;

	// DOF

	this->m_numDim_x = 2;
	this->m_numDim_0 = 2;

	this->m_numDimRaw = 2;

	this->m_numNodeRaw = numv;
	this->m_numNodeSim = numv;

	this->m_nsimDOF_x = this->m_nsimDOF_0 = this->m_nrawDOF = 2 * numv;

	this->m_state_x->m_vx.resize(this->m_nrawDOF);
	this->m_state_x->m_vv.resize(this->m_nrawDOF);
	this->m_state_x->m_vx.setZero();
	this->m_state_x->m_vv.setZero();

	this->m_state_0->m_vx = this->m_state_x->m_vx;

	this->m_state_x->m_vx = toEigen(this->m_vposRaw);
	this->m_state_0->m_vx = toEigen(this->m_vposRaw);

	// Initialize elements

	this->freeElements();

	// Initialize elements

	// Length

	for (int i = 0; i < numv; ++i)
	{
		int i0 = i;
		int i1 = (i + 1) % numv;

		iVector vinds;
		vinds.push_back(i0);
		vinds.push_back(i1);
		Curve2DEdgeElement* pEle = new Curve2DEdgeElement(2);
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to vector
		this->m_vpEles.push_back(pEle);
		this->m_vpEdgeEle.push_back(pEle);
	}

	// Angle

	for (int i = 0; i < numv; ++i)
	{
		int i1 = i;
		int i0 = (i - 1) % numv;
		int i2 = (i + 1) % numv;
		if (i0 < 0) // At 0 
			i0 = numv + i0;

		iVector vinds;
		vinds.push_back(i0);
		vinds.push_back(i1);
		vinds.push_back(i2);
		Curve2DAngleElement* pEle = new Curve2DAngleElement(2);
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);

		// Add element to vector
		this->m_vpEles.push_back(pEle);
		this->m_vpAngleEle.push_back(pEle);
	}

	this->initializeMaterials();

	this->m_isReady_Setup = true;
	this->deprecateDiscretization();

	// Update simulation 
	this->update_SimDOF();
}

void Curve2DModel::update_Rest()
{
	NodalSolidModel::update_Rest();

	if (this->m_pvrestLen)
	{
		assert(this->m_numNodeSim == (int) this->m_pvrestAng->size());
		int numEle = (int) this->m_vpEdgeEle.size();
		for (int i = 0; i < numEle; ++i)
			this->m_vpEdgeEle[i]->setRestLength(this->m_pvrestLen->at(i));
	}

	if (this->m_pvrestAng)
	{
		assert(this->m_numNodeSim == (int) this->m_pvrestLen->size());
		int numEle = (int) this->m_vpAngleEle.size();
		for (int i = 0; i < numEle; ++i)
			this->m_vpAngleEle[i]->setRestAngle(this->m_pvrestAng->at(i));
	}

	this->m_isReady_Mass = false;
}

void Curve2DModel::update_Mass()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mass)
		return; // Already done

	this->m_vMass.resize(this->m_nsimDOF_x);
	this->m_vMass.setZero(); // No mass used
	this->updateSparseMatrixToLumpedMass();

	this->m_isReady_Mass = true;
}

void Curve2DModel::freeElements()
{
	NodalSolidModel::freeElements();

	this->m_vpEdgeEle.clear();
	this->m_vpAngleEle.clear();
}