/*=====================================================================================*/
/*!
\file		TensileStructure.h
\author		jesusprod
\brief		TensileStructure.h implementation.
*/
/*=====================================================================================*/

#ifndef TENSILE_STRUCTURE_MODEL_2_H
#define TENSILE_STRUCTURE_MODEL_2_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/RodMesh.h>

#include <JSandbox/Model/RodMeshModel.h>
#include <JSandbox/Model/SurfaceModel.h>
#include <JSandbox/Model/CompositeModel.h>

#include <JSandbox/Geometry/TriMeshContourer.h>

enum AttachmentType
{
	Fixed,
	Wired
};

typedef struct RodAttachment
{
	RodAttachment()
	{
		this->m_vertIdx = -1;
		this->m_arcLen = -1.0;
		this->m_wireLen = -1.0;

		// Default attachment fixed to surface
		this->m_type = AttachmentType::Fixed;
	}

	RodAttachment(const RodAttachment& toCopy)
	{
		this->m_type = toCopy.m_type;
		this->m_arcLen = toCopy.m_arcLen;
		this->m_vertIdx = toCopy.m_vertIdx;
		this->m_vmodels = toCopy.m_vmodels;
		this->m_wireLen = toCopy.m_wireLen;
		this->m_mat = toCopy.m_mat;
	}

	~RodAttachment()
	{
		// Nothing to do here
	}

	// Rod position
	Real m_arcLen;
	int m_vertIdx;

	// Target models
	iVector m_vmodels;

	// For wired
	Real m_wireLen;
	SolidMaterial m_mat;

	// Is it fixed/wired?
	AttachmentType m_type;

} CageAttachment;



typedef struct TSCoupling
{

	TSCoupling()
	{
		// Nothing to do here
	}

	TSCoupling(const TSCoupling& toCopy)
	{
		this->m_coupling = toCopy.m_coupling;
	}

	~TSCoupling()
	{
		// Nothing to do here
	}

	// Coupling
	ModelCoupling m_coupling;
} 
TSCoupling;

class JSANDBOX_EXPORT TensileStructureModel : public CompositeModel
{
public:
	TensileStructureModel();

	virtual ~TensileStructureModel();

	/* From SolidModel */

	virtual void setup();

	/* From SolidModel */

	void setStateFromMesh(vector<RodMesh*> vpRodMeshx,
						  vector<RodMesh*> vpRodMesh0,
						  vector<TriMesh*> vpTriMesh);

	virtual void updateState0_Surface(int i, const VectorXd& vX);
	virtual void updateState0_RodMesh(int i, const VectorXd& vX);

	void deprecateConfiguration();
	void deprecateDiscretization();

	// Configure models individually

	virtual void clearSurfaceModels();
	virtual void clearRodMeshModels();
	virtual int getNumSurfaceModels() const;
	virtual int getNumRodMeshModels() const;
	virtual void addSurfaceModel(SurfaceModel* pModel);
	virtual void addRodMeshModel(RodMeshModel* pModel);
	virtual void setSurfaceModel(int i, SurfaceModel* pModel);
	virtual void setRodMeshModel(int i, RodMeshModel* pModel);

	virtual ModelCoupling* getTSCoupling(int i) { return this->m_vpTSCouplings[i]; }
	virtual void setTensileStructureCouplings(const vector<ModelCoupling*>& vcouplings);
	virtual int getNumTSCouplings() const { return (int)  this->m_vpTSCouplings.size(); }

	virtual void setRemeshSurfaceAtAttachments(bool r) { this->m_remeshSurfaceAtAttachments = r; }
	virtual void setRemeshRodMeshAtAttachments(bool r) { this->m_remeshRodMeshAtAttachments = r; }
	virtual void setSurfaceContourer(const TriMeshContourer& c) { this->m_surfMeshContour = c; }

	virtual void configureRodMeshAttachments(int i, const vector<vector<RodAttachment>>& vattach) { checkRodMeshModelIndex(i); this->m_vRodAttach[i] = vattach; }

	virtual TriMesh& getSurfaceMesh(int i) { this->checkSurfaceModelIndex(i); return this->m_vpSurfaceModel[i]->getSimMesh(); }

	virtual const RodMesh& getRodMeshMesh_x(int i) const { this->checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]->getSimMesh_x(); }
	virtual const RodMesh& getRodMeshMesh_0(int i) const { this->checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]->getSimMesh_0(); }

	virtual int getNumRod(int i) const { this->checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]->getNumRod(); }
	virtual const Rod& getRod_x(int i, int j) const { this->checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]->getSimRod_x(j); }
	virtual const Rod& getRod_0(int i, int j) const{ this->checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]->getSimRod_0(j); }

	virtual int getSurfaceModelOffset(int i) const { this->checkSurfaceModelIndex(i); return this->m_surfaceOffset + i; }
	virtual int getRodMeshModelOffset(int i) const { this->checkRodMeshModelIndex(i); return this->m_rodMeshOffset + i; }

	virtual int getSurfaceModelRawOffset(int i) const { this->checkSurfaceModelIndex(i); return this->m_vmodelRawIdx[this->m_surfaceOffset + i]; }
	virtual int getRodMeshModelRawOffset(int i) const { this->checkRodMeshModelIndex(i); return this->m_vmodelRawIdx[this->m_rodMeshOffset + i]; }

	virtual SurfaceModel* getSurfaceModel(int i) { this->checkSurfaceModelIndex(i); return this->m_vpSurfaceModel[i]; }
	virtual RodMeshModel* getRodMeshModel(int i) { this->checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]; }

	iVector getNodeRawDOF(int modelIdx, int nodeIdx);
	iVector getNodeSimDOF_x(int modelIdx, int nodeIdx);
	iVector getNodeSimDOF_0(int modelIdx, int nodeIdx);

	virtual int getNumNonZeros_DfxDX_Surface(int i) { checkSurfaceModelIndex(i); return this->m_vpSurfaceModel[i]->getNumNonZeros_DfxD0(); }
	virtual int getNumNonZeros_DfxDle_RodMesh(int i) { checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]->getNumNonZeros_DfxDle(); }
	virtual int getNumNonZeros_DfxDlt_RodMesh(int i) { checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]->getNumNonZeros_DfxDlr(); }
	virtual int getNumNonZeros_DfxDr_RodMesh(int i) { checkRodMeshModelIndex(i); return this->m_vpRodMeshModel[i]->getNumNonZeros_DfxDrr(); }

	virtual void add_fle_RodMesh(int i, VectorXd& vfl);
	virtual void add_flt_RodMesh(int i, VectorXd& vfl);
	virtual void add_fr_RodMesh(int i, VectorXd& vfr);

	virtual void add_DfxDX_Surface(int i, tVector& vJ);
	virtual void add_DfxDle_RodMesh(int i, tVector& vJ);
	virtual void add_DfxDlt_RodMesh(int i, tVector& vJ);
	virtual void add_DfxDr_RodMesh(int i, tVector& vJ);

	void getParam_LengthEdge(VectorXd& vl) const;
	void setParam_LengthEdge(const VectorXd& vl);

	void getParam_LengthTotal(VectorXd& vl) const;
	void setParam_LengthTotal(const VectorXd& vl);

	void getParam_RadiusEdge(VectorXd& vr) const;
	void setParam_RadiusEdge(const VectorXd& vr);

	virtual void getEigenvaluesSpaceRepresentation(int idx, const vector<EigenPair>& veigenPairs, vector<Vector3d>& vp, vector<Vector3d>& vv);

	const SparseMatrixXd& getSimToRodMeshMap(int i) { return this->m_vmSim2Rod[i]; }
	const SparseMatrixXd& getSimToSurfaceMap(int i) { return this->m_vmSim2Mem[i]; }

protected:

	virtual void updateMapMatrices();

	virtual bool remeshFromCouplings();
	virtual bool remeshFromCouplings_RodMeshes(int i);
	virtual bool remeshFromCouplings_Surfaces(int i);

	virtual void checkSurfaceModelIndex(int i) const { assert(i >= 0 && i < this->getNumSurfaceModels()); };
	virtual void checkRodMeshModelIndex(int i) const { assert(i >= 0 && i < this->getNumRodMeshModels()); };

	virtual void preSetupEmbeddedRodMesh(int iMesh);
	virtual void preSetupAttachedRodMesh(int iMesh);

	virtual void computeCouplings(vector<ModelCoupling*>& vpCouplings);

	vector<vector<vector<RodAttachment>>> m_vRodAttach;

	bool m_remeshSurfaceAtAttachments;
	bool m_remeshRodMeshAtAttachments;
	TriMeshContourer m_surfMeshContour;

	int m_surfaceOffset;
	int m_rodMeshOffset;
	vector<SurfaceModel*> m_vpSurfaceModel;
	vector<RodMeshModel*> m_vpRodMeshModel;

	vector<SparseMatrixXd> m_vmSim2Mem;
	vector<SparseMatrixXd> m_vmSim2Rod;

private:

	// Should not be used, use configureMesh instead to initialize the model (this composite is clearly defined)
	virtual void configureModels(const vector<SolidModel*>& vpModels, const vector<ModelCoupling*>& vpCouplings)
	{
		assert(false);
	}

	vector<ModelCoupling*> m_vpTSCouplings;

};

#endif