/*=====================================================================================*/
/*!
\file		CSTElement.h
\author		jesusprod,bthomasz
\brief		Basic implementation of Constant Strain Triangle element. Adapted from bthomasz
implementation for Phys3D DeformablesSurface plugin. This provides a continuous
based strain-stress response for diverse models. These use Lame coefficients as
parameters.
*/
/*=====================================================================================*/

// TODO: This class is still not working, do not use it until this message is removed

#ifndef CST_ELEMENT_H
#define CST_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>

#include <JSandbox/Element/NodalSolidElement.h>

typedef Real DNDx[3][2];

class CSTElement : public NodalSolidElement
{
public:
	CSTElement(int dim0);
	virtual ~CSTElement();

	virtual void update_Rest(const VectorXd& vX);
	virtual void update_Mass(const VectorXd& vX);

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv) = 0;
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv) = 0;

	virtual void update_DfxDx(const VectorXd& vx, const VectorXd& vX) = 0;
	virtual void update_DfxD0(const VectorXd& vx, const VectorXd& vX) = 0;
	virtual void update_DfxDs(const VectorXd& vx, const VectorXd& vX) = 0;

	virtual void update_DmxD0(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DmxDs(const VectorXd& vx, const VectorXd& vX);

	virtual void add_DfxDs(tVector& vDfxDs);
	virtual void add_DmxDs(tVector& vDmxds);

	virtual Real getRestArea() const { return this->m_A0; }

	virtual Real compute_Area_2D(const vector<Vector2d>& vx) const;
	virtual Real compute_Area_3D(const vector<Vector3d>& vx) const;
	virtual void compute_DNDx_2D(const vector<Vector2d>& vx, DNDx& DNDx) const;
	virtual void compute_DNDx_3D(const vector<Vector3d>& vx, DNDx& DNDx) const;

	virtual Real getIntegrationVolume() const { return this->m_A0; }

	virtual Real getThickness() const { return this->m_t0; }
	virtual void setThickness(Real t0) { this->m_t0 = t0; }

	virtual void computeN(const VectorXd& vX, DNDx& mDNDx);
	virtual void computeF(const VectorXd& vx, const VectorXd& vX, DNDx& mDxDX);
	virtual void computeC(const VectorXd& vx, const VectorXd& vX, Matrix2d& mC);
	virtual void computeLambdas(const VectorXd& vx, const VectorXd& vX, Real& L1, Real& L2, Vector2d& vL1, Vector2d& vL2);


protected:

	Real m_A0;		// Rest area	
	Real m_t0;		// Thinckness

	DNDx m_dNdx;	// DN/Dx

	MatrixXd m_mDmxDs;
	
	VectorXd m_vfs;
	MatrixXd m_mDfsDs;
	MatrixXd m_mDfxDs;

};

#endif