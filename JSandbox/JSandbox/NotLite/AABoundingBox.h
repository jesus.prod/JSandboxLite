/*=====================================================================================*/
/*!
\file		AABoundingBox.h
\author		jesusprod,bthomasz
\brief		Basic implementation of AABB volume. This is adapted to this project 
			from bthomasz implementation for Phys3D Collision Detections Plugins.
*/
/*=====================================================================================*/

#ifndef AA_BOUNDING_BOX_H
#define AA_BOUNDING_BOX_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

#include <JSandbox/Geometry/BoundingVolume.h>

class JSANDBOX_EXPORT AABoundingBox : public BoundingVolume
{

public:
	AABoundingBox(void);
	virtual ~AABoundingBox(void);

	// <BoundingVolume>

	bool overlaps(const BoundingVolume* bvIn, double tol = 1.0e-9) const;
	void merge(const BoundingVolume* bvIn);
	void insert(const Vector3d& p);
	void clear();

	int getLongestDimension(double* pLength = NULL) const;

	// </BoundingVolume>

	const Vector3d& getMaxIntervals() const { assert(!m_empty); return m_maxIntervals; }
	const Vector3d& getMinIntervals() const { assert(!m_empty); return m_minIntervals; }

private:
	Vector3d m_maxIntervals;
	Vector3d m_minIntervals;
};

#endif