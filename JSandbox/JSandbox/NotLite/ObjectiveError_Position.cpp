///*=====================================================================================*/
///*!
//\file		ObjectiveError_Position.cpp
//\author		jesusprod
//\brief		Implementation of ObjectiveError_Position.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Optim/ObjectiveError_Position.h>
//
//ObjectiveError_Position::ObjectiveError_Position() : ObjectiveError("POSITION")
//{
//	// Nothing to do here
//}
//
//ObjectiveError_Position::~ObjectiveError_Position()
//{
//	// Nothing to do here
//}
//
//const VectorXd& ObjectiveError_Position::getTargetPosition() const
//{
//	return this->m_vxt;
//}
//
//void ObjectiveError_Position::setTargetPosition(const VectorXd& vxt)
//{
//	assert((int)vxt.size() == this->m_pModel->getNumRawDOF());
//	const iVector& vsimDOF = this->m_pModel->getRawToSim_x();
//	int nrawDOF = this->m_pModel->getNumRawDOF();
//	int nsimDOF = this->m_pModel->getNumSimDOF_x();
//	this->m_vxt.resize(nsimDOF);
//	for (int i = 0; i < nrawDOF; ++i)
//	{
//		if (vsimDOF[i] == -1)
//			continue; // Ignore
//
//		this->m_vxt[vsimDOF[i]] = vxt[i];
//	}
//}
//
//Real ObjectiveError_Position::getError() const
//{
//	assert(this->m_pModel != NULL); // Initialized model
//
//	const VectorXd& vx = this->m_pModel->getPositions_x();
//	VectorXd vxD = vx - this->m_vxt;
//	return  0.5*vxD.dot(vxD);
//}
//
//void ObjectiveError_Position::add_DEDx(VectorXd& vDEDx) const
//{
//	assert(this->m_pModel != NULL); // Initialized model
//
//	const VectorXd& vx = this->m_pModel->getPositions_x();
//	vDEDx += vx - this->m_vxt; // Add distance gradient
//}
//
//void ObjectiveError_Position::add_D2EDx2(tVector& vD2EDx2) const
//{
//	assert(this->m_pModel != NULL); // Initialized model
//
//	int nsimDOF = this->m_pModel->getNumSimDOF_x();
//	for (int i = 0; i < nsimDOF; ++i) // Add identity
//		vD2EDx2.push_back(Triplet<Real>(i, i, 1.0));
//}
//
//void ObjectiveError_Position::setup()
//{
//	assert(this->m_pModel != NULL); // Initialized
//}