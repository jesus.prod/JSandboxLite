/*=====================================================================================*/
/*!
\file		SurfaceRemesherCutter.h
\author		jesusprod
\brief		Implementation of a surface remesher that cuts a surface.

*/
/*=====================================================================================*/


#ifndef SURFACE_REMESHER_CUT_H
#define SURFACE_REMESHER_CUT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/Geometry/SurfaceRemesher.h>

class JSANDBOX_EXPORT SurfaceRemesherCutter : public SurfaceRemesher
{
public:

	SurfaceRemesherCutter();
	virtual ~SurfaceRemesherCutter();

	virtual const RemeshOP& remesh(const TriMesh* pMesh, const BVHTree* pEdgeTree = NULL, const BVHTree* pFaceTree = NULL);

	virtual void setCutEdges(const vector<MeshTraits::ID>& ve) { this->m_vcutEdges = ve; }

	virtual const vector<MeshTraits::ID>& getCutEdges() const { return this->m_vcutEdges; }
	virtual const vector<MeshTraits::ID>& getCutVerts() const { return this->m_vcutVerts; }

	virtual const vector<map<MeshTraits::ID, MeshTraits::ID>>& getDuplicatesMap() const { return this->m_vdupsMap; }

protected:

	vector<MeshTraits::ID> m_vcutEdges;
	vector<MeshTraits::ID> m_vcutVerts;
	vector<map<MeshTraits::ID, MeshTraits::ID>> m_vdupsMap;
	

};

#endif