/*=====================================================================================*/
/*!
\file		SurfaceRemesher.h
\author		jesusprod
\brief		Basic abstract implementation of a surface remesher provinding some
common primitive operations for flipping and edge, splitting an edge
and other.

*/
/*=====================================================================================*/


#ifndef SURFACE_REMESHER_NEW_H
#define SURFACE_REMESHER_NEW_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

#include <JSandbox/Geometry/BVHTree.h>

class JSANDBOX_EXPORT SurfaceRemesherNew
{
public:

	SurfaceRemesherNew() {}
	virtual ~SurfaceRemesherNew() {}

	virtual bool remesh(TriMesh* pMesh, const BVHTree* pEdgeTree = NULL, const BVHTree* pFaceTree = NULL) = 0;

protected:

	/*!
	For a given face ABC this opertaion splits the face
	at the midpoint X. As a result, faces XAB, XBC, XCA
	are created.
	*/
	virtual bool splitFace(TriMesh::FaceHandle);

	/*!
	For a given edge AB with adjoint faces ABC and ADB,
	this operation splits the edge at its midpoint X. As
	a result faces XCA, XBC, XAD and XDB are created. If
	the edge is a boundary edge, only the two first faces
	are created.
	*/
	virtual bool splitEdge(TriMesh::EdgeHandle);

	/*!
	For a given edge AB with adjoint faces ABC and ADB,
	this operation removes the edge AB and introduces an
	edge CB with adjoint faces CDB and CAD. This operation
	is only valid if the edge is not a boundary edge.
	*/
	virtual bool flipEdge(TriMesh::EdgeHandle);

	// Utitilities ----------------------------------------------------------------------------------------------------

	//virtual bool filterIndependent(const vector<MeshTraits::ID>& vehIn, vector<MeshTraits::ID>& vehOut) const;
	//virtual bool isIndependentEdge(const MeshTraits::ID& eh, const vector<MeshTraits::ID>& veh) const;
	//virtual void updateActiveSet(vector<MeshTraits::ID>& vactive, const RemeshOP& op) const;
	//virtual RemeshOP appendOperations(const RemeshOP& op0, const RemeshOP& op1) const;
	//virtual RemeshOP createOperation() const;

	TriMesh* m_pMesh;

	//BVHTree* m_pEdgeTree;
	//BVHTree* m_pFaceTree;
	//RemeshOP m_remeshOP;

};

#endif