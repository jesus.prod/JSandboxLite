/*=====================================================================================*/
/*!
\file		DSEdgeElement.cpp
\author		jesusprod
\brief		Implementation of DSEdgeElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/DSEdgeElement.h>

DSEdgeElement::DSEdgeElement(int dim0) : NodalSolidElement(2, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);

	// This carries mass
	this->m_addMass = true;
}

DSEdgeElement::~DSEdgeElement()
{
	// Nothing to do here
}

void DSEdgeElement::update_Rest(const VectorXd& vX)
{
	if (this->m_numDim_0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX)*this->m_preStrain;
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX)*this->m_preStrain;
		this->m_L0 = (x1 - x0).norm();
	}
	else if (this->m_numDim_0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX)*this->m_preStrain;
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX)*this->m_preStrain;
		this->m_L0 = (x1 - x0).norm();
	}
	else assert(false);
}

void DSEdgeElement::update_Mass(const VectorXd& vX)
{
	if (!this->m_addMass)
		return; // Ignore

	double L0 = -1;

	if (this->m_numDim_0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX)*this->m_preStrain;;
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX)*this->m_preStrain;;
		L0 = (x1 - x0).norm();
	}
	else if (this->m_numDim_0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX)*this->m_preStrain;;
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX)*this->m_preStrain;;
		L0 = (x1 - x0).norm();
	}
	else assert(false);

	Real rho = this->m_vpmat[0]->getDensity();
	Real mu = this->m_vpmat[0]->getThickness();
	Real nodeMass = (1.0 / 2.0)*L0*rho*mu;
	for (int i = 0; i < this->m_numDOFx; ++i)
		this->m_vmx[i] = nodeMass;
}

void DSEdgeElement::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}
	double kS = m_vpmat[0]->getStretchK();
	double L = this->m_L0; // *this->m_preStrain;;

	#include "../../Maple/Code/DSEdgeEnergy.mcg"

	this->m_energy = t21;
}

void DSEdgeElement::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}
	double kS = m_vpmat[0]->getStretchK();
	double L = this->m_L0; // *this->m_preStrain;

	double vfx[6];

#include "../../Maple/Code/DSEdgeForce.mcg"

	for (int i = 0; i < 6; ++i)
		if (!isfinite(vfx[i]))
			logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 6; ++i)
		this->m_vfVal(i) = vfx[i];
}

void DSEdgeElement::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}
	double kS = m_vpmat[0]->getStretchK();
	double L = this->m_L0; // *this->m_preStrain;

	double mJx[6][6];

#include "../../Maple/Code/DSEdgeJacobian.mcg"

	for (int i = 0; i < 6; ++i)
		for (int j = 0; j < 6; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 6; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}

void DSEdgeElement::update_DfxDx(const VectorXd& vx, const VectorXd& vX)
{
	// Get material points

	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);

	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
		}
	}
	else assert(false); // WTF?

	double kS = m_vpmat[0]->getStretchK();
	double pS = this->getPreStrain();

	double mJx[6][6];

#include "../../Maple/Code/DSEdgeRest_DfxDx.mcg"

	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j < this->m_numDOFx; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store matrix

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_x; ++k)
			{
				this->m_mDfxDx(i, this->m_numDim_x*j + k) = mJx[i][this->m_numDim_x*j + k];
			}
		}
	}
}

void DSEdgeElement::update_DfxD0(const VectorXd& vx, const VectorXd& vX)
{
	// Get material points

	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);

	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
		}
	}
	else assert(false); // WTF?

	double kS = m_vpmat[0]->getStretchK();
	double pS = this->getPreStrain();

	double mJX[6][6];

#include "../../Maple/Code/DSEdgeRest_DfxD0.mcg"

	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j < this->m_numDOFx; ++j)
			if (!isfinite(mJX[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store matrix

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_0; ++k)
			{
				this->m_mDfxD0(i, this->m_numDim_0*j + k) = mJX[i][this->m_numDim_x*j + k];
			}
		}
	}
}

void DSEdgeElement::update_DmxD0(const VectorXd& vx, const VectorXd& vX)
{
	double mu = this->m_vpmat[0]->getDensity();
	double th = this->m_vpmat[0]->getThickness();
	double pS = this->m_preStrain;

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);
	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
		}
	}
	else assert(false); // WTF?

	double DmxDX[6];

#include "../../Maple/Code/DSEdgeRest_DmxD0.mcg"

	for (int i = 0; i < 6; ++i)
		if (!isfinite(DmxDX[i]))
			logSimu("In %s: DmxDX value is NAN\n", __FUNCDNAME__);

	this->m_mDmxD0.resize(this->m_numDOFx, this->m_numDOF0);

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_0; ++k)
			{
				this->m_mDmxD0(i, this->m_numDim_0*j + k) = DmxDX[this->m_numDim_x*j + k];
			}
		}
	}
}