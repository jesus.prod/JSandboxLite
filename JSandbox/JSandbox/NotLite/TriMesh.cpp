/*=====================================================================================*/
/*!
\file		TriMesh.cpp
\author		jesusprod
\brief		Implementation of TriMesh.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/TriMesh.h>

//#include <JSandbox/Optim/AlgLib_Sparse_BoxC_QP.h>
//#include <JSandbox/Optim/AlgLib_Sparse_EQIN_QP.h>
//#include <JSandbox/Optim/Mosek_Sparse_EQIN_QP.h>

//#include <igl/avg_edge_length.h>
//#include <igl/cotmatrix.h>
//#include <igl/invert_diag.h>
//#include <igl/massmatrix.h>
//#include <igl/parula.h>
//#include <igl/per_corner_normals.h>
//#include <igl/per_face_normals.h>
//#include <igl/per_vertex_normals.h>
//#include <igl/principal_curvature.h>
//#include <igl/read_triangle_mesh.h>

const string VProperty::VELE = "VELE";
const string VProperty::POSE = "POSE";

const string VProperty::POS0 = "POS0";
const string VProperty::VEL0 = "VEL0";

const string VProperty::POSP = "POSP";

const string VProperty::TARE = "TARE";

const string VPropGeo::POS3D = "POS3D";
const string VPropGeo::PAR2D = "PAR2D";

MeshTraits::ID TriMesh::m_IDCount = -1;

TriMesh::TriMesh(void)
{
	// Nothing to do here
}

TriMesh::TriMesh(const dVector& vpos, const iVector& vidx, const vector<string>& vps)
{
	this->init(vpos, vidx, vps);
}

void TriMesh::init(const dVector& vpos, const iVector& vidx, const vector<string>& vps)
{
	assert((int)vpos.size() >= 9);
	assert((int)vidx.size() >= 3);
	assert((int)vidx.size() % 3 == 0);

	int nv = (int)vpos.size() / 3;
	int nf = (int)vidx.size() / 3;

	// Initialize vertices

	for (int i = 0; i < nv; ++i)
	{
		int offset = 3 * i;
		OpenMesh::Vec3d p = OpenMesh::Vec3d(vpos[offset + 0],
											vpos[offset + 1],
											vpos[offset + 2]);
		this->add_vertex(p);
	}

	// Initialize faces

	vector<VertexHandle> vfv(3);
	for (int f = 0; f < nf; ++f)
	{
		int offset = 3 * f;
		for (int v = 0; v < 3; ++v)
			vfv[v] = this->vertex_handle(vidx[offset + v]);
		this->add_face(vfv);
	}

	// Store vertex properties

	this->m_vproperties = vps;
	int numProp = (int)vps.size();
	for (int i = 0; i < numProp; ++i)
	{
		OpenMesh::VPropHandleT<MeshTraits::Point> ph;
		this->add_property(ph, vps[i]); // Add property
	}
}

void TriMesh::initPlane(Real dimW, Real dimH, const vector<string>& vps)
{
	assert(dimW >= 2);
	assert(dimH >= 2);

	Real minX = -0.5;
	Real minZ = -0.5;

	int nsW = (int) (dimW-1);
	int nsH = (int) (dimH-1);
	int nv = (int) (dimW*dimH);
	int nf = 2*nsW*nsH;

	Real incX = 1.0 / nsW;
	Real incZ = 1.0 / nsH;

	dVector vpos(3*nv);
	iVector vidx(3*nf);

	int countv = 0;

	// Create vertices

	for (int i = 0; i < dimW; ++i)
	{
		for (int j = 0; j < dimH; ++j)
		{
			set3D(countv++, Vector3d(minX + i*incX, 0.0, minZ + j*incZ), vpos);
		}
	}

	// Create indices

	int counti = 0;

	for (int i = 0; i < nsW; ++i)
	{
		for (int j = 0; j < nsH; ++j)
		{
			vidx[counti++] = (int) (i*dimW + j); vidx[counti++] = (int)((i + 1)*dimW + j); vidx[counti++] = (int)((i + 1)*dimW + j + 1);
			vidx[counti++] = (int)(i*dimW + j); vidx[counti++] = (int)((i + 1)*dimW + j + 1); vidx[counti++] = (int)(i*dimW + j + 1);
		}
	}
	
	// Initialize vectors mesh
	this->init(vpos, vidx, vps);
}

void TriMesh::initSphere(Real dimW, Real dimH, const vector<string>& vps)
{
	// TODO

	throw new exception("Not implemeted yet...");
}

TriMesh::~TriMesh(void)
{
	for (map<string, BVHTree*>::iterator it = this->m_mBVHTreeEdge.begin(); it != this->m_mBVHTreeEdge.end(); ++it)
		delete (it->second);

	for (map<string, BVHTree*>::iterator it = this->m_mBVHTreeFace.begin(); it != this->m_mBVHTreeFace.end(); ++it)
		delete (it->second);

	logSimu("[TRACE] Destroying TriMesh: %d", this);
}

TriMesh::TriMesh(const TriMesh& toCopy) : BaseMesh(toCopy)
{
	// Copy point values

	m_vproperties = toCopy.getProperties();
	int numProp = toCopy.getNumProp();
	for (int i = 0; i < numProp; ++i)
	{
		OpenMesh::VPropHandleT<MeshTraits::Point> ph;
		this->add_property(ph, this->m_vproperties[i]);
	}

	for (int i = 0; i < numProp; ++i)
	{
		for (VertexIter v_it = this->vertices_begin(); v_it != this->vertices_end(); v_it++)
			this->setPoint(*v_it, toCopy.getPoint(*v_it, m_vproperties[i]), m_vproperties[i]);
	}
}

void TriMesh::TestEquivalence(const TriMesh* pMesh0, const TriMesh* pMesh1)
{
	// Check elements identifiers

	for (VertexIter v_it = pMesh0->vertices_begin(); v_it != pMesh0->vertices_end(); v_it++)
	{
		int vidx = (*v_it).idx();
		VertexHandle vh0 = pMesh0->vertex_handle(vidx);
		VertexHandle vh1 = pMesh0->vertex_handle(vidx);
		MeshTraits::ID id0 = pMesh0->getVertID(vh0);
		MeshTraits::ID id1 = pMesh0->getVertID(vh1);

		assert(vh0 == vh1);
		assert(id0 == id1);
	}

	for (EdgeIter e_it = pMesh0->edges_begin(); e_it != pMesh0->edges_end(); e_it++)
	{
		int eidx = (*e_it).idx();
		EdgeHandle eh0 = pMesh0->edge_handle(eidx);
		EdgeHandle eh1 = pMesh0->edge_handle(eidx);
		MeshTraits::ID id0 = pMesh0->getEdgeID(eh0);
		MeshTraits::ID id1 = pMesh0->getEdgeID(eh1);

		assert(eh0 == eh1);
		assert(id0 == id1);
	}

	for (FaceIter f_it = pMesh0->faces_begin(); f_it != pMesh0->faces_end(); f_it++)
	{
		int fidx = (*f_it).idx();
		FaceHandle fh0 = pMesh0->face_handle(fidx);
		FaceHandle fh1 = pMesh0->face_handle(fidx);
		MeshTraits::ID id0 = pMesh0->getFaceID(fh0);
		MeshTraits::ID id1 = pMesh0->getFaceID(fh1);

		assert(fh0 == fh1);
		assert(id0 == id1);
	}

	// Check property values distances

	int numProp0 = pMesh0->getNumProp();
	int numProp1 = pMesh1->getNumProp();
	assert(numProp0 == numProp1);

	const vector<string>& vps0 = pMesh0->getProperties();
	const vector<string>& vps1 = pMesh1->getProperties();

	for (int i = 0; i < numProp0; ++i)
	{
		assert(vps0[i].compare(vps1[i]) == 0);

		for (VertexIter v_it = pMesh0->vertices_begin(); v_it != pMesh0->vertices_end(); v_it++) // Check
			assert((pMesh0->getPoint(*v_it, vps0[i]) - pMesh1->getPoint(*v_it, vps1[i])).norm() < 1e-9);
	}

	// Check regular values

	for (VertexIter v_it = pMesh0->vertices_begin(); v_it != pMesh0->vertices_end(); v_it++)
		assert((pMesh0->getPoint(*v_it) - pMesh1->getPoint(*v_it)).norm() < 1e-9);  // Check
}

 int TriMesh::getNumProp() const
{
	return (int) this->m_vproperties.size();
}

 const vector<string>& TriMesh::getProperties() const
 {
	 return this->m_vproperties;
 }

bool TriMesh::hasProperty(const string& pname) const
{
	bool found = false;
	int numProps = this->getNumProp();
	for (int i = 0; i < numProps; ++i) // Traverse
		if (this->m_vproperties[i].compare(pname.c_str()) == 0)
		{
		found = true;
		break; // Out
		}
	return found;
}

MeshTraits::Point& TriMesh::point(VertexHandle vh, const string& pname)
{
	if (pname.empty()) // Get default
		return BaseMesh::point(vh);
	else 
	{
		assert(this->hasProperty(pname));

		OpenMesh::VPropHandleT<MeshTraits::Point> ph;
		this->get_property_handle(ph, pname);
		return this->property(ph, vh);
	}
}

const MeshTraits::Point& TriMesh::point(VertexHandle vh, const string& pname) const
{
	if (pname.empty()) // Get default
		return BaseMesh::point(vh);
	else
	{
		assert(this->hasProperty(pname));

		OpenMesh::VPropHandleT<MeshTraits::Point> ph;
		this->get_property_handle(ph, pname);
		return this->property(ph, vh);
	}
}

Vector3d TriMesh::getPoint(const VertexHandle& vh, const string& pname) const
{
	const Point& p = this->point(vh, pname);
	return Vector3d(p[0], p[1], p[2]); // Out
}

void TriMesh::setPoint(const VertexHandle& vh, const Vector3d& v, const string& pname)
{
	Point& p = this->point(vh, pname);
	p[0] = v.x();
	p[1] = v.y();
	p[2] = v.z();
}

Vector3d TriMesh::evaluatePoint(const ModelPoint& mp, const string& pname) const
{
	if (mp.getNumDim() != 3 && mp.getNumSup() != 3)
		throw new exception("[ERROR] Invalid point");

	if (mp.isExplicit())
	{
		// Retrieve specified index

		return this->getPoint(this->vertex_handle(mp.getPointIdx()), pname);
	}
	else
	{
		// Compute barycentric point

		Vector3d point;
		point.setZero();

		for (int i = 0; i < 3; ++i)
		{
			int idx = mp.getIndices()[i];
			Real wei = mp.getWeights()[i];
			point += wei*this->getPoint(this->vertex_handle(idx), pname);
		}

		return point;
	}
}

void TriMesh::getNormals(dVector& vn, bool flip, const string& pname) const
{
	int nv = (int) this->getNumNode();

	vn.resize(nv * 3);
	int C = (flip) ? -1 : 1;
	for (int i = 0; i < nv; ++i)
	{
		set3D(i, C*this->getNormal(this->vertex_handle(i), pname).normalized(), vn);
	}
}

Real TriMesh::computeSectorAngle(const HalfedgeHandle& hh, const string& pname) const
{
	Vector3d p0 = this->getPoint(this->to_vertex_handle(hh), pname);
	Vector3d pn = this->getPoint(this->to_vertex_handle(this->next_halfedge_handle(hh)), pname);
	Vector3d pp = this->getPoint(this->to_vertex_handle(this->prev_halfedge_handle(hh)), pname);

	Vector3d n0 = (p0 - pp).normalized();
	Vector3d n1 = (pn - p0).normalized();

	return acos(n0.dot(n1));
}

Real TriMesh::getFanAngle(const VertexHandle& vh, const string& pname) const
{
	Real angle = 0;

	if (is_boundary(vh))
	{
		for (TriMesh::CVIHIter it = this->cvih_begin(vh); it != this->cvih_end(vh);)
		{
			Real A = this->calc_sector_angle(*it);
			++it;
			if (it == this->cvih_end(vh))
				break;
			angle += A;
		}
	}
	else
	{
		for (TriMesh::CVIHIter it = this->cvih_begin(vh); it != this->cvih_end(vh); ++it)
		{
			angle += this->calc_sector_angle(*it);
		}
	}

	return angle;
}

Real TriMesh::getVoronoiArea(const VertexHandle& vh, const string& pname) const
{
	// Neighbourhood
	vector<FaceHandle> vvfh;
	this->getVertexFaces(vh, vvfh);

	Real A = 0.0;

	int nfNei = (int) vvfh.size();
	for (int i = 0; i < nfNei; ++i)
	{
		const FaceHandle& fh = vvfh[i];
		vector<HalfedgeHandle> vfhh(3);
		int j = 0;
		for (ConstFaceHalfedgeIter fh_it = this->cfh_begin(fh); fh_it != this->cfh_end(fh); ++fh_it, j++)
			vfhh[j] = *fh_it;

		dVector va(3);
		int obtuseIdx = -1;
		int centerIdx = -1;
		for (int j = 0; j < 3; ++j)
		{
			if (this->to_vertex_handle(vfhh[j]) == vh)
				centerIdx = j;

			va[j] = this->computeSectorAngle(vfhh[j], pname);
			if (va[j] > M_PI /2.0)
				obtuseIdx = j;
		}
		
		// Obtuse triangle
		if (obtuseIdx != -1)
		{
			if (obtuseIdx == centerIdx)
			{
				A += this->getFaceArea(fh, pname) / 2.0;
			}
			else
			{
				A += this->getFaceArea(fh, pname) / 4.0;
			}
		}
		else // Not obtuse
		{
			for (int j = 0; j < 3; ++j)
			{
				if (j == centerIdx)
					continue;

				int j0 = (j + 1) % 3;
				int j1 = (j + 2) % 3;
				Vector3d p0 = this->getPoint(this->to_vertex_handle(vfhh[j0]), pname);
				Vector3d p1 = this->getPoint(this->to_vertex_handle(vfhh[j1]), pname);
				A += (1.0 / 8.0)*(p0 - p1).squaredNorm()*(1.0 / tan(va[j]));
			}
		}
	}

	return A;
}

Vector3d TriMesh::getMeanCurvatureNormal(const VertexHandle& vh, const string& pname) const
{
	if (this->is_boundary(vh))
		return Vector3d(0,0,0);

	// Neighbourhood
	vector<EdgeHandle> vveh;
	this->getVertexEdges(vh, vveh);

	Vector3d vK;
	vK.setZero();

	int neNei = (int)vveh.size();
	for (int i = 0; i < neNei; ++i)
	{
		HalfedgeHandle heh0 = this->halfedge_handle(vveh[i], 0);
		HalfedgeHandle heh1 = this->halfedge_handle(vveh[i], 1);
		VertexHandle vh0 = this->to_vertex_handle(heh0);
		VertexHandle vh1 = this->to_vertex_handle(heh1);
		Vector3d p0 = this->getPoint(vh0, pname);
		Vector3d p1 = this->getPoint(vh1, pname);
		Vector3d ve;
		if (vh != vh0 && vh != vh1)
			throw new exception("What!?");
		else if (vh == vh0)
			ve = p0 - p1;
		else if (vh == vh1)
			ve = p1 - p0;

		Real Aij = this->computeSectorAngle(this->next_halfedge_handle(heh0), pname);
		Real Bij = this->computeSectorAngle(this->next_halfedge_handle(heh1), pname);

		//Real Aij = this->calc_sector_angle(heh0);
		//Real Bij = this->calc_sector_angle(heh1);

		vK += ve*(1.0 / tan(Aij));
		vK += ve*(1.0 / tan(Bij));
	}

	Real A = this->getVoronoiArea(vh, pname);

	vK /= 2;
	vK /= A;

	return vK*0.5;
}

Real TriMesh::getMeanCurvature(const VertexHandle& vh, const string& pname) const
{
	return this->getMeanCurvatureNormal(vh, pname).norm();
}

Real TriMesh::getGaussianCurvature(const VertexHandle& vh, const string& pname) const
{
	Real area = this->getVoronoiArea(vh, pname);
	Real angle = this->getFanAngle(vh, pname);
	return (2 * M_PI - angle) / area;
}

Vector2d TriMesh::getPrincipalCurvatures(const VertexHandle& vh, const string& pname) const
{
	Real kH = this->getMeanCurvature(vh, pname);
	Real kG = this->getGaussianCurvature(vh, pname);
	Real delta = kH*kH - kG;
	if (delta < 0) delta = 0;
	Vector2d vk;
	vk.x() = kH + sqrt(delta);
	vk.y() = kH - sqrt(delta);
	return vk;
}

void TriMesh::getFanAngles(dVector& va, const string& pname) const
{
	int nv = this->getNumNode();
	va.resize(nv);
	for (int i = 0; i < nv; ++i)
		va[i] = this->getFanAngle(this->vertex_handle(i), pname);
}

void TriMesh::getVoronoiAreas(dVector& va, const string& pname) const
{
	int nv = this->getNumNode();
	va.resize(nv);
	for (int i = 0; i < nv; ++i)
		va[i] = this->getVoronoiArea(this->vertex_handle(i), pname);
}

void TriMesh::getMeanCurvatures(dVector& vh, const string& pname) const
{
	int nv = this->getNumNode();
	vh.resize(nv);
	for (int i = 0; i < nv; ++i)
		vh[i] = this->getMeanCurvature(this->vertex_handle(i), pname);
}

void TriMesh::getGaussianCurvatures(dVector& vk, const string& pname) const
{
	int nv = this->getNumNode();
	vk.resize(nv);
	for (int i = 0; i < nv; ++i)
		vk[i] = this->getGaussianCurvature(this->vertex_handle(i), pname);
}

void TriMesh::getMeanCurvatureNormals(vector<Vector3d>& vh, const string& pname) const
{
	int nv = this->getNumNode();
	vh.resize(nv);
	for (int i = 0; i < nv; ++i)
		vh[i] = this->getMeanCurvatureNormal(this->vertex_handle(i), pname);
}

void TriMesh::getPrincipalCurvatures(vector<Vector2d>& vk, const string& pname) const
{
	int nv = this->getNumNode();
	vk.resize(nv);
	for (int i = 0; i < nv; ++i)
		vk[i] = this->getPrincipalCurvatures(this->vertex_handle(i), pname);
}

void TriMesh::getNodesMatrix(MatrixXd& V, const string& pname) const
{
	dVector vpos;
	this->getPoints(vpos, pname);
	int nv = this->getNumNode();
	
	V.resize(nv, 3);
	for (int i = 0; i < nv; ++i)
	{
		int offset = 3 * i;
		V.row(i).x() = vpos[offset + 0];
		V.row(i).y() = vpos[offset + 1];
		V.row(i).z() = vpos[offset + 2];
	}
}

void TriMesh::getFacesMatrix(MatrixXi& F, const string& pname) const
{
	iVector vidx;
	this->getIndices(vidx);
	int nf = this->getNumFace();

	F.resize(nf, 3);
	for (int i = 0; i < nf; ++i)
	{
		int offset = 3 * i;
		F.row(i).x() = vidx[offset + 0];
		F.row(i).y() = vidx[offset + 1];
		F.row(i).z() = vidx[offset + 2];
	}
}

//Real TriMesh::getAverageEdgeLength(const string& pname) const
//{
//	MatrixXd V;
//	MatrixXi F;
//	this->getNodesMatrix(V, pname);
//	this->getFacesMatrix(F, pname);
//
//	return igl::avg_edge_length(V, F);
//}
//
//void TriMesh::getVoronoiAreas_LIBIGL(dVector& va, const string& pname) const
//{
//	SparseMatrixXd M;
//
//	MatrixXd V;
//	MatrixXi F;
//	this->getNodesMatrix(V, pname);
//	this->getFacesMatrix(F, pname);
//	
//	igl::massmatrix(V, F, igl::MASSMATRIX_TYPE_VORONOI, M);
//
//	toSTL(M.diagonal(), va);
//}
//
//void TriMesh::getMeanCurvatures_LIBIGL_LB(dVector& vH, const string& pname) const
//{
//	SparseMatrixXd M;
//	SparseMatrixXd L;
//	SparseMatrixXd Mi;
//
//	MatrixXd V;
//	MatrixXi F;
//	this->getNodesMatrix(V, pname);
//	this->getFacesMatrix(F, pname);
//
//	igl::massmatrix(V, F, igl::MASSMATRIX_TYPE_VORONOI, M);
//	igl::invert_diag(M, Mi);
//	igl::cotmatrix(V, F, L);
//	MatrixXd MH = -Mi*(L*V);
//
//	toSTL(MH.rowwise().norm()*0.5, vH);
//}
//
//void TriMesh::getMeanCurvatures_LIBIGL_MK(dVector& vH, const string& pname) const
//{
//	// Principals mean
//
//	MatrixXd V;
//	MatrixXi F;
//	this->getNodesMatrix(V, pname);
//	this->getFacesMatrix(F, pname);
//
//	MatrixXd PD1, PD2;
//	VectorXd PV1, PV2;
//	igl::principal_curvature(V, F, PD1, PD2, PV1, PV2, 5U, false);
//
//	toSTL(0.5*(PV1 + PV2), vH);
//}
//
//void TriMesh::getGaussianCurvatures_LIBIGL(dVector& vK, const string& pname) const
//{
//	MatrixXd V;
//	MatrixXi F;
//	this->getNodesMatrix(V, pname);
//	this->getFacesMatrix(F, pname);
//
//	MatrixXd PD1, PD2;
//	VectorXd PV1, PV2;
//	igl::principal_curvature(V, F, PD1, PD2, PV1, PV2);
//
//	toSTL(PV1.cwiseProduct(PV2), vK);
//}
//
//void TriMesh::getMeanCurvatureNormals_LIBIGL(vector<Vector3d>& vh, const string& pname) const
//{
//	SparseMatrixXd M;
//	SparseMatrixXd L;
//	SparseMatrixXd Mi;
//
//	MatrixXd V;
//	MatrixXi F;
//	this->getNodesMatrix(V, pname);
//	this->getFacesMatrix(F, pname);
//
//	igl::massmatrix(V, F, igl::MASSMATRIX_TYPE_VORONOI, M);
//
//	//tVector vM;
//	//dVector vm;
//	//this->getVoronoiAreas(vm, pname);
//	//for (int i = 0; i < this->getNumNode(); ++i)
//	//	vM.push_back(Triplet<Real>(i, i, vm[i]));
//	//M.resize(this->getNumNode(), this->getNumNode());
//	//M.setFromTriplets(vM.begin(), vM.end());
//	//M.makeCompressed();
//
//	igl::invert_diag(M, Mi);
//	igl::cotmatrix(V, F, L);
//
//	MatrixXd MH = -Mi*(L*V);
//
//	int nv = this->getNumNode();
//	vh.resize(nv);
//	for (int i = 0; i < nv; ++i)
//		vh[i] = MH.row(i)*0.5;
//}
//
//void TriMesh::getPrincipalCurvatures_LIBIGL(vector<Vector2d>& vk, const string& pname) const
//{
//	MatrixXd V;
//	MatrixXi F;
//	this->getNodesMatrix(V, pname);
//	this->getFacesMatrix(F, pname);
//
//	MatrixXd PD1, PD2;
//	VectorXd PV1, PV2;
//	igl::principal_curvature(V, F, PD1, PD2, PV1, PV2);
//
//	int nv = this->getNumNode();
//	vk.resize(nv);
//	for (int i = 0; i < nv; ++i)
//	{
//		vk[i].x() = PV1(i);
//		vk[i].y() = PV2(i);
//	}
//}
//
//void TriMesh::getPrincipalCurvatures_LIBIGL(vector<vector<Vector3d>>& vk, const string& pname) const
//{
//	MatrixXd V;
//	MatrixXi F;
//	this->getNodesMatrix(V, pname);
//	this->getFacesMatrix(F, pname);
//
//	MatrixXd PD1, PD2;
//	VectorXd PV1, PV2;
//	igl::principal_curvature(V, F, PD1, PD2, PV1, PV2);
//
//	int nv = this->getNumNode();
//	vk.resize(nv);
//	for (int i = 0; i < nv; ++i)
//	{
//		vk[i].push_back(PD1.row(i));
//		vk[i].push_back(PD2.row(i));
//	}
//}

void TriMesh::getPoints(dVector& vp, const string& pname) const
{
	vp.resize(this->getNumNode() * 3);

	for (VertexIter v_it = vertices_begin(); v_it != vertices_end(); ++v_it)
	{
		const Point& p = point(*v_it, pname);
		int idx = (*v_it).idx();
		int offset = 3 * idx;
		vp[offset + 0] = p[0];
		vp[offset + 1] = p[1];
		vp[offset + 2] = p[2];
	}
}

void TriMesh::setPoints(const dVector& vx, const string& pname)
{
	assert(vx.size() == 3 * getNumNode());

	for (VertexIter v_it = vertices_begin(); v_it != vertices_end(); ++v_it)
	{
		Point& p = point(*v_it, pname);
		int idx = (*v_it).idx();
		int offset = 3 * idx;
		p[0] = vx[offset];
		p[1] = vx[offset + 1];
		p[2] = vx[offset + 2];
	}
}

void TriMesh::getEdgePoints(const EdgeHandle& eh, vector<Vector3d>& vev, const string& pname) const
{
	vev.resize(2);
	const Point& v0 = this->point(this->to_vertex_handle(this->halfedge_handle(eh, 0)), pname);
	const Point& v1 = this->point(this->to_vertex_handle(this->halfedge_handle(eh, 1)), pname);
	vev[0](0) = v0[0]; vev[0](1) = v0[1]; vev[0](2) = v0[2];
	vev[1](0) = v1[0]; vev[1](1) = v1[1]; vev[1](2) = v0[2];
}

void TriMesh::getFacePoints(const FaceHandle& fh, vector<Vector3d>& vfv, const string& pname) const
{
	vfv.resize(3);
	int vidx = 0;
	for (ConstFaceVertexIter fv_it = this->cfv_begin(fh); fv_it != this->cfv_end(fh); fv_it++)
	{
		const Point& v = this->point(*fv_it, pname);
		vfv[vidx](0) = v[0];
		vfv[vidx](1) = v[1];
		vfv[vidx](2) = v[2];
		vidx++;
	}
}

bool TriMesh::getHingePoints(const EdgeHandle& eh, vector<Vector3d>& vv, const string& pname) const
{
	if (!this->is_boundary(eh))
	{
		vv.resize(4);
		vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = this->halfedge_handle(eh, 1);
		vHalfEdges[1] = this->halfedge_handle(eh, 0);
		vHalfEdges[2] = this->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = this->next_halfedge_handle(vHalfEdges[0]);
		for (int i = 0; i < 4; ++i)
		{
			const Point& p = this->point(this->to_vertex_handle(vHalfEdges[i]), pname);
			vv[i](0) = p[0];
			vv[i](1) = p[1];
			vv[i](2) = p[2];
		}
		return true;
	}
	else // Boundary, return face
	{
		vv.resize(3);
		vector<TriMesh::HalfedgeHandle> vHalfEdges(3);
		vHalfEdges[0] = this->halfedge_handle(eh, 1);
		vHalfEdges[1] = this->halfedge_handle(eh, 0);
		vHalfEdges[2] = this->next_halfedge_handle(vHalfEdges[1]);
		for (int i = 0; i < 3; ++i)
		{
			const Point& p = this->point(this->to_vertex_handle(vHalfEdges[i]), pname);
			vv[i](0) = p[0];
			vv[i](1) = p[1];
			vv[i](2) = p[2];
		}
		return true;
	}

	return true;
}

double TriMesh::getFaceArea(const FaceHandle& fh, const string& pname) const
{
	return getFaceNormal(fh, pname).norm()*0.5;
}

Vector3d TriMesh::getNormal(const VertexHandle& vh, const string& pname) const
{
	Vector3d n;
	n.setZero();
	for (ConstVertexFaceIter vf_it = this->cvf_begin(vh); vf_it != this->cvf_end(vh); vf_it++)
		n += this->getFaceNormal(*vf_it, pname); // No normalized vector, already weighted
	return n;
}

Vector3d TriMesh::getUnitNormal(const VertexHandle& vh, const string& pname) const
{
	return getNormal(vh, pname).normalized();
}

Vector3d TriMesh::getFaceNormal(const FaceHandle& fh, const string& pname) const
{
	vector<Vector3d> vfv(3);
	int vidx = 0;
	for (ConstFaceVertexIter fv_it = this->cfv_begin(fh); fv_it != this->cfv_end(fh); fv_it++)
	{
		const Point& v = this->point(*fv_it, pname);
		vfv[vidx](0) = v[0];
		vfv[vidx](1) = v[1];
		vfv[vidx](2) = v[2];
		vidx++;
	}
	return (vfv[1] - vfv[0]).cross(vfv[2] - vfv[0]);
}

Vector3d TriMesh::getFaceUnitNormal(const FaceHandle& fh, const string& pname) const
{
	return getFaceNormal(fh, pname).normalized();
}

double TriMesh::getDihedralAngle(const EdgeHandle& eh, const string& pname) const
{
	// TODO: This might be further optimized to avoid calling

	if (this->is_boundary(eh))
		return 0.0; // No hinge

	vector<Vector3d> vev;
	getEdgePoints(eh, vev, pname);
	Vector3d e = vev[1] - vev[0];
	double norme = e.norm();
	if (isApprox(norme, 0.0, EPS_APPROX))
		return 0.0; // Collapsed edge
	else e /= norme;

	Vector3d n0 = this->getFaceNormal(this->face_handle(this->halfedge_handle(eh, 0)), pname);
	Vector3d n1 = this->getFaceNormal(this->face_handle(this->halfedge_handle(eh, 1)), pname);

	// TODO: Consider if we should unwrap the dihedral
	// angle here of just return the angle as it is. The
	// reference angle may change depending on the purpose
	// of the mesh itself...

	double cos = n0.dot(n1);
	double sin = e.dot(n0.cross(n1));
	return atan2(sin, cos); // Not unwrapped
}

TriMesh::VertexHandle TriMesh::getClosestVertex(const Vector3d& p, Real& D, const string& pname) const
{
	D = HUGE_VAL;
	VertexHandle closest;
	for (VertexIter v_it = this->vertices_begin(); v_it != this->vertices_end(); ++v_it)
	{
		Vector3d v = this->getPoint(*v_it, pname);;
		Real distance = (v - p).norm();
		if (distance < D)
		{
			// Update
			D = distance;
			closest = *v_it;
		}
	}

	//D = sqrt(D);

	return closest;
}

Matrix3d TriMesh::getCurvature(const FaceHandle& fh, const string& pname) const
{
	// TODO: This might be further optimized to avoid calling

	// This implementation is based on the basic tri-averaged
	// operator appeared in GrinspunEtAl06: "Computing discrete
	// shape operators on general meshes", Eq. 6

	Matrix3d H;
	H.setZero();

	double area = this->getFaceArea(fh, pname);

	// Traverse edge iter
	for (ConstFaceEdgeIter fe_it = cfe_begin(fh); fe_it != cfe_end(fh); fe_it++)
	{
		const EdgeHandle eh = *fe_it;

		vector<Vector3d> vev;
		getEdgePoints(eh, vev);
		Vector3d e = vev[0] - vev[1];
		double norme = e.norm();
		if (isApprox(norme, 0.0, EPS_APPROX))
			continue; // Zero contribution
		else e /= norme;

		// Compute dihedral angle and proyection matrix
		Vector3d n0 = this->getFaceNormal(this->face_handle(this->halfedge_handle(eh, 0)), pname);
		Vector3d n1 = this->getFaceNormal(this->face_handle(this->halfedge_handle(eh, 1)), pname);
		double angle = atan2(e.dot(n0.cross(n1)), n0.dot(n1));
		Vector3d t = e.cross(0.5*(n0 + n1));
		Matrix3d T = t*t.transpose();

		H += (angle / norme)*T;
	}

	return (0.5*H) / area;
}

dVector TriMesh::getAABBRanges(const string& pname) const
{
	dVector bb = this->getAABBExtremes(pname);
	dVector out(3);
	out[0] = bb[1] - bb[0];
	out[1] = bb[3] - bb[2];
	out[2] = bb[5] - bb[4];
	return out;
}

dVector TriMesh::getAABBExtremes(const string& pname) const
{
	dVector out(6);
	out[0] = out[2] = out[4] = HUGE_VAL;
	out[1] = out[3] = out[5] = -HUGE_VAL;

	for (VertexIter v_it = this->vertices_begin(); v_it != this->vertices_end(); v_it++)
	{
		Vector3d v = this->getPoint(*v_it, pname);
		if (v.x() < out[0]) out[0] = v.x();
		if (v.x() > out[1]) out[1] = v.x();
		if (v.y() < out[2]) out[2] = v.y();
		if (v.y() > out[3]) out[3] = v.y();
		if (v.z() < out[4]) out[4] = v.z();
		if (v.z() > out[5]) out[5] = v.z();
	}

	return out;
}

vector<dVector> TriMesh::getAABBExtremesCC(const string& pname) const
{
	vector<vector<TriMesh::FaceHandle>> vfhs;
	vector<vector<TriMesh::VertexHandle>> vvhs;
	this->getPartitionConnectedComponents(vfhs, vvhs);

	int numCC = (int) vfhs.size();
	assert((int)vvhs.size() == numCC);

	vector<dVector> vout(numCC);
	for (int i = 0; i < numCC; ++i)
	{
		dVector& vaabb = vout[i];
		vaabb.resize(6);
		vaabb[0] = vaabb[2] = vaabb[4] = HUGE_VAL;
		vaabb[1] = vaabb[3] = vaabb[5] = -HUGE_VAL;

		for (int j = 0; j < (int)vvhs[i].size(); ++j) 
		{
			const TriMesh::VertexHandle& vh = vvhs[i][j];
			Vector3d v = this->getPoint(vh, pname);
			if (v.x() < vaabb[0]) vaabb[0] = v.x();
			if (v.x() > vaabb[1]) vaabb[1] = v.x();
			if (v.y() < vaabb[2]) vaabb[2] = v.y();
			if (v.y() > vaabb[3]) vaabb[3] = v.y();
			if (v.z() < vaabb[4]) vaabb[4] = v.z();
			if (v.z() > vaabb[5]) vaabb[5] = v.z();
		}
	}

	return vout;
}

Real TriMesh::getClosestVertexDistance(const string& pname) const
{
	Real minDist = HUGE_VAL;
	for (TriMesh::ConstVertexIter v_it0 = this->vertices_begin(); v_it0 != this->vertices_end(); ++v_it0)
	{
		for (TriMesh::ConstVertexIter v_it1 = this->vertices_begin(); v_it1 != this->vertices_end(); ++v_it1)
		{
			if (*v_it0 == *v_it1)
				continue; // Self

			Real dist = (this->getPoint(*v_it0, pname) - this->getPoint(*v_it1, pname)).squaredNorm();
			if (dist < minDist)
				minDist = dist;
		}
	}

	minDist = sqrt(minDist);

	return minDist;
}

void TriMesh::testOverlappedVertices(Real value, const string& pname) const
{
	double dist = this->getClosestVertexDistance(pname);
	if (dist < value)
	{
		logSimu("[ERROR] Triangle mesh vertex overlapped %.9f", dist);
		//exit(-1); // Critical error should not happen, exit program
	}
}

void TriMesh::testIsolatedVertices(const string& pname) const
{
	for (TriMesh::VertexIter vit = this->vertices_begin(); vit != this->vertices_end(); ++vit)
	{
		if (this->is_isolated(*vit))
		{
			logSimu("[ERROR] Triangle mesh vertex isolated");
			//exit(-1); // Critical error should not happen
		}
	}
}

void TriMesh::testInvertedTriangles(const string& pname) const
{
	for (TriMesh::FaceIter fit = this->faces_begin(); fit != this->faces_end(); ++fit)
	{
		TriMesh::FaceHandle fh = *fit;
		vector<TriMesh::FaceHandle> vfh;
		this->getFaceFaces(fh, vfh);

		Vector3d n = this->getFaceNormal(fh, pname);

		bool inverted = false;
		int nf = (int)vfh.size();
		for (int i = 0; i < nf; ++i)
		{
			Vector3d nein = this->getFaceNormal(vfh[i], pname);
			inverted |= (n.dot(nein) < 0.0); // Not inverted
		}

		if (inverted)
		{
			logSimu("[ERROR] Triangle mesh face inverted");
			//exit(-1); // Critical error should not happen
		}
	}
}

void TriMesh::getIndices(iVector& vidx) const
{
	int nf = (int) this->getNumFace();
	vidx.resize(3 * nf);
	int offset = 0;
	for (ConstFaceIter f_it = this->faces_begin(); f_it != this->faces_end(); f_it++)
	{
		int i = 0;
		for (ConstFaceVertexIter fv_it = this->cfv_begin(*f_it); fv_it != this->cfv_end(*f_it); fv_it++, i++)
			vidx[offset + i] = (*fv_it).idx();
		offset += 3;
	}
}

void TriMesh::getVertexVertices(const VertexHandle& vh, vector<VertexHandle>& vvh) const
{
	vvh.clear();
	for (ConstVertexVertexIter vv_it = this->cvv_begin(vh); vv_it != this->cvv_end(vh); vv_it++)
		vvh.push_back(*vv_it);
}

void TriMesh::getVertexEdges(const VertexHandle& vh, vector<EdgeHandle>& veh) const
{
	veh.clear();
	for (ConstVertexEdgeIter ve_it = this->cve_begin(vh); ve_it != this->cve_end(vh); ve_it++)
		veh.push_back(*ve_it);
}

void TriMesh::getVertexFaces(const VertexHandle& vh, vector<FaceHandle>& vfh) const
{
	vfh.clear();
	for (ConstVertexFaceIter vf_it = this->cvf_begin(vh); vf_it != this->cvf_end(vh); vf_it++)
		vfh.push_back(*vf_it);
}

void TriMesh::getEdgeVertices(const EdgeHandle& eh, vector<VertexHandle>& vhv) const
{
	vhv.resize(2);
	vhv[0] = this->to_vertex_handle(this->halfedge_handle(eh, 0));
	vhv[1] = this->to_vertex_handle(this->halfedge_handle(eh, 1));
}

bool TriMesh::getVerticesEdge(const vector<VertexHandle>& vehv, EdgeHandle& eh) const
{
	assert((int) vehv.size() == 2);

	for (ConstVertexEdgeIter cve_it = this->cve_begin(vehv[0]); cve_it != this->cve_end(vehv[0]); ++cve_it)
	{
		EdgeHandle ehtest = *cve_it;
		vector<VertexHandle> vehvOther;
		this->getEdgeVertices(ehtest, vehvOther);
		if ((vehv[0] == vehvOther[0] && vehv[1] == vehvOther[1]) ||
			(vehv[0] == vehvOther[1] && vehv[1] == vehvOther[0]))
		{
			eh = ehtest;
			return true;
		}
	}

	return false;
}

void TriMesh::getEdgeFaces(const EdgeHandle& eh, vector<FaceHandle>& vhv) const
{
	if (this->is_boundary(eh))
	{
		vhv.resize(1);
		vhv[0] = this->face_handle(this->halfedge_handle(eh, 0));
	}
	else
	{
		vhv.resize(2);
		vhv[0] = this->face_handle(this->halfedge_handle(eh, 0));
		vhv[1] = this->face_handle(this->halfedge_handle(eh, 1));
	}

}

void TriMesh::getFaceVertices(const FaceHandle& fh, vector<VertexHandle>& vhv) const
{
	vhv.resize(3);
	int vidx = 0;
	for (ConstFaceVertexIter fv_it = this->cfv_begin(fh); fv_it != this->cfv_end(fh); fv_it++)
		vhv[vidx++] = *fv_it;
}

bool TriMesh::getVerticesFace(const vector<VertexHandle>& vfhv, FaceHandle& fh) const
{
	throw new exception("[ERROR] Not implemented yet");

	return false;
}

void TriMesh::getFaceEdges(const FaceHandle& fh, vector<EdgeHandle>& vhe) const
{
	vhe.resize(3);
	int eidx = 0;
	for (ConstFaceEdgeIter fe_it = this->cfe_begin(fh); fe_it != this->cfe_end(fh); fe_it++)
		vhe[eidx++] = *fe_it;
}

void TriMesh::getFaceFaces(const FaceHandle& fh, vector<FaceHandle>& vhf) const
{
	for (ConstFaceFaceIter ff_it = this->cff_begin(fh); ff_it != this->cff_end(fh); ff_it++)
		vhf.push_back(*ff_it);
}

bool TriMesh::getHingeVertices(const EdgeHandle& eh, vector<VertexHandle>& vhv) const
{
	vector<VertexHandle> vvh;
	this->getEdgeVertices(eh, vvh);

	if (!this->is_boundary(eh))
	{
		vhv.resize(4);
		vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = this->halfedge_handle(eh, 0);
		vHalfEdges[1] = this->halfedge_handle(eh, 1);
		vHalfEdges[2] = this->next_halfedge_handle(vHalfEdges[0]);
		vHalfEdges[3] = this->next_halfedge_handle(vHalfEdges[1]);;
		vhv[0] = this->to_vertex_handle(vHalfEdges[0]);
		vhv[1] = this->to_vertex_handle(vHalfEdges[1]);
		vhv[2] = this->to_vertex_handle(vHalfEdges[2]);
		vhv[3] = this->to_vertex_handle(vHalfEdges[3]);
		return true;
	}
	else // Boundary, return face
	{
		vhv.resize(3);
		vector<TriMesh::HalfedgeHandle> vHalfEdges(3);
		vHalfEdges[0] = this->halfedge_handle(eh, 0);
		vHalfEdges[1] = this->halfedge_handle(eh, 1);
		vHalfEdges[2] = this->next_halfedge_handle(vHalfEdges[0]);
		vhv[0] = this->to_vertex_handle(vHalfEdges[0]);
		vhv[1] = this->to_vertex_handle(vHalfEdges[1]);
		vhv[2] = this->to_vertex_handle(vHalfEdges[2]);
		return false;
	}
}

void TriMesh::getRegionFacePartition(const vector<EdgeHandle>& veh, vector<FaceHandle>& vfh0, vector<FaceHandle>& vfh1) const
{
	int ne = (int) this->getNumEdge();
	int nf = (int) this->getNumFace();

	bVector vfacesVisit(nf);
	for (int i = 0; i < nf; ++i)
		vfacesVisit[0] = false;

	bVector vedgesVisit(ne);
	for (int i = 0; i < ne; ++i)
		vedgesVisit[0] = false;

	// Avoid going through region limit edges
	for (int i = 0; i < (int)veh.size(); ++i)
		vedgesVisit[veh[i].idx()] = true;

	iVector vfacePile;

	vector<FaceHandle> vfh;
	this->getEdgeFaces(veh[0], vfh);
	vfacePile.push_back(vfh[0].idx());

	// Traverse face

	while ((int)vfacePile.size() > 0)
	{
		int nextFace = vfacePile.front();
		vfacePile.erase(vfacePile.begin());

		vfacesVisit[nextFace] = true; // Set visited once
		const FaceHandle& fh = this->face_handle(nextFace);

		vector<EdgeHandle> vfeh;
		this->getFaceEdges(fh, vfeh);
		for (int i = 0; i < 3; ++i)
		{
			const EdgeHandle& eh = vfeh[i];

			if (vedgesVisit[eh.idx()])
				continue; // Visited

			vedgesVisit[eh.idx()] = true;

			if (this->is_boundary(eh))
				continue; // No other

			FaceHandle theOtherFace;
			vector<FaceHandle> vefh;

			this->getEdgeFaces(vfeh[i], vefh);
			if (vefh[0] == fh) theOtherFace = vefh[1];
			else if (vefh[1] == fh) theOtherFace = vefh[0];
			else assert(false); // Should not happen, weirdo!

			vfacePile.push_back(theOtherFace.idx()); // Push
		}
	}

	vfh0.clear();
	vfh1.clear();
	for (int i = 0; i < nf; ++i)
		if (vfacesVisit[i])
			vfh0.push_back(this->face_handle(i));
		else vfh1.push_back(this->face_handle(i));
}

void TriMesh::getPartitionConnectedComponents(vector<vector<FaceHandle>>& vfhs, vector<vector<VertexHandle>>& vvhs) const
{
	int nf = (int) this->getNumFace();
	int nv = (int) this->getNumNode();

	vfhs.clear();
	vvhs.clear();

	bVector vfacesVisit(nf);
	for (int i = 0; i < nf; ++i)
		vfacesVisit[0] = false;

	bVector vverticesVisit(nv);
	for (int i = 0; i < nv; ++i)
		vverticesVisit[0] = false;

	int first;
	do
	{
		first = -1;
		for (int i = 0; i < nf; ++i)
			if (!vfacesVisit[i])
			{
			// Start at i
			first = i;
			break;
			}

		// Out of faces
		if (first == -1)
			break; // Out

		// Traverse connected

		vfhs.push_back(vector<FaceHandle>());
		vvhs.push_back(vector<VertexHandle>());
		vector<FaceHandle>& vcurfCC = vfhs.back();
		vector<VertexHandle>& vcurvCC = vvhs.back();

		iVector vfacePile;
		vfacePile.push_back(first);
		while ((int)vfacePile.size() > 0)
		{
			int nextFace = vfacePile.front();
			vfacePile.erase(vfacePile.begin());

			if (vfacesVisit[nextFace])
				continue; // Visited

			vfacesVisit[nextFace] = true; // Set visited once
			const FaceHandle& fh = this->face_handle(nextFace);

			vector<VertexHandle> vvh;
			this->getFaceVertices(fh, vvh);
			for (int i = 0; i < 3; ++i)
				if (!vverticesVisit[vvh[i].idx()])
				{
				vverticesVisit[vvh[i].idx()] = true;
				vcurvCC.push_back(vvh[i]); // Added
				}

			vcurfCC.push_back(fh);

			vector<FaceHandle> vffh;
			this->getFaceFaces(fh, vffh);
			int nff = (int)vffh.size();
			for (int i = 0; i < nff; ++i)
			{
				int fidx = vffh[i].idx();

				if (vfacesVisit[fidx])
					continue; // Visited
				vfacePile.push_back(fidx);
			}
		}
	} while (true);
}

void TriMesh::getPartitionBoundaryEdgeRegions(const vector<EdgeHandle>& veh, vector<vector<FaceHandle>>& vfhs) const
{
	int ne = (int) this->getNumEdge();
	int nf = (int) this->getNumFace();

	vfhs.clear();

	bVector vfacesVisit(nf, false);
	bVector vedgesVisit(ne, false);

	// Avoid going through region limit edges
	for (int i = 0; i < (int)veh.size(); ++i)
		vedgesVisit[veh[i].idx()] = true;

	while (true)
	{
		int first = -1;

		for (int i = 0; i < nf; ++i)
			if (!vfacesVisit[i])
			{
			// Starting
			first = i;
			break;
			}

		if (first == -1)
			break; // Out

		vfhs.push_back(vector<FaceHandle>());
		vector<FaceHandle>& vcurfReg = vfhs.back();

		iVector vfacePile;
		vfacePile.push_back(first);
		while (!vfacePile.empty())
		{
			int nextFace = vfacePile.front();
			vfacePile.erase(vfacePile.begin());

			if (vfacesVisit[nextFace])
				continue; // Already

			vfacesVisit[nextFace] = true; // Set visited once and continue
			const TriMesh::FaceHandle& fh = this->face_handle(nextFace);

			vcurfReg.push_back(fh);

			vector<TriMesh::EdgeHandle> vfeh;
			this->getFaceEdges(fh, vfeh);
			for (int i = 0; i < 3; ++i)
			{
				const TriMesh::EdgeHandle& eh = vfeh[i];

				if (vedgesVisit[eh.idx()])
					continue; // Visited

				if (this->is_boundary(eh))
					continue; // No other

				TriMesh::FaceHandle theOtherFace;
				vector<TriMesh::FaceHandle> vefh;

				this->getEdgeFaces(vfeh[i], vefh);
				if (vefh[0] == fh) theOtherFace = vefh[1];
				else if (vefh[1] == fh) theOtherFace = vefh[0];
				else assert(false); // Should not happen, weirdo!

				if (!vfacesVisit[theOtherFace.idx()]) // Push
					vfacePile.push_back(theOtherFace.idx());
			}
		}
	}
}

void TriMesh::getBoundaryComponents_Simple(vector<EdgeHandle>& vehs, vector<VertexHandle>& vvhs) const
{
	vehs.clear();
	vvhs.clear();
	vehs.reserve(this->getNumEdge());
	vvhs.reserve(this->getNumNode());

	for (VertexIter v_it = this->vertices_begin(); v_it != this->vertices_end(); ++v_it)
		if (this->is_boundary(*v_it))
			vvhs.push_back(*v_it);

	for (EdgeIter e_it = this->edges_begin(); e_it != this->edges_end(); ++e_it)
		if (this->is_boundary(*e_it))
			vehs.push_back(*e_it);
}

void TriMesh::getBoundaryComponents_Sorted(vector<vector<EdgeHandle>>& vehs, vector<vector<VertexHandle>>& vvhs) const
{
	int nv = (int) this->getNumNode();

	vehs.clear();
	vvhs.clear();

	bVector vverticesVisit(nv);
	for (int i = 0; i < nv; ++i)
	{
		VertexHandle& vh = this->vertex_handle(i);
		vverticesVisit[i] = !this->is_boundary(vh);
	}

	int first;
	do
	{
		first = -1;
		for (int i = 0; i < nv; ++i)
		{
			if (!vverticesVisit[i])
			{
				// Start at i
				first = i;
				break;
			}
		}

		if (first == -1)
			break; // Out

		// Traverse connected

		iVector vvertexPile;
		vvertexPile.push_back(first);

		vehs.push_back(vector<EdgeHandle>());
		vvhs.push_back(vector<VertexHandle>());
		vector<EdgeHandle>& vcureLoop = vehs.back();
		vector<VertexHandle>& vcurvLoop = vvhs.back();

		while ((int)vvertexPile.size() > 0)
		{
			int nextVert = vvertexPile.front();
			vvertexPile.erase(vvertexPile.begin());

			vverticesVisit[nextVert] = true; // Set visited once
			const VertexHandle& vh = this->vertex_handle(nextVert);
			vcurvLoop.push_back(vh);

			vector<EdgeHandle> veh;
			this->getVertexEdges(vh, veh);
			int nvedges = (int)veh.size();
			for (int i = 0; i < nvedges; ++i)
			{
				const EdgeHandle& eh = veh[i];
				if (!this->is_boundary(eh))
					continue;

				vector<VertexHandle> vevh;
				getEdgeVertices(eh, vevh);
				int idx0 = vevh[0].idx();
				int idx1 = vevh[1].idx();

				if (!vverticesVisit[idx0])
				{
					vvertexPile.push_back(idx0);
					vverticesVisit[idx0] = true;
					vcureLoop.push_back(eh);
					break;
				}

				if (!vverticesVisit[idx1])
				{
					vvertexPile.push_back(idx1);
					vverticesVisit[idx1] = true;
					vcureLoop.push_back(eh);
					break;
				}
			}
		}

		vector<VertexHandle> vevh(2);
		vevh[0] = vcurvLoop.front();
		vevh[1] = vcurvLoop.back();
		EdgeHandle closer;
		if (!this->getVerticesEdge(vevh, closer))
			throw new exception("[ERROR] What?");
		vcureLoop.push_back(closer);

		assert(vcurvLoop.size() == vcureLoop.size());

	} while (true);
}

void TriMesh::getBoundaryComponents_Smooth(vector<vector<vector<EdgeHandle>>>& vehs, vector<vector<vector<VertexHandle>>>& vvhs, Real angle, const string& pname) const
{
	assert(angle >= 0 && angle < M_PI);
	vector<vector<EdgeHandle>> vehsSorted;
	vector<vector<VertexHandle>> vvhsSorted;
	this->getBoundaryComponents_Sorted(vehsSorted, vvhsSorted);
	assert((int) vehsSorted.size() == (int) vvhsSorted.size());

	// Split each component boundary
	int numCC = (int) vehsSorted.size();
	vehs.resize(numCC);
	vvhs.resize(numCC);
	for (int i = 0; i < numCC; ++i)
	{
		int numV = (int) vvhsSorted[i].size();
		int numE = (int) vehsSorted[i].size();
		assert(numE == numV);

		vehs[i].push_back(vector<EdgeHandle>());
		vvhs[i].push_back(vector<VertexHandle>());
		vehs[i].back().reserve(numE);
		vvhs[i].back().reserve(numV);
		vehs[i].back().push_back(vehsSorted[i][0]); // Edge 0 
		vvhs[i].back().push_back(vvhsSorted[i][0]); // Vertex 0
		vvhs[i].back().push_back(vvhsSorted[i][1]); // Vertex 1

		Vector3d v0p = getPoint(vvhsSorted[i][0], pname);
		Vector3d v1p = getPoint(vvhsSorted[i][1], pname);
		Vector3d v2p;
		Vector3d n0 = (v1p - v0p).normalized();
		Vector3d n1;
		for (int j = 0; j < numE; ++j)
		{
			// Compare the angle with next edge

			int v1 = (j != numE - 1) ? j + 1 : 0;
			int v2 = (j != numE - 2) ? j + 2 : 0;

			v2p = getPoint(vvhsSorted[i][v2], pname);

			n1 = (v2p - v1p).normalized();
			if (acos(n0.dot(n1)) > angle)
			{
				// Not smooth initiate new one
				vehs[i].push_back(vector<EdgeHandle>());
				vvhs[i].push_back(vector<VertexHandle>());
				vehs[i].back().reserve(numE);
				vvhs[i].back().reserve(numV);
				vehs[i].back().push_back(vehsSorted[i][j]); // Edge 0 
				vvhs[i].back().push_back(vvhsSorted[i][v1]); // Vertex 0
				vvhs[i].back().push_back(vvhsSorted[i][v2]); // Vertex 1
			}
			else
			{
				vehs[i].back().push_back(vehsSorted[i][j]);
				vvhs[i].back().push_back(vvhsSorted[i][v2]);
			}
			
			// Next
			n0 = n1;
			v0p = v1p;
			v1p = v2p;
		}
	}
}

void TriMesh::getSplittedMeshMaps(vector<iVector>& vmapSplit2Whole, vector<iVector>& vmapWhole2Split, map<int, int>& mapEdges) const
{
	// Build vertices maps

	int nVert = (int) this->getNumNode();

	vmapSplit2Whole.clear();
	vmapWhole2Split.clear();

	vmapSplit2Whole.resize(nVert);

	set<int> setVisitedVerts;

	for (ConstVertexIter v_it0 = this->vertices_begin(); v_it0 != this->vertices_end(); ++v_it0)
	{
		VertexHandle vh0 = *v_it0;
		Vector3d p0 = getPoint(vh0);
		int idx0 = vh0.idx();

		if (setVisitedVerts.find(idx0) != setVisitedVerts.end())
			continue; // Already visited, and so added to the whole

		setVisitedVerts.insert(idx0); // Visited

		vmapWhole2Split.push_back(iVector()); // New
		int wholeIdx = (int)vmapWhole2Split.size() - 1;
		iVector& vSplitVerts = vmapWhole2Split.back();
		vSplitVerts.push_back(idx0);

		if (this->is_boundary(vh0))
		{
			for (ConstVertexIter v_it1 = this->vertices_begin(); v_it1 != this->vertices_end(); ++v_it1)
			{
				VertexHandle vh1 = *v_it1;
				Vector3d p1 = getPoint(vh1);
				int idx1 = vh1.idx();

				if (setVisitedVerts.find(idx1) != setVisitedVerts.end())
					continue; // Already visited, and so added to the whole

				if (!this->is_boundary(vh1))
					continue; // Not boundary

				if ((p0 - p1).norm() < EPS_APPROX)
				{
					setVisitedVerts.insert(idx1);
					vSplitVerts.push_back(idx1);
				}
			}
		}

		int nSplit = (int)vSplitVerts.size();
		for (int iSplit = 0; iSplit < nSplit; ++iSplit)
			vmapSplit2Whole[vSplitVerts[iSplit]].push_back(wholeIdx);
	}

	// Build edges map

	TriMesh::BuildSplittedMeshEdgesMap(this, vmapSplit2Whole, vmapWhole2Split, mapEdges);
}

MeshTraits::ID TriMesh::getVertID(const VertexHandle& vh) const
{
	OpenMesh::VPropHandleT<MeshTraits::ID> vph;
	this->get_property_handle(vph, "ID");
	return this->property(vph, vh);
}

TriMesh::VertexHandle TriMesh::getVertHandle(const MeshTraits::ID& id) const
{
	return this->m_mvertIDs.at(id);
}

MeshTraits::ID TriMesh::getFaceID(const FaceHandle& fh) const
{

	OpenMesh::FPropHandleT<MeshTraits::ID> fph;
	this->get_property_handle(fph, "ID");
	return this->property(fph, fh);
}

TriMesh::FaceHandle TriMesh::getFaceHandle(const MeshTraits::ID& id) const
{
	return this->m_mfaceIDs.at(id);
}

MeshTraits::ID TriMesh::getEdgeID(const EdgeHandle& eh) const
{
	OpenMesh::EPropHandleT<MeshTraits::ID> eph;
	this->get_property_handle(eph, "ID");
	return this->property(eph, eh);
}

TriMesh::EdgeHandle TriMesh::getEdgeHandle(const MeshTraits::ID& id) const
{
	return this->m_medgeIDs.at(id);
}

bool TriMesh::hasVertID(const MeshTraits::ID& id) const
{
	return this->m_mvertIDs.find(id) != this->m_mvertIDs.end();
}

bool TriMesh::hasEdgeID(const MeshTraits::ID& id) const
{
	return this->m_medgeIDs.find(id) != this->m_medgeIDs.end();
}

bool TriMesh::hasFaceID(const MeshTraits::ID& id) const
{
	return this->m_mfaceIDs.find(id) != this->m_mfaceIDs.end();
}

void TriMesh::setVertID(const VertexHandle& vh, const MeshTraits::ID& id)
{
	OpenMesh::VPropHandleT<MeshTraits::ID> vph;
	this->get_property_handle(vph, "ID");
	this->property(vph, vh) = id;

	std::pair<std::map<MeshTraits::ID, VertexHandle>::iterator, bool> ret;
	ret = this->m_mvertIDs.insert(std::pair<MeshTraits::ID, VertexHandle>(id, vh));
	if (!ret.second)
		this->m_mvertIDs[id] = vh; // Already exist, overwrite its value
}

void TriMesh::setFaceID(const FaceHandle& fh, const MeshTraits::ID& id)
{
	OpenMesh::FPropHandleT<MeshTraits::ID> fph;
	this->get_property_handle(fph, "ID");
	this->property(fph, fh) = id;

	std::pair<std::map<MeshTraits::ID, FaceHandle>::iterator, bool> ret;
	ret = this->m_mfaceIDs.insert(std::pair<MeshTraits::ID, FaceHandle>(id, fh));
	if (!ret.second)
		this->m_mfaceIDs[id] = fh; // Already exist, overwrite its value
}

void TriMesh::setEdgeID(const EdgeHandle& eh, const MeshTraits::ID& id)
{
	OpenMesh::EPropHandleT<MeshTraits::ID> eph;
	this->get_property_handle(eph, "ID");
	this->property(eph, eh) = id;

	std::pair<std::map<MeshTraits::ID, EdgeHandle>::iterator, bool> ret;
	ret = this->m_medgeIDs.insert(std::pair<MeshTraits::ID, EdgeHandle>(id, eh));
	if (!ret.second)
		this->m_medgeIDs[id] = eh; // Already exist, overwrite its value
}

void TriMesh::updateIdentifiersMap()
{
	this->m_mfaceIDs.clear();
	this->m_medgeIDs.clear();
	this->m_mvertIDs.clear();

	for (VertexIter v_it = this->vertices_begin(); v_it != this->vertices_end(); v_it++)
		this->setVertID(*v_it, this->getVertID(*v_it));

	for (EdgeIter e_it = this->edges_begin(); e_it != this->edges_end(); e_it++)
		this->setEdgeID(*e_it, this->getEdgeID(*e_it));

	for (FaceIter f_it = this->faces_begin(); f_it != this->faces_end(); f_it++)
		this->setFaceID(*f_it, this->getFaceID(*f_it));
}

MeshTraits::ID TriMesh::CreateIdentifier()
{
	return ++TriMesh::m_IDCount; // Unique
}

void TriMesh::BuildSplittedMeshVerticesMap(const TriMesh* pWholeMesh, const TriMesh* pSplittedMesh,
										 const vector<map<MeshTraits::ID, MeshTraits::ID>>& vdupsMap,
										 vector<iVector>& vmapSplitToWhole, vector<iVector>& vmapWholeToSplit)
{
	int nReg = (int)vdupsMap.size();

	vmapWholeToSplit.resize(pWholeMesh->getNumNode());
	vmapSplitToWhole.resize(pSplittedMesh->getNumNode());

	for (TriMesh::ConstVertexIter v_it = pWholeMesh->vertices_begin(); v_it != pWholeMesh->vertices_end(); ++v_it)
	{
		const MeshTraits::ID& oldid = pWholeMesh->getVertID(*v_it);

		vector<MeshTraits::ID> vdup;
		for (int iReg = 0; iReg < nReg; ++iReg)
			if (vdupsMap[iReg].find(oldid) != vdupsMap[iReg].end())
				vdup.push_back(vdupsMap[iReg].at(oldid)); // Append

		int nDup = (int)vdup.size();
		if (nDup == 0) // Use old one
		{
			vmapWholeToSplit[(*v_it).idx()].push_back(pSplittedMesh->getVertHandle(oldid).idx());
		}
		else // Use new identifiers
		{
			for (int iDup = 0; iDup < nDup; ++iDup)
				vmapWholeToSplit[(*v_it).idx()].push_back(vdup[iDup]);
		}
	}

	int nGloVert = (int)vmapWholeToSplit.size();
	for (int iVert = 0; iVert < nGloVert; ++iVert)
	{
		int nDups = (int)vmapWholeToSplit[iVert].size();
		for (int iDup = 0; iDup < nDups; ++iDup) // Invert indices map
			vmapSplitToWhole[vmapWholeToSplit[iVert][iDup]].push_back(iVert);
	}
}

void TriMesh::BuildSplittedMeshEdgesMap(const TriMesh* pSplittedMesh,
									    const vector<iVector>& vmapSplitToWhole,
									    const vector<iVector>& vmapWholeToSplit,
									    map<int, int>& mapBoundaryEdges)
{
	mapBoundaryEdges.clear();

	for (ConstEdgeIter e_it = pSplittedMesh->edges_begin(); e_it != pSplittedMesh->edges_end(); ++e_it)
	{
		if (!pSplittedMesh->is_boundary(*e_it))
			continue; // Only boundary edges

		const EdgeHandle& eh = *e_it;

		vector<VertexHandle> vvh; // Mapped verts
		pSplittedMesh->getEdgeVertices(eh, vvh);

		int idxSplit0 = vvh[0].idx();
		int idxSplit1 = vvh[1].idx();
		const iVector& vidxSplitConn0 = vmapWholeToSplit[vmapSplitToWhole[idxSplit0][0]];
		const iVector& vidxSplitConn1 = vmapWholeToSplit[vmapSplitToWhole[idxSplit1][0]];
		int nConn0 = (int)vidxSplitConn0.size();
		int nConn1 = (int)vidxSplitConn1.size();

		if (nConn0 == 1 && nConn1 == 1)
			continue; // Not a seam edge

		bool found = false;

		for (int i0 = 0; i0 < nConn0; ++i0)
		{
			if (nConn0 != 1 && vidxSplitConn0[i0] == idxSplit0)
				continue; // Same as in input edge and not unique

			int idxSplitConn0 = vidxSplitConn0[i0];

			vector<EdgeHandle> veh; // Get circular edges to look for all neighbor vertices
			pSplittedMesh->getVertexEdges(pSplittedMesh->vertex_handle(idxSplitConn0), veh);
			int nRingEdge = (int)veh.size();

			for (int i1 = 0; i1 < nConn1; ++i1)
			{
				if (nConn1 != 1 && vidxSplitConn1[i1] == idxSplit1)
					continue; // Same as in input edge and not unique

				int idxSplitConn1 = vidxSplitConn1[i1];

				for (int iRingEdge = 0; iRingEdge < nRingEdge; ++iRingEdge)
				{
					const EdgeHandle& ehOther = veh[iRingEdge];

					vector<VertexHandle> vvh;
					pSplittedMesh->getEdgeVertices(ehOther, vvh);
					if ((vvh[0].idx() == idxSplitConn1 && vvh[1].idx() == idxSplitConn0) ||
						(vvh[0].idx() == idxSplitConn0 && vvh[1].idx() == idxSplitConn1))
					{
						assert(pSplittedMesh->is_boundary(eh));
						assert(pSplittedMesh->is_boundary(ehOther));
						found = true; // Finally found, map and out
						mapBoundaryEdges[eh.idx()] = ehOther.idx();
						break;
					}
				}

				if (found)
					break;
			}

			if (found)
				break;
		}
	}
}

void TriMesh::getColorCC(dVector& vcol) const
{
	int nv = (int) this->getNumNode();

	vcol.resize(3*nv); // Default
	for (int i = 0; i < 3*nv; ++i)
		vcol[i] = 255.0; // White

	vector<vector<TriMesh::FaceHandle>> vfhs;
	vector<vector<TriMesh::VertexHandle>> vvhs;
	this->getPartitionConnectedComponents(vfhs, vvhs);
	int nCC = (int)vfhs.size(); // Connected components
	double alpha = 1.0 / (double)(nCC - 1);
	for (int i = 0; i < nCC; ++i)
	{
		dVector vrgb = computeHeatColorRGB(i*alpha);

		int nf = (int)vfhs[i].size();

		for (int f = 0; f < nf; ++f)
		{
			vector<TriMesh::VertexHandle> vvh;
			getFaceVertices(vfhs[i][f], vvh);

			for (int i = 0; i < 3; ++i)
			{
				int offset = 3*vvh[i].idx();
				for (int j = 0; j < 3; ++j)
				{
					// Colorize white vertices
					if (vcol[offset + j] == 255.0)
						vcol[offset + j] = vrgb[j];
				}
			}
		}
	}
}

void TriMesh::getPointEmbedding(const Vector3d& p, Real maxD, PointPro& pP, const string& pname)
{
	// TODO

	assert(false);
}

void TriMesh::getPointsEmbedding(const vector<Vector3d>& vp, Real maxD, vector<PointPro>& vpP, const string& pname)
{
	int nPoints = (int)vp.size();

	vpP.clear();
	vpP.resize(nPoints);
	for (int i = 0; i < nPoints; ++i)
		vpP[i].m_valid = false; // Init.

	// Retrieve (creating one if necessary a face BVH tree)
	const BVHTree* meshTree = this->getBVHTree_Face(pname);

	BVHTree	pointCloudTree(1); // BVH
	pointCloudTree.initializeCloud(vp);

	vector<BVHClosePrimitivePair> vcolls; // Get potential point projections
	BVHTree::testRecursive(meshTree, &pointCloudTree, 0, 0, vcolls, maxD);

	dVector vminDist(nPoints);
	for (int i = 0; i < nPoints; ++i)
		vminDist[i] = HUGE_VAL; // Init.

	int nPotColl = (int)vcolls.size(); // Project points
	for (int iPotColl = 0; iPotColl < nPotColl; iPotColl++)
	{
		int faceIdx = vcolls[iPotColl].m_prim0.m_index;
		int pointIdx = vcolls[iPotColl].m_prim1.m_index;

		Vector3d point = vp[pointIdx];
		vector<TriMesh::VertexHandle> vvh; // Get the mesh face!
		this->getFaceVertices(this->face_handle(faceIdx), vvh);
		Vector3d v0 = this->getPoint(vvh[0], pname);
		Vector3d v1 = this->getPoint(vvh[1], pname);
		Vector3d v2 = this->getPoint(vvh[2], pname);

		Real l0, l1;
		Real distance = sqrDistanceVertexTriangle(point, v0, v1, v2, &l0, &l1);
		if (distance >= 0.0 && distance < maxD) // Consider projection distance
		{
			Vector3d projp = v0*(1 - l0 - l1) + v1*l0 + v2*l1;
			double D = (projp - point).squaredNorm(); // Dist

			if (vminDist[pointIdx] <= D)
				continue; // Already closer
			vminDist[pointIdx] = D; // update

			vpP[pointIdx].m_D = D;
			vpP[pointIdx].m_oriPoint = point;
			vpP[pointIdx].m_proPoint = projp;
			vpP[pointIdx].m_valid = true;
			dVector vwei;
			iVector vidx;
			vidx.push_back(vvh[0].idx());
			vidx.push_back(vvh[1].idx());
			vidx.push_back(vvh[2].idx());
			vwei.push_back(1 - l0 - l1);
			vwei.push_back(l0);
			vwei.push_back(l1);
			//if (isApprox(vwei[0], 1.0, EPS_POS)) // Explicit
			//{
			//	vpP[pointIdx].m_point = ModelPoint(3, vidx[0]);
			//}
			//else if (isApprox(vwei[1], 1.0, EPS_POS)) // Explicit
			//{
			//	vpP[pointIdx].m_point = ModelPoint(3, vidx[1]);
			//}
			//else if (isApprox(vwei[2], 1.0, EPS_POS)) // Explicit
			//{
			//	vpP[pointIdx].m_point = ModelPoint(3, vidx[2]);
			//}
			//else // Embedded point
			//{
			//	vpP[pointIdx].m_point = ModelPoint(3, vidx, vwei);
			//}
			vpP[pointIdx].m_point = ModelPoint(3, vidx, vwei);
		}
	}
}

void TriMesh::getSortedBoundaryIndices(iVector& vboundList, iVector& vslaveList) const
{
	// Build sorted boundary/slave list

	vboundList.reserve(this->getNumNode());
	vslaveList.reserve(this->getNumNode());

	vector<vector<TriMesh::VertexHandle>> vvhs;
	vector<vector<TriMesh::EdgeHandle>> vehs;
	getBoundaryComponents_Sorted(vehs, vvhs);

	for (int j = 0; j < (int)vvhs.size(); ++j)
		for (int k = 0; k < (int)vvhs[j].size(); ++k)
			vboundList.push_back(vvhs[j][k].idx());

	bVector vboundaryStencil(this->getNumNode(), false);
	for (int i = 0; i < (int) vboundList.size(); ++i)
		vboundaryStencil[vboundList[i]] = true;

	for (int i = 0; i < this->getNumNode(); ++i)
		if (!vboundaryStencil[i])
			vslaveList.push_back(i);
}

void TriMesh::getSortedBoundaryPoints(dVector& vpos, const string& pname) const
{
	iVector vboundList;
	iVector vslaveList;
	this->getSortedBoundaryIndices(vboundList, vslaveList);

	vpos.resize((int) vboundList.size() * 3);

	for (int i = 0; i < (int)vboundList.size(); ++i)
	{
		set3D(i, this->getPoint(this->vertex_handle(vboundList[i]), pname), vpos);
	}
}

void TriMesh::setSortedBoundaryPoints(const dVector& vpos, const string& pname)
{
	iVector vboundList;
	iVector vslaveList;
	this->getSortedBoundaryIndices(vboundList, vslaveList);

	for (int i = 0; i < (int)vboundList.size(); ++i)
	{
		this->setPoint(this->vertex_handle(vboundList[i]), get3D(i, vpos), pname);
	}
}

//void TriMesh::computeBoundaryToMinimalMap_OptimizedCotangent(SparseMatrixXd& mHB, const string& pname, int DIM)
//{
//	dVector vedgeWeights;
//	this->computeLaplacianWeights_Cotangent(vedgeWeights, pname);
//	this->computeBoundaryToMinimalMap_Optimized(vedgeWeights, mHB, DIM);
//}

//void TriMesh::computeBoundaryToMinimalMap_OptimizedRegular(SparseMatrixXd& mHB, const string& pname, int DIM)
//{
//	dVector vedgeWeights;
//	this->computeLaplacianWeights_Regular(vedgeWeights, pname);
//	this->computeBoundaryToMinimalMap_Optimized(vedgeWeights, mHB, DIM);
//}

void TriMesh::computeBoundaryToMinimalMap_LaplacianCotangent(SparseMatrixXd& mHB, const string& pname, int DIM)
{
	dVector vedgeWeights;
	this->computeLaplacianWeights_Cotangent(vedgeWeights, pname);
	this->computeBoundaryToMinimalMap_Laplacian(vedgeWeights, mHB, DIM);
}

void TriMesh::computeBoundaryToMinimalMap_LaplacianRegular(SparseMatrixXd& mHB, const string& pname, int DIM)
{
	dVector vedgeWeights;
	this->computeLaplacianWeights_Regular(vedgeWeights, pname);
	this->computeBoundaryToMinimalMap_Laplacian(vedgeWeights, mHB, DIM);
}

//void TriMesh::computeBoundaryToMinimalMap_Optimized(const dVector& vedgeWeights, SparseMatrixXd& mHB, int DIM)
//{
//	// Based on the work: 
//
//	int numD = DIM;
//	int numV = this->getNumNode();
//	int numE = this->getNumEdge();
//
//	iVector vboundList;
//	iVector vslaveList;
//	this->getSortedBoundaryIndices(vboundList, vslaveList);
//	int numB = (int)vboundList.size();
//	int numS = (int)vslaveList.size();
//	iVector vfull2bound(numV, -1);
//	iVector vfull2slave(numV, -1);
//	int countBound = 0;
//	int countSlave = 0;
//	for (int i = 0; i < numB; ++i)
//		vfull2bound[vboundList[i]] = countBound++;
//	for (int i = 0; i < numS; ++i)
//		vfull2slave[vslaveList[i]] = countSlave++;
//
//	// Assemble Laplacian stiffness matrix
//
//	tVector vL;
//
//	dVector vm(numV, 0.0);
//
//	for (TriMesh::ConstEdgeIter e_it = this->edges_begin(); e_it != this->edges_end(); e_it++)
//	{
//		vector<TriMesh::VertexHandle> vvh;
//		this->getEdgeVertices(*e_it, vvh);
//
//		double w = vedgeWeights[(*e_it).idx()];
//		int idx0 = vvh[0].idx();
//		int idx1 = vvh[1].idx();
//
//		assert(idx0 != idx1);
//
//		vL.push_back(Triplet<Real>(idx0, idx0, w));
//		vL.push_back(Triplet<Real>(idx1, idx1, w));
//		vL.push_back(Triplet<Real>(idx0, idx1, -w));
//		vL.push_back(Triplet<Real>(idx1, idx0, -w));
//
//		// Add Voronoi
//		vm[idx0] += w;
//		vm[idx1] += w;
//	}
//
//	// Assemble inverse lumped mass matrixx
//
//	tVector vM;
//
//	vM.reserve(numV);
//	for (int i = 0; i < numV; ++i) // Invert diagonal
//		vM.push_back(Triplet<Real>(i, i, 1.0 / vm[i]));
//
//	// Compute QP quadratic matrix
//
//	SparseMatrixXd mM(numV, numV);
//	SparseMatrixXd mL(numV, numV);
//	mM.setFromTriplets(vM.begin(), vM.end());
//	mL.setFromTriplets(vL.begin(), vL.end());
//	mM.makeCompressed();
//	mL.makeCompressed();
//
//	SparseMatrixXd mG = mL*mM*mM*mL;
//
//	// Prepare solver parameters
//
//	tVector vG;
//	toTriplets(mG, vG);
//
//	VectorXd vx(numV);
//	vx.setZero();
//
//	VectorXd vc(numV);
//	vc.setZero();
//
//	iVector vCT(numB, 0);
//
//	VectorXd vb(numB);
//	vb.setZero();
//
//	VectorXd vl(numV);
//	VectorXd vu(numV);
//	vl.setZero();
//	vu.setOnes();
//
//	tVector vA;
//	vA.reserve(numB);
//	for (int i = 0; i < numB; ++i)
//		vA.push_back(Triplet<Real>(i, vboundList[i], 1.0));
//
//	// Compute binding weights
//
//	MatrixXd mBTemp(numV, numB);
//	for (int i = 0; i < numB; ++i)
//	{
//		// i-th master 1.0, rest 0.0
//
//		vb.setZero();
//		vb(i) = 1.0;
//
//		string error;
//
//		VectorXd vxo(numV);
//
//		Mosek_Sparse_EQIN_QP solver;
//		if (!solver.solve(vG, vc, vA, vb, vCT, vx, vl, vu, vxo, error))
//			throw new exception("[ERROR] Impossible to solve binding");
//
//		// Copy computed harmonic weights
//		for (int j = 0; j < numV; ++j)
//			mBTemp(j, i) = vxo(j);
//
//		logSimu("[TRACE] Finished harmonic binding for handle %d", i);
//	}
//
//	// Normalize weigths
//
//	MatrixXd mBTempRaw = mBTemp;
//
//	for (int i = 0; i < numV; ++i)
//	{
//		Real weiSum = 0;
//
//		for (int j = 0; j < numB; ++j)
//			weiSum += mBTempRaw(i, j);
//
//		if (!isApprox(weiSum, 0.0, EPS_APPROX))
//		for (int j = 0; j < numB; ++j)
//			mBTemp(i, j) /= weiSum;
//	}
//
//	MatrixXd mBTempNor = mBTemp;
//
//	// Compare raw with normalized 
//
//	//logSimu("[TRACE] Harmonic binding normalization difference: %f", (mBTempRaw - mBTempNor).norm() / mBTempRaw.norm());
//
//	// Initialize temporal B matrix
//
//	tVector vBTemp;
//
//	for (int i = 0; i < numV; ++i)
//	{
//		for (int j = 0; j < numB; ++j)
//		{
//			int offsetV = i*numD;
//			int offsetC = j*numD;
//
//			for (int d = 0; d < numD; ++d) // Add diagonal blocks to temporal B matrix
//				vBTemp.push_back(Triplet<Real>(offsetV + d, offsetC + d, mBTemp(i, j)));
//		}
//	}
//
//	mHB.resize(numD*numV, numD*numB);
//	mHB.setFromTriplets(vBTemp.begin(), vBTemp.end());
//	mHB.makeCompressed(); // Ensure assymetric, sparse
//}

void TriMesh::computeBoundaryToMinimalMap_Laplacian(const dVector& vedgeWeights, SparseMatrixXd& mHB, int DIM)
{
	int numD = DIM;
	int numV = this->getNumNode();
	int numE = this->getNumEdge();

	iVector vboundList;
	iVector vslaveList;
	this->getSortedBoundaryIndices(vboundList, vslaveList);
	int numB = (int)vboundList.size();
	int numS = (int)vslaveList.size();
	iVector vfull2bound(numV, -1);
	iVector vfull2slave(numV, -1);
	int countBound = 0;
	int countSlave = 0;
	for (int i = 0; i < numB; ++i)
		vfull2bound[vboundList[i]] = countBound++;
	for (int i = 0; i < numS; ++i)
		vfull2slave[vslaveList[i]] = countSlave++;


	// Compute Laplacian matrix

	SparseMatrixXd mL;
	this->computeSymmetricLaplacian(vedgeWeights, mL);

	// Assemble splited Laplacian

	tVector vL;
	toTriplets(mL, vL);
	int numC = (int)vL.size();

	tVector vC;
	tVector vB;
	vC.reserve(numC);
	vB.reserve(numC);
	for (int i = 0; i < numC; ++i)
	{
		int idx0 = vL[i].row();
		int idx1 = vL[i].col();

		if (vfull2slave[idx0] != -1 && vfull2slave[idx1] != -1) // Slave - Slave
		{
			vB.push_back(Triplet<Real>(vfull2slave[idx0], vfull2slave[idx1], vL[i].value()));
		}

		if (vfull2slave[idx0] != -1 && vfull2bound[idx1] != -1) // Slave - Master
		{
			vC.push_back(Triplet<Real>(vfull2slave[idx0], vfull2bound[idx1], vL[i].value()));
		}
	}

	SparseMatrixXd mB(numS, numS);
	SparseMatrixXd mC(numS, numB);
	mC.setFromTriplets(vC.begin(), vC.end());
	mB.setFromTriplets(vB.begin(), vB.end());
	mC.makeCompressed();
	mB.makeCompressed();

	// Compute DqDp = -(B)^-1*C

	mC = -1 * mC;

	// Solve the linear system
	for (int i = 0; i < 10; ++i)
	{
		if (i != 0)
		{
			Real value = 0.001*pow(10.0, i - 1);

			logSimu("[ERROR] Regularizing system, iteration %d: %f", i, value);
			logSimu("[ERROR] In %s: Regularizing system, iteration %d: %f\n", __FUNCTION__, i, value);

			for (int ii = 0; ii < numS; ++ii)
				mB.coeffRef(ii, ii) += value;
		}

		// Assume B matrix is symmetric and D.P.
		SimplicialLDLT<SparseMatrixXd> solver;

		solver.compute(mB); // Factor
		if (solver.info() != Success)
			continue;

		// Compute the matrix A = -(B)^-1*C
		SparseMatrixXd mA = solver.solve(mC);
		if (solver.info() != Success)
			continue;

		// Test computed solution
		Real realNorm = mC.norm();
		SparseMatrixXd mCTest = mB*mA;
		Real diffNorm = (mC - mCTest).norm();
		if (diffNorm / realNorm > 1e-6)
			continue;

		// Assemble the temporal matrix A at the DOFs

		tVector vADOFs;

		tVector vANode;
		toTriplets(mA, vANode);
		int numC = (int)vANode.size();
		vADOFs.reserve((numB + numC)*numD);

		for (int j = 0; j < numB; ++j)
		for (int d = 0; d < numD; ++d)
			vADOFs.push_back(Triplet<Real>(numD*vboundList[j] + d, numD*j + d, 1.0));

		for (int j = 0; j < numC; ++j)
		{
			int offsetB = numD*vANode[j].col();

			int offsetV = numD*vslaveList[vANode[j].row()];

			for (int d = 0; d < numD; ++d) // Add diagonal blocks to the temporal A matrix
				vADOFs.push_back(Triplet<Real>(offsetV + d, offsetB + d, vANode[j].value()));
		}

		mHB.resize(numD*numV, numD*numB);
		mHB.setFromTriplets(vADOFs.begin(), vADOFs.end());
		mHB.makeCompressed(); // Ensure assymetric, sparse

		break; // Done
	}
}

void TriMesh::computeCartesianToDifferentialMap_Graph(SparseMatrixXd& mHB, const string& pname, int DIM)
{
	// Compute Laplacian matrix

	SparseMatrixXd mL;
	this->computeAbsoluteLaplacian_Graph(mL, pname);

	// Assemble full Laplacian map

	tVector vL;
	toTriplets(mL, vL);
	int numC = (int)vL.size();

	tVector vH;
	vH.reserve(DIM*numC);
	for (int i = 0; i < numC; ++i)
	{
		for (int d = 0; d < DIM; ++d)
		{
			vH.push_back(Triplet<Real>(DIM*vL[i].row() + d, DIM*vL[i].col() + d, vL[i].value()));
		}
	}

	int numV = this->getNumNode();
	mHB = SparseMatrixXd(DIM*numV, DIM*numV);
	mHB.setFromTriplets(vH.begin(), vH.end());
	mHB.makeCompressed();
}

void TriMesh::computeSymmetricLaplacian_Graph(SparseMatrixXd& mL, const string& pname)
{
	dVector vedgeWeights;
	this->computeLaplacianWeights_Graph(vedgeWeights, pname);
	this->computeSymmetricLaplacian(vedgeWeights, mL);
}

void TriMesh::computeSymmetricLaplacian_Cotangent(SparseMatrixXd& mL, const string& pname)
{
	dVector vedgeWeights;
	this->computeLaplacianWeights_Cotangent(vedgeWeights, pname);
	this->computeSymmetricLaplacian(vedgeWeights, mL);
}

void TriMesh::computeSymmetricLaplacian_MeanValue(SparseMatrixXd& mL, const string& pname)
{
	dVector vedgeWeights;
	this->computeLaplacianWeights_MeanValue(vedgeWeights, pname);
	this->computeSymmetricLaplacian(vedgeWeights, mL);
}

void TriMesh::computeSymmetricLaplacian_Regular(SparseMatrixXd& mL, const string& pname)
{
	dVector vedgeWeights;
	this->computeLaplacianWeights_Regular(vedgeWeights, pname);
	this->computeSymmetricLaplacian(vedgeWeights, mL);
}

void TriMesh::computeAbsoluteLaplacian_Graph(SparseMatrixXd& mL, const string& pname)
{
	int N = this->getNumNode();
	dVector vnodeAreas(N, 0);
	for (int i = 0; i < N; ++i)
	{
		VertexHandle vh = this->vertex_handle(i);

		vector<VertexHandle> vvh;
		this->getVertexVertices(vh, vvh);
		vnodeAreas[i] = (int) vvh.size();
	}

	dVector vedgeWeights;

	this->computeLaplacianWeights_Graph(vedgeWeights, pname);
	this->computeAbsoluteLaplacian(vedgeWeights, vnodeAreas, mL);
}

void TriMesh::computeAbsoluteLaplacian_Cotangent(SparseMatrixXd& mL, const string& pname)
{
	dVector vnodeAreas;
	dVector vedgeWeights;
	this->getVoronoiAreas(vnodeAreas, pname);
	this->computeLaplacianWeights_Cotangent(vedgeWeights, pname);
	this->computeAbsoluteLaplacian(vedgeWeights, vnodeAreas, mL);
}

void TriMesh::computeAbsoluteLaplacian_MeanValue(SparseMatrixXd& mL, const string& pname)
{
	dVector vnodeAreas;
	dVector vedgeWeights;
	this->getVoronoiAreas(vnodeAreas, pname);
	this->computeLaplacianWeights_MeanValue(vedgeWeights, pname);
	this->computeAbsoluteLaplacian(vedgeWeights, vnodeAreas, mL);
}

void TriMesh::computeAbsoluteLaplacian_Regular(SparseMatrixXd& mL, const string& pname)
{
	dVector vnodeAreas;
	dVector vedgeWeights;
	this->getVoronoiAreas(vnodeAreas, pname);
	this->computeLaplacianWeights_Regular(vedgeWeights, pname);
	this->computeAbsoluteLaplacian(vedgeWeights, vnodeAreas, mL);
}

void TriMesh::computeAbsoluteLaplacian(const dVector& vedgeWeights, const dVector& vnodeAreas, SparseMatrixXd& mL)
{
	tVector vL;

	int numV = this->getNumNode();
	int numE = this->getNumEdge();

	vL.reserve(4 * numE);

	for (EdgeIter e_it = this->edges_begin(); e_it != this->edges_end(); ++e_it)
	{
		vector<TriMesh::VertexHandle> vvh;
		this->getEdgeVertices(*e_it, vvh);

		double w = vedgeWeights[(*e_it).idx()];
		int idx0 = vvh[0].idx();
		int idx1 = vvh[1].idx();

		vL.push_back(Triplet<Real>(idx0, idx0, w));
		vL.push_back(Triplet<Real>(idx1, idx1, w));
		vL.push_back(Triplet<Real>(idx0, idx1, -w));
		vL.push_back(Triplet<Real>(idx1, idx0, -w));
	}

	// Laplacian matrix L

	mL = SparseMatrixXd(numV, numV);
	mL.setFromTriplets(vL.begin(),
					   vL.end());
	mL.makeCompressed();

	// Inverse D matrix

	tVector vD;
	vD.reserve(numV);
	for (int i = 0; i < numV; ++i)
		vD.push_back(Triplet<Real>(i, i, 1.0/vnodeAreas[i]));
	
	SparseMatrixXd mD(numV, numV);
	mD.setFromTriplets(vD.begin(),
					   vD.end());
	mD.makeCompressed();

	mL = mD*mL;
}

void TriMesh::computeSymmetricLaplacian(const dVector& vedgeWeights, SparseMatrixXd& mL)
{
	tVector vL;

	int numV = this->getNumNode();
	int numE = this->getNumEdge();

	vL.reserve(4*numE);

	for (EdgeIter e_it = this->edges_begin(); e_it != this->edges_end(); ++e_it)
	{
		vector<TriMesh::VertexHandle> vvh;
		this->getEdgeVertices(*e_it, vvh);

		double w = vedgeWeights[(*e_it).idx()];
		int idx0 = vvh[0].idx();
		int idx1 = vvh[1].idx();

		vL.push_back(Triplet<Real>(idx0, idx0, w));
		vL.push_back(Triplet<Real>(idx1, idx1, w));
		vL.push_back(Triplet<Real>(idx0, idx1, -w));
		vL.push_back(Triplet<Real>(idx1, idx0, -w));
	}

	// Laplacian matrix L

	mL = SparseMatrixXd(numV, numV);
	mL.setFromTriplets(vL.begin(), 
					   vL.end());
	mL.makeCompressed();
}

void TriMesh::computeLaplacianWeights_Graph(dVector& vedgeWeights, const string& pname)
{
	vedgeWeights.resize(this->getNumEdge(), 1);
}

void TriMesh::computeLaplacianWeights_Cotangent(dVector& vedgeWeights, const string& pname)
{
	vedgeWeights.resize(this->getNumEdge());

	Real tol = 1e-6;

	TriMesh::HalfedgeHandle h0, h1, h2;
	TriMesh::VertexHandle v0, v1, v2;
	Vector3d p0, p1, p2, d0, d1;

	for (TriMesh::ConstEdgeIter e_it = this->edges_begin(); e_it != this->edges_end(); ++e_it)
	{
		double w = 0.0;

		// Edge

		h0 = this->halfedge_handle(*e_it, 0);
		v0 = this->to_vertex_handle(h0);
		p0 = this->getPoint(v0, pname);

		h1 = this->halfedge_handle(*e_it, 1);
		v1 = this->to_vertex_handle(h1);
		p1 = this->getPoint(v1, pname);

		// Face #0

		h2 = this->next_halfedge_handle(h0);
		v2 = this->to_vertex_handle(h2);
		p2 = this->getPoint(v2, pname);

		d0 = (p0 - p2).normalized();
		d1 = (p1 - p2).normalized();
		w += 1.0 / tan(acos(std::min(1 - tol, std::max(-1 + tol, (d0.dot(d1))))));

		// Face #1

		if (!this->is_boundary(*e_it))
		{
			h2 = this->next_halfedge_handle(h1);
			v2 = this->to_vertex_handle(h2);
			p2 = this->getPoint(v2, pname);

			d0 = (p0 - p2).normalized();
			d1 = (p1 - p2).normalized();
			w += 1.0 / tan(acos(std::min(1 - tol, std::max(-1 + tol, (d0.dot(d1))))));
		}

		//if (w < 0.0)
		//	w = 0.0;

		if (!this->is_boundary(*e_it))
			w /= 2.0; // Average weight

		vedgeWeights[(*e_it).idx()] = w;
	}
}

void TriMesh::computeLaplacianWeights_MeanValue(dVector& vedgeWeights, const string& pname)
{
	vedgeWeights.resize(this->getNumEdge());

	Real tol = 1e-6;

	TriMesh::HalfedgeHandle h0, h1, h2;
	TriMesh::VertexHandle v0, v1, v2;
	Vector3d p0, p1, p2, d0, d1;

	for (TriMesh::ConstEdgeIter e_it = this->edges_begin(); e_it != this->edges_end(); ++e_it)
	{
		double w = 0.0;

		// Edge

		h0 = this->halfedge_handle(*e_it, 0);
		v0 = this->to_vertex_handle(h0);
		p0 = this->getPoint(v0, pname);

		h1 = this->halfedge_handle(*e_it, 1);
		v1 = this->to_vertex_handle(h1);
		p1 = this->getPoint(v1, pname);

		// Face #0

		h2 = this->next_halfedge_handle(h0);
		v2 = this->to_vertex_handle(h2);
		p2 = this->getPoint(v2, pname);

		d0 = (p0 - p2).normalized();
		d1 = (p1 - p2).normalized();
		w += 1.0 / tan(acos(std::min(1 - tol, std::max(-1 + tol, (d0.dot(d1))))));

		// Face #1

		if (!this->is_boundary(*e_it))
		{
			h2 = this->next_halfedge_handle(h1);
			v2 = this->to_vertex_handle(h2);
			p2 = this->getPoint(v2, pname);

			d0 = (p0 - p2).normalized();
			d1 = (p1 - p2).normalized();
			w += 1.0 / tan(acos(std::min(1 - tol, std::max(-1 + tol, (d0.dot(d1))))));
		}

		//if (w < 0.0)
		//	w = 0.0;

		if (!this->is_boundary(*e_it))
			w /= 2.0; // Average weight

		vedgeWeights[(*e_it).idx()] = w;
	}
}

void TriMesh::computeLaplacianWeights_Regular(dVector& vedgeWeights, const string& pname)
{
	vedgeWeights.resize(this->getNumEdge());

	dVector vxprop;
	dVector vxbase;
	this->getPoints(vxprop, pname);
	this->getPoints(vxbase);
	this->setPoints(vxprop);

	TriMesh::HalfedgeHandle h0, h1;
	TriMesh::VertexHandle v0, v1;

	for (TriMesh::ConstEdgeIter e_it = this->edges_begin(); e_it != this->edges_end(); ++e_it)
	{
		double w = 0.0;

		h0 = this->halfedge_handle(*e_it, 0);
		v0 = this->to_vertex_handle(h0);

		h1 = this->halfedge_handle(*e_it, 1);
		v1 = this->to_vertex_handle(h1);

		vector<FaceHandle> vfh0;
		vector<FaceHandle> vfh1;
		this->getVertexFaces(v0, vfh0);
		this->getVertexFaces(v1, vfh1);
		int nf0 = (int) vfh0.size();
		int nf1 = (int) vfh1.size();

		Real angle0 = this->getFanAngle(v0, pname);
		Real angle1 = this->getFanAngle(v1, pname);

		Real avgAngle0 = angle0 / nf0;
		Real avgAngle1 = angle1 / nf1;
		Real tarAngle0 = (M_PI - avgAngle0) / 2;
		Real tarAngle1 = (M_PI - avgAngle1) / 2;

		w += 1.0 / tan(tarAngle0);
		w += 1.0 / tan(tarAngle1);
		w /= 2.0;

		vedgeWeights[(*e_it).idx()] = w;
	}

	this->setPoints(vxbase);
}

void TriMesh::updateBVH_Face(bool clear, const string& pname)
{
	map<string, bool>::iterator find_it = this->m_mBVHUpdatedFace.find(pname);

	if (find_it == this->m_mBVHUpdatedFace.end())
	{
		BVHTree* pTree = new BVHTree(0);
		this->m_mBVHTreeFace[pname] = pTree;
		pTree->initializeMeshFace(this, pname);
	}
	else
	{
		dVector vx;
		this->getPoints(vx, pname);
		this->m_mBVHTreeFace[pname]->update(vx, clear);
	}

	this->m_mBVHUpdatedFace[pname] = true;
}

void TriMesh::updateBVH_Edge(bool clear, const string& pname)
{
	map<string, bool>::iterator find_it = this->m_mBVHUpdatedEdge.find(pname);

	if (find_it == this->m_mBVHUpdatedEdge.end())
	{
		BVHTree* pTree = new BVHTree(0);
		this->m_mBVHTreeEdge[pname] = pTree;
		pTree->initializeMeshEdge(this, pname);
	}
	else
	{
		dVector vx;
		this->getPoints(vx, pname);
		this->m_mBVHTreeEdge[pname]->update(vx, clear);
	}

	this->m_mBVHUpdatedEdge[pname] = true;
}

const BVHTree* TriMesh::getBVHTree_Face(const string& pname)
{
	if (!this->isBVHUpdated_Face(pname))
		this->updateBVH_Face(true, pname);

	return this->m_mBVHTreeFace[pname];
}

const BVHTree* TriMesh::getBVHTree_Edge(const string& pname)
{
	if (!this->isBVHUpdated_Edge(pname))
		this->updateBVH_Edge(true, pname);

	return this->m_mBVHTreeEdge[pname];
}

bool TriMesh::isBVHUpdated_Face(const string& pname)
{
	map<string, bool>::iterator find_it = this->m_mBVHUpdatedFace.find(pname);
	if (find_it == this->m_mBVHUpdatedFace.end())
		return false;

	return find_it->second;
}

bool TriMesh::isBVHUpdated_Edge(const string& pname)
{
	map<string, bool>::iterator find_it = this->m_mBVHUpdatedEdge.find(pname);
	if (find_it == this->m_mBVHUpdatedEdge.end())
		return false;

	return find_it->second;
}

void TriMesh::scale(Real sX, Real sY, Real sZ, const string& pname)
{
	dVector vx;

	this->getPoints(vx, pname);
	int nv = this->getNumNode();

	for (int i = 0; i < nv; ++i)
	{
		int offset = 3*i;
		vx[offset + 0] *= sX;
		vx[offset + 1] *= sY;
		vx[offset + 2] *= sZ;
	}

	this->setPoints(vx, pname);
}

void TriMesh::rotate(Real rX, Real rY, Real rZ, const string& pname)
{
	dVector vx;

	this->getPoints(vx, pname);
	int nv = this->getNumNode();

	// TODO

	Matrix3d R;
	R.Identity();

	for (int i = 0; i < nv; ++i)
		set3D(i, R*get3D(i, vx), vx);

	this->setPoints(vx, pname);
}

void TriMesh::translate(Real tX, Real tY, Real tZ, const string& pname)
{
	dVector vx;

	this->getPoints(vx, pname);
	int nv = this->getNumNode();

	for (int i = 0; i < nv; ++i)
	{
		int offset = 3 * i;
		vx[offset + 0] += tX;
		vx[offset + 1] += tY;
		vx[offset + 2] += tZ;
	}

	this->setPoints(vx, pname);
}