/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_RodMeshRest.h
\author		jesusprod
\brief		Parameter set for the rest state of rods in a tensile structure.
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_TENSILE_RODMESH_REST_H
#define PARAMETER_SET_TENSILE_RODMESH_REST_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/Model/TensileStructureModel.h>

class JSANDBOX_EXPORT ParameterSet_Tensile_RodMeshRest : public ParameterSet
{
public:
	ParameterSet_Tensile_RodMeshRest(int midx);
	virtual ~ParameterSet_Tensile_RodMeshRest();



	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual bool getIs2D() const { return this->m_is2D; }
	virtual void setIs2D(bool is2D) { this->m_is2D = is2D; }
	
	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

protected:

	TensileStructureModel* m_pTSModel;

	int m_midx;

	bool m_is2D;

};

#endif