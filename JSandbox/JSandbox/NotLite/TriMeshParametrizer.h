/*=====================================================================================*/
/*!
\file		TriMeshParametrizer.h
\author		skourasm,jesusprod
\brief		Patch parametrizer.
*/
/*=====================================================================================*/

#ifndef TRIMESH_PARAMETRIZER_H
#define TRIMESH_PARAMETRIZER_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/TriMesh.h>

class JSANDBOX_EXPORT TriMeshParametrizer
{

public:
	TriMeshParametrizer(void);
	virtual ~TriMeshParametrizer(void);

	virtual Real getSolverError() const { return this->m_curveSolverError; }
	virtual void setSolverError(Real tol) { this->m_curveSolverError = tol; }

	virtual Real getCurveLengthK() const { return this->m_curveLengthK; }
	virtual void setCurveLengthK(Real lK) { this->m_curveLengthK = lK; }

	virtual Real getCurveAngleK() const { return this->m_curveAngleK; }
	virtual void setCurveAngleK(Real aK) { this->m_curveAngleK = aK; }

	virtual void setCorrectArea(bool ca) { this->m_correctArea = ca; }
	virtual bool getCorrectArea() const { return this->m_correctArea; }

	virtual bool parametrize(TriMesh* pMesh, dVector& vpatuv) const;

protected:

	virtual bool parametrizePatch(TriMesh* pMesh, dVector& vpatuv) const;
	virtual void initializeCurve(const dVector& vposIn, const dVector& vang, const dVector& vlen, dVector& vposOut) const;
	virtual bool parametrizeCurve(const dVector& vposIn, const dVector& vang, const dVector& vlen, dVector& vposOut) const;
	virtual void computeHarmonicWeights(const TriMesh* pMesh, dVector& vedgeWeights) const;
	
	Real m_curveSolverError;

	Real m_curveLengthK;
	Real m_curveAngleK;

	bool m_correctArea;

};

#endif