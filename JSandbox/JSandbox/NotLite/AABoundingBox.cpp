/*=====================================================================================*/
/*!
\file		AABoundingBox.cpp
\author		jesusprod
\brief		Implementation of AABoundingBox.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/AABoundingBox.h>

AABoundingBox::AABoundingBox(void)
{
	this->clear();
}

AABoundingBox::~AABoundingBox(void)
{
	// Nothing to do here
}

void AABoundingBox::clear()
{
	m_empty = true;
	m_minIntervals = Vector3d(HUGE_VAL, HUGE_VAL, HUGE_VAL);
	m_maxIntervals = Vector3d(-HUGE_VAL, -HUGE_VAL, -HUGE_VAL);
}

void AABoundingBox::merge(const BoundingVolume* bvIn)
{
	const AABoundingBox* aabbIn = dynamic_cast<const AABoundingBox*>(bvIn);
	assert(aabbIn);
	this->insert(aabbIn->getMaxIntervals());
	this->insert(aabbIn->getMinIntervals());
}

bool AABoundingBox::overlaps(const BoundingVolume* bvIn, double tol) const
{
	const AABoundingBox* aabbIn = dynamic_cast<const AABoundingBox*>(bvIn);
	assert(aabbIn);
	const Vector3d& max0 = this->m_maxIntervals;
	const Vector3d& min0 = this->m_minIntervals;
	const Vector3d& max1 = aabbIn->getMaxIntervals();
	const Vector3d& min1 = aabbIn->getMinIntervals();
	for(int i = 0; i < 3; ++i)
	{
		bool ovl = (max0[i] - min1[i] > -tol) && 
				   (max1[i] - min0[i] > -tol);
		if (!ovl)
			return false;
	}
	return true;
}

void AABoundingBox::insert(const Vector3d& p)
{
	this->m_empty = false;

	for(int i = 0; i < 3; ++i)
	{
		m_maxIntervals[i] = (p[i] > m_maxIntervals[i])? p[i]:m_maxIntervals[i];
		m_minIntervals[i] = (p[i] < m_minIntervals[i])? p[i]:m_minIntervals[i];
	}
}

int AABoundingBox::getLongestDimension(double* pLength) const
{
	int imax = -1;
	double lmax = -1;
	for(int j = 0; j < 3; ++j)
	{
		double li = m_maxIntervals[j] - m_minIntervals[j];
		if (li > lmax)
		{
			imax = j;
			lmax = li;			
		}
	}
	assert(imax > -1);
	if (pLength)
		*pLength = lmax;
	return imax;
}

