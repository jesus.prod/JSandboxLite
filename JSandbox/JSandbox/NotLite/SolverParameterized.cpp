// /*=====================================================================================*/
///*!
//\file		SolverParameterized.cpp
//\author		jesusprod
//\brief		TODO
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Solver/SolverParameterized.h>
//
//#include <JSandbox/Optim/AlgLib_Sparse_BoxC_QP.h>
//#include <JSandbox/Optim/AlgLib_Sparse_EQIN_QP.h>
//#include <JSandbox/Optim/Mosek_Sparse_EQIN_QP.h>
//
//SolverParameterized::SolverParameterized(const string& ID)
//{
//	this->m_ID = ID;
//	this->m_pModel = NULL;
//	this->m_pSolver = NULL;
//	this->m_pParam = NULL;
//
//	// Set default parameters
//	this->m_solverMaxError = 1e-6;
//	this->m_linearMaxError = 1e-6;
//	this->m_useRegularize = true;
//	this->m_useLineSearch = true;
//	this->m_lsMaxIters = 10;
//	this->m_regMaxIters = 10;
//	this->m_solverMaxIters = 50;
//	this->m_linearMaxIters = 1000;
//	this->m_rFactor = 1e-4;
//	this->m_lsBeta = 0.5;
//
//	this->m_hasMaxStep = false;
//	this->m_maxStep = HUGE_VAL;
//}
//
//SolverParameterized::~SolverParameterized()
//{
//	// Nothing to do here...
//}
//
//void SolverParameterized::setup()
//{
//	assert(this->m_pParam != NULL);
//
//	this->m_pModel = this->m_pParam->getModel();
//	this->m_pSolver = this->m_pParam->getSolver();
//}
//
//ModelParameterization* SolverParameterized::getParameterization() const
//{
//	return this->m_pParam;
//}
//
//void SolverParameterized::setParameterization(ModelParameterization* pP)
//{
//	assert(pP != NULL);
//	this->m_pParam = pP;
//}
//
//bool SolverParameterized::solveLinearSystem_Mosek(SparseMatrixXd& mA, VectorXd& vb, VectorXd& vt, double wx, double wp, bool bounded, double regDef)
//{
//	int Nx = mA.rows();
//	int Nt = mA.cols();
//	int Np = Nt - Nx;
//
//	assert((int)vb.size() == Nx);
//
//	// Build G matrix and c vector
//
//	VectorXd vc(Nt);
//	vc.setZero();
//
//	tVector vA;
//	toTriplets(mA, vA);
//
//	tVector vG;
//	vG.reserve(Nt);
//
//	// Add I*wx to x and Reg.
//	for (int i = 0; i < Nx; ++i)
//	{
//		vG.push_back(Triplet<Real>(i, i, wx + regDef));
//	}
//
//	// Add I*wp to p and Reg.
//	for (int i = Nx; i < Nt; ++i)
//	{
//		vG.push_back(Triplet<Real>(i, i, wp + regDef));
//	}
//
//	// Get the vectors of parameter bounds
//
//	VectorXd vdpUB = this->m_pParam->getParametersUpperBound() - this->m_vp;
//	VectorXd vdpLB = this->m_pParam->getParametersLowerBound() - this->m_vp;
//
//	VectorXd vxUB(Nt);
//	VectorXd vxLB(Nt);
//	for (int i = 0; i < Nt; ++i)
//	{
//		// It is bounded parameter!
//		if (bounded && i >= Nx && i < Nt)
//		{
//			vxUB(i) = vdpUB(i - Nx);
//			vxLB(i) = vdpLB(i - Nx);
//		}
//		else
//		{
//			vxUB(i) = HUGE_VAL;
//			vxLB(i) = -HUGE_VAL;
//		}
//	}
//
//	// Build constraint type vector
//
//	iVector vCT(Nx);
//	for (int i = 0; i < Nx; ++i)
//		vCT[i] = 0; // Equality
//
//	// Solve QP
//
//	Mosek_Sparse_EQIN_QP QPsolver;
//
//	VectorXd vx0(Nt);
//	VectorXd vxo(Nt);
//	vx0.setZero();
//	vxo.setZero();
//
//	string error;
//
//	bool solved = QPsolver.solve(vG, vc, vA, vb, vCT, vx0, vxLB, vxUB, vxo, error);
//
//	if (solved)
//	{
//		// Check constraints
//
//		Real realNorm = vb.norm();
//		VectorXd vbTest = mA*vxo-vb;
//		Real testNorm = vbTest.norm();
//		Real relError = testNorm/realNorm;
//
//		if (relError > this->m_linearMaxError)
//		{
//			logSimu("[ERROR] Constrained QP not solved properly: %.9f", relError);
//		}
//		else
//		{
//			logSimu("[GOOD] Constrained QP was solved properly: %.9f", relError);
//		}
//
//		vt = vxo;
//
//		assert((int)vt.size() == Nt);
//	}
//
//	logSimu("[TRACE] Bounded QP result:\n\t%s", error.c_str());
//
//	return true;
//}
//
//
//bool SolverParameterized::solveLinearSystem_AlgLibBLEIC(SparseMatrixXd& mA, VectorXd& vb, VectorXd& vt, double wx, double wp, bool bounded, double regDef)
//{
//	int Nx = mA.rows();
//	int Nt = mA.cols();
//	int Np = Nt - Nx;
//
//	assert((int)vb.size() == Nx);
//
//
//	// Build G matrix and c vector
//
//	VectorXd vc(Nt);
//	vc.setZero();
//
//	tVector vG;
//	vG.reserve(Nt);
//
//	// Add I*wx to x and Reg.
//	for (int i = 0; i < Nx; ++i)
//	{
//		vG.push_back(Triplet<Real>(i, i, wx + regDef));
//	}
//
//	// Add I*wp to p and Reg.
//	for (int i = Nx; i < Nt; ++i)
//	{
//		vG.push_back(Triplet<Real>(i, i, wp + regDef));
//	}
//
//	// Get the vectors of parameter bounds
//
//	VectorXd vdpUB = this->m_pParam->getParametersUpperBound() - this->m_vp;
//	VectorXd vdpLB = this->m_pParam->getParametersLowerBound() - this->m_vp;
//
//	VectorXd vxUB(Nt);
//	VectorXd vxLB(Nt);
//	for (int i = 0; i < Nt; ++i)
//	{
//		// It is bounded parameter!
//		if (bounded && i >= Nx && i < Nt)
//		{
//			vxUB(i) = vdpUB(i - Nx);
//			vxLB(i) = vdpLB(i - Nx);
//		}
//		else
//		{
//			vxUB(i) = HUGE_VAL;
//			vxLB(i) = -HUGE_VAL;
//		}
//	}
//
//	// Build constraint type vector
//
//	iVector vCT(Nx);
//	for (int i = 0; i < Nx; ++i)
//		vCT[i] = 0; // Equality
//
//	// Solve QP
//
//	AlgLib_Sparse_EQIN_QP QPsolver;
//
//	VectorXd vx0(Nt);
//	VectorXd vxo(Nt);
//	vx0.setZero();
//	vxo.setZero();
//
//	string error;
//
//	bool solved = QPsolver.solve(vG, vc, mA.toDense(), vb, vCT, vx0, vxLB, vxUB, vxo, error);
//
//	if (solved)
//	{
//		// Check constraints
//
//		Real realNorm = vb.norm();
//		VectorXd vbTest = mA*vxo - vb;
//		Real testNorm = vbTest.norm();
//
//		Real relError = testNorm / realNorm;
//
//		if (relError > 1e-6)
//		{
//			logSimu("[ERROR] Constrained QP not solved properly: %f", relError);
//		}
//		else
//		{
//			logSimu("[GOOD] Constrained QP was solved properly: %f", relError);
//		}
//
//		vt = vxo;
//
//		assert((int)vt.size() == Nt);
//	}
//
//	logSimu("[TRACE] Bounded QP result:\n\t%s", error.c_str());
//
//	return true;
//}
//
//bool SolverParameterized::solveLinearSystem_AlgLibQuick(SparseMatrixXd& mA, VectorXd& vb, VectorXd& vt, double wx, double wp, bool bounded, double regDef)
//{
//	int Nx = mA.rows();
//	int Nt = mA.cols();
//	int Nc = Nt + Nx;
//
//	assert((int)vb.size() == Nx);
//
//	// Build G matrix
//
//	tVector vA; // Get!
//	toTriplets(mA, vA);
//
//	tVector vG; // Jacobians + D
//	vG.reserve(2 * vA.size() + Nc);
//
//	// Add A and A^T
//	for (int i = 0; i < (int)vA.size(); ++i)
//	{
//		vG.push_back(Triplet<Real>(Nt + vA[i].row(), vA[i].col(), vA[i].value()));
//		vG.push_back(Triplet<Real>(vA[i].col(), Nt + vA[i].row(), vA[i].value()));
//	}
//
//	// Add I*wx to x and Reg.
//	for (int i = 0; i < Nx; ++i)
//	{
//		vG.push_back(Triplet<Real>(i, i, wx + regDef));
//	}
//
//	// Add I*wp to p and Reg.
//	for (int i = Nx; i < Nt; ++i)
//	{
//		vG.push_back(Triplet<Real>(i, i, wp + regDef));
//	}
//
//	// Build c vector
//
//	VectorXd vc(Nc); 
//	vc.setZero();
//
//	for (int i = Nt; i < Nc; ++i)
//		vc(i) = vb(i - Nt); 
//
//	// Get the vectors of parameter bounds
//
//	VectorXd vdpUB = this->m_pParam->getParametersUpperBound() - this->m_vp;
//	VectorXd vdpLB = this->m_pParam->getParametersLowerBound() - this->m_vp;
//
//	VectorXd vxUB(Nc);
//	VectorXd vxLB(Nc);
//	for (int i = 0; i < Nc; ++i)
//	{
//		// It is bounded parameter!
//		if (bounded && i >= Nx && i < Nt)
//		{
//			vxUB(i) = vdpUB(i - Nx);
//			vxLB(i) = vdpLB(i - Nx);
//		}
//		else
//		{
//			vxUB(i) = HUGE_VAL;
//			vxLB(i) = -HUGE_VAL;
//		}
//	}
//
//	// Solve QP
//
//	AlgLib_Sparse_BoxC_QP QPsolver;
//
//	VectorXd vx0(Nc);
//	VectorXd vxo(Nc);
//	vx0.setZero();
//	vxo.setZero();
//
//	string error;
//
//	bool solved = QPsolver.solve(vG, vc, vx0, vxLB, vxUB, vxo, error);
//
//	if (solved)
//	{
//		// Split vectors
//		VectorXd vto(Nt); // Primal
//		VectorXd vlo(Nx); // Dual
//		getSubvector(0, Nt, vxo, vto);
//		getSubvector(Nt, Nc, vxo, vlo);
//	
//		// Check constraints
//
//		Real realNorm = vb.norm();
//		VectorXd vbTest = mA*vto - vb;
//		Real testNorm = vbTest.norm();
//
//		Real relError = testNorm / realNorm;
//		if (relError > 1e-6)
//		{
//			logSimu("[ERROR] Constrained QP not solved properly: %f", relError);
//		}
//		else
//		{
//			logSimu("[GOOD] Constrained QP was solved properly: %f", relError);
//		}
//
//		vt = vto;
//
//		assert((int)vt.size() == Nt);
//	}
//
//	logSimu("[TRACE] Bounded QP result:\n\t%s", error.c_str());
//
//	return true;
//}
//
//bool SolverParameterized::solveLinearSystem_FactorBound(SparseMatrixXd& mA, VectorXd& vb, VectorXd& vt, double wx, double wp, bool bounded, double regDef)
//{
//	int Nx = mA.rows();
//	int Nt = mA.cols();
//	int Np = Nt - Nx;
//
//	assert((int)vb.size() == Nx);
//
//	// Build the W^-1 matrix such that the solver
//	// result is min_t { t^T * W * t } s.t. J*t = b
//
//	tVector vWi(Nt);
//	for (int i = 0; i < Nx; ++i)
//		vWi.push_back(Triplet<Real>(i, i, 1.0 / (wx + regDef)));
//	for (int i = Nx; i < Nt; ++i)
//		vWi.push_back(Triplet<Real>(i, i, 1.0 / (wp + regDef)));
//	SparseMatrixXd mWi(Nt, Nt);
//	mWi.setFromTriplets(vWi.begin(), vWi.end());
//	mWi.makeCompressed(); // Diagonal sparse Wi
//
//	SparseMatrixXd mI;
//	mI.setIdentity();
//
//	// Compute parameter bounds if the are any box-constrained parameters
//
//	iVector vpBoundIdx;
//
//	dVector vdpUB = toSTL(VectorXd(this->m_pParam->getParametersUpperBound() - this->m_vp));
//	dVector vdpLB = toSTL(VectorXd(this->m_pParam->getParametersLowerBound() - this->m_vp));
//
//	if (bounded)
//	{
//		// Potentially all them
//		vpBoundIdx.reserve(Np);
//
//		for (int i = 0; i < Np; ++i)
//		{
//			if (vdpUB[i] != HUGE_VAL || vdpLB[i] != -HUGE_VAL)
//				vpBoundIdx.push_back(i); // Potentially active
//		}
//	}
//
//	int Nb = (int) vpBoundIdx.size();
//	bVector vParActiveSet(Nb, false);
//	bool activeSetChanged = true;
//
//	// Initially empty
//	int NbActive = 0;
//	
//	// Build full constraints J*t - c = 0, with J^T = [A^T B^T] and c^T = [b^T d^T]. 
//	// Initially , J = A and c = b as the active set is empty. We succesively add
//	// new rows to the constraints system.
//	
//	VectorXd vc = vb;
//
//	tVector vJ;
//	toTriplets(mA, vJ);
//	vJ.reserve((int) mA.size() + Nb);
//
//	// Iterate until the constraints in the active set remains the same. Currently
//	// there is no way to get out the active set. We expect the active set to be
//	// complete within a reduced amount of iterations.
//
//	while (activeSetChanged)
//	{
//		logSimu("[ERROR] Solving smallest norm linear system. Active set size: %d", NbActive);
//		logSimu("[ERROR] In %s: Solving smallest norm linear system. Active set size: %d\n", __FUNCTION__, NbActive);
//
//		activeSetChanged = false;
//
//		int Nc = Nx + NbActive;
//
//		// Build J from triplets
//		SparseMatrixXd mJ(Nc, Nt);
//		mJ.setFromTriplets(vJ.begin(), vJ.end());
//		mJ.makeCompressed(); // Sparse assymetric
//
//		// Compute J*Wi*J^T
//		SparseMatrixXd mJWiJt;
//
//		int it = 0;
//
//		// Solve the linear system s.t. regularization
//		for (it = 0; it < this->m_regMaxIters; ++it)
//		{
//			// Assume DFDx is symmetric and D.P.
//			SimplicialLDLT<SparseMatrixXd> solver;
//
//			if (it != 0)
//			{
//				// 0.001, 0.01, 0.1, 1, 10....
//				Real val = 1e-3*pow(10, it - 1);
//
//				logSimu("[TRACE] Regularizing linear system: %f", val);
//				logSimu("[ERROR] In %s: Regularizing linear system: %f\n", __FUNCTION__, val);
//
//				for (int ii = 0; ii < Nx; ++ii)
//				{
//					mJ.coeffRef(ii, ii) += val;
//					mA.coeffRef(ii, ii) += val;
//				}
//			}
//
//			mJWiJt = mJ*mWi*mJ.transpose();
//			
//			// Pre-factorization
//			solver.compute(mJWiJt);
//
//			if (solver.info() != Success)
//				continue; // Invalid factor
//
//			// Compute (J*W^-1*J^T)^-1*vc
//			VectorXd ve = solver.solve(vc);
//
//			if (solver.info() != Success)
//				continue; // Invalid solve
//
//			// Test calculated solution
//			VectorXd vcTest = mJWiJt*ve;
//			Real realNorm = vc.norm();
//			Real diffNorm = (vc - vcTest).norm();
//			if (diffNorm > 1e-9 && diffNorm / realNorm > this->m_linearMaxError)
//				continue; // Check also absolute error to avoid numerical issues
//
//			// Compute vt = Wi*J^T*vc
//			vt = mWi*(mJ.transpose()*ve);
//			assert((int)vt.size() == Nt);
//
//			break; // Done
//		}
//
//		// Not properly solved, trace!
//		if (it == this->m_regMaxIters)
//		{
//			// Invalid linear solve: trace!
//			writeToFile(mWi, "WError.csv");
//			writeToFile(mJ, "JError.csv");
//			writeToFile(vc, "bError.csv");
//			writeToFile(mJWiJt, "AError.csv");
//
//			throw new exception("Wrong :(!");
//		}
//
//		// Check constraints
//
//		Real realNorm = vc.norm();
//		VectorXd vcTest = mJ*vt - vc;
//		Real absError = vcTest.norm();
//		Real relError = absError / realNorm;
//
//		if (absError > 1e-9 && relError > this->m_linearMaxError)
//		{
//			logSimu("[ERROR] Constrained QP not solved properly: %.9f", relError);
//		}
//		else
//		{
//			logSimu("[GOOD] Constrained QP was solved properly: %.9f", relError);
//		}
//
//		// Check if the temptative solution violates any of the parameter box constraints. 
//		// If that happens, introduce them in the active set and iterate again the solver.
//
//		dVector vcNew = toSTL(vc);
//
//		// This might grow up
//		vcNew.reserve(Nc + Nb);
//
//		for (int i = 0; i < Nb; ++i)
//		{
//			int parIdx = vpBoundIdx[i];
//			Real val = vt(Nx + parIdx);
//
//			if (val - vdpUB[parIdx] > this->m_linearMaxError)
//			{
//				if (vParActiveSet[i])
//				{
//					// Is already in the active set
//					vt(Nx + parIdx) = vdpUB[parIdx];
//
//					continue;
//				}
//
//				vParActiveSet[i] = true;
//				activeSetChanged = true;
//				vcNew.push_back(vdpUB[parIdx]);
//				vJ.push_back(Triplet<Real>(Nx + NbActive, Nx + parIdx, 1.0));
//
//				NbActive++;
//
//				continue;
//			}
//
//			if (vdpLB[parIdx] - val > this->m_linearMaxError)
//			{
//				if (vParActiveSet[i])
//				{
//					// Is already in the active set
//					vt(Nx + parIdx) = vdpLB[parIdx];
//
//					continue;
//				}
//
//				vParActiveSet[i] = true;
//				activeSetChanged = true;
//				vcNew.push_back(vdpLB[parIdx]);
//				vJ.push_back(Triplet<Real>(Nx + NbActive, Nx + parIdx, 1.0));
//
//				NbActive++;
//
//				continue;
//			}
//		}
//
//		if (activeSetChanged)
//			vc = toEigen(vcNew);
//	}
//
//
//	// Check constraints
//
//	Real realNorm = vb.norm();
//	VectorXd vbTest = mA*vt- vb;
//	Real absError = vbTest.norm();
//	Real relError = absError / realNorm;
//
//	if (absError > 1e-9 && relError > this->m_linearMaxError)
//	{
//		logSimu("[ERROR] Constrained QP not solved properly: %.9f", relError);
//	}
//	else
//	{
//		logSimu("[GOOD] Constrained QP was solved properly: %.9f", relError);
//	}
//
//	// Assert bounds
//
//	dVector vtSTL = toSTL(vt);
//
//	for (int i = 0; i < Np; ++i)
//	{
//		if (vt(Nx + i) - vdpUB[i] > this->m_linearMaxError)
//		{
//			logSimu("[ERROR] Box-constraints do not hold: %.9f", vt(Nx + i) - vdpUB[i]);
//
//			throw new exception("This should not happen!");
//		}
//
//		if (vdpLB[i] - vt(Nx + i) > this->m_linearMaxError)
//		{
//			logSimu("[ERROR] Box-constraints do not hold: %.9f", vdpLB[i] - vt(Nx + i));
//
//			throw new exception("This should not happen!");
//		}
//	}
//
//	return true;
//}
//
//bool SolverParameterized::solveLinearSystem_CustomFullQP(SparseMatrixXd& mA, VectorXd& vb, VectorXd& vt, double wx, double wp, bool bounded, double regDef)
//{
//	int Nx = mA.rows();
//	int Nt = mA.cols();
//
//	assert((int)vb.size() == Nx);
//
//	tVector vA; // Get!
//	toTriplets(mA, vA);
//
//	// Build system matrix
//
//	tVector vW;
//	vW.reserve(2 * vA.size() + Nt + Nx);
//	SparseMatrixXd mW(Nt + Nx, Nt + Nx);
//
//	// Add A and A^T
//	for (int i = 0; i < (int)vA.size(); ++i)
//	{
//		vW.push_back(Triplet<Real>(Nt + vA[i].row(), vA[i].col(), vA[i].value()));
//		vW.push_back(Triplet<Real>(vA[i].col(), Nt + vA[i].row(), vA[i].value()));
//	}
//	// Add I*wx to x and Reg.
//	for (int i = 0; i < Nx; ++i)
//	{
//		vW.push_back(Triplet<Real>(i, i, wx + regDef));
//	}
//
//	// Add I*wp to p and Reg.
//	for (int i = Nx; i < Nt; ++i)
//	{
//		vW.push_back(Triplet<Real>(i, i, wp + regDef));
//	}
//
//	mW.setFromTriplets(vW.begin(), vW.end());
//	mW.makeCompressed(); // Symmetric & sparse
//
//	// Build system vector
//
//	VectorXd vbl(Nt + Nx);
//	vbl.setZero(); // Init.
//
//	for (int i = Nt; i < Nt + Nx; ++i)
//		vbl(i) = vb(i - Nt); // Residual
//
//	// Get constrained solution
//
//	VectorXd vxl(Nt + Nx);
//	vxl.setZero(); // Init.
//
//	// Solve the linear system s.t. regularization
//	for (int i = 0; i < this->m_regMaxIters; ++i)
//	{
//		if (i != 0)
//		{
//			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//
//			for (int ii = 0; ii < Nt; ++ii) // Regularize diagonal
//				mW.coeffRef(ii, ii) += this->m_rFactor*pow(10.0, i);
//
//			exit(-1);
//		}
//
//		// Assume W is not definite pos.
//		SparseLU<SparseMatrixXd> solver;
//
//		solver.compute(mW); // Factor
//		if (solver.info() != Success)
//			continue;
//
//		// Solve linear system
//		vxl = solver.solve(vbl);
//
//		// Test obtained solution
//		VectorXd vtestbl = mW*vxl;
//		Real realNorm = vbl.norm();
//		Real diffNorm = (vtestbl - vbl).norm();
//		if (diffNorm / realNorm > this->m_linearMaxError)
//			continue;
//
//		// Get subvector for variables
//		getSubvector(0, Nt, vxl, vt);
//
//		assert((int)vt.size() == Nt);
//
//		break; // Done
//	}
//
//	return true;
//}
//
//bool SolverParameterized::solveLinearSystem_CustomFactQP(SparseMatrixXd& mA, VectorXd& vb, VectorXd& vt, double wx, double wp, bool bounded, double regDef)
//{
//	// This linear solver assumes A [n x m], b [n x 1] and x [m x 1] and n < m
//	// so this is an underconstrained linear system. The problem has multiple 
//	// solutions. This is solved as a minimization min_{x} 1/2 x^T*x subject
//	// to A*x - b = 0, resulting x = A^T*(A*A^T)^-1*b (basic pseudo-inverse).
//
//	int Nx = mA.rows();
//	int Nt = mA.cols();
//
//	assert((int)vb.size() == Nx);
//
//	// Build fixed-stencil
//
//	iVector vfixIdx = this->m_pSolver->getFixedIndices();
//	iVector vsimIdx = this->m_pModel->getRawToSim_x();
//
//	bVector vfixStencil(Nx);
//	for (int i = 0; i < Nx; ++i)
//		vfixStencil[i] = false;
//
//	for (int i = 0; i < (int)vfixIdx.size(); ++i)
//	{
//		if (vsimIdx[vfixIdx[i]] == -1)
//			continue; // Not simulated
//
//		vfixStencil[vsimIdx[vfixIdx[i]]] = true;
//	}
//
//	// Default regularization
//	SparseMatrixXd R(Nx, Nx);
//	R.setIdentity(); 
//	R = R * regDef;
//
//	SparseMatrixXd AAt = mA*mA.transpose() + R;
//
//	// Compute inverse
//	SparseMatrixXd AAti;
//
//	// Solve the linear system s.t. regularization
//	for (int i = 0; i < this->m_regMaxIters; ++i)
//	{
//		if (i != 0)
//		{
//			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//
//			for (int ii = 0; ii < Nx; ++ii) // Regularize diagonal
//				AAt.coeffRef(ii, ii) += this->m_rFactor*pow(10.0, i);
//
//			exit(-1);
//		}
//
//		SparseMatrixXd I(Nx, Nx);
//		I.setIdentity(); // BBi = I
//
//		// Assume DFDx is symmetric and D.P.
//		SimplicialLDLT<SparseMatrixXd> solver;
//
//		solver.compute(AAt); // Factor
//		if (solver.info() != Success)
//			continue;
//
//		// Solve linear system
//		AAti = solver.solve(I);
//
//		//// Test obtained solution
//		//MatrixXd realI = I.toDense();
//		//SparseMatrixXd testIs = (AAt*AAti);
//		//MatrixXd testI = testIs.toDense();
//		//Real realNorm = realI.norm();
//		//Real diffNorm = (realI - testI).norm();
//		//if (diffNorm / realNorm > this->m_linearMaxError)
//		//	continue;
//
//		// Compute final solution. Here we compute
//		// first the product (AAti*vb) and to avoid
//		// the dense matrix multiplication.
//
//		vt = mA.transpose()*(AAti*vb);
//
//		assert((int)vt.size() == Nt);
//
//		break; // Done
//	}
//
//	return true;
//}
//
//bool SolverParameterized::solve() 
//{
//	this->m_pSolver->advanceBoundary(); // Advance boundary conditions: on step stronger
//
//	logSimu("\n[TRACE] Non-linear parameterized static solve with %d steps", m_solverMaxIters);
//	logSimu("\n[INFO] In %s: Non-linear parameterized static solve with %d steps\n", __FUNCTION__, m_solverMaxIters);
//
//	int Nx = this->m_pModel->getNumSimDOF_x();
//	int Np = this->m_pParam->getParNumber();
//	int Nt = Nx + Np;
//
//	VectorXd vv = this->m_pModel->getVelocities_x();
//
//	VectorXd vRi(Nx);
//	vRi.setZero();
//	Real Ui = 0.0;
//	Real Rnormi = 0.0;
//
//	// Get initial model state (unconstrained) 
//	this->m_vx = this->m_pModel->getPositions_x();
//	this->m_vp = this->m_pParam->getParameters();
//
//	this->m_pSolver->constrainBoundary();
//
//	this->m_pModel->pushState();
//
//	// Get initial model state (constrained) 
//	VectorXd vxi = this->m_pModel->getPositions_x();
//	VectorXd vpi = this->m_pParam->getParameters();
//
//	//this->m_vfp = this->m_pParam->get_fp();
//
//	int i;
//	bool solved = false;
//	for (i = 0; i < this->m_solverMaxIters; ++i)
//	{
//		this->getResidual(vxi, vpi, vRi);
//		Ui = this->getPotential(vxi, vpi);
//		Rnormi = vRi.norm();
//
//		logSimu("[TRACE] ITERATION %d - Initial residual: %.9f, potential: %.9f", i, Rnormi, Ui);
//		logSimu("[INFO] In %s: ITERATION %d - Starting solve with initial residual: %.9f, potential: %.9f\n", __FUNCTION__, i, Rnormi, Ui);
//
//		if (Rnormi <= this->m_solverMaxError)
//		{
//			solved = true;
//			break; // Out
//		}
//
//		// Solve linear system
//
//		SparseMatrixXd mA(Nx, Nt);
//		mA.setZero(); // Initialize
//		this->getMatrix(vxi, vpi, mA);
//
//		//VectorXd vdtCustomFullQP(Nt);
//		//VectorXd vdtCustomFactQP(Nt);
//		//VectorXd vdtAlgLibBLEIC(Nt);
//
//		//vdtCustomFullQP.setZero();
//		//vdtCustomFactQP.setZero();
//		//vdtAlgLibBLEIC.setZero();
//
//		VectorXd vdtFactorBound(Nt);
//		vdtFactorBound.setZero();
//
//		//VectorXd vdtMosek(Nt);
//		//vdtMosek.setZero();
//
//		//if (!this->solveLinearSystem_CustomFullQP(mA, vRi, vdtCustomFullQP, 1.0, 1.0, true, 0.0))
//		//{
//		//	solved = false;
//		//	break; // Out
//		//}
//
//		//if (!this->solveLinearSystem_CustomFactQP(mA, vRi, vdtCustomFactQP, 1.0, 1.0, true, 0.0))
//		//{
//		//	solved = false;
//		//	break; // Out
//		//}
//
//		if (!this->solveLinearSystem_FactorBound(mA, vRi, vdtFactorBound, 1.0, 1.0, true, 0.0))
//		{
//			solved = false;
//			break; // Out
//		}
//
//		//if (!this->solveLinearSystem_AlgLibBLEIC(mA, vRi, vdtAlgLibBLEIC, 1.0, 1.0, true, 0.0))
//		//{
//		//	solved = false;
//		//	break; // Out
//		//}
//
//		//if (!this->solveLinearSystem_Mosek(mA, vRi, vdtMosek, 1.0, 1.0, true, 0.0))
//		//{
//		//	solved = false;
//		//	break; // Out
//		//}
//
//		//VectorXd vdxCustomFullQP(Nx);
//		//VectorXd vdpCustomFullQP(Np); 
//		//getSubvector(0, Nx, vdtCustomFullQP, vdxCustomFullQP);
//		//getSubvector(Nx, Np, vdtCustomFullQP, vdpCustomFullQP);
//
//		//VectorXd vdxCustomFactQP(Nx);
//		//VectorXd vdpCustomFactQP(Np);
//		//getSubvector(0, Nx, vdtCustomFactQP, vdxCustomFactQP);
//		//getSubvector(Nx, Np, vdtCustomFactQP, vdpCustomFactQP);
//
//		//VectorXd vdxFactorBound(Nx);
//		//VectorXd vdpFactorBound(Np);
//		//getSubvector(0, Nx, vdtFactorBound, vdxFactorBound);
//		//getSubvector(Nx, Np, vdtFactorBound, vdpFactorBound);
//
//		//VectorXd vdxAlgLibBLEIC(Nx);
//		//VectorXd vdpAlgLibBLEIC(Np); 
//		//getSubvector(0, Nx, vdtAlgLibBLEIC, vdxAlgLibBLEIC);
//		//getSubvector(Nx, Np, vdtAlgLibBLEIC, vdpAlgLibBLEIC);
//
//		//VectorXd vdxMosek(Nx);
//		//VectorXd vdpMosek(Np);
//		//getSubvector(0, Nx, vdtMosek, vdxMosek);
//		//getSubvector(Nx, Np, vdtMosek, vdpMosek);
//
//		//logSimu("[TRACE] CUSTOM FULL STEP: vx: %.9f - vp: %.9f", vdxCustomFullQP.norm(), vdpCustomFullQP.norm());
//		//logSimu("[TRACE] CUSTOM FACT STEP: vx: %.9f - vp: %.9f", vdxCustomFactQP.norm(), vdpCustomFactQP.norm());
//		//logSimu("[TRACE] FACTOR BOUND STEP: vx:  %.9f - vp:  %.9f", vdxFactorBound.norm(), vdpFactorBound.norm());
//		//logSimu("[TRACE] ALGLIB BLEIC STEP: vx: %.9f - vp: %.9f", vdxAlgLibBLEIC.norm(), vdpAlgLibBLEIC.norm());
//		//logSimu("[TRACE] MOSEK STEP: vx: %.9f - vp: %.9f", vdxMosek.norm(), vdpMosek.norm());
//
//		//VectorXd vdt = vdtCustomFullQP;
//		//VectorXd vdt = vdtCustomFactQP;
//		VectorXd vdt = vdtFactorBound;
//		//VectorXd vdt = vdtAlgLibBLEIC;
//		//VectorXd vdt = vdtMosek;
//
//		if (this->m_hasMaxStep)
//		{
//			vdt = vdt.normalized();
//			vdt *= this->m_maxStep;
//		}
//
//		// DEBUG step: Plot energy and residual environment
//		this->plotSearchDirection(Nx, Np, vxi, vpi, vdt);
//
//		double stepSize = 0.0;
//		VectorXd vxl = vxi;
//		VectorXd vpl = vpi;
//		Real Rnorml = Rnormi;
//		Real Ul = Ui;
//		if (this->m_useLineSearch)
//		{
//			// Line-search in dx direction
//
//			int l;
//			for (l = 0; l < this->m_lsMaxIters; ++l)
//			{
//				stepSize = vdt.norm();
//
//				// Roll-back model state
//				this->m_pModel->peekState();
//
//				VectorXd vdx(Nx);
//				VectorXd vdp(Np);
//				getSubvector(0, Nx, vdt, vdx);
//				getSubvector(Nx, Np, vdt, vdp);
//
//				logSimu("[TRACE] Testing steps, parameter: %.9f, deformed: %.9f", vdp.norm(), vdx.norm());
//
//				// Update model state to the new state
//
//				vxi += vdx;
//
//				this->m_pModel->updateState_x(vxi, vv);
//				vxi = this->m_pModel->getPositions_x();
//				Ui = this->getPotential(vxi, vpi);
//
//				// Update parameterization state 
//
//				vpi += vdp;
//
//				VectorXd vpBoundDist; // Check if the computed step is valid within the tolerance
//				if (!this->m_pParam->isValidParameters(vpi, vpBoundDist, this->m_linearMaxError))
//				{
//					logSimu("[TRACE] Invalid step, distance to valid: %.9f", vpBoundDist.norm());
//					exit(-1);
//				}
//
//				this->m_pParam->setParameters(vpi);
//				vpi = this->m_pParam->getParameters();
//				this->getResidual(vxi, vpi, vRi);
//				Rnormi = vRi.norm();
//
//				bool improved = false;
//
//				// Improve?
//				if (Ui < Ul)
//				{
//					logSimu("[TRACE] Improved potential %.9f", Ui / Ul);
//					improved |= true;
//				}
//
//				if (Rnormi < Rnorml)
//				{
//					logSimu("[TRACE] Improved residual %.9f", Rnormi / Rnorml);
//					improved |= true;
//				}
//
//				if (improved)
//					break;
//
//				// Bisection / restore
//
//				vdt *= this->m_lsBeta;
//				vxi = vxl;
//				vpi = vpl;
//			}
//
//			if (l == this->m_lsMaxIters)
//			{
//				//writeToFile(mA, "jacobianNotConverged.csv", false);
//
//				//this->m_vfp = this->m_pParam->get_fp();
//				this->m_vx = vxi;
//				this->m_vp = vpi;
//				logSimu("[ERROR] Bisections: %d, step size: %.9f, residual: %.9f, potential: %.9f\n", l, stepSize, Rnormi, Ui);
//				logSimu("[ERROR] In %s: NOT improved after %d bisections, step size: %.9f, residual: %.9f, potential: %.9f\n", __FUNCTION__, l, stepSize, Rnormi, Ui);
//
//				solved = false;
//				break; // Error
//			}
//			else
//			{
//				//this->m_vfp = this->m_pParam->get_fp();
//				this->m_vx = vxi;
//				this->m_vp = vpi;
//				logSimu("[GOOD] Bisections: %d, step size: %.9f, residual: %.9f, potential: %.9f\n", l, stepSize, Rnormi, Ui);
//				logSimu("[GOOD] In %s: improved after %d bisections, step size: %.9f, residual: %.9f, potential: %.9f\n", __FUNCTION__, l, stepSize, Rnormi, Ui);
//			}
//		}
//		else
//		{
//			// Do not perform line search, this way the solution
//			// is not guaranteed to reduce the overall error so
//			// should be used carefully
//
//			VectorXd vdx(Nx); vdx.setZero();
//			VectorXd vdp(Np); vdp.setZero();
//			getSubvector(0, Nx, vdt, vdx);
//			getSubvector(Nx, Np, vdt, vdp);
//
//			vxi += vdx; 
//			vpi += vdp;
//	
//			// Update model state to the new state
//			this->m_pModel->updateState_x(vxi, vv);
//
//			// Update parameterization state 
//			this->m_pParam->setParameters(vpi);
//
//
//			// Compute residual and potential
//			this->getResidual(vxi, vpi, vRi);
//			Ui = this->getPotential(vxi, vpi);
//			Rnormi = vRi.norm();
//
//			// Improve?
//			if (Ui < Ul)
//			{
//				break;
//			}
//			else if (Rnormi < Rnorml)
//			{
//				break;
//			}
//		}
//	}
//
//	// Pop the last state
//	this->m_pModel->popState();
//
//	if (solved)
//	{
//		logSimu("[GOOD] Converged after %d iterations, residual: %.9f", i, Rnormi);
//		logSimu("[GOOD] In %s: converged after %d iterations, residual: %.9f\n", __FUNCTION__, i, Rnormi);
//	}
//	else
//	{
//		logSimu("[ERROR] NOT converged after %d iterations, residual: %.9f", i, Rnormi);
//		logSimu("[ERROR] In %s: NOT converged after %d iterations, residual: %.9f\n", __FUNCTION__, i, Rnormi);
//	}
//
//	return solved;
//}
//
//void SolverParameterized::plotSearchDirection(int Nx, int Np, const VectorXd& vxi, const VectorXd& vpi, const VectorXd& vdt)
//{
//	const VectorXd& vv = this->m_pModel->getVelocities_x();
//
//	VectorXd vgOri = this->m_pModel->getGravity();
//	VectorXd vgNil(3);
//	vgNil.setZero();
//	//this->m_pModel->setGravity(vgNil);
//
//	MatrixXd results(101, 7 + 2*(int) vpi.size());
//
//	// Both
//
//	for (int i = 0; i < 101; ++i)
//	{
//		Real alpha = -1 + 0.02*i;
//
//		// Roll-back model state
//	
//		VectorXd vdt_i = alpha*vdt;
//
//		VectorXd vdx_i(Nx);
//		VectorXd vdp_i(Np);
//		getSubvector(0, Nx, vdt_i, vdx_i);
//		getSubvector(Nx, Np, vdt_i, vdp_i);
//
//		results(i, 0) = sign(alpha)*vdt_i.norm();
//
//		VectorXd vxnew;
//		VectorXd vpnew;
//		VectorXd vR_i;
//		Real resNorm = 0.0;
//		Real energy = 0.0;
//	
//		// X
//
//		this->m_pModel->peekState();
//
//		this->m_pModel->updateState_x(vxi + vdx_i, vv);
//		this->m_pParam->setParameters(vpi);
//		this->getResidual(vxi + vdx_i, vv, vR_i);
//		energy = this->m_pModel->getEnergy();
//		resNorm = vR_i.norm();
//		results(i, 1) = energy;
//		results(i, 2) = resNorm;
//
//		// P
//
//		this->m_pModel->peekState();
//
//		this->m_pModel->updateState_x(vxi, vv);
//		this->m_pParam->setParameters(vpi + vdp_i);
//		this->getResidual(vxi, vv, vR_i);
//		energy = this->m_pModel->getEnergy();
//		resNorm = vR_i.norm();
//		results(i, 3) = energy;
//		results(i, 4) = resNorm;
//	
//		// Both
//
//		this->m_pModel->peekState();
//
//		this->m_pModel->updateState_x(vxi + vdx_i, vv);
//		this->m_pParam->setParameters(vpi + vdp_i);
//		this->getResidual(vxi + vdx_i, vv, vR_i);
//		energy = this->m_pModel->getEnergy();
//		resNorm = vR_i.norm();
//		results(i, 5) = energy;
//		results(i, 6) = resNorm;
//
//		// Individual P
//
//		for (int j = 0; j < (int)vpi.size(); ++j)
//		{
//			VectorXd vxnew = vxi;
//			VectorXd vpnew = vpi;
//			vpnew(j) += vdp_i(j);
//
//			// P
//
//			this->m_pModel->peekState();
//
//			this->m_pModel->updateState_x(vxi, vv);
//			this->m_pParam->setParameters(vpnew);
//			this->getResidual(vxi + vdx_i, vv, vR_i);
//			energy = this->m_pModel->getEnergy();
//			resNorm = vR_i.norm();
//			results(i, 7 + 2 * j + 0) = energy;
//			results(i, 7 + 2 * j + 1) = resNorm; 
//		}
//	}
//
//	writeToFile(results, "stepEnv.csv");
//
//	//this->m_pModel->setGravity(vgOri);
//}
//
//
//Real SolverParameterized::getPotential(const VectorXd& vx, const VectorXd& vp)
//{
//	Real potential = 0;
//
//	const VectorXd& vv = this->m_pModel->getVelocities_x();
//
//	// Add the model elastic potential first
//	potential += this->m_pModel->getEnergy();
//
//	// Add boundary conditions potential: gravity, soft-fixed
//	potential += this->m_pSolver->getBoundaryEnergy(vx, vv);
//
//	// Add an estimation of the work done by parameter forces. For that we use the displacement in the parameter values 
//	// since the last iteration. As a result, the contribution of parameter work should be zero at the beginning of the
//	// iteration and, in every subsequent evaluation (e.g. line search) this should be one such that it compensate the 
//	// fact that we are not looking for a critical point at parameters. As an estimation of parameter forces we average
//	// current value to the one at the beginning of the step.
//
//	//VectorXd fp = 0.5*(this->m_pParam->get_fp() + this->m_vfp);
//	//VectorXd fp = this->m_pParam->get_fp();
//	//VectorXd fp = this->m_vfp;
//
//	//VectorXd dp = vp - this->m_vp;
//
//	//Real fpWork = fp.dot(dp);
//	//if (!isApprox(fpWork, 0.0, EPS_APPROX))
//	//	potential -= fpWork; // Add fp work
//
//	 // TODO: Add soft-constraints and possibly
//	 // fixed points also on parameters forces
//
//	return potential;
//}
//
//void SolverParameterized::getResidual(const VectorXd& vx, const VectorXd& vp, VectorXd& vR)
//{
//	// We use here exatly the same residual as when we are computing
//	// the usual static equilibrium of the model as we are not really
//	// interested on the force at parameter degrees-of-freedom.
//
//	this->m_pSolver->getResidual(vx, this->m_pModel->getVelocities_x(), vR);
//}
//
//void SolverParameterized::getMatrix(const VectorXd& vx, const VectorXd& vp, SparseMatrixXd& mA)
//{
//	const VectorXd& vv = this->m_pModel->getVelocities_x();
//
//	int nx = this->m_pModel->getNumSimDOF_x();
//	int np = this->m_pParam->getParNumber();
//
//	tVector vDfxDt;
//	tVector vDfDp;
//
//	// Jacobian at deformation -----------------------
//
//	vDfxDt.reserve(this->m_pModel->getNumNonZeros_DfxDx());
//
//	// Append triplets from elastic DfxDx
//	this->m_pModel->add_DfxDx(vDfxDt, NULL);
//
//	// Append triplests from boundary conditions DfxDx
//	this->m_pSolver->addBoundaryJacobian(vx, vv, vDfxDt);
//
//	// Constrain matrix given BC (e.g fixed)
//	this->m_pSolver->constrainBoundary(vDfxDt);
//
//	// Add default regularization
//	for (int i = 0; i < nx; ++i)
//	{
//		vDfxDt.push_back(Triplet<Real>(i, i, 1e-3));
//	}
//
//	// Jacobian at parameters --------------------------
//
//	SparseMatrixXd mDfDp = this->m_pParam->get_DfDp();
//
//	// Get matrix in triplets
//	toTriplets(mDfDp, vDfDp);
//
//	// TODO: Add soft-constraints and possibly
//	// fixed points also on parameters forces
//
//	// Assemble both Jacobians ------------------------
//
//	int numCoeffx = (int)vDfxDt.size();
//	int numCoeffp = (int)vDfDp.size();
//
//	// Extend DfxDt with parameters colums
//	vDfxDt.reserve(numCoeffx + numCoeffp);
//
//	for (int i = 0; i < numCoeffp; ++i)
//	{
//		vDfxDt.push_back(Triplet<Real>(vDfDp[i].row(), vDfDp[i].col() + nx, vDfDp[i].value()));
//	}
//
//	mA.resize(nx, nx + np);
//
//	mA.setFromTriplets(vDfxDt.begin(), vDfxDt.end());
//	mA.makeCompressed(); // Ensure asymmetric sparse
//
//	// We are considering a positive sum of potential
//	// energies so we have to change the sign of forces
//	// so that dUdx = -fx
//
//	mA *= -1;
//}
//
////bool SolverParameterized::solveLinearSystem(const SparseMatrixXd& mAx, 
////											const SparseMatrixXd& mAp, 
////											const VectorXd& vbx, 
////											VectorXd& vx, 
////											VectorXd& vp)
////{
//////	// This linear solver assumes A [n x m], b [n x 1] and x [m x 1] and n < m
//////	// so this is an underconstrained linear system. The problem has multiple 
//////	// solutions. This is solved as a minimization min_{x} 1/2 x^T*x subject
//////	// to A*x - b = 0, resulting x = A^T*(A*A^T)^-1*b (basic pseudo-inverse).
//////
//////	int Nx = mA.rows();
//////	int Nt = mA.cols();
//////
//////	assert((int)vb.size() == Nx);
//////
//////	// Build fixed-stencil
//////
//////	iVector vfixIdx = this->m_pSolver->getFixedIndices();
//////	iVector vsimIdx = this->m_pModel->getMapSimDOF_x();
//////
//////	bVector vfixStencil(Nx);
//////	for (int i = 0; i < Nx; ++i)
//////		vfixStencil[i] = false;
//////
//////	for (int i = 0; i < (int)vfixIdx.size(); ++i)
//////	{
//////		if (vsimIdx[vfixIdx[i]] == -1)
//////			continue; // Not simulated
//////
//////		vfixStencil[vsimIdx[vfixIdx[i]]] = true;
//////	}
//////
//////	// Default regularization
//////	SparseMatrixXd R(Nx, Nx);
//////	R.setIdentity(); R *= 1e-3;
//////
//////	SparseMatrixXd AAt = mA*mA.transpose() + R;
//////
//////	// Compute inverse
//////	SparseMatrixXd AAti;
//////
//////	// Solve the linear system s.t. regularization
//////	for (int i = 0; i < this->m_regMaxIters; ++i)
//////	{
//////		if (i != 0)
//////		{
//////			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//////			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//////
//////			for (int ii = 0; ii < Nx; ++ii) // Regularize diagonal
//////				AAt.coeffRef(ii, ii) += this->m_rFactor*pow(10.0, i);
//////		}
//////
//////		SparseMatrixXd I(Nx, Nx);
//////		I.setIdentity(); // BBi = I
//////
//////		// Assume DFDx is symmetric and D.P.
//////		SimplicialLDLT<SparseMatrixXd> solver;
//////
//////		solver.compute(AAt); // Factor
//////		if (solver.info() != Success)
//////			continue;
//////
//////		// Solve linear system
//////		AAti = solver.solve(I);
//////
//////		//// Test obtained solution
//////		//MatrixXd realI = I.toDense();
//////		//SparseMatrixXd testIs = (AAt*AAti);
//////		//MatrixXd testI = testIs.toDense();
//////		//Real realNorm = realI.norm();
//////		//Real diffNorm = (realI - testI).norm();
//////		//if (diffNorm / realNorm > this->m_linearMaxError)
//////		//	continue;
//////
//////		//// Assume DFDx is symmetric and D.P.
//////		//SparseLU<SparseMatrixXd> solverLU;
//////		//solverLU.compute(AAt); // Factor
//////		//if (solverLU.info() == Success)
//////		//{
//////		//	// Solve linear system
//////		//	AAti = solverLU.solve(I);
//////
//////		//	// Test obtained solution
//////		//	MatrixXd realI = I.toDense();
//////		//	SparseMatrixXd testIs = (AAt*AAti);
//////		//	MatrixXd testI = testIs.toDense();
//////		//	Real realNorm = realI.norm();
//////		//	Real diffNorm = (realI - testI).norm();
//////		//	if (diffNorm / realNorm > this->m_linearMaxError)
//////		//		continue;
//////		//}
//////
//////		//// Assume DFDx is symmetric and D.P.
//////		//SimplicialLDLT<SparseMatrixXd> solverLDLT;
//////		//solverLDLT.compute(AAt); // Factor
//////		//if (solverLDLT.info() == Success)
//////		//{
//////		//	// Solve linear system
//////		//	AAti = solverLDLT.solve(I);
//////
//////		//	// Test obtained solution
//////		//	MatrixXd realI = I.toDense();
//////		//	SparseMatrixXd testIs = (AAt*AAti);
//////		//	MatrixXd testI = testIs.toDense();
//////		//	Real realNorm = realI.norm();
//////		//	Real diffNorm = (realI - testI).norm();
//////		//	if (diffNorm / realNorm > this->m_linearMaxError)
//////		//		continue;
//////		//}
//////
//////		// Compute final solution. Here we compute
//////		// first the product (AAti*vb) and to avoid
//////		// the dense matrix multiplication.
//////
//////		vx = mA.transpose()*(AAti*vb);
//////
//////		writeToFile(vx, "rawStep.csv");
//////
//////		assert((int)vx.size() == Nt);
//////
//////		break; // Done
//////	}
////
////	return true;
////}
//
//
////void SolverParameterized::getMatrix(const VectorXd& vx, const VectorXd& vp, SparseMatrixXd& mAx, SparseMatrixXd& mAp)
////{
////	const VectorXd& vv = this->m_pModel->getVelocities_x();
////
////	int nx = this->m_pModel->getNumSimDOF_x();
////	int np = this->m_pParam->getParNumber();
////
////	tVector vDfxDx;
////	tVector vDfDp;
////
////	// Jacobian at deformation -----------------------
////
////	vDfxDx.reserve(this->m_pModel->getNumNonZeros_DfxDx());
////
////	// Append triplets from elastic DfxDx
////	this->m_pModel->add_DfxDx(vDfxDx, NULL);
////
////	// Append triplests from boundary conditions DfxDx
////	this->m_pSolver->addBoundaryJacobian(vx, vv, vDfxDx);
////
////	// Constrain matrix given BC (e.g fixed)
////	this->m_pSolver->constrainBoundary(vDfxDx);
////
////	mAx.resize(nx, nx);
////	mAx.setFromTriplets(vDfxDx.begin(), vDfxDx.end());
////	mAx.makeCompressed(); // Ensure symmetric & sparse
////
////	// Jacobian at paramters --------------------------
////
////	vDfDp = this->m_pParam->get_DfDp();
////
////	// TODO: Add soft-constraints and possibly
////	// fixed points also on parameters forces
////
////	mAp.resize(nx, np);
////	mAp.setFromTriplets(vDfDp.begin(), vDfDp.end());
////	mAp.makeCompressed(); // Ensure asymmetric & sparse
////
////	// We are considering a positive sum of potential
////	// energies so we have to change the sign of forces
////	// so that dUdx = -fx
////
////	mAx *= -1;
////	mAp *= -1;
////}
////
////bool SolverParameterized::solve()
////{
////	this->m_pSolver->advanceBoundary(); // Advance boundary conditions: on step stronger
////
////	logSimu("\n[TRACE] Non-linear parameterized static solve with %d steps", m_solverMaxIters);
////	logSimu("\n[INFO] In %s: Non-linear parameterized static solve with %d steps\n", __FUNCTION__, m_solverMaxIters);
////
////	int Nx = this->m_pModel->getNumSimDOF_x();
////	int Np = this->m_pParam->getParNumber();
////
////	VectorXd vv = this->m_pModel->getVelocities_x();
////
////	VectorXd vRi(Nx);
////	vRi.setZero();
////	Real Ui = 0.0;
////	Real Rnormi = 0.0;
////
////	// Back-up model state
////	this->m_pModel->pushState();
////
////	// Get initial model state (unconstrained) 
////	this->m_vx = this->m_pModel->getPositions_x();
////	this->m_vp = this->m_pParam->getParameters();
////
////	this->m_pSolver->constrainBoundary();
////
////	this->m_pModel->pushState();
////
////	// Get initial model state (constrained) 
////	VectorXd vxi = this->m_pModel->getPositions_x();
////	VectorXd vpi = this->m_pParam->getParameters();
////
////	this->m_vfp = this->m_pParam->get_fp();
////
////	int i;
////	bool solved = true;
////	for (i = 0; i < this->m_solverMaxIters; ++i)
////	{
////		if (i == 0)
////		{
////			this->getResidual(vxi, vpi, vRi);
////			Ui = this->getPotential(vxi, vpi);
////			Rnormi = vRi.norm();
////
////			logSimu("[TRACE] Starting solve with initial residual: %.9f, potential: %.9f", Rnormi, Ui);
////			logSimu("[INFO] In %s: Starting solve with initial residual: %.9f, potential: %.9f\n", __FUNCTION__, Rnormi, Ui);
////		}
////
////		if (Rnormi <= this->m_solverMaxError)
////		{
////			break; // Out
////		}
////
////		// Solve linear system
////
////		SparseMatrixXd mAx(Nx, Nx);
////		SparseMatrixXd mAp(Nx, Np);
////		mAx.setZero();
////		mAp.setZero();
////		this->getMatrix(vxi, vpi, mAx, mAp);
////		vRi = -vRi; // Step dx = -A^-1*R
////
////		VectorXd vdx(Nx);
////		VectorXd vdp(Np);
////		vdx.setZero();
////		vdp.setZero();
////
////		if (!this->solveLinearSystem(mAx, mAp, vRi, vdx, vdp))
////		{
////			solved = false;
////			break; // Out
////		}
////
////		if (this->m_hasMaxStep)
////		{
////			vdx = vdx.normalized();
////			vdp = vdp.normalized();
////			vdx *= this->m_maxStep;
////			vdp *= this->m_maxStep;
////		}
////
////		double stepSize;
////		VectorXd vxl = vxi;
////		VectorXd vpl = vpi;
////		Real Rnorml = Rnormi;
////		Real Ul = Ui;
////		if (this->m_useLineSearch)
////		{
////			// Line-search in dx direction
////
////			int l;
////			for (l = 0; l < this->m_lsMaxIters; ++l)
////			{
////				stepSize = vdx.norm() + vdp.norm();
////
////				// Roll-back model state
////				this->m_pModel->peekState();
////
////				vxi += vdx;
////				vpi += vdp;
////
////				// Update model state to the new state
////				this->m_pModel->updateState_x(vxi, vv);
////				vxi = this->m_pModel->getPositions_x();
////
////				// Update parameterization state 
////				this->m_pParam->setParameters(vpi);
////				vpi = this->m_pParam->getParameters();
////
////				// Compute residual and potential
////				this->getResidual(vxi, vpi, vRi);
////				Ui = this->getPotential(vxi, vpi);
////				Rnormi = vRi.norm();
////
////				// Improve?
////				if (Ui < Ul)
////				{
////					logSimu("[TRACE] Improved through potential");
////					break;
////				}
////				//else if (Rnormi < Rnorml)
////				//{
////				//	logSimu("[TRACE] Improved through residual");
////				//	break;
////				//}
////
////				// Bisection / restore
////
////				vdx *= this->m_lsBeta;
////				vdp *= this->m_lsBeta;
////				vxi = vxl;
////				vpi = vpl;
////			}
////
////			if (l == this->m_lsMaxIters)
////			{
////				writeToFile(mAx, "Ax_NotConverged.csv", false);
////				writeToFile(mAp, "Ap_NotConverged.csv", false);
////
////				this->m_vfp = this->m_pParam->get_fp();
////				this->m_vx = vxi;
////				this->m_vp = vpi;
////				logSimu("[ERROR] Improved after %d bisections, step size: %.9f, residual: %.9f, potential: %.9f\n", l, stepSize, Rnormi, Ui);
////				logSimu("[ERROR] In %s: NOT improved after %d bisections, step size: %.9f, residual: %.9f, potential: %.9f\n", __FUNCTION__, l, stepSize, Rnormi, Ui);
////
////				solved = false;
////				break; // Error
////			}
////			else
////			{
////				this->m_vfp = this->m_pParam->get_fp();
////				this->m_vx = vxi;
////				this->m_vp = vpi;
////				logSimu("[GOOD] Improved after %d bisections, step size: %.9f, residual: %.9f, potential: %.9f\n", l, stepSize, Rnormi, Ui);
////				logSimu("[GOOD] In %s: improved after %d bisections, step size: %.9f, residual: %.9f, potential: %.9f\n", __FUNCTION__, l, stepSize, Rnormi, Ui);
////			}
////		}
////		else
////		{
////			// Do not perform line search, this way the solution
////			// is not guaranteed to reduce the overall error so
////			// should be used carefully
////
////			vxi += vdx;
////			vpi += vdp;
////
////			// Update model state to the new state
////			this->m_pModel->updateState_x(vxi, vv);
////
////			// Update parameterization state 
////			this->m_pParam->setParameters(vpi);
////
////			// Compute residual and potential
////			this->getResidual(vxi, vpi, vRi);
////			Ui = this->getPotential(vxi, vpi);
////			Rnormi = vRi.norm();
////
////			// Improve?
////			if (Ui < Ul)
////			{
////				break;
////			}
////			else if (Rnormi < Rnorml)
////			{
////				break;
////			}
////		}
////	}
////
////	if (solved)
////	{
////		logSimu("[GOOD] Converged after %d iterations, residual: %.9f", Rnormi);
////		logSimu("[GOOD] In %s: converged after %d iterations, residual: %.9f\n", __FUNCTION__, i, Rnormi);
////	}
////	else
////	{
////		logSimu("[ERROR] NOT converged after %d iterations, residual: %.9f", i, Rnormi);
////		logSimu("[ERROR] In %s: NOT converged after %d iterations, residual: %.9f\n", __FUNCTION__, i, Rnormi);
////	}
////
////	return solved;
////}