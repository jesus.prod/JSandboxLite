/*=====================================================================================*/
/*!
\file		BConditionPreStrainTensileS.cpp
\author		jesusprod
\brief		Implementation of BConditionPreStrainTensileS
/*=====================================================================================*/

#include <JSandbox/Solver/BConditionPreStrainTensileS.h>

#include <JSandbox/Model/TensileStructureModel.h>

BConditionPreStrainTensileS::BConditionPreStrainTensileS()
{
	this->m_factor = 1.0;
	this->m_maxStep = 1;
	this->m_curStep = 1;
}

BConditionPreStrainTensileS::~BConditionPreStrainTensileS()
{
	// Nothing to do here...
}

void BConditionPreStrainTensileS::constrain(SolidModel* pModel) const
{
	TensileStructureModel* pTSModel = dynamic_cast<TensileStructureModel*>(pModel);

	double factor = 1.0 - (1.0 - this->m_factor)*this->getLoadingFactor();

	for (int i = 0; i < pTSModel->getNumSurfaceModels(); ++i)
	{
		SurfaceModel* pSModel = pTSModel->getSurfaceModel(i);

		VectorXd vs; // Check current
		pSModel->getParam_Stretch(vs);

		// Set new stretch factor if necesary
		if (!isApprox(vs(0), factor, EPS_POS))
		{
			vs(0) = factor; // New stretch
			pSModel->setParam_Stretch(vs);
		}
	}

	logSimu("[TRACE] Constrain applied: Surface pre-strain factor %f\n", factor);
}