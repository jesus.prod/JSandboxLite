/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_RodMeshRest.h
\author		jesusprod
\brief		Parameter set for the rest state of rods in a tensile structure.
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_TENSILE_HERMITE_RODMESH_REST_H
#define PARAMETER_SET_TENSILE_HERMITE_RODMESH_REST_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/Model/TensileStructureModel.h>

class JSANDBOX_EXPORT ParameterSet_Tensile_HermiteRodMeshRest : public ParameterSet
{
public:
	ParameterSet_Tensile_HermiteRodMeshRest(int midx);
	virtual ~ParameterSet_Tensile_HermiteRodMeshRest();

	virtual int getNumPoints() const { return this->m_controlNum; }
	virtual void setNumPoints(int cn) { this->m_controlNum = cn; }

	virtual Real getTolerance() const { return this->m_tolerance; }
	virtual void setTolerance(Real to) { this->m_tolerance = to; }

	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual bool getIs2D() const { return this->m_is2D; }
	virtual void setIs2D(bool is2D) { this->m_is2D = is2D; }

	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

	virtual void updateControlPoints();

	virtual void testHermiteJacobian() const;

protected:

	TensileStructureModel* m_pTSModel;

	virtual void concatenatedToRods(const VectorXd& vpAll, vector<VectorXd>& vpRod) const;
	virtual void rodsToConcatenated(const vector<VectorXd>& vpRod, VectorXd& vpAll) const;

	virtual void computeJac(SparseMatrixXd& mDpDm) const;

	virtual void computeJacRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, tVector& vDpDm) const;
	virtual void computeValRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, VectorXd& vpInt) const;

	int m_midx;

	bool m_is2D;

	int m_controlNum;
	Real m_tolerance;
	vector<VectorXd> m_vcontrolVal;
	vector<VectorXd> m_vcontrolPar;

	iVector m_vmapSplit2Whole;

};

#endif