///*=====================================================================================*/
///*!
//\file		ObjectiveError_Position.h
//\author		jesusprod
//\brief		Position based objective for models.
//*/
///*=====================================================================================*/
//
//#ifndef OBJECTIVE_ERROR_POSITION_H
//#define OBJECTIVE_ERROR_POSITION_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//
//#include <JSandbox/Optim/ObjectiveError.h>
//
//class ModelParameterization;
//
//class JSANDBOX_EXPORT ObjectiveError_Position : public ObjectiveError
//{
//public:
//	ObjectiveError_Position();
//
//	virtual ~ObjectiveError_Position();
//
//	virtual const VectorXd& getTargetPosition() const;
//	virtual void setTargetPosition(const VectorXd& vt);
//
//	virtual void setup();
//
//	virtual Real getError() const;
//	virtual void add_DEDx(VectorXd& vDEDx) const;
//	virtual void add_D2EDx2(tVector& vD2EDx2) const;
//
//protected:
//
//	VectorXd m_vxt;
//
//};
//
//#endif