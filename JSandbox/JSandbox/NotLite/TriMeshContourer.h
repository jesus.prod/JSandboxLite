/*=====================================================================================*/
/*!
\file		TriMeshPartitioner.h
\author		jesusprod
\brief		TriMesh partion maker (to parametrize).
*/
/*=====================================================================================*/

#ifndef TRIMESH_CONTOURER_H
#define TRIMESH_CONTOURER_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/RodMesh.h>
#include <JSandbox/Geometry/TriMesh.h>
#include <JSandbox/Geometry/SurfaceRemesherContour.h>

typedef struct ContouringOP
{
	bool m_isOK;
	TriMesh* m_pTriMesh;
	RodMesh* m_pRodMesh;
	RemeshOP m_remOP;
} ContouringOP;

class JSANDBOX_EXPORT TriMeshContourer
{

public:
	TriMeshContourer(void);
	virtual ~TriMeshContourer(void);

	virtual Real getEdgeMoveFactor() const { return this->m_edgeMoveFactor; }
	virtual Real getFaceMoveFactor() const { return this->m_faceMoveFactor; }
	virtual Real getIntersecEpsilon() const { return this->m_intersecEpsi; }
	virtual Real getBoundaryEpsilon() const { return this->m_boundaryEpsi; }

	virtual void setContourNodes(bool c) { this->m_contourNodes = c; }
	virtual void setContourEdges(bool c) { this->m_contourEdges = c; }
	virtual void setContourExtremes(bool c) { this->m_contourExtremes = c; }

	virtual void setEdgeMoveFactor(Real f) { this->m_edgeMoveFactor = f; }
	virtual void setFaceMoveFactor(Real f) { this->m_faceMoveFactor = f; }
	virtual void setIntersecEpsilon(Real e) { this->m_intersecEpsi = e; }
	virtual void setBoundaryEpsilon(Real e) { this->m_boundaryEpsi = e; }

	virtual bool makeContouring(const TriMesh& triMesh, const RodMesh& rodMesh);

	virtual const ContouringOP& getResult() const { return this->m_resultOP; }

protected:

	virtual bool prepareRodMesh(const RodMesh& meshIn, RodMesh& meshOut);

	ContouringOP m_resultOP;

	Real m_edgeMoveFactor;
	Real m_faceMoveFactor;
	Real m_intersecEpsi;
	Real m_boundaryEpsi;

	bool m_contourNodes;
	bool m_contourEdges;
	bool m_contourExtremes;

};

#endif