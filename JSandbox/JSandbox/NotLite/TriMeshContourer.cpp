/*=====================================================================================*/
/*!
\file		TriMeshContourer.cpp
\author		jesusprod
\brief		Implementation of TriMeshContourer.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/TriMeshContourer.h>


TriMeshContourer::TriMeshContourer()
{
	this->m_resultOP.m_pTriMesh = NULL;
	this->m_resultOP.m_pRodMesh = NULL;
}

TriMeshContourer::~TriMeshContourer()
{
	// Nothing to do here...
}

bool TriMeshContourer::makeContouring(const TriMesh& triMesh, const RodMesh& rodMesh)
{
	this->m_resultOP.m_isOK = false;
	delete this->m_resultOP.m_pRodMesh;
	delete this->m_resultOP.m_pTriMesh;

	this->m_resultOP.m_pTriMesh = new TriMesh(triMesh);
	this->m_resultOP.m_pRodMesh = new RodMesh(rodMesh);

	// Prepare rod mesh: split rod meshes whenever a pair
	// of rods intersect so that in the end intersections
	// are only placed at extremes. That improves the
	// contouring

	this->prepareRodMesh(rodMesh, *m_resultOP.m_pRodMesh);

	// Use a Contour Surface Remesher to create a sequence
	// of edges in the triangle mesh that follow a set of
	// curves.

	SurfaceRemesherContour contourRemesher;

	contourRemesher.setContourEdges(this->m_contourEdges);
	contourRemesher.setContourNodes(this->m_contourNodes);
	contourRemesher.setContourExtremes(this->m_contourExtremes);
	contourRemesher.setEdgeMoveParameter(this->m_edgeMoveFactor);
	contourRemesher.setFaceMoveParameter(this->m_faceMoveFactor);
	contourRemesher.setInsersectEPS(this->m_intersecEpsi);
	contourRemesher.setBoundaryEPS(this->m_boundaryEpsi);

	const RodMesh& contours = *this->m_resultOP.m_pRodMesh;
	int nr = contours.getNumRod(); // Add mesh contours
	for (int i = 0; i < nr; ++i)
		contourRemesher.addContourCurve(contours.getRod(i).getCurve());

	const RemeshOP& contourOP = contourRemesher.remesh(this->m_resultOP.m_pTriMesh);
	if (!contourOP.m_OK)
		return false; // Error in contouring, return false
	else
	{
		this->m_resultOP.m_remOP = contourOP;

		delete this->m_resultOP.m_pTriMesh; // Exchange
		this->m_resultOP.m_pTriMesh = contourOP.m_pMesh;

		this->m_resultOP.m_pTriMesh->testOverlappedVertices(1e-6);
		this->m_resultOP.m_pTriMesh->testInvertedTriangles();
		this->m_resultOP.m_pTriMesh->testIsolatedVertices();
	}

	this->m_resultOP.m_isOK = true;

	return true;
}

bool TriMeshContourer::prepareRodMesh(const RodMesh& meshIn, RodMesh& meshOut)
{
	// TODO: Made fast, to many unnecessary copies
	// of rods and curves here. Optimize this part
	// of the code.

	int nr = meshIn.getNumRod();

	vector<Curve> vcurves;
	for (int r = 0; r < nr; ++r) // Copy curves to split
		vcurves.push_back(meshIn.getRod(r).getCurve());

	vector<Curve> vsplittedCurves;

	// Split curves at intersection
	vector<vector<SplitPoint>> vpoints;
	Curve::Intersect(vcurves, vpoints);
	for (int r = 0; r < nr; r++)
	{
		if (!vpoints[r].empty())
		{
			vector<Curve> rodCurves; // Vector of splitted curve
			Curve::Split(vcurves[r], vpoints[r], rodCurves);
			for (int c = 0; c < (int)rodCurves.size(); c++)
				vsplittedCurves.push_back(rodCurves[c]);
		}
		else vsplittedCurves.push_back(vcurves[r]); // Whole curve
	}

	int nsc = (int)vsplittedCurves.size();

	vector<Rod> vsplittedRods(nsc);
	for (int c = 0; c < nsc; ++c) // Add splitted rod
		vsplittedRods[c].initialize(vsplittedCurves[c]);

	meshOut.init(vsplittedRods);

	return true;
}
