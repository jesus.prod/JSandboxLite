/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_EdgeRadius.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_EdgeRadius.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_EdgeRadius.h>

ParameterSet_Tensile_EdgeRadius::ParameterSet_Tensile_EdgeRadius(int midx) : ParameterSet("TENEDGERADIUS")
{
	assert(midx >= 0);
	this->m_midx = midx;

	this->m_isAni = true;
}

ParameterSet_Tensile_EdgeRadius::~ParameterSet_Tensile_EdgeRadius()
{
	// Nothing to do here
}

void ParameterSet_Tensile_EdgeRadius::getParameters(VectorXd& vp) const
{
	this->m_pTSModel->getRodMeshModel(this->m_midx)->getParam_RadiusEdge(vp);

	if (!this->m_isAni)
	{
		throw new exception("Not implemented yet...");
	}

	assert((int)vp.size() == this->m_np);
}

void ParameterSet_Tensile_EdgeRadius::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	VectorXd vpB = vp;
	this->limitParameters(vpB);
	this->m_pTSModel->getRodMeshModel(this->m_midx)->setParam_RadiusEdge(vpB);

	if (!this->m_isAni)
	{
		throw new exception("Not implemented yet...");
	}
}

bool ParameterSet_Tensile_EdgeRadius::isReady_fp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_fre();
}

bool ParameterSet_Tensile_EdgeRadius::isReady_DfDp() const
{
	// TODO: Convert to isotropic 

	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_DfxDre();
}

void ParameterSet_Tensile_EdgeRadius::get_fp(VectorXd& vfp) const
{
	this->m_pTSModel->getRodMeshModel(this->m_midx)->add_fre(vfp);

	if (!this->m_isAni)
	{
		throw new exception("Not implemented yet...");
	}
}

void ParameterSet_Tensile_EdgeRadius::get_DfDp(tVector& vDfDp_t) const
{
	tVector vDfDp_i;
	vDfDp_i.reserve(this->m_pTSModel->getRodMeshModel(this->m_midx)->getNumNonZeros_DfxDrr());
	vDfDp_t.reserve(this->m_pTSModel->getRodMeshModel(this->m_midx)->getNumNonZeros_DfxDrr());
	this->m_pTSModel->getRodMeshModel(this->m_midx)->add_DfxDre(vDfDp_i);
	int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);
	this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_i, vDfDp_t);

	if (!this->m_isAni)
	{
		throw new exception("Not implemented yet...");
	}
}

void ParameterSet_Tensile_EdgeRadius::setup()
{
	assert(this->m_pModel != NULL);
	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	this->m_np = (int) this->m_pTSModel->getRodMeshModel(this->m_midx)->getSimMesh_0().getNumEdge();

	// Anisotropic?
	if (this->m_isAni)
		this->m_np *= 2;

	this->updateBoundsVector();
}