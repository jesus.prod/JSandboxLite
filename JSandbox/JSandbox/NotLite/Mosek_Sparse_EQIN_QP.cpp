///*=====================================================================================*/
///*!
//\file		Mosek_Sparse_EQIN_QP.cpp
//\author		jesusprod
//\brief		Implementation of Mosek_Sparse_EQIN_QP.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Optim/Mosek_Sparse_EQIN_QP.h>
//
//static void MSKAPI taskLogger(void *handle, MSKCONST char str[])
//{
//	printf("%s", str);
//}
//
//bool Mosek_Sparse_EQIN_QP::solve(const tVector& vQ,			 // The quadratic coeffients matrix
//								 const VectorXd& vc,		 // The linear coefficients vector
//								 const tVector& vA,			 // The constraints coefficients matrix
//								 const VectorXd& vb,		 // The constraints independent vector
//								 const iVector& vCT,		 // The type of constraints { -, +, 0}
//								 const VectorXd& vx,		 // The initial solution vector
//								 const VectorXd& vl,		 // The lower bounds: -Inf if not bounded
//								 const VectorXd& vu,		 // The uppder bounds: Inf if not bounded
//								 VectorXd& vxo,				 // The solution of the QP problem
//								 string& error)
//{
//	int Nx = (int) vc.size();
//	int Nc = (int) vb.size();
//
//	iVector vxIdx_MSK(Nx);
//	iVector vcIdx_MSK(Nc);
//	for (int i = 0; i < Nx; ++i) vxIdx_MSK[i] = i;
//	for (int i = 0; i < Nc; ++i) vcIdx_MSK[i] = i;
//
//	int NCoefQ = (int)vQ.size();
//	int NCoefA = (int)vA.size();
//
//	// Create environment
//
//	MSKenv_t envi = NULL;
//	MSKtask_t task = NULL;
//	 
//	MSKrescodee res;
//	res = MSK_makeenv(&envi, NULL);
//	res = MSK_maketask(envi, Nc, Nx, &task);
//	res = MSK_linkfunctotaskstream(task, MSK_STREAM_LOG, NULL, taskLogger);
//
//	if (res != MSK_RES_OK)
//	{
//		error = "Impossible to create environment";
//		return false;
//	}
//
//	res = MSK_appendvars(task, Nx);
//	res = MSK_appendcons(task, Nc);
//
//	// Objective linear term -----------------------------------------
//
//	res = MSK_putclist(task, Nx, vxIdx_MSK.data(), vc.data());
//
//	if (res != MSK_RES_OK)
//	{
//		error = "Impossible to set linear term";
//		return false;
//	}
//
//	// Objective quadratic term --------------------------------------
//
//	int NCoefQSim = 0;
//	iVector vQi_MSK; vQi_MSK.reserve(NCoefQ);
//	iVector vQj_MSK; vQj_MSK.reserve(NCoefQ);
//	dVector vQv_MSK; vQv_MSK.reserve(NCoefQ);
//	for (int c = 0; c < NCoefQ; ++c)
//	{
//		if (vQ[c].row() < vQ[c].col())
//			continue; // Ignore upper
//
//		vQi_MSK.push_back(vQ[c].row());
//		vQj_MSK.push_back(vQ[c].col());
//		vQv_MSK.push_back(vQ[c].value());
//
//		NCoefQSim++;
//	}
//
//	res = MSK_putqobj(task, NCoefQSim, vQi_MSK.data(), vQj_MSK.data(), vQv_MSK.data());
//
//	if (res != MSK_RES_OK)
//	{
//		error = "Impossible to set quadratic term";
//		return false;
//	}
//
//	// Variable box-constraints --------------------------------------
//
//	//vector<MSKboundkeye> vBT_MSK; vBT_MSK.reserve(Nx);
//	//dVector vl_MSK; vl_MSK.reserve(Nx);
//	//dVector vu_MSK; vu_MSK.reserve(Nx);
//	//iVector vBIdx; vBIdx.reserve(Nx);
//	//for (int j = 0; j < Nx; ++j)
//	//{
//	//	if (vl(j) == -HUGE_VAL && vu(j) == HUGE_VAL)
//	//		continue; // There is no box-contraints
//
//	//	if (vl(j) != -HUGE_VAL)
//	//		vl_MSK.push_back(vl(j));
//	//	else vl_MSK.push_back(-MSK_INFINITY);
//
//	//	if (vu(j) != HUGE_VAL)
//	//		vu_MSK.push_back(vu(j));
//	//	else vu_MSK.push_back(MSK_INFINITY);
//
//	//	if (isApprox(vl(j), vu(j), EPS_APPROX)) vBT_MSK.push_back(MSK_BK_FX);		// Fixed
//	//	else if (vl(j) == -HUGE_VAL) vBT_MSK.push_back(MSK_BK_UP);					// Upper bound
//	//	else if (vu(j) == +HUGE_VAL) vBT_MSK.push_back(MSK_BK_LO);					// Lower bound
//	//	else vBT_MSK.push_back(MSK_BK_RA);											// Ranged
//
//	//	vBIdx.push_back(j);
//	//}
//
//	//res = MSK_putvarboundlist(task, Nx, vBIdx.data(), vBT_MSK.data(), vl_MSK.data(), vu_MSK.data());
//
//	vector<MSKboundkeye> vBT_MSK(Nx);
//	dVector vl_MSK(Nx);
//	dVector vu_MSK(Nx);
//	for (int j = 0; j < Nx; ++j)
//	{
//		if (vl(j) == -HUGE_VAL)
//			vl_MSK[j] = -MSK_INFINITY;
//		else vl_MSK[j] = vl(j);
//
//		if (vu(j) == +HUGE_VAL)
//			vu_MSK[j] = +MSK_INFINITY;
//		else vu_MSK[j] = vu(j);
//
//		if (vl(j) == -HUGE_VAL && vu(j) == HUGE_VAL) vBT_MSK[j] = MSK_BK_FR;	// Free
//		else if (isApprox(vl(j), vu(j), EPS_APPROX)) vBT_MSK[j] = MSK_BK_FX;	// Fixed
//		else if (vl(j) == -HUGE_VAL) vBT_MSK[j] = MSK_BK_UP;					// Upper bound
//		else if (vu(j) == +HUGE_VAL) vBT_MSK[j] = MSK_BK_LO;					// Lower bound
//		else vBT_MSK[j] = MSK_BK_RA;											// Ranged
//	}
//
//	res = MSK_putvarboundlist(task, Nx, vxIdx_MSK.data(), vBT_MSK.data(), vl_MSK.data(), vu_MSK.data());
//
//	if (res != MSK_RES_OK)
//	{
//		error = "Impossible to set box-constraints";
//		return false;
//	}
//	
//	// Constraints linear term ---------------------------------------
//
//	iVector vAi_MSK(NCoefA);
//	iVector vAj_MSK(NCoefA);
//	dVector vAv_MSK(NCoefA);
//	for (int c = 0; c < NCoefA; ++c)
//	{
//		vAi_MSK[c] = vA[c].row();
//		vAj_MSK[c] = vA[c].col();
//		vAv_MSK[c] = vA[c].value();
//	}
//
//	res = MSK_putaijlist64(task, NCoefA, vAi_MSK.data(), vAj_MSK.data(), vAv_MSK.data());
//
//	SparseMatrixXd mA(Nc, Nx); // Debugging
//	mA.setFromTriplets(vA.begin(), vA.end());
//	MatrixXd mAD = mA.toDense();
//
//	for (int i = 0; i < Nc; ++i)
//		for (int j = 0; j < Nx; ++j)
//		{
//			double val;
//			MSK_getaij(task, i, j, &val);
//			if (!isApprox(val, mAD(i, j), EPS_APPROX))
//				logSimu("[ERROR] Discrepancy found (%d,%d): %.9f", i, j, abs(val - mAD(i, j)));
//		}
//
//	if (res != MSK_RES_OK)
//	{
//		error = "Impossible to set linear constraints";
//		return false;
//	}
//
//	// Constraints values --------------------------------------------
//
//	vector<MSKboundkeye> vCT_MSK(Nc);
//	dVector vbl_MSK(Nc);
//	dVector vbu_MSK(Nc);
//	for (int j = 0; j < Nc; ++j)
//	{
//		if (vCT[j] == -1)
//		{
//			vCT_MSK[j] = MSK_BK_UP;
//			vbu_MSK[j] = vb(j);
//		}
//		
//		if (vCT[j] == +1)
//		{
//			vCT_MSK[j] = MSK_BK_LO;
//			vbl_MSK[j] = vb(j);
//		}
//
//		if (vCT[j] == 0)
//		{
//			vCT_MSK[j] = MSK_BK_FX;
//			vbl_MSK[j] = vb(j);
//			vbu_MSK[j] = vb(j);
//		}
//	}
//
//	res = MSK_putconboundlist(task, Nc, vcIdx_MSK.data(), vCT_MSK.data(), vbl_MSK.data(), vbu_MSK.data());
//
//	if (res != MSK_RES_OK)
//	{
//		error = "Impossible to set constraints values";
//		return false;
//	}
//
//	// Compute solution --------------------------------------------
//
//	bool solved;
//
//	MSKrescodee trmcode;
//	
//	res = MSK_optimizetrm(task, &trmcode);
//
//	if (res != MSK_RES_OK)
//	{
//		error = "Impossible to optimize the problem";
//		return false;
//	}
//
//	MSK_solutionsummary(task, MSK_STREAM_MSG);
//
//	MSKsolstae solsta;
//	double* vxo_MSK = new double[Nx];
//	MSK_getsolsta(task, MSK_SOL_ITR, &solsta);
//
//	switch (solsta)
//	{
//	case MSK_SOL_STA_OPTIMAL: 
//	case MSK_SOL_STA_NEAR_OPTIMAL:
//		error = "[TRACE] Near Optimal solution found using Mosek";
//		MSK_getxx(task, MSK_SOL_ITR,vxo_MSK);
//		solved = true;
//		break;
//	case MSK_SOL_STA_DUAL_INFEAS_CER:
//	case MSK_SOL_STA_PRIM_INFEAS_CER:
//	case MSK_SOL_STA_NEAR_DUAL_INFEAS_CER:
//	case MSK_SOL_STA_NEAR_PRIM_INFEAS_CER:
//		error = "[ERROR] Primal or dual infeasibility certificate found";
//		solved = false;
//		break;
//	case MSK_SOL_STA_UNKNOWN:
//		error = "[ERROR] The status of the solution could not be determined";
//		solved = false;
//		break;
//	default:
//		error = "[ERROR] Other solution status";
//		solved = false;
//		break;
//	}
//
//	MSK_deletetask(&task);
//	MSK_deleteenv(&envi);
//
//	if (solved)
//	{
//		vxo.resize(Nx); // Solution
//		for (int i = 0; i < Nx; ++i)
//			vxo(i) = vxo_MSK[i]; 
//
//		VectorXd vbtest = mA*vxo;
//		double absError = (vb - vbtest).norm();
//		double relError = absError / vb.norm();
//		logSimu("[TRACE] Constraints absolute error %.9f", absError);
//		logSimu("[TRACE] Constraints relative error %.9f", relError);
//	}
//
//	delete[] vxo_MSK;
//
//	return solved;
//}