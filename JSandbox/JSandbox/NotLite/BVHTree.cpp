/*=====================================================================================*/
/*!
\file		BVHTree.cpp
\author		jesusprod,bthomasz
\brief		Implementation of BVHTree.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/BVHTree.h>
#include <JSandbox/Geometry/AABoundingBox.h>

BVHTree::BVHTree(const BVHTree& toCopy)
{
	this->m_ID = toCopy.m_ID;
	this->m_vx = toCopy.m_vx;
	this->m_vnodes = toCopy.m_vnodes;
	this->m_vprims = toCopy.m_vprims;

	int nvb = (int) toCopy.m_vBVs.size();

	this->m_vBVs.resize(nvb);
	for (int i = 0; i < nvb; ++i)
	{
		this->m_vBVs[i] = new AABoundingBox();
		*(this->m_vBVs[i]) = *(toCopy.m_vBVs[i]);
	}
}

BVHTree::~BVHTree(void)
{
	this->freeMemory();
}

void BVHTree::freeMemory()
{
	for (int i = 0; i < (int) this->m_vBVs.size(); ++i)
		delete this->m_vBVs[i]; // Free bounding boxes
	this->m_vBVs.clear();
}

void BVHTree::initializeCurve(const Curve& curve)
{
	freeMemory();

	// Load points and edges

	int ne = curve.getNumEdge();
	int nn = curve.getNumNode();

	this->m_vprims.resize(ne);
	for (int i = 0; i < ne; ++i)
	{
		int pv, nv;
		curve.getEdgeNeighborNodes(i, pv, nv);
		iVector vedge(2);
		vedge[0] = pv;
		vedge[1] = nv;
		this->m_vprims[i] = BVHPrimitive(i, 2, vedge);
	}

	this->m_vx.resize(3*nn);
	for (int i = 0; i < nn; ++i)
	{
		int offset = 3*i;
		const Vector3d& p = curve.getPosition(i);
		this->m_vx[offset + 0] = p(0);
		this->m_vx[offset + 1] = p(1);
		this->m_vx[offset + 2] = p(2);
	}

	// Recursively create structure

	this->m_vBVs.clear();
	this->m_vnodes.clear();

	this->m_vnodes.push_back(BVHNode(-1));
	AABoundingBox* aabb = new AABoundingBox();
	for (int i = 0; i < ne; ++i)
	{
		m_vnodes[0].addPrimitive(i); // Insert primitives
		this->insert(aabb, this->m_vprims[i], this->m_vx);
	}

	this->m_vBVs.push_back(aabb);
	splitRecursive(0, this->m_vx);
}

void BVHTree::initializeCloud(const vector<Vector3d>& vcloud)
{
	freeMemory();

	// Load points

	int np = (int)vcloud.size();

	this->m_vprims.resize(np);
	for (int i = 0; i < np; ++i)
	{
		iVector vpoint; 
		vpoint.push_back(i); // Single point primitive
		this->m_vprims[i] = BVHPrimitive(i, 1, vpoint);
	}

	this->m_vx.resize(3 * np);
	for (int i = 0; i < np; ++i)
	{
		int offset = 3 * i;
		const Vector3d& p = vcloud[i];
		this->m_vx[offset + 0] = p(0);
		this->m_vx[offset + 1] = p(1);
		this->m_vx[offset + 2] = p(2);
	}

	// Recursively create structure

	this->m_vBVs.clear();
	this->m_vnodes.clear();

	this->m_vnodes.push_back(BVHNode(-1));
	AABoundingBox* aabb = new AABoundingBox();
	for (int i = 0; i < np; ++i)
	{
		m_vnodes[0].addPrimitive(i); // Insert primitives
		this->insert(aabb, this->m_vprims[i], this->m_vx);
	}

	this->m_vBVs.push_back(aabb);
	splitRecursive(0, this->m_vx);
}

void BVHTree::initializeMeshEdge(const TriMesh* pMesh, const string& pname)
{
	freeMemory();

	// Load points and faces

	int ne = (int) pMesh->n_edges();

	this->m_vprims.resize(ne);
	for (int i = 0; i < ne; ++i)
	{
		TriMesh::EdgeHandle eh = pMesh->edge_handle(i);

		// Just ignore the deleted edges
		if (pMesh->status(eh).deleted())
			continue;

		vector<TriMesh::VertexHandle> vevh;
		pMesh->getEdgeVertices(eh, vevh);
		iVector vedge(2);
		vedge[0] = vevh[0].idx();
		vedge[1] = vevh[1].idx();

		this->m_vprims[i] = BVHPrimitive(eh.idx(), 2, vedge);
	}

	pMesh->getPoints(this->m_vx, pname);

	// Recursively create structure

	this->m_vBVs.clear();
	this->m_vnodes.clear();

	this->m_vnodes.push_back(BVHNode(-1));
	AABoundingBox* aabb = new AABoundingBox();
	for (int i = 0; i < ne; ++i)
	{
		m_vnodes[0].addPrimitive(i); // Insert primitives
		this->insert(aabb, this->m_vprims[i], this->m_vx);
	}

	this->m_vBVs.push_back(aabb);
	splitRecursive(0, this->m_vx);
}

void BVHTree::initializeMeshFace(const TriMesh* pMesh, const string& pname)
{
	freeMemory();

	// Load points and faces

	int nf = (int) pMesh->n_faces();
	
	iVector vidx;
	pMesh->getIndices(vidx);
	this->m_vprims.resize(nf);
	for (int i = 0; i < nf; ++i)
	{
		TriMesh::FaceHandle fh = pMesh->face_handle(i);

		// Just ignore the deleted faces
		if (pMesh->status(fh).deleted())
			continue;

		int offset = 3*i;
		iVector vface(3);
		vface[0] = vidx[offset + 0];
		vface[1] = vidx[offset + 1];
		vface[2] = vidx[offset + 2];

		this->m_vprims[i] = BVHPrimitive(fh.idx(), 3, vface);
	}

	pMesh->getPoints(this->m_vx, pname);

	// Recursively create structure
	
	this->m_vBVs.clear();
	this->m_vnodes.clear();
	
	this->m_vnodes.push_back(BVHNode(-1));
	AABoundingBox* aabb = new AABoundingBox();
	for (int i = 0; i < nf; ++i)
	{
		m_vnodes[0].addPrimitive(i); // Insert primitives
		this->insert(aabb, this->m_vprims[i], this->m_vx);
	}

	this->m_vBVs.push_back(aabb);
	splitRecursive(0, this->m_vx);
}

void BVHTree::update(const dVector& vpos, bool clear)
{
	assert((int)vpos.size() == (int) this->m_vx.size());

	this->m_vx = vpos; // Substitute current
	this->updateVolumesRecursive(0, this->m_vx, clear);
}

void BVHTree::update(const vector<BVHPrimitive>& vrem,
					 const vector<BVHPrimitive>& vadd)
{
	// TODO: Provisionally, just reinitialize the whole thing
}

void BVHTree::updateVolumesRecursive(int idx, const dVector& vpos, bool clear)
{
	const BVHNode& node = m_vnodes[idx];
	BoundingVolume* bv = m_vBVs[idx];
	if (clear)
		bv->clear();
	if (node.isLeaf())
	{
		AABoundingBox* aabb = dynamic_cast<AABoundingBox*>(bv);
		insert(aabb, m_vprims[node.getPrimitives()[0]], vpos);
	}
	else
	{
		for (int i = 0; i<node.getNumChildren(); i++)
		{
			int idxChild = node.getChildren()[i];
			updateVolumesRecursive(idxChild, vpos, clear);
			bv->merge(this->m_vBVs[idxChild]); // Merge BV
		}
	}
}

void BVHTree::insert(BoundingVolume* pBV, const BVHPrimitive& prim, const dVector& vpos)
{
	assert(pBV);
	for (int i = 0; i < prim.m_npoints; ++i)
		pBV->insert(get3D(prim.m_vpidx[i], vpos));
}

int BVHTree::splitNode(int idx, const dVector& vpos)
{
	BoundingVolume* bv = this->m_vBVs[idx];

	const BVHNode& node = this->m_vnodes[idx];
	const iVector& vnodePrims = node.getPrimitives();
	int np = (int) vnodePrims.size();
	vector<Vector3d> vcenters(np);
	for (int i = 0; i < np; ++i)
	{
		BVHPrimitive prim = this->m_vprims[vnodePrims[i]];

		vcenters[i] = Vector3d(0.0, 0.0, 0.0);
		for (int j = 0; j < prim.m_npoints; ++j) // Center
			vcenters[i] +=  get3D(prim.m_vpidx[j], vpos);
		vcenters[i] /= prim.m_npoints;
	}

	int idim = bv->getLongestDimension();
	vector<pair<double, int>> vprojCenters;
	for (int i = 0; i < np; i++)
		vprojCenters.push_back(make_pair(vcenters[i](idim), vnodePrims[i]));
	sort(vprojCenters.begin(), vprojCenters.end()); // Sort to compute median

	// Split children faces by median

	iVector vchildPrims[2];
	for (int i = 0; i < np / 2; ++i) vchildPrims[0].push_back(vprojCenters[i].second);
	for (int i = np / 2; i < np; ++i) vchildPrims[1].push_back(vprojCenters[i].second);
	assert((vchildPrims[0].size() + vchildPrims[1].size()) == np);

	int nchildren = 0;
	for (int j = 0; j < 2; j++)
	{
		if (vchildPrims[j].size() == 0)
			continue; // No primitives
		nchildren++;

		BoundingVolume* cbv = new AABoundingBox();
		for (int i = 0; i < (int) vchildPrims[j].size(); ++i)
			insert(cbv, this->m_vprims[vchildPrims[j][i]], vpos);

		m_vBVs.push_back(cbv);
		m_vnodes.push_back(BVHNode(idx));
		m_vnodes.back().setPrimitives(vchildPrims[j]);
		m_vnodes[idx].addChild((int) m_vnodes.size() - 1);
	}
	return nchildren;
}

void BVHTree::splitRecursive(int idx, const dVector& vpos)
{
	BVHNode& parent = this->m_vnodes[idx];
	if (parent.isLeaf()) // Do not split!
		return; 

	int nc = splitNode(idx, vpos);
	iVector vchildren = this->m_vnodes[idx].getChildren();
	for (int i = 0; i < nc; i++)
	{
		BVHNode& child = this->m_vnodes[vchildren[i]];
		if (child.isLeaf())
			continue;

		splitRecursive(vchildren[i], vpos);
	}
}

bool BVHTree::testNodes(const BVHTree* bvh1, const BVHTree* bvh2, int i1, int i2, vector<BVHClosePrimitivePair>& cpps, double tol)
{
	const BVHNode& node1 = bvh1->getNode(i1); //m_nodes[i1];
	const BVHNode& node2 = bvh2->getNode(i2); //m_nodes[i2];
	const BoundingVolume* bv1 = bvh1->getBV(i1);
	const BoundingVolume* bv2 = bvh2->getBV(i2);

	bool ovl = bv1->overlaps(bv2, tol); // Overlapping?
	if (ovl && node1.isLeaf() && node2.isLeaf())
	{
		cpps.push_back(BVHClosePrimitivePair(bvh1->getID(), 
											 bvh2->getID(),
											 bvh1->getPrimitive(node1.getPrimitives()[0]),
											 bvh2->getPrimitive(node2.getPrimitives()[0])));
	}

	return ovl;
}

bool BVHTree::testRecursive(const BVHTree* bvh1, const BVHTree* bvh2, int i1, int i2, vector<BVHClosePrimitivePair>& cpps, double tol)
{
	bool ovl = testNodes(bvh1, bvh2, i1, i2, cpps, tol);
	if (!ovl) // Do not overlap? Stop recursion
		return false;

	const BVHNode& node1 = bvh1->getNode(i1); //m_nodes[i1];
	const BVHNode& node2 = bvh2->getNode(i2); //m_nodes[i2];
	const BoundingVolume* bv1 = bvh1->getBV(i1);
	const BoundingVolume* bv2 = bvh2->getBV(i2);

	if (node1.isLeaf() && node2.isLeaf())
		return true;

	//continue recursion
	double l1 = 0, l2 = 0;
	bv1->getLongestDimension(&l1);
	bv2->getLongestDimension(&l2);
	if ((!node1.isLeaf() && (l1 > l2)) || node2.isLeaf())
	{
		const iVector& children1 = node1.getChildren();
		for (int i = 0; i < children1.size(); i++) // Same i2
			testRecursive(bvh1, bvh2, children1[i], i2, cpps, tol);
	}
	else 
	{
		const iVector& children2 = node2.getChildren();
		for (int i = 0; i < children2.size(); i++) // same i1
			testRecursive(bvh1, bvh2, i1, children2[i], cpps, tol);
	}

	return true;
}

// -- Not yet implemented ------------------------------------------------------

//void BVHTree::getTriangleVertexIndeces(int itri, std::vector<int>& inds) const
//{
//	assert(itri<m_triangles.size());
//	assert(inds.size()==3);
//	for(int i=0; i<3; i++)
//		inds[i] = m_triangles[itri].inds[i];
//}
//
////void BVHTree::updateHiearchy(const Phys3DTriMesh* mesh)
//void BVHTree::updateHierarchy(const dVector& pos, bool clear)
//{
//	//const P3DIndexArray* pIB = mesh->GetIndexArray();
//	//int nTriangles = pIB->GetNumIdx()/3;	
//	//const P3DVertexArray* pVB = mesh->GetVertexArray();
//
//	//int nVerts = pVB->GetNumVtx();
//	assert(pos.size()==m_pos.size());
//	m_pos = pos;
//	//for(int i=0; i<nVerts; i++)
//	//{
//	//	const Vector3 pi = pVB->GetVtxPos(i);
//	//	cdutil::set3D(i,get3D(V3D(pi[0],pi[1],pi[2]),m_pos);
//	//}
//	updateBoundingVolumesRecursive(0,m_pos, clear);
//	if(normalCones())
//	{
//		dVector normals(3*m_triangles.size());
//		computeNormals(m_pos,normals);
//		updateNormalConesRecursive(0,normals);
//	}
//}
//
//void BVHTree::splitRecursive(int iNode, const dVector& pos)
//{
//	//BVHNode& parent = m_nodes[iNode];
//	if(m_nodes[iNode].isLeaf())
//		return;
//	int nChildren = splitNode(iNode,pos);
//	for(int i=0; i<nChildren;i++)
//	{
//		int iChild = m_nodes[iNode].getChildren()[i];
//		BVHNode& childi = m_nodes[iChild];
//		if(childi.isLeaf())
//			continue;
//		splitRecursive(iChild,pos);
//	}
//}
//
//int BVHTree::splitNode(int iNode, const dVector& pos)
//{
//	BoundingVolume* bv = m_boundingVolumes[iNode];
//	//BVHNode& node = m_nodes[iNode];
//	const std::vector<int>& faces = m_nodes[iNode].faces();
//	dVector vcenters(3*faces.size());
//
//	int idim = bv->getLongestDimension();
//	computeBaryCenters(faces,pos,vcenters);
//	//could also use median here to obtain more balanced tree
//	double mean = 0;
//	std::vector<std::pair<double,int> > projectedCenters;
//	for(int i=0; i<faces.size();i++)
//	{
//		projectedCenters.push_back(std::make_pair(vcenters[3*i+idim],faces[i]));
//		mean += vcenters[3*i+idim];
//	}
//	mean /= (double)faces.size();
//	std::sort(projectedCenters.begin(),projectedCenters.end());
//
//	std::vector<int> inds[2];
//	//for(int i=0; i<faces.size();i++)
//	//	if(vcenters[3*i+idim]<mean)
//	//		inds[0].push_back(faces[i]);
//	//	else
//	//		inds[1].push_back(faces[i]);
//
//	//distribute faces evenly to among children
//	for(int i=0; i<faces.size()/2;i++)
//		inds[0].push_back(projectedCenters[i].second);
//	for(int i=faces.size()/2; i<faces.size();i++)
//		inds[1].push_back(projectedCenters[i].second);
//	assert((inds[0].size()+inds[1].size())==faces.size());
//
//	int nchildren = 0;
//	for(int j =0; j<2; j++)
//	{
//		if(inds[j].size()==0)
//			continue;
//		nchildren++;
//		BoundingVolume* cbv = new AABoundingBox;
//		for(int i=0; i<inds[j].size();i++)
//			insertFaceIntoBV(cbv,m_triangles[inds[j][i]],pos);
//		m_nodes.push_back(BVHNode(iNode));
//		m_nodes.back().setFaces(inds[j]);
//		m_boundingVolumes.push_back(cbv);
//		m_nodes[iNode].addChild(m_nodes.size()-1);
//	}
//	return nchildren;
//}
//
//void BVHTree::computeBaryCenters(const std::vector<int>& faces, const dVector& pos, dVector& vcenters)
//{
//	assert(vcenters.size()==3*faces.size());
//	for(int i=0;i<faces.size();i++)
//	{
//		assert(faces[i]<m_triangles.size());
//		const BVHTriangle& tri = m_triangles[faces[i]];
//		V3D vbary;
//		for(int j=0;j<3;j++)
//			vbary += cdutil::get3D(tri.inds[j],pos);
//		vbary *= 1.0/3.0;
//		cdutil::set3D(i,vbary,vcenters);
//	}
//
//
//}
//
//void BVHTree::insertFaceIntoBV(BoundingVolume* cbv, const BVHTriangle& face, const dVector& pos)
//{
//	for(int i=0; i<3; i++)
//	{
//		V3D pi = cdutil::get3D(face.inds[i],pos);
//		cbv->insert(pi);
//	}
//}
//
//void BVHTree::getSelfCollisions(BVHCloseFacePairs& cfps)
//{
//	if(normalCones())
//	{
//		dVector normals(3*m_triangles.size());
//		computeNormals(m_pos,normals);
//		updateNormalConesRecursive(0,normals);
//	}
//	testSelfRecursive(0,cfps);
//}
//
//bool BVHTree::testNodes_self(int i1, int i2, BVHCloseFacePairs& cfps)
//{
//	const BVHNode& node1 = m_nodes[i1];
//	const BVHNode& node2 = m_nodes[i2];
//	bool ovl = m_boundingVolumes[i1]->overlaps(m_boundingVolumes[i2]);
//	if(ovl&&node1.isLeaf()&&node2.isLeaf())
//	{
//		int if1 = node1.faces()[0];
//		int if2 = node2.faces()[0];
//		if(!facesAdjacent(if1,if2))	
//		{
//			BVHCloseFacePair cfp(m_objID, m_objID, if1,if2);
//			//for(int i=0;i<3;i++)
//			//{
//			//	cfp.vertices[0][i] = m_triangles[3*if1+i];
//			//	cfp.vertices[1][i] = m_triangles[3*if2+i];
//			//}
//			cfps.push_back(cfp);
//		}
//		else
//			;
//	}
//	return ovl;
//}
//
//void BVHTree::getExternalCollisions(const BVHTree* bvh1, const BVHTree* bvh2,BVHCloseFacePairs& cfps)
//{
//
//	testNodesRecursive(bvh1,bvh2,0,0,cfps);
//	//if(distanceCulling())
//	//	cullCloseFacePairs(m_pos,m_pos,cfps);
//}
//
//
//bool BVHTree::testNodes(const BVHTree* bvh1, const BVHTree* bvh2, int i1, int i2, BVHCloseFacePairs& cfps)
//{
//	const BVHNode& node1 = bvh1->getNode(i1); //m_nodes[i1];
//	const BVHNode& node2 = bvh2->getNode(i2); //m_nodes[i2];
//	const BoundingVolume* bv1 = bvh1->getBoundingVolume(i1);
//	const BoundingVolume* bv2 = bvh2->getBoundingVolume(i2);
//	//bool ovl = m_boundingVolumes[i1]->overlaps(m_boundingVolumes[i2]);
//	bool ovl = bv1->overlaps(bv2);
//	if(ovl&&node1.isLeaf()&&node2.isLeaf())
//	{
//		int if1 = node1.faces()[0];
//		int if2 = node2.faces()[0];
//		int objID1 = bvh1->objectID();
//		int objID2 = bvh2->objectID();
//		//if(!facesAdjacent(if1,if2))
//			//BVHCloseFacePair cfp(m_objID, m_objID, if1,if2);
//			//std::vector<int> inds1(3),inds2(3);
//			//bvh1.getTriangleVertexIndeces(if1,inds1);
//			//bvh2.getTriangleVertexIndeces(if1,inds2);
//			//for(int i=0;i<3;i++)
//			//{
//			//	cfp.vertices[0][i] = inds1[i];
//			//	cfp.vertices[1][i] = inds2[i];
//			//}
//			//cfps.push_back(cfp);
//
//		cfps.push_back(BVHCloseFacePair(objID1, objID2, if1,if2));
//	}
//	return ovl;
//}
//
//bool BVHTree::testNodesRecursive(const BVHTree* bvh1,  const BVHTree* bvh2, int i1, int i2, BVHCloseFacePairs& cfps)
//{
//	bool ovl = testNodes(bvh1,bvh2,i1,i2,cfps);
//	if(!ovl)
//		return false;
//
//	const BVHNode& node1 = bvh1->getNode(i1); //m_nodes[i1];
//	const BVHNode& node2 = bvh2->getNode(i2); //m_nodes[i2];
//	const BoundingVolume* bv1 = bvh1->getBoundingVolume(i1);
//	const BoundingVolume* bv2 = bvh2->getBoundingVolume(i2);
//	//bool ovl = m_boundingVolumes[i1]->overlaps(m_boundingVolumes[i2]);
//
//
//	if(node1.isLeaf()&&node2.isLeaf())
//		return true;
//
//	//continue recursion
//	double l1 =0, l2 =0;
//	bv1->getLongestDimension(&l1);
//	bv2->getLongestDimension(&l2);
//	if((!node1.isLeaf()&&(l1>l2))||node2.isLeaf())
//	{
//		const std::vector<int>& children = 	node1.getChildren();
//		for(int i=0; i<children.size();i++)
//			testNodesRecursive(bvh1,bvh2,children[i],i2,cfps);
//	}
//	else {
//	const std::vector<int>& children = 	node2.getChildren();
//		for(int i=0; i<children.size();i++)
//			testNodesRecursive(bvh1,bvh2,i1,children[i],cfps);
//	}
//	return true;
//}
//
//bool BVHTree::testNodesRecursive_self(int i1, int i2, BVHCloseFacePairs& cfps)
//{
//	bool ovl = testNodes_self(i1,i2,cfps);
//	if(!ovl)
//		return false;
//
//	if(m_nodes[i1].isLeaf()&&m_nodes[i2].isLeaf())
//		return true;
//
//	//continue recursion
//	BoundingVolume* bv1 = m_boundingVolumes[i1];
//	BoundingVolume* bv2 = m_boundingVolumes[i2];
//	double l1 =0, l2 =0;
//	bv1->getLongestDimension(&l1);
//	bv2->getLongestDimension(&l2);
//	if((!m_nodes[i1].isLeaf()&&(l1>l2))||m_nodes[i2].isLeaf())
//	{
//		const std::vector<int>& children = 	m_nodes[i1].getChildren();
//		for(int i=0; i<children.size();i++)
//			testNodesRecursive_self(children[i],i2,cfps);
//	}
//	else {
//	const std::vector<int>& children = 	m_nodes[i2].getChildren();
//		for(int i=0; i<children.size();i++)
//			testNodesRecursive_self(children[i],i1,cfps);
//	}
//
//	return true;
//
//}
//
//
//
//bool BVHTree::testSelfRecursive(int iNode, BVHCloseFacePairs& cfps)
//{
//	//if(m_nodes[iNode].faces().size()<3)
//	//	return false;
//	if(m_nodes[iNode].faces().size()==2)
//	{
//		int if1 = m_nodes[iNode].faces()[0];
//		int if2 = m_nodes[iNode].faces()[1];
//		if(!facesAdjacent(if1,if2))	
//			cfps.push_back(BVHCloseFacePair(m_objID, m_objID, if1,if2));
//		return true;
//	
//	}
//	const std::vector<int>& children = 	m_nodes[iNode].getChildren();
//	for(int i=0; i<children.size();i++)
//		for(int j=i+1; j<children.size();j++)
//			testNodesRecursive_self(children[i],children[j],cfps);
//
//	if(normalCones())
//	{
//		for(int i=0; i<children.size();i++)
//		{
//			if(m_normalCones[children[i]]->getAngle()>m_cullAngle)
//				testSelfRecursive(children[i],cfps);
//		}
//	}
//	else
//	{
//		for(int i=0; i<children.size();i++)
//		testSelfRecursive(children[i],cfps);
//	}
//
//	return true;
//
//}
//
//bool BVHTree::facesAdjacent(int if1, int if2)
//{
//	for(int i=0;i<3;i++)
//		for(int j=0;j<3;j++)
//			if(m_triangles[if1].inds[i]==m_triangles[if2].inds[j])
//				return true;
//	return false;
//}
//
//void BVHTree::updateNormalCones(const dVector& normals)
//{
//	assert(m_normalCones.size()==m_nodes.size());
//	assert(normals.size()==3*m_triangles.size());
//
//	updateNormalConesRecursive(0,normals);
//}
//
//void BVHTree::updateNormalConesRecursive(int iNode, const dVector& normals)
//{
//	if(m_nodes[iNode].isLeaf())
//	{
//		V3D n = cdutil::get3D(m_nodes[iNode].faces()[0], normals);
//		m_normalCones[iNode]->reset(n,0.0);
//	}
//	else
//	{
//		m_normalCones[iNode]->clear();
//		for(int i=0; i<m_nodes[iNode].getNumChildren();i++)
//		{
//			int ichild =m_nodes[iNode].getChildren()[i];
//			updateNormalConesRecursive(ichild,normals);
//			m_normalCones[iNode]->add(*m_normalCones[ichild]);
//		}
//	}
//}
//
//void BVHTree::computeNormals(const dVector& pos, dVector& normals) const
//{
//	for(int i=0; i<m_triangles.size();i++)
//	{
//		V3D p[3];
//		for(int j =0;j<3; j++)
//			p[j] =  cdutil::get3D(m_triangles[i].inds[j],pos);
//		V3D n = (p[1]-p[0]).cross(p[2]-p[0]);
//		n.normalize();
//		cdutil::set3D(i,n,normals);
//	}
//
//}
//
//
//void BVHTree::updateBoundingVolumesRecursive(int iNode, const dVector& pos, bool clear)
//{
//	const BVHNode& node = m_nodes[iNode];
//	BoundingVolume* bv = m_boundingVolumes[iNode];
//	if(clear)
//		bv->clear();
//	if(node.isLeaf())
//	{
//		AABoundingBox* aabb = dynamic_cast<AABoundingBox*>(bv);	
//		insertFaceIntoBV(aabb,m_triangles[node.faces()[0]],pos);
//	}
//	else
//	{
//		for(int i=0; i<node.getNumChildren(); i++)
//		{
//			int ichild = node.getChildren()[i];
//			updateBoundingVolumesRecursive(ichild,pos,clear);
//			bv->merge(m_boundingVolumes[ichild]);
//		}
//	}
//}
//
//
//
