/*=====================================================================================*/
/*!
\file		Curve2DEdgeElement.cpp
\author		jesusprod
\brief		Implementation of Curve2DEdgeElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/Curve2DEdgeElement.h>

Curve2DEdgeElement::Curve2DEdgeElement(int dim0) : NodalSolidElement(2, 2, dim0)
{
	// This carries mass
	this->m_addMass = true;

	this->m_pMatComp = NULL;
	this->m_pMatExte = NULL;

	this->m_dim0 = dim0;
}

Curve2DEdgeElement::~Curve2DEdgeElement()
{
	// Nothing to do here
}

void Curve2DEdgeElement::update_Rest(const VectorXd& vX)
{
	if (this->m_dim0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
		Vector2d x01 = x1 - x0;
		this->m_L0 = x01.norm();
	}
	if (this->m_dim0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
		Vector3d x01 = x1 - x0;
		this->m_L0 = x01.norm();
	}
}

void Curve2DEdgeElement::update_Mass(const VectorXd& vX)
{
	if (!this->m_addMass)
		return; // Ignore

	Real nodeMass = (1.0 / 2.0)*this->m_L0*this->m_vpmat[0]->getDensity();
	for (int i = 0; i < this->m_numDOFx; ++i)
		this->m_vmx[i] = nodeMass;
}

void Curve2DEdgeElement::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[2 + i]);
	}

	double L = this->m_L0;

	double kS = -1;

	if (this->m_pMatComp != NULL && this->m_pMatExte != NULL)
	{
		Vector2d x1e(x1[0], x1[1]);
		Vector2d x2e(x2[0], x2[1]);
		double Lx = (x1e - x2e).norm();

		if (Lx <= this->m_L0) // Compression
			kS = this->m_pMatComp->getStretchK();

		if (Lx > this->m_L0) // Extension
			kS = this->m_pMatExte->getStretchK();
	}
	else kS = m_vpmat[0]->getStretchK();

#include "../../Maple/Code/Curve2DEdgeEnergy.mcg"

	this->m_energy = t17;
}

void Curve2DEdgeElement::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[2 + i]);
	}

	double L = this->m_L0;

	double kS = -1;

	if (this->m_pMatComp != NULL && this->m_pMatExte != NULL)
	{
		Vector2d x1e(x1[0], x1[1]);
		Vector2d x2e(x2[0], x2[1]);
		double Lx = (x1e - x2e).norm();

		if (Lx <= this->m_L0) // Compression
			kS = this->m_pMatComp->getStretchK();

		if (Lx > this->m_L0) // Extension
			kS = this->m_pMatExte->getStretchK();
	}
	else kS = m_vpmat[0]->getStretchK();

	double vfx[4];

#include "../../Maple/Code/Curve2DEdgeForce.mcg"

	for (int i = 0; i < 4; ++i)
		if (!isfinite(vfx[i]))
			logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 4; ++i)
		this->m_vfVal(i) = vfx[i];
}

void Curve2DEdgeElement::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[2 + i]);
	}

	double L = this->m_L0;

	double kS = -1;

	if (this->m_pMatComp != NULL && this->m_pMatExte != NULL)
	{
		Vector2d x1e(x1[0], x1[1]);
		Vector2d x2e(x2[0], x2[1]);
		double Lx = (x1e - x2e).norm();

		if (Lx <= this->m_L0) // Compression
			kS = this->m_pMatComp->getStretchK();

		if (Lx > this->m_L0) // Extension
			kS = this->m_pMatExte->getStretchK();
	}
	else kS = m_vpmat[0]->getStretchK();

	double mJx[4][4];

#include "../../Maple/Code/Curve2DEdgeJacobian.mcg"

	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}