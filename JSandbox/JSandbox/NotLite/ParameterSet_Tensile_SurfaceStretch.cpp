/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_SurfaceStretch.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_SurfaceStretch.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_SurfaceStretch.h>
#include <JSandbox/Solver/BConditionPreStrainTensileS.h>
#include <JSandbox/Solver/PhysSolver.h>

ParameterSet_Tensile_SurfaceStretch::ParameterSet_Tensile_SurfaceStretch(int midx) : ParameterSet("TENSURFACESTRETCH")
{
	this->m_midx = midx;
}

ParameterSet_Tensile_SurfaceStretch::~ParameterSet_Tensile_SurfaceStretch()
{
	// Nothing to do here
}

void ParameterSet_Tensile_SurfaceStretch::getParameters(VectorXd& vp) const
{
	this->m_pSurfModel->getParam_Stretch(vp);

	assert((int)vp.size() == this->m_np);
}

void ParameterSet_Tensile_SurfaceStretch::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	BConditionPreStrainTensileS* pBC = static_cast<BConditionPreStrainTensileS*>(this->m_pParam->getSolver()->getBCondition(BCIndex::BCStretch));
	this->m_pSurfModel->setParam_Stretch(vp);
	pBC->setFactor(vp(0));
}

bool ParameterSet_Tensile_SurfaceStretch::isReady_fp() const
{
	throw new exception("Not implemented yet...");
}

bool ParameterSet_Tensile_SurfaceStretch::isReady_DfDp() const
{
	return this->m_pSurfModel->isReady_DfxDs();
}

void ParameterSet_Tensile_SurfaceStretch::get_fp(VectorXd& vfp) const
{
	throw new exception("Not implemented yet...");
}

void ParameterSet_Tensile_SurfaceStretch::get_DfDp(tVector& vDFDp) const
{
	int modelIdx = this->m_pTSModel->getSurfaceModelOffset(this->m_midx);

	tVector vDFDs;

	vDFDs.reserve(this->m_pSurfModel->getNumNonZeros_DfxDs());
	vDFDp.reserve(this->m_pSurfModel->getNumNonZeros_DfxDs());

	this->m_pSurfModel->add_DfxDs(vDFDs);

	this->m_pTSModel->addLocal2Global_Row(modelIdx, vDFDs, vDFDp);
}

void ParameterSet_Tensile_SurfaceStretch::setup()
{
	assert(this->m_pModel != NULL);

	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);

	assert(this->m_pTSModel != NULL);

	this->m_pSurfModel = this->m_pTSModel->getSurfaceModel(this->m_midx);

	this->m_np = 1; // Global

	this->updateBoundsVector();
}