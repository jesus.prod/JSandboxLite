/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_HermiteRodMeshRest.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_HermiteRodMeshRest.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_HermiteRodMeshRest.h>

ParameterSet_Tensile_HermiteRodMeshRest::ParameterSet_Tensile_HermiteRodMeshRest(int midx) : ParameterSet("TENRODMESHREST")
{
	assert(midx >= 0);
	this->m_midx = midx;
	this->m_is2D = false;
	this->m_tolerance = 0;
	this->m_controlNum = 4;
}

ParameterSet_Tensile_HermiteRodMeshRest::~ParameterSet_Tensile_HermiteRodMeshRest()
{
	// Nothing to do here
}

void ParameterSet_Tensile_HermiteRodMeshRest::concatenatedToRods(const VectorXd& vpAll, vector<VectorXd>& vpRod) const
{
	int offsetRod = 0;

	int nr = (int)vpRod.size();
	for (int i = 0; i < nr; ++i)
	{
		int nc = (int)vpRod[i].size()/3;

		for (int j = 0; j < nc; ++j)
		{
			if (this->m_is2D)
			{
				int offsetSplit = 3*j;
				int offsetWhole = 2*this->m_vmapSplit2Whole[offsetRod + j];
				vpRod[i](offsetSplit + 0) = vpAll(offsetWhole + 0);
				vpRod[i](offsetSplit + 2) = vpAll(offsetWhole + 1);
				vpRod[i](offsetSplit + 1) = 0.0;
			}
			else
			{
				int offsetSplit = 3 * j;
				int offsetWhole = 3 * this->m_vmapSplit2Whole[offsetRod + j];
				vpRod[i](offsetSplit + 0) = vpAll(offsetWhole + 0);
				vpRod[i](offsetSplit + 1) = vpAll(offsetWhole + 1);
				vpRod[i](offsetSplit + 2) = vpAll(offsetWhole + 2);
			}
		}
			
		offsetRod += nc;
	}
}

void ParameterSet_Tensile_HermiteRodMeshRest::rodsToConcatenated(const vector<VectorXd>& vpRod, VectorXd& vpAll) const
{
	int offsetRod = 0;

	int nr = (int)vpRod.size();
	for (int i = 0; i < nr; ++i)
	{
		int nc = (int)vpRod[i].size()/3;

		for (int j = 0; j < nc; ++j)
		{
			if (this->m_is2D)
			{
				int offsetSplit = 3 * j;
				int offsetWhole = 2 * this->m_vmapSplit2Whole[offsetRod + j];
				vpAll(offsetWhole + 0) = vpRod[i](offsetSplit + 0);
				vpAll(offsetWhole + 1) = vpRod[i](offsetSplit + 2);
			}
			else
			{
				int offsetSplit = 3 * j;
				int offsetWhole = 3 * this->m_vmapSplit2Whole[offsetRod + j];
				vpAll(offsetWhole + 0) = vpRod[i](offsetSplit + 0);
				vpAll(offsetWhole + 1) = vpRod[i](offsetSplit + 1);
				vpAll(offsetWhole + 2) = vpRod[i](offsetSplit + 2);
			}
		}

		offsetRod += nc;
	}
}

void ParameterSet_Tensile_HermiteRodMeshRest::getParameters(VectorXd& vp) const
{
	vp.resize(this->m_np);

	this->rodsToConcatenated(this->m_vcontrolVal, vp);
}

void ParameterSet_Tensile_HermiteRodMeshRest::setParameters(const VectorXd& vp)
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	assert((int)vp.size() == this->m_np);

	VectorXd vpB = vp;
	this->limitParameters(vpB);
	this->concatenatedToRods(vpB, this->m_vcontrolVal);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int nv = rodMesh.getNumNode();
	int nr = rodMesh.getNumRod();

	int offsetRod = 0;
	VectorXd vpInt(3*nv);
	for (int r = 0; r < nr; ++r)
	{
		int npInt = 3*pModel->getSimRod_0(r).getNumNode();

		VectorXd vpIntRod(npInt);
		dVector vsInt = pModel->getSimRod_0(r).getCurve().getVertex01Parametrization();
		this->computeValRod(this->m_vcontrolPar[r], this->m_vcontrolVal[r], vsInt, vpIntRod);

		// Assemble interpolated
		for (int j = 0; j < npInt; ++j)
			vpInt(offsetRod + j) = vpIntRod(j);

		offsetRod += npInt;
	}

	pModel->setParam_VertexRest(vpInt);
}

bool ParameterSet_Tensile_HermiteRodMeshRest::isReady_fp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_f0v();
}

bool ParameterSet_Tensile_HermiteRodMeshRest::isReady_DfDp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_DfxD0v();
}

void ParameterSet_Tensile_HermiteRodMeshRest::get_fp(VectorXd& vfv0) const
{
	throw new exception("Not implemented yet...");

	// TODO

	//this->m_pTSModel->getRodMeshModel(this->m_midx)->add_fv0(vfp);
}

void ParameterSet_Tensile_HermiteRodMeshRest::get_DfDp(tVector& vDfDp_t) const
{
	//this->testHermiteJacobian();

	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);
	int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);

	SparseMatrixXd mDiDc;
	this->computeJac(mDiDc);
	int ni = mDiDc.rows();
	int nc = mDiDc.cols();

	tVector vDfDp_int_split; // With respect to interpolated value 
	tVector vDfDp_con_split; // With respect to the control points
	SparseMatrixXd mDfDp_int_split(pModel->getNumSimDOF_x(), ni);
	SparseMatrixXd mDfDp_con_split(pModel->getNumSimDOF_x(), nc);
	vDfDp_int_split.reserve(pModel->getNumNonZeros_DfxD0v());

	// Add derivative (interpolated)
	pModel->add_DfxD0v(vDfDp_int_split);

	mDfDp_int_split.setFromTriplets(vDfDp_int_split.begin(), vDfDp_int_split.end());

	// Compute derivative (control)
	mDfDp_con_split = mDfDp_int_split * mDiDc;
	toTriplets(mDfDp_con_split, vDfDp_con_split);
	int nCoef = (int) vDfDp_con_split.size();

	tVector vDfDp_con_whole; 

	// At most same element number
	vDfDp_con_whole.reserve(nCoef);

	if (!this->m_is2D)
	{
		for (int i = 0; i < nCoef; ++i)
		{
			const Triplet<Real>& t = vDfDp_con_split[i];

			int colSpl = t.col();
			int col = 3 * this->m_vmapSplit2Whole[colSpl/3] + (colSpl % 3);
			vDfDp_con_whole.push_back(Triplet<Real>(t.row(), col, t.value()));
		}

		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_con_whole, vDfDp_t);
	}
	else
	{
		for (int i = 0; i < nCoef; ++i)
		{
			const Triplet<Real>& t = vDfDp_con_split[i];
			int colSpl = t.col();
			int mod = colSpl % 3;
			int div = this->m_vmapSplit2Whole[colSpl / 3];
			if (mod == 0) vDfDp_con_whole.push_back(Triplet<Real>(t.row(), 2 * div + 0, t.value()));
			if (mod == 2) vDfDp_con_whole.push_back(Triplet<Real>(t.row(), 2 * div + 1, t.value()));
		}

		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_con_whole, vDfDp_t);
	}
}

void ParameterSet_Tensile_HermiteRodMeshRest::setup()
{
	assert(this->m_pModel != NULL);

	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	int nr = pModel->getNumRod();
	this->m_vcontrolPar.resize(nr);
	this->m_vcontrolVal.resize(nr);

	int npSplit = 0;

	if (this->m_controlNum != -1)
	{
		// Use a specific number of control ponints

		for (int r = 0; r < nr; ++r)
		{
			this->m_vcontrolVal[r].resize(3*this->m_controlNum);
			this->m_vcontrolPar[r].resize(this->m_controlNum);
			npSplit += this->m_controlNum;
		}
	}
	else
	{
		// Use control points depending on tolerance

		for (int r = 0; r < nr; ++r)
		{
			const Curve& curve = pModel->getSimRod_0(r).getCurve();
			int numPoints = ceil(curve.getLength() / m_tolerance);
			this->m_vcontrolVal[r].resize(3*numPoints);
			this->m_vcontrolPar[r].resize(numPoints);
			npSplit += numPoints;
		}
	}

	this->updateControlPoints();

	// Count number of independent 

	int npWhole = 0;
	for (int i = 0; i < npSplit; ++i)
		if (this->m_vmapSplit2Whole[i] > npWhole)
			npWhole = this->m_vmapSplit2Whole[i]; 
	npWhole++;

	if (this->m_is2D)
		this->m_np = 2*npWhole;
	else this->m_np = 3*npWhole;

	this->updateBoundsVector();
}


void ParameterSet_Tensile_HermiteRodMeshRest::computeJac(SparseMatrixXd& mDiDc) const
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int nr = rodMesh.getNumRod();

	int offsetCon = 0;
	int offsetInt = 0;

	tVector vDiDc;

	for (int r = 0; r < nr; ++r)
	{
		int nvCon = (int) this->m_vcontrolVal[r].size();
		int nvInt = 3*pModel->getSimRod_0(r).getNumNode();

		tVector vDiDc_r;
		dVector vsInt = pModel->getSimRod_0(r).getCurve().getVertex01Parametrization();
		this->computeJacRod(this->m_vcontrolPar[r], this->m_vcontrolVal[r], vsInt, vDiDc_r);

		// Assemble interpolate radii

		int numCoe = (int)vDiDc_r.size();
		for (int c = 0; c < numCoe; ++c)
		{
			const Triplet<Real>& t = vDiDc_r[c];
			vDiDc.push_back(Triplet<Real>(offsetInt + t.row(),
										  offsetCon + t.col(),
										  t.value()));
		}

		offsetCon += nvCon;
		offsetInt += nvInt;
	}

	mDiDc = SparseMatrixXd(offsetInt, offsetCon);
	mDiDc.setFromTriplets(vDiDc.begin(), vDiDc.end());
}

void ParameterSet_Tensile_HermiteRodMeshRest::computeValRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, VectorXd& vpInt) const
{
	int nvCon = (int)vsCon.size();
	int nvInt = (int)vsInt.size();

	int firstInt = 0, lastInt = nvCon - 2;

	int curInt = 0;
	for (int v = 0; v < nvInt; ++v)
	{
		Real si = vsInt[v];
		while (si > vsCon[curInt + 1])
			curInt++; // Next interval

		Real s0;
		Real s1 = vsCon[curInt + 0];
		Real s2 = vsCon[curInt + 1];
		Real s3;

		if (curInt > firstInt && curInt < lastInt)
		{
			s0 = vsCon[curInt - 1];
			s3 = vsCon[curInt + 2];
		}
		else if (curInt == firstInt && curInt == lastInt)
		{
			Real srange = s2 - s1;
			s0 = s1 - srange;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt)
		{
			s3 = vsCon[curInt + 2];
			Real srange = s2 - s1;
			s0 = s1 - srange;
		}
		else if (curInt == lastInt)
		{
			s0 = vsCon[curInt - 1];
			Real srange = s2 - s1;
			s3 = s2 + srange;
		}

		Real sp1 = (si - s1) / (s2 - s1);
		Real sp2 = sp1*sp1;
		Real sp3 = sp2*sp1;

		int offsetv = 3 * v;
		int offsetvInt0 = 3 * (curInt - 1);
		int offsetvInt1 = 3 * (curInt + 0);
		int offsetvInt2 = 3 * (curInt + 1);
		int offsetvInt3 = 3 * (curInt + 2);

		for (int d = 0; d < 3; ++d)
		{
			int offsetd = offsetv + d;
			int offsetdInt0 = offsetvInt0 + d;
			int offsetdInt1 = offsetvInt1 + d;
			int offsetdInt2 = offsetvInt2 + d;
			int offsetdInt3 = offsetvInt3 + d;

			Real p0;
			Real p1 = vpCon[offsetdInt1];
			Real p2 = vpCon[offsetdInt2];
			Real p3;

			if (curInt > firstInt && curInt < lastInt)
			{
				p0 = vpCon[offsetdInt0];
				p3 = vpCon[offsetdInt3];
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				p0 = p1;
				p3 = p2;
			}
			else if (curInt == firstInt)
			{
				p3 = vpCon[offsetdInt3];
				p0 = p1;
			}
			else if (curInt == lastInt)
			{
				p0 = vpCon[offsetdInt0];
				p3 = p2;
			}

			{
	#include "../../Maple/Code/CatmullRom3DPoint.mcg"

				vpInt(offsetd) = t25;
			}
		}
	}
}

void ParameterSet_Tensile_HermiteRodMeshRest::computeJacRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, tVector& vDpDm) const
{
	int nvCon = (int)vsCon.size();
	int nvInt = (int)vsInt.size();

	int firstInt = 0, lastInt = nvCon - 2;

	int curInt = 0;
	for (int v = 0; v < nvInt; ++v)
	{
		Real si = vsInt[v];
		while (si > vsCon[curInt + 1])
			curInt++; // Next interval

		Real s0;
		Real s1 = vsCon[curInt + 0];
		Real s2 = vsCon[curInt + 1];
		Real s3;
		if (curInt > firstInt && curInt < lastInt)
		{
			s0 = vsCon[curInt - 1];
			s3 = vsCon[curInt + 2];
		}
		else if (curInt == firstInt && curInt == lastInt)
		{
			Real srange = s2 - s1;
			s0 = s1 - srange;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt)
		{
			s3 = vsCon[curInt + 2];
			Real srange = s2 - s1;
			s0 = s1 - srange;
		}
		else if (curInt == lastInt)
		{
			s0 = vsCon[curInt - 1];
			Real srange = s2 - s1;
			s3 = s2 + srange;
		}

		Real sp1 = (si - s1) / (s2 - s1);
		Real sp2 = sp1*sp1;
		Real sp3 = sp2*sp1;

		int offsetv = 3 * v;
		int offsetvInt0 = 3 * (curInt - 1);
		int offsetvInt1 = 3 * (curInt + 0);
		int offsetvInt2 = 3 * (curInt + 1);
		int offsetvInt3 = 3 * (curInt + 2);

		for (int d = 0; d < 3; ++d)
		{
			int offsetd = offsetv + d;
			int offsetdInt0 = offsetvInt0 + d;
			int offsetdInt1 = offsetvInt1 + d;
			int offsetdInt2 = offsetvInt2 + d;
			int offsetdInt3 = offsetvInt3 + d;

			Real p0;
			Real p1 = vpCon[offsetdInt1];
			Real p2 = vpCon[offsetdInt2];
			Real p3;

			if (curInt > firstInt && curInt < lastInt)
			{
				p0 = vpCon[offsetdInt0];
				p3 = vpCon[offsetdInt3];
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				p0 = p1;
				p3 = p2;
			}
			else if (curInt == firstInt)
			{
				p3 = vpCon[offsetdInt3];
				p0 = p1;
			}
			else if (curInt == lastInt)
			{
				p0 = vpCon[offsetdInt0];
				p3 = p2;
			}

			dVector mJp(4);

			{
#include "../../Maple/Code/CatmullRom3DJacobian.mcg"
			}

			if (curInt > firstInt && curInt < lastInt)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[3]));
			}
			else if (curInt == firstInt)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
			else if (curInt == lastInt)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[3]));
			}
		}
	}
}

void ParameterSet_Tensile_HermiteRodMeshRest::updateControlPoints()
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int curIdx = 0;
	int offset = 0;

	this->m_vmapSplit2Whole.reserve(rodMesh.getNumNode());

	// Map connection points to whole indices
	iVector vmapConIdx(rodMesh.getNumCon(), -1);

	int nr = rodMesh.getNumRod();

	for (int r = 0; r < nr; ++r)
	{
		int nv = rodMesh.getRod(r).getNumNode();
		int nc = (int) this->m_vcontrolPar[r].size();
		const Curve& curve = rodMesh.getRod(r).getCurve();

		dVector vsNode = curve.getNodeParameters();

		Real parInc = 1.0 / (nc - 1);

		const pair<int, int>& rodCons = rodMesh.getRodCons(r);

		// First point -----------------------

		this->m_vcontrolPar[r][0] = 0;

		if (rodCons.first == -1)
		{
			m_vmapSplit2Whole.push_back(curIdx++);
		}
		else
		{
			if (vmapConIdx[rodCons.first] == -1)
				vmapConIdx[rodCons.first] = curIdx++;

			m_vmapSplit2Whole.push_back(vmapConIdx[rodCons.first]);
		}

		setBlock3x1(0, curve.getPosition(0), this->m_vcontrolVal[r]);

		// Middle points -----------------------

		for (int c = 1; c < nc - 1; ++c)
		{
			int vi, ei;
			Real parUni = c * parInc;
			Real parLen = parUni*vsNode.back();
			Vector3d pos = curve.samplePosition(parLen, vi, ei, 1e-6);

			this->m_vmapSplit2Whole.push_back(curIdx++);
			setBlock3x1(c, pos, this->m_vcontrolVal[r]);
			this->m_vcontrolPar[r](c) = parUni;
		}

		// Last point -----------------------

		this->m_vcontrolPar[r][nc-1] = 1.0;

		if (rodCons.second == -1)
		{
			m_vmapSplit2Whole.push_back(curIdx++);
		}
		else
		{
			if (vmapConIdx[rodCons.second] == -1)
				vmapConIdx[rodCons.second] = curIdx++;

			m_vmapSplit2Whole.push_back(vmapConIdx[rodCons.second]);
		}

		setBlock3x1(nc-1, curve.getPosition(nv-1), this->m_vcontrolVal[r]);

		// Add offset
		offset += nc;
	}
}


void ParameterSet_Tensile_HermiteRodMeshRest::testHermiteJacobian() const
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int nr = rodMesh.getNumRod();

	int offsetCon = 0;
	int offsetInt = 0;

	tVector vDiDc;

	for (int r = 0; r < nr; ++r)
	{
		int nvCon = (int) this->m_vcontrolVal[r].size();

		int nvInt;
		if (this->m_is2D)
			nvInt = 2 * pModel->getSimRod_0(r).getNumNode();
		else nvInt = 3 * pModel->getSimRod_0(r).getNumNode();

		VectorXd vpIntRodP(nvInt);
		VectorXd vpIntRodM(nvInt);
		dVector vsInt = pModel->getSimRod_0(r).getCurve().getVertex01Parametrization();

		VectorXd vcontrolVal = this->m_vcontrolVal[r];

		MatrixXd mDiDc_r(nvInt, nvCon);
		mDiDc_r.setZero(); // Initialize

		for (int c = 0; c < nvCon; ++c)
		{
			// +
			vpIntRodP.setZero();
			vcontrolVal(c) += 1e-6;
			this->computeValRod(this->m_vcontrolPar[r], vcontrolVal, vsInt, vpIntRodP);

			// -
			vpIntRodM.setZero();
			vcontrolVal(c) -= 2e-6;
			this->computeValRod(this->m_vcontrolPar[r], vcontrolVal, vsInt, vpIntRodM);

			// Estimate
			VectorXd vpIntRodDiff = (vpIntRodP - vpIntRodM) / (2e-6);

			for (int e = 0; e < nvInt; ++e)
				mDiDc_r(e, c) = vpIntRodDiff(e);

			// Restore
			vcontrolVal(c) += 1e-6;
		}

		// Assemble interpolate radii

		for (int i = 0; i < nvInt; ++i)
			for (int j = 0; j < nvCon; ++j)
				vDiDc.push_back(Triplet<Real>(offsetInt + i,
											  offsetCon + j,
											  mDiDc_r(i, j)));

		offsetCon += nvCon;
		offsetInt += nvInt;
	}

	SparseMatrixXd mDiDc_F(offsetInt, offsetCon);
	mDiDc_F.setFromTriplets(vDiDc.begin(), vDiDc.end());

	SparseMatrixXd mDiDc_A;
	this->computeJac(mDiDc_A);

	MatrixXd mDiDcDiff = (mDiDc_F.toDense() - mDiDc_A.toDense());
	Real FDNorm = mDiDc_F.norm();
	Real diffNorm = mDiDcDiff.norm();
	Real relError = diffNorm / FDNorm;

	if (FDNorm < 1e-9)
		diffNorm = 0.0;

	if (relError > 1e-9)
	{
		writeToFile(mDiDc_A, "DiDc_A.csv"); // Trace
		writeToFile(mDiDc_F, "DiDc_F.csv"); // Trace
		logSimu("[ERROR] Model: %s. DfDp test error (global): %.9f", this->m_ID.c_str(), relError);
	}
}