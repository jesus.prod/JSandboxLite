/*=====================================================================================*/
/*!
\file		CSTElementStVKOrthotropic.cpp
\author		jesusprod
\brief		Implementation of CSTElementStVKOrthotropic.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/CSTElementStVKOrthotropic.h>

CSTElementStVKOrthotropic::CSTElementStVKOrthotropic(int d0) : CSTElement(d0)
{
	// Nothing to do here

	this->m_vfs.resize(1);
	this->m_mDfsDs.resize(1, 1);
	this->m_mDfxDs.resize(m_numDOFx, 1);
}

CSTElementStVKOrthotropic::~CSTElementStVKOrthotropic()
{
	// Nothing to do here
}

void CSTElementStVKOrthotropic::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double mu = m_vpmat[0]->getPoisson();

	if (isApprox(E1, 0.0, 1e-9) &&
		isApprox(E2, 0.0, 1e-9) &&
		isApprox(mu, 0.0, 1e-9))
	{
		this->m_energy = 0;
		return;
	}

	const double& A0 = this->m_A0;
	const DNDx& dNdx = this->m_dNdx;

#include "../../Maple/Code/CSTStVKOrthotropicEnergy.mcg"

	this->m_energy = t80;

//	{
//		double lame1 = m_vpmat[0]->getLameFirst();
//		double lame2 = m_vpmat[0]->getLameSecond();
//
//#include "../../Maple/Code/CSTStVKIsotropicEnergy.mcg"
//
//		Real testEnergy = t57;
//
//		logSimu("[TRACE] Ortho/StVK: %f.9", this->m_energy / testEnergy);
//	}
//
//	{
//		double E1 = m_vpmat[0]->getYoung();
//		double E2 = m_vpmat[0]->getYoung();
//		double mu = m_vpmat[0]->getPoisson();
//
//#include "../../Maple/Code/CSTStVKTrueOrthotropicEnergy.mcg"
//
//		Real testEnergy = t71;
//
//		logSimu("[TRACE] Ortho/Custom: %f.9", this->m_energy / testEnergy);
//	}
}

void CSTElementStVKOrthotropic::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double mu = m_vpmat[0]->getPoisson();

	if (isApprox(E1, 0.0, 1e-9) && 
		isApprox(E2, 0.0, 1e-9) &&
		isApprox(mu, 0.0, 1e-9))
	{
		this->m_vfVal.setZero();
		return;
	}

	const double& A0 = this->m_A0;
	const DNDx& dNdx = this->m_dNdx;

	double vfx[9];

#include "../../Maple/Code/CSTStVKOrthotropicForce.mcg"

	for (int i = 0; i < 9; ++i)
		if (!isfinite(vfx[i]))
			logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 9; ++i)
		this->m_vfVal(i) = vfx[i];
}

void CSTElementStVKOrthotropic::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double mu = m_vpmat[0]->getPoisson();

	if (isApprox(E1, 0.0, 1e-9) &&
		isApprox(E2, 0.0, 1e-9) &&
		isApprox(mu, 0.0, 1e-9))
	{
		this->m_vJVal.setZero();
		return;
	}

	const double& A0 = this->m_A0;
	const DNDx& dNdx = this->m_dNdx;

	double mJx[9][9];

#include "../../Maple/Code/CSTStVKOrthotropicJacobian.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}

void CSTElementStVKOrthotropic::update_DfxDx(const VectorXd& vx, const VectorXd& vX)
{
	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double mu = m_vpmat[0]->getPoisson();
	double pS = this->m_preStrain;

	if (isApprox(E1, 0.0, 1e-9) &&
		isApprox(E2, 0.0, 1e-9) &&
		isApprox(mu, 0.0, 1e-9))
	{
		this->m_mDfxDx.setZero();
		return;
	}

	// Get material points

	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);
	vector<double> X3(3);
	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
			X3[i] = vX(this->m_vidx0[4 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
		X3[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
			X3[i] = vX(this->m_vidx0[6 + i]);
		}
	}
	else assert(false); // WTF?

	double mJx[9][9];

#include "../../Maple/Code/CSTStVKOrthotropicRest_DfxDx.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			this->m_mDfxDx(i, j) = mJx[i][j];
}

void CSTElementStVKOrthotropic::update_DfxD0(const VectorXd& vx, const VectorXd& vX)
{
	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double mu = m_vpmat[0]->getPoisson();
	double pS = this->m_preStrain;

	if (isApprox(E1, 0.0, 1e-9) &&
		isApprox(E2, 0.0, 1e-9) &&
		isApprox(mu, 0.0, 1e-9))
	{
		this->m_mDfxD0.setZero();
		return;
	}

	// Get material points

	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);
	vector<double> X3(3);
	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
			X3[i] = vX(this->m_vidx0[4 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
		X3[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
			X3[i] = vX(this->m_vidx0[6 + i]);
		}
	}
	else assert(false); // WTF?

	double mJX[9][9];

#include "../../Maple/Code/CSTStVKOrthotropicRest_DfxD0.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJX[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_0; ++k)
			{
				this->m_mDfxD0(i, this->m_numDim_0*j + k) = mJX[i][this->m_numDim_x*j + k];
			}
		}
	}
}

void CSTElementStVKOrthotropic::update_DfxDs(const VectorXd& vx, const VectorXd& vX)
{
	double E1 = m_vpmat[0]->getYoung();
	double E2 = m_vpmat[0]->getYoung();
	double mu = m_vpmat[0]->getPoisson();
	double pS = this->m_preStrain;

	if (isApprox(E1, 0.0, 1e-9) &&
		isApprox(E2, 0.0, 1e-9) &&
		isApprox(mu, 0.0, 1e-9))
	{
		this->m_mDfxDs.setZero();
		return;
	}

	// Get material points

	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);
	vector<double> X3(3);
	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
			X3[i] = vX(this->m_vidx0[4 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
		X3[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
			X3[i] = vX(this->m_vidx0[6 + i]);
		}
	}
	else assert(false); // WTF?

	double mJs[9][1];

#include "../../Maple/Code/CSTStVKOrthotropicRest_DfxDs.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 1; ++j)
			if (!isfinite(mJs[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	this->m_mDfxDs.resize(this->m_numDOFx, 1);
	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		this->m_mDfxDs(i, 0) = mJs[i][0];
	}
}