/*=====================================================================================*/
/*!
\file		BConditionPreStrain.cpp
\author		jesusprod
\brief		Implementation of BConditionPreStrain
/*=====================================================================================*/

#include <JSandbox/Solver/BConditionPreStrainSurface.h>

#include <JSandbox/Model/SurfaceModel.h>

BConditionPreStrainSurface::BConditionPreStrainSurface()
{
	this->m_factor = 1.0;
	this->m_maxStep = 1;
	this->m_curStep = 1;
}

BConditionPreStrainSurface::~BConditionPreStrainSurface()
{
	// Nothing to do here...
}

void BConditionPreStrainSurface::constrain(SolidModel* pModel) const
{
	double factor = 1.0 - (1.0 - this->m_factor)*this->getLoadingFactor();
	SurfaceModel* pSModel = dynamic_cast<SurfaceModel*>(pModel);
	int numEle = pSModel->getNumEle();

	// PreStrain elements
	for (int i = 0; i < numEle; ++i)
		pSModel->getElement(i)->setPreStrain(factor);

	logSimu("[TRACE] Constraint applied: Surface pre-strain factor %f\n", factor);
}