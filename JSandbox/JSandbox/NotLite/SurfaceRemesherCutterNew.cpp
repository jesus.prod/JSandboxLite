/*=====================================================================================*/
/*!
\file		SurfaceRemesherCutterNew.cpp
\author		jesusprod
\brief		Implementation of SurfaceRemesherCutterNew.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/SurfaceRemesherCutterNew.h>

SurfaceRemesherCutterNew::SurfaceRemesherCutterNew()
{
	// Nothing to do here
}

SurfaceRemesherCutterNew::~SurfaceRemesherCutterNew()
{
	// Nothing to do here
}
//
//bool SurfaceRemesherCutterNew::remesh(TriMesh* pMesh, const BVHTree* pEdgeTree, const BVHTree* pFaceTree)
//{
//	assert(pMesh != NULL);
//
//	// Copy mesh to remesh
//
//	this->m_pMesh = pMesh;
//
//	this->m_vcutVerts.clear();
//	this->m_vdupsMap.clear();
//
//	int nFace = (int) this->m_pMesh->n_faces();
//	int nVert = (int) this->m_pMesh->n_vertices();
//	int nCutEdge = (int) this->m_vcutEdges.size();
//
//	// Get face partition
//
//	vector<TriMesh::EdgeHandle> vcutEdgesH(nCutEdge);
//
//	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge)
//		vcutEdgesH[iEdge] = m_pMesh->edge_handle(this->m_vcutEdges[iEdge]);
//
//	vector<vector<TriMesh::FaceHandle>> vfhs; // Get face partition
//	this->m_pMesh->getPartitionBoundaryEdgeRegions(vcutEdgesH, vfhs);
//
//	if (vfhs.size() < 2)
//		return true; // OK
//
//	iVector vfaceCol(nFace);
//
//	int nReg = (int)vfhs.size();
//	this->m_vdupsMap.resize(nReg);
//	for (int r = 0; r < nReg; ++r)
//	{
//		int nRegFace = (int)vfhs[r].size();
//		for (int f = 0; f < nRegFace; ++f)
//			vfaceCol[vfhs[r][f].idx()] = r;
//	}
//
//	// Get cut vertices
//
//	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge)
//	{
//		const TriMesh::EdgeHandle& eh = vcutEdgesH[iEdge];
//
//		vector<TriMesh::VertexHandle> vvh;
//		m_pMesh->getEdgeVertices(eh, vvh);
//
//		m_vcutVerts.push_back(vvh[0].idx());
//		m_vcutVerts.push_back(vvh[1].idx());
//	}
//
//	// Remove duplicates from vertices vector
//
//	sort(m_vcutVerts.begin(), m_vcutVerts.end());
//
//	m_vcutVerts.erase(unique(m_vcutVerts.begin(), m_vcutVerts.end()), m_vcutVerts.end());
//
//	int nCutVert = (int) this->m_vcutVerts.size();
//
//	// Duplicate vertices
//
//	bVector visCutVert(nVert, false);
//
//	for (int iVert = 0; iVert < nCutVert; ++iVert)
//	{
//		int vidx = this->m_vcutVerts[iVert];
//
//		const TriMesh::VertexHandle& vh = this->m_pMesh->vertex_handle(vidx);
//
//		vector<TriMesh::FaceHandle> vfh;
//		m_pMesh->getVertexFaces(vh, vfh);
//		int vnf = (int)vfh.size();
//		for (int f = 0; f < vnf; ++f)
//		{
//			int col = vfaceCol[vfh[f].idx()];
//
//			// Add duplicated vertex if needed for each vertex face
//			if (m_vdupsMap[col].find(vh) == m_vdupsMap[col].end())
//			{
//				TriMesh::Point p = this->m_pMesh->point(vh); // Add
//				m_vdupsMap[col][vh] = this->m_pMesh->add_vertex(p);
//			}
//		}
//
//		visCutVert[vh.idx()] = true;
//	}
//
//	// Rebuild faces
//
//	bVector vrebuiltFaces(nFace, false);
//
//	for (int iVert = 0; iVert < nCutVert; ++iVert)
//	{
//		int vidx = this->m_vcutVerts[iVert];
//
//		const TriMesh::VertexHandle& vh = this->m_pMesh->vertex_handle(vidx);
//
//		if (this->m_pMesh->is_isolated(vh))
//			continue; // Already isolated
//
//		vector<TriMesh::FaceHandle> vfh;
//		m_pMesh->getVertexFaces(vh, vfh);
//		int vnf = (int)vfh.size();
//		for (int f = 0; f < vnf; ++f)
//		{
//			int fidx = vfh[f].idx();
//			if (vrebuiltFaces[fidx])
//				continue;
//
//			vrebuiltFaces[fidx] = true;
//			int col = vfaceCol[fidx];
//
//			vector<TriMesh::VertexHandle> vvh;
//			m_pMesh->getFaceVertices(vfh[f], vvh);
//			vector<TriMesh::VertexHandle> vvhNew = vvh;
//
//			for (int v = 0; v < 3; ++v)
//			{
//				// Duplicated
//				if (visCutVert[vvh[v].idx()]) 
//					vvhNew[v] = m_vdupsMap[col][vvh[v]];
//			}
//
//			// Substitute face
//
//			m_pMesh->delete_face(vfh[f], false); // Remove faces 
//
//			TriMesh::FaceHandle fhnew = m_pMesh->add_face(vvhNew);
//		}
//	}
//
//	this->m_pMesh->delete_isolated_vertices();
//	this->m_pMesh->garbage_collection();
//
//	return true;
//}


bool SurfaceRemesherCutterNew::remesh(TriMesh* pMesh, const BVHTree* pEdgeTree, const BVHTree* pFaceTree)
{
	assert(pMesh != NULL);

	// Copy mesh to remesh

	this->m_pMesh = pMesh;

	this->m_vcutVerts.clear();
	this->m_vdupsMap.clear();

	int nEdge = (int) this->m_pMesh->getNumEdge();
	int nFace = (int) this->m_pMesh->getNumFace();
	int nNode = (int) this->m_pMesh->getNumNode();
	int nCutEdge = (int) this->m_vcutEdges.size();
	int nCutNode = 0;
	int nCutFace = 0;

	// Return no change

	if (nCutEdge == 0)
		return false;

	vector<TriMesh::EdgeHandle> vcutEdgesH;
	vector<TriMesh::FaceHandle> vcutFacesH;
	vector<TriMesh::VertexHandle> vcutNodesH;
	iVector vcutEdgesB(nEdge, -1);
	iVector vcutFacesB(nFace, -1);
	iVector vcutNodesB(nNode, -1);

	// Get cut edges and stencil -----------------------------------------

	vcutEdgesH.resize(nCutEdge);
	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge)
		vcutEdgesH[iEdge] = m_pMesh->edge_handle(this->m_vcutEdges[iEdge]);

	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge) 
		vcutEdgesB[vcutEdgesH[iEdge].idx()] = iEdge;

	// Get cut nodes and stencil -----------------------------------------

	vector<vector<TriMesh::VertexHandle>> vcopies;
	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge)
	{
		const TriMesh::EdgeHandle& eh = vcutEdgesH[iEdge];

		vector<TriMesh::VertexHandle> vvh;
		m_pMesh->getEdgeVertices(eh, vvh);

		for (int iEdgeNode = 0; iEdgeNode < 2; ++iEdgeNode)
		{
			const TriMesh::VertexHandle& vh = vvh[iEdgeNode];
			vector<TriMesh::EdgeHandle> veh;
			m_pMesh->getVertexEdges(vh, veh);

			int idx = vh.idx();

			int nc = 0;

			for (int iNodeEdge = 0; iNodeEdge < (int) veh.size(); ++iNodeEdge)
				if (vcutEdgesB[veh[iNodeEdge].idx()] != -1) 
					nc++; // Add another copy

			if (m_pMesh->is_boundary(vh))
				nc++; // Add another copy

			if (nc > 1 && vcutNodesB[idx] == -1)
			{
				int cutIdx = (int) vcutNodesH.size();

				vcutNodesB[idx] = cutIdx;
				vcutNodesH.push_back(vh);

				// Duplicate the node

				TriMesh::Point point = this->m_pMesh->point(vh);
				vcopies.push_back(vector<TriMesh::VertexHandle>());
				
				vcopies.back().resize(nc);
				for (int iCopy = 0; iCopy < nc; ++iCopy)
					vcopies.back()[iCopy] = m_pMesh->add_vertex(point);
			}
		}
	}

	nCutNode = (int) vcutNodesH.size();

	// Get cut faces and stencil -------------------------------------------

	for (int iNode = 0; iNode < nCutNode; ++iNode)
	{
		const TriMesh::VertexHandle& vh = vcutNodesH[iNode];

		vector<TriMesh::FaceHandle> vfh;
		m_pMesh->getVertexFaces(vh, vfh);

		for (int iFace = 0; iFace < (int)vfh.size(); ++iFace)
		{
			int idx = vfh[iFace].idx();
			if (vcutFacesB[idx] == -1)
			{
				vcutFacesB[idx] = (int) vcutFacesH.size();
				vcutFacesH.push_back(vfh[iFace]);
			}
		}
	}

	nCutFace = (int) vcutFacesH.size();

	// Get cut strips -----------------------------------------------------

	vector<int> vnextFace; 
	vnextFace.push_back(0);

	iVector vaddedFaces(nCutFace, -1);

	vector<vector<TriMesh::FaceHandle>> vcutStripsH;
	vcutStripsH.push_back(vector<TriMesh::FaceHandle>());

	while (!vnextFace.empty())
	{
		int nextFaceIdx = vnextFace.back();

		vnextFace.pop_back();

		if (vaddedFaces[nextFaceIdx] == -1)
		{
			const TriMesh::FaceHandle& fh = vcutFacesH[nextFaceIdx];

			// Add face to strip

			int strN = (int)vcutStripsH.size() - 1;

			vcutStripsH.back().push_back(fh);
			vaddedFaces[nextFaceIdx] = strN;

			// Get neighbor edges

			vector<TriMesh::EdgeHandle> vfeh;
			m_pMesh->getFaceEdges(fh, vfeh);

			// Add neighbor faces

			for (int iEdge = 0; iEdge < 3; ++iEdge)
			{
				if (this->m_pMesh->is_boundary(vfeh[iEdge]))
					continue; // Do not consider boundary!

				if (vcutEdgesB[vfeh[iEdge].idx()] != -1)
					continue; // Do not consider cut edges!

				TriMesh::FaceHandle fhOther;
				vector<TriMesh::FaceHandle> vfhOther;
				this->m_pMesh->getEdgeFaces(vfeh[iEdge], vfhOther);
				if (vfhOther[0] == fh) fhOther = vfhOther[1];
				if (vfhOther[1] == fh) fhOther = vfhOther[0];

				int cutIdx = vcutFacesB[fhOther.idx()];

				// Is a cut face?
				if (cutIdx == -1)
					continue;

				// Is already in another strip?
				if (vaddedFaces[cutIdx] != -1)
					continue;

				vnextFace.push_back(cutIdx);
			}
		}

		// If empty, new strip
		
		if (vnextFace.empty())
		{
			for (int iFace = 0; iFace < nCutFace; ++iFace)
				if (vaddedFaces[iFace] == -1)
				{
				vcutStripsH.push_back(vector<TriMesh::FaceHandle>());
				vnextFace.push_back(iFace);
				break;
				}
		}
	}

	// Remesh faces -------------------------------------------------

	vector<vector<TriMesh::VertexHandle>> vfaceNodes(nCutFace);

	for (int iFace = 0; iFace < nCutFace; ++iFace)
	{
		const TriMesh::FaceHandle& fh = vcutFacesH[iFace];
		m_pMesh->getFaceVertices(fh, vfaceNodes[iFace]);
	}

	int nStrips = (int) vcutStripsH.size();

	for (int iStrip = 0; iStrip < nStrips; ++iStrip)
	{
		int nFaces = (int) vcutStripsH[iStrip].size();

		// Remesh strip

		bVector vnodeUsed(nCutNode, false);

		for (int iFace = 0; iFace < nFaces; ++iFace)
		{
			const TriMesh::FaceHandle& fh = vcutStripsH[iStrip][iFace];

			const vector<TriMesh::VertexHandle>& vvhOld = vfaceNodes[vcutFacesB[fh.idx()]];

			// Delete previous face

			m_pMesh->delete_face(fh, false);

			// Create from substitutes

			vector<TriMesh::VertexHandle> vvhNew;
			for (int iNode = 0; iNode < 3; ++iNode)
			{
				int nodeIdx = vvhOld[iNode].idx();
				int cutIdx = vcutNodesB[nodeIdx];
				if (cutIdx != -1)
				{
					vnodeUsed[cutIdx] = true;
					vvhNew.push_back(vcopies[cutIdx].back());
				}
				else vvhNew.push_back(vvhOld[iNode]);
			}

			m_pMesh->add_face(vvhNew);
		}

		// Reduce nodes

		for (int iNode = 0; iNode < nCutNode; ++iNode)
			if (vnodeUsed[iNode])
				vcopies[iNode].pop_back();
	}

	this->m_pMesh->delete_isolated_vertices();
	this->m_pMesh->garbage_collection();

	return true;
}