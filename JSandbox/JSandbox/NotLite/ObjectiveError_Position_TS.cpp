///*=====================================================================================*/
///*!
//\file		ObjectiveError_Position_TS.cpp
//\author		jesusprod
//\brief		Implementation of ObjectiveError_Position.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Optim/ObjectiveError_Position_TS.h>
//
//ObjectiveError_Position_TS::ObjectiveError_Position_TS() : ObjectiveError("POSITIONTS")
//{
//	// Nothing to do here
//}
//
//ObjectiveError_Position_TS::~ObjectiveError_Position_TS()
//{
//	// Nothing to do here
//}
//
//const VectorXd& ObjectiveError_Position_TS::getTargetPosition() const
//{
//	return this->m_vxt;
//}
//
//void ObjectiveError_Position_TS::setTargetPosition(const VectorXd& vxt)
//{
//	assert((int)vxt.size() == this->m_pSurfModel->getNumRawDOF());
//
//	// Target position is specified in configuration space, so
//	// it should be transformed to simulation space to compare
//	// the value.
//
//	// TODO: Implement shortcuts for space conversion.
//
//	const iVector& vsimDOF = this->m_pSurfModel->getRawToSim_x();
//	int nrawDOF = this->m_pSurfModel->getNumRawDOF();
//	int nsimDOF = this->m_pSurfModel->getNumSimDOF_x();
//	this->m_vxt.resize(nsimDOF);
//	for (int i = 0; i < nrawDOF; ++i)
//	{
//		if (vsimDOF[i] == -1)
//			continue; // Ignore
//
//		this->m_vxt[vsimDOF[i]] = vxt[i];
//	}
//}
//
//Real ObjectiveError_Position_TS::getError() const
//{
//	assert(this->m_pSurfModel != NULL); // Initialized model
//
//	const VectorXd& vx = this->m_pSurfModel->getPositions_x();
//	VectorXd vxD = vx - this->m_vxt;
//	return  0.5*vxD.dot(vxD);
//}
//
//void ObjectiveError_Position_TS::add_DEDx(VectorXd& vDEDx) const
//{
//	assert(this->m_pSurfModel != NULL); // Initialized model
//	
//	const VectorXd& vx_Mod = this->m_pSurfModel->getPositions_x();
//	VectorXd vDEDx_Mod = vx_Mod - this->m_vxt; // Model gradient
//
//	this->m_pTSModel->addLocal2Global(0, vDEDx_Mod, vDEDx);
//}
//
//void ObjectiveError_Position_TS::add_D2EDx2(tVector& vD2EDx2) const
//{
//	assert(this->m_pSurfModel != NULL); // Initialized model
//
//	int nsimDOF = this->m_pSurfModel->getNumSimDOF_x();
//
//	tVector vD2EDx2_Mod(nsimDOF);
//	for (int i = 0; i < nsimDOF; ++i) // Add identity
//		vD2EDx2_Mod[i] = Triplet<Real>(i, i, 1.0);
//
//	this->m_pTSModel->addLocal2Global(0, vD2EDx2_Mod, vD2EDx2);
//}
//
//void ObjectiveError_Position_TS::setup()
//{
//	assert(this->m_pModel != NULL);
//
//	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
//
//	assert(this->m_pTSModel != NULL);
//
//	this->m_pSurfModel = this->m_pTSModel->getSurfaceModel(0); 
//}