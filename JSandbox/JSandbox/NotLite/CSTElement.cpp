/*=====================================================================================*/
/*!
\file		CSTElement.cpp
\author		jesusprod
\brief		Implementation of CSTElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/CSTElement.h>

CSTElement::CSTElement(int d0) : NodalSolidElement(3, 3, d0)
{
	this->m_t0 = 1.0;
	this->m_A0 = 0.0;

	// This carries mass
	this->m_addMass = true;
}

CSTElement::~CSTElement()
{
	// Nothing to do here
}

void CSTElement::update_Rest(const VectorXd& vX)
{
	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);
	vector<double> X3(3);
	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
			X3[i] = vX(this->m_vidx0[4 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
		X3[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
			X3[i] = vX(this->m_vidx0[6 + i]);
		}
	}
	else assert(false); // WTF?

	double pS = this->m_preStrain;

	DNDx& dNdx = this->m_dNdx;

	{
#include "../../Maple/Code/CSTStVKIsotropicRest_dNdx.mcg"
	}

	{
#include "../../Maple/Code/CSTStVKIsotropicRest_area.mcg"

		this->m_A0 = t151;
	}
}

void CSTElement::computeN(const VectorXd& vX, DNDx& mDNDx)
{
	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);
	vector<double> X3(3);
	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
			X3[i] = vX(this->m_vidx0[4 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
		X3[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
			X3[i] = vX(this->m_vidx0[6 + i]);
		}
	}
	else assert(false); // WTF?

	double pS = this->m_preStrain;

	DNDx& dNdx = mDNDx;

	{
#include "../../Maple/Code/CSTStVKIsotropicRest_dNdx.mcg"
	}
}

void CSTElement::computeF(const VectorXd& vx, const VectorXd& vX, DNDx& mDxDX)
{
	DNDx mDNDx;

	this->computeN(vX, mDNDx);

	memset(&mDxDX, 0, 6*sizeof(Real));

	for (int nodei = 0; nodei < 3; nodei++)
	{
		int nidx = m_vnodeIdx0[nodei];

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				mDxDX[i][j] += vx[3*nidx + i] * mDNDx[nodei][j];
			}
		}
	}
}

void CSTElement::computeC(const VectorXd& vx, const VectorXd& vX, Matrix2d& C)
{
	DNDx mDxDX;

	this->computeF(vx, vX, mDxDX);

	C.setZero();
	for (int j = 0; j < 2; j++)
	for (int k = 0; k < 2; k++)
	for (int i = 0; i < 3; i++)
		C(j, k) += mDxDX[i][j] * mDxDX[i][k];
}

void CSTElement::computeLambdas(const VectorXd& vx, const VectorXd& vX, Real& L1, Real& L2, Vector2d& vL1, Vector2d& vL2)
{
	Matrix2d mC;

	this->computeC(vx, vX, mC);

	EigenSolver<Matrix2d> solver(mC, true);
	L1 = sqrt(solver.eigenvalues().real().x());
	L2 = sqrt(solver.eigenvalues().real().y());
	VectorXd vL1_x = solver.eigenvectors().real().col(0);
	VectorXd vL2_x = solver.eigenvectors().real().col(1);
}

void CSTElement::update_Mass(const VectorXd& vX)
{
	Real nodeMass = (1.0 / 3.0)*this->m_A0
					*this->m_vpmat[0]->getDensity()
					*this->m_vpmat[0]->getThickness();

	for (int i = 0; i < this->m_numDOFx; ++i)
		this->m_vmx[i] = nodeMass; // All same
}

Real CSTElement::compute_Area_2D(const vector<Vector2d>& vp) const
{
	return 0.5*(vp[1] - vp[0]).norm()*(vp[2] - vp[0]).norm();
}

void CSTElement::compute_DNDx_2D(const vector<Vector2d>& vp, DNDx& DNDx) const
{
	Vector2d x1_2D(0., 0.), x2_2D(0., 0.), x3_2D(0., 0.);

	Vector2d x12 = vp[1] - vp[0];
	Vector2d x13 = vp[2] - vp[0];

	Vector2d u = x12.normalized();
	Vector2d v = Vector2d(-u[1], u[0]);

	x2_2D(0) = x12.dot(u);
	x3_2D(0) = x13.dot(u);
	x3_2D(1) = x13.dot(v);

	double twiceArea = x1_2D[0] * x2_2D[1] + x2_2D[0] * x3_2D[1] + x3_2D[0] * x1_2D[1]
					 - x1_2D[1] * x2_2D[0] - x2_2D[1] * x3_2D[0] - x3_2D[1] * x1_2D[0];

	DNDx[0][0] = (x2_2D[1] - x3_2D[1]) / twiceArea;
	DNDx[0][1] = (x3_2D[0] - x2_2D[0]) / twiceArea;

	DNDx[1][0] = (x3_2D[1] - x1_2D[1]) / twiceArea;
	DNDx[1][1] = (x1_2D[0] - x3_2D[0]) / twiceArea;

	DNDx[2][0] = (x1_2D[1] - x2_2D[1]) / twiceArea;
	DNDx[2][1] = (x2_2D[0] - x1_2D[0]) / twiceArea;
}


Real CSTElement::compute_Area_3D(const vector<Vector3d>& vp) const
{
	return 0.5*(vp[1] - vp[0]).cross(vp[2] - vp[0]).norm();
}

void CSTElement::compute_DNDx_3D(const vector<Vector3d>& vp, DNDx& DNDx) const
{
	Vector2d x1_2D(0., 0.), x2_2D(0., 0.), x3_2D(0., 0.);

	Vector3d x12 = vp[1] - vp[0];
	Vector3d x13 = vp[2] - vp[0];

	Vector3d n = x12.cross(x13);
	Vector3d w = n.normalized();
	Vector3d u = x12.normalized();
	Vector3d v = w.cross(u);

	x2_2D[0] = x12.dot(u);
	x3_2D[0] = x13.dot(u);
	x3_2D[1] = x13.dot(v);

	double TwiceArea = x1_2D[0] * x2_2D[1] + x2_2D[0] * x3_2D[1] + x3_2D[0] * x1_2D[1]
					 - x1_2D[1] * x2_2D[0] - x2_2D[1] * x3_2D[0] - x3_2D[1] * x1_2D[0];

	DNDx[0][0] = (x2_2D[1] - x3_2D[1]) / TwiceArea;
	DNDx[0][1] = (x3_2D[0] - x2_2D[0]) / TwiceArea;

	DNDx[1][0] = (x3_2D[1] - x1_2D[1]) / TwiceArea;
	DNDx[1][1] = (x1_2D[0] - x3_2D[0]) / TwiceArea;

	DNDx[2][0] = (x1_2D[1] - x2_2D[1]) / TwiceArea;
	DNDx[2][1] = (x2_2D[0] - x1_2D[0]) / TwiceArea;
}

void CSTElement::update_DmxD0(const VectorXd& vx, const VectorXd& vX)
{
	double mu = this->m_vpmat[0]->getDensity();
	double th = this->m_vpmat[0]->getThickness();
	double pS = this->m_preStrain;

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);
	vector<double> X3(3);
	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
			X3[i] = vX(this->m_vidx0[4 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
		X3[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
			X3[i] = vX(this->m_vidx0[6 + i]);
		}
	}
	else assert(false); // WTF?

	double DmxDX[9];

#include "../../Maple/Code/CSTStVKIsotropicRest_DmxD0.mcg"

	for (int i = 0; i < 9; ++i)
		if (!isfinite(DmxDX[i]))
			logSimu("In %s: DmxDX value is NAN\n", __FUNCDNAME__);

	this->m_mDmxD0.resize(this->m_numDOFx, this->m_numDOF0);

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_0; ++k)
			{
				this->m_mDmxD0(i, this->m_numDim_0*j + k) = DmxDX[this->m_numDim_x*j + k];
			}
		}
	}
}

void CSTElement::update_DmxDs(const VectorXd& vx, const VectorXd& vX)
{
	double mu = this->m_vpmat[0]->getDensity();
	double th = this->m_vpmat[0]->getThickness();
	double pS = this->m_preStrain;

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);
	vector<double> X3(3);
	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
			X3[i] = vX(this->m_vidx0[4 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
		X3[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
			X3[i] = vX(this->m_vidx0[6 + i]);
		}
	}
	else assert(false); // WTF?

	double DmxDs[1];

#include "../../Maple/Code/CSTStVKIsotropicRest_DmxDs.mcg"

	for (int i = 0; i < 1; ++i)
		if (!isfinite(DmxDs[i]))
			logSimu("In %s: DmxDs value is NAN\n", __FUNCDNAME__);

	this->m_mDmxDs.resize(this->m_numDOFx, 1);
	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		this->m_mDmxDs(i, 0) = DmxDs[0];
	}
}

void CSTElement::add_DfxDs(tVector& vDfxDs)
{
	for (int i = 0; i < this->m_numDOFx; ++i) // Add triplets in the allocated index
		vDfxDs.push_back(Triplet<Real>(this->m_vidxx[i], 0, this->m_mDfxDs(i, 0)));
}

void CSTElement::add_DmxDs(tVector& vDmxDs)
{
	for (int i = 0; i < this->m_numDOFx; ++i) // Add triplets in the allocated index
		vDmxDs.push_back(Triplet<Real>(this->m_vidxx[i], 0, this->m_mDmxDs(i, 0)));
}