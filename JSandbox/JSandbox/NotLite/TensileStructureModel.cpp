/*=====================================================================================*/
/*!
\file		TensileStructureModel.cpp
\author		jesusprod
\brief		Implementation of RodMeshModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/TensileStructureModel.h>

#include <JSandbox/Model/DERModel.h>

#include <omp.h> // MP support

#include <JSandbox/Model/SurfaceModel2DRest.h>

#include <JSandbox/Geometry/CGALConstrainedMesher.h>

TensileStructureModel::TensileStructureModel() : CompositeModel("TS")
{
	// Nothing to do here...
}

TensileStructureModel::~TensileStructureModel()
{
	logSimu("[TRACE] Destroying TensileStructureModel: %d", this);
}

void TensileStructureModel::setup()
{
	for (int i = 0; i < this->getNumSurfaceModels(); ++i)
		if (this->m_vpSurfaceModel[i] == NULL)
			throw new exception("[ERROR] NPE");

	for (int i = 0; i < this->getNumRodMeshModels(); ++i)
		if (this->m_vpRodMeshModel[i] == NULL)
			throw new exception("[ERROR] NPE");

	// Create model vector

	vector<SolidModel*> vpModels;

	this->m_surfaceOffset = (int) vpModels.size();
	for (int i = 0; i < this->getNumSurfaceModels(); ++i)
		vpModels.push_back(this->m_vpSurfaceModel[i]);

	this->m_rodMeshOffset = (int)vpModels.size();
	for (int i = 0; i < this->getNumRodMeshModels(); ++i)
		vpModels.push_back(this->m_vpRodMeshModel[i]);

	// Remesh considering couplings

	//if (!this->remeshFromCouplings())
	//{
	//	throw new exception("[ERROR] Invalid remeshing");
	//}

	//for (int i = 0; i < (int) vpModels.size(); ++i)
	//	vpModels[i]->setup(); // Necessary setup

	// NOTE: Only valid for all embedded...

	int numCouplingsTS = (int) this->m_vpTSCouplings.size();

	for (int i = 0; i < numCouplingsTS; ++i)
	{
		ModelCoupling* pCoupling = this->m_vpTSCouplings[i];

		RodMesh* pRodMesh_0 = NULL;
		RodMesh* pRodMesh_x = NULL;
		TriMesh* pTriMesh = NULL;
		int idxTri = -1;
		int idxRod = -1;

		if (pCoupling->getModel0() < this->m_rodMeshOffset && pCoupling->getModel1() >= this->m_rodMeshOffset)
		{
			int midxSur = pCoupling->getModel0();
			int midxRod = pCoupling->getModel1() - this->m_rodMeshOffset;
			pRodMesh_x = &this->m_vpRodMeshModel[midxRod]->getSimMesh_x();
			pRodMesh_0 = &this->m_vpRodMeshModel[midxRod]->getSimMesh_0();
			pTriMesh = &this->m_vpSurfaceModel[midxSur]->getSimMesh();
			idxTri = pCoupling->getModelPoint0().getPointIdx();
			idxRod = pCoupling->getModelPoint1().getPointIdx();
		}
		else if (pCoupling->getModel1() < this->m_rodMeshOffset && pCoupling->getModel0() >= this->m_rodMeshOffset)
		{
			int midxSur = pCoupling->getModel1();
			int midxRod = pCoupling->getModel0() - this->m_rodMeshOffset;
			pRodMesh_x = &this->m_vpRodMeshModel[midxRod]->getSimMesh_x();
			pRodMesh_0 = &this->m_vpRodMeshModel[midxRod]->getSimMesh_0();
			pTriMesh = &this->m_vpSurfaceModel[midxSur]->getSimMesh();
			idxTri = pCoupling->getModelPoint1().getPointIdx();
			idxRod = pCoupling->getModelPoint0().getPointIdx();
		}
		else continue;

		int rodIdx, nodeIdx;
		pRodMesh_0->getRodNodeForMeshNode(idxRod, rodIdx, nodeIdx);
		Vector3d targetPosx = pRodMesh_x->getRod(rodIdx).getCurve().getPosition(nodeIdx);
		Vector3d targetPos0 = pRodMesh_0->getRod(rodIdx).getCurve().getPosition(nodeIdx);
		Vector3d currentPosx = pTriMesh->getPoint(pTriMesh->vertex_handle(idxTri), VProperty::POSE);
		Vector3d currentPos0 = pTriMesh->getPoint(pTriMesh->vertex_handle(idxTri), VProperty::POS0);
		if ((targetPos0 - currentPos0).norm() > EPS_POS)
		{
			logSimu("[TRACE] Correcting position (0): %.9f", (targetPos0 - currentPos0).norm());
			pTriMesh->setPoint(pTriMesh->vertex_handle(idxTri), targetPos0, VProperty::POS0);
		}
		if ((targetPosx - currentPosx).norm() > EPS_POS)
		{
			logSimu("[TRACE] Correcting position (x): %.9f", (targetPosx - currentPosx).norm());
			pTriMesh->setPoint(pTriMesh->vertex_handle(idxTri), targetPosx, VProperty::POSE);
		}
	}

	for (int i = 0; i < this->getNumSurfaceModels(); ++i)
		this->m_vpSurfaceModel[i]->update_StateFromSimMesh();

	// Create couplings and set master points

	vector<iVector> vmasterMem(this->getNumSurfaceModels());
	vector<iVector> vmasterRod(this->getNumRodMeshModels());

	int numCouplings = (int) this->m_vpTSCouplings.size();
	vector<ModelCoupling*> vpCouplingsDOF(numCouplings);

	for (int i = 0; i < numCouplings; ++i)
	{
		vpCouplingsDOF[i] = new ModelCoupling(*this->m_vpTSCouplings[i]);

		assert(this->m_vpTSCouplings[i]->getModelPoint0().isExplicit());
		assert(this->m_vpTSCouplings[i]->getModelPoint1().isExplicit());

		int m0 = this->m_vpTSCouplings[i]->getModel0();
		int m1 = this->m_vpTSCouplings[i]->getModel1();

		if (m0 >= this->m_rodMeshOffset)
		{
			int modelIdx = m0 - this->m_rodMeshOffset;
			int nodeIdx = vpCouplingsDOF[i]->getModelPoint0().getPointIdx();
			int DOFIdx = getRodMeshModel(modelIdx)->getNodeRawDOF(nodeIdx)[0];

			vmasterRod[modelIdx].push_back(vpCouplingsDOF[i]->getModelPoint0().getPointIdx());

			vpCouplingsDOF[i]->getModelPoint0().setPointIdx(DOFIdx);
		}
		else
		{
			vmasterMem[m0 - this->m_surfaceOffset].push_back(vpCouplingsDOF[i]->getModelPoint0().getPointIdx());
			vpCouplingsDOF[i]->getModelPoint0().setPointIdx(vpCouplingsDOF[i]->getModelPoint0().getPointIdx()*3);
		}

		if (m1 >= this->m_rodMeshOffset)
		{
			int modelIdx = m1 - this->m_rodMeshOffset;
			int nodeIdx = vpCouplingsDOF[i]->getModelPoint1().getPointIdx();
			int DOFIdx = getRodMeshModel(modelIdx)->getNodeRawDOF(nodeIdx)[0];

			vmasterRod[modelIdx].push_back(vpCouplingsDOF[i]->getModelPoint1().getPointIdx());

			vpCouplingsDOF[i]->getModelPoint1().setPointIdx(DOFIdx);
		}
		else
		{
			vmasterMem[m1 - this->m_surfaceOffset].push_back(vpCouplingsDOF[i]->getModelPoint1().getPointIdx());
			vpCouplingsDOF[i]->getModelPoint1().setPointIdx(vpCouplingsDOF[i]->getModelPoint1().getPointIdx()*3);
		}
	}

	// Recompute rest-state considering coupling points

	for (int i = 0; i < this->getNumSurfaceModels(); ++i)
	{
		SurfaceModel* pModel = this->getSurfaceModel(i);

		VectorXd vb0, vc0;
		pModel->configureHarmonicRest(true, vmasterMem[i]);
		pModel->getParam_BoundaryRest(vb0);
		pModel->setParam_BoundaryRest(vb0);
		pModel->getParam_CouplingRest(vc0);
		pModel->setParam_CouplingRest(vc0);
	}

	CompositeModel::configureModels(vpModels, vpCouplingsDOF);

	CompositeModel::setup();

	this->updateMapMatrices();

	logSimu("[TRACE] Initialized TensileStructureModel. Raw: %d, Sim: %d", this->m_nrawDOF, this->m_nsimDOF_x);
}

void TensileStructureModel::setStateFromMesh(
	vector<RodMesh*> vpRodMeshx,
	vector<RodMesh*> vpRodMesh0,
	vector<TriMesh*> vpTriMesh)
{
	for (int i = 0; i < this->getNumRodMeshModels(); ++i)
	{
		this->getRodMeshModel(i)->setStateToMesh_x(*vpRodMeshx[i]);
		this->getRodMeshModel(i)->setStateToMesh_0(*vpRodMesh0[i]);
	}

	for (int i = 0; i < this->getNumSurfaceModels(); ++i)
	{
		SurfaceModel2DRest* pSurModel = static_cast<SurfaceModel2DRest*>(this->getSurfaceModel(i));
		pSurModel->setStateToMesh(*vpTriMesh[i]);
	}

	for (int i = 0; i < this->getNumModel(); ++i)
	{
		this->mapLocal2GlobalSim_x(i, this->m_vpModels[i]->getPositions_x(), this->m_state_x->m_vx);
	}

	this->deprecateDeformation();
	this->deprecateUndeformed();
}

void TensileStructureModel::updateMapMatrices()
{
	const iVector& vmapRaw2Sim = this->getRawToSim_x();

	this->m_vmSim2Mem.resize(this->m_vpSurfaceModel.size());
	this->m_vmSim2Rod.resize(this->m_vpRodMeshModel.size());

	int numSim = this->getNumSimDOF_x();

	// Surface model

	for (int i = 0; i < (int) this->m_vpSurfaceModel.size(); ++i)
	{
		// Get DOF to membrane selection matrix

		SurfaceModel* pSModel = this->getSurfaceModel(i);
		int rawOff = this->getSurfaceModelRawOffset(i);

		int numRaw = pSModel->getNumRawDOF();

		// Build selection matrices

		vector<iVector> vmapSim2Mem(numSim);
		for (int j = 0; j < numRaw; ++j)
			vmapSim2Mem[vmapRaw2Sim[rawOff + j]].push_back(j);
			
		buildSelectionMatrix(vmapSim2Mem, m_vmSim2Mem[i], true);
	}

	// Rod mesh model

	for (int i = 0; i < (int) this->m_vpRodMeshModel.size(); ++i)
	{
		// Get DOF to membrane selection matrix

		RodMeshModel* pRModel = this->getRodMeshModel(i);
		int rawOff = this->getRodMeshModelRawOffset(i);

		int numRaw = pRModel->getNumRawDOF();

		// Build selection matrices

		vector<iVector> vmapSim2Rod(numSim);
		for (int j = 0; j < numRaw; ++j)
			vmapSim2Rod[vmapRaw2Sim[rawOff + j]].push_back(j);

		buildSelectionMatrix(vmapSim2Rod, m_vmSim2Rod[i], true);
	}
}

void TensileStructureModel::deprecateConfiguration()
{
	CompositeModel::deprecateConfiguration();

	// Nothing to do here..
}

void TensileStructureModel::deprecateDiscretization()
{
	CompositeModel::deprecateDiscretization();

	// Nothing to do here...
}

bool TensileStructureModel::remeshFromCouplings()
{
	int numRod = (int) this->m_vpRodMeshModel.size();
	int numMem = (int) this->m_vpSurfaceModel.size();

	for (int i = 0; i < numMem; ++i)
		if (!this->remeshFromCouplings_Surfaces(i))
			return false;

	for (int i = 0; i < numRod; ++i)
		if (!this->remeshFromCouplings_RodMeshes(i))
			return false;

	return true;
}

bool TensileStructureModel::remeshFromCouplings_RodMeshes(int ri)
{
	// TODO: Compute intersections in deformation space
	// bewteen embedded rod edges and membrane boundary
	// curves... Remesh rod meshes using intersections
	// as new constraints and modify the coupling point
	// list accordingly...

	return true;
}

bool TensileStructureModel::remeshFromCouplings_Surfaces(int mi)
{
	int numCou = (int) this->m_vpTSCouplings.size();

	// Partition couplings depending on the membrane they are
	// affecting, the couplings are indexed by a pair where:
	//		First: The model number of the rod mesh
	//		Second: The index number of the rod mesh

	map<pair<int, int>, ModelCoupling*> vmc;

	for (int i = 0; i < numCou; ++i)
	{
		int m0 = this->m_vpTSCouplings[i]->getModel0();
		int m1 = this->m_vpTSCouplings[i]->getModel1();

		if (m0 != mi && m1 != mi)
			continue; // Ignore

		if (m0 == mi) // Is this the membrane?
		{
			if (this->m_vpTSCouplings[i]->getModelPoint1().isExplicit())
				vmc[pair<int, int>(m1, this->m_vpTSCouplings[i]->getModelPoint1().getPointIdx())] = this->m_vpTSCouplings[i];
			else
			{
				logSimu("[WARNING] Not explicit model point");
			}
		}
		if (m1 == mi) // Is this the membrane?
		{
			if (this->m_vpTSCouplings[i]->getModelPoint0().isExplicit())
				vmc[pair<int, int>(m0, this->m_vpTSCouplings[i]->getModelPoint0().getPointIdx())] = this->m_vpTSCouplings[i];
			else
			{
				logSimu("[WARNING] Not explicit model point");
			}
		}
	}

	// Prepare the set of remeshing constraints due to couplings. 
	//		- Input nodes vector contains membrane model points
	//		- Input edges vector contains indices of membrane
	//		  model points sharing an edge at the corresponding
	//		  rod mesh.

	pVector vnodesI;
	iVector vedgesI;

	// Add coupling vertices / edges

	// Map coupling points to the corresponding index of 
	// remeshing node constraints to be referenced by edge
	// constraints.

	map<ModelCoupling*, int> vindexC;

	int countV = 0;

	for (map<pair<int, int>, ModelCoupling*>::iterator mp_it = vmc.begin(); mp_it != vmc.end(); ++mp_it)
	{
		pair<int, int> modelNode = mp_it->first;
		ModelCoupling* pCoupling = mp_it->second;

		ModelPoint* pModelPoint = NULL;
		if (pCoupling->getModel0() == mi) pModelPoint = &pCoupling->getModelPoint0();
		if (pCoupling->getModel1() == mi) pModelPoint = &pCoupling->getModelPoint1();

		// Add coupling point to remesh
		vnodesI.push_back(*pModelPoint);
		vindexC[pCoupling] = countV++;

		if (modelNode.first < this->m_rodMeshOffset)
			continue; // Not a rod mesh so no edges

		const RodMesh& rodMesh = this->m_vpRodMeshModel[modelNode.first - this->m_rodMeshOffset]->getSimMesh_x();

		// Check surrounding rod nodes (edges)
		int rodIdx, nodeIdx, nodeIdxp, nodeIdxn;
		rodMesh.getRodNodeForMeshNode(modelNode.second, rodIdx, nodeIdx);
		rodMesh.getRod(rodIdx).getCurve().getNodeNeighborNodes(nodeIdx, nodeIdxp, nodeIdxn);

		// Check prev. node
		if (nodeIdxp != -1)
		{
			int meshNodep;
			rodMesh.getMeshNodeForRodNode(nodeIdxp, rodIdx, meshNodep);
			map<pair<int, int>, ModelCoupling*>::iterator find_it = vmc.find(pair<int, int>(modelNode.first, meshNodep));
			if (find_it != vmc.end() && vindexC.find(find_it->second) != vindexC.end())
			{
				vedgesI.push_back(vindexC[pCoupling]);
				vedgesI.push_back(vindexC[find_it->second]);
			}
		}

		// Check next node
		if (nodeIdxn != -1)
		{
			int meshNoden;
			rodMesh.getMeshNodeForRodNode(nodeIdxn, rodIdx, meshNoden);
			map<pair<int, int>, ModelCoupling*>::iterator find_it = vmc.find(pair<int, int>(modelNode.first, meshNoden));
			if (find_it != vmc.end() && vindexC.find(find_it->second) != vindexC.end())
			{
				vedgesI.push_back(vindexC[pCoupling]);
				vedgesI.push_back(vindexC[find_it->second]);
			}
		}
	}

	// Remesh considering node and edge constratins together
	// with the corresponding tolerances, then remap points
	// to their new index as those might have changed.

	pVector vpointsExtO;

	int numV = -1;
	Real minA = 0.0;
	Real toleranceI = this->getSurfaceModel(mi)->getRemeshInternalTolerance();
	Real toleranceB = this->getSurfaceModel(mi)->getRemeshBoundaryTolerance();

	this->getSurfaceModel(mi)->remeshDelaunay(vnodesI, vedgesI, vpointsExtO, numV, minA, toleranceI, toleranceB);

	// Reallocate model point indices

	for (map<ModelCoupling*, int>::iterator mc_it = vindexC.begin(); mc_it != vindexC.end(); ++mc_it)
	{
		ModelCoupling* pCoupling = mc_it->first;
		if (mi == pCoupling->getModel0()) pCoupling->setModelPoint0(vpointsExtO[mc_it->second]);
		if (mi == pCoupling->getModel1()) pCoupling->setModelPoint1(vpointsExtO[mc_it->second]);
	}

	return true;
}

void TensileStructureModel::updateState0_Surface(int i, const VectorXd& vX)
{
	this->checkSurfaceModelIndex(i);

	//this->m_vpSurfaceModel[i]->setParam_Rest(vX, this->m_vpSurfaceModel[i]->getVelocities_0());
}

void TensileStructureModel::updateState0_RodMesh(int i, const VectorXd& vX)
{
	this->checkRodMeshModelIndex(i);

	//this->m_vpRodMeshModel[i]->setParam_Rest(vX, this->m_vpRodMeshModel[i]->getVelocities_0());
}

void TensileStructureModel::setTensileStructureCouplings(const vector<ModelCoupling*>& vpCoupling)
{
	// Delete previous

	int numTSCouplings = (int) this->m_vpTSCouplings.size();
	for (int i = 0; i < numTSCouplings; ++i)
		if (this->m_vpTSCouplings[i] != NULL)
			delete this->m_vpTSCouplings[i];
	this->m_vpTSCouplings.clear();

	// Make a new copy

	numTSCouplings = (int) vpCoupling.size();
	this->m_vpTSCouplings.resize(numTSCouplings);

	for (int i = 0; i < numTSCouplings; ++i)
	{
		this->m_vpTSCouplings[i] = new ModelCoupling(*vpCoupling[i]);

		//// NOTE: Temporal workaround because saved file is deprecated
		//this->m_vpTSCouplings[i]->getModelPoint0().setPointIdx(this->m_vpTSCouplings[i]->getModelPoint0().getPointIdx()/3);
		//this->m_vpTSCouplings[i]->getModelPoint1().setPointIdx(this->m_vpTSCouplings[i]->getModelPoint1().getPointIdx()/3);
	}

	this->deprecateConfiguration();
}

void TensileStructureModel::clearSurfaceModels()
{
	this->m_vpSurfaceModel.clear();

	this->deprecateConfiguration();
}

void TensileStructureModel::clearRodMeshModels()
{
	this->m_vpRodMeshModel.clear();

	this->deprecateConfiguration();
}

int TensileStructureModel::getNumSurfaceModels() const
{
	return (int) this->m_vpSurfaceModel.size();
}

int TensileStructureModel::getNumRodMeshModels() const
{
	return (int) this->m_vpRodMeshModel.size();
}

void TensileStructureModel::addSurfaceModel(SurfaceModel* pModel)
{
	assert(pModel != NULL);

	this->m_vpSurfaceModel.push_back(pModel);

	this->deprecateConfiguration();
}

void TensileStructureModel::addRodMeshModel(RodMeshModel* pModel)
{
	assert(pModel != NULL);

	this->m_vpRodMeshModel.push_back(pModel);

	this->deprecateConfiguration();
}

void TensileStructureModel::setSurfaceModel(int i, SurfaceModel* pModel)
{
	this->checkSurfaceModelIndex(i);

	assert(pModel != NULL);

	this->m_vpSurfaceModel[i] = pModel;

	this->deprecateConfiguration();
}

void TensileStructureModel::setRodMeshModel(int i, RodMeshModel* pModel)
{
	this->checkRodMeshModelIndex(i);

	assert(pModel != NULL);

	this->m_vpRodMeshModel[i] = pModel;

	this->deprecateConfiguration();
}

void TensileStructureModel::add_DfxDX_Surface(int i, tVector& vJ)
{
	throw new exception("DEPRECATED");

	//checkSurfaceModelIndex(i);

	//tVector vDfxDX_Surface;
	//vDfxDX_Surface.reserve(this->m_vpSurfaceModel[i]->getNumNonZeros_DfxD0());
	//this->m_vpSurfaceModel[i]->add_DfxD0(vDfxDX_Surface); // Local Jacobian

	//this->addLocal2Global_Row(this->m_surfaceOffset + i, vDfxDX_Surface, vJ);
}

void TensileStructureModel::add_fle_RodMesh(int i, VectorXd& vfe)
{
	throw new exception("DEPRECATED");

	//this->m_vpRodMeshModel[i]->add_fle(vfe); // Local force
}

void TensileStructureModel::add_flt_RodMesh(int i, VectorXd& vfl)
{
	throw new exception("DEPRECATED");

	//this->m_vpRodMeshModel[i]->add_flr(vfl); // Local force
}

void TensileStructureModel::add_fr_RodMesh(int i, VectorXd& vfr)
{
	throw new exception("DEPRECATED");

	//this->m_vpRodMeshModel[i]->add_frr(vfr); // Local force
}

void TensileStructureModel::add_DfxDle_RodMesh(int i, tVector& vJ)
{
	throw new exception("DEPRECATED");

	//tVector vDfDl_RodMesh;
	//vDfDl_RodMesh.reserve(this->m_vpRodMeshModel[i]->getNumNonZeros_DfxDle());
	//this->m_vpRodMeshModel[i]->add_DfxDle(vDfDl_RodMesh); // Local Jacobian

	//this->addLocal2Global_Row(this->m_rodMeshOffset + i, vDfDl_RodMesh, vJ);
}

void TensileStructureModel::add_DfxDlt_RodMesh(int i, tVector& vJ)
{
	throw new exception("DEPRECATED");

	//tVector vDfDl_RodMesh;
	//vDfDl_RodMesh.reserve(this->m_vpRodMeshModel[i]->getNumNonZeros_DfxDlr());
	//this->m_vpRodMeshModel[i]->add_DfxDlr(vDfDl_RodMesh); // Local Jacobian

	//this->addLocal2Global_Row(this->m_rodMeshOffset + i, vDfDl_RodMesh, vJ);
}

void TensileStructureModel::add_DfxDr_RodMesh(int i, tVector& vJ)
{
	throw new exception("DEPRECATED");

	//tVector vDfDr_RodMesh;
	//vDfDr_RodMesh.reserve(this->m_vpRodMeshModel[i]->getNumNonZeros_DfxDrr());
	//this->m_vpRodMeshModel[i]->add_DfxDrr(vDfDr_RodMesh); // Local Jacobian

	//this->addLocal2Global_Row(this->m_rodMeshOffset + i, vDfDr_RodMesh, vJ);
}

void TensileStructureModel::getEigenvaluesSpaceRepresentation(int idx, const vector<EigenPair>& veigenPairs, vector<Vector3d>& vp, vector<Vector3d>& vv)
{
	assert(idx >= 0 && idx < this->m_nsimDOF_x);

	VectorXd vxRawAll;
	VectorXd vxRawSurface;
	VectorXd vxRawRodMesh;
	this->mapSim2Raw(this->getPositions_x(), vxRawAll);
	this->mapGlobal2LocalRaw(0, vxRawAll, vxRawSurface);
	this->mapGlobal2LocalRaw(1, vxRawAll, vxRawRodMesh);

	const VectorXd& veigenVecMaxAll = veigenPairs[idx].m_veigenVec_r;
	VectorXd veigenVecMaxSurface;
	VectorXd veigenVecMaxRodMesh;
	this->mapGlobal2LocalRaw(0, veigenVecMaxAll, veigenVecMaxSurface);
	this->mapGlobal2LocalRaw(1, veigenVecMaxAll, veigenVecMaxRodMesh);

	// Surface arrows
	SurfaceModel* pSurfModel = this->getSurfaceModel(0);
	for (int i = 0; i < pSurfModel->getNumNodeRaw(); ++i)
	{
		iVector vnodeDOF = pSurfModel->getNodeRawDOF(i);

		vp.push_back(Vector3d(vxRawSurface(vnodeDOF[0]),
							  vxRawSurface(vnodeDOF[1]),
							  vxRawSurface(vnodeDOF[2])));

		vv.push_back(Vector3d(veigenVecMaxSurface[vnodeDOF[0]],
							  veigenVecMaxSurface[vnodeDOF[1]],
							  veigenVecMaxSurface[vnodeDOF[2]])*veigenPairs[idx].m_eigenVal_r);
	}

	// Rodmesh arrows
	RodMeshModel* pRodsModel = this->getRodMeshModel(0);
	for (int i = 0; i < pRodsModel->getNumNodeRaw(); ++i)
	{
		iVector vnodeDOF = pRodsModel->getNodeRawDOF(i);
		vp.push_back(Vector3d(vxRawRodMesh(vnodeDOF[0]),
							  vxRawRodMesh(vnodeDOF[1]),
							  vxRawRodMesh(vnodeDOF[2])));

		vv.push_back(Vector3d(veigenVecMaxRodMesh[vnodeDOF[0]],
							  veigenVecMaxRodMesh[vnodeDOF[1]],
							  veigenVecMaxRodMesh[vnodeDOF[2]])*veigenPairs[idx].m_eigenVal_r);
	}
}

iVector TensileStructureModel::getNodeRawDOF(int modelIdx, int nodeIdx)
{
	assert(modelIdx >= 0 && modelIdx < this->getNumModel());

	if (modelIdx < this->m_rodMeshOffset) 
	{
		SurfaceModel* pModel = this->getSurfaceModel(modelIdx - this->m_surfaceOffset);
		return pModel->getNodeRawDOF(nodeIdx);
	}
	else 
	{
		RodMeshModel* pModel = this->getRodMeshModel(modelIdx - this->m_rodMeshOffset);
		return pModel->getNodeRawDOF(nodeIdx);
	}
}

iVector TensileStructureModel::getNodeSimDOF_x(int modelIdx, int nodeIdx)
{
	assert(modelIdx >= 0 && modelIdx < this->getNumModel());

	if (modelIdx < this->m_rodMeshOffset)
	{
		SurfaceModel* pModel = this->getSurfaceModel(modelIdx - this->m_surfaceOffset);
		return pModel->getNodeSimDOF_x(nodeIdx);
	}
	else
	{
		iVector vrawIdx = this->getNodeRawDOF(modelIdx, nodeIdx);

		iVector vsimIdx;

		this->mapRaw2Sim(vrawIdx, vsimIdx);

		return vsimIdx;
	}
}

iVector TensileStructureModel::getNodeSimDOF_0(int modelIdx, int nodeIdx)
{
	assert(modelIdx >= 0 && modelIdx < this->getNumModel());

	if (modelIdx < this->m_rodMeshOffset)
	{
		SurfaceModel* pModel = this->getSurfaceModel(modelIdx - this->m_surfaceOffset);
		return pModel->getNodeSimDOF_0(nodeIdx);
	}
	else
	{
		assert(false); // TODO

		//RodMeshModel* pModel = this->getRodMeshModel(modelIdx - this->m_rodMeshOffset);
		//return pModel->getNodeSimDOF_0(nodeIdx);
		return iVector();
	}
}

void TensileStructureModel::getParam_LengthEdge(VectorXd& vl) const
{
	int numRodMesh = (int) this->m_vpRodMeshModel.size();

	int edgeCount = 0;
	for (int i = 0; i < numRodMesh; ++i) // Count total edges
		edgeCount += this->m_vpRodMeshModel[i]->getNumEdgeRaw();
	vl.resize(edgeCount);

	edgeCount = 0;
	for (int i = 0; i < numRodMesh; ++i)
	{
		int numEdges = this->m_vpRodMeshModel[i]->getNumEdgeRaw();

		VectorXd vlMesh;

		this->m_vpRodMeshModel[i]->getParam_LengthEdge(vlMesh);
		assert(numEdges == (int)vlMesh.size()); // Check size

		for (int j = 0; j < numEdges; ++j)
			vl[edgeCount++] = vlMesh[j];
	}
}

void TensileStructureModel::getParam_LengthTotal(VectorXd& vl) const
{
	int numRodMesh = (int) this->m_vpRodMeshModel.size();

	int rodCount = 0;
	for (int i = 0; i < numRodMesh; ++i) // Count total rods
		rodCount += this->m_vpRodMeshModel[i]->getNumRod();
	vl.resize(rodCount);

	rodCount = 0;
	for (int i = 0; i < numRodMesh; ++i)
	{
		int numRods = this->m_vpRodMeshModel[i]->getNumRod();

		VectorXd vlMesh;

		this->m_vpRodMeshModel[i]->getParam_LengthRod(vlMesh);
		assert(numRods == (int)vlMesh.size()); // Check size

		for (int j = 0; j < numRods; ++j)
			vl[rodCount++] = vlMesh[j];
	}
}

void TensileStructureModel::getParam_RadiusEdge(VectorXd& vr) const
{
	int numRodMesh = (int) this->m_vpRodMeshModel.size();

	int edgeCount = 0;
	for (int i = 0; i < numRodMesh; ++i) // Count total edges
		edgeCount += this->m_vpRodMeshModel[i]->getNumEdgeRaw();
	vr.resize(edgeCount);

	edgeCount = 0;
	for (int i = 0; i < numRodMesh; ++i)
	{
		int numEdges = this->m_vpRodMeshModel[i]->getNumEdgeRaw();

		VectorXd vrMesh;

		this->m_vpRodMeshModel[i]->getParam_RadiusEdge(vrMesh);
		assert(numEdges == (int)vrMesh.size()); // Check size

		for (int j = 0; j < numEdges; ++j)
			vr[edgeCount++] = vrMesh[j];
	}
}

void TensileStructureModel::setParam_LengthEdge(const VectorXd& vl)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int numRodMesh = (int) this->m_vpRodMeshModel.size();

	int edgeCount = 0;
	for (int i = 0; i < numRodMesh; ++i)
	{
		int numEdges = this->m_vpRodMeshModel[i]->getNumEdgeRaw();

		VectorXd vlMesh(numEdges);
		for (int j = 0; j < numEdges; ++j)
			vlMesh[j] = vl[edgeCount++];

		this->m_vpRodMeshModel[i]->setParam_LengthEdge(vlMesh);
	}

	this->m_isReady_Mass = false;
	this->deprecateDeformation();
}

void TensileStructureModel::setParam_LengthTotal(const VectorXd& vl)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int numRodMesh = (int) this->m_vpRodMeshModel.size();

	int rodCount = 0;
	for (int i = 0; i < numRodMesh; ++i)
	{
		int numRods = this->m_vpRodMeshModel[i]->getNumRod();

		VectorXd vlMesh(numRods);
		for (int j = 0; j < numRods; ++j)
			vlMesh[j] = vl[rodCount++];

		this->m_vpRodMeshModel[i]->setParam_LengthRod(vlMesh);
	}

	this->m_isReady_Mass = false;
	this->deprecateDeformation();
}

void TensileStructureModel::setParam_RadiusEdge(const VectorXd& vr)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int numRodMesh = (int) this->m_vpRodMeshModel.size();

	int edgeCount = 0;
	for (int i = 0; i < numRodMesh; ++i)
	{
		int numEdges = this->m_vpRodMeshModel[i]->getNumEdgeRaw();

		VectorXd vrMesh(numEdges);
		for (int j = 0; j < numEdges; ++j)
			vrMesh[j] = vr[edgeCount++];

		this->m_vpRodMeshModel[i]->setParam_RadiusEdge(vrMesh);
	}

	this->m_isReady_Mass = false;
	this->deprecateDeformation();
}

/* DEPRECATED ----------------------------------------------------------- */

//void TensileStructureModel::configureSurfaceMeshModel(SurfaceModel* pModel)
//{
//	assert(pModel != NULL);
//	this->m_isReady_Setup = false;
//	this->m_pSurfaceModel = pModel;
//}
//
//void TensileStructureModel::configureCageRodMeshModel(RodMeshModel* pModel)
//{
//	assert(pModel != NULL);
//	this->m_isReady_Setup = false;
//	this->m_pCageRodModel = pModel;
//}
//
//void TensileStructureModel::configureRopeRodMeshModel(RodMeshModel* pModel)
//{
//	assert(pModel != NULL);
//	this->m_isReady_Setup = false;
//	this->m_pRopeRodModel = pModel;
//}
//
//void TensileStructureModel::configureRodMeshAttachments(const vector<vector<RodAttachment>>& vattach)
//{
//	this->m_vCageAttach = vattach;
//	this->m_isReady_Setup = false;
//}

//void TensileStructureModel::setupEmbeddedSystem()
//{
//	//// Rods are attached at surface at every point
//
//	//const TriMesh& surfMesh = this->m_pSurfaceModel->getRawMesh();
//	//const RodMesh& cageMesh = this->m_pCageRodModel->getRawMesh();
//	//int nr = cageMesh.getNumRod();
//
//	//vector<vector<MeshTraits>> vedgeIntersection(nr);
//
//	//// Remesh surface mesh, if needed
//
//	////if (this->m_remeshSurfaceAtAttachments)
//	////{
//	////	this->m_surfMeshContour.setContourEdges(true);
//	////	this->m_surfMeshContour.setContourNodes(true);
//
//	////	if (this->m_surfMeshContour.makeContouring(surfMesh, cageMesh))
//	////	{
//	////		const ContouringOP& result = this->m_surfMeshContour.getResult();
//
//	////		// Configure surface mesh using the result 
//	////		const TriMesh& surfMeshNew = *result.m_pTriMesh;
//	////		this->m_pSurfaceModel->configureMesh(surfMeshNew);
//
//	////		// Remesh cage rod mesh, if needed
//
//	////		if (this->m_remeshRodMeshAtAttachments)
//	////		{
//	////			dVector bbRanges = surfMeshNew.getAABBRanges();
//	////			double max = -HUGE_VAL;
//	////			for (int i = 0; i < 3; ++i)
//	////				if (bbRanges[i] > max)
//	////					max = bbRanges[i];
//
//	////			double intersectEPS = this->m_surfMeshContour.getIntersecEpsilon()*max;
//	////			const vector<MeshTraits::ID>& vcontourVerts = result.m_remOP.m_vAddVert;
//
//	////			int nv = (int)vcontourVerts.size();
//
//	////			// New mesh rod vector
//	////			vector<Rod> vrodsNew;
//
//	////			for (int r = 0; r < nr; ++r)
//	////			{
//	////				Rod rod = cageMesh.getRod(r);
//	////				Curve curve = rod.getCurve();
//
//	////				for (int i = 0; i < nv; ++i)
//	////				{
//	////					Vector3d pori = surfMeshNew.getPoint(surfMeshNew.getVertHandle(vcontourVerts[i]));
//
//	////					Real pD;
//	////					Real pt;
//	////					Vector3d ppro;
//	////					if (curve.projectPoint(pori, ppro, pD, pt, intersectEPS))
//	////					{
//	////						int vi, ei;
//	////						Vector3d newpoint = curve.samplePosition(pt, vi, ei, EPS_POS);
//	////						if (vi != -1)
//	////							continue;
//
//	////						// Projected point at edge: insert
//	////						curve.insertAtEdge(ei, newpoint);
//	////					}
//	////				}
//
//	////				// Re-initialize rod of cage mesh
//
//	////				int ne = curve.getNumEdge();
//
//	////				vector<Vector2d> vr(ne);
//	////				vr[0] = rod.getRadius(0);
//	////				for (int e = 1; e < ne; ++e)
//	////					vr[e] = vr[0]; // Copy
//
//	////				vector<Frame> vF(ne); // Paralell transport first
//	////				Curve::FramePTForward(rod.getFrame(0), curve, vF);
//
//	////				// Initialize from curve + frames
//	////				rod.initialize(curve, vF, vr);
//
//	////				// Push-back new mesh rod
//	////				vrodsNew.push_back(rod);
//	////			}
//
//	////			RodMesh cageMeshNew;
//	////			cageMeshNew.initialize(vrodsNew);
//	////			this->m_pCageRodModel->configureMesh(cageMeshNew);
//	////		}
//	////	}
//	////	else
//	////	{
//	////		logSimu("[ERROR] I was unable to contour surface raw mesh");
//	////	}
//	////}
//
//	//const TriMesh& surfMeshRaw = this->m_pSurfaceModel->getRawMesh();
//
//	//RodMesh cageMeshRaw = this->m_pCageRodModel->getRawMesh();
//
//	////vector<vector<TriMesh::EdgeHandle>> vsortBoundEdges;
//	////vector<vector<TriMesh::VertexHandle>> vsortBoundNodes;
//	////surfMeshRaw.getSortedBoundaryLoopsVertices(vsortBoundEdges, vsortBoundNodes);
//	////
//	////vector<Rod> vcageRods = cageMeshRaw.getRods();
//
//	////// Add a rod for each of the boundaries
//	////int nb = (int) vsortBoundEdges.size();
//	////for (int i = 0; i < nb; ++i)
//	////{
//	////	int nn = (int) vsortBoundNodes[i].size();
//
//	////	dVector vposBoundCurve(3*nn);
//	////	for (int j = 0; j < nn; ++j)
//	////	{
//	////		TriMesh::VertexHandle vh = vsortBoundNodes[i][j];
//	////		set3D(j, surfMeshRaw.getPoint(vh), vposBoundCurve);
//	////	}
//
//	////	vcageRods.push_back(Rod(Curve(vposBoundCurve, true)));
//	////}
//
//	////cageMeshRaw.initialize(vcageRods); // Initialize
//	////this->m_pCageRodModel->configureMesh(cageMeshRaw);
//
//	//this->m_pSurfaceModel->setup();
//	//this->m_pCageRodModel->setup();
//
//	//nr = cageMeshRaw.getNumRod();
//	//this->m_vCageAttach.resize(nr);
//	//for (int i = 0; i < nr; ++i)
//	//{
//	//	const Rod& rod_i = cageMeshRaw.getRod(i);
//	//	const Curve& curve_i = rod_i.getCurve();
//
//	//	dVector vt_i = curve_i.getVertexParameters();
//
//	//	int nn = curve_i.getNumNode();
//	//	this->m_vCageAttach[i].resize(nn);
//	//	for (int j = 0; j < nn; ++j)
//	//	{
//	//		this->m_vCageAttach[i][j].m_vertIdx = j;
//	//		this->m_vCageAttach[i][j].m_arcLen = vt_i[j];
//	//		this->m_vCageAttach[i][j].m_type = AttachmentType::Fixed;
//	//	}
//	//}
//}
//
//void TensileStructureModel::setupAttachedSystem()
//{
//	//// Resample cage rods to consider attachments to the surface
//	//RodMesh cageMeshRaw = this->m_pCageRodModel->getRawMesh();
//
//	//int nr = cageMeshRaw.getNumRod();
//
//	//for (int i = 0; i < nr; ++i)
//	//{
//	//	Rod& rod_i = cageMeshRaw.getRod(i);
//	//	Curve curve_i = rod_i.getCurve();
//
//	//	int nAttach = (int) this->m_vCageAttach[i].size();
//	//	for (int j = 0; j < nAttach; ++j) // Attachment
//	//	{
//	//		const RodAttachment& ra = this->m_vCageAttach[i][j];
//
//	//		if (ra.m_arcLen >= 0.0)
//	//		{
//	//			int vidx, eidx;
//	//			Vector3d p = curve_i.samplePosition(ra.m_arcLen, vidx, eidx);
//
//	//			if (vidx != -1)
//	//			{
//	//				// The point is exactly at a vertex: use it
//	//				this->m_vCageAttach[i][j].m_vertIdx = vidx;
//	//			}
//	//			else
//	//			{
//	//				// The point is situated along some of the curve edges: insert new
//	//				this->m_vCageAttach[i][j].m_vertIdx = curve_i.insertAtEdge(eidx, p);
//	//			}
//	//		}
//	//		else if (ra.m_vertIdx >= 0)
//	//		{
//	//			this->m_vCageAttach[i][j].m_arcLen = curve_i.getVertexParameters()[ra.m_vertIdx];
//	//		}
//	//		else
//	//		{
//	//			assert(false); // Either one or the other must be provided
//	//		}
//	//	}
//
//	//	int ne_i = curve_i.getNumEdge();
//
//	//	vector<Vector2d> vr_i(ne_i);
//	//	vr_i[0] = rod_i.getRadius(0);
//	//	for (int e = 1; e < ne_i; ++e)
//	//		vr_i[e] = vr_i[0]; // Same
//
//	//	vector<Frame> vF_i(ne_i); // Paralell transport first frame
//	//	Curve::FramePTForward(rod_i.getFrame(0), curve_i, vF_i);
//
//	//	// Initialize rod from curve + frames
//	//	rod_i.initialize(curve_i, vF_i, vr_i);
//
//	//	cageMeshRaw.setRod(i, rod_i);
//	//}
//
//	//// Discretization might have changed, reconfigure
//	//this->m_pCageRodModel->configureMesh(cageMeshRaw);
//
//	//this->m_pCageRodModel->setup();
//
//	////// Remesh surface mesh, if needed
//
//	////const TriMesh& surfMesh = this->m_pSurfaceModel->getRawMesh();
//	////const RodMesh& cageMesh = this->m_pCageRodModel->getRawMesh();
//
//	////if (this->m_remeshSurfaceAtAttachments)
//	////{
//	////	this->m_surfMeshContour.setContourNodes(true);
//	////	this->m_surfMeshContour.setContourEdges(false);
//
//	////	if (this->m_surfMeshContour.makeContouring(surfMesh, cageMesh))
//	////	{
//	////		const ContouringOP& result = this->m_surfMeshContour.getResult();
//
//	////		// Configure surface mesh using the result 
//	////		const TriMesh& surfMeshNew = *result.m_pTriMesh;
//	////		this->m_pSurfaceModel->configureMesh(surfMeshNew);
//	////		
//	////		this->m_pSurfaceModel->setup();
//	////	}
//	////	else
//	////	{
//	////		logSimu("[ERROR] I was unable to contour surface raw mesh");
//	////	}
//	////}
//}

//void TensileStructureModel::setup()
//{
//	assert(this->m_pSurfaceModel != NULL);
//	assert(this->m_pCageRodModel != NULL);
//	//assert(this->m_pRopeRodModel != NULL);
//
//	const RodMesh& cageMeshRaw = this->m_pCageRodModel->getRawMesh();
//
//	int nRod = cageMeshRaw.getNumRod();
//
//	int nAtt = (int) this->m_vCageAttach.size();
//	if (nAtt == 0) // No attachments provided
//	{
//		setupEmbeddedSystem();
//	}
//	else
//	{
//		assert(nAtt == nRod);
//		setupAttachedSystem();
//	}
//
//	// Create model vector
//
//	vector<SolidModel*> vpModels;
//	vpModels.push_back(this->m_pSurfaceModel);
//	vpModels.push_back(this->m_pCageRodModel);
//	//vpModels.push_back(this->m_pRopeRodModel);
//
//	// Create DOF couplings
//
//	vector<IdentityDOFCoupling*> vpICouplings;
//	vector<EmbeddingDOFCoupling*> vpECouplings;
//	this->computeIdentityCouplings(vpICouplings);
//	this->computeEmbeddingCouplings(vpECouplings);
//
//	CompositeModel::configureModels(vpModels, vpICouplings, vpECouplings);
//
//	CompositeModel::setup();
//}

//void TensileStructureModel::precompute_DfxDX_Surf()
//{
//	tVector testJ;
//	this->add_DfxDX_Surf(testJ); // Precompute
//	this->m_nNZ_DfxDX_Surf = (int)testJ.size();
//}
//
//void TensileStructureModel::precompute_DfxDl_Cage()
//{
//	tVector testJ;
//	this->add_DfxDL_Cage(testJ); // Precompute
//	this->m_nNZ_DfxDl_Cage = (int)testJ.size();
//}
//
//void TensileStructureModel::precompute_DfxDr_Cage()
//{
//	tVector testJ;
//	this->add_DfxDr_Cage(testJ); // Precompute
//	this->m_nNZ_DfxDr_Cage = (int)testJ.size();
//}
//
//void TensileStructureModel::update_DfxDX_Surf()
//{
//	this->m_pSurfaceModel->update_DfxD0();
//}
//
//void TensileStructureModel::update_DfxDl_Cage()
//{
//	this->m_pCageRodModel->update_DfxDlt();
//}
//
//void TensileStructureModel::update_DfxDr_Cage()
//{
//	this->m_pCageRodModel->update_DfxDr();
//}

//void TensileStructureModel::add_DfxDX_Surf(tVector& vJ)
//{
//	tVector vDfxDX_Surf;
//	vDfxDX_Surf.reserve(this->m_pSurfaceModel->getNumNonZeros_DfxD0());
//	this->m_pSurfaceModel->add_DfxD0(vDfxDX_Surf); // Get local Jacobian
//
//	this->addLocal2Global_Row(0, vDfxDX_Surf, vJ); // Transform to global DOF
//}
//
//void TensileStructureModel::add_DfxDL_Cage(tVector& vJ)
//{
//	tVector vDfDl_Cage;
//	vDfDl_Cage.reserve(this->m_pCageRodModel->getNumNonZeros_DfxDlr());
//	this->m_pCageRodModel->add_DfxDlr(vDfDl_Cage); // Get local Jacobian
//
//	this->addLocal2Global_Row(1, vDfDl_Cage, vJ); // Transform to gobal DOF
//}
//
//void TensileStructureModel::add_DfxDr_Cage(tVector& vJ)
//{
//	tVector vDfDr_Cage;
//	vDfDr_Cage.reserve(this->m_pCageRodModel->getNumNonZeros_DfxDrr());
//	this->m_pCageRodModel->add_DfxDr(vDfDr_Cage); // Get local Jacobian
//
//	this->addLocal2Global_Row(1, vDfDr_Cage, vJ); // Transform to gobal DOF
//}
//
//void TensileStructureModel::test_DfxDX_Surf()
//{
//	int nsimDOF0_Surf = this->m_pSurfaceModel->getSimDOFNumber0();
//
//	double EPS = 1e-6;
//	VectorXd vX_Surf = this->m_pSurfaceModel->getPositions0();
//	VectorXd vV_Surf = this->m_pSurfaceModel->getVelocities0();
//	VectorXd vXFD_Surf = vX_Surf;
//	VectorXd vVFD_Surf = vV_Surf;
//	MatrixXd mJFD(this->m_nsimDOF_x, nsimDOF0_Surf);
//	mJFD.setZero();
//	for (int j = 0; j < nsimDOF0_Surf; ++j)
//	{
//		// +
//		vXFD_Surf(j) += EPS;
//		VectorXd vfp(this->m_nsimDOF_x);
//		this->updateState0_Surf(vXFD_Surf, vVFD_Surf);
//		vfp.setZero();
//		addForce(vfp);
//
//		// -
//		vXFD_Surf(j) -= 2 * EPS;
//		VectorXd vfm(this->m_nsimDOF_x);
//		this->updateState0_Surf(vXFD_Surf, vVFD_Surf);
//		vfm.setZero();
//		addForce(vfm);
//
//		// Estimate
//		VectorXd vfFD = (vfp - vfm) / (2 * EPS);
//		for (int i = 0; i < this->m_nsimDOF_x; ++i)
//			mJFD(i, j) = vfFD(i); // Set Jacobian
//
//		vXFD_Surf(j) += EPS;
//	}
//
//	// Get analytic
//	this->updateState0_Surf(vX_Surf, vV_Surf);
//	tVector vJA;
//	this->add_DfxDX_Surf(vJA);
//
//	SparseMatrixXd mJASparse(m_nsimDOF_x, nsimDOF0_Surf);
//	mJASparse.setFromTriplets(vJA.begin(), vJA.end());
//	mJASparse.makeCompressed();
//
//	MatrixXd mJA = MatrixXd(mJASparse);
//
//	// Compare
//	MatrixXd mJDiff = (mJA - mJFD);
//	Real FDNorm = mJFD.norm();
//	Real diffNorm = mJDiff.norm() / FDNorm;
//
//	if (FDNorm < 1e-9)
//		diffNorm = 0.0;
//
//	if (diffNorm > 1e-9)
//	{
//		writeToFile(mJA, "JacobianA.csv"); // Trace
//		writeToFile(mJFD, "JacobianFD.csv"); // Trace
//		writeToFile(mJDiff, "JacobianFDError.csv"); // Trace
//		logSimu("[ERROR] Model: %s. Jacobian test error (global): %.9f\n", this->m_ID.c_str(), diffNorm);
//	}
//
//	this->update_Force(); // Recover force vector
//}

//void TensileStructureModel::precompute_DfxDX_Surface(int i)
//{
//	checkSurfaceModelIndex(i);
//
//	tVector testJ;
//	this->add_DfxDX_Surface(i, testJ); // Precompute
//	this->m_vnNZ_DfxDX_Surface[i] = (int)testJ.size();
//}
//
//void TensileStructureModel::precompute_DfxDl_RodMesh(int i)
//{
//	checkRodMeshModelIndex(i);
//
//	tVector testJ;
//	this->add_DfxDl_RodMesh(i, testJ); // Precompute
//	this->m_vnNZ_DfxDl_RodMesh[i] = (int)testJ.size();
//}
//
//void TensileStructureModel::precompute_DfxDr_RodMesh(int i)
//{
//	checkRodMeshModelIndex(i);
//
//	tVector testJ;
//	this->add_DfxDr_RodMesh(i, testJ); // Precompute
//	this->m_vnNZ_DfxDr_RodMesh[i] = (int)testJ.size();
//}

//void TensileStructureModel::update_fl_RodMesh(int i)
//{
//	checkRodMeshModelIndex(i);
//
//	//this->m_vpRodMeshModel[i]->update_flt();
//}
//
//void TensileStructureModel::update_fr_RodMesh(int i)
//{
//	checkRodMeshModelIndex(i);
//
//	//this->m_vpRodMeshModel[i]->update_frr();
//}
//
//void TensileStructureModel::update_DfxDl_RodMesh(int i)
//{
//	checkRodMeshModelIndex(i);
//
//	this->m_vpRodMeshModel[i]->update_DfxDlt();
//}
//
//void TensileStructureModel::update_DfxDr_RodMesh(int i)
//{
//	checkRodMeshModelIndex(i);
//
//	this->m_vpRodMeshModel[i]->update_DfxDr();
//}

//void TensileStructureModel::update_DfxDX_Surface(int i)
//{
//	checkSurfaceModelIndex(i);
//
//	this->m_vpSurfaceModel[i]->update_DfxD0();
//}

void TensileStructureModel::computeCouplings(vector<ModelCoupling*>& vpCoupling)
{
	//vpECoupling.clear();

	//// Compute acceptable range for traversing the BVH tree
	//const TriMesh& surfMesh = this->m_vpSurfaceModel[0]->getRawMesh();
	//dVector surfMeshRanges = surfMesh.getAABBRanges(VProperty::POSE);
	//double maxRange = -HUGE_VAL;
	//for (int i = 0; i < 3; ++i)
	//	if (surfMeshRanges[i] > maxRange)
	//		maxRange = surfMeshRanges[i];

	//// Compute rod mesh embedding for each rod mesh model

	//for (int k = 0; k < this->getNumRodMeshModels(); ++k)
	//{
	//	vector<Vector3d> vrodsPointCloud;
	//	vector<PointProjection> vrodsPointProjection;
	//	const RodMesh& rodMesh = this->m_vpRodMeshModel[k]->getRawMesh();

	//	int numRods = rodMesh.getNumRod();
	//	for (int i = 0; i < numRods; ++i)
	//	{
	//		const Rod& rod = rodMesh.getRod(i); // Get i-th
	//		int nAttach = (int) this->m_vRodAttach[k][i].size();

	//		for (int j = 0; j < nAttach; ++j) // Add attachments
	//		{
	//			const RodAttachment& ra = this->m_vRodAttach[k][i][j];
	//			vrodsPointCloud.push_back(rod.getPosition(ra.m_vertIdx));
	//		}
	//	}

	//	if (vrodsPointCloud.size() == 0)
	//		return; // No attachments

	//	// TODO: Max. range is set here to an acceptable distance considering the point should be lying 
	//	// on the surface. To consider also attachments that might be separated originally and so joint
	//	// through a wire, this value should be tweaked.
	//	surfMesh.getPointsEmbedding(vrodsPointCloud, 1e-3*maxRange, vrodsPointProjection, VProperty::POSE);

	//	vector<Rod> vnewRods;

	//	int count = 0;
	//	for (int i = 0; i < numRods; ++i)
	//	{
	//		const Rod& rod = rodMesh.getRod(i); // Get the i-th rod
	//		const Curve& curve = rod.getCurve(); // Get the i-th curve
	//		int nAttach = (int) this->m_vRodAttach[k][i].size();

	//		Curve newCurve(curve);

	//		for (int j = 0; j < nAttach; ++j)
	//		{
	//			const RodAttachment& ra = this->m_vRodAttach[k][i][j];
	//			const PointProjection& pp = vrodsPointProjection[count++];

	//			if (pp.m_valid)
	//			{
	//				if (ra.m_type == AttachmentType::Fixed)
	//				{
	//					// Add an embedding coupling for each of the node degrees-of-freedom
	//					iVector vDOFs = this->m_vpRodMeshModel[k]->getNodeRawDOF(i, ra.m_vertIdx);

	//					// DEBUG check: the originally projected point might be the one at the raw rod curve
	//					assert((curve.getPosition(ra.m_vertIdx) - pp.m_oriPoint).squaredNorm() < EPS_APPROX);

	//					Vector3d v0 = surfMesh.getPoint(surfMesh.vertex_handle(pp.m_embedding.m_vidx[0]), VProperty::POSE);
	//					Vector3d v1 = surfMesh.getPoint(surfMesh.vertex_handle(pp.m_embedding.m_vidx[1]), VProperty::POSE);
	//					Vector3d v2 = surfMesh.getPoint(surfMesh.vertex_handle(pp.m_embedding.m_vidx[2]), VProperty::POSE);
	//					Vector3d p = v0*pp.m_embedding.m_vwei[0] + v1*pp.m_embedding.m_vwei[1] + v2*pp.m_embedding.m_vwei[2];

	//					//assert((curve.getPosition(ra.m_vertIdx) - p).squaredNorm() < EPS_APPROX);

	//					// Modify the curve position to the projected
	//					newCurve.setPosition(ra.m_vertIdx, pp.m_proPoint);

	//					iVector vidx = pp.m_embedding.m_vidx;
	//					dVector vwei = pp.m_embedding.m_vwei;

	//					for (int d0 = 0; d0 < (int)vDOFs.size(); ++d0)
	//					{
	//						EmbeddingDOFCoupling* pCoup = new EmbeddingDOFCoupling();
	//						pCoup->m_model1 = 0; // Surface mesh
	//						pCoup->m_model0 = 1; // Cage rod mesh
	//						pCoup->m_idx0 = vDOFs[d0];
	//						for (int d1 = 0; d1 < (int)vidx.size(); ++d1)
	//						{
	//							pCoup->m_vwei1.push_back(vwei[d1]); // Bari.
	//							pCoup->m_vidx1.push_back(3 * vidx[d1] + d0);
	//						}
	//						vpECoupling.push_back(pCoup);
	//					}
	//				}
	//				else if (ra.m_type == AttachmentType::Wired)
	//				{
	//					// TODO: Implement wired attachments
	//				}
	//				else assert(false); // I should not be here :(
	//			}
	//		}

	//		// Create the new rod, same radii vector, recompute adapted frames
	//		vnewRods.push_back(Rod(newCurve, vector<Frame>(), rod.getRadius()));
	//	}

	//	// Reconfigure rod mesh model to consider embedding changes
	//	this->m_vpRodMeshModel[k]->configureMesh(RodMesh(vnewRods));
	//}
}



void TensileStructureModel::preSetupEmbeddedRodMesh(int iMesh)
{
	//this->checkRodMeshModelIndex(iMesh);

	//// TODO
	//// By default it is embedded in the first
	//// surface mesh. This should be improved to
	//// specify somehow the embedding surface.

	//int iSurf = 0;

	//// Remesh surface meshes, if needed

	//if (this->m_remeshSurfaceAtAttachments)
	//{
	//	const TriMesh& oldTriMesh = this->m_vpSurfaceModel[iSurf]->getRawMesh();
	//	const RodMesh& oldRodMesh = this->m_vpRodMeshModel[iMesh]->getRawMesh();

	//	// Embedded: contour both edges and nodes
	//	this->m_surfMeshContour.setContourEdges(true);
	//	this->m_surfMeshContour.setContourNodes(true);

	//	if (this->m_surfMeshContour.makeContouring(oldTriMesh, oldRodMesh))
	//	{
	//		const ContouringOP& result = this->m_surfMeshContour.getResult();

	//		// Configure the surface model to use the new triangle remeshed
	//		this->m_vpSurfaceModel[iSurf]->configureMesh(*result.m_pTriMesh);

	//		// Remesh also the rod mesh, if needed
	//		if (this->m_remeshRodMeshAtAttachments)
	//		{
	//			const TriMesh& newTriMesh = this->m_vpSurfaceModel[iSurf]->getRawMesh();

	//			dVector bbRanges = newTriMesh.getAABBRanges();
	//			double max = -HUGE_VAL;
	//			for (int i = 0; i < 3; ++i)
	//				if (bbRanges[i] > max)
	//					max = bbRanges[i];

	//			double intersectEPS = this->m_surfMeshContour.getIntersecEpsilon()*max;
	//			const vector<MeshTraits::ID>& vcontourVerts = result.m_remOP.m_vAddVert;

	//			int nv = (int)vcontourVerts.size();
	//			int nr = oldRodMesh.getNumRod();

	//			// New mesh rod vector
	//			vector<Rod> vnewRods;

	//			for (int r = 0; r < nr; ++r)
	//			{
	//				Rod rod = oldRodMesh.getRod(r);
	//				Curve curve = rod.getCurve();

	//				for (int i = 0; i < nv; ++i)
	//				{
	//					Vector3d pori = newTriMesh.getPoint(newTriMesh.getVertHandle(vcontourVerts[i]));

	//					Real pD;
	//					Real pt;
	//					Vector3d ppro;
	//					if (curve.projectPoint(pori, ppro, pD, pt, intersectEPS))
	//					{
	//						int vi, ei;
	//						Vector3d newpoint = curve.samplePosition(pt, vi, ei, EPS_POS);
	//						if (vi != -1)
	//							continue;

	//						// Projected point at edge: insert
	//						curve.insertAtEdge(ei, newpoint);
	//					}
	//				}

	//				// Re-initialize rod of cage mesh

	//				int ne = curve.getNumEdge();

	//				vector<Vector2d> vr(ne);
	//				vr[0] = rod.getRadius(0);
	//				for (int e = 1; e < ne; ++e)
	//					vr[e] = vr[0]; // Copy

	//				vector<Frame> vF(ne); // Paralell transport first
	//				Curve::FramePTForward(rod.getFrame(0), curve, vF);

	//				// Initialize: curve + frames
	//				rod.initialize(curve, vF, vr);

	//				// Push-back new mesh rod
	//				vnewRods.push_back(rod);
	//			}

	//			// Update rod mesh structure
	//			RodMesh newRodMesh(vnewRods);

	//			// Configure the rod mesh model with the new rod mesh
	//			this->m_vpRodMeshModel[iMesh]->configureMesh(newRodMesh);
	//		}
	//	}
	//	else
	//	{
	//		logSimu("[ERROR] I was unable to contour surface raw mesh %d for rod %d", iSurf, iMesh);
	//	}
	//}

	//// Configure embedded attachments (one for each rod mesh nodes)

	//const RodMesh& rodMesh = this->m_vpRodMeshModel[iMesh]->getRawMesh();

	//int nr = rodMesh.getNumRod();
	//this->m_vRodAttach[iMesh].resize(nr);
	//for (int i = 0; i < nr; ++i)
	//{
	//	const Rod& rod_i = rodMesh.getRod(i);
	//	const Curve& curve_i = rod_i.getCurve();

	//	dVector vt_i = curve_i.getVertexParameters();

	//	int nn = curve_i.getNumNode();
	//	this->m_vRodAttach[iMesh][i].resize(nn);
	//	for (int j = 0; j < nn; ++j)
	//	{
	//		this->m_vRodAttach[iMesh][i][j].m_vertIdx = j;
	//		this->m_vRodAttach[iMesh][i][j].m_arcLen = vt_i[j];
	//		this->m_vRodAttach[iMesh][i][j].m_vmodels.push_back(iSurf);
	//		this->m_vRodAttach[iMesh][i][j].m_type = AttachmentType::Fixed;
	//	}
	//}
}

void TensileStructureModel::preSetupAttachedRodMesh(int iMesh)
{
	//this->checkRodMeshModelIndex(iMesh);

	//// Rods are attached at surface at every point

	//vector<const TriMesh*> vnewTriMesh(this->getNumSurfaceModels());
	//for (int j = 0; j < this->getNumSurfaceModels(); ++j) // Transform
	//	vnewTriMesh[j] = &this->m_vpSurfaceModel[j]->getRawMesh();

	//RodMesh newRodMesh(this->m_vpRodMeshModel[iMesh]->getRawMesh());

	//int nr = newRodMesh.getNumRod();

	//for (int j = 0; j < nr; ++j)
	//{
	//	Rod& rod_j = newRodMesh.getRod(j);
	//	Curve curve_j = rod_j.getCurve();

	//	int nAttach = (int) this->m_vRodAttach[iMesh][j].size();
	//	for (int k = 0; k < nAttach; ++k) // Rod attachments
	//	{
	//		const RodAttachment& ra = this->m_vRodAttach[iMesh][j][k];

	//		if (ra.m_arcLen >= 0.0)
	//		{
	//			int vidx, eidx;
	//			Vector3d p = curve_j.samplePosition(ra.m_arcLen, vidx, eidx);

	//			if (vidx != -1)
	//			{
	//				// The point is exactly at a vertex: use it
	//				this->m_vRodAttach[iMesh][j][k].m_vertIdx = vidx;
	//			}
	//			else
	//			{
	//				// The point is situated along some of the curve edges: insert new
	//				this->m_vRodAttach[iMesh][j][k].m_vertIdx = curve_j.insertAtEdge(eidx, p);
	//			}
	//		}
	//		else if (ra.m_vertIdx >= 0)
	//		{
	//			this->m_vRodAttach[iMesh][j][k].m_arcLen = curve_j.getVertexParameters()[ra.m_vertIdx];
	//		}
	//		else
	//		{
	//			assert(false); // Either one or the other must be provided
	//		}
	//	}

	//	int ne_j = curve_j.getNumEdge();

	//	vector<Vector2d> vr_j(ne_j);
	//	vr_j[0] = rod_j.getRadius(0);
	//	for (int e = 1; e < ne_j; ++e)
	//		vr_j[e] = vr_j[0]; // Same

	//	vector<Frame> vF_j(ne_j); // Paralell transport first frame
	//	Curve::FramePTForward(rod_j.getFrame(0), curve_j, vF_j);

	//	// Initialize rod from curve + frames
	//	rod_j.initialize(curve_j, vF_j, vr_j);

	//	newRodMesh.setRod(j, rod_j);
	//}

	//// Discretization might have changed, reconfigure mesh
	//this->m_vpRodMeshModel[iMesh]->configureMesh(newRodMesh);

	//// Remesh surface meshes, if needed

	//if (this->m_remeshSurfaceAtAttachments)
	//{
	//	for (int j = 0; j < this->getNumSurfaceModels(); ++j)
	//	{
	//		this->m_surfMeshContour.setContourNodes(true);
	//		this->m_surfMeshContour.setContourEdges(false);

	//		if (this->m_surfMeshContour.makeContouring(*vnewTriMesh[j], newRodMesh))
	//		{
	//			const ContouringOP& result = this->m_surfMeshContour.getResult();

	//			// Configure surface mesh using the resulting remeshed surface
	//			this->m_vpSurfaceModel[j]->configureMesh(*result.m_pTriMesh);
	//		}
	//		else
	//		{
	//			logSimu("[ERROR] I was unable to contour surface raw mesh %d for rod %d", j, iMesh);
	//		}

	//		this->m_vpSurfaceModel[j]->configureMesh(*vnewTriMesh[j]);
	//	}
	//}
}
