/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_RodMeshRC.h
\author		jesusprod
\brief		TODO
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_TENSILE_HERMITE_RODMESH_RC_H
#define PARAMETER_SET_TENSILE_HERMITE_RODMESH_RC_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/Model/TensileStructureModel.h>

class JSANDBOX_EXPORT ParameterSet_Tensile_HermiteRodMeshRC : public ParameterSet
{
public:
	ParameterSet_Tensile_HermiteRodMeshRC(int ridx);
	virtual ~ParameterSet_Tensile_HermiteRodMeshRC();

	virtual iVector getPointNumber() const { return this->m_vcontrolNum; }
	virtual void setPointNumber(const iVector& vc) { this->m_vcontrolNum = vc; }

	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

	virtual void updateControlPoints();
	virtual void updateCouplingMaps();

	virtual void testHermiteJacobian() const;

	virtual Real computeParameterRegularizer_Potential(const VectorXd& vp);
	virtual void computeParameterRegularizer_Hessian(const VectorXd& vp, tVector& vH);
	virtual void computeParameterRegularizer_Gradient(const VectorXd& vp, VectorXd& vg);

	virtual const vector<VectorXd>& getControlVal() const { return this->m_vcontrolVal; }
	virtual const vector<VectorXd>& getControlPar() const { return this->m_vcontrolPar; }

protected:

	TensileStructureModel* m_pTSModel;

	virtual void concatenatedToRods(const VectorXd& vpAll, vector<VectorXd>& vpRod) const;
	virtual void rodsToConcatenated(const vector<VectorXd>& vpRod, VectorXd& vpAll) const;

	virtual void computeJac(SparseMatrixXd& mDpDm) const;

	virtual void computeJacRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, tVector& vDpDm, bool closed) const;
	virtual void computeValRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, VectorXd& vpInt, bool closed) const;

	int m_ridx;	// Rodmesh model index

	iVector m_vcontrolNum;
	vector<VectorXd> m_vcontrolVal;
	vector<VectorXd> m_vcontrolPar;

	iVector m_vmapSplit2Whole;

	iVector m_vnumB;
	iVector m_vnumC;
	vector<iVector> m_vmapBoundary2Rod;
	vector<iVector> m_vmapCoupling2Rod;

};

#endif