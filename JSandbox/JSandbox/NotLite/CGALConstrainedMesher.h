///*=====================================================================================*/
///*!
//\file		CGALConstrainedMesher.h
//\author		jesusprod
//\brief		TODO
//*/
///*=====================================================================================*/
//
//#ifndef CGAL_CONSTRAINED_MESHER_H
//#define CGAL_CONSTRAINED_MESHER_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//
//#include <JSandbox/MathUtils.h>
//
//#include <JSandbox/Geometry/TriMesh.h>
//#include <JSandbox/Geometry/BVHNode.h>
//
//
//class JSANDBOX_EXPORT CGALConstrainedMesher
//{
//
//public:
//	CGALConstrainedMesher();
//
//	virtual ~CGALConstrainedMesher();
//
//	virtual bool createConstrainedMesh(const dVector& vnodesI, const iVector& vedgesI, dVector& vnodesO, iVector& vfacesO, iVector& vnewCons, string& error);
//
//	virtual Real getMinimumAngle() const { return this->m_minAngle; }
//	virtual Real getMaximumLength() const { return this->m_maxLength; }
//	virtual int getVerticesCount() const { return this->m_verCount; }
//
//	virtual void setMinimumAngle(Real ma) { this->m_minAngle = ma; }
//	virtual void setMaximumLength(Real es) { this->m_maxLength = es; }
//	virtual void setVerticesCount(int vc) { this->m_verCount = vc; }
//
//
//protected:
//
//	Real m_minAngle;
//	Real m_maxLength;
//	int m_verCount;
//
//};
//
//#endif