/*=====================================================================================*/
/*!
\file		SurfaceRemesherContour.h
\author		jesusprod
\brief		Implementation of a surface remesher that refines a surface by introducing
			new vertices at those points a given number of input contours intersect 
			mesh edges.

*/
/*=====================================================================================*/


#ifndef SURFACE_REMESHER_CONTOUR_H
#define SURFACE_REMESHER_CONTOUR_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>

#include <JSandbox/Geometry/Curve.h>

#include <JSandbox/Geometry/SurfaceRemesher.h>

class JSANDBOX_EXPORT SurfaceRemesherContour : public SurfaceRemesher
{
public:

	SurfaceRemesherContour();
	virtual ~SurfaceRemesherContour();

	virtual const RemeshOP& remesh(const TriMesh* pMesh, const BVHTree* pEdgeTree = NULL, const BVHTree* pFaceTree = NULL);

	virtual int getContourNumber() const;
	
	virtual Real getEdgeMoveParameter() const { return this->m_edgeMovePara; }
	virtual Real getFaceMoveParameter() const { return this->m_faceMovePara; }
	virtual Real getIntersectEPS() const { return this->m_intersecEpsi; }
	virtual Real getBoundaryEPS() const { return this->m_boundaryEpsi; }

	virtual void setEdgeMoveParameter(Real m) { this->m_edgeMovePara = m; }
	virtual void setFaceMoveParameter(Real m) { this->m_faceMovePara = m; }
	virtual void setInsersectEPS(Real e) { this->m_intersecEpsi = e; }
	virtual void setBoundaryEPS(Real e) { this->m_boundaryEpsi = e; }

	virtual void setContourExtremes(bool cx) { this->m_contExtremes = cx; }
	virtual bool getContourExtremes() const { return this->m_contExtremes; }

	virtual void setContourEdges(bool cx) { this->m_contEdges = cx; }
	virtual bool getContourEdges() const { return this->m_contEdges; }

	virtual void setContourNodes(bool cx) { this->m_contNodes = cx; }
	virtual bool getContourNodes() const { return this->m_contNodes; }

	virtual void addContourCurve(const Curve& curve);

	virtual const vector<MeshTraits::ID>& getContourEdges(int i) const;
	virtual const vector<MeshTraits::ID>& getContourVerts(int i) const;

	virtual void computeContourEdges(int i, vector<MeshTraits::ID>& vcontour) const;

protected:

	virtual RemeshOP contourEdges(int i);
	virtual RemeshOP contourNodes(int i);
	virtual RemeshOP contourExtremes(int i);
	virtual RemeshOP moveMeshVertex(const MeshTraits::ID& vid, const Vector3d& p);
	virtual RemeshOP projectPointToFace(const Vector3d& A, const MeshTraits::ID& fid);
	virtual RemeshOP projectSegmentToEdge(const Vector3d& A, const Vector3d& B, const MeshTraits::ID& eid);

	vector<vector<MeshTraits::ID>> m_vVerts;
	vector<vector<MeshTraits::ID>> m_vEdges;

	vector<Curve> m_vrawContours;

	vector<MeshTraits::ID> m_vconflictNode;
	vector<MeshTraits::ID> m_vconflictEdge;

	bool m_contExtremes;
	bool m_contEdges;
	bool m_contNodes;

	double m_intersecEpsi;
	double m_boundaryEpsi;
	double m_boundaryEpsiScaled;
	double m_intersecEpsiScaled;
	double m_edgeMovePara;
	double m_faceMovePara;
	
	// Deprecated

	//vector<vector<MeshTraits::ID>> m_vSortVerts;
	//vector<vector<MeshTraits::ID>> m_vSortEdges;

	//virtual void getSortedContour(int i, const Curve& curveIn, vector<MeshTraits::ID>& vvertsOut, vector<MeshTraits::ID>& vedgesOut) const;

	//virtual const vector<MeshTraits::ID>& getSortedContourEdges(int i) const;
	//virtual const vector<MeshTraits::ID>& getSortedContourVerts(int i) const;

	//virtual void computeSortedContour(const vector<MeshTraits::ID>& vvertsIn, const vector<MeshTraits::ID>& vedgesIn, vector<MeshTraits::ID>& vcontVerts, vector<MeshTraits::ID>& vcontEdges) const;

	//virtual void computeContourVerts(const vector<MeshTraits::ID>& vvertsIn, const vector<MeshTraits::ID>& vedgesIn, vector<MeshTraits::ID>& vcontour) const; // To deprecate?


};

#endif