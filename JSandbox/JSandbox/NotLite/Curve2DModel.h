/*=====================================================================================*/
/*!
\file		Curve2DModel.h
\author		jesusprod
\brief		Custom made model for patch boundaries. Mainly a rod in 2D.
*/
/*=====================================================================================*/

#ifndef CURVE2D_MODEL_H
#define CURVE2D_MODEL_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/Curve2DEdgeElement.h>
#include <JSandbox/Element/Curve2DAngleElement.h>

#include <JSandbox/Model/NodalSolidModel.h>

class JSANDBOX_EXPORT Curve2DModel : public NodalSolidModel
{

public:
	Curve2DModel();
	virtual ~Curve2DModel();

	/* From Solid Model */

	virtual void setup();

	virtual void update_Rest();
	virtual void update_Mass();

	/* From Solid Model */

	virtual void configureCurve(const dVector& vpos);

	virtual dVector* getParam_Angles() const { return this->m_pvrestAng; }
	virtual void setParam_Angles(dVector* pva) { this->m_pvrestAng = pva; }

	virtual dVector* getParam_Lengths() const { return this->m_pvrestLen; }
	virtual void setParam_Lengths(dVector* pvl) { this->m_pvrestLen = pvl; }

protected:

	/* From Solid Model */

	virtual void freeElements();

	/* From Solid Model */

	dVector m_vposRaw;
	dVector* m_pvrestAng;	// Rest angles
	dVector* m_pvrestLen;	// Rest lengths

	vector<Curve2DEdgeElement*> m_vpEdgeEle;
	vector<Curve2DAngleElement*> m_vpAngleEle;

};

#endif