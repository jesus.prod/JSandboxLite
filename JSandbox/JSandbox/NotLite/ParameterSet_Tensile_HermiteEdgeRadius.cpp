/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_HermiteEdgeRadius.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_HermiteEdgeRadius.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_HermiteEdgeRadius.h>

ParameterSet_Tensile_HermiteEdgeRadius::ParameterSet_Tensile_HermiteEdgeRadius(int midx) : ParameterSet("TENEDGERADIUS")
{
	assert(midx >= 0);
	this->m_midx = midx;

	this->m_type = RadiusType::Anisotropic;
}

ParameterSet_Tensile_HermiteEdgeRadius::~ParameterSet_Tensile_HermiteEdgeRadius()
{
	// Nothing to do here
}

//void ParameterSet_Tensile_HermiteEdgeRadius::concatenatedToRods(const VectorXd& vpAll, vector<VectorXd>& vpRod) const
//{
//	int offsetRod = 0;
//
//	int nr = (int)vpRod.size();
//	for (int i = 0; i < nr; ++i)
//	{
//		int nc = (int)vpRod[i].size()/2;
//
//		for (int j = 0; j < nc; ++j)
//		{
//			int offsetSplit;
//			int offsetWhole;
//
//			switch (this->m_type)
//			{
//			case RadiusType::Isotropic:
//				offsetSplit = 2 * j;
//				offsetWhole = offsetRod + j;
//				vpRod[i](offsetSplit + 0) = vpAll(offsetWhole);
//				vpRod[i](offsetSplit + 1) = vpAll(offsetWhole); 
//				break;
//
//			case RadiusType::Constrained:
//			case RadiusType::Anisotropic:
//				offsetSplit = 2 * j;
//				offsetWhole = 2 * (offsetRod + j);
//				vpRod[i](offsetSplit + 0) = vpAll(offsetWhole + 0); 
//				vpRod[i](offsetSplit + 1) = vpAll(offsetWhole + 1);
//				break;
//			}
//		}
//
//		offsetRod += nc;
//	}
//}

//void ParameterSet_Tensile_HermiteEdgeRadius::rodsToConcatenated(const vector<VectorXd>& vpRod, VectorXd& vpAll) const
//{
//	int offsetRod = 0;
//
//	int nr = (int) vpRod.size();
//	for (int i = 0; i < nr; ++i)
//	{
//		int nc = (int)vpRod[i].size()/2;
//
//		for (int j = 0; j < nc; ++j)
//		{
//			int offsetSplit;
//			int offsetWhole;
//
//			switch (this->m_type)
//			{
//			case RadiusType::Isotropic:
//				offsetSplit = 2 * j;
//				offsetWhole = offsetRod + j;
//				vpAll(offsetWhole + 0) = 0.5*(vpRod[i](offsetSplit + 0) +
//										      vpRod[i](offsetSplit + 1));
//				break;
//
//			case RadiusType::Constrained:
//			case RadiusType::Anisotropic:
//				offsetSplit = 2 * j;
//				offsetWhole = 2 * (offsetRod + j);
//				vpAll(offsetWhole + 0) = vpRod[i](offsetSplit + 0);
//				vpAll(offsetWhole + 1) = vpRod[i](offsetSplit + 1);
//				break;
//			}
//		}
//
//		offsetRod += nc;
//	}
//}

void ParameterSet_Tensile_HermiteEdgeRadius::concatenatedToRods(const VectorXd& vpAll, vector<VectorXd>& vpRod) const
{
	int offsetRod = 0;

	int nr = (int)vpRod.size();
	for (int i = 0; i < nr; ++i)
	{
		if (this->m_vdummy[i])
			continue; // Ignore

		int nc = (int)vpRod[i].size()/2;

		for (int j = 0; j < nc; ++j)
		{
			int offsetSplit;
			int offsetWhole;

			switch (this->m_type)
			{
			case RadiusType::Isotropic:
				offsetSplit = 2 * j;
				offsetWhole = this->m_vmapSplit2Whole[offsetRod + j];
				vpRod[i](offsetSplit + 0) = vpAll(offsetWhole);
				vpRod[i](offsetSplit + 1) = vpAll(offsetWhole); 
				break;

			case RadiusType::Constrained:
			case RadiusType::Anisotropic:
				offsetSplit = 2 * j;
				offsetWhole = 2 * this->m_vmapSplit2Whole[(offsetRod + j)];
				vpRod[i](offsetSplit + 0) = vpAll(offsetWhole + 0); 
				vpRod[i](offsetSplit + 1) = vpAll(offsetWhole + 1);
				break;
			}
		}

		offsetRod += nc;
	}
}

void ParameterSet_Tensile_HermiteEdgeRadius::rodsToConcatenated(const vector<VectorXd>& vpRod, VectorXd& vpAll) const
{
	int offsetRod = 0;

	int nr = (int)vpRod.size();
	for (int i = 0; i < nr; ++i)
	{
		if (this->m_vdummy[i])
			continue; // Ignore

		int nc = (int)vpRod[i].size() / 2;

		for (int j = 0; j < nc; ++j)
		{
			int offsetSplit;
			int offsetWhole;

			switch (this->m_type)
			{
			case RadiusType::Isotropic:
				offsetSplit = 2 * j;
				offsetWhole = this->m_vmapSplit2Whole[offsetRod + j];
				vpAll(offsetWhole + 0) = 0.5*(vpRod[i](offsetSplit + 0) +
											  vpRod[i](offsetSplit + 1));
				break;

			case RadiusType::Constrained:
			case RadiusType::Anisotropic:
				offsetSplit = 2 * j;
				offsetWhole = 2 * this->m_vmapSplit2Whole[(offsetRod + j)];
				vpAll(offsetWhole + 0) = vpRod[i](offsetSplit + 0);
				vpAll(offsetWhole + 1) = vpRod[i](offsetSplit + 1);
				break;
			}
		}

		offsetRod += nc;
	}
}

void ParameterSet_Tensile_HermiteEdgeRadius::getParameters(VectorXd& vp) const
{
	vp.resize(this->m_np);

	this->rodsToConcatenated(this->m_vcontrolVal, vp);
}

void ParameterSet_Tensile_HermiteEdgeRadius::setParameters(const VectorXd& vp)
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	assert((int)vp.size() == this->m_np);

	VectorXd vpB = vp;
	this->limitParameters(vpB);
	this->concatenatedToRods(vpB, this->m_vcontrolVal);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int ne = rodMesh.getNumEdge();
	int nr = rodMesh.getNumRod();

	int offsetRod = 0;
	VectorXd vpInt(2*ne);
	for (int r = 0; r < nr; ++r)
	{
		int nvInt = pModel->getSimRod_0(r).getNumNode();
		int neInt = pModel->getSimRod_0(r).getNumEdge();
		int npInt = 2*neInt;

		if (this->m_vdummy[r])
		{
			const vector<Vector2d> vr = pModel->getSimRod_0(r).getRadius();

			for (int j = 0; j < neInt; ++j)
			{
				vpInt(offsetRod + 2*j + 0) = vr[j].x();
				vpInt(offsetRod + 2*j + 1) = vr[j].y();
			}
			offsetRod += npInt;

			continue;
		}

		VectorXd vcontrolRodMod = this->m_vcontrolVal[r];
		int nc = (int) this->m_vcontrolVal[r].size() / 2;
		if (this->m_type == RadiusType::Constrained)
		{
			// Set RH value from RW*alpha

			for (int i = 0; i < nc; ++i)
			{
				int offset = 2 * i;
				Real rw = vcontrolRodMod(offset + 0);
				Real ra = vcontrolRodMod(offset + 1);
				vcontrolRodMod[offset + 1] = rw*ra;
			}
		}

		bool closed = pModel->getSimRod_0(r).getCurve().getIsClosed();

		VectorXd vpIntRod(npInt);
		dVector vsInt;
		if (closed)
		{
			dVector vsIntTemp = Curve(dVector(3*nvInt+3), true).getEdge01Parametrization();
			vsInt.insert(vsInt.end(), vsIntTemp.begin(), vsIntTemp.begin() + neInt);
		}
		else
		{
			vsInt = pModel->getSimRod_0(r).getCurve().getEdge01Parametrization();
		}

		this->computeValRod(this->m_vcontrolPar[r], vcontrolRodMod, vsInt, vpIntRod, closed);

		dVector newvp;
		toSTL(vpIntRod, newvp);

		// Assemble interpolated
		for (int j = 0; j < npInt; ++j)
			vpInt(offsetRod + j) = vpIntRod(j);

		offsetRod += npInt;
	}

	pModel->setParam_RadiusEdge(vpInt);
}

bool ParameterSet_Tensile_HermiteEdgeRadius::isReady_fp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_fre();
}

bool ParameterSet_Tensile_HermiteEdgeRadius::isReady_DfDp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_DfxDre();
}

void ParameterSet_Tensile_HermiteEdgeRadius::get_fp(VectorXd& vfp) const
{
	throw new exception("Not implemented yet...");

	// TODO

	//this->m_pTSModel->getRodMeshModel(this->m_midx)->add_fre(vfp);
}

//void ParameterSet_Tensile_HermiteEdgeRadius::get_DfDp(tVector& vDfDp_t) const
//{
//	//this->testHermiteJacobian();
//
//    RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);
//	int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);
//
//	SparseMatrixXd mDiDc;
//	this->computeJac(mDiDc);
//	int ni = mDiDc.rows();
//	int nc = mDiDc.cols();
//
//	tVector vDfDp_int_split; // With respect to interpolated value 
//	tVector vDfDp_con_split; // With respect to the control points
//	SparseMatrixXd mDfDp_int_split(pModel->getNumSimDOF_x(), ni);
//	SparseMatrixXd mDfDp_con_split(pModel->getNumSimDOF_x(), nc);
//	vDfDp_int_split.reserve(pModel->getNumNonZeros_DfxDre());
//
//	// Add derivative (interpolated)
//	pModel->add_DfxDre(vDfDp_int_split);
//
//	mDfDp_int_split.setFromTriplets(vDfDp_int_split.begin(), vDfDp_int_split.end());
//
//	// Compute derivative (control)
//	mDfDp_con_split = mDfDp_int_split * mDiDc;
//	toTriplets(mDfDp_con_split, vDfDp_con_split);
//	int nCoef = (int) vDfDp_con_split.size();
//
//	tVector vDfDp_con_whole;
//
//	VectorXd vp;
//
//	switch (this->m_type)
//	{
//	case RadiusType::Anisotropic:
//		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_con_split, vDfDp_t);
//		break;
//
//	case RadiusType::Constrained:
//		
//		this->getParameters(vp);
//
//		// At most same element number
//		vDfDp_con_whole.reserve(nCoef);
//
//		for (int i = 0; i < nCoef; ++i)
//		{
//			const Triplet<Real>& t = vDfDp_con_split[i];
//
//			int parIdx = t.col() / 2;
//			int parPos = t.col() % 2;
//
//			if (parPos == 0) vDfDp_con_whole.push_back(Triplet<Real>(t.row(), t.col(), t.value()));
//			if (parPos == 1)
//			{
//				double rw = vp(2*parIdx + 0);
//				double ra = vp(2*parIdx + 1);
//				vDfDp_con_whole.push_back(Triplet<Real>(t.row(), 2*parIdx + 0, t.value()*ra));
//				vDfDp_con_whole.push_back(Triplet<Real>(t.row(), 2*parIdx + 1, t.value()*rw));
//			}
//		}
//
//		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_con_whole, vDfDp_t);
//		break;
//
//	case RadiusType::Isotropic:
//
//		vDfDp_con_whole.reserve(nCoef);
//
//		for (int i = 0; i < nCoef; ++i)
//		{
//			const Triplet<Real>& t = vDfDp_con_split[i]; // Sum all the derivatives!
//			vDfDp_con_whole.push_back(Triplet<Real>(t.row(), t.col() / 2, t.value()));
//		}
//
//		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_con_whole, vDfDp_t);
//		break;
//	}
//}

void ParameterSet_Tensile_HermiteEdgeRadius::get_DfDp(tVector& vDfDp_t) const
{
	//this->testHermiteJacobian();

    RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);
	int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);

	SparseMatrixXd mDiDc;
	this->computeJac(mDiDc);
	int ni = mDiDc.rows();
	int nc = mDiDc.cols();

	tVector vDfDp_int_split; // With respect to interpolated value 
	tVector vDfDp_con_split; // With respect to the control points
	SparseMatrixXd mDfDp_int_split(pModel->getNumSimDOF_x(), ni);
	SparseMatrixXd mDfDp_con_split(pModel->getNumSimDOF_x(), nc);
	vDfDp_int_split.reserve(pModel->getNumNonZeros_DfxDre());

	// Add derivative (interpolated)
	pModel->add_DfxDre(vDfDp_int_split);

	mDfDp_int_split.setFromTriplets(vDfDp_int_split.begin(), vDfDp_int_split.end());

	// Compute derivative (control)
	mDfDp_con_split = mDfDp_int_split * mDiDc;
	toTriplets(mDfDp_con_split, vDfDp_con_split);
	int nCoef = (int) vDfDp_con_split.size();

	tVector vDfDp_con_whole;

	vDfDp_con_whole.reserve(nCoef);
	for (int i = 0; i < nCoef; ++i)
	{
		const Triplet<Real>& t = vDfDp_con_split[i];
		int colSpl = t.col();
		switch (this->m_type)
		{
		case RadiusType::Isotropic:
			vDfDp_con_whole.push_back(Triplet<Real>(t.row(), this->m_vmapSplit2Whole[colSpl], t.value()));
			break;

		case RadiusType::Anisotropic:
			vDfDp_con_whole.push_back(Triplet<Real>(t.row(), 2 * this->m_vmapSplit2Whole[colSpl / 2] + colSpl % 2, t.value()));
			break;

		case RadiusType::Constrained:
			vDfDp_con_whole.push_back(Triplet<Real>(t.row(), 2 * this->m_vmapSplit2Whole[colSpl / 2] + colSpl % 2, t.value()));
			break;
		}
	}

	VectorXd vp;

	tVector vDfDp_con_radius;

	switch (this->m_type)
	{
	case RadiusType::Anisotropic:
		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_con_whole, vDfDp_t);
		break;

	case RadiusType::Constrained:
		
		this->getParameters(vp);

		// At most 1.5 times the element number
		vDfDp_con_radius.reserve(1.5*nCoef);

		for (int i = 0; i < nCoef; ++i)
		{
			const Triplet<Real>& t = vDfDp_con_whole[i];

			int parIdx = t.col() / 2;
			int parPos = t.col() % 2;

			if (parPos == 0) vDfDp_con_radius.push_back(Triplet<Real>(t.row(), t.col(), t.value()));
			if (parPos == 1)
			{
				double rw = vp(2*parIdx + 0);
				double ra = vp(2*parIdx + 1);
				vDfDp_con_radius.push_back(Triplet<Real>(t.row(), 2 * parIdx + 0, t.value()*ra));
				vDfDp_con_radius.push_back(Triplet<Real>(t.row(), 2 * parIdx + 1, t.value()*rw));
			}
		}

		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_con_radius, vDfDp_t);
		break;

	case RadiusType::Isotropic:

		// At most 0.5 times the element number
		vDfDp_con_radius.reserve(0.5*nCoef);

		for (int i = 0; i < nCoef; ++i)
		{
			const Triplet<Real>& t = vDfDp_con_whole[i]; // Sum all the derivatives!
			vDfDp_con_radius.push_back(Triplet<Real>(t.row(), t.col() / 2, t.value()));
		}

		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_con_radius, vDfDp_t);
		break;
	}
}

void ParameterSet_Tensile_HermiteEdgeRadius::setup()
{
	assert(this->m_pModel != NULL);

	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	int nr = pModel->getNumRod();
	this->m_vcontrolPar.resize(nr);
	this->m_vcontrolVal.resize(nr);
	
	int npSplit = 0;

	// Use a specific number of control ponints

	for (int r = 0; r < nr; ++r)
	{
		if (this->m_vdummy[r])
		{
			this->m_vcontrolPar[r].resize(0);
			this->m_vcontrolVal[r].resize(0);
		}
		else
		{
			this->m_vcontrolVal[r].resize(2*this->m_vcontrolNum[r]);
			this->m_vcontrolPar[r].resize(this->m_vcontrolNum[r]);
			npSplit += this->m_vcontrolNum[r];
		}
	}

	this->updateControlPoints();

	int npWhole = 0;
	for (int i = 0; i < npSplit; ++i)
		if (this->m_vmapSplit2Whole[i] > npWhole)
			npWhole = this->m_vmapSplit2Whole[i];
	npWhole++;

	// Anisotropic or constrained?
	if (this->m_type == RadiusType::Anisotropic ||
		this->m_type == RadiusType::Constrained)
		this->m_np = 2*npWhole;
	if (this->m_type == RadiusType::Isotropic)
		this->m_np = npWhole;

	this->updateBoundsVector();
}

void ParameterSet_Tensile_HermiteEdgeRadius::updateBoundsVector()
{
	VectorXd vp;

	// Get current values!
	this->getParameters(vp);

	if (this->m_type == RadiusType::Constrained)
	{
		this->m_vmaxValues.resize(this->m_np);
		this->m_vminValues.resize(this->m_np);
		for (int i = 0; i < this->m_np; ++i)
		{
			int par = i / 2;
			int mod = i % 2;

			if (mod == 0)
			{
				Real minR = this->m_minRadius/
							this->m_maxRatio;
				Real maxR = this->m_maxRadius;
				this->m_vminValues(i) = minR;
				this->m_vmaxValues(i) = maxR;
			}
			if (mod == 1)
			{
				Real vr = vp(2 * par);
				if (vr > this->m_maxRadius)
					vr = this->m_maxRadius;
				if (vr < this->m_minRadius)
					vr = this->m_minRadius;
				Real mina = m_minRadius / vr;
				if (mina < this->m_minRatio)
					mina = this->m_minRatio;
				Real maxa = this->m_maxRatio;
				this->m_vminValues(i) = mina;
				this->m_vmaxValues(i) = maxa; 
			}
		}
	}
	else ParameterSet::updateBoundsVector();
}

void ParameterSet_Tensile_HermiteEdgeRadius::computeJac(SparseMatrixXd& mDiDc) const
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int nr = rodMesh.getNumRod();

	int offsetCon = 0;
	int offsetInt = 0;

	tVector vDiDc;

	for (int r = 0; r < nr; ++r)
	{
		int neCon = (int) this->m_vcontrolVal[r].size();
		int nvInt = pModel->getSimRod_0(r).getNumNode();
		int neInt = pModel->getSimRod_0(r).getNumEdge();
		int npInt = 2*neInt;

		if (m_vdummy[r])
		{
			offsetInt += npInt;
			continue; // Jump
		}

		VectorXd vcontrolRodMod = this->m_vcontrolVal[r];
		int nc = (int) this->m_vcontrolVal[r].size() / 2;
		if (this->m_type == RadiusType::Constrained)
		{
			// Set RH value from RW*alpha

			for (int i = 0; i < nc; ++i)
			{
				int offset = 2 * i;
				Real rw = vcontrolRodMod(offset + 0);
				Real ra = vcontrolRodMod(offset + 1);
				vcontrolRodMod[offset + 1] = rw*ra;
			}
		}

		bool closed = pModel->getSimRod_0(r).getCurve().getIsClosed();

		tVector vDiDc_r;
		dVector vsInt;
		if (closed)
		{
			dVector vsIntTemp = Curve(dVector(3*nvInt+3), true).getEdge01Parametrization();
			vsInt.insert(vsInt.end(), vsIntTemp.begin(), vsIntTemp.begin() + neInt);
		}
		else
		{
			vsInt = pModel->getSimRod_0(r).getCurve().getEdge01Parametrization();
		}

		this->computeJacRod(this->m_vcontrolPar[r], vcontrolRodMod, vsInt, vDiDc_r, closed);

		// Assemble interpolate radii

		int numCoe = (int)vDiDc_r.size();
		for (int c = 0; c < numCoe; ++c)
		{
			const Triplet<Real>& t = vDiDc_r[c];
			vDiDc.push_back(Triplet<Real>(offsetInt + t.row(),
										  offsetCon + t.col(),
										  t.value()));
		}

		offsetCon += neCon;
		offsetInt += npInt;
	}

	mDiDc = SparseMatrixXd(offsetInt, offsetCon);
	mDiDc.setFromTriplets(vDiDc.begin(), vDiDc.end());
}

//void ParameterSet_Tensile_HermiteEdgeRadius::computeValRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, VectorXd& vpInt, bool closed) const
//{
//	int neCon = (int)vsCon.size();
//	int neInt = (int)vsInt.size();
//
//	int firstInt = 0, lastInt = neCon - 2;
//
//	int N = 2;
//
//	int curInt = 0;
//	for (int e = 0; e < neInt; ++e)
//	{
//		Real si = vsInt[e];
//		while (si > vsCon[curInt + 1])
//			curInt++; // Next interval
//
//		Real s0;
//		Real s1 = vsCon[curInt + 0];
//		Real s2 = vsCon[curInt + 1];
//		Real s3;
//
//		if (curInt > firstInt && curInt < lastInt)
//		{
//			s0 = vsCon[curInt - 1];
//			s3 = vsCon[curInt + 2];
//		}
//		else if (curInt == firstInt && curInt == lastInt)
//		{
//			Real srange = s2 - s1;
//			s0 = s1 - srange;
//			s3 = s2 + srange;
//		}
//		else if (curInt == firstInt)
//		{
//			s3 = vsCon[curInt + 2];
//			Real srange = s2 - s1;
//			s0 = s1 - srange;
//		}
//		else if (curInt == lastInt)
//		{
//			s0 = vsCon[curInt - 1];
//			Real srange = s2 - s1;
//			s3 = s2 + srange;
//		}
//
//		Real sp1 = (si - s1) / (s2 - s1);
//		Real sp2 = sp1*sp1;
//		Real sp3 = sp2*sp1;
//
//		int offsete = N * e;
//		int offseteInt0 = N * (curInt - 1);
//		int offseteInt1 = N * (curInt + 0);
//		int offseteInt2 = N * (curInt + 1);
//		int offseteInt3 = N * (curInt + 2);
//
//		for (int d = 0; d < N; ++d)
//		{
//			int offsetd = offsete + d;
//			int offsetdInt0 = offseteInt0 + d;
//			int offsetdInt1 = offseteInt1 + d;
//			int offsetdInt2 = offseteInt2 + d;
//			int offsetdInt3 = offseteInt3 + d;
//
//			Real p0;
//			Real p1 = vpCon[offsetdInt1];
//			Real p2 = vpCon[offsetdInt2];
//			Real p3;
//
//			if (curInt > firstInt && curInt < lastInt)
//			{
//				p0 = vpCon[offsetdInt0];
//				p3 = vpCon[offsetdInt3];
//			}
//			else if (curInt == firstInt && curInt == lastInt)
//			{
//				p0 = p1;
//				p3 = p2;
//			}
//			else if (curInt == firstInt)
//			{
//				p3 = vpCon[offsetdInt3];
//				p0 = p1;
//			}
//			else if (curInt == lastInt)
//			{
//				p0 = vpCon[offsetdInt0];
//				p3 = p2;
//			}
//
//			{
//#include "../../Maple/Code/CatmullRom3DPoint.mcg"
//
//				vpInt(offsetd) = t25;
//			}
//		}
//	}
//}

void ParameterSet_Tensile_HermiteEdgeRadius::computeValRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, VectorXd& vpInt, bool closed) const
{
	int nvCon = (int)vsCon.size();
	int nvInt = (int)vsInt.size();

	int firstInt = 0, lastInt = nvCon - 2;

	int curInt = 0;
	for (int v = 0; v < nvInt; ++v)
	{
		Real si = vsInt[v];
		while (si > vsCon[curInt + 1])
			curInt++; // Next interval

		Real s0;
		Real s1 = vsCon[curInt + 0];
		Real s2 = vsCon[curInt + 1];
		Real s3;

		if (curInt > firstInt && curInt < lastInt)
		{
			s0 = vsCon[curInt - 1];
			s3 = vsCon[curInt + 2];
		}
		else if (curInt == firstInt && curInt == lastInt)
		{
			Real srange = s2 - s1;
			s0 = s1 - srange;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt && !closed)
		{
			s3 = vsCon[curInt + 2];
			Real srange = s2 - s1;
			s0 = s1 - srange;
		}
		else if (curInt == lastInt && !closed)
		{
			s0 = vsCon[curInt - 1];
			Real srange = s2 - s1;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt && closed)
		{
			s3 = vsCon[curInt + 2];
			s0 = vsCon[lastInt - 0] - 1;
		}
		else if (curInt == lastInt && closed)
		{
			s0 = vsCon[curInt - 1];
			s3 = 1 + vsCon[firstInt + 1];
		}

		Real sp1 = (si - s1) / (s2 - s1);
		Real sp2 = sp1*sp1;
		Real sp3 = sp2*sp1;

		int offsetv = 2 * v;
		int offsetvInt0 = 2 * (curInt - 1);
		int offsetvInt1 = 2 * (curInt + 0);
		int offsetvInt2 = 2 * (curInt + 1);
		int offsetvInt3 = 2 * (curInt + 2);

		for (int d = 0; d < 2; ++d)
		{
			int offsetd = offsetv + d;
			int offsetdInt0 = offsetvInt0 + d;
			int offsetdInt1 = offsetvInt1 + d;
			int offsetdInt2 = offsetvInt2 + d;
			int offsetdInt3 = offsetvInt3 + d;

			Real p0;
			Real p1 = vpCon[offsetdInt1];
			Real p2 = vpCon[offsetdInt2];
			Real p3;

			if (curInt > firstInt && curInt < lastInt)
			{
				p0 = vpCon[offsetdInt0];
				p3 = vpCon[offsetdInt3];
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				p0 = p1;
				p3 = p2;
			}
			else if (curInt == firstInt && !closed)
			{
				p3 = vpCon[offsetdInt3];
				p0 = p1;
			}
			else if (curInt == lastInt && !closed)
			{
				p0 = vpCon[offsetdInt0];
				p3 = p2;
			}
			else if (curInt == firstInt && closed)
			{
				p3 = vpCon[offsetdInt3];
				offsetdInt0 = 2 * (lastInt - 0) + d;
				p0 = vpCon[offsetdInt0];
			}
			else if (curInt == lastInt && closed)
			{
				p0 = vpCon[offsetdInt0];
				offsetdInt3 = 2 * (firstInt + 1) + d;
				p3 = vpCon[offsetdInt3];
			}

			{
#include "../../Maple/Code/CatmullRom3DPoint.mcg"

				vpInt(offsetd) = t25;
			}
		}
	}
}

//void ParameterSet_Tensile_HermiteEdgeRadius::computeJacRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, tVector& vDpDm, bool closed) const
//{
//	int neCon = (int)vsCon.size();
//	int neInt = (int)vsInt.size();
//
//	int firstInt = 0, lastInt = neCon - 2;
//
//	int N = 2;
//
//	int curInt = 0;
//	for (int e = 0; e < neInt; ++e)
//	{
//		Real si = vsInt[e];
//		while (si > vsCon[curInt + 1])
//			curInt++; // Next interval
//
//		Real s0;
//		Real s1 = vsCon[curInt + 0];
//		Real s2 = vsCon[curInt + 1];
//		Real s3;
//		if (curInt > firstInt && curInt < lastInt)
//		{
//			s0 = vsCon[curInt - 1];
//			s3 = vsCon[curInt + 2];
//		}
//		else if (curInt == firstInt && curInt == lastInt)
//		{
//			Real srange = s2 - s1;
//			s0 = s1 - srange;
//			s3 = s2 + srange;
//		}
//		else if (curInt == firstInt)
//		{
//			s3 = vsCon[curInt + 2];
//			Real srange = s2 - s1;
//			s0 = s1 - srange;
//		}
//		else if (curInt == lastInt)
//		{
//			s0 = vsCon[curInt - 1];
//			Real srange = s2 - s1;
//			s3 = s2 + srange;
//		}
//
//		Real sp1 = (si - s1) / (s2 - s1);
//		Real sp2 = sp1*sp1;
//		Real sp3 = sp2*sp1;
//
//		int offsete = N * e;
//		int offseteInt0 = N * (curInt - 1);
//		int offseteInt1 = N * (curInt + 0);
//		int offseteInt2 = N * (curInt + 1);
//		int offseteInt3 = N * (curInt + 2);
//
//		for (int d = 0; d < N; ++d)
//		{
//			int offsetd = offsete + d;
//			int offsetdInt0 = offseteInt0 + d;
//			int offsetdInt1 = offseteInt1 + d;
//			int offsetdInt2 = offseteInt2 + d;
//			int offsetdInt3 = offseteInt3 + d;
//
//			Real p0;
//			Real p1 = vpCon[offsetdInt1];
//			Real p2 = vpCon[offsetdInt2];
//			Real p3;
//
//			if (curInt > firstInt && curInt < lastInt)
//			{
//				p0 = vpCon[offsetdInt0];
//				p3 = vpCon[offsetdInt3];
//			}
//			else if (curInt == firstInt && curInt == lastInt)
//			{
//				p0 = p1;
//				p3 = p2;
//			}
//			else if (curInt == firstInt)
//			{
//				p3 = vpCon[offsetdInt3];
//				p0 = p1;
//			}
//			else if (curInt == lastInt)
//			{
//				p0 = vpCon[offsetdInt0];
//				p3 = p2;
//			}
//
//			dVector mJp(4);
//
//			{
//#include "../../Maple/Code/CatmullRom3DJacobian.mcg"
//			}
//
//			if (curInt > firstInt && curInt < lastInt)
//			{
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
//			}
//			else if (curInt == firstInt && curInt == lastInt)
//			{
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[0]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[3]));
//			}
//			else if (curInt == firstInt)
//			{
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[0]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
//			}
//			else if (curInt == lastInt)
//			{
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
//				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[3]));
//			}
//		}
//	}
//}

void ParameterSet_Tensile_HermiteEdgeRadius::computeJacRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, tVector& vDpDm, bool closed) const
{
	int nvCon = (int)vsCon.size();
	int nvInt = (int)vsInt.size();

	int firstInt = 0, lastInt = nvCon - 2;

	int curInt = 0;
	for (int v = 0; v < nvInt; ++v)
	{
		Real si = vsInt[v];
		while (si > vsCon[curInt + 1])
			curInt++; // Next interval

		Real s0;
		Real s1 = vsCon[curInt + 0];
		Real s2 = vsCon[curInt + 1];
		Real s3;
		if (curInt > firstInt && curInt < lastInt)
		{
			s0 = vsCon[curInt - 1];
			s3 = vsCon[curInt + 2];
		}
		else if (curInt == firstInt && curInt == lastInt)
		{
			Real srange = s2 - s1;
			s0 = s1 - srange;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt && !closed)
		{
			s3 = vsCon[curInt + 2];
			Real srange = s2 - s1;
			s0 = s1 - srange;
		}
		else if (curInt == lastInt && !closed)
		{
			s0 = vsCon[curInt - 1];
			Real srange = s2 - s1;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt && closed)
		{
			s3 = vsCon[curInt + 2];
			s0 = vsCon[lastInt - 0] - 1;
		}
		else if (curInt == lastInt && closed)
		{
			s0 = vsCon[curInt - 1];
			s3 = 1 + vsCon[firstInt + 1];
		}

		Real sp1 = (si - s1) / (s2 - s1);
		Real sp2 = sp1*sp1;
		Real sp3 = sp2*sp1;

		int offsetv = 2 * v;
		int offsetvInt0 = 2 * (curInt - 1);
		int offsetvInt1 = 2 * (curInt + 0);
		int offsetvInt2 = 2 * (curInt + 1);
		int offsetvInt3 = 2 * (curInt + 2);

		for (int d = 0; d < 2; ++d)
		{
			int offsetd = offsetv + d;
			int offsetdInt0 = offsetvInt0 + d;
			int offsetdInt1 = offsetvInt1 + d;
			int offsetdInt2 = offsetvInt2 + d;
			int offsetdInt3 = offsetvInt3 + d;

			Real p0;
			Real p1 = vpCon[offsetdInt1];
			Real p2 = vpCon[offsetdInt2];
			Real p3;

			if (curInt > firstInt && curInt < lastInt)
			{
				p0 = vpCon[offsetdInt0];
				p3 = vpCon[offsetdInt3];
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				p0 = p1;
				p3 = p2;
			}
			else if (curInt == firstInt && !closed)
			{
				p3 = vpCon[offsetdInt3];
				p0 = p1;
			}
			else if (curInt == lastInt && !closed)
			{
				p0 = vpCon[offsetdInt0];
				p3 = p2;
			}
			else if (curInt == firstInt && closed)
			{
				p3 = vpCon[offsetdInt3];
				offsetdInt0 = 2 * (lastInt - 0) + d;
				p0 = vpCon[offsetdInt0];
			}
			else if (curInt == lastInt && closed)
			{
				p0 = vpCon[offsetdInt0];
				offsetdInt3 = 2 * (firstInt + 1) + d;
				p3 = vpCon[offsetdInt3];
			}

			dVector mJp(4);

			{
#include "../../Maple/Code/CatmullRom3DJacobian.mcg"
			}

			if (curInt > firstInt && curInt < lastInt)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[3]));
			}
			else if (curInt == firstInt && !closed)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
			else if (curInt == lastInt && !closed)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[3]));
			}
			else if (curInt == firstInt && closed)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
			else if (curInt == lastInt && closed)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
		}
	}
}

//void ParameterSet_Tensile_HermiteEdgeRadius::updateControlPoints()
//{
//	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);
//
//    const  RodMesh& rodMesh = pModel->getSimMesh_0();
//
//	int curIdx = 0;
//	int offset = 0;
//
//	this->m_vmapSplit2Whole.reserve(rodMesh.getNumNode());
//
//	// Map connection points to whole indices
//	iVector vmapConIdx(rodMesh.getNumCon(), -1);
//
//	int nr = rodMesh.getNumRod();
//
//	for (int r = 0; r < nr; ++r)
//	{
//		int nc = (int) this->m_vcontrolPar[r].size();
//		const Curve& curve = rodMesh.getRod(r).getCurve();
//
//		int ne = curve.getNumEdge();
//		dVector vsEdge = curve.getEdgeParameters();
//		const vector<Vector2d>& vrEdge = rodMesh.getRod(r).getRadius();
//	
//		Real parInc = 1.0 / (nc - 1);
//
//		this->m_vcontrolPar[r][0] = 0;
//		this->m_vcontrolPar[r][nc-1] = 1.0;
//
//		if (this->m_type == RadiusType::Constrained)
//		{
//			this->m_vcontrolVal[r][0] = vrEdge.front().x();
//			this->m_vcontrolVal[r][1] = vrEdge.front().y()/
//										vrEdge.front().x();
//			this->m_vcontrolVal[r][2 * nc - 2] = vrEdge.back().x();
//			this->m_vcontrolVal[r][2 * nc - 1] = vrEdge.back().y()/
//												 vrEdge.back().x();
//		}
//		else
//		{
//			this->m_vcontrolVal[r][0] = vrEdge.front().x();
//			this->m_vcontrolVal[r][1] = vrEdge.front().y();
//			this->m_vcontrolVal[r][2 * nc - 2] = vrEdge.back().x();
//			this->m_vcontrolVal[r][2 * nc - 1] = vrEdge.back().y();
//		}
//
//		for (int c = 1; c < nc - 1; ++c)
//		{
//			Real parUni = c * parInc;
//
//			// Get initial radius value
//			for (int e = 0; e < ne; ++e)
//			{
//				Real L0 = curve.getEdge(e).norm();
//				Real parLen = parUni*vsEdge.back();
//
//				if (parLen > vsEdge[e] - L0/2 && parLen <= vsEdge[e] + L0/2)
//				{
//					// If constrained radius, store ratio RH/RW
//					if (this->m_type == RadiusType::Constrained)
//					{
//						this->m_vcontrolVal[r](2 * c + 0) = vrEdge[e].x();
//						this->m_vcontrolVal[r](2 * c + 1) = vrEdge[e].y()/
//															vrEdge[e].x();
//					}
//					else
//					{
//						this->m_vcontrolVal[r](2 * c + 0) = vrEdge[e].x();
//						this->m_vcontrolVal[r](2 * c + 1) = vrEdge[e].y();
//					}
//					this->m_vcontrolPar[r](c) = parUni;
//					break;
//				}
//			}
//		}
//	}
//}

void ParameterSet_Tensile_HermiteEdgeRadius::updateControlPoints()
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

    const RodMesh& rodMesh = pModel->getSimMesh_0();

	int curIdx = 0;

	vector<Vector2d> vconRadius(rodMesh.getNumCon());
	for (int i = 0; i < rodMesh.getNumCon(); ++i)
	{
		vconRadius[i].setZero();

		iVector vedges = rodMesh.getConEdges(i);
		for (int j = 0; j < (int)vedges.size(); ++j)
		{
			int rodIdx, edgeIdx;
			rodMesh.getRodEdgeForMeshEdge(vedges[j], rodIdx, edgeIdx);
			vconRadius[i] += rodMesh.getRod(rodIdx).getRadius(edgeIdx);
		}

		vconRadius[i] /= (int) vedges.size();
	}

	this->m_vmapSplit2Whole.reserve(rodMesh.getNumEdge());

	// Map connection points to whole indices
	iVector vmapConIdx(rodMesh.getNumCon(), -1);

	int nr = rodMesh.getNumRod();

	for (int r = 0; r < nr; ++r)
	{
		if (this->m_vdummy[r])
			continue; // Ignore

		int ne = rodMesh.getRod(r).getNumEdge();
		int nc = (int) this->m_vcontrolPar[r].size();
		const Curve& curve = rodMesh.getRod(r).getCurve();

		dVector vsEdge = curve.getEdgeParameters();

		const vector<Vector2d>& vrEdge = rodMesh.getRod(r).getRadius();
	
		Real parInc = 1.0 / (nc - 1);

		const pair<int, int>& rodCons = rodMesh.getRodCons(r);

		// First point -----------------------

		this->m_vcontrolPar[r][0] = 0;

		int firstEdgeIdx = -1;
		int lastEdgeIdx = -1;
		Vector2d firstRadius;
		Vector2d lastRadius;

		if (this->m_constrainConnections)
		{
			if (rodCons.first == -1)
			{
				if (curve.getIsClosed())
				{
					firstEdgeIdx = curIdx++;
					m_vmapSplit2Whole.push_back(firstEdgeIdx);
				}
				else
				{
					m_vmapSplit2Whole.push_back(curIdx++);
				}

				firstRadius = vrEdge.front();
			}
			else
			{
				if (vmapConIdx[rodCons.first] == -1)
					vmapConIdx[rodCons.first] = curIdx++;

				m_vmapSplit2Whole.push_back(vmapConIdx[rodCons.first]);

				firstRadius = vconRadius[rodCons.first];
			}
		}
		else
		{
			m_vmapSplit2Whole.push_back(curIdx++);

			firstRadius = vrEdge.front();
		}

		if (this->m_type == RadiusType::Constrained)
		{
			this->m_vcontrolVal[r][0] = firstRadius.x();
			this->m_vcontrolVal[r][1] = firstRadius.y() /
										firstRadius.x();
		}
		else
		{
			this->m_vcontrolVal[r][0] = firstRadius.x();
			this->m_vcontrolVal[r][1] = firstRadius.y();
		}

		// Middle points -----------------------

		for (int c = 1; c < nc - 1; ++c)
		{
			Real parUni = c * parInc;

			// Get initial radius value
			for (int e = 0; e < ne; ++e)
			{
				Real L0 = curve.getEdge(e).norm();
				Real parLen = parUni*vsEdge.back();

				if (parLen > vsEdge[e] - L0/2 && parLen <= vsEdge[e] + L0/2)
				{
					// If constrained radius, store ratio RH/RW
					if (this->m_type == RadiusType::Constrained)
					{
						this->m_vcontrolVal[r](2 * c + 0) = vrEdge[e].x();
						this->m_vcontrolVal[r](2 * c + 1) = vrEdge[e].y()/
															vrEdge[e].x();
					}
					else
					{
						this->m_vcontrolVal[r](2 * c + 0) = vrEdge[e].x();
						this->m_vcontrolVal[r](2 * c + 1) = vrEdge[e].y();
					}
					this->m_vcontrolPar[r](c) = parUni;
					break;
				}
			}

			this->m_vmapSplit2Whole.push_back(curIdx++);
		}

		// Last point -----------------------

		this->m_vcontrolPar[r][nc - 1] = 1.0;

		if (this->m_constrainConnections)
		{
			if (rodCons.second == -1)
			{
				if (curve.getIsClosed())
				{
					lastEdgeIdx = firstEdgeIdx;
					m_vmapSplit2Whole.push_back(lastEdgeIdx);
				}
				else
				{
					m_vmapSplit2Whole.push_back(curIdx++);
				}

				lastRadius = vrEdge.back();
			}
			else
			{
				if (vmapConIdx[rodCons.second] == -1)
					vmapConIdx[rodCons.second] = curIdx++;

				m_vmapSplit2Whole.push_back(vmapConIdx[rodCons.second]);
			
				lastRadius = vconRadius[rodCons.second];
			}
		}
		else
		{
			m_vmapSplit2Whole.push_back(curIdx++);

			lastRadius = vrEdge.back();
		}

		if (this->m_type == RadiusType::Constrained)
		{
			if (curve.getIsClosed())
			{
				this->m_vcontrolVal[r][2 * nc - 2] = firstRadius.x();
				this->m_vcontrolVal[r][2 * nc - 1] = firstRadius.y() /
													 firstRadius.x();
			}
			else
			{
				this->m_vcontrolVal[r][2 * nc - 2] = lastRadius.x();
				this->m_vcontrolVal[r][2 * nc - 1] = lastRadius.y() /
													 lastRadius.x();
			}
		}
		else
		{
			if (curve.getIsClosed())
			{
				this->m_vcontrolVal[r][2 * nc - 2] = firstRadius.x();
				this->m_vcontrolVal[r][2 * nc - 1] = firstRadius.y();
			}
			else
			{
				this->m_vcontrolVal[r][2 * nc - 2] = lastRadius.x();
				this->m_vcontrolVal[r][2 * nc - 1] = lastRadius.y();
			}
		}
	}
}

void ParameterSet_Tensile_HermiteEdgeRadius::testHermiteJacobian() const
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int nr = rodMesh.getNumRod();

	int offsetCon = 0;
	int offsetInt = 0;

	tVector vDiDc;

	for (int r = 0; r < nr; ++r)
	{
		int neCon = (int) this->m_vcontrolVal[r].size();
		
		int neInt;
		if (this->m_type == RadiusType::Isotropic)
			neInt = pModel->getSimRod_0(r).getNumEdge();
		else neInt = 2*pModel->getSimRod_0(r).getNumEdge();

		VectorXd vpIntRodP(neInt);
		VectorXd vpIntRodM(neInt);
		bool closed = pModel->getSimRod_0(r).getCurve().getIsClosed();
		dVector vsInt = pModel->getSimRod_0(r).getCurve().getEdge01Parametrization();

		VectorXd vcontrolVal = this->m_vcontrolVal[r];

		MatrixXd mDiDc_r(neInt, neCon);
		mDiDc_r.setZero(); // Initialize

		for (int c = 0; c < neCon; ++c)
		{
			// +
			vpIntRodP.setZero();
			vcontrolVal(c) += 1e-6;
			this->computeValRod(this->m_vcontrolPar[r], vcontrolVal, vsInt, vpIntRodP, closed);

			// -
			vpIntRodM.setZero();
			vcontrolVal(c) -= 2e-6;
			this->computeValRod(this->m_vcontrolPar[r], vcontrolVal, vsInt, vpIntRodM, closed);

			// Estimate
			VectorXd vpIntRodDiff = (vpIntRodP - vpIntRodM) / (2e-6);

			for (int e = 0; e < neInt; ++e)
				mDiDc_r(e, c) = vpIntRodDiff(e);

			// Restore
			vcontrolVal(c) += 1e-6;
		}

		// Assemble interpolate radii

		for (int i = 0; i < neInt; ++i)
			for (int j = 0; j < neCon; ++j)
				vDiDc.push_back(Triplet<Real>(offsetInt + i,
											  offsetCon + j,
											  mDiDc_r(i,j)));

		offsetCon += neCon;
		offsetInt += neInt;
	}

	SparseMatrixXd mDiDc_F(offsetInt, offsetCon);
	mDiDc_F.setFromTriplets(vDiDc.begin(), vDiDc.end());

	SparseMatrixXd mDiDc_A;
	this->computeJac(mDiDc_A);

	MatrixXd mDiDcDiff = (mDiDc_F.toDense() - mDiDc_A.toDense());
	Real FDNorm = mDiDc_F.norm();
	Real diffNorm = mDiDcDiff.norm();
	Real relError = diffNorm / FDNorm;

	if (FDNorm < 1e-9)
		diffNorm = 0.0;

	if (relError > 1e-9)
	{
		writeToFile(mDiDc_A, "DiDc_A.csv"); // Trace
		writeToFile(mDiDc_F, "DiDc_F.csv"); // Trace
		logSimu("[ERROR] Model: %s. DfDp test error (global): %.9f", this->m_ID.c_str(), relError);
	}
}