/*=====================================================================================*/
/*!
\file		CurveMesh2DModel.cpp
\author		jesusprod
\brief		Implementation of CurveMesh2DModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/CurveMesh2DModel.h>

#include <JSandbox/Element/Curve2DEdgeElement.h>
#include <JSandbox/Element/Curve2DAngleElement.h>

#include <omp.h> // MP support

CurveMesh2DModel::CurveMesh2DModel() : NodalSolidModel("CM2D")
{
	this->m_pSimMesh_x = NULL;
	this->m_pSimMesh_0 = NULL;

	this->deprecateConfiguration();
}

CurveMesh2DModel::~CurveMesh2DModel()
{
	freeMesh();

	logSimu("[TRACE] Destroying CurveMesh2DModel: %d", this);
}

void CurveMesh2DModel::configureRestDimensionNumber(int dim)
{
	this->m_dim0 = dim;

	this->deprecateConfiguration();
}

void CurveMesh2DModel::configureMesh(const RodMesh& mesh_0, const RodMesh& mesh_x)
{
	// Perform a deep clone of the given 
	// mesh. Maybe this is not the best
	// idea but it feels safer by now.

	this->freeMesh(); // Reinitialization
	this->m_pSimMesh_x = new RodMesh(mesh_x);
	this->m_pSimMesh_0 = new RodMesh(mesh_0);

	this->deprecateConfiguration();
}

void CurveMesh2DModel::configureMaterials(const SolidMaterial& compMat, const SolidMaterial& exteMat)
{
	assert(compMat.getStretchK() != -1);
	assert(exteMat.getStretchK() != -1);
	this->m_matComp = compMat;
	this->m_matExte = exteMat;

	this->deprecateConfiguration();
}

void CurveMesh2DModel::setup()
{
	const vector<iVector>& vwhole2Split = this->m_pSimMesh_0->getWholeToSplitMap();
	const vector<iVector>& vsplit2Whole = this->m_pSimMesh_0->getSplitToWholeMap();

	int numvSplit = (int)vsplit2Whole.size();
	int numvWhole = (int)vwhole2Split.size();

	if (this->m_dim0 == 2)
	{
		this->m_numDim_x = 2;
		this->m_numDim_0 = 2;

		this->m_numDimRaw = 3;

		this->m_numNodeSim = numvWhole;
		this->m_numNodeRaw = numvSplit;

		this->m_nsimDOF_x = 2 * numvWhole;
		this->m_nsimDOF_0 = 2 * numvWhole;
		this->m_nrawDOF = 3 * numvSplit;

		dVector vxSTLSplit3D;
		dVector vXSTLSplit3D;
		dVector vxSTLSplit2D;
		dVector vXSTLSplit2D;
		dVector vxSTLWhole2D(this->m_nsimDOF_x);
		dVector vXSTLWhole2D(this->m_nsimDOF_0);
		this->m_pSimMesh_x->getPoints(vxSTLSplit3D);
		this->m_pSimMesh_0->getPoints(vXSTLSplit3D);
		to2D(vXSTLSplit3D, vXSTLSplit2D, 1);
		to2D(vxSTLSplit3D, vxSTLSplit2D, 1);
		applyIndexMap(vsplit2Whole, vxSTLSplit2D, 2, vxSTLWhole2D);
		applyIndexMap(vsplit2Whole, vXSTLSplit2D, 2, vXSTLWhole2D);
		toEigen(vxSTLWhole2D, this->m_state_x->m_vx);
		toEigen(vXSTLWhole2D, this->m_state_0->m_vx);
	}

	if (this->m_dim0 == 3)
	{
		this->m_numDim_x = 2;
		this->m_numDim_0 = 3;

		this->m_numDimRaw = 3;

		this->m_numNodeSim = numvWhole;
		this->m_numNodeRaw = numvSplit;

		this->m_nsimDOF_x = 2 * numvWhole;
		this->m_nsimDOF_0 = 3 * numvWhole;
		this->m_nrawDOF = 3 * numvSplit;

		dVector vxSTLSplit3D;
		dVector vXSTLSplit3D;
		dVector vxSTLSplit2D;
		dVector vxSTLWhole2D(this->m_nsimDOF_x);
		dVector vXSTLWhole3D(this->m_nsimDOF_0);
		this->m_pSimMesh_x->getPoints(vxSTLSplit3D);
		this->m_pSimMesh_0->getPoints(vXSTLSplit3D);
		to2D(vxSTLSplit3D, vxSTLSplit2D, 1);
		applyIndexMap(vsplit2Whole, vxSTLSplit2D, 2, vxSTLWhole2D);
		applyIndexMap(vsplit2Whole, vXSTLSplit3D, 3, vXSTLWhole3D);
		toEigen(vxSTLWhole2D, this->m_state_x->m_vx);
		toEigen(vXSTLWhole3D, this->m_state_0->m_vx);
	}

	this->m_state_0->m_vv.resize(this->m_nsimDOF_0);
	this->m_state_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vv.setZero();
	this->m_state_x->m_vv.setZero();

	// Initialize elements

	this->freeElements();

	// Initialize stretch elements

	this->initializeElements_Stretch();
	this->initializeElements_Bending();
	this->initializeMaterials();

	this->m_isReady_Setup = true;

	this->deprecateDiscretization();

	// Update simulation 
	this->update_SimDOF();

	logSimu("[TRACE] Initialized CurveMesh2DModel. Raw: %d, Sim: %d", this->m_nrawDOF, this->m_nsimDOF_x);
}

void CurveMesh2DModel::update_SimDOF()
{
	iVector vsimNodeIdx(this->m_numNodeRaw);
	iVector vrawNodeIdx(this->m_numNodeRaw);

	for (int i = 0; i < this->m_numNodeRaw; ++i)
		vrawNodeIdx[i] = i; // Default indexation

	transformIndex(this->m_pSimMesh_0->getSplitToWholeMap(), vrawNodeIdx, 0, vsimNodeIdx);

	this->m_vraw2sim_x.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_numNodeRaw; ++i)
	{
		this->m_vraw2sim_x[3 * i + 0] = 2 * vsimNodeIdx[i] + 0;
		this->m_vraw2sim_x[3 * i + 2] = 2 * vsimNodeIdx[i] + 1;
		this->m_vraw2sim_x[3 * i + 1] = -1;
	}

	if (this->m_dim0 == 2)
	{
		this->m_vraw2sim_0.resize(this->m_nrawDOF);
		for (int i = 0; i < this->m_numNodeRaw; ++i)
		{
			this->m_vraw2sim_0[3 * i + 0] = 2 * vsimNodeIdx[i] + 0;
			this->m_vraw2sim_0[3 * i + 2] = 2 * vsimNodeIdx[i] + 1;
			this->m_vraw2sim_0[3 * i + 1] = -1;
		}
	}

	if (this->m_dim0 == 3)
	{
		this->m_vraw2sim_0.resize(this->m_nrawDOF);
		for (int i = 0; i < this->m_numNodeRaw; ++i)
		{
			this->m_vraw2sim_0[3 * i + 0] = 3 * vsimNodeIdx[i] + 0;
			this->m_vraw2sim_0[3 * i + 1] = 3 * vsimNodeIdx[i] + 1;
			this->m_vraw2sim_0[3 * i + 2] = 3 * vsimNodeIdx[i] + 2;
		}
	}
}

void CurveMesh2DModel::getParam_LengthEdge(VectorXd& vl)
{
	if (!this->isReady_Rest())
		this->update_Rest();

	int ne = (int) this->m_vpStretchEles.size();
	
	vl.resize(ne);
	vl.setZero();

	for (int i = 0; i < ne; ++i)
	{
		vl(i) = static_cast<Curve2DEdgeElement*>(this->m_vpStretchEles[i])->getRestLength();
	}

	this->deprecateDeformation();
}

void CurveMesh2DModel::setParam_LengthEdge(const VectorXd& vl)
{
	if (!this->isReady_Rest())
		this->update_Rest();

	int ne = (int) this->m_vpStretchEles.size();

	assert(ne == (int) vl.size());

	for (int i = 0; i < ne; ++i)
	{
		static_cast<Curve2DEdgeElement*>(this->m_vpStretchEles[i])->setRestLength(vl(i));
	}

	this->m_isReady_Rest = true;
	this->deprecateDeformation();
}

void CurveMesh2DModel::initializeElements_Stretch()
{
	int ne = this->m_pSimMesh_0->getNumEdge();

	for (int i = 0; i < ne; ++i)
	{
		int rodIdx, edgeIdx;
		this->m_pSimMesh_0->getRodEdgeForMeshEdge(i, rodIdx, edgeIdx);
		Curve& curve = this->m_pSimMesh_0->getRod(rodIdx).getCurve();

		int pidxRod, nidxRod;
		int pidxMesh, nidxMesh;
		curve.getEdgeNeighborNodes(edgeIdx, pidxRod, nidxRod);
		this->m_pSimMesh_0->getMeshNodeForRodNode(pidxRod, rodIdx, pidxMesh);
		this->m_pSimMesh_0->getMeshNodeForRodNode(nidxRod, rodIdx, nidxMesh);
		int pidxMeshWhole = this->m_pSimMesh_0->getSplitToWholeMap()[pidxMesh][0];
		int nidxMeshWhole = this->m_pSimMesh_0->getSplitToWholeMap()[nidxMesh][0];

		NodalSolidElement* pEle = new Curve2DEdgeElement(this->m_dim0);

		iVector vinds(2);
		vinds[0] = pidxMeshWhole;
		vinds[1] = nidxMeshWhole;
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);

		this->m_vpEles.push_back(pEle);
		this->m_vpStretchEles.push_back(pEle);
	}
}

void CurveMesh2DModel::initializeElements_Bending()
{
	int nv = this->m_pSimMesh_0->getNumNode();
	int nc = this->m_pSimMesh_0->getNumCon();

	const vector<iVector>& vsplit2Whole = this->m_pSimMesh_0->getSplitToWholeMap();

	for (int i = 0; i < nv; ++i)
	{
		int rodIdx, nodeIdx;
		this->m_pSimMesh_0->getRodNodeForMeshNode(i, rodIdx, nodeIdx);
		Curve& curve = this->m_pSimMesh_0->getRod(rodIdx).getCurve();

		// In any case
		if (nodeIdx == 0)
			continue;

		int pidxRod, nidxRod, pidxEdge, nidxEdge;
		curve.getNodeNeighborNodes(nodeIdx, pidxRod, nidxRod);
		curve.getNodeNeighborEdges(nodeIdx, pidxEdge, nidxEdge);
		if (pidxRod == -1 || nidxRod == -1)
			continue; // Extreme, open curve

		Frame F0 = this->m_pSimMesh_0->getRod(rodIdx).getFrame(pidxEdge);
		Frame F1 = this->m_pSimMesh_0->getRod(rodIdx).getFrame(nidxEdge);
		Vector3d vref = (F0.nor + F1.nor).normalized();

		int pidxMesh, nidxMesh;
		this->m_pSimMesh_0->getMeshNodeForRodNode(pidxRod, rodIdx, pidxMesh);
		this->m_pSimMesh_0->getMeshNodeForRodNode(nidxRod, rodIdx, nidxMesh);
		int pidxMeshWhole = vsplit2Whole[pidxMesh][0];
		int nidxMeshWhole = vsplit2Whole[nidxMesh][0];
		int idxMeshWhole = vsplit2Whole[i][0];

		Curve2DAngleElement* pEle = new Curve2DAngleElement(this->m_dim0);

		iVector vinds(3);
		vinds[0] = pidxMeshWhole;
		vinds[1] = idxMeshWhole;
		vinds[2] = nidxMeshWhole;
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);
		pEle->setReferenceVector(vref);

		this->m_vpEles.push_back(pEle);
		this->m_vpBendingEles.push_back(pEle);
	}

	for (int i = 0; i < nc; ++i)
	{
		const Con& con = this->m_pSimMesh_0->getCon(i);

		set<pair<int, int>>::const_iterator
			it_cur0 = con.m_srods.begin(),
			it_end0 = con.m_srods.end();
		for (; it_cur0 != it_end0; ++it_cur0)
		{
			set<pair<int, int>>::const_iterator
				it_cur1 = it_cur0;

			it_cur1++;

			for (; it_cur1 != it_end0; ++it_cur1)
			{
				int r0 = it_cur0->first;
				int r1 = it_cur1->first;
				int s0 = it_cur0->second;
				int s1 = it_cur1->second;

				Rod& rod0 = this->m_pSimMesh_0->getRod(r0);
				Rod& rod1 = this->m_pSimMesh_0->getRod(r1);
				int ne0 = rod0.getNumEdge();
				int ne1 = rod1.getNumEdge();
				int e0 = (s0 == 0) ? 0 : ne0 - 1;
				int e1 = (s1 == 0) ? 0 : ne1 - 1;

				int node00_rod, node01_rod;
				int node10_rod, node11_rod;
				rod0.getCurve().getEdgeNeighborNodes(e0, node00_rod, node01_rod);
				rod1.getCurve().getEdgeNeighborNodes(e1, node10_rod, node11_rod);
				int node00_mesh, node01_mesh;
				int node10_mesh, node11_mesh;
				this->m_pSimMesh_0->getMeshNodeForRodNode(node00_rod, r0, node00_mesh);
				this->m_pSimMesh_0->getMeshNodeForRodNode(node01_rod, r0, node01_mesh);
				this->m_pSimMesh_0->getMeshNodeForRodNode(node10_rod, r1, node10_mesh);
				this->m_pSimMesh_0->getMeshNodeForRodNode(node11_rod, r1, node11_mesh);

				Frame F0 = this->m_pSimMesh_0->getRod(r0).getFrame(e0);
				Frame F1 = this->m_pSimMesh_0->getRod(r1).getFrame(e1);
				Vector3d vref = (F0.nor + F1.nor).normalized();

				int node00_mesh_whole = vsplit2Whole[node00_mesh][0];
				int node01_mesh_whole = vsplit2Whole[node01_mesh][0];
				int node10_mesh_whole = vsplit2Whole[node10_mesh][0];
				int node11_mesh_whole = vsplit2Whole[node11_mesh][0];

				Curve2DAngleElement* pEle = new Curve2DAngleElement(this->m_dim0);

				iVector vinds(3);

				if (s0 == 0 && s1 == 0)
				{
					vinds[0] = node01_mesh_whole;
					vinds[1] = node00_mesh_whole;
					vinds[2] = node11_mesh_whole;
					assert(node10_mesh_whole == node00_mesh_whole);
				}
				if (s0 == 0 && s1 == 1)
				{
					vinds[0] = node10_mesh_whole;
					vinds[1] = node11_mesh_whole;
					vinds[2] = node01_mesh_whole;
					assert(node11_mesh_whole == node00_mesh_whole);
				}
				if (s0 == 1 && s1 == 0)
				{
					vinds[0] = node00_mesh_whole;
					vinds[1] = node01_mesh_whole;
					vinds[2] = node11_mesh_whole;
					assert(node01_mesh_whole == node10_mesh_whole);
				}
				if (s0 == 1 && s1 == 1)
				{
					vinds[0] = node00_mesh_whole;
					vinds[1] = node01_mesh_whole;
					vinds[2] = node10_mesh_whole;
					assert(node01_mesh_whole == node11_mesh_whole);
				}

				pEle->setReferenceVector(vref);

				pEle->setNodeIndicesx(vinds);
				pEle->setNodeIndices0(vinds);

				this->m_vpEles.push_back(pEle);
				this->m_vpBendingEles.push_back(pEle);
			}
		}
	}
}

void CurveMesh2DModel::initializeMaterials()
{
	// Initialize elements materials

	for (int i = 0; i < this->getNumEle(); ++i) // Same all
		this->m_vpEles[i]->setMaterial(&this->m_material);

	if (this->m_matComp.getStretchK() != -1 ||
		this->m_matExte.getStretchK() != -1)
	{
		for (int i = 0; i < (int) this->m_vpStretchEles.size(); ++i)
		{
			Curve2DEdgeElement* pEle = static_cast<Curve2DEdgeElement*>(this->m_vpStretchEles[i]);
			pEle->setHeterogeneousMaterials(&this->m_matComp, 
											&this->m_matExte);
		}
	}
}

void CurveMesh2DModel::update_StateFromSimMesh()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (m_dim0 == 2)
	{
		dVector vxSTLSplit3D;
		dVector vXSTLSplit3D;
		dVector vXSTLSplit2D;
		dVector vxSTLSplit2D;
		dVector vxSTLWhole2D(this->m_nsimDOF_x);
		dVector vXSTLWhole2D(this->m_nsimDOF_0);
		this->m_pSimMesh_x->getPoints(vxSTLSplit3D);
		this->m_pSimMesh_0->getPoints(vXSTLSplit3D);
		to2D(vXSTLSplit3D, vXSTLSplit2D, 1);
		to2D(vxSTLSplit3D, vxSTLSplit2D, 1);
		applyIndexMap(this->m_pSimMesh_x->getSplitToWholeMap(), vxSTLSplit2D, 2, vxSTLWhole2D);
		applyIndexMap(this->m_pSimMesh_0->getSplitToWholeMap(), vXSTLSplit2D, 2, vXSTLWhole2D);
		toEigen(vxSTLWhole2D, this->m_state_x->m_vx);
		toEigen(vXSTLWhole2D, this->m_state_0->m_vx);
	}

	if (m_dim0 == 3)
	{
		dVector vxSTLSplit3D;
		dVector vXSTLSplit3D;
		dVector vxSTLSplit2D;
		dVector vxSTLWhole2D(this->m_nsimDOF_x);
		dVector vXSTLWhole3D(this->m_nsimDOF_0);
		this->m_pSimMesh_x->getPoints(vxSTLSplit3D);
		this->m_pSimMesh_0->getPoints(vXSTLSplit3D);
		to2D(vxSTLSplit3D, vxSTLSplit2D, 1);
		applyIndexMap(this->m_pSimMesh_x->getSplitToWholeMap(), vxSTLSplit2D, 2, vxSTLWhole2D);
		applyIndexMap(this->m_pSimMesh_0->getSplitToWholeMap(), vXSTLSplit3D, 3, vXSTLWhole3D);
		toEigen(vxSTLWhole2D, this->m_state_x->m_vx);
		toEigen(vXSTLWhole3D, this->m_state_0->m_vx);
	}

	this->m_isReady_Rest = false;
	this->deprecateDeformation();
	this->m_isReady_Mesh = true;
}

void CurveMesh2DModel::update_SimMeshFromState()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mesh)
		return; // Already

	if (this->m_dim0 == 2)
	{
		dVector vxSTLWhole2D;
		dVector vXSTLWhole2D;
		dVector vxSTLWhole3D;
		dVector vXSTLWhole3D;
		dVector vxSTLSplit3D(this->m_nrawDOF);
		dVector vXSTLSplit3D(this->m_nrawDOF);
		toSTL(this->m_state_x->m_vx, vxSTLWhole2D);
		toSTL(this->m_state_0->m_vx, vXSTLWhole2D);
		to3D(vxSTLWhole2D, vxSTLWhole3D, 1);
		to3D(vXSTLWhole2D, vXSTLWhole3D, 1);
		applyIndexMap(this->m_pSimMesh_x->getWholeToSplitMap(), vxSTLWhole3D, 3, vxSTLSplit3D);
		applyIndexMap(this->m_pSimMesh_0->getWholeToSplitMap(), vXSTLWhole3D, 3, vXSTLSplit3D);
		this->m_pSimMesh_x->setPoints(vxSTLSplit3D);
		this->m_pSimMesh_0->setPoints(vXSTLSplit3D);
	}

	if (this->m_dim0 == 3)
	{
		dVector vxSTLWhole2D;
		dVector vxSTLWhole3D;
		dVector vXSTLWhole3D;
		dVector vxSTLSplit3D(this->m_nrawDOF);
		dVector vXSTLSplit3D(this->m_nrawDOF);
		toSTL(this->m_state_x->m_vx, vxSTLWhole2D);
		toSTL(this->m_state_0->m_vx, vXSTLWhole3D);
		to3D(vxSTLWhole2D, vxSTLWhole3D, 1);
		applyIndexMap(this->m_pSimMesh_x->getWholeToSplitMap(), vxSTLWhole3D, 3, vxSTLSplit3D);
		applyIndexMap(this->m_pSimMesh_0->getWholeToSplitMap(), vXSTLWhole3D, 3, vXSTLSplit3D);
		this->m_pSimMesh_x->setPoints(vxSTLSplit3D);
		this->m_pSimMesh_0->setPoints(vXSTLSplit3D);
	}

	for (int i = 0; i < this->m_pSimMesh_0->getNumRod(); ++i)
	{
		this->m_pSimMesh_0->getRod(i).readaptFrames();
		this->m_pSimMesh_x->getRod(i).readaptFrames();
	}

	this->m_isReady_Mesh = true;
}

void CurveMesh2DModel::freeElements()
{
	SolidModel::freeElements();

	this->m_vpStretchEles.clear();
	this->m_vpBendingEles.clear();
}

void CurveMesh2DModel::freeMesh()
{
	if (this->m_pSimMesh_x != NULL)
		delete this->m_pSimMesh_x;
	if (this->m_pSimMesh_0 != NULL)
		delete this->m_pSimMesh_0;
	this->m_pSimMesh_0 = NULL;
	this->m_pSimMesh_x = NULL;
}