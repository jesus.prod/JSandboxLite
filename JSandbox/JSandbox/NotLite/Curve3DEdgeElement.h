/*=====================================================================================*/
/*!
\file		Curve3DEdgeElement.h
\author		jesusprod
\brief		Blah, blah, blah, TODO.
*/
/*=====================================================================================*/

#ifndef CURVE3D_EDGE_ELEMENT_H
#define CURVE3D_EDGE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>

#include <JSandbox/Element/NodalSolidElement.h>

class Curve3DEdgeElement : public NodalSolidElement
{
public:
	Curve3DEdgeElement(int dim0);
	~Curve3DEdgeElement();

	virtual void update_Rest(const VectorXd& vX);
	virtual void update_Mass(const VectorXd& vX);

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual void update_DfxDx(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DfxD0(const VectorXd& vx, const VectorXd& vX);

	virtual void update_DmxD0(const VectorXd& vx, const VectorXd& vX) { }

	virtual Real getIntegrationVolume() const { return this->m_L0; }

	virtual Real getRestLength() const { return this->m_L0; }
	virtual void setRestLength(Real L0) { this->m_L0 = L0; }

protected:
	Real m_L0; // Rest length
};

#endif