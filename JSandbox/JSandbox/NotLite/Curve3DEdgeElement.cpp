/*=====================================================================================*/
/*!
\file		Curve3DEdgeElement.cpp
\author		jesusprod
\brief		Implementation of Curve3DEdgeElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/Curve3DEdgeElement.h>

Curve3DEdgeElement::Curve3DEdgeElement(int dim0) : NodalSolidElement(2, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);
}

Curve3DEdgeElement::~Curve3DEdgeElement()
{
	// Nothing to do here
}

void Curve3DEdgeElement::update_Rest(const VectorXd& vX)
{
	if (this->m_numDim_0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
		this->m_L0 = (x1 - x0).norm();
	}
	else if (this->m_numDim_0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
		this->m_L0 = (x1 - x0).norm();
	}
	else assert(false);
}

void Curve3DEdgeElement::update_Mass(const VectorXd& vX)
{
	if (!this->m_addMass)
		return; // Ignore

	Real nodeMass = (1.0 / 2.0)*this->m_L0*this->m_vpmat[0]->getDensity();
	for (int i = 0; i < this->m_numDOFx; ++i)
		this->m_vmx[i] = nodeMass;
}

void Curve3DEdgeElement::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}

	double L = this->m_L0;
	double kS = m_vpmat[0]->getStretchK();

#include "../../Maple/Code/Curve3DEdgeEnergy.mcg"

	this->m_energy = t21;
}

void Curve3DEdgeElement::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}

	double L = this->m_L0;
	double kS = m_vpmat[0]->getStretchK();

	double vfx[6];

#include "../../Maple/Code/Curve3DEdgeForce.mcg"

	for (int i = 0; i < 6; ++i)
		if (!isfinite(vfx[i]))
		{
		logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);
		logSimu("Length: %.9f, Stiffness: %.9f", L, kS);
		}

	// Store force vector

	for (int i = 0; i < 6; ++i)
		this->m_vfVal(i) = vfx[i];
}

void Curve3DEdgeElement::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}

	double L = this->m_L0;

	double kS = m_vpmat[0]->getStretchK();

	double mJx[6][6];

#include "../../Maple/Code/Curve3DEdgeJacobian.mcg"

	for (int i = 0; i < 6; ++i)
		for (int j = 0; j < 6; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 6; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}

void Curve3DEdgeElement::update_DfxDx(const VectorXd& vx, const VectorXd& vX)
{
	// Get material points

	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);

	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
		}
	}
	else assert(false); // WTF?

	double kS = m_vpmat[0]->getStretchK();

	double mJx[6][6];

#include "../../Maple/Code/Curve3DEdgeRest_DfxDx.mcg"

	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j < this->m_numDOFx; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store matrix

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_x; ++k)
			{
				this->m_mDfxDx(i, this->m_numDim_x*j + k) = mJx[i][this->m_numDim_x*j + k];
			}
		}
	}
}

void Curve3DEdgeElement::update_DfxD0(const VectorXd& vx, const VectorXd& vX)
{
	// Get material points

	vector<double> x1(3);
	vector<double> x2(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
	}

	// Get rest points

	vector<double> X1(3);
	vector<double> X2(3);

	if (this->m_numDim_0 == 2)
	{
		for (int i = 0; i < 2; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[2 + i]);
		}

		// Keep at plane
		X1[2] = 0.0;
		X2[2] = 0.0;
	}
	else if (this->m_numDim_0 == 3)
	{
		for (int i = 0; i < 3; ++i)
		{
			X1[i] = vX(this->m_vidx0[0 + i]);
			X2[i] = vX(this->m_vidx0[3 + i]);
		}
	}
	else assert(false); // WTF?

	double kS = m_vpmat[0]->getStretchK();

	double mJX[6][6];

#include "../../Maple/Code/Curve3DEdgeRest_DfxD0.mcg"

	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j < this->m_numDOFx; ++j)
			if (!isfinite(mJX[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store matrix

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_0; ++k)
			{
				this->m_mDfxD0(i, this->m_numDim_0*j + k) = mJX[i][this->m_numDim_x*j + k];
			}
		}
	}
}