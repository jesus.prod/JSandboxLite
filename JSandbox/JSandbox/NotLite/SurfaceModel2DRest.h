/*=====================================================================================*/
/*!
\file		SurfaceModel2DRest.h
\author		jesusprod
\brief		Custom made model for generic deformable surfaces with 2D rest shape. It 
			provides the necessary functions to create a discretization using different 
			surface elements. The current implementation is restricted to triangle meshes.
*/
/*=====================================================================================*/

#ifndef SURFACE_MODEL_2DREST_2_H
#define SURFACE_MODEL_2DREST_2_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/TriMesh.h>

#include <JSandbox/Model/SurfaceModel.h>

#include <JSandbox/Element/DSHingeElement.h>

typedef struct BoundaryPair
{
	int m_compo = -1;
	int m_curve = -1;
	bool m_inverted = false;
}
BoundaryPair;

class JSANDBOX_EXPORT SurfaceModel2DRest : public SurfaceModel
{
public:

public:
	SurfaceModel2DRest();
	virtual ~SurfaceModel2DRest();

	/* From Solid Model */

	virtual void setup();
	virtual void update_Rest();
	virtual void update_SimDOF();

	iVector getAABBIndices(const dVector& vaabb) const;

	/* From Solid Model */

	/* From Surface Model */

	void setStateToMesh(const TriMesh& mesh);

	virtual void update_StateFromSimMesh();
	virtual void update_SimMeshFromState();
 
	virtual void remeshDelaunay(const pVector& vnodesExtI, 
								const iVector& vedgesExtI, 
								pVector& vnodesExtO, 
								int numV = -1, Real minA = 0.0, 
								Real internalMaxL = HUGE_VAL, 
								Real boundaryMaxL = HUGE_VAL);

	/* From Surface Model */

protected:

	virtual void freeElements();

	virtual void initializeElements_QuadArea();
	virtual void initializeElements_MassSpring();
	virtual void initializeElements_CSTStretch();
	virtual void initializeElements_Bending();

	void getMasterBoundarySplit(const Real& maxA, const Real& tolI, pVector& vMP, vector<vector<ModelCurve>>& vBS, vector<vector<BoundaryPair>>& vBP);

	void getMasterBoundaryPoints_Basic(const TriMesh* pMesh, vector<ModelCurve>& vBP);
	void addMasterBoundaryPoints_Angle(const TriMesh* pMesh, const Real& maxA, ModelCurve& vBP);
	void addMasterBoundaryPoints_Const(const TriMesh* pMesh, const Real& tolI, const pVector& vMP, ModelCurve& vBP);

protected:

	int m_plane;

	vector<iVector> m_vmapSplit2Whole;
	vector<iVector> m_vmapWhole2Split;
	map<int, int> m_mapBoundaryEdges;

	vector<DSHingeElement*> m_vpSeamBendingEles;
	vector<vector<TriMesh::EdgeHandle>> m_vSeamEleH;

};

#endif