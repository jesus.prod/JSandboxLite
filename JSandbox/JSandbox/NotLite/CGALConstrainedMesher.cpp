///*=====================================================================================*/
///*!
//\file		CGALConstrainedMesher.cpp
//\author		jesusprod
//\brief		Implementation of CGALConstrainedMesher.h
//*/
///*=====================================================================================*/
//
//
////#include <vector>
////typedef CGAL::Exact_predicates_exact_constructions_kernel        K;
////typedef CGAL::Polygon_2<K>                                       Polygon_2;
////typedef CGAL::Triangulation_vertex_base_2<K>                     Vb;
////typedef CGAL::Constrained_triangulation_face_base_2<K>           Fb;
////typedef CGAL::Triangulation_data_structure_2<Vb, Fb>              TDS;
////typedef CGAL::Exact_intersections_tag                            Itag;
////typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, Itag>  CDT;
////typedef CGAL::Constrained_triangulation_plus_2<CDT>              CDTP;
////typedef CDTP::Point                                              Point;
////typedef CDTP::Constraint_id                                      Cid;
////typedef CDTP::Vertex_handle                                      Vertex_handle;
////void
////print(const CDTP& cdtp, Cid cid)
////{
////	typedef CDTP::Vertices_in_constraint Vertices_in_constraint;
////	std::cout << "Polyline constraint:" << std::endl;
////	for (Vertices_in_constraint it = cdtp.vertices_in_constraint_begin(cid);
////		it != cdtp.vertices_in_constraint_end(cid);
////		it++){
////		Vertex_handle vh = *it;
////		std::cout << vh->point() << std::endl;
////	}
////}
////void
////contexts(const CDTP& cdtp)
////{
////	CDTP::Subconstraint_iterator
////		beg = cdtp.subconstraints_begin(),
////		end = cdtp.subconstraints_end();
////	for (; beg != end; ++beg){
////		Vertex_handle vp = beg->first.first, vq = beg->first.second;
////		if (cdtp.number_of_enclosing_constraints(vp, vq) == 2){
////			CDTP::Context_iterator cbeg = cdtp.contexts_begin(vp, vq),
////				cend = cdtp.contexts_end(vp, vq);
////			std::cout << "subconstraint " << vp->point() << " " << vq->point()
////				<< " is on constraints starting at:\n";
////			for (; cbeg != cend; ++cbeg){
////				CDTP::Context c = *cbeg;
////				std::cout << (*(c.vertices_begin()))->point() << std::endl;
////			}
////		}
////	}
////}
////int
////main()
////{
////	CDTP cdtp;
////	cdtp.insert_constraint(Point(0, 0), Point(1, 1));
////	std::vector<Point> points;
////	points.push_back(Point(1, 1));
////	points.push_back(Point(5, 2));
////	points.push_back(Point(6, 0));
////	points.push_back(Point(3, 0));
////	Cid id1 = cdtp.insert_constraint(points.begin(), points.end());
////	print(cdtp, id1);
////	Polygon_2 poly;
////	poly.push_back(Point(2, 3));
////	poly.push_back(Point(4, 0));
////	poly.push_back(Point(5, 0));
////	poly.push_back(Point(6, 2));
////	Cid id2 = cdtp.insert_constraint(poly.vertices_begin(), poly.vertices_end(), true);
////	print(cdtp, id1);
////	print(cdtp, id2);
////	contexts(cdtp);
////	return 0;
////}
////10 The Triangulation Hierarchy
//
//#include <JSandbox/Geometry/CGALConstrainedMesher.h>
//
//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
//#include <CGAL/Constrained_Delaunay_triangulation_2.h>
//#include <CGAL/Constrained_triangulation_plus_2.h>
//#include <CGAL/Delaunay_mesher_2.h>
//#include <CGAL/Delaunay_mesh_face_base_2.h>
//#include <CGAL/Delaunay_mesh_size_criteria_2.h>
//
//typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
//typedef CGAL::Triangulation_vertex_base_2<Kernel> VertexBase;
//typedef CGAL::Delaunay_mesh_face_base_2<Kernel> FaceBase;
//
//typedef CGAL::Triangulation_data_structure_2<VertexBase, FaceBase> TDS;
//typedef CGAL::Constrained_Delaunay_triangulation_2<Kernel, TDS> CDT;
//
//typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> Criteria;
//typedef CGAL::Delaunay_mesher_2<CDT, Criteria> Mesher;
//typedef CDT::Vertex_handle VertexH;
//typedef CDT::Point Point;
//
//CGALConstrainedMesher::CGALConstrainedMesher()
//{
//	this->m_minAngle = 0.125;
//	this->m_maxLength = 0.100;
//}
//
//CGALConstrainedMesher::~CGALConstrainedMesher()
//{
//	// Nothing to do here...
//}
//
//bool CGALConstrainedMesher::createConstrainedMesh(const dVector& vnodesI, const iVector& vedgesI, dVector& vnodesO, iVector& vfacesO, iVector& vnewCons, string& error)
//{
//	if ((int) vnodesI.size() % 2 != 0) throw new exception("[ERROR] Invalid vertex buffer size");
//	if ((int) vedgesI.size() % 2 != 0) throw new exception("[ERROR] Invalid edge buffer size");
//
//	int nv = (int)vnodesI.size() / 2;
//	int ne = (int)vedgesI.size() / 2;
//
//	// Are points constrained?
//	bVector visCon(nv, false);
//	for (int i = 0; i < ne; ++i)
//	{
//		int offset = 2 * i;
//		visCon[vedgesI[offset + 0]] = true;
//		visCon[vedgesI[offset + 1]] = true;
//	}
//
//	// Build matrices
//
//	MatrixXd mNodes(nv, 2);
//	MatrixXd mEdges(ne, 2);
//	for (int i = 0; i < nv; ++i)
//	{
//		mNodes(i, 0) = vnodesI[2 * i + 0];
//		mNodes(i, 1) = vnodesI[2 * i + 1];
//	}
//	for (int i = 0; i < ne; ++i)
//	{
//		mEdges(i, 0) = vedgesI[2 * i + 0];
//		mEdges(i, 1) = vedgesI[2 * i + 1];
//	}
//	//writeToFile(mNodes, "remeshingNodes.csv");
//	//writeToFile(mEdges, "remeshingEdges.csv");
//
//	CDT cdtp;
//
//	// Add individual nodes
//	vector<VertexH> vvh(nv);
//	for (int i = 0; i < nv; ++i)
//	{
//		int offset = 2*i;
//		vvh[i] = cdtp.insert(Point(vnodesI[offset + 0], vnodesI[offset + 1]));
//	}
//	// Add constrained edges
//	for (int i = 0; i < ne; ++i)
//	{
//		int offset = 2*i;
//		cdtp.insert_constraint(vvh[vedgesI[offset + 0]], vvh[vedgesI[offset + 1]]);
//	}
//
//	// Select 
//	std::list<Point> pointList;
//
//	// Use any point outside the domain
//	pointList.push_back(Point(1e3, 1e3));
//
//	CGAL::refine_Delaunay_mesh_2(cdtp, 
//								 pointList.begin(), pointList.end(),
//								 Criteria(m_minAngle, m_maxLength), 
//								 false);
//	int nvNew = (int)cdtp.number_of_vertices();
//	int nfNew = (int)cdtp.number_of_faces();
//
//	vnodesO.clear();
//	vfacesO.clear();
//
//	// Create output buffers
//	vnodesO.reserve(nvNew*2);
//	vfacesO.reserve(nfNew*3);
//	map<VertexH, int> vhmap;
//	int count = 0;
//
//	for (CDT::Finite_vertices_iterator v_it = cdtp.finite_vertices_begin(); v_it != cdtp.finite_vertices_end(); ++v_it)
//	{
//		vnodesO.push_back(v_it->point().x());
//		vnodesO.push_back(v_it->point().y());
//		vhmap[v_it->handle()] = count;
//
//		if (count >= nv)
//		{
//			CDT::Edge_circulator ec = v_it->incident_edges();
//			CGAL::Container_from_circulator<CDT::Edge_circulator> ecc(ec);
//			CGAL::Container_from_circulator<CDT::Edge_circulator>::iterator
//				itEnd = ecc.end(),
//				itCur = ecc.begin();
//			for (; itCur != itEnd; ++itCur)
//			{
//				if (cdtp.is_constrained(*itCur))
//				{
//					vnewCons.push_back(count);
//					break; // Already found
//				}
//			}
//		}
//
//		count++;
//	}
//
//	for (CDT::Finite_faces_iterator f_it = cdtp.finite_faces_begin(); f_it != cdtp.finite_faces_end(); ++f_it)
//	{
//		if (!f_it->is_in_domain())
//			continue; // Ignore
//
//		vfacesO.push_back(vhmap[f_it->vertex(0)]);
//		vfacesO.push_back(vhmap[f_it->vertex(1)]);
//		vfacesO.push_back(vhmap[f_it->vertex(2)]);
//	}
//	
//	logSimu("");
//	logSimu("[TRACE] New mesh created from %d nodes and %d edges", nv, ne);
//	logSimu("Criteria used: (%f,%f)", this->m_minAngle, this->m_maxLength);
//	logSimu("\t- Resulting nodes: %d", (int) vnodesO.size()/2);
//	logSimu("\t- Resulting faces: %d", (int) vfacesO.size()/2);
//	logSimu("");
//
//	return true;
//}