///*=====================================================================================*/
///*!
//\file		SQPOptimizer.cpp
//\author		jesusprod
//\brief		Implementation of SQPOptimizer.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Optim/SQPOptimizer.h>
//
//SQPOptimizer::SQPOptimizer(const string& ID)
//{
//	this->m_ID = ID;
//}
//
//SQPOptimizer::~SQPOptimizer()
//{
//	// Nothing to do here
//}
//
//void SQPOptimizer::setSolver(PhysSolver* pSolver)
//{
//	assert(pSolver != NULL);
//	this->m_pSolver = pSolver;
//}
//
//
//void SQPOptimizer::setModel(SolidModel* pModel)
//{
//	assert(pModel != NULL);
//	this->m_pModel = pModel;
//}
//
//void SQPOptimizer::setTarget(const VectorXd& vt)
//{
//	this->m_vxT = vt;
//}
//
//void SQPOptimizer::getTarget(VectorXd& vt) const
//{
//	vt = this->m_vxT;
//}
//
//void SQPOptimizer::setFixedParameters(const iVector& vidx)
//{
//	this->m_vfixIdx = vidx;
//}
//
//const iVector& SQPOptimizer::getFixedParameters() const
//{
//	return this->m_vfixIdx;
//}
//
//void SQPOptimizer::setup()
//{
//	this->m_nX = this->m_pModel->getNumRawDOF(); // Rest configuration
//	this->m_nx = this->m_pModel->getNumSimDOF_x(); // Degrees-of-freedom
//	this->m_np = this->m_nx + this->m_nX;
//	this->init();
//}
//
//bool SQPOptimizer::init()
//{
//	// Initialize vectors
//
// 	this->m_vl = VectorXd(this->m_nx);
//	this->m_vl.setZero(); // From zero
//
//	this->fixConstraintVector(this->m_vl);
//
//	this->m_mAs = SparseMatrixXd(this->m_nx, this->m_np);
//
//	this->pullParameters();
//
//	this->restartConstraints();
//	this->restartGradient();
//	this->restartHessian();
//
//	this->updateConstraints();
//	this->updateGradient();
//	this->updateHessian();
//
//	//this->m_pModel->testD2fxDxxLocal();
//	//this->m_pModel->testD2fxDx0Local();
//	//this->m_pModel->testD2fxD00Local();
//
//	//this->m_pModel->testD2fxDxxGlobal();
//	//this->m_pModel->testD2fxDx0Global();
//	//this->m_pModel->testD2fxD00Global();
//
//	//this->testThirdOrderTensor();
//
//	//this->testGradient();
//	//this->testHessian();
//
//	// Initialize constants
//
//	this->m_maxConst = 1e-6*max(this->m_vC.cwiseAbs().maxCoeff(), 1.0);
//	this->m_maxGradi = 1e-6*max(this->m_vg.cwiseAbs().maxCoeff(), 1.0);
//
//	this->m_mu = 0.1;
//	this->m_tau = 0.2;
//	this->m_psi = 10.0;
//	this->m_alpha = 0.5;
//	this->m_gamma = 1e-2;
//	this->m_theta = 1e-8;
//	this->m_sigma = this->m_tau * (1 - this->m_gamma);
//
//	this->pushParameters();
//
//	//logSimu("Target loaded: %s", vectorToString(this->m_vxT).c_str());
//
//	return true;
//}
//
//bool SQPOptimizer::step()
//{
//	//this->testGradient();
//
//	this->pullParameters();
//	this->updateConstraints();
//	this->updateGradient();
//
//	// Current QP converged
//	Real constNorm = this->m_vC.norm();
//	Real gradiNorm = this->m_vG.norm();
//
//	if (constNorm <= this->m_maxConst && 
//		gradiNorm <= this->m_maxGradi)
//		return true;
//	else
//	{
//		logSimu("[TRACE] Starting quadratic step\n");
//
//		Real Ll = this->computeMeritError();
//		Real L = 0.0;
//
//		logSimu("[TRACE] Merit function: %.9f\n", Ll);
//
//		// QP step for current constraints
//
//		bool improved = false;
//		VectorXd vpl = this->m_vp;
//		VectorXd vll = this->m_vl;
//
//		VectorXd vq; // QP
//		this->getStep(vq);
//
//		VectorXd vd; // Get parameters step
//		getSubvector(0, this->m_np, vq, vd);
//
//		VectorXd ve; // Get Lagrange multipliers step
//		getSubvector(this->m_np, this->m_nx, vq, ve);
//
//		this->fixConstraintVector(ve); // Fix zeros
//
//		logSimu("[TRACE] Parameters step lenght: %.9f\n", vd.norm());
//		logSimu("[TRACE] Multipliers step lenght: %.9f\n", ve.norm());
//
//		int i = 0;
//		Real beta = 1.0;
//		for (i = 0; i < 20; ++i)
//		{
//			this->m_vp += beta*vd;
//			this->m_vl += beta*ve;
//
//			this->pushParameters();
//			this->updateConstraints();
//			this->updateGradient();
//
//			L = this->computeMeritError();
//
//			if (L < Ll)
//			{
//				improved = true;
//				break; // Return
//			}
//
//			beta *= this->m_alpha;
//			this->m_vp = vpl;
//			this->m_vl = vll;
//		}
//
//		if (!improved)
//		{
//			this->pushParameters();
//			this->updateConstraints();
//			this->updateGradient();
//
//			logSimu("[TRACE] Halted after %d step\n", i);
//
//			exit(-1);
//		}
//		else
//		{
//			logSimu("[TRACE] Improved after %d step\n", i);
//		}
//
//		Real Cnorm = this->m_vC.norm();
//		Real Gnorm = this->m_vG.norm();
//
//		logSimu("[TRACE] LS Merit function: %.9f\n", L);
//		logSimu("[TRACE] Constraint norm: %.9f\n", Cnorm);
//		logSimu("[TRACE] Gradient norm: %.9f\n", Gnorm);
//	}
//
//	logSimu("-------------------------------------------------------\n");
//
//	return true;
//}
//
//void SQPOptimizer::getStep(VectorXd& vq)
//{
//	updateHessian();
//
//	// Update theta parameter for acceptance condition
//	this->m_theta = 1e-8*this->m_mWs.cwiseAbs().sum();
//
//	// N. parameters + N. constraints
//	int N = this->m_np + this->m_nx;
//
//	tVector vA;
//	SparseMatrixXd mAs(N, N);
//	VectorXd vb(N);
//
//	// Matrix assembly
//
//	vA.insert(vA.end(), this->m_vWs.begin(), this->m_vWs.end());
//
//	for (int i = 0; i < (int) this->m_vAs.size(); ++i)
//	{
//		const Triplet<Real>& t = this->m_vAs[i];
//		vA.push_back(Triplet<Real>(this->m_np + t.row(), t.col(), t.value())); // Assemble A
//		vA.push_back(Triplet<Real>(t.col(), this->m_np + t.row(), t.value()));	// Assemble A^T
//	}
//
//	SparseMatrixXd mA(this->m_nx, this->m_np);
//	mA.setFromTriplets(this->m_vAs.begin(), this->m_vAs.end());
//	mA.makeCompressed();
//	writeToFile(mA, "ConstraintJacobian.csv");
//
//	mAs.setFromTriplets(vA.begin(), vA.end());
//	mAs.makeCompressed(); // Compress structure
//
//	// Right-hand side
//
//	for (int i = 0; i < this->m_np; ++i)
//		vb(i) = -this->m_vG(i); // DLDp
//
//	for (int i = 0; i < this->m_nx; ++i)
//		vb(this->m_np + i) = -this->m_vC(i);
//
//	//writeToFile(this->m_mWs, "QPHessianMatrix.csv");
//	//writeToFile(this->m_mAs, "QPConstraintsJ.csv");
//	//writeToFile(mAs, "QPSystemMatrix.csv");
//	//writeToFile(vb, "QPSystemRHS.csv");
//
//	// Solve s.t. regularization
//	for (int i = 0; i < 50; ++i)
//	{
//		if (i != 0)
//		{
//			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//
//			// Regularize diagonal
//			for (int ii = 0; ii < this->m_np; ++ii)
//				mAs.coeffRef(ii, ii) += 0.0001*pow(10.0, i);
//		}
//
//		SparseLU<SparseMatrixXd> solver;
//		
//		solver.analyzePattern(mAs);
//		solver.factorize(mAs);
//		vq = solver.solve(vb);
//		if (solver.info() != Success)
//			continue; // Iterate again
//
//		/////////////////////// TEST ///////////////////////
//		VectorXd vbTest = mAs.selfadjointView<Lower>()*vq;
//		double error = (vb - vbTest).squaredNorm();
//		if (error > 1e-9) // Iterate again
//			continue;
//		/////////////////////// TEST ///////////////////////
//
//		VectorXd vd; // Get parameters step
//		getSubvector(0, this->m_np, vq, vd);
//
//		Real dWd05 = 0.5*vd.transpose()*this->m_mWs*vd;
//		Real tBound = this->computeTangentialBound(vd);
//		Real nBound = this->computeOrthogonalBound(vd);
//		Real Cnorm = this->m_vC.norm();
//
//		Real modelOBJBound = max(dWd05, this->m_theta*tBound);
//		Real modelCONBound = this->m_sigma*this->m_mu*Cnorm;
//		Real modelReduction = this->computeModelReduction(vd);
//
//		// Termination Test 1
//
//		if (modelReduction >= modelOBJBound + modelCONBound)
//		{
//			logSimu("[TRACE] T1 Passed. Step accepted. Current mu: %f\n", this->m_mu);
//			break;
//		}
//
//		// Termination Test 2
//
//		if (dWd05 >= this->m_theta*tBound || this->m_psi*nBound >= tBound)
//		{
//			logSimu("[TRACE] T2 Passed. Step accepted. Checking mu...\n");
//
//			Real muTrial = (this->m_vg.dot(vd) + modelOBJBound) / ((1.0 - this->m_tau)*Cnorm);
//
//			if (this->m_mu < muTrial)
//			{
//				this->m_mu = muTrial + 1e-4;
//
//				logSimu("[TRACE] Mu updated: %f\n", this->m_mu);
//			}
//			else
//			{
//				logSimu("[TRACE] Mu keeped: %f\n", this->m_mu);
//			}
//
//			break;
//		}
//	}
//}
//
//Real SQPOptimizer::computeObjective() const
//{
//	VectorXd vxD = this->m_pModel->getPositions_x() - this->m_vxT;
//	return 0.5 * vxD.dot(vxD); // Position error deformed|target
//}
//
//Real SQPOptimizer::computeLagrangian() const
//{
//	return  this->computeObjective() + this->m_vl.dot(this->m_vC);
//}
//
//Real SQPOptimizer::computeMeritError() const
//{
//	return this->computeObjective() + this->m_mu * this->m_vC.norm();
//}
//
//Real SQPOptimizer::computeModelReduction(const VectorXd& vd) const
//{
//	return computeModelReduction_OBJ(vd) + 
//		   computeModelReduction_CON(vd);
//}
//
//Real SQPOptimizer::computeModelReduction_OBJ(const VectorXd& vd) const
//{
//	return -vd.dot(this->m_vg);
//}
//
//Real SQPOptimizer::computeModelReduction_CON(const VectorXd& vd) const
//{
//	return this->m_mu*(this->m_vC.norm() - (this->m_vC + this->m_mAs*vd).norm());
//}
//
//Real SQPOptimizer::computeTangentialBound(const VectorXd& vd) const
//{
//	return vd.squaredNorm() - this->computeOrthogonalBound(vd);
//}
//
//Real SQPOptimizer::computeOrthogonalBound(const VectorXd& vd) const
//{
//	return (this->m_mAs*vd).squaredNorm() / this->m_mAs.squaredNorm();;
//}
//
//void SQPOptimizer::restartConstraints()
//{
//	this->m_vC.resize(this->m_nx);
//	this->m_vC.setZero(); // Init.
//
//	this->m_mAs.resize(this->m_nx, this->m_np);
//	this->m_mAs.setZero(); // Empty Jacobian
//
//	this->m_vAs.clear();
//
//	logSimu("[TRACE] Constraints value restarted. Norm: %.9f\n", this->m_vC.norm());
//	logSimu("[TRACE] Constraints Jacobian restarted. Norm: %.9f\n", this->m_mAs.norm());
//}
//
//void SQPOptimizer::updateConstraints()
//{
//	// Constraint vector
//
//	VectorXd vx = this->m_pModel->getPositions_x();
//	VectorXd vv = this->m_pModel->getVelocities_x();
//
//	this->m_vC.resize(this->m_nx);
//	this->m_vC.setZero(); // Init.
//
//	this->m_pModel->addForce(this->m_vC, NULL); // Internal
//	this->m_pSolver->addBoundaryForce(vx, vv, this->m_vC);
//
//	this->fixConstraintVector(this->m_vC); // Set fixed
//
//	// Constraint Jacobian
//
//	// TODO: Reserve here
//	this->m_vAs.clear();
//
//	// Derivative w.r.t. material
//
//	// TODO: Reserve here
//	tVector vDfxDx;
//	this->m_pModel->add_DfxDx(vDfxDx, NULL); // Internal
//	this->m_pSolver->addBoundaryJacobian(vx, vv, vDfxDx);
//
//	// Derivative w.r.t. rest
//
//	// TODO: Reserve here
//	tVector vDfxDX;
//	m_pModel->add_DfxD0(vDfxDX);
//
//	int numCoeffsx = (int)vDfxDx.size();
//	int numCoeffsX = (int)vDfxDX.size();
//
//	for (int i = 0; i < numCoeffsx; ++i)
//	{
//		this->m_vAs.push_back(vDfxDx[i]);
//	}
//	for (int i = 0; i < numCoeffsX; ++i)
//	{
//		this->m_vAs.push_back(Triplet<Real>(vDfxDX[i].row(), vDfxDX[i].col() + this->m_nx, vDfxDX[i].value()));
//	}
//
//	this->fixConstraintJacobian(this->m_vAs); // Set fixed points
//	this->m_mAs.setFromTriplets(this->m_vAs.begin(), this->m_vAs.end());
//	this->m_mAs.makeCompressed(); // Compress structure (non-symmetric)
//
//	//logSimu("[TRACE] Constraints value updated. Norm: %.9f\n", this->m_vC.norm());
//	//logSimu("[TRACE] Constraints Jacobian updated. Norm: %.9f\n", this->m_mAs.norm());
//}
//
//void SQPOptimizer::restartGradient()
//{
//	// Objective gradient
//	this->m_vg.resize(this->m_np);
//	this->m_vg.setZero(); // Init
//
//	// Lagrangian gradient
//	this->m_vG.resize(this->m_np);
//	this->m_vG.setZero(); // Init.
//
//	logSimu("[TRACE] Objective gradient restarted. Norm: %.9f\n", this->m_vg.norm());
//	logSimu("[TRACE] Lagrangian gradient restarted. Norm: %.9f\n", this->m_vG.norm());
//}
//
//void SQPOptimizer::updateGradient()
//{
//	updateGradientA();
//	// updateGradientFD();
//}
//
//void SQPOptimizer::updateHessian()
//{
//	//updateHessianA();
//	updateHessianFD();
//	//updateHessianBFGS();
//}
//
//void SQPOptimizer::updateGradientA()
//{
//	const VectorXd& vx = this->m_pModel->getPositions_x();
//	const VectorXd& vv = this->m_pModel->getVelocities_x();
//
//	VectorXd vxD = vx - this->m_vxT;
//
//	this->m_vg.setZero();
//	this->m_vG.setZero();
//	setSubvector(0, this->m_nx, vxD, this->m_vg);
//	setSubvector(0, this->m_nx, vxD, this->m_vG);
//	this->m_vG += this->m_vl.transpose()*this->m_mAs;
//
//	this->fixLagrangianVector(this->m_vg);
//	this->fixLagrangianVector(this->m_vG);
//
//	//logSimu("[TRACE] Objective gradient updated (A). Norm: %.9f\n", this->m_vg.norm());
//	//logSimu("[TRACE] Lagrangian gradient updated (A). Norm: %.9f\n", this->m_vG.norm());
//}
//
//void SQPOptimizer::updateGradientFD()
//{
//	Real EPS = 1e-6;
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lp = this->computeLagrangian();
//		Real fp = this->computeObjective();
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lm = this->computeLagrangian();
//		Real fm = this->computeObjective();
//
//		// Estimate
//		this->m_vg[i] = (fp - fm) / (2 * EPS);
//		this->m_vG[i] = (Lp - Lm) / (2 * EPS);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//
//	this->fixLagrangianVector(this->m_vg);
//	this->fixLagrangianVector(this->m_vG);
//
//	//logSimu("[TRACE] Objective gradient updated (FD). Norm: %.9f\n", this->m_vg.norm());
//	//logSimu("[TRACE] Lagrangian gradient updated (FD). Norm: %.9f\n", this->m_vG.norm());
//}
//
//void SQPOptimizer::restartHessian()
//{
//	this->m_mW.resize(this->m_np, this->m_np);
//	this->m_mW.setIdentity(); // Init. from I
//
//	m_modifier1.resize(this->m_np, this->m_np);
//	m_modifier2.resize(this->m_np, this->m_np);
//	m_tempMatrix.resize(this->m_np, this->m_np);
//
//	m_sk.resize(this->m_np);
//	m_yk.resize(this->m_np);
//	m_vp_P = this->m_vp;
//	m_vg_P = this->m_vG;
//
//	this->m_mWs.resize(this->m_np, this->m_np);
//	this->m_mWs.setZero(); // Empty Hessian
//	this->m_vWs.clear();
//
//	logSimu("[TRACE] Lagrangian Hessian restarted. Identity\n");
//}
//
//void SQPOptimizer::updateHessianA()
//{
//	// TODO: Reserve space here
//
//	this->m_vWs.clear();
//
//	// D2{f(p)}/Dp2 = I
//	for (int i = 0; i < this->m_nx; ++i)
//		this->m_vWs.push_back(Triplet<Real>(i, i, 1.0));
//
//	// D2{L^T*C(p)}/Dp2 = Sum_i [ L_i * D2{F_i(p)}/Dp2 ]
//
//	// TODO: Reserve space here
//
//	vector<tVector> vD2FxDxx(this->m_nx);
//	vector<tVector> vD2FxDxX(this->m_nX);
//	vector<tVector> vD2FxDXX(this->m_nX);
//
//	for (int k = 0; k < this->m_nx; ++k)
//	{
//		int kMat = k;
//
//		for (int i = 0; i < (int)vD2FxDxx[k].size(); ++i)
//		{
//			const Triplet<Real>& t = vD2FxDxx[k][i];
//			int iMat = t.row();
//			int jMat = t.col();
//			this->m_vWs.push_back(Triplet<Real>(kMat, jMat, this->m_vl[iMat] * t.value()));
//		}
//	}
//
//	for (int k = 0; k < this->m_nX; ++k)
//	{
//		int kRes = this->m_nx + k;
//
//		for (int i = 0; i < (int)vD2FxDxX[k].size(); ++i)
//		{
//			const Triplet<Real>& t = vD2FxDxX[k][i];
//			int iMat = t.row();
//			int jMat = t.col();
//			int jRes = jMat + this->m_nx;
//			this->m_vWs.push_back(Triplet<Real>(kRes, jMat, this->m_vl[iMat] * t.value()));
//			this->m_vWs.push_back(Triplet<Real>(jMat, kRes, this->m_vl[iMat] * t.value()));
//		}
//	}
//
//	for (int k = 0; k < this->m_nX; ++k)
//	{
//		int kRes = this->m_nx + k;
//
//		for (int i = 0; i < (int)vD2FxDXX[k].size(); ++i)
//		{
//			const Triplet<Real>& t = vD2FxDXX[k][i];
//			int iMat = t.row();
//			int jMat = t.col();
//			int jRes = jMat + this->m_nx;
//			this->m_vWs.push_back(Triplet<Real>(kRes, jRes, this->m_vl[iMat] * t.value()));
//		}
//	}
//
//	this->fixLagrangianHessian(this->m_vWs);
//	this->m_mWs.setFromTriplets(this->m_vWs.begin(), this->m_vWs.end());
//	this->m_mWs.makeCompressed(); // Compress sparse structure (symmetric)
//
//	logSimu("[TRACE] Lagrangian Hessian updated (A)\n");
//}
//
//void SQPOptimizer::updateHessianFD()
//{
//	Real EPS = 1e-6;
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		this->updateGradient();
//
//		VectorXd vgp = this->m_vG;
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		this->updateGradient();
//
//		VectorXd vgm = this->m_vG;
//
//		// Estimate
//		VectorXd Wcol = (vgp - vgm) / (2 * EPS);
//		for (int j = 0; j < this->m_np; ++j)
//			this->m_mW(j, i) = Wcol(j);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//	this->updateGradient();
//
//	// TODO: Reserve space here
//
//	this->m_vWs.clear();
//	for (int i = 0; i < this->m_np; ++i)
//		for (int j = 0; j < this->m_np; ++j) // Get entries (all entries)
//			this->m_vWs.push_back(Triplet<Real>(i, j, this->m_mW(i, j)));
//	
//	this->fixLagrangianHessian(this->m_vWs);
//	this->m_mWs.setFromTriplets(this->m_vWs.begin(), this->m_vWs.end());
//	this->m_mWs.makeCompressed(); // Compress sparse structure (symmetric)
//
//	writeToFile(this->m_mWs, "LagrangianHessian.csv");
//
//	logSimu("[TRACE] Lagrangian Hessian updated (FD)\n");
//}
//
//void SQPOptimizer::updateHessianBFGS()
//{
//	m_sk = this->m_vp - this->m_vp_P;
//	m_yk = this->m_vG - this->m_vg_P;
//
//	this->m_vp_P = this->m_vp;
//	this->m_vg_P = this->m_vG;
//
//	double D = m_yk.dot(m_sk);
//
//	if (fabs(D) > 1e-6) // ?
//	{
//		m_rho = 1.0 / D; // For too small discriminant, the update is invalid
//
//		// Update the Hessian matrix (direct one) (Numerical Optimization, Nocedal, Wright, page 25)
//		// Hk+1 = (I - rho * m_sk * m_yk^T) * Hk * (I - rho * m_yk * m_sk^T) + rho * m_yk * m_yk^T
//		for (int i = 0; i < this->m_np; i++)
//		{
//			for (int j = 0; j <= i; j++)
//			{
//				if (i == j)
//				{
//					m_modifier1(i, i) = 1.0 - m_rho * m_yk[i] * m_sk[i];
//				}
//				else
//				{
//					m_modifier1(i, j) = -m_rho * m_sk[i] * m_yk[j];
//					m_modifier1(j, i) = -m_rho * m_sk[j] * m_yk[i];
//				}
//
//				m_modifier2(i, j) = m_rho * m_yk[i] * m_yk[j];
//				m_modifier2(j, i) = m_rho * m_yk[j] * m_yk[i];
//			}
//		}
//
//		this->m_mW = m_modifier1*this->m_mW*m_modifier1.transpose();
//		this->m_mW = this->m_mW + m_modifier2; // Add Hessian modifier
//
//		// TODO: Reserve space here
//
//		this->m_vWs.clear();
//		for (int i = 0; i < this->m_np; ++i)
//			for (int j = 0; j < this->m_np; ++j) // Get entries (all entries)
//				this->m_vWs.push_back(Triplet<Real>(i, j, this->m_mW(i, j)));
//
//		this->fixLagrangianHessian(this->m_vWs);
//		this->m_mWs.setFromTriplets(this->m_vWs.begin(), this->m_vWs.end());
//		this->m_mWs.makeCompressed(); // Compress sparse structure (symmetric)
//
//		logSimu("[TRACE] Lagrangian Hessian updated (BFGS)\n");
//	}
//	else
//	{
//		// Initialize to FD Hess
//		this->updateHessianFD();
//	}
//}
//
//void SQPOptimizer::testGradient()
//{
//	this->updateGradientFD();
//	VectorXd vgFD = this->m_vG;
//
//	this->updateGradientA();
//	VectorXd vgA = this->m_vG;
//
//	VectorXd vgDiff = vgA - vgFD;
//	Real vgDiffNorm = vgDiff.norm();
//	Real vgFDNorm = vgFD.norm();
//	if (vgFDNorm > 1e-9)
//	{
//		Real relativeError = vgDiffNorm / vgFDNorm;
//		if (relativeError > 1e-9) // Significative
//		{
//			writeToFile(vgA, "LagrangianGradientA.csv");
//			writeToFile(vgFD, "LagrangianGradientFD.csv");
//			writeToFile(vgDiff, "LagrangianGradientDiff.csv");
//
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//			logSimu("[ERROR] Gradient test. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgFDNorm, vgDiffNorm, relativeError);
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//		}
//	}
//}
//
//void SQPOptimizer::testHessian()
//{
//	this->updateHessianFD(); // Copy dense
//	MatrixXd mHFD = this->m_mWs.toDense();
//
//	this->updateHessianA(); // Copy dense
//	MatrixXd mHA = this->m_mWs.toDense();
//
//	MatrixXd mHDiff = mHA - mHFD;
//	Real mHDiffNorm = mHDiff.norm();
//	Real mHFDNorm = mHFD.norm();
//	if (mHFDNorm > 1e-9)
//	{
//		Real relativeError = mHDiffNorm / mHFDNorm;
//		if (relativeError >= 1e-9) // Significative
//		{
//			writeToFile(mHA, "LagrangianHessianA.csv");
//			writeToFile(mHFD, "LagrangianHessianFD.csv");
//			writeToFile(mHDiff, "LagrangianHessianDiff.csv");
//
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//			logSimu("[ERROR] Hessian test. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", mHFDNorm, mHDiffNorm, relativeError);
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//		}
//	}
//}
//
//void SQPOptimizer::pullParameters()
//{
//	this->m_vp.resize(this->m_np);
//	this->m_vp.setZero(); // Init.
//	setSubvector(0, this->m_nx, this->m_pModel->getPositions_x(), this->m_vp);
//	//setSubvector(this->m_nx, this->m_nX, this->m_pModel->getPositions_0(), this->m_vp);
//}
//
//void SQPOptimizer::pushParameters()
//{
//	VectorXd vx;
//	VectorXd vX;
//	getSubvector(0, this->m_nx, this->m_vp, vx);
//	getSubvector(this->m_nx, this->m_nX, this->m_vp, vX);
//	this->m_pModel->updateState_x(vx, this->m_pModel->getVelocities_x());
//	//this->m_pModel->setParam_Rest(vX, this->m_pModel->getVelocities_0());
//}
//
//void SQPOptimizer::fixLagrangianVector(VectorXd& vL) const
//{
//	// Fix degrees-of-freedom (both rest and material)
//	for (int i = 0; i < (int) this->m_vfixIdx.size(); ++i)
//	{
//		int matIdx = this->m_vfixIdx[i];
//		int resIdx = matIdx + this->m_nx;
//
//		// Zero movement
//		vL(matIdx) = 0.0;
//		vL(resIdx) = 0.0;
//	}
//}
//
//void SQPOptimizer::fixConstraintVector(VectorXd& vc) const
//{
//	// Fix degrees-of-freedom (material coordinate only)
//	for (int i = 0; i < (int) this->m_vfixIdx.size(); ++i)
//	{
//		int matIdx = this->m_vfixIdx[i];
//		vc(matIdx) = 0.0; // Zero value
//	}
//}
//
//void SQPOptimizer::fixLagrangianHessian(tVector& vW) const
//{
//	bVector fixedStencil(this->m_nx, false);
//	for (int i = 0; i < (int)this->m_vfixIdx.size(); ++i)
//		fixedStencil[this->m_vfixIdx[i]] = true; // Fixed
//
//	int nEntries = (int)vW.size();
//	for (int i = 0; i < nEntries; ++i)
//	{
//		Triplet<Real>& t = vW[i];
//		int r = t.row();
//		int c = t.col();
//		int rfix = (r >= this->m_nx) ? r - this->m_nx : r;
//		int cfix = (c >= this->m_nx) ? c - this->m_nx : c;
//
//		if (fixedStencil[rfix] || fixedStencil[cfix])
//			vW[i] = Triplet<Real>(r, c, 0.0);
//	}
//
//	for (int i = 0; i < (int) this->m_vfixIdx.size(); ++i)
//	{
//		int dMat = this->m_vfixIdx[i];
//		int dRes = dMat + this->m_nx;
//		vW.push_back(Triplet<Real>(dMat, dMat, 1.0));
//		vW.push_back(Triplet<Real>(dRes, dRes, 1.0));
//	}
//}
//
//void SQPOptimizer::fixConstraintJacobian(tVector& vA) const
//{
//	bVector fixedStencil(this->m_nx, false);
//	for (int i = 0; i < (int)this->m_vfixIdx.size(); ++i)
//		fixedStencil[this->m_vfixIdx[i]] = true; // Fixed
//
//	// Zeros in non-diagonal entries
//
//	int nEntries = (int)vA.size();
//	for (int i = 0; i < nEntries; ++i)
//	{
//		Triplet<Real>& t = vA[i];
//		int r = t.row();
//		int c = t.col();
//		int cfix = (c >= this->m_nx) ? c - this->m_nx : c;
//
//		if (fixedStencil[r] || fixedStencil[cfix])
//			vA[i] = Triplet<Real>(r, c, 0.0);
//	}
//
//	for (int i = 0; i < (int) this->m_vfixIdx.size(); ++i)
//	{
//		int dMat = this->m_vfixIdx[i];
//		vA.push_back(Triplet<Real>(dMat, dMat, 1.0));
//	}
//}
//
//void SQPOptimizer::testThirdOrderTensor() const
//{
//	// TODO: Reserve space here
//	vector<tVector> vD2fxDxx_t(this->m_nx);
//	vector<tVector> vD2fxDxX_t(this->m_nX);
//	vector<tVector> vD2fxDXX_t(this->m_nX);
//	//vector<MatrixXd> vD2fxDxx_m(this->m_nx);
//	//vector<MatrixXd> vD2fxDxX_m(this->m_nX);
//	//vector<MatrixXd> vD2fxDXX_m(this->m_nX);
//	for (int i = 0; i < this->m_nx; ++i)
//	{
//		ostringstream name; // Output name
//		name << "D2fxDxx_" << i << ".csv";
//
//		SparseMatrixXd test(this->m_nx, this->m_nx);
//		test.setFromTriplets(vD2fxDxx_t[i].begin(), vD2fxDxx_t[i].end());
//		test.makeCompressed();
//
//		writeToFile(test, name.str().c_str());
//	}
//	for (int i = 0; i < this->m_nX; ++i)
//	{
//		ostringstream name; // Output name
//		name << "D2fxDx0_" << i << ".csv";
//
//		SparseMatrixXd test(this->m_nx, this->m_nx);
//		test.setFromTriplets(vD2fxDxX_t[i].begin(), vD2fxDxX_t[i].end());
//		test.makeCompressed();
//
//		writeToFile(test, name.str().c_str());
//	}
//	for (int i = 0; i < this->m_nX; ++i)
//	{
//		ostringstream name; // Output name
//		name << "D2fxD00_" << i << ".csv";
//
//		SparseMatrixXd test(this->m_nx, this->m_nX);
//		test.setFromTriplets(vD2fxDXX_t[i].begin(), vD2fxDXX_t[i].end());
//		test.makeCompressed();
//
//		writeToFile(test, name.str().c_str());
//	}
//}