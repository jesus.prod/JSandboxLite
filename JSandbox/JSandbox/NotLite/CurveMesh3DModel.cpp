/*=====================================================================================*/
/*!
\file		CurveMesh3DModel.cpp
\author		jesusprod
\brief		Implementation of CurveMesh3DModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/CurveMesh3DModel.h>

#include <JSandbox/Element/Curve3DEdgeElement.h>
#include <JSandbox/Element/Curve3DAngleElement.h>

#include <omp.h> // MP support

CurveMesh3DModel::CurveMesh3DModel() : NodalSolidModel("MG")
{
	this->m_pSimMesh_x = NULL;
	this->m_pSimMesh_0 = NULL;

	this->deprecateConfiguration();
}

CurveMesh3DModel::~CurveMesh3DModel()
{
	freeMesh();

	logSimu("[TRACE] Destroying CurveMesh3DModel: %d", this);
}

void CurveMesh3DModel::configureMesh(const RodMesh& mesh_0, const RodMesh& mesh_x)
{
	// Perform a deep clone of the given 
	// mesh. Maybe this is not the best
	// idea but it feels safer by now.

	this->freeMesh(); // Reinitialization
	this->m_pSimMesh_x = new RodMesh(mesh_x);
	this->m_pSimMesh_0 = new RodMesh(mesh_0);

	this->deprecateConfiguration();
}

void CurveMesh3DModel::configureMaterialStretch(const SolidMaterial& mat)
{
	assert(mat.getStretchK() != -1);

	this->m_matStretch = mat;

	this->deprecateConfiguration();
}

void CurveMesh3DModel::configureMaterialBending(const SolidMaterial& mat)
{
	assert(mat.getBendingK() != -1);

	this->m_matBending = mat;

	this->deprecateConfiguration();
}
void CurveMesh3DModel::setup()
{
	const vector<iVector>& vwhole2Split = this->m_pSimMesh_0->getWholeToSplitMap();
	const vector<iVector>& vsplit2Whole = this->m_pSimMesh_0->getSplitToWholeMap();

	int numvSplit = (int) vsplit2Whole.size();
	int numvWhole = (int) vwhole2Split.size();

	// DOF

	this->m_numDim_x = 3;
	this->m_numDim_0 = 2;

	this->m_numDimRaw = 3;

	this->m_numNodeSim = numvWhole;
	this->m_numNodeRaw = numvWhole;

	this->m_nsimDOF_x = 3 * numvWhole;
	this->m_nsimDOF_0 = 3 * numvWhole;
	this->m_nrawDOF = 3 * numvSplit;
	
	dVector vxSTLSplit3D;
	dVector vXSTLSplit3D;
	dVector vxSTLWhole3D(this->m_nsimDOF_x);
	dVector vXSTLWhole3D(this->m_nsimDOF_0);
	this->m_pSimMesh_x->getPoints(vxSTLSplit3D);
	this->m_pSimMesh_0->getPoints(vXSTLSplit3D);
	applyIndexMap(vsplit2Whole, vxSTLSplit3D, 3, vxSTLWhole3D);
	applyIndexMap(vsplit2Whole, vXSTLSplit3D, 3, vXSTLWhole3D);
	toEigen(vxSTLWhole3D, this->m_state_x->m_vx);
	toEigen(vXSTLWhole3D, this->m_state_0->m_vx);

	this->m_state_x->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_0->m_vv.resize(this->m_nsimDOF_x);
	this->m_state_x->m_vv.setZero();
	this->m_state_0->m_vv.setZero();

	// Initialize elements

	this->freeElements();

	// Initialize stretch elements
	
	this->initializeElements_Stretch();
	this->initializeElements_Bending();

	this->m_isReady_Setup = true;

	this->deprecateDiscretization();

	// Update simulation 
	this->update_SimDOF();

	logSimu("[TRACE] Initialized CurveMesh3DModel. Raw: %d, Sim: %d", this->m_nrawDOF, this->m_nsimDOF_x);
}

void CurveMesh3DModel::update_SimDOF()
{
	iVector vsimNodeIdx(this->m_numNodeRaw);
	iVector vrawNodeIdx(this->m_numNodeRaw);
	for (int i = 0; i < this->m_numNodeRaw; ++i)
		vrawNodeIdx[i] = i; // Default indexation

	transformIndex(this->m_pSimMesh_0->getSplitToWholeMap(), vrawNodeIdx, 0, vsimNodeIdx);

	this->m_vraw2sim_x.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_numNodeRaw; ++i)
	{
		this->m_vraw2sim_x[3 * i + 0] = 3 * vsimNodeIdx[i] + 0;
		this->m_vraw2sim_x[3 * i + 1] = 3 * vsimNodeIdx[i] + 1;
		this->m_vraw2sim_x[3 * i + 2] = 3 * vsimNodeIdx[i] + 2;
	}

	this->m_vraw2sim_0.resize(this->m_nrawDOF);
	for (int i = 0; i < this->m_numNodeRaw; ++i)
	{
		this->m_vraw2sim_0[3 * i + 0] = 3 * vsimNodeIdx[i] + 0;
		this->m_vraw2sim_0[3 * i + 1] = 3 * vsimNodeIdx[i] + 1;
		this->m_vraw2sim_0[3 * i + 2] = 3 * vsimNodeIdx[i] + 2;
	}
}

void CurveMesh3DModel::initializeElements_Stretch()
{
	int ne = this->m_pSimMesh_0->getNumEdge();

	for (int i = 0; i < ne; ++i)
	{
		int rodIdx, edgeIdx;
		this->m_pSimMesh_0->getRodEdgeForMeshEdge(i, rodIdx, edgeIdx);
		Curve& curve = this->m_pSimMesh_0->getRod(rodIdx).getCurve();

		int pidxRod, nidxRod;
		int pidxMesh, nidxMesh;
		curve.getEdgeNeighborNodes(edgeIdx, pidxRod, nidxRod);
		this->m_pSimMesh_0->getMeshNodeForRodNode(pidxRod, rodIdx, pidxMesh);
		this->m_pSimMesh_0->getMeshNodeForRodNode(nidxRod, rodIdx, nidxMesh);
		int pidxMeshWhole = this->m_pSimMesh_0->getSplitToWholeMap()[pidxMesh][0];
		int nidxMeshWhole = this->m_pSimMesh_0->getSplitToWholeMap()[nidxMesh][0];
		
		NodalSolidElement* pEle = new Curve3DEdgeElement(3);

		iVector vinds(2);
		vinds[0] = pidxMeshWhole;
		vinds[1] = nidxMeshWhole;
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);

		pEle->setMaterial(&this->m_matStretch);

		this->m_vpEles.push_back(pEle);
		this->m_vpStretchEles.push_back(pEle);
	}
}

void CurveMesh3DModel::initializeElements_Bending()
{
	int nv = this->m_pSimMesh_0->getNumNode();
	int nc = this->m_pSimMesh_0->getNumCon();

	const vector<iVector>& vsplit2Whole = this->m_pSimMesh_0->getSplitToWholeMap();

	for (int i = 0; i < nv; ++i)
	{
		int rodIdx, nodeIdx;
		this->m_pSimMesh_0->getRodNodeForMeshNode(i, rodIdx, nodeIdx);
		Curve& curve = this->m_pSimMesh_0->getRod(rodIdx).getCurve();

		int pidxRod, nidxRod;
		curve.getNodeNeighborNodes(nodeIdx, pidxRod, nidxRod);
		if (pidxRod == -1 || nidxRod == -1)
			continue; // Extreme, open curve

		int pidxMesh, nidxMesh;
		this->m_pSimMesh_0->getMeshNodeForRodNode(pidxRod, rodIdx, pidxMesh);
		this->m_pSimMesh_0->getMeshNodeForRodNode(nidxRod, rodIdx, nidxMesh);
		int pidxMeshWhole = vsplit2Whole[pidxMesh][0];
		int nidxMeshWhole = vsplit2Whole[nidxMesh][0];
		int idxMeshWhole = vsplit2Whole[i][0];

		NodalSolidElement* pEle = new Curve3DAngleElement(3);

		iVector vinds(3);
		vinds[0] = pidxMeshWhole;
		vinds[1] = idxMeshWhole;
		vinds[2] = nidxMeshWhole;
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);

		pEle->setMaterial(&this->m_matBending);

		this->m_vpEles.push_back(pEle);
		this->m_vpBendingEles.push_back(pEle);
	}
}

void CurveMesh3DModel::getParam_LengthEdge(VectorXd& vl)
{
	if (!this->isReady_Rest())
		this->update_Rest();

	int ne = (int) this->m_vpStretchEles.size();

	vl.resize(ne);
	vl.setZero();

	for (int i = 0; i < ne; ++i)
	{
		vl(i) = static_cast<Curve3DEdgeElement*>(this->m_vpStretchEles[i])->getRestLength();
	}
}

void CurveMesh3DModel::setParam_LengthEdge(const VectorXd& vl)
{
	if (!this->isReady_Rest())
		this->update_Rest();

	int ne = (int) this->m_vpStretchEles.size();

	assert(ne == (int)vl.size());

	for (int i = 0; i < ne; ++i)
	{
		static_cast<Curve3DEdgeElement*>(this->m_vpStretchEles[i])->setRestLength(vl(i));
	}

	this->deprecateUndeformed();
	this->m_isReady_Rest = true;
}


void CurveMesh3DModel::update_StateFromSimMesh()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	dVector vxSTLSplit3D;
	dVector vXSTLSplit3D;
	dVector vxSTLWhole3D(this->m_nsimDOF_x);
	dVector vXSTLWhole3D(this->m_nsimDOF_0);
	this->m_pSimMesh_x->getPoints(vxSTLSplit3D);
	this->m_pSimMesh_0->getPoints(vXSTLSplit3D);
	applyIndexMap(this->m_pSimMesh_x->getSplitToWholeMap(), vxSTLSplit3D, 3, vxSTLWhole3D);
	applyIndexMap(this->m_pSimMesh_0->getSplitToWholeMap(), vXSTLSplit3D, 3, vXSTLWhole3D);

	toEigen(vxSTLWhole3D, this->m_state_x->m_vx);
	toEigen(vXSTLWhole3D, this->m_state_0->m_vx);

	this->deprecateUndeformed();
	this->m_isReady_Mesh = true;
}

void CurveMesh3DModel::update_SimMeshFromState()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mesh)
		return; // Already

	dVector vxSTLWhole3D;
	dVector vXSTLWhole3D;
	dVector vxSTLSplit3D(this->m_nrawDOF);
	dVector vXSTLSplit3D(this->m_nrawDOF);

	toSTL(this->m_state_x->m_vx, vxSTLWhole3D);
	toSTL(this->m_state_0->m_vx, vXSTLWhole3D);
	applyIndexMap(this->m_pSimMesh_x->getWholeToSplitMap(), vxSTLWhole3D, 3, vxSTLSplit3D);
	applyIndexMap(this->m_pSimMesh_0->getWholeToSplitMap(), vXSTLWhole3D, 3, vXSTLSplit3D);

	this->m_pSimMesh_x->setPoints(vxSTLSplit3D);
	this->m_pSimMesh_0->setPoints(vXSTLSplit3D);

	for (int i = 0; i < this->m_pSimMesh_0->getNumRod(); ++i)
	{
		this->m_pSimMesh_0->getRod(i).readaptFrames();
		this->m_pSimMesh_x->getRod(i).readaptFrames();
	}

	this->m_isReady_Mesh = true;
}

void CurveMesh3DModel::freeElements()
{
	SolidModel::freeElements();

	this->m_vpStretchEles.clear();
	this->m_vpBendingEles.clear();
}

void CurveMesh3DModel::freeMesh()
{
	if (this->m_pSimMesh_x != NULL)
		delete this->m_pSimMesh_x;
	if (this->m_pSimMesh_0 != NULL)
		delete this->m_pSimMesh_0;
	this->m_pSimMesh_0 = NULL;
	this->m_pSimMesh_x = NULL;
}