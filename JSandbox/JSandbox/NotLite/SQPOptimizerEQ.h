///*=====================================================================================*/
///*!
//\file		SQPOptimizerEQ.h
//\author		jesusprod
//\brief		Basic class for SQP optimizer
//*/
///*=====================================================================================*/
//
//#ifndef SQP_OPTIMIZER_EQ_H
//#define SQP_OPTIMIZER_EQ_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//
//#include <JSandbox/Model/SolidModel.h>
//#include <JSandbox/Solver/PhysSolver.h>
//
//#include <JSandbox/Optim/ParameterSet.h>
//#include <JSandbox/Optim/ObjectiveError.h>
//
//#include <JSandbox/CustomTimer.h>
//
//class ParameterSet;
//class ObjectiveError;
//
//class JSANDBOX_EXPORT SQPOptimizerEQ
//{
//public:
//	SQPOptimizerEQ(const string& ID);
//
//	virtual ~SQPOptimizerEQ();
//
//	virtual void setModel(SolidModel* pModel);
//	virtual void setSolver(PhysSolver* pSolver);
//
//	virtual SolidModel* getModel() const;
//	virtual PhysSolver* getSolver() const;
//
//	virtual void setObjectiveErrors(const vector<ObjectiveError*>& vpobjs);
//	virtual const vector<ObjectiveError*>& getObjectiveErrors() const;
//
//	virtual void setParameterSets(const vector<ParameterSet*>& vpsets);
//	virtual const vector<ParameterSet*>& getParameterSets() const;
//
//	// DEPRECATED
//	//virtual void setTarget(const VectorXd& vt);
//	//virtual void getTarget(VectorXd& vt) const;
//
//	virtual const VectorXd& getGradient() const { return this->m_vg; }
//	virtual const VectorXd& getConstraint() const { return this->m_vC; }
//	virtual const VectorXd& getParameters() const { return this->m_vp; }
//
//	virtual void setup();
//
//	virtual bool init();
//	virtual bool step();
//
//	virtual Real getBetaRegularizerValue() const { return this->m_smoothInitial; }
//	virtual void setBetaRegularizerValue(Real val) { this->m_smoothInitial = val; }
//	virtual Real getBetaRegularizerFactor() const { return this->m_smoothFactor; }
//	virtual void setBetaRegularizerFactor(Real fac) { this->m_smoothFactor = fac; }
//
//protected:
//
//	virtual void getStep(VectorXd& vq);
//
//	virtual Real computeModelReduction(const VectorXd& vd) const;
//	virtual Real computeModelReduction_OBJ(const VectorXd& vd) const;
//	virtual Real computeModelReduction_CON(const VectorXd& vd) const;
//
//	virtual Real computeTangentialBound(const VectorXd& vd) const;
//	virtual Real computeOrthogonalBound(const VectorXd& vd) const;
//
//	virtual Real computeObjective() const;
//	virtual Real computeLagrangian() const;
//	virtual Real computeMeritError() const;
//
//	virtual void restartConstraints();
//	virtual void updateConstraints();
//
//	virtual void restartGradient();
//	virtual void restartHessian();
//	virtual void updateGradient();
//	virtual void updateHessian();
//
//	virtual void testGradient();
//	virtual void updateGradientA();
//	virtual void updateGradientFD();
//
//	virtual void testHessian();
//	virtual void updateHessianA();
//	virtual void updateHessianFD();
//	virtual void updateHessianBFGS();
//
//	virtual void pushParameters();
//	virtual void pullParameters();
//
//	virtual void fixLagrangianVector(VectorXd& vp) const;
//	virtual void fixConstraintVector(VectorXd& vc) const;
//
//	virtual void fixLagrangianHessian(tVector& vW) const;
//	virtual void fixConstraintJacobian(tVector& vA) const;
//
//	virtual void testThirdOrderTensor() const;
//
//	virtual void freeObjectives();
//	virtual void freeParameters();
//
//	virtual void testJacobian();
//	virtual void updateJacobianA();
//	virtual void updateJacobianFD();
//	
//
//protected:
//
//	int m_nx;
//	int m_nc;
//	int m_np;
//
//	int m_nobjSet;
//	int m_nparamSet;
//	iVector m_vparamOff;
//	vector<ObjectiveError*> m_vobjSet;
//	vector<ParameterSet*> m_vparamSet;
//
//	Real m_mu;						// Current mu value
//
//	VectorXd m_vp;					// Current solution
//	VectorXd m_vl;					// Current lambdas
//	VectorXd m_vC;					// Current Constraints values
//
//	VectorXd m_vg;					// Current objective gradient DfDp
//	VectorXd m_vG;					// Current Lagrangian gradient DLDp
//
//	tVector m_vAs;	// Constraint Jacobian entries
//	tVector m_vWs;	// Lagrangian Hessian entries
//	SparseMatrixXd m_mWs;			// Sparse Lagrangian Hessian
//	SparseMatrixXd m_mAs;			// Sparse Constraint Jacobian
//
//	MatrixXd m_mW;
//
//	// Smoothness parameters
//
//	Real m_smooth;
//	Real m_smoothInitial;
//	Real m_smoothFactor;
//
//	// Reduction parameters
//
//	Real m_alpha;
//	Real m_theta;
//	Real m_sigma;
//	Real m_gamma;
//	Real m_tau;
//	Real m_psi;
//
//	// BFGS variables
//
//	VectorXd m_vg_P;				// Previous gradient
//	VectorXd m_vp_P;				// Previous solution
//	VectorXd m_sk;
//	VectorXd m_yk;
//	MatrixXd m_modifier1;
//	MatrixXd m_modifier2;
//	MatrixXd m_tempMatrix;
//	Real m_rho;
//
//	Real m_maxGradi;
//	Real m_maxConst;
//
//	SolidModel* m_pModel;
//	PhysSolver* m_pSolver;
//
//	string m_ID;
//
//	VectorXd m_vxT;
//
//};
//
//#endif