/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_EdgeRadius.h
\author		jesusprod
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_TENSILE_HERMITE_EDGE_RADIUS_H
#define PARAMETER_SET_TENSILE_HERMITE_EDGE_RADIUS_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/Model/TensileStructureModel.h>

class JSANDBOX_EXPORT ParameterSet_Tensile_HermiteEdgeRadius : public ParameterSet
{
public:
	enum RadiusType
	{
		Isotropic,
		Constrained,
		Anisotropic
	};

public:
	ParameterSet_Tensile_HermiteEdgeRadius(int midx);
	virtual ~ParameterSet_Tensile_HermiteEdgeRadius();

	virtual void setDummyRods(const bVector& vd) { this->m_vdummy = vd; }
	virtual const bVector& getDummyRods() const { return this->m_vdummy; }

	virtual iVector getPointNumber() const { return this->m_vcontrolNum; }
	virtual void setPointNumber(const iVector& vc) { this->m_vcontrolNum = vc; }

	virtual RadiusType getRadiusType() const { return this->m_type; }
	virtual void setRadiusType(RadiusType type) { this->m_type = type; }

	virtual Real getMinimumRadii() const { return this->m_minRadius; }
	virtual Real getMaximumRadii() const { return this->m_maxRadius; }
	virtual void setMinimumRadii(Real rm) { this->m_minRadius = rm; }
	virtual void setMaximumRadii(Real rM) { this->m_maxRadius = rM; }

	virtual Real getMinimumRatio() const { return this->m_minRatio; }
	virtual Real getMaximumRatio() const { return this->m_maxRatio; }
	virtual void setMinimumRatio(Real rm) { this->m_minRatio = rm; }
	virtual void setMaximumRatio(Real rM) { this->m_maxRatio = rM; }

	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

	virtual void updateControlPoints();

	virtual void testHermiteJacobian() const;

	virtual void updateBoundsVector();

	virtual bool getConstrainConnections() const { return this->m_constrainConnections; }
	virtual void setConstrainConnections(bool cc) { this->m_constrainConnections = cc; }

protected:

	virtual void concatenatedToRods(const VectorXd& vpAll, vector<VectorXd>& vpRod) const;
	virtual void rodsToConcatenated(const vector<VectorXd>& vpRod, VectorXd& vpAll) const;

	virtual void computeJac(SparseMatrixXd& mDpDm) const;

	virtual void computeJacRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, tVector& vDpDm, bool closed) const;
	virtual void computeValRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, VectorXd& vpInt, bool closed) const;

	TensileStructureModel* m_pTSModel;

	int m_midx;

	iVector m_vmapSplit2Whole;

	iVector m_vcontrolNum;
	vector<VectorXd> m_vcontrolVal;
	vector<VectorXd> m_vcontrolPar;

	Real m_minRatio;
	Real m_maxRatio;
	Real m_minRadius;
	Real m_maxRadius;

	RadiusType m_type;

	bVector m_vdummy;

	bool m_constrainConnections;

};

#endif