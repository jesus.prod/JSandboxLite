/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_SurfaceCoupling.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_SurfaceCoupling.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_SurfaceCoupling.h>

ParameterSet_Tensile_SurfaceCoupling::ParameterSet_Tensile_SurfaceCoupling() : ParameterSet("TENSURFCOUPLING")
{
	// Nothing to do here
}

ParameterSet_Tensile_SurfaceCoupling::~ParameterSet_Tensile_SurfaceCoupling()
{
	// Nothing to do here
}

void ParameterSet_Tensile_SurfaceCoupling::getParameters(VectorXd& vp) const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	vp.resize(this->m_np);

	int count = 0;
	for (int i = 0; i < numSurfaces; ++i)
	{
		SurfaceModel* pSurfModel = this->m_pTSModel->getSurfaceModel(i);

		VectorXd vc0_i;
		pSurfModel->getParam_CouplingRest(vc0_i);
		for (int j = 0; j < (int)vc0_i.size(); ++j)
			vp(count++) = vc0_i(j); // Set subset
	}
}

void ParameterSet_Tensile_SurfaceCoupling::setParameters(const VectorXd& vp)
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	assert((int)vp.size() == this->m_np);

	int count = 0;
	for (int i = 0; i < numSurfaces; ++i)
	{
		SurfaceModel* pSurfModel = this->m_pTSModel->getSurfaceModel(i);
		int numC_i = (int) pSurfModel->getCouplingNodes().size();
		int numD_i = pSurfModel->getNumSimDim_0();

		VectorXd vc0_i(numC_i*numD_i);
		for (int j = 0; j < numC_i; ++j)
			for (int d = 0; d < numD_i; ++d)
				vc0_i(j*numD_i + d) = vp(count++);

		pSurfModel->setParam_CouplingRest(vc0_i);
	}
}

bool ParameterSet_Tensile_SurfaceCoupling::isReady_fp() const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	bool ready = true;
	for (int i = 0; i < numSurfaces; ++i)
		ready &= this->m_pTSModel->getSurfaceModel(i)->isReady_fc0();

	return ready;
}

bool ParameterSet_Tensile_SurfaceCoupling::isReady_DfDp() const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	bool ready = true;
	for (int i = 0; i < numSurfaces; ++i)
		ready &= this->m_pTSModel->getSurfaceModel(i)->isReady_DfxDc0();

	return ready;
}

void ParameterSet_Tensile_SurfaceCoupling::get_fp(VectorXd& vfp) const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	vfp.resize(this->m_np);

	int count = 0;
	for (int i = 0; i < numSurfaces; ++i)
	{
		SurfaceModel* pSurfModel = this->m_pTSModel->getSurfaceModel(i);

		VectorXd vfp_i;
		pSurfModel->add_fc0(vfp_i);
		for (int j = 0; j < (int)vfp_i.size(); ++j)
			vfp(count++) = vfp_i(j); // Set subset
	}
}

void ParameterSet_Tensile_SurfaceCoupling::get_DfDp(tVector& vDFDp) const
{
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();

	int coefficientSum = 0;
	for (int i = 0; i < numSurfaces; ++i)
		coefficientSum += this->m_pTSModel->getSurfaceModel(i)->getNumNonZeros_DfxDc0();

	vDFDp.reserve(coefficientSum);

	int couplingCount = 0;
	for (int i = 0; i < numSurfaces; ++i)
	{
		SurfaceModel* pSurfModel = this->m_pTSModel->getSurfaceModel(i);
		int numC_i = (int)pSurfModel->getCouplingNodes().size();
		int numD_i = pSurfModel->getNumSimDim_0();

		// Local Jacob.
		tVector vDFDpi;

		vDFDpi.reserve(pSurfModel->getNumNonZeros_DfxDc0());
		pSurfModel->add_DfxDc0(vDFDpi); // Forces Jacobian

		int ncoeff = (int)vDFDpi.size();
		for (int j = 0; j < ncoeff; ++j) // Concatenate this coefficients with the rest of the surfaces
			vDFDpi[j] = Triplet<Real>(vDFDpi[j].row(), vDFDpi[j].col() + couplingCount, vDFDpi[j].value());

		couplingCount += numC_i*numD_i;

		this->m_pTSModel->addLocal2Global_Row(i, vDFDpi, vDFDp);
	}
}

void ParameterSet_Tensile_SurfaceCoupling::setup()
{
	assert(this->m_pModel != NULL);

	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	int numSurfaces = this->m_pTSModel->getNumSurfaceModels();
	this->m_np = 0;
	
	for (int i = 0; i < numSurfaces; ++i) 
	{
		VectorXd vc0_i;
		this->m_pTSModel->getSurfaceModel(i)->getParam_CouplingRest(vc0_i);
		this->m_np += (int) vc0_i.size(); // Add to surface coupling DOF
	}

	this->updateBoundsVector();
}