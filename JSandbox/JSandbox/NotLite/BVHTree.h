/*=====================================================================================*/
/*!
\file		BVHNode.h
\author		jesusprod,bthomasz
\brief		Tree of a Bounding Volume Hierarchy for spatial partitioning of mesh faces.
			This class is adapted from bthomasz implementation for Phys3D Collision
			Detection Plugins.
*/
/*=====================================================================================*/

#ifndef BVH_TREE_HH
#define BVH_TREE_HH

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/Curve.h>
#include <JSandbox/Geometry/TriMesh.h>

#include <JSandbox/Geometry/BoundingVolume.h>
#include <JSandbox/Geometry/BVHNode.h>

typedef struct BVHClosePrimitivePair
{
	BVHClosePrimitivePair()
	{
		this->m_ID0 = -1;
		this->m_ID1 = -1;
	}

	BVHClosePrimitivePair(int ID0, int ID1, BVHPrimitive prim0, BVHPrimitive prim1)
	{
		this->m_ID0 = -1;
		this->m_ID1 = -1;
		this->m_prim0 = prim0;
		this->m_prim1 = prim1;
	}

	int m_ID0;
	int m_ID1;
	BVHPrimitive m_prim0;
	BVHPrimitive m_prim1;

} BVHClosePrimitivePair;

class TriMesh;

class JSANDBOX_EXPORT BVHTree
{

public:
	BVHTree(int ID) { m_ID = ID;  }

	BVHTree(const BVHTree& toCopy);

	virtual ~BVHTree(void);

	virtual int getID() const { return m_ID; }

	void initializeCurve(const Curve& curve);
	void initializeCloud(const vector<Vector3d>& vcloud);
	void initializeMeshFace(const TriMesh* pMesh, const string& pname = "");
	void initializeMeshEdge(const TriMesh* pMesh, const string& pname = "");

	/*! Updates the hierarchy for a new set of positions. If clear is set to false, old values in bounding volumes are
	not deleted so that recursive tests consider potential collisions at any "instance" of the primitives. This is useful 
	for continuous collision detection of moving objects.
	*/
	virtual void update(const dVector& vpos, bool clear);

	/*! Updates the hierarchy for some changes in the set of primitives.
	*/
	virtual void update(const vector<BVHPrimitive>& vrem,
						const vector<BVHPrimitive>& vadd);

	virtual const BVHNode& getNode(int idx) const { assert(idx < (int)m_vnodes.size()); return m_vnodes[idx]; }
	virtual const BoundingVolume* getBV(int idx) const { assert(idx < (int)m_vBVs.size()); return m_vBVs[idx]; }
	virtual const BVHPrimitive& getPrimitive(int idx) const { assert(idx < (int)m_vprims.size()); return m_vprims[idx]; }

	// Generic overlap tests for primitives using the bounding volumes

	static bool testNodes(const BVHTree* bvh1, const BVHTree* bvh2, int i1, int i2, vector<BVHClosePrimitivePair>& cpps, double tol = 1.0e-9);
	static bool testRecursive(const BVHTree* bvh1, const BVHTree* bvh2, int i1, int i2, vector<BVHClosePrimitivePair>& cpps, double tol = 1.0e-9);

protected:

	virtual int splitNode(int idx, const dVector& vpos);
	virtual void splitRecursive(int idx, const dVector& vpos);
	virtual void updateVolumesRecursive(int idx, const dVector& vpos, bool clear);
	virtual void insert(BoundingVolume* bv, const BVHPrimitive& prim, const dVector& vpos);

	virtual void freeMemory();

protected:
	int m_ID;
	vector<BVHNode> m_vnodes;
	vector<BVHPrimitive> m_vprims;
	vector<BoundingVolume*> m_vBVs;
	dVector m_vx;

};

#endif