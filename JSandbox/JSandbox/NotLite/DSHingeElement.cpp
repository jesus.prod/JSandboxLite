/*=====================================================================================*/
/*!
\file		DSHingeElement.cpp
\author		jesusprod
\brief		Implementation of DSHingeElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/DSHingeElement.h>

DSHingeElement::DSHingeElement(int dim0) : NodalSolidElement(4, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);

	this->m_vfs.resize(1);
	this->m_mDfsDs.resize(1, 1);
	this->m_mDfxDs.resize(m_numDOFx, 1);
}

DSHingeElement::~DSHingeElement()
{
	// Nothing to do here
}

void DSHingeElement::update_Rest(const VectorXd& vX)
{
//	if (this->m_numDim_0 == 2)
//	{
//		Vector3d x0(vX(this->m_vidx0[0]), vX(this->m_vidx0[1]), 0.0);
//		Vector3d x1(vX(this->m_vidx0[2]), vX(this->m_vidx0[3]), 0.0);
//		Vector3d x2(vX(this->m_vidx0[4]), vX(this->m_vidx0[5]), 0.0);
//		Vector3d x3(vX(this->m_vidx0[6]), vX(this->m_vidx0[7]), 0.0);
//		x0 = x0*this->m_preStrain;
//		x1 = x1*this->m_preStrain;
//		x2 = x2*this->m_preStrain;
//		x3 = x3*this->m_preStrain;
//
//		Vector3d n0 = (x0 - x1).cross(x3 - x1);
//		Vector3d n1 = (x2 - x1).cross(x0 - x1);
//		double n0len = n0.norm();
//		double n1len = n1.norm();
//
//		double A0 = 0.5*n0len;
//		double A1 = 0.5*n1len;
//		double L = (x0 - x1).norm();
//		double Amean = 0.5*(A0 + A1);
//		this->m_gFac0 = L*(Amean/3.0);
//
//		this->m_theta0 = 0;
//	}
//	else if (this->m_numDim_0 == 3)
//	{
//		vector<double> x0(3, 0.0);
//		vector<double> x1(3, 0.0);
//		vector<double> x2(3, 0.0);
//		vector<double> x3(3, 0.0);
//		for (int i = 0; i < this->m_numDim_0; ++i)
//		{
//			x0[i] = vX(this->m_vidx0[i])*this->m_preStrain;
//			x1[i] = vX(this->m_vidx0[1 * this->m_numDim_0 + i])*this->m_preStrain;
//			x2[i] = vX(this->m_vidx0[2 * this->m_numDim_0 + i])*this->m_preStrain;
//			x3[i] = vX(this->m_vidx0[3 * this->m_numDim_0 + i])*this->m_preStrain;
//		}
//
//		{
//#include "../../Maple/Code/DSHingeElementTheta.mcg"
//			this->m_theta0 = t333;
//		}
//
//		{
//#include "../../Maple/Code/DSHingeElementGFac.mcg"
//			this->m_gFac0 = t249;
//		}
//	}
//	else assert(false); // Should not be here!

	Real pS = this->m_preStrain;

	vector<double> X0Raw(3, 0.0);
	vector<double> X1Raw(3, 0.0);
	vector<double> X2Raw(3, 0.0);
	vector<double> X3Raw(3, 0.0);
	for (int i = 0; i < this->m_numDim_0; ++i)
	{
		X0Raw[i] = vX(this->m_vidx0[0 * this->m_numDim_0 + i]);
		X1Raw[i] = vX(this->m_vidx0[1 * this->m_numDim_0 + i]);
		X2Raw[i] = vX(this->m_vidx0[2 * this->m_numDim_0 + i]);
		X3Raw[i] = vX(this->m_vidx0[3 * this->m_numDim_0 + i]);
	}

	{
#include "../../Maple/Code/DSHingeRest_Theta.mcg"
		this->m_theta0 = t395;
	}

	{
#include "../../Maple/Code/DSHingeRest_GFac.mcg"
		this->m_gFac0 = t302;
	}
}

void DSHingeElement::update_Mass(const VectorXd& vX)
{
	// Does not carry mass
	this->m_vmx.setZero();
}

void DSHingeElement::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	if (isApprox(m_vpmat[0]->getBendingK(), 0.0, EPS_APPROX) ||
		isApprox(m_vpmat[0]->getBendingK(), -1.0, EPS_APPROX))
	{
		this->m_energy = 0;
		return;
	}

	Vector3d x0(vx(this->m_vidxx[0]), vx(this->m_vidxx[1]), vx(this->m_vidxx[2]));
	Vector3d x1(vx(this->m_vidxx[3]), vx(this->m_vidxx[4]), vx(this->m_vidxx[5]));
	Vector3d x2(vx(this->m_vidxx[6]), vx(this->m_vidxx[7]), vx(this->m_vidxx[8]));
	Vector3d x3(vx(this->m_vidxx[9]), vx(this->m_vidxx[10]), vx(this->m_vidxx[11]));

	Vector3d n0 = (x0 - x1).cross(x3 - x1);
	Vector3d n1 = (x2 - x1).cross(x0 - x1);
	double n0len = n0.norm();
	double n1len = n1.norm();

	double gFac0 = m_gFac0; // *this->m_preStrain;

	double cosAngle = (n0.dot(n1)) / (n0len*n1len);
	if (abs(cosAngle) > 1.0) // Prevent NaN from acos 
		cosAngle = (cosAngle >= 0.0) ? 1.0 : -1.0;
	double theta = acos(cosAngle);

	this->m_energy = 0.5*m_vpmat[0]->getBendingK()*(theta - m_theta0)*(theta - m_theta0)*gFac0;
}

void DSHingeElement::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	if (isApprox(m_vpmat[0]->getBendingK(), 0.0, EPS_APPROX) ||
		isApprox(m_vpmat[0]->getBendingK(), -1.0, EPS_APPROX))
	{
		this->m_vfVal.setZero();
		return;
	}

	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidxx[0 + i]);
		x1[i] = vx(this->m_vidxx[3 + i]);
		x2[i] = vx(this->m_vidxx[6 + i]);
		x3[i] = vx(this->m_vidxx[9 + i]);
	}

	double gFac0 = m_gFac0;// *this->m_preStrain;
	double kB = m_vpmat[0]->getBendingK();
	double theta0 = this->m_theta0;
	dVector vfx(12);

	if (kB == 0)
	{
		for (int i = 0; i < 12; ++i)
			this->m_vfVal(i) = 0.0;
		return;
	}

	{
#include "../../Maple/Code/DSHingeElementForce.mcg"
	}

	for (int i = 0; i < 12; ++i)
		if (!isfinite(vfx[i]))
			logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 12; ++i)
		this->m_vfVal(i) = vfx[i];
}

void DSHingeElement::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	if (isApprox(m_vpmat[0]->getBendingK(), 0.0, EPS_APPROX) ||
		isApprox(m_vpmat[0]->getBendingK(), -1.0, EPS_APPROX))
	{
		this->m_vJVal.setZero();
		return;
	}

	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidxx[0 + i]);
		x1[i] = vx(this->m_vidxx[3 + i]);
		x2[i] = vx(this->m_vidxx[6 + i]);
		x3[i] = vx(this->m_vidxx[9 + i]);
	}

	double gFac0 = m_gFac0;// *this->m_preStrain;
	double kB = m_vpmat[0]->getBendingK();
	double theta0 = this->m_theta0;

	double mJx[12][12];

	if (kB == 0)
	{
		int count = 0;
		for (int i = 0; i < 12; ++i)
			for (int j = 0; j < i; ++j)
				this->m_vJVal(count++) = 0.0;
		return;
	}

	{
#include "../../Maple/Code/DSHingeElementJacobian.mcg"
	}

	for (int i = 0; i < 12; ++i)
		for (int j = 0; j < 12; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 12; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}

void DSHingeElement::update_DfxDx(const VectorXd& vx, const VectorXd& vX) 
{ 
	if (isApprox(m_vpmat[0]->getBendingK(), 0.0, EPS_APPROX) ||
		isApprox(m_vpmat[0]->getBendingK(), -1.0, EPS_APPROX))
	{
		this->m_mDfxDx.setZero();
		return;
	}

	Real pS = this->m_preStrain;

	// Get material points

	vector<double> x0(3, 0.0);
	vector<double> x1(3, 0.0);
	vector<double> x2(3, 0.0);
	vector<double> x3(3, 0.0);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidxx[0 + i]);
		x1[i] = vx(this->m_vidxx[3 + i]);
		x2[i] = vx(this->m_vidxx[6 + i]);
		x3[i] = vx(this->m_vidxx[9 + i]);
	}

	// Get rest points

	vector<double> X0Raw(3, 0.0);
	vector<double> X1Raw(3, 0.0);
	vector<double> X2Raw(3, 0.0);
	vector<double> X3Raw(3, 0.0);
	for (int i = 0; i < this->m_numDim_0; ++i)
	{
		X0Raw[i] = vX(this->m_vidx0[0 * this->m_numDim_0 + i]);
		X1Raw[i] = vX(this->m_vidx0[1 * this->m_numDim_0 + i]);
		X2Raw[i] = vX(this->m_vidx0[2 * this->m_numDim_0 + i]);
		X3Raw[i] = vX(this->m_vidx0[3 * this->m_numDim_0 + i]);
	}

	//// Get rest points

	//vector<double> X0(3);
	//vector<double> X1(3);
	//vector<double> X2(3);
	//vector<double> X3(3);

	//if (this->m_numDim_0 == 2)
	//{
	//	for (int i = 0; i < 2; ++i)
	//	{
	//		X0[i] = vX(this->m_vidx0[0 + i]);
	//		X1[i] = vX(this->m_vidx0[2 + i]);
	//		X2[i] = vX(this->m_vidx0[4 + i]);
	//		X3[i] = vX(this->m_vidx0[6 + i]);
	//	}

	//	// Keep at plane
	//	X0[2] = 0.0;
	//	X1[2] = 0.0;
	//	X2[2] = 0.0;
	//	X3[2] = 0.0;
	//}
	//else if (this->m_numDim_0 == 3)
	//{
	//	for (int i = 0; i < 3; ++i)
	//	{
	//		X0[i] = vX(this->m_vidx0[0 + i]);
	//		X1[i] = vX(this->m_vidx0[3 + i]);
	//		X2[i] = vX(this->m_vidx0[6 + i]);
	//		X3[i] = vX(this->m_vidx0[9 + i]);
	//	}
	//}
	//else assert(false); // WTF?

	double kB = m_vpmat[0]->getBendingK();

	double mJx[12][12];

#include "../../Maple/Code/DSHingeRest_DfxDx.mcg"

	for (int i = 0; i < 12; ++i)
		for (int j = 0; j < 12; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	for (int i = 0; i < this->m_numDOFx; ++i)
		for (int j = 0; j < this->m_numDOFx; ++j)
			this->m_mDfxDx(i, j) = mJx[i][j];
}

void DSHingeElement::update_DfxD0(const VectorXd& vx, const VectorXd& vX) 
{
	if (isApprox(m_vpmat[0]->getBendingK(), 0.0, EPS_APPROX) ||
		isApprox(m_vpmat[0]->getBendingK(), -1.0, EPS_APPROX))
	{
		this->m_mDfxD0.setZero();
		return;
	}

	// Get material points

	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidxx[0 + i]);
		x1[i] = vx(this->m_vidxx[3 + i]);
		x2[i] = vx(this->m_vidxx[6 + i]);
		x3[i] = vx(this->m_vidxx[9 + i]);
	}

	// Get rest points

	Real pS = this->m_preStrain;

	vector<double> X0Raw(3, 0.0);
	vector<double> X1Raw(3, 0.0);
	vector<double> X2Raw(3, 0.0);
	vector<double> X3Raw(3, 0.0);
	for (int i = 0; i < this->m_numDim_0; ++i)
	{
		X0Raw[i] = vX(this->m_vidx0[0 * this->m_numDim_0 + i]);
		X1Raw[i] = vX(this->m_vidx0[1 * this->m_numDim_0 + i]);
		X2Raw[i] = vX(this->m_vidx0[2 * this->m_numDim_0 + i]);
		X3Raw[i] = vX(this->m_vidx0[3 * this->m_numDim_0 + i]);
	}

	double kB = m_vpmat[0]->getBendingK();

	double mJX[12][12];

#include "../../Maple/Code/DSHingeRest_DfxD0.mcg"

	for (int i = 0; i < 12; ++i)
		for (int j = 0; j < 12; ++j)
			if (!isfinite(mJX[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		for (int j = 0; j < this->m_numNode; ++j)
		{
			for (int k = 0; k < this->m_numDim_0; ++k)
			{
				this->m_mDfxD0(i, this->m_numDim_0*j + k) = mJX[i][this->m_numDim_x*j + k];
			}
		}
	}
}

void DSHingeElement::update_fs(const VectorXd& vx, const VectorXd& vX)
{
	if (isApprox(m_vpmat[0]->getBendingK(), 0.0, EPS_APPROX) ||
		isApprox(m_vpmat[0]->getBendingK(), -1.0, EPS_APPROX))
	{
		this->m_vfs.setZero();
		return;
	}

	// Get material points

	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidxx[0 + i]);
		x1[i] = vx(this->m_vidxx[3 + i]);
		x2[i] = vx(this->m_vidxx[6 + i]);
		x3[i] = vx(this->m_vidxx[9 + i]);
	}

	// Get rest points

	Real pS = this->m_preStrain;

	vector<double> X0Raw(3, 0.0);
	vector<double> X1Raw(3, 0.0);
	vector<double> X2Raw(3, 0.0);
	vector<double> X3Raw(3, 0.0);
	for (int i = 0; i < this->m_numDim_0; ++i)
	{
		X0Raw[i] = vX(this->m_vidx0[0 * this->m_numDim_0 + i]);
		X1Raw[i] = vX(this->m_vidx0[1 * this->m_numDim_0 + i]);
		X2Raw[i] = vX(this->m_vidx0[2 * this->m_numDim_0 + i]);
		X3Raw[i] = vX(this->m_vidx0[3 * this->m_numDim_0 + i]);
	}

	double kB = m_vpmat[0]->getBendingK();

	double vfs[1];

	throw new exception("Not implemented yet...");

//#include "../../Maple/Code/DSHingeRest_fs.mcg"

	for (int i = 0; i < 1; ++i)
		if (!isfinite(vfs[i]))
			logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);

	this->m_vfs(0) = vfs[0];
}

void DSHingeElement::update_DfsDs(const VectorXd& vx, const VectorXd& vX)
{
	throw new exception("Not implemented yet");

//	if (isApprox(m_vpmat[0]->getBendingK(), 0.0, EPS_APPROX) ||
//		isApprox(m_vpmat[0]->getBendingK(), -1.0, EPS_APPROX))
//	{
//		this->m_mDfxDs.setZero();
//		return;
//	}
//
//	// Get material points
//
//	vector<double> x0(3);
//	vector<double> x1(3);
//	vector<double> x2(3);
//	vector<double> x3(3);
//	for (int i = 0; i < 3; ++i)
//	{
//		x0[i] = vx(this->m_vidxx[0 + i]);
//		x1[i] = vx(this->m_vidxx[3 + i]);
//		x2[i] = vx(this->m_vidxx[6 + i]);
//		x3[i] = vx(this->m_vidxx[9 + i]);
//	}
//
//	// Get rest points
//
//	Real pS = this->m_preStrain;
//
//	vector<double> X0Raw(3, 0.0);
//	vector<double> X1Raw(3, 0.0);
//	vector<double> X2Raw(3, 0.0);
//	vector<double> X3Raw(3, 0.0);
//	for (int i = 0; i < this->m_numDim_0; ++i)
//	{
//		X0Raw[i] = vX(this->m_vidx0[0 * this->m_numDim_0 + i]);
//		X1Raw[i] = vX(this->m_vidx0[1 * this->m_numDim_0 + i]);
//		X2Raw[i] = vX(this->m_vidx0[2 * this->m_numDim_0 + i]);
//		X3Raw[i] = vX(this->m_vidx0[3 * this->m_numDim_0 + i]);
//	}
//
//	double kB = m_vpmat[0]->getBendingK();
//
//	double mJs[1][1];
//
//	throw new exception("Not implemented yet...");
//
////#include "../../Maple/Code/DSHingeRest_DfsDs.mcg"
//
//	for (int i = 0; i < 1; ++i)
//		for (int j = 0; j < 1; ++j)
//			if (!isfinite(mJs[i][j]))
//				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//
//	this->m_mDfxDs(0, 0) = mJs[0][0];
}

void DSHingeElement::update_DfxDs(const VectorXd& vx, const VectorXd& vX)
{
	if (isApprox(m_vpmat[0]->getBendingK(), 0.0, EPS_APPROX) ||
		isApprox(m_vpmat[0]->getBendingK(), -1.0, EPS_APPROX))
	{
		this->m_mDfxDs.setZero();
		return;
	}

	// Get material points

	vector<double> x0(3);
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x0[i] = vx(this->m_vidxx[0 + i]);
		x1[i] = vx(this->m_vidxx[3 + i]);
		x2[i] = vx(this->m_vidxx[6 + i]);
		x3[i] = vx(this->m_vidxx[9 + i]);
	}

	// Get rest points

	Real pS = this->m_preStrain;

	vector<double> X0Raw(3, 0.0);
	vector<double> X1Raw(3, 0.0);
	vector<double> X2Raw(3, 0.0);
	vector<double> X3Raw(3, 0.0);
	for (int i = 0; i < this->m_numDim_0; ++i)
	{
		X0Raw[i] = vX(this->m_vidx0[0 * this->m_numDim_0 + i]);
		X1Raw[i] = vX(this->m_vidx0[1 * this->m_numDim_0 + i]);
		X2Raw[i] = vX(this->m_vidx0[2 * this->m_numDim_0 + i]);
		X3Raw[i] = vX(this->m_vidx0[3 * this->m_numDim_0 + i]);
	}

	double kB = m_vpmat[0]->getBendingK();

	double mJs[12][1];

#include "../../Maple/Code/DSHingeRest_DfxDs.mcg"

	for (int i = 0; i < 12; ++i)
		for (int j = 0; j < 1; ++j)
			if (!isfinite(mJs[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	for (int i = 0; i < this->m_numDOFx; ++i)
	{
		this->m_mDfxDs(i, 0) = mJs[i][0];
	}
}

void DSHingeElement::add_fs(VectorXd& vfs)
{
	vfs(0) += this->m_vfs(0);
}

void DSHingeElement::add_DfsDs(tVector& vDfsDs)
{
	vDfsDs.push_back(Triplet<Real>(0, 0, this->m_mDfsDs(0, 0)));
}

void DSHingeElement::add_DfxDs(tVector& vDfxDs)
{
	for (int i = 0; i < this->m_numDOFx; ++i) // Add triplets in the allocated index
		vDfxDs.push_back(Triplet<Real>(this->m_vidxx[i], 0, this->m_mDfxDs(i, 0)));
}