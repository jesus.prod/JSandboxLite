/*=====================================================================================*/
/*!
\file		BVHNode.h
\author		jesusprod,bthomasz
\brief		Node of a Bounding Volume Hierarchy for spatial partitioning of mesh faces.
			This class is adapted from bthomasz implementation for Phys3D Collision
			Detection Plugins.
*/
/*=====================================================================================*/

#ifndef BVH_NODE_HH
#define BVH_NODE_HH

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

typedef struct BVHPrimitive
{
	BVHPrimitive()
	{
		this->m_npoints = 0;
	}

	BVHPrimitive(int idx, int np, const iVector& vpidx)
	{
		assert(np == (int)vpidx.size());
		
		this->m_index = idx;
		this->m_npoints = np;
		this->m_vpidx = vpidx;
	}

	int m_index;
	int m_npoints;
	iVector m_vpidx;

} BVHPrimitive;

class JSANDBOX_EXPORT BVHNode
{
public:
	BVHNode(int parentIdx) { m_parentIdx = parentIdx; }
	virtual ~BVHNode(void) {}

	virtual const iVector& getPrimitives() const { return m_vprimsIdx; }
	virtual void addPrimitive(int primIdx) { m_vprimsIdx.push_back(primIdx); }
	virtual void setPrimitives(const iVector& vprims) { m_vprimsIdx = vprims; }
	virtual void addChild(int childIdx) { m_vchildIdx.push_back(childIdx); }
	virtual int getNumChildren() const { return (int)m_vprimsIdx.size(); }

	virtual const iVector& getChildren() const { return m_vchildIdx; }
	virtual bool isLeaf() const { return m_vprimsIdx.size() == 1; }
	virtual bool isEmpty() const { return m_vprimsIdx.size() == 0; }

protected:
	iVector m_vprimsIdx;
	iVector m_vchildIdx;
	int m_parentIdx;

};

#endif