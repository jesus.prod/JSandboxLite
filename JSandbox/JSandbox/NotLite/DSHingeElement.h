/*=====================================================================================*/
/*!
\file		DSHingeElement.h
\author		jesusprod,bthomasz
\brief		Basic implementation of a discrete-shell bending element. Adapted bthomasz
			implementation for Phys3D DeformablesSurface plugin. This provides a simple
			quadratic energy density for angle preserving between pairs of triangles. It
			relies on Maple auto-differentiation tool for force and Jacobian expressions.
			It uses SolidMaterial bending K as elasticity coefficient.
*/
/*=====================================================================================*/

#ifndef DS_HINGE_ELEMENT_H
#define DS_HINGE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>

#include <JSandbox/Element/NodalSolidElement.h>

class JSANDBOX_EXPORT DSHingeElement : public NodalSolidElement
{
public:
	DSHingeElement(int dim0);
	~DSHingeElement();

	virtual void update_Rest(const VectorXd& vX);
	virtual void update_Mass(const VectorXd& vX);

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual void update_DfxDx(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DfxD0(const VectorXd& vx, const VectorXd& vX);

	virtual void update_fs(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DfsDs(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DfxDs(const VectorXd& vx, const VectorXd& vX);

	virtual void add_fs(VectorXd& vfs);
	virtual void add_DfsDs(tVector& vDfsDs);
	virtual void add_DfxDs(tVector& vDfxDs);

	virtual void update_DmxD0(const VectorXd& vx, const VectorXd& vX) { this->m_mDmxD0.setZero(); }

	virtual Real getIntegrationVolume() const { return m_gFac0; }

	virtual Real getRestAngle() const { return this->m_theta0; }
	virtual void setRestAngle(Real ang) { this->m_theta0 = ang; }

	virtual Real getRestFactor() const { return this->m_gFac0; }
	virtual void setRestFactor(Real fac) { this->m_gFac0 = fac; }

protected:
	Real m_theta0;
	Real m_gFac0;

	VectorXd m_vfs;
	MatrixXd m_mDfsDs;
	MatrixXd m_mDfxDs;

};

#endif