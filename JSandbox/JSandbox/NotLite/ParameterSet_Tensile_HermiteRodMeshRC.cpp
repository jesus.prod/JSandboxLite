/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_HermiteRodMeshRC.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_HermiteRodMeshRC.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_HermiteRodMeshRC.h>

ParameterSet_Tensile_HermiteRodMeshRC::ParameterSet_Tensile_HermiteRodMeshRC(int ridx) : ParameterSet("TENRODMESHRC")
{
	assert(ridx >= 0);
	this->m_ridx = ridx;
}

ParameterSet_Tensile_HermiteRodMeshRC::~ParameterSet_Tensile_HermiteRodMeshRC()
{
	// Nothing to do here
}

void ParameterSet_Tensile_HermiteRodMeshRC::concatenatedToRods(const VectorXd& vpAll, vector<VectorXd>& vpRod) const
{
	int offsetRod = 0;

	int nr = (int)vpRod.size();
	for (int i = 0; i < nr; ++i)
	{
		int nc = (int)vpRod[i].size() / 3;

		for (int j = 0; j < nc; ++j)
		{
			int offsetSplit = 3*j;
			int offsetWhole = 2*this->m_vmapSplit2Whole[offsetRod + j];
			vpRod[i](offsetSplit + 0) = vpAll(offsetWhole + 0);
			vpRod[i](offsetSplit + 2) = vpAll(offsetWhole + 1);
			vpRod[i](offsetSplit + 1) = 0.0;
		}

		offsetRod += nc;
	}
}

void ParameterSet_Tensile_HermiteRodMeshRC::rodsToConcatenated(const vector<VectorXd>& vpRod, VectorXd& vpAll) const
{
	int offsetRod = 0;

	int nr = (int)vpRod.size();
	for (int i = 0; i < nr; ++i)
	{
		int nc = (int)vpRod[i].size() / 3;

		for (int j = 0; j < nc; ++j)
		{
			int offsetSplit = 3*j;
			int offsetWhole = 2*this->m_vmapSplit2Whole[offsetRod + j];
			vpAll(offsetWhole + 0) = vpRod[i](offsetSplit + 0);
			vpAll(offsetWhole + 1) = vpRod[i](offsetSplit + 2);
		}

		offsetRod += nc;
	}
}

void ParameterSet_Tensile_HermiteRodMeshRC::getParameters(VectorXd& vp) const
{
	vp.resize(this->m_np);

	this->rodsToConcatenated(this->m_vcontrolVal, vp);
}

void ParameterSet_Tensile_HermiteRodMeshRC::setParameters(const VectorXd& vp)
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);

	assert((int)vp.size() == this->m_np);

	// Check parameter bounds

	VectorXd vpB = vp;
	this->limitParameters(vpB);
	this->concatenatedToRods(vpB, this->m_vcontrolVal);

	// Compute interpolation

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int nv = rodMesh.getNumNode();
	int nr = rodMesh.getNumRod();

	int offsetRod = 0;
	VectorXd vpInt(3*nv);
	for (int r = 0; r < nr; ++r)
	{
		int nvInt = pModel->getSimRod_0(r).getNumNode();
		int npInt = 3*nvInt;

		VectorXd vpIntRod(npInt);
		bool closed = pModel->getSimRod_0(r).getCurve().getIsClosed();
		dVector vsInt;
		if (closed)
		{
			dVector vsIntTemp = Curve(dVector(npInt+3)).getVertex01Parametrization();
			vsInt.insert(vsInt.end(), vsIntTemp.begin(), vsIntTemp.begin() + nvInt);
		}
		else
		{
			vsInt = pModel->getSimRod_0(r).getCurve().getVertex01Parametrization();
		}

		this->computeValRod(this->m_vcontrolPar[r], this->m_vcontrolVal[r], vsInt, vpIntRod, closed);

		// Assemble interpolated
		for (int j = 0; j < npInt; ++j)
			vpInt(offsetRod + j) = vpIntRod(j);

		offsetRod += npInt;
	}

	// Set rod mesh vertex rest

	pModel->setParam_VertexRest(vpInt);

	for (int j = 0; j < this->m_pTSModel->getNumSurfaceModels(); ++j)
	{
		SurfaceModel* pSurModel = this->m_pTSModel->getSurfaceModel(j);

		// Set surface vertex rest

		VectorXd vpInt2D; // ~Y
		to2D(vpInt, vpInt2D, 1);

		VectorXd vpInt2DBoundary(2 * this->m_vnumB[j]);
		VectorXd vpInt2DCoupling(2 * this->m_vnumC[j]);
		for (int i = 0; i < this->m_vnumB[j]; ++i)
		{
			int offsetB = 2 * i;
			int offsetR = 2 * this->m_vmapBoundary2Rod[j][i];
			vpInt2DBoundary(offsetB + 0) = vpInt2D[offsetR + 0];
			vpInt2DBoundary(offsetB + 1) = vpInt2D[offsetR + 1];
		}
		for (int i = 0; i < this->m_vnumC[j]; ++i)
		{
			int offsetC = 2 * i;
			int offsetR = 2 * this->m_vmapCoupling2Rod[j][i];
			vpInt2DCoupling(offsetC + 0) = vpInt2D[offsetR + 0];
			vpInt2DCoupling(offsetC + 1) = vpInt2D[offsetR + 1];
		}

		pSurModel->setParam_BoundaryRest(vpInt2DBoundary);
		pSurModel->setParam_CouplingRest(vpInt2DCoupling);
	}
}

bool ParameterSet_Tensile_HermiteRodMeshRC::isReady_fp() const
{
	bool meshesReady = true;
	for (int i = 0; i < this->m_pTSModel->getNumSurfaceModels() && meshesReady; ++i)
		meshesReady &=
		this->m_pTSModel->getSurfaceModel(i)->isReady_fb0() &&
		this->m_pTSModel->getSurfaceModel(i)->isReady_fc0();

	return this->m_pTSModel->getRodMeshModel(this->m_ridx)->isReady_f0v() && meshesReady;
}

bool ParameterSet_Tensile_HermiteRodMeshRC::isReady_DfDp() const
{
	bool meshesReady = true;
	for (int i = 0; i < this->m_pTSModel->getNumSurfaceModels() && meshesReady; ++i)
		meshesReady &= 
		this->m_pTSModel->getSurfaceModel(i)->isReady_DfxDb0() &&
		this->m_pTSModel->getSurfaceModel(i)->isReady_DfxDc0();

	return this->m_pTSModel->getRodMeshModel(this->m_ridx)->isReady_DfxD0v() && meshesReady;
}

void ParameterSet_Tensile_HermiteRodMeshRC::get_fp(VectorXd& vfv0) const
{
	throw new exception("Not implemented yet...");

	// TODO

	//this->m_pTSModel->getRodMeshModel(this->m_midx)->add_fv0(vfp);
}

void ParameterSet_Tensile_HermiteRodMeshRC::get_DfDp(tVector& vDfDp_t) const
{
	//this->testHermiteJacobian();

	RodMeshModel* pRodModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);
	int rodModelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_ridx);

	SparseMatrixXd mDiDc;
	this->computeJac(mDiDc);
	int ni = mDiDc.rows();
	int nc = mDiDc.cols();

	// Compute derivative (interpolated points) -------------------------------

	// Add derivatives w.r.t. rod mesh vertex

	tVector vDfxD0v_int_split;
	tVector vDfxD0v_con_split;
	tVector vDfxD0v_con_whole;
	SparseMatrixXd mDfxD0v_int_split(pRodModel->getNumSimDOF_x(), ni);
	SparseMatrixXd mDfxD0v_con_split(pRodModel->getNumSimDOF_x(), nc);
	vDfxD0v_int_split.reserve(pRodModel->getNumNonZeros_DfxD0v());

	pRodModel->add_DfxD0v(vDfxD0v_int_split);
	int nCoefV = (int)vDfxD0v_int_split.size();

	mDfxD0v_int_split.setFromTriplets(vDfxD0v_int_split.begin(), vDfxD0v_int_split.end());

	// Transform to control points

	mDfxD0v_con_split = mDfxD0v_int_split * mDiDc;
	toTriplets(mDfxD0v_con_split, vDfxD0v_con_split);
	nCoefV = (int)vDfxD0v_con_split.size();

	vDfxD0v_con_whole.reserve(nCoefV);

	for (int i = 0; i < nCoefV; ++i)
	{
		const Triplet<Real>& t = vDfxD0v_con_split[i];
		int colSpl = t.col();
		int mod = colSpl % 3;
		int div = this->m_vmapSplit2Whole[colSpl / 3];
		if (mod == 0) vDfxD0v_con_whole.push_back(Triplet<Real>(t.row(), 2 * div + 0, t.value()));
		if (mod == 2) vDfxD0v_con_whole.push_back(Triplet<Real>(t.row(), 2 * div + 1, t.value()));
	}
	this->m_pTSModel->addLocal2Global_Row(rodModelIdx, vDfxD0v_con_whole, vDfDp_t);

	// Add derivatives w.r.t. surface boundary & couplings

	for (int j = 0; j < this->m_pTSModel->getNumSurfaceModels(); ++j)
	{
		SurfaceModel* pSurModel = this->m_pTSModel->getSurfaceModel(j);
		int surModelIdx = this->m_pTSModel->getSurfaceModelOffset(j);

		// Add derivatives w.r.t. surface boundary

		tVector vDfxDb0_int_split;
		tVector vDfxDb0_con_split;
		SparseMatrixXd mDfxDb0_int_split(pSurModel->getNumSimDOF_x(), ni);
		SparseMatrixXd mDfxDb0_con_split(pSurModel->getNumSimDOF_x(), nc);
		vDfxDb0_int_split.reserve(pSurModel->getNumNonZeros_DfxDb0());

		pSurModel->add_DfxDb0(vDfxDb0_int_split);
		int nCoefB = (int)vDfxDb0_int_split.size();

		for (int i = 0; i < nCoefB; ++i)
		{
			const Triplet<Real>& t = vDfxDb0_int_split[i];
			int colDiv = t.col() / 2;
			int colMod = t.col() % 2;
			if (colMod == 0) vDfxDb0_int_split[i] = Triplet<Real>(t.row(), 3 * this->m_vmapBoundary2Rod[j][colDiv] + 0, t.value()); // X
			if (colMod == 1) vDfxDb0_int_split[i] = Triplet<Real>(t.row(), 3 * this->m_vmapBoundary2Rod[j][colDiv] + 2, t.value()); // Z
		}

		// Add derivatives w.r.t. surface coupling

		tVector vDfxDc0_int_split;
		tVector vDfxDc0_con_split;
		SparseMatrixXd mDfxDc0_int_split(pSurModel->getNumSimDOF_x(), ni);
		SparseMatrixXd mDfxDc0_con_split(pSurModel->getNumSimDOF_x(), nc);
		vDfxDc0_int_split.reserve(pSurModel->getNumNonZeros_DfxDc0());

		pSurModel->add_DfxDc0(vDfxDc0_int_split);
		int nCoefC = (int)vDfxDc0_int_split.size();

		for (int i = 0; i < nCoefC; ++i)
		{
			const Triplet<Real>& t = vDfxDc0_int_split[i];
			int colDiv = t.col() / 2;
			int colMod = t.col() % 2;
			if (colMod == 0) vDfxDc0_int_split[i] = Triplet<Real>(t.row(), 3 * this->m_vmapCoupling2Rod[j][colDiv] + 0, t.value()); // X
			if (colMod == 1) vDfxDc0_int_split[i] = Triplet<Real>(t.row(), 3 * this->m_vmapCoupling2Rod[j][colDiv] + 2, t.value()); // Z
		}

		// Build matrices

		mDfxDb0_int_split.setFromTriplets(vDfxDb0_int_split.begin(), vDfxDb0_int_split.end());
		mDfxDc0_int_split.setFromTriplets(vDfxDc0_int_split.begin(), vDfxDc0_int_split.end());

		// Compute derivative (control points) ------------------------------------

		mDfxDb0_con_split = mDfxDb0_int_split * mDiDc;
		mDfxDc0_con_split = mDfxDc0_int_split * mDiDc;
		toTriplets(mDfxDb0_con_split, vDfxDb0_con_split);
		toTriplets(mDfxDc0_con_split, vDfxDc0_con_split);
		nCoefB = (int)vDfxDb0_con_split.size();
		nCoefC = (int)vDfxDc0_con_split.size();

		// Transform to whole 2D

		tVector vDfxDb0_con_whole;
		tVector vDfxDc0_con_whole;
		vDfxDb0_con_whole.reserve(nCoefB);
		vDfxDc0_con_whole.reserve(nCoefC);

		for (int i = 0; i < nCoefB; ++i)
		{
			const Triplet<Real>& t = vDfxDb0_con_split[i];
			int colSpl = t.col();
			int mod = colSpl % 3;
			int div = this->m_vmapSplit2Whole[colSpl / 3];
			if (mod == 0) vDfxDb0_con_whole.push_back(Triplet<Real>(t.row(), 2 * div + 0, t.value()));
			if (mod == 2) vDfxDb0_con_whole.push_back(Triplet<Real>(t.row(), 2 * div + 1, t.value()));
		}

		for (int i = 0; i < nCoefC; ++i)
		{
			const Triplet<Real>& t = vDfxDc0_con_split[i];
			int colSpl = t.col();
			int mod = colSpl % 3;
			int div = this->m_vmapSplit2Whole[colSpl / 3];
			if (mod == 0) vDfxDc0_con_whole.push_back(Triplet<Real>(t.row(), 2 * div + 0, t.value()));
			if (mod == 2) vDfxDc0_con_whole.push_back(Triplet<Real>(t.row(), 2 * div + 1, t.value()));
		}

		this->m_pTSModel->addLocal2Global_Row(surModelIdx, vDfxDb0_con_whole, vDfDp_t);
		this->m_pTSModel->addLocal2Global_Row(surModelIdx, vDfxDc0_con_whole, vDfDp_t);
	}
}

void ParameterSet_Tensile_HermiteRodMeshRC::setup()
{
	assert(this->m_pModel != NULL);

	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);

	int nr = pModel->getNumRod();
	this->m_vcontrolPar.resize(nr);
	this->m_vcontrolVal.resize(nr);

	int npSplit = 0;

	// Use a specific number of control ponints

	for (int r = 0; r < nr; ++r)
	{
		this->m_vcontrolVal[r].resize(3*this->m_vcontrolNum[r]);
		this->m_vcontrolPar[r].resize(this->m_vcontrolNum[r]);
		npSplit += this->m_vcontrolNum[r];
	}

	this->updateControlPoints();
	this->updateCouplingMaps();

	// Count number of independent 

	int npWhole = 0;
	for (int i = 0; i < npSplit; ++i)
		if (this->m_vmapSplit2Whole[i] > npWhole)
			npWhole = this->m_vmapSplit2Whole[i];
	npWhole++;

	this->m_np = 2*npWhole;

	this->updateBoundsVector();
}


void ParameterSet_Tensile_HermiteRodMeshRC::computeJac(SparseMatrixXd& mDiDc) const
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int nr = rodMesh.getNumRod();

	int offsetCon = 0;
	int offsetInt = 0;

	tVector vDiDc;

	for (int r = 0; r < nr; ++r)
	{
		int nvCon = (int) this->m_vcontrolVal[r].size();
		int nvInt = pModel->getSimRod_0(r).getNumNode();
		int npInt = 3 * nvInt;

		tVector vDiDc_r;
		bool closed = pModel->getSimRod_0(r).getCurve().getIsClosed();
		dVector vsInt;
		if (closed)
		{
			dVector vsIntTemp = Curve(dVector(npInt+3)).getVertex01Parametrization();
			vsInt.insert(vsInt.end(), vsIntTemp.begin(), vsIntTemp.begin() + nvInt);
		}
		else
		{
			vsInt = pModel->getSimRod_0(r).getCurve().getVertex01Parametrization();
		}
		this->computeJacRod(this->m_vcontrolPar[r], this->m_vcontrolVal[r], vsInt, vDiDc_r, closed);

		// Assemble interpolate radii

		int numCoe = (int)vDiDc_r.size();
		for (int c = 0; c < numCoe; ++c)
		{
			const Triplet<Real>& t = vDiDc_r[c];
			vDiDc.push_back(Triplet<Real>(offsetInt + t.row(),
										  offsetCon + t.col(),
										  t.value()));
		}

		offsetCon += nvCon;
		offsetInt += npInt;
	}

	mDiDc = SparseMatrixXd(offsetInt, offsetCon);
	mDiDc.setFromTriplets(vDiDc.begin(), vDiDc.end());
}

void ParameterSet_Tensile_HermiteRodMeshRC::computeValRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, VectorXd& vpInt, bool closed) const
{
	int nvCon = (int)vsCon.size();
	int nvInt = (int)vsInt.size();

	int firstInt = 0, lastInt = nvCon - 2;

	int curInt = 0;
	for (int v = 0; v < nvInt; ++v)
	{
		Real si = vsInt[v];
		while (si > vsCon[curInt + 1])
			curInt++; // Next interval

		Real s0;
		Real s1 = vsCon[curInt + 0];
		Real s2 = vsCon[curInt + 1];
		Real s3;

		if (curInt > firstInt && curInt < lastInt)
		{
			s0 = vsCon[curInt - 1];
			s3 = vsCon[curInt + 2];
		}
		else if (curInt == firstInt && curInt == lastInt)
		{
			Real srange = s2 - s1;
			s0 = s1 - srange;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt && !closed)
		{
			s3 = vsCon[curInt + 2];
			Real srange = s2 - s1;
			s0 = s1 - srange;
		}
		else if (curInt == lastInt && !closed)
		{
			s0 = vsCon[curInt - 1];
			Real srange = s2 - s1;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt && closed)
		{
			s3 = vsCon[curInt + 2];
			s0 = vsCon[lastInt - 0] - 1;
		}
		else if (curInt == lastInt && closed)
		{
			s0 = vsCon[curInt - 1];
			s3 = 1 + vsCon[firstInt + 1];
		}

		Real sp1 = (si - s1) / (s2 - s1);
		Real sp2 = sp1*sp1;
		Real sp3 = sp2*sp1;

		int offsetv = 3 * v;
		int offsetvInt0 = 3 * (curInt - 1);
		int offsetvInt1 = 3 * (curInt + 0);
		int offsetvInt2 = 3 * (curInt + 1);
		int offsetvInt3 = 3 * (curInt + 2);

		for (int d = 0; d < 3; ++d)
		{
			int offsetd = offsetv + d;
			int offsetdInt0 = offsetvInt0 + d;
			int offsetdInt1 = offsetvInt1 + d;
			int offsetdInt2 = offsetvInt2 + d;
			int offsetdInt3 = offsetvInt3 + d;

			Real p0;
			Real p1 = vpCon[offsetdInt1];
			Real p2 = vpCon[offsetdInt2];
			Real p3;

			if (curInt > firstInt && curInt < lastInt)
			{
				p0 = vpCon[offsetdInt0];
				p3 = vpCon[offsetdInt3];
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				p0 = p1;
				p3 = p2;
			}
			else if (curInt == firstInt && !closed)
			{
				p3 = vpCon[offsetdInt3];
				p0 = p1;
			}
			else if (curInt == lastInt && !closed)
			{
				p0 = vpCon[offsetdInt0];
				p3 = p2;
			}
			else if (curInt == firstInt && closed)
			{
				p3 = vpCon[offsetdInt3];
				offsetdInt0 = 3*(lastInt - 0) + d;
				p0 = vpCon[offsetdInt0];
			}
			else if (curInt == lastInt && closed)
			{
				p0 = vpCon[offsetdInt0];
				offsetdInt3 = 3*(firstInt + 1) + d;
				p3 = vpCon[offsetdInt3];
			}

			{
#include "../../Maple/Code/CatmullRom3DPoint.mcg"

				vpInt(offsetd) = t25;
			}
		}
	}
}

void ParameterSet_Tensile_HermiteRodMeshRC::computeJacRod(const VectorXd& vsCon, const VectorXd& vpCon, const dVector& vsInt, tVector& vDpDm, bool closed) const
{
	int nvCon = (int)vsCon.size();
	int nvInt = (int)vsInt.size();

	int firstInt = 0, lastInt = nvCon - 2;

	int curInt = 0;
	for (int v = 0; v < nvInt; ++v)
	{
		Real si = vsInt[v];
		while (si > vsCon[curInt + 1])
			curInt++; // Next interval

		Real s0;
		Real s1 = vsCon[curInt + 0];
		Real s2 = vsCon[curInt + 1];
		Real s3;
		if (curInt > firstInt && curInt < lastInt)
		{
			s0 = vsCon[curInt - 1];
			s3 = vsCon[curInt + 2];
		}
		else if (curInt == firstInt && curInt == lastInt)
		{
			Real srange = s2 - s1;
			s0 = s1 - srange;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt && !closed)
		{
			s3 = vsCon[curInt + 2];
			Real srange = s2 - s1;
			s0 = s1 - srange;
		}
		else if (curInt == lastInt && !closed)
		{
			s0 = vsCon[curInt - 1];
			Real srange = s2 - s1;
			s3 = s2 + srange;
		}
		else if (curInt == firstInt && closed)
		{
			s3 = vsCon[curInt + 2];
			s0 = vsCon[lastInt - 0] - 1;
		}
		else if (curInt == lastInt && closed)
		{
			s0 = vsCon[curInt - 1];
			s3 = 1 + vsCon[firstInt + 1];
		}

		Real sp1 = (si - s1) / (s2 - s1);
		Real sp2 = sp1*sp1;
		Real sp3 = sp2*sp1;

		int offsetv = 3 * v;
		int offsetvInt0 = 3 * (curInt - 1);
		int offsetvInt1 = 3 * (curInt + 0);
		int offsetvInt2 = 3 * (curInt + 1);
		int offsetvInt3 = 3 * (curInt + 2);

		for (int d = 0; d < 3; ++d)
		{
			int offsetd = offsetv + d;
			int offsetdInt0 = offsetvInt0 + d;
			int offsetdInt1 = offsetvInt1 + d;
			int offsetdInt2 = offsetvInt2 + d;
			int offsetdInt3 = offsetvInt3 + d;

			Real p0;
			Real p1 = vpCon[offsetdInt1];
			Real p2 = vpCon[offsetdInt2];
			Real p3;

			if (curInt > firstInt && curInt < lastInt)
			{
				p0 = vpCon[offsetdInt0];
				p3 = vpCon[offsetdInt3];
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				p0 = p1;
				p3 = p2;
			}
			else if (curInt == firstInt && !closed)
			{
				p3 = vpCon[offsetdInt3];
				p0 = p1;
			}
			else if (curInt == lastInt && !closed)
			{
				p0 = vpCon[offsetdInt0];
				p3 = p2;
			}
			else if (curInt == firstInt && closed)
			{
				p3 = vpCon[offsetdInt3];
				offsetdInt0 = 3*(lastInt - 0) + d;
				p0 = vpCon[offsetdInt0];
			}
			else if (curInt == lastInt && closed)
			{
				p0 = vpCon[offsetdInt0];
				offsetdInt3 = 3*(firstInt + 1) + d;
				p3 = vpCon[offsetdInt3];
			}

			dVector mJp(4);

			{
#include "../../Maple/Code/CatmullRom3DJacobian.mcg"
			}

			if (curInt > firstInt && curInt < lastInt)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
			else if (curInt == firstInt && curInt == lastInt)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[3]));
			}
			else if (curInt == firstInt && !closed)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
			else if (curInt == lastInt && !closed)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[3]));
			}
			else if (curInt == firstInt && closed)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
			else if (curInt == lastInt && closed)
			{
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt0, mJp[0]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt1, mJp[1]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt2, mJp[2]));
				vDpDm.push_back(Triplet<Real>(offsetd, offsetdInt3, mJp[3]));
			}
		}
	}
}

void ParameterSet_Tensile_HermiteRodMeshRC::updateControlPoints()
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int curIdx = 0;

	this->m_vmapSplit2Whole.reserve(rodMesh.getNumNode());

	// Map connection points to whole indices
	iVector vmapConIdx(rodMesh.getNumCon(), -1);

	int nr = rodMesh.getNumRod();

	for (int r = 0; r < nr; ++r)
	{
		int nv = rodMesh.getRod(r).getNumNode();
		int nc = (int) this->m_vcontrolPar[r].size();
		const Curve& curve = rodMesh.getRod(r).getCurve();

		dVector vsNode = curve.getNodeParameters();

		Real parInc = 1.0 / (nc - 1);

		const pair<int, int>& rodCons = rodMesh.getRodCons(r);

		// First point -----------------------

		this->m_vcontrolPar[r][0] = 0;

		int firstNodeIdx = -1;
		int lastNodeIdx = -1;

		if (rodCons.first == -1)
		{
			if (curve.getIsClosed())
			{
				firstNodeIdx = curIdx++;
				m_vmapSplit2Whole.push_back(firstNodeIdx);
			}
			else
			{
				m_vmapSplit2Whole.push_back(curIdx++);
			}
		}
		else
		{
			if (vmapConIdx[rodCons.first] == -1)
				vmapConIdx[rodCons.first] = curIdx++;

			m_vmapSplit2Whole.push_back(vmapConIdx[rodCons.first]);
		}

		setBlock3x1(0, curve.getPosition(0), this->m_vcontrolVal[r]);

		// Middle points -----------------------

		for (int c = 1; c < nc - 1; ++c)
		{
			int vi, ei;
			Real parUni = c * parInc;
			Real parLen = parUni*vsNode.back();
			Vector3d pos = curve.samplePosition(parLen, vi, ei, 1e-6);

			this->m_vmapSplit2Whole.push_back(curIdx++);
			setBlock3x1(c, pos, this->m_vcontrolVal[r]);
			this->m_vcontrolPar[r](c) = parUni;
		}

		// Last point -----------------------

		this->m_vcontrolPar[r][nc - 1] = 1.0;

		if (rodCons.second == -1)
		{
			if (curve.getIsClosed())
			{
				lastNodeIdx = firstNodeIdx; 
				m_vmapSplit2Whole.push_back(lastNodeIdx);
			}
			else
			{
				m_vmapSplit2Whole.push_back(curIdx++);
			}
		}
		else
		{
			if (vmapConIdx[rodCons.second] == -1)
				vmapConIdx[rodCons.second] = curIdx++;

			m_vmapSplit2Whole.push_back(vmapConIdx[rodCons.second]);
		}

		if (curve.getIsClosed())
		{
			setBlock3x1(nc - 1, curve.getPosition(0), this->m_vcontrolVal[r]);
		}
		else
		{
			setBlock3x1(nc - 1, curve.getPosition(nv - 1), this->m_vcontrolVal[r]);
		}
	}
}

void ParameterSet_Tensile_HermiteRodMeshRC::updateCouplingMaps()
{
	RodMeshModel* pRodModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);
	int rodModelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_ridx);

	for (int i = 0; i < this->m_pTSModel->getNumSurfaceModels(); ++i)
	{
		SurfaceModel* pSurModel = this->m_pTSModel->getSurfaceModel(i);
		int surModelIdx = this->m_pTSModel->getSurfaceModelOffset(i);

		const iVector& vfull2Boundary = pSurModel->getFull2BoundaryMap();
		const iVector& vfull2Coupling = pSurModel->getFull2CouplingMap();

		this->m_vnumB.push_back((int)pSurModel->getBoundaryNodes().size());
		this->m_vnumC.push_back((int)pSurModel->getCouplingNodes().size());
		this->m_vmapBoundary2Rod.push_back(iVector());
		this->m_vmapCoupling2Rod.push_back(iVector());
		this->m_vmapBoundary2Rod.back().resize(this->m_vnumB[i], -1);
		this->m_vmapCoupling2Rod.back().resize(this->m_vnumC[i], -1);

		int numCouplings = this->m_pTSModel->getNumTSCouplings();
		for (int j = 0; j < numCouplings; ++j)
		{
			ModelCoupling* pCoupling = this->m_pTSModel->getTSCoupling(j);

			int surIdx = -1;
			int rodIdx = -1;
			if (pCoupling->getModel0() == rodModelIdx && pCoupling->getModel1() == surModelIdx)
			{
				surIdx = pCoupling->getModelPoint1().getPointIdx();
				rodIdx = pCoupling->getModelPoint0().getPointIdx();
			}
			if (pCoupling->getModel1() == rodModelIdx && pCoupling->getModel0() == surModelIdx)
			{
				surIdx = pCoupling->getModelPoint0().getPointIdx();
				rodIdx = pCoupling->getModelPoint1().getPointIdx();
			}
			if (surIdx == -1 && rodIdx == -1)
				continue; // Not this model!

			if (vfull2Boundary[surIdx] != -1)
			{
				this->m_vmapBoundary2Rod.back()[vfull2Boundary[surIdx]] = rodIdx;
				continue;
			}

			if (vfull2Coupling[surIdx] != -1)
			{
				this->m_vmapCoupling2Rod.back()[vfull2Coupling[surIdx]] = rodIdx;
				continue;
			}

			throw new exception("Should not reach this point! :)");
		}

		// Check if all boundary/coupling points are allocated

		for (int j = 0; j < this->m_vnumB[i]; ++j)
			if (this->m_vmapBoundary2Rod.back()[j] == -1)
				throw new exception("Should not happen! D:");

		for (int j = 0; j < this->m_vnumC[i]; ++j)
			if (this->m_vmapCoupling2Rod.back()[j] == -1)
				throw new exception("Should not happen! D:");
	}
}


void ParameterSet_Tensile_HermiteRodMeshRC::testHermiteJacobian() const
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);

	const RodMesh& rodMesh = pModel->getSimMesh_0();

	int nr = rodMesh.getNumRod();

	int offsetCon = 0;
	int offsetInt = 0;

	tVector vDiDc;

	for (int r = 0; r < nr; ++r)
	{
		int nvCon = (int) this->m_vcontrolVal[r].size();
		int nvInt = 2*pModel->getSimRod_0(r).getNumNode();

		VectorXd vpIntRodP(nvInt);
		VectorXd vpIntRodM(nvInt);
		bool closed = pModel->getSimRod_0(r).getCurve().getIsClosed();
		dVector vsInt = pModel->getSimRod_0(r).getCurve().getVertex01Parametrization();

		VectorXd vcontrolVal = this->m_vcontrolVal[r];

		MatrixXd mDiDc_r(nvInt, nvCon);
		mDiDc_r.setZero(); // Initialize

		for (int c = 0; c < nvCon; ++c)
		{
			// +
			vpIntRodP.setZero();
			vcontrolVal(c) += 1e-6;
			this->computeValRod(this->m_vcontrolPar[r], vcontrolVal, vsInt, vpIntRodP, closed);

			// -
			vpIntRodM.setZero();
			vcontrolVal(c) -= 2e-6;
			this->computeValRod(this->m_vcontrolPar[r], vcontrolVal, vsInt, vpIntRodM, closed);

			// Estimate
			VectorXd vpIntRodDiff = (vpIntRodP - vpIntRodM) / (2e-6);

			for (int e = 0; e < nvInt; ++e)
				mDiDc_r(e, c) = vpIntRodDiff(e);

			// Restore
			vcontrolVal(c) += 1e-6;
		}

		// Assemble interpolate radii

		for (int i = 0; i < nvInt; ++i)
			for (int j = 0; j < nvCon; ++j)
				vDiDc.push_back(Triplet<Real>(offsetInt + i,
											  offsetCon + j,
											  mDiDc_r(i, j)));

		offsetCon += nvCon;
		offsetInt += nvInt;
	}

	SparseMatrixXd mDiDc_F(offsetInt, offsetCon);
	mDiDc_F.setFromTriplets(vDiDc.begin(), vDiDc.end());

	SparseMatrixXd mDiDc_A;
	this->computeJac(mDiDc_A);

	MatrixXd mDiDcDiff = (mDiDc_F.toDense() - mDiDc_A.toDense());
	Real FDNorm = mDiDc_F.norm();
	Real diffNorm = mDiDcDiff.norm();
	Real relError = diffNorm / FDNorm;

	if (FDNorm < 1e-9)
		diffNorm = 0.0;

	if (relError > 1e-9)
	{
		writeToFile(mDiDc_A, "DiDc_A.csv"); // Trace
		writeToFile(mDiDc_F, "DiDc_F.csv"); // Trace
		logSimu("[ERROR] Model: %s. DfDp test error (global): %.9f", this->m_ID.c_str(), relError);
	}
}

Real ParameterSet_Tensile_HermiteRodMeshRC::computeParameterRegularizer_Potential(const VectorXd& vpConc)
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);
	const RodMesh& rodMesh = pModel->getSimMesh_0();
	int nr = rodMesh.getNumRod();

	vector<VectorXd> vpRods = m_vcontrolVal;
	this->concatenatedToRods(vpConc, vpRods);
	
	iVector vdofMap(6);
	vdofMap[0] = -3;
	vdofMap[1] = -1;
	vdofMap[2] = 0;
	vdofMap[3] = 2;
	vdofMap[4] = 3;
	vdofMap[5] = 5;

	Real potential = 0;

	for (int i = 0; i < nr; ++i)
	{
		int nc = (int) vpRods[i].size()/3;

		for (int j = 1; j < nc - 1; ++j)
		{
			int nodeDOF = 3 * j;
			Vector2d x0(vpRods[i][nodeDOF + vdofMap[0]], vpRods[i][nodeDOF + vdofMap[1]]);
			Vector2d x1(vpRods[i][nodeDOF + vdofMap[2]], vpRods[i][nodeDOF + vdofMap[3]]);
			Vector2d x2(vpRods[i][nodeDOF + vdofMap[4]], vpRods[i][nodeDOF + vdofMap[5]]);
			Real K = 1.0;

			{
#include "../../Maple/Code/EquidistantHinge2D_Energy.mcg"
				potential += t22;
			}
		}
	}

	return potential;
}

void ParameterSet_Tensile_HermiteRodMeshRC::computeParameterRegularizer_Hessian(const VectorXd& vpConc, tVector& vH)
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);
	const RodMesh& rodMesh = pModel->getSimMesh_0();
	int nr = rodMesh.getNumRod();

	vector<VectorXd> vpRods = m_vcontrolVal;
	this->concatenatedToRods(vpConc, vpRods);

	vH.reserve(6*6*this->m_vmapSplit2Whole.size());

	int idxOffset = 0;
	
	iVector vdofMap(6);
	vdofMap[0] = -3;
	vdofMap[1] = -1;
	vdofMap[2] = 0;
	vdofMap[3] = 2;
	vdofMap[4] = 3;
	vdofMap[5] = 5;

	for (int i = 0; i < nr; ++i)
	{
		int nc = (int)vpRods[i].size() / 3;

		for (int j = 1; j < nc - 1; ++j)
		{
			int nodeIdx = j;
			int nodeDOF = 3 * j;
			Vector2d x0(vpRods[i][nodeDOF + vdofMap[0]], vpRods[i][nodeDOF + vdofMap[1]]);
			Vector2d x1(vpRods[i][nodeDOF + vdofMap[2]], vpRods[i][nodeDOF + vdofMap[3]]);
			Vector2d x2(vpRods[i][nodeDOF + vdofMap[4]], vpRods[i][nodeDOF + vdofMap[5]]);
			Real K = 1.0;

			double mJx[6][6];

			{
#include "../../Maple/Code/EquidistantHinge2D_Jacobian.mcg"
			}

			for (int n0 = 0; n0 < 3; ++n0)
			for (int n1 = 0; n1 < 3; ++n1)
			for (int k0 = 0; k0 < 2; ++k0)
			for (int k1 = 0; k1 < 2; ++k1)
			{
				vH.push_back(Triplet<Real>(2*m_vmapSplit2Whole[idxOffset + nodeIdx + n0 - 1] + k0,
										   2*m_vmapSplit2Whole[idxOffset + nodeIdx + n1 - 1] + k1,
										   -mJx[2*n0 + k0][2*n0 + k1]));
			}
		}

		idxOffset += nc;
	}
}

void ParameterSet_Tensile_HermiteRodMeshRC::computeParameterRegularizer_Gradient(const VectorXd& vpConc, VectorXd& vg)
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_ridx);
	const RodMesh& rodMesh = pModel->getSimMesh_0();
	int nr = rodMesh.getNumRod();

	vector<VectorXd> vpRods = m_vcontrolVal;
	this->concatenatedToRods(vpConc, vpRods);

	vg.resize(this->m_np);
	vg.setZero();

	int idxOffset = 0;

	iVector vdofMap(6);
	vdofMap[0] = -3;
	vdofMap[1] = -1;
	vdofMap[2] = 0;
	vdofMap[3] = 2;
	vdofMap[4] = 3;
	vdofMap[5] = 5;

	for (int i = 0; i < nr; ++i)
	{
		int nc = (int)vpRods[i].size() / 3;

		for (int j = 1; j < nc - 1; ++j)
		{
			int nodeIdx = j;
			int nodeDOF = 3 * j;
			Vector2d x0(vpRods[i][nodeDOF + vdofMap[0]], vpRods[i][nodeDOF + vdofMap[1]]);
			Vector2d x1(vpRods[i][nodeDOF + vdofMap[2]], vpRods[i][nodeDOF + vdofMap[3]]);
			Vector2d x2(vpRods[i][nodeDOF + vdofMap[4]], vpRods[i][nodeDOF + vdofMap[5]]);
			Real K = 1.0;

			double vfx[6];

			{
#include "../../Maple/Code/EquidistantHinge2D_Force.mcg"
			}

			for (int n = 0; n < 3; ++n)
			for (int k = 0; k < 2; ++k)
			{
				vg(2*m_vmapSplit2Whole[idxOffset + nodeIdx + n - 1] + k) -= vfx[2*n + k];
			}
		}

		idxOffset += nc;
	}
}