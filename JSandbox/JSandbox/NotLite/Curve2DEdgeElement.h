/*=====================================================================================*/
/*!
\file		Curve2DEdgeElement.h
\author		jesusprod
\brief		Basic angle element for a 2D curve.
*/
/*=====================================================================================*/

#ifndef CURVE2D_EDGE_ELEMENT_H
#define CURVE2D_EDGE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/Element/SolidMaterial.h>

#include <JSandbox/Element/NodalSolidElement.h>

class Curve2DEdgeElement : public NodalSolidElement
{
public:
	Curve2DEdgeElement(int dim0);
	~Curve2DEdgeElement();

	virtual void update_Rest(const VectorXd& vX);
	virtual void update_Mass(const VectorXd& vX);

	void setHeterogeneousMaterials(const SolidMaterial* pMatComp, const SolidMaterial* pMatExte)
	{
		assert(pMatComp != NULL);
		assert(pMatExte != NULL);
		this->m_pMatComp = pMatComp;
		this->m_pMatExte = pMatExte;
	}

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getIntegrationVolume() const { return this->m_L0; }

	virtual Real getRestLength() const { return this->m_L0; }
	virtual void setRestLength(Real L0) { this->m_L0 = L0; }

protected:
	Real m_L0; // Rest length

	const SolidMaterial* m_pMatComp;
	const SolidMaterial* m_pMatExte;

	int m_dim0;
};

#endif