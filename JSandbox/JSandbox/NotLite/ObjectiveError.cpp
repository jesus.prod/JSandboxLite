///*=====================================================================================*/
///*!
//\file		ObjectiveError.cpp
//\author		jesusprod
//\brief		Implementation of ObjectiveError.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Optim/ObjectiveError.h>
//
//ObjectiveError::ObjectiveError(const string& ID)
//{
//	this->m_ID = ID;
//}
//
//ObjectiveError::~ObjectiveError()
//{
//	// Nothing to do here
//}
//
//void ObjectiveError::setOptimizer(SQPOptimizerEQ* pOptim)
//{
//	assert(pOptim != NULL);
//
//	this->m_pOptim = pOptim;
//	this->m_pModel = this->m_pOptim->getModel();
//	this->m_pSolver = this->m_pOptim->getSolver();
//}
//
//const SQPOptimizerEQ* ObjectiveError::getOptimizer() const
//{
//	return this->m_pOptim;
//}
//
//const string& ObjectiveError::getID() const
//{
//	return this->m_ID;
//}