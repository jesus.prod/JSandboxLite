/*=====================================================================================*/
/*!
\file		SurfaceRemesherCutter.cpp
\author		jesusprod
\brief		Implementation of SurfaceRemesherCutter.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/SurfaceRemesherCutter.h>

SurfaceRemesherCutter::SurfaceRemesherCutter()
{
	// Nothing to do here
}

SurfaceRemesherCutter::~SurfaceRemesherCutter()
{
	this->freeMemory();
}

const RemeshOP& SurfaceRemesherCutter::remesh(const TriMesh* pMesh, const BVHTree* pEdgeTree, const BVHTree* pFaceTree)
{
	assert(pMesh != NULL);

	// Copy mesh to remesh

	this->m_pMesh = new TriMesh(*pMesh);

	// Create remesh operation

	this->m_remeshOP = this->createOperation();

	this->m_vcutVerts.clear();
	this->m_vdupsMap.clear();

	int nFace = (int) this->m_pMesh->n_faces();
	int nVert = (int) this->m_pMesh->n_vertices();
	int nCutEdge = (int) this->m_vcutEdges.size();

	// Get face partition

	vector<TriMesh::EdgeHandle> vcutEdgesH(nCutEdge);
	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge) // Get edge handlers
		vcutEdgesH[iEdge] = this->m_pMesh->getEdgeHandle(m_vcutEdges[iEdge]);

	vector<vector<TriMesh::FaceHandle>> vfhs; // Get face partition
	this->m_pMesh->getPartitionBoundaryEdgeRegions(vcutEdgesH, vfhs);

	if (vfhs.size() < 2) // OK
		return this->m_remeshOP;

	int nReg = (int)vfhs.size();
	this->m_vdupsMap.resize(nReg);

	iVector vfaceCol(nFace);
	for (int r = 0; r < nReg; ++r)
	{
		int nRegFace = (int)vfhs[r].size();
		for (int f = 0; f < nRegFace; ++f)
			vfaceCol[vfhs[r][f].idx()] = r;
	}

	// Get cut vertices

	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge)
	{
		const TriMesh::EdgeHandle& eh = vcutEdgesH[iEdge];

		vector<TriMesh::VertexHandle> vvh;
		m_pMesh->getEdgeVertices(eh, vvh);

		m_vcutVerts.push_back(this->m_pMesh->getVertID(vvh[0]));
		m_vcutVerts.push_back(this->m_pMesh->getVertID(vvh[1]));
	}

	sort(m_vcutVerts.begin(), m_vcutVerts.end()); // Remove duplicated verts from vector
	m_vcutVerts.erase(unique(m_vcutVerts.begin(), m_vcutVerts.end()), m_vcutVerts.end());

	int nCutVert = (int) this->m_vcutVerts.size();

	// Duplicate vertices

	bVector visCutVert(nVert, false);

	for (int iVert = 0; iVert < nCutVert; ++iVert)
	{
		const MeshTraits::ID& vid = this->m_vcutVerts[iVert]; // Cache ID
		const TriMesh::VertexHandle& vh = this->m_pMesh->getVertHandle(vid);

		vector<TriMesh::FaceHandle> vfh;
		m_pMesh->getVertexFaces(vh, vfh);
		int vnf = (int)vfh.size();
		for (int f = 0; f < vnf; ++f)
		{
			int col = vfaceCol[vfh[f].idx()]; // Check duplicated
			if (m_vdupsMap[col].find(vid) == m_vdupsMap[col].end())
			{ 
				MeshTraits::Point p = this->m_pMesh->point(vh);
				TriMesh::VertexHandle vhnew = this->m_pMesh->add_vertex(p);
				MeshTraits::ID vidnew = TriMesh::CreateIdentifier();
				this->m_remeshOP.m_vAddVert.push_back(vidnew);
				this->m_pMesh->setVertID(vhnew, vidnew);
				m_vdupsMap[col][vid] = vidnew;
			}
		}

		visCutVert[vh.idx()] = true;
	}

	// Rebuild faces

	bVector vrebuiltFaces(nFace, false);
	
	for (int iVert = 0; iVert < nCutVert; ++iVert)
	{
		const MeshTraits::ID& vid = this->m_vcutVerts[iVert]; // Cache ID
		const TriMesh::VertexHandle& vh = this->m_pMesh->getVertHandle(vid);

		if (this->m_pMesh->is_isolated(vh))
			continue; // Already isolated

		vector<TriMesh::FaceHandle> vfh;
		m_pMesh->getVertexFaces(vh, vfh);
		int vnf = (int)vfh.size();
		for (int f = 0; f < vnf; ++f)
		{
			int fidx = vfh[f].idx();
			if (vrebuiltFaces[fidx])
				continue;

			vrebuiltFaces[fidx] = true;
			int col = vfaceCol[fidx]; 

			vector<TriMesh::EdgeHandle> veh;
			m_pMesh->getFaceEdges(vfh[f], veh);
	
			vector<TriMesh::VertexHandle> vvh;
			m_pMesh->getFaceVertices(vfh[f], vvh);

			vector<TriMesh::VertexHandle> vvhNew = vvh; 
			for (int v = 0; v < 3; ++v)
			{
				if (visCutVert[vvh[v].idx()]) // If duplicated, use the new handler stored at map
					vvhNew[v] = m_pMesh->getVertHandle(m_vdupsMap[col][m_pMesh->getVertID(vvh[v])]);
			}

			// Substitute face

			m_pMesh->delete_face(vfh[f], false); // Remove faces and edges
			this->m_remeshOP.m_vDelFace.push_back(m_pMesh->getFaceID(vfh[f]));
			for (int j = 0; j < 3; ++j)
			{
				this->m_remeshOP.m_vDelVert.push_back(m_pMesh->getVertID(vvh[j]));
				this->m_remeshOP.m_vDelEdge.push_back(m_pMesh->getEdgeID(veh[j]));
			}

			TriMesh::FaceHandle fhnew = m_pMesh->add_face(vvhNew);

			// Identifiers
			MeshTraits::ID idnew = TriMesh::CreateIdentifier(); 
			this->m_remeshOP.m_vAddFace.push_back(idnew); 
			m_pMesh->setFaceID(fhnew, idnew);

			// Edges
			vector<TriMesh::EdgeHandle> vehNew;
			m_pMesh->getFaceEdges(fhnew, vehNew);
			for (int e = 0; e < 3; ++e)
			{
				MeshTraits::ID eidnew = TriMesh::CreateIdentifier();
				this->m_remeshOP.m_vAddEdge.push_back(eidnew);
				m_pMesh->setEdgeID(vehNew[e], eidnew);
			}
		}
	}

	this->m_pMesh->delete_isolated_vertices();

	this->m_pMesh->garbage_collection();
	this->m_pMesh->updateIdentifiersMap();

	if (!this->m_remeshOP.m_OK)
	{
		this->freeMemory(); // Invalid
	}
	else
	{
		// Prepare output
	}

	return this->m_remeshOP;
}

//const RemeshOP& SurfaceRemesherCutter::remesh(TriMesh* pMesh, BVHTree* pTree)
//{
//	assert(pMesh != NULL);
//
//	// Copy mesh to remesh
//
//	this->m_pMesh = new TriMesh(*pMesh);
//
//	// Create remesh operation
//
//	this->m_remeshOP = this->createOperation();
//
//	this->m_vcutVerts.clear();
//	this->m_vdupsMap.clear();
//
//	int nFace = (int) this->m_pMesh->n_faces();
//	int nVert = (int) this->m_pMesh->n_vertices();
//	int nCutEdge = (int) this->m_vcutEdges.size();
//
//	// Get face partition
//
//	vector<TriMesh::EdgeHandle> vcutEdgesH(nCutEdge);
//	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge) // Get edge handlers
//		vcutEdgesH[iEdge] = this->m_pMesh->getEdgeHandle(m_vcutEdges[iEdge]);
//
//	vector<vector<TriMesh::FaceHandle>> vfhs; // Get face partition
//	this->m_pMesh->getPartitionBoundaryEdgeRegions(vcutEdgesH, vfhs);
//
//	//vector<TriMesh::FaceHandle> vfhs0;
//	//vector<TriMesh::FaceHandle> vfhs1;
//	//this->m_pMesh->getRegionFacePartition(vcutEdgesH, vfhs0, vfhs1);
//
//	int nReg = (int)vfhs.size();
//	this->m_vdupsMap.resize(nReg);
//
//	iVector vfaceCol(nFace);
//	for (int r = 0; r < nReg; ++r)
//	{
//		int nRegFace = (int)vfhs[r].size();
//		for (int f = 0; f < nRegFace; ++f)
//			vfaceCol[vfhs[r][f].idx()] = r;
//	}
//
//	map<pair<MeshTraits::ID, MeshTraits::ID>, MeshTraits::ID> mFaceVert2Dup;
//
//	// Get cut vertices
//
//	for (int iEdge = 0; iEdge < nCutEdge; ++iEdge)
//	{
//		const TriMesh::EdgeHandle& eh = vcutEdgesH[iEdge];
//
//		vector<TriMesh::VertexHandle> vvh;
//		m_pMesh->getEdgeVertices(eh, vvh);
//
//		vector<TriMesh::FaceHandle> vvfh;
//		int nVertFace = -1;
//		int iVertFace = -1;
//
//		// Vertex 0
//
//		// Vertex 1
//
//		m_vcutVerts.push_back(this->m_pMesh->getVertID(vvh[0]));
//		m_vcutVerts.push_back(this->m_pMesh->getVertID(vvh[1]));
//	}
//
//	sort(m_vcutVerts.begin(), m_vcutVerts.end()); // Sort to erase
//	m_vcutVerts.erase(unique(m_vcutVerts.begin(), m_vcutVerts.end()));
//
//	int nCutVert = (int) this->m_vcutVerts.size();
//
//	// Duplicate vertices
//
//	bVector visCutVert(nVert, false);
//
//	for (int iVert = 0; iVert < nCutVert; ++iVert)
//	{
//		const MeshTraits::ID& vid = this->m_vcutVerts[iVert]; // Cache ID
//		const TriMesh::VertexHandle& vh = this->m_pMesh->getVertHandle(vid);
//
//		vector<TriMesh::FaceHandle> vfh;
//		m_pMesh->getVertexFaces(vh, vfh);
//		int vnf = (int)vfh.size();
//		for (int f = 0; f < vnf; ++f)
//		{
//			int col = vfaceCol[vfh[f].idx()]; // Check duplicated
//			if (m_vdupsMap[col].find(vid) == m_vdupsMap[col].end())
//			{
//				MeshTraits::Point p = this->m_pMesh->point(vh);
//				TriMesh::VertexHandle vhnew = this->m_pMesh->add_vertex(p);
//				MeshTraits::ID vidnew = TriMesh::CreateIdentifier();
//				this->m_remeshOP.m_vAddVert.push_back(vidnew);
//				this->m_pMesh->setVertID(vhnew, vidnew);
//				m_vdupsMap[col][vid] = vidnew;
//			}
//		}
//
//		visCutVert[vh.idx()] = true;
//	}
//
//	// Rebuild faces
//
//	bVector vrebuiltFaces(nFace, false);
//
//	for (int iVert = 0; iVert < nCutVert; ++iVert)
//	{
//		const MeshTraits::ID& vid = this->m_vcutVerts[iVert]; // Cache ID
//		const TriMesh::VertexHandle& vh = this->m_pMesh->getVertHandle(vid);
//
//		if (this->m_pMesh->is_isolated(vh))
//			continue; // Already isolated
//
//		vector<TriMesh::FaceHandle> vfh;
//		m_pMesh->getVertexFaces(vh, vfh);
//		int vnf = (int)vfh.size();
//		for (int f = 0; f < vnf; ++f)
//		{
//			int fidx = vfh[f].idx();
//			if (vrebuiltFaces[fidx])
//				continue;
//
//			vrebuiltFaces[fidx] = true;
//			int col = vfaceCol[fidx];
//
//			vector<TriMesh::EdgeHandle> veh;
//			m_pMesh->getFaceEdges(vfh[f], veh);
//
//			vector<TriMesh::VertexHandle> vvh;
//			m_pMesh->getFaceVertices(vfh[f], vvh);
//
//			vector<TriMesh::VertexHandle> vvhNew = vvh;
//			for (int v = 0; v < 3; ++v)
//			{
//				if (visCutVert[vvh[v].idx()]) // If duplicated, use the new handler stored at map
//					vvhNew[v] = m_pMesh->getVertHandle(m_vdupsMap[col][m_pMesh->getVertID(vvh[v])]);
//			}
//
//			// Substitute face
//
//			m_pMesh->delete_face(vfh[f], false); // Remove faces and edges
//			this->m_remeshOP.m_vDelFace.push_back(m_pMesh->getFaceID(vfh[f]));
//			for (int j = 0; j < 3; ++j)
//			{
//				this->m_remeshOP.m_vDelVert.push_back(m_pMesh->getVertID(vvh[j]));
//				this->m_remeshOP.m_vDelEdge.push_back(m_pMesh->getEdgeID(veh[j]));
//			}
//
//			TriMesh::FaceHandle fhnew = m_pMesh->add_face(vvhNew);
//			MeshTraits::ID idnew = TriMesh::CreateIdentifier();
//			this->m_remeshOP.m_vAddFace.push_back(idnew);
//			m_pMesh->setFaceID(fhnew, idnew);
//		}
//	}
//
//	this->m_pMesh->delete_isolated_vertices();
//
//	this->m_pMesh->garbage_collection();
//	this->m_pMesh->updateIdentifiersMap();
//
//	if (!this->m_remeshOP.m_OK)
//	{
//		this->freeMemory(); // Invalid
//	}
//	else
//	{
//		// Prepare output
//	}
//
//	return this->m_remeshOP;
//}