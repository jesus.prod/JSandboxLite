///*=====================================================================================*/
///*!
//\file		ALOptimizer.cpp
//\author		jesusprod
//\brief		Implementation of ALOptimizer.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Optim/ALOptimizer.h>
//
//ALOptimizer::ALOptimizer(const string& ID)
//{
//	this->m_ID = ID;
//}
//
//ALOptimizer::~ALOptimizer()
//{
//	// Nothing to do here
//}
//
//void ALOptimizer::setSolver(PhysSolver* pSolver)
//{
//	assert(pSolver != NULL);
//	this->m_pSolver = pSolver;
//}
//
//
//void ALOptimizer::setModel(SolidModel* pModel)
//{
//	assert(pModel != NULL);
//	this->m_pModel = pModel;
//}
//
//void ALOptimizer::setTarget(const VectorXd& vt)
//{
//	this->m_vxT = vt;
//}
//
//void ALOptimizer::getTarget(VectorXd& vt) const
//{
//	vt = this->m_vxT;
//}
//
//void ALOptimizer::setFixedParameters(const iVector& vidx)
//{
//	this->m_vfixIdx = vidx;
//}
//
//const iVector& ALOptimizer::getFixedParameters() const
//{
//	return this->m_vfixIdx;
//}
//
//void ALOptimizer::setup()
//{
//	this->m_nX = this->m_pModel->getNumRawDOF(); // Rest configuration
//	this->m_nx = this->m_pModel->getNumSimDOF_x(); // Degrees-of-freedom
//	this->m_np = this->m_nX + this->m_nx;
//	this->init();
//}
//
//bool ALOptimizer::init()
//{
//	// Initialize vectors
//
//	this->m_mu = 1.0;
//
//	this->m_vl = VectorXd(this->m_nx);
//	this->m_vl.setZero(); // From zero
//
//	this->m_mDCDp = SparseMatrixXd(this->m_nx, this->m_np);
//
//	this->pullParameters();
//
//	this->restartConstraints();
//	this->restartGradient();
//	this->restartHessian();
//
//	this->updateConstraints();
//	this->updateGradient();
//	this->updateHessian();
//
//	// Initialize factors
//
//	this->m_tau = 0.1;
//	this->m_gamma = 0.5;
//	this->m_betaConst = 1.0;
//	this->m_betaGradi = 1.0;
//	this->m_alphaConst = 1.0;
//	this->m_alphaGradi = 1.0;
//	this->m_alpha = min(this->m_mu, this->m_gamma);
//	this->m_curGradi = 1e-2 * pow(this->m_alpha, this->m_alphaGradi);
//	this->m_curConst = 1e-2 * pow(this->m_alpha, this->m_alphaConst);
//	this->m_maxConst = 1e-6;
//	this->m_maxGradi = 1e-6;
//
//	//VectorXd vini = this->m_vxT; // Random
//	//for (int i = 0; i < this->m_nx/3; ++i)
//	//{
//	//	bool found = false;
//	//	for (int j = 0; j < this->m_vfixIdx.size()/3; ++j)
//	//	{
//	//		if (m_vfixIdx[3*j] % 3 == 0 && m_vfixIdx[3*j] / 3 == i)
//	//		{
//	//			found = true;
//	//			break; // Out
//	//		}
//	//	}
//
//	//	if (found)
//	//		continue;
//
//	//	Vector3d p = getBlock3x1(i, vini);
//	//	Vector3d r;
//	//	r.setRandom();
//	//	r *= 0.01;
//	//	setBlock3x1(i, p + r, vini);
//	//}
//	//setSubvector(0, this->m_nx, this->m_vxT, this->m_vp);
//	//setSubvector(this->m_nx, this->m_nX, vini, this->m_vp);
//	///setSubvector(this->m_nx, this->m_nX, this->m_vxT, this->m_vp);
//	this->pushParameters(); 
//
//	//logSimu("Target loaded: %s", vectorToString(this->m_vxT).c_str());
//
//	this->m_innerIt = 0;
//	this->m_outerIt = 0;
//	this->m_solved = false;
//
//	return true;
//}
//
//bool ALOptimizer::step()
//{
//	if (this->m_solved)
//		return true;
//
//	this->pullParameters();
//	this->updateConstraints();
//	this->updateGradient();
//
//	//this->testGradient();
//
//	// Current QP converged
//	Real constNorm = this->m_vC.norm();
//	Real gradiNorm = this->m_vg.norm();
//	if (gradiNorm <= this->m_curGradi)
//	{
//		this->m_outerIt++;
//
//		logSimu("[TRACE] Gradient converged after %d iterations. Norm: %.9f\n", this->m_outerIt, gradiNorm);
//
//		if (constNorm <= this->m_curConst)
//		{
//			logSimu("[TRACE] Constraint converged after %d iterations. Norm: %.9f\n", this->m_outerIt, constNorm);
//
//			// Check global convergence: close to
//			// zero gradient and constraints norms
//
//			if (constNorm <= this->m_maxConst && gradiNorm <= this->m_maxGradi)
//			{
//				logSimu("[TRACE] Solution found after (%d, %d) iterations\n", this->m_outerIt, this->m_innerIt);
//				this->m_solved = true;
//				return true;
//			}
//
//			this->updateLambdas();	// Update multipliers
//		}
//		else
//		{
//			this->updatePenalty();	// Decrease penalty
//		}
//	}
//	else
//	{
//		this->m_innerIt++;
//
//		logSimu("[TRACE] Starting quadratic step %d\n", this->m_innerIt);
//
//		Real Ll = this->computeMeritError();
//		//Real Gnorml = this->m_vg.norm();
//
//		logSimu("[TRACE] Merit function: %.9f\n", Ll);
//
//		// QP step for current constraints
//
//		VectorXd vdp; // QP
//		this->getStep(vdp);
//
//		logSimu("[TRACE] Step lenght: %.9f\n", vdp.norm());
//
//		int i = 0;
//		Real beta = 0.5;
//		bool improved = false;
//		VectorXd vpl = this->m_vp;
//		for (i = 0; i < 20; ++i)
//		{
//			this->m_vp += beta*vdp;
//			this->pushParameters();
//			this->updateConstraints();
//			this->updateGradient();
//
//			Real L = this->computeMeritError();
//			//Real Gnorm = this->m_vg.norm();
//
//			if (L < Ll)
//			{
//				improved = true;
//				break; // Return
//			}
//
//			//if (Gnorm < Gnorml)
//			//{
//			//	improved = true;
//			//	break; // Return
//			//}
//
//			beta = beta*0.5;
//			this->m_vp = vpl;
//		}
//
//		if (!improved)
//		{
//			this->pushParameters();
//			this->updateConstraints();
//
//			logSimu("[TRACE] Not improved after %d step\n", i);
//
//			exit(-1);
//		}
//		else
//		{
//			logSimu("[TRACE] Improved after %d step\n", i);
//		}
//
//		this->updateGradient();
//
//		Real L = this->computeMeritError();
//		Real Cnorm = this->m_vC.norm();
//		Real Gnorm = this->m_vg.norm();
//
//		logSimu("[TRACE] LS Merit function: %.9f\n", L);
//		logSimu("[TRACE] Constraint norm: %.9f\n", Cnorm);
//		logSimu("[TRACE] Gradient norm: %.9f\n", Gnorm);
//	}
//
//	logSimu("-------------------------------------------------------\n");
//
//	return false;
//}
//
//void ALOptimizer::getStep(VectorXd& vdp)
//{
//	updateHessian();
//
//	MatrixXd mADense = this->m_mH;
//	VectorXd vb = -1.0*this->m_vg;
//
//	// Fix degrees-of-freedom (both rest and material DOF)
//	for (int i = 0; i < (int) this->m_vfixIdx.size(); ++i)
//	{
//		int matIdx = this->m_vfixIdx[i];
//		int resIdx = matIdx + this->m_nx;
//
//		for (int j = 0; j < this->m_np; ++j)
//		{
//			// Row and column 0.0
//			mADense(matIdx, j) = 0.0;
//			mADense(j, matIdx) = 0.0;
//			mADense(resIdx, j) = 0.0;
//			mADense(j, resIdx) = 0.0;
//		}
//
//		// Diagonal 1.0
//		mADense(matIdx, matIdx) = 1.0;
//		mADense(resIdx, resIdx) = 1.0;
//
//		// Residual 0.0
//		vb(matIdx) = 0.0;
//		vb(resIdx) = 0.0;
//	}
//
//	SparseMatrixXd mASparse = mADense.sparseView(1e-9);
//	
//	// Solve s.t. regularization
//	for (int i = 0; i < 50; ++i)
//	{
//		if (i != 0)
//		{
//			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//
//			// Regularize diagonal
//			for (int ii = 0; ii < this->m_np; ++ii)
//				mASparse.coeffRef(ii, ii) += 0.0001*pow(10.0, i);
//		}
//
//		SimplicialLDLT<SparseMatrixXd> solver;
//
//		solver.compute(mASparse);
//
//		if (solver.info() != Success)
//			continue; // Iterate again
//
//		vdp = solver.solve(vb);
//
//		if (solver.info() != Success)
//			continue; // Iterate again
//
//		VectorXd vbTest = mASparse.selfadjointView<Lower>()*vdp;
//		double error = (vb - vbTest).squaredNorm();
//		if (error > 1e-9) // Iterate again
//			continue; 
//
//		Real dot = vdp.dot(vb);
//		if (dot < 0.0) // Check
//		{
//			writeToFile(mASparse, "OptimizationStepMatrix.csv");
//
//			logSimu("[ERROR] In %s: Not descent direction. Norm g: %.9f, Norm s: %.9f, Dot: %.9f\n", __FUNCTION__, vb.norm(), vdp.norm(), dot);
//
//			continue;
//		}
//
//		logSimu("[GOOD] In %s: Linear system solved using Cholesky\n", __FUNCTION__);
//
//		break; // Solved
//	}
//}
//
//Real ALOptimizer::computeMeritError() const
//{
//	VectorXd vxD = this->m_pModel->getPositions_x() - this->m_vxT;
//
//	Real posError = 0.5*vxD.dot(vxD);
//	Real multiError = this->m_vl.dot(this->m_vC);
//	Real penalError = (0.5 / this->m_mu)*this->m_vC.dot(this->m_vC);
//	
//	return posError - multiError + penalError;
//}
//
//void ALOptimizer::restartConstraints()
//{
//	this->m_vC.resize(this->m_nx);
//	this->m_vC.setZero(); // Init.
//
//	logSimu("[TRACE] Constraints restarted. Norm: %.9f\n", this->m_vC.norm());
//}
//
//void ALOptimizer::updateConstraints()
//{
//	// Constraint vector
//
//	VectorXd vx = this->m_pModel->getPositions_x();
//	VectorXd vv = this->m_pModel->getVelocities_x();
//
//	this->m_vC.resize(this->m_nx);
//	this->m_vC.setZero(); // Init.
//
//	this->m_pModel->addForce(this->m_vC, NULL); // Internal
//	this->m_pSolver->addBoundaryForce(vx, vv, this->m_vC);
//	
//	this->fixConstraintVector(this->m_vC);
//
//	// Constraint Jacobian 
//
//	tVector vDCDp;
//
//	// Derivative w.r.t. material
//	tVector vDfxDx;
//	this->m_pModel->add_DfxDx(vDfxDx, NULL); // Internal
//	this->m_pSolver->addBoundaryJacobian(vx, vv, vDfxDx);
//
//	// Derivative w.r.t. rest
//	tVector vDfxDX;
//	m_pModel->add_DfxD0(vDfxDX);
//
//	this->fixConstraintJacobian(vDfxDx);
//	this->fixConstraintJacobian(vDfxDX);
//
//	int numCoeffsx = (int)vDfxDx.size();
//	int numCoeffsX = (int)vDfxDX.size();
//	for (int i = 0; i < numCoeffsx; ++i)
//	{
//		vDCDp.push_back(vDfxDx[i]);
//	}
//	for (int i = 0; i < numCoeffsX; ++i)
//	{
//		vDCDp.push_back(Triplet<Real>(vDfxDX[i].row(), // Same row
//									  vDfxDX[i].col() + this->m_nx,
//									  vDfxDX[i].value()));
//	}
//
//	this->m_mDCDp.setFromTriplets(vDCDp.begin(), vDCDp.end());
//	this->m_mDCDp.makeCompressed(); // Compress sparse structure
//}
//
//void ALOptimizer::restartGradient()
//{
//	this->m_vg.resize(this->m_np);
//	this->m_vg.setZero(); // Init.
//
//	logSimu("[TRACE] Gradient restarted. Norm: %.9f\n", this->m_vg.norm());
//}
//
//void ALOptimizer::updateGradient()
//{
//	updateGradientA();
//	//updateGradientFD();
//}
//
//Real ALOptimizer::computePositionError() const
//{
//	VectorXd vxD = this->m_pModel->getPositions_x() - this->m_vxT;
//
//	return 0.5*vxD.dot(vxD);
//}
//
//VectorXd ALOptimizer::computePositionGradient() const
//{
//	VectorXd vxD = this->m_pModel->getPositions_x() - this->m_vxT;
//
//	VectorXd DhDp(this->m_np);
//	DhDp.setZero(); // From zero
//	setSubvector(0, this->m_nx, vxD, DhDp);
//
//	this->fixParameterVector(DhDp);
//
//	return DhDp;
//}
//
//VectorXd ALOptimizer::computePositionGradientFD()
//{
//	Real EPS = 1e-6;
//
//	VectorXd vg(this->m_np);
//	vg.setZero(); // From zero
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lp = this->computePositionError();
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lm = this->computePositionError();
//
//		// Estimate
//		vg[i] = (Lp - Lm) / (2 * EPS);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//
//	this->fixParameterVector(vg);
//
//	return vg;
//}
//
//Real ALOptimizer::computeMultiplierError() const
//{
//	return this->m_vl.dot(this->m_vC);
//}
//
//VectorXd ALOptimizer::computeMultiplierGradient() const
//{
//	const VectorXd& vx = this->m_pModel->getPositions_x();
//	const VectorXd& vv = this->m_pModel->getVelocities_x();
//
//	VectorXd LmuC(this->m_nx);
//	LmuC.setZero(); // From zero
//	LmuC = this->m_vl;
//	VectorXd DCDp = LmuC.transpose()*this->m_mDCDp;
//
//	this->fixParameterVector(DCDp);
//
//	return DCDp;
//}
//
//VectorXd ALOptimizer::computeMultiplierGradientFD()
//{
//	Real EPS = 1e-6;
//
//	VectorXd vg(this->m_np);
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lp = this->computeMultiplierError();
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lm = this->computeMultiplierError();
//
//		// Estimate
//		vg[i] = (Lp - Lm) / (2 * EPS);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//
//	this->fixParameterVector(vg);
//
//	return vg;
//}
//
//Real ALOptimizer::computePenaltyError() const
//{
//	return (0.5 / this->m_mu)*this->m_vC.dot(this->m_vC);
//}
//
//VectorXd ALOptimizer::computePenaltyGradient() const
//{
//	const VectorXd& vx = this->m_pModel->getPositions_x();
//	const VectorXd& vv = this->m_pModel->getVelocities_x();
//
//	VectorXd LmuC(this->m_nx);
//	LmuC.setZero(); // From zero
//	LmuC = (1.0 / this->m_mu)*this->m_vC;
//	VectorXd DCDp = LmuC.transpose()*this->m_mDCDp;
//
//	this->fixParameterVector(DCDp);
//
//	return DCDp;
//}
//
//VectorXd ALOptimizer::computePenaltyGradientFD()
//{
//	Real EPS = 1e-6;
//
//	VectorXd vg(this->m_np);
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lp = this->computePenaltyError();
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lm = this->computePenaltyError();
//
//		// Estimate
//		vg[i] = (Lp - Lm) / (2 * EPS);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//
//	this->fixParameterVector(vg);
//
//	return vg;
//}
//
//void ALOptimizer::updateHessian()
//{
//	//updateHessianA();
//	//updateHessianFD();
//	//updateHessian2ND();
//	updateHessianBFGS();
//	//restartHessian();
//}
//
//void ALOptimizer::updateGradientA()
//{
//	//this->m_pModel->testForceLocal();
//	//this->m_pModel->testJacobianLocal();
//	//this->m_pModel->testForceGlobal();
//	//this->m_pModel->testDfxDxGlobal();
//	//this->m_pModel->testDfxD0Global();
//
//	const VectorXd& vx = this->m_pModel->getPositions_x();
//	const VectorXd& vv = this->m_pModel->getVelocities_x();
//
//	VectorXd vxD = vx - this->m_vxT;
//
//	VectorXd DhDp(this->m_np);
//	DhDp.setZero(); // From zero
//	setSubvector(0, this->m_nx, vxD, DhDp);
//
//	VectorXd LmuC(this->m_nx);
//	LmuC.setZero(); // From zero
//	LmuC = (1.0/this->m_mu)*this->m_vC - this->m_vl;
//	VectorXd DCDp = LmuC.transpose()*this->m_mDCDp;
//
//	this->m_vg = DhDp + DCDp;
//
//	this->fixParameterVector(this->m_vg);
//}
//
//void ALOptimizer::updateGradientFD()
//{
//	Real EPS = 1e-6;
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lp = this->computeMeritError();
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lm = this->computeMeritError();
//
//		// Estimate
//		this->m_vg[i] = (Lp - Lm)/(2*EPS);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//
//	this->fixParameterVector(this->m_vg);
//}
//
//void ALOptimizer::restartHessian()
//{
//	this->m_mH.resize(this->m_np, this->m_np);
//	this->m_mH.setIdentity(); // Init. from I
//
//	m_modifier1.resize(this->m_np, this->m_np);
//	m_modifier2.resize(this->m_np, this->m_np);
//	m_tempMatrix.resize(this->m_np, this->m_np);
//	
//	m_sk.resize(this->m_np);
//	m_yk.resize(this->m_np);
//	m_vp_P = this->m_vp;
//	m_vg_P = this->m_vg;
//
//	logSimu("[TRACE] Hessian restarted. Identity\n");
//}
//
//void ALOptimizer::updateHessianA()
//{
//	assert(false); // TODO: True Hessian
//}
//
//void ALOptimizer::updateHessianFD()
//{
//	Real EPS = 1e-6;
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		this->updateGradient();
//
//		VectorXd vgp = this->m_vg;
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		this->updateGradient();
//
//		VectorXd vgm = this->m_vg;
//
//		// Estimate
//		VectorXd Hcol = (vgp - vgm) / (2 * EPS);
//		for (int j = 0; j < this->m_np; ++j)
//			this->m_mH(j, i) = Hcol(j);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//	this->updateGradient();
//}
//
//void ALOptimizer::updateHessian2ND()
//{
//	const VectorXd& vx = this->m_pModel->getPositions_x();
//	const VectorXd& vv = this->m_pModel->getVelocities_x();
//
//	// Derivative w.r.t. material
//	tVector vDfxDx;
//	this->m_pModel->add_DfxDx(vDfxDx, NULL); // Internal
//	this->m_pSolver->addBoundaryJacobian(vx, vv, vDfxDx);
//
//	// Derivative w.r.t. rest
//	tVector vDfxDX;
//	m_pModel->add_DfxD0(vDfxDX);
//
//	tVector vDfDp;
//	int numCoeffsx = (int)vDfxDx.size();
//	int numCoeffsX = (int)vDfxDX.size();
//	for (int i = 0; i < numCoeffsx; ++i)
//	{
//		vDfDp.push_back(vDfxDx[i]);
//	}
//	for (int i = 0; i < numCoeffsX; ++i)
//	{
//		vDfDp.push_back(Triplet<Real>(vDfxDX[i].row(), // Same row
//									   vDfxDX[i].col() + this->m_nx,
//									   vDfxDX[i].value()));
//	}
//
//	// Constraints derivative w.r.t. all parameters
//	SparseMatrixXd mJSparse(this->m_nx, this->m_np);
//	mJSparse.setFromTriplets(vDfDp.begin(), vDfDp.end());
//	mJSparse.makeCompressed(); // Sparse matrix compression
//
//	MatrixXd DCDp(this->m_nx, this->m_np);
//	DCDp = mJSparse.toDense(); // Go dense
//
//	MatrixXd I(this->m_np, this->m_np);
//	I.setIdentity(); // Identity matrix
//
//	this->m_mH = I + (1.0 / this->m_mu)*(DCDp.transpose()*DCDp);
//}
//
//void ALOptimizer::updateHessianBFGS()
//{
//	m_sk = this->m_vp - this->m_vp_P;
//	m_yk = this->m_vg - this->m_vg_P;
//
//	this->m_vp_P = this->m_vp;
//	this->m_vg_P = this->m_vg;
//
//	double D = m_yk.dot(m_sk);
//
//	if (fabs(D) > 1e-6) // ?
//	{
//		m_rho = 1.0 / D; // For too small discriminant, the update is invalid
//
//		// Update the Hessian matrix (direct one) (Numerical Optimization, Nocedal, Wright, page 25)
//		// Hk+1 = (I - rho * m_sk * m_yk^T) * Hk * (I - rho * m_yk * m_sk^T) + rho * m_yk * m_yk^T
//		for (int i = 0; i < this->m_np; i++)
//		{
//			for (int j = 0; j <= i; j++)
//			{
//				if (i == j)
//				{
//					m_modifier1(i, i) = 1.0 - m_rho * m_yk[i] * m_sk[i];
//				}
//				else
//				{
//					m_modifier1(i, j) = -m_rho * m_sk[i] * m_yk[j];
//					m_modifier1(j, i) = -m_rho * m_sk[j] * m_yk[i];
//				}
//
//				m_modifier2(i, j) = m_rho * m_yk[i] * m_yk[j];
//				m_modifier2(j, i) = m_rho * m_yk[j] * m_yk[i];
//			}
//		}
//
//		this->m_mH = m_modifier1*this->m_mH*m_modifier1.transpose();
//		this->m_mH = this->m_mH + m_modifier2; // Add Hessian modifier
//
//		logSimu("[TRACE] Hessian updated. Disc: %.9f\n", D);
//	}
//	else
//	{
//		// Initialize to FD Hess
//		this->updateHessianFD();
//		//this->restartHessian();
//	}
//}
//
//void ALOptimizer::testGradient()
//{
//	this->updateGradientFD();
//	VectorXd vgFD = this->m_vg;
//
//	this->updateGradientA();
//	VectorXd vgA = this->m_vg;
//
//	VectorXd vgDiff = vgA - vgFD;
//	Real vgDiffNorm = vgDiff.norm(); 
//	Real vgFDNorm = vgFD.norm();
//	if (vgFDNorm > 1e-9)
//	{
//		Real relativeError = vgDiffNorm/vgFDNorm;
//		if (relativeError > 1e-9) // Significative
//		{
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//			logSimu("[ERROR] Gradient test. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgFDNorm, vgDiffNorm, relativeError);
//			
//			//VectorXd vgPositionA = this->computePositionGradient();
//			//VectorXd vgPositionFD = this->computePositionGradientFD();
//			//VectorXd vgPositionDiff = vgPositionA - vgPositionFD;
//			//Real vgPositionDiffNorm = vgPositionDiff.norm();
//			//Real vgPositionFDNorm = vgPositionFD.norm();
//			//if (vgPositionFDNorm > 1e-9)
//			//{
//			//	logSimu("\t- Position. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgPositionFDNorm, vgPositionDiffNorm, vgPositionDiffNorm / vgPositionFDNorm);
//			//	writeToFile(vgPositionDiff, "positionGradientError.csv");
//			//}
//			//else logSimu("\t- Position. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgPositionFDNorm, vgPositionDiffNorm, -1); // Invalid value of relative error 
//
//			//VectorXd vgMultiplierA = this->computeMultiplierGradient();
//			//VectorXd vgMultiplierFD = this->computeMultiplierGradientFD();
//			//VectorXd vgMultiplierDiff = vgMultiplierA - vgMultiplierFD;
//			//Real vgMultiplierDiffNorm = vgMultiplierDiff.norm();
//			//Real vgMultiplierFDNorm = vgMultiplierFD.norm();
//			//if (vgMultiplierFDNorm > 1e-9)
//			//{
//			//	logSimu("\t- Multiplier. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgMultiplierFDNorm, vgMultiplierDiffNorm, vgMultiplierDiffNorm / vgMultiplierFDNorm);
//			//	writeToFile(vgMultiplierDiff, "multiplierGradientError.csv");
//			//}
//			//else logSimu("\t- Multiplier. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgMultiplierFDNorm, vgMultiplierDiffNorm, -1); // Invalid value of relative error 
//
//			//VectorXd vgPenaltyA = this->computePenaltyGradient();
//			//VectorXd vgPenaltyFD = this->computePenaltyGradientFD();
//			//VectorXd vgPenaltyDiff = vgPenaltyA - vgPenaltyFD;
//			//Real vgPenaltyDiffNorm = vgPenaltyDiff.norm();
//			//Real vgPenaltyFDNorm = vgPenaltyFD.norm();
//			//if (vgPenaltyFDNorm > 1e-9)
//			//{
//			//	logSimu("\t- Penalty. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgPenaltyFDNorm, vgPenaltyDiffNorm, vgPenaltyDiffNorm / vgPenaltyFDNorm);
//			//	writeToFile(vgPenaltyDiff, "penaltyGradientError.csv");
//			//}
//			//else logSimu("\t- Penalty. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgPenaltyFDNorm, vgPenaltyDiffNorm, -1); // Invalid value of relative error 
//
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//		}
//	}
//}
//
//void ALOptimizer::testHessian()
//{
//	//this->updateHessianFD();
//	//MatrixXd mHFD = this->m_mH;
//
//	//this->updateHessianA();
//	//const MatrixXd& mHA = this->m_mH;
//
//	//MatrixXd mHDiff = mHA - mHFD;
//	//Real mHDiffNorm = mHDiff.norm();
//	//Real mHFDNorm = mHFD.norm();
//	//if (mHFDNorm)
//	//{
//	//	Real relativeError = mHDiffNorm / mHFDNorm;
//	//	if (relativeError >= 1e-9) // Significative
//	//	{
//	//		logSimu("[ERROR] Hessian test error: %.9f", relativeError);
//	//		logSimu("%s", vectorToString(mHDiff).c_str()); // Trace
//	//	}
//	//}
//}
//
//void ALOptimizer::updateLambdas()
//{
//	this->m_vl = this->m_vl - this->m_vC*(1.0 / this->m_mu);
//	this->m_mu = this->m_mu; // No change in penalty weight
//	this->m_alpha = this->m_mu;
//	this->m_curConst = this->m_curConst*pow(this->m_alpha, this->m_betaConst);
//	this->m_curGradi = this->m_curGradi*pow(this->m_alpha, this->m_betaGradi);
//	if (this->m_curConst < this->m_maxConst)
//		this->m_curConst = this->m_maxConst;
//	if (this->m_curGradi < this->m_maxGradi)
//		this->m_curGradi = this->m_maxGradi;
//
//	logSimu("[TRACE] Lambdas updated. Values:\n");
//	logSimu("\t- Lambda norm: %.9f\n", this->m_vl.norm());
//	logSimu("\t- Mu: %.9f\n", this->m_mu);
//	logSimu("\t- Alpha: %.9f\n", this->m_alpha);
//	logSimu("\t- Const. Threshold: %.9f\n", this->m_curConst);
//	logSimu("\t- Gradi. Threshold: %.9f\n", this->m_curGradi);
//
//	this->fixConstraintVector(this->m_vl);
//}
//
//void ALOptimizer::updatePenalty()
//{
//	this->m_vl = this->m_vl; // No change
//	this->m_mu = this->m_mu * this->m_tau; 
//	this->m_alpha = this->m_mu * this->m_gamma;
//	this->m_curConst = this->m_curConst*pow(this->m_alpha, this->m_betaConst);
//	this->m_curGradi = this->m_curGradi*pow(this->m_alpha, this->m_betaGradi);
//	if (this->m_curConst < this->m_maxConst)
//		this->m_curConst = this->m_maxConst;
//	if (this->m_curGradi < this->m_maxGradi)
//		this->m_curGradi = this->m_maxGradi;
//
//	logSimu("[TRACE] Lambdas updated. Values:\n");
//	logSimu("\t- Lambda norm: %.9f\n", this->m_vl.norm());
//	logSimu("\t- Mu: %.9f\n", this->m_mu);
//	logSimu("\t- Alpha: %.9f\n", this->m_alpha);
//	logSimu("\t- Const. Threshold: %.9f\n", this->m_curConst);
//	logSimu("\t- Gradi. Threshold: %.9f\n", this->m_curGradi);
//}
//
//void ALOptimizer::pullParameters()
//{
//	this->m_vp.resize(this->m_np);
//	this->m_vp.setZero(); // Init.
//	setSubvector(0, this->m_nx, this->m_pModel->getPositions_x(), this->m_vp);
//	//setSubvector(this->m_nx, this->m_nX, this->m_pModel->getPositions_0(), this->m_vp);
//}
//
//void ALOptimizer::pushParameters()
//{
//	VectorXd vx;
//	VectorXd vX;
//	getSubvector(0, this->m_nx, this->m_vp, vx);
//	getSubvector(this->m_nx, this->m_nX, this->m_vp, vX);
//	this->m_pModel->updateState_x(vx, this->m_pModel->getVelocities_x());
//	//this->m_pModel->setParam_Rest(vX, this->m_pModel->getVelocities_0());
//}
//
//void ALOptimizer::fixParameterVector(VectorXd& vp) const
//{
//	// Fix degrees-of-freedom (both rest and material)
//	for (int i = 0; i < (int) this->m_vfixIdx.size(); ++i)
//	{
//		int matIdx = this->m_vfixIdx[i];
//		int resIdx = matIdx + this->m_nx;
//		
//		// Zero movement
//		vp(matIdx) = 0.0;
//		vp(resIdx) = 0.0;
//	}
//}
//
//void ALOptimizer::fixConstraintVector(VectorXd& vc) const
//{
//	// Fix degrees-of-freedom (material coordinate only)
//	for (int i = 0; i < (int) this->m_vfixIdx.size(); ++i)
//	{
//		int matIdx = this->m_vfixIdx[i];
//		vc(matIdx) = 0.0; // Zero value
//	}
//}
//
//void ALOptimizer::fixConstraintJacobian(tVector& vJ) const
//{
//	bVector fixedStencil(this->m_nx, false);
//	for (int i = 0; i < (int)this->m_vfixIdx.size(); ++i)
//		fixedStencil[this->m_vfixIdx[i]] = true; // Fixed
//
//	int nEntries = (int)vJ.size();
//	for (int i = 0; i < nEntries; ++i)
//	{
//		Triplet<Real>& t = vJ[i];
//		int r = t.row();
//		int c = t.col();
//		
//		if (fixedStencil[r] || fixedStencil[c])
//			vJ[i] = Triplet<Real>(r, c, 0.0);
//
//		if (fixedStencil[r] && fixedStencil[c])
//			vJ[i] = Triplet<Real>(r, c, 1.0);
//	}
//}