/*=====================================================================================*/
/*!
\file		Curve2DAngleElement.h
\author		jesusprod
\brief		Basic angle element for a 2D curve.
*/
/*=====================================================================================*/

#ifndef CURVE2D_ANGLE_ELEMENT_H
#define CURVE2D_ANGLE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>
#include <JSandbox/Element/SolidMaterial.h>

#include <JSandbox/Element/NodalSolidElement.h>

class Curve2DAngleElement : public NodalSolidElement
{
public:
	Curve2DAngleElement(int dim0);
	~Curve2DAngleElement();

	virtual void update_Rest(const VectorXd& vX);
	virtual void update_Mass(const VectorXd& vX);

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual Real getIntegrationVolume() const { return this->m_vL0; }

	virtual Real getRestAngle() const { return this->m_phi0; }
	virtual void setRestAngle(Real phi0) { this->m_phi0 = phi0; }

	virtual const Vector3d& getReferenceVector() const { return this->m_ref; }
	virtual void setReferenceVector(const Vector3d& vr) { this->m_ref = vr; }

protected:
	Real m_phi0; // Rest angle
	Real m_vL0; // Vertex length

	int m_dim0;

	Vector3d m_ref;

};

#endif