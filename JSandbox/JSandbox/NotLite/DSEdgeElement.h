/*=====================================================================================*/
/*!
\file		DSEdgeElement.h
\author		jesusprod,bthomasz
\brief		Basic implementation of a discrete-shell edge element. Adapted from bthomasz
			implementation for Phys3D DeformablesSurface plugin. This provides a simple
			non-linear energy density for edge length preserving. It uses SolidMaterial
			stretch K.
*/
/*=====================================================================================*/

#ifndef DS_EDGE_ELEMENT_H
#define DS_EDGE_ELEMENT_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/SolidMaterial.h>

#include <JSandbox/Element/NodalSolidElement.h>

class DSEdgeElement : public NodalSolidElement
{
public:
	DSEdgeElement(int dim0);
	~DSEdgeElement();

	virtual void update_Rest(const VectorXd& vX);
	virtual void update_Mass(const VectorXd& vX);

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual void update_DfxDx(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DfxD0(const VectorXd& vx, const VectorXd& vX);

	virtual void update_DmxD0(const VectorXd& vx, const VectorXd& vX);

	virtual Real getIntegrationVolume() const { return this->m_L0; }

	virtual Real getRestLength() const { return this->m_L0; }

protected:
	Real m_L0; // Rest length
};

#endif