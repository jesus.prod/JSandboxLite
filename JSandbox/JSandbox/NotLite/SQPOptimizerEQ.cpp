///*=====================================================================================*/
///*!
//\file		SQPOptimizerEQ.cpp
//\author		jesusprod
//\brief		Implementation of SQPOptimizerEQ.h
//*/
///*=====================================================================================*/
//
//#include <JSandbox/Optim/SQPOptimizerEQ.h>
//
//SQPOptimizerEQ::SQPOptimizerEQ(const string& ID)
//{
//	this->m_ID = ID;
//
//	this->m_nobjSet = 0;
//	this->m_nparamSet = 0;
//}
//
//SQPOptimizerEQ::~SQPOptimizerEQ()
//{
//	freeParameters();
//	freeObjectives();
//}
//
//void SQPOptimizerEQ::setObjectiveErrors(const vector<ObjectiveError*>& vpobjs)
//{
//	// At least one objective error
//	assert((int)vpobjs.size() >= 1);
//
//	freeObjectives();
//
//	this->m_vobjSet = vpobjs;
//	this->m_nobjSet = (int) vpobjs.size();
//}
//
//void SQPOptimizerEQ::freeObjectives()
//{
//	if (this->m_nobjSet != 0)
//	{
//		for (int i = 0; i < m_nobjSet; ++i)
//			delete this->m_vobjSet[i];
//		this->m_vobjSet.clear();
//		this->m_nobjSet = 0;
//	}
//}
//
//const vector<ObjectiveError*>& SQPOptimizerEQ::getObjectiveErrors() const
//{
//	return this->m_vobjSet;
//}
//
//void SQPOptimizerEQ::setParameterSets(const vector<ParameterSet*>& vpsets)
//{
//	// At least material coordinates
//	assert((int)vpsets.size() >= 1);
//
//	freeParameters();
//
//	this->m_vparamSet = vpsets;
//	this->m_nparamSet = (int) vpsets.size();
//}
//
//void SQPOptimizerEQ::freeParameters()
//{
//	int numParams = (int) this->m_vparamSet.size();
//	if (numParams != 0)
//	{
//		for (int i = 0; i < numParams; ++i)
//			delete this->m_vparamSet[i];
//		this->m_vparamSet.clear();
//		this->m_nparamSet = 0;
//	}
//}
//
//const vector<ParameterSet*>& SQPOptimizerEQ::getParameterSets() const
//{
//	return this->m_vparamSet;
//}
//
//void SQPOptimizerEQ::setSolver(PhysSolver* pSolver)
//{
//	assert(pSolver != NULL);
//	this->m_pSolver = pSolver;
//}
//
//PhysSolver* SQPOptimizerEQ::getSolver() const
//{
//	return this->m_pSolver;
//}
//
//SolidModel* SQPOptimizerEQ::getModel() const
//{
//	return this->m_pModel;
//}
//
//void SQPOptimizerEQ::setModel(SolidModel* pModel)
//{
//	assert(pModel != NULL);
//	this->m_pModel = pModel;
//}
//
//// DEPRECATED
////void SQPOptimizerEQ::setTarget(const VectorXd& vt)
////{
////	this->m_vxT = vt;
////}
////
////void SQPOptimizerEQ::getTarget(VectorXd& vt) const
////{
////	vt = this->m_vxT;
////}
//
//void SQPOptimizerEQ::setup()
//{
//	// Setup parameters
//
//	this->m_np = 0;
//	
//	this->m_vparamOff.clear(); // Set offsets
//	for (int i = 0; i < this->m_nparamSet; ++i)
//	{
//		this->m_vparamSet[i]->setup();
//
//		this->m_vparamOff.push_back(this->m_np); // Par. offset
//		this->m_np += this->m_vparamSet[i]->getNumParameter();
//	}
//
//	// Setup objectives
//
//	for (int i = 0; i < this->m_nobjSet; ++i)
//		this->m_vobjSet[i]->setup(); // Setup
//
//	// Setup constraints
//	this->m_nc = this->m_pModel->getNumSimDOF_x();
//	assert(this->m_nc == this->m_vparamOff[1]);
//
//	this->m_nx = this->m_nc;
//
//	this->init();
//}
//
//bool SQPOptimizerEQ::init()
//{
//	// Initialize vectors
//
//	this->m_vl = VectorXd(this->m_nc);
//	this->m_vl.setZero(); // From zero
//	//this->m_vl.setOnes();
//	//this->m_vl *= 0.1;
//
//	this->fixConstraintVector(this->m_vl);
//
//	this->m_mAs = SparseMatrixXd(this->m_nc, this->m_np);
//
//	this->pullParameters();
//
//	this->m_mu = 0.001;
//	this->m_tau = 0.2;
//	this->m_psi = 10.0;
//	this->m_alpha = 0.5;
//	this->m_gamma = 1e-2;
//	this->m_theta = 1e-8;
//	this->m_sigma = this->m_tau*(1 - this->m_gamma);
//
//	// Initialize smooth regularization 
//
//	this->m_smoothFactor = 0.9;
//	this->m_smoothInitial = 0.01;
//	this->m_smooth = this->m_smoothInitial;
//
//	this->restartConstraints();
//	this->restartGradient();
//	this->restartHessian();
//	
//	this->updateConstraints();
//	this->updateGradient();
//	this->updateHessian();
//
//	// Initialize constants
//
//	this->m_maxConst = 1e-6*max(this->m_vC.cwiseAbs().maxCoeff(), 1.0);
//	this->m_maxGradi = 1e-6*max(this->m_vG.cwiseAbs().maxCoeff(), 1.0);
//
//	// DEBUG tests...
//
//	//this->m_pModel->testD2fxDxxLocal();
//	//this->m_pModel->testD2fxDx0Local();
//	//this->m_pModel->testD2fxD00Local();
//
//	//this->m_pModel->testD2fxDxxGlobal();
//	//this->m_pModel->testD2fxDx0Global();
//	//this->m_pModel->testD2fxD00Global();
//
//	//this->testThirdOrderTensor();
//
//	//this->testJacobian();
//	//this->testGradient();
//	//this->testHessian();
//
//	this->pushParameters();
//
//	//logSimu("Target loaded: %s", vectorToString(this->m_vxT).c_str());
//
//	return true;
//}
//
//bool SQPOptimizerEQ::step()
//{
//	//this->testGradient();
//
//	this->pullParameters();
//	this->updateConstraints();
//	this->updateGradient();
//
//	// Current QP converged
//	Real constNorm = this->m_vC.norm();
//	Real gradiNorm = this->m_vG.norm();
//
//	if (constNorm <= this->m_maxConst &&
//		gradiNorm <= this->m_maxGradi)
//		return true;
//	else
//	{
//		logSimu("[TRACE] Starting quadratic step\n");
//
//		// QP step for current constraints
//
//		bool improved = false;
//		VectorXd vpl = this->m_vp;
//		VectorXd vll = this->m_vl;
//
//		VectorXd vq; // QP
//		this->getStep(vq);
//
//		VectorXd vd; // Get parameters step
//		getSubvector(0, this->m_np, vq, vd);
//
//		VectorXd ve; // Get Lagrange multipliers step
//		getSubvector(this->m_np, this->m_nc, vq, ve);
//
//		this->fixConstraintVector(ve); // Fix zeros
//
//		Real Ll = this->computeMeritError();
//		Real L = 0.0;
//
//		VectorXd vdx;
//		VectorXd vdX;
//		VectorXd vde;
//		getSubvector(0, this->m_nx, vq, vdx);
//		getSubvector(this->m_nx, this->m_np - this->m_nx, vq, vdX);
//		getSubvector(this->m_np, this->m_nx, vq, vde);
//
//		logSimu("[TRACE] Material-length: %.9f\n", vdx.norm());
//		logSimu("[TRACE] Rest-length: %.9f\n", vdX.norm());
//		logSimu("[TRACE] Lambda-length: %.9f\n", vde.norm());
//
//		logSimu("[TRACE] Merit function: %.9f\n", Ll);
//		logSimu("[TRACE] Parameters step lenght: %.9f\n", vd.norm());
//		logSimu("[TRACE] Multipliers step lenght: %.9f\n", ve.norm());
//
//		int i = 0;
//		Real beta = 1.0;
//		for (i = 0; i < 20; ++i)
//		{
//			this->m_vp += beta*vd;
//			this->m_vl += beta*ve;
//
//			this->pushParameters();
//			this->updateConstraints();
//			this->updateGradient();
//
//			L = this->computeMeritError();
//
//			if (L < Ll)
//			{
//				improved = true;
//				break; // Return
//			}
//
//			beta *= this->m_alpha;
//			this->m_vp = vpl;
//			this->m_vl = vll;
//		}
//
//		if (!improved)
//		{
//			this->pushParameters();
//			this->updateConstraints();
//			this->updateGradient();
//
//			logSimu("[TRACE] Halted after %d step\n", i);
//
//			exit(-1);
//		}
//		else
//		{
//			//this->m_mu = this->m_mu*1.1;
//			this->m_smooth = this->m_smooth*this->m_smoothFactor;
//			logSimu("[TRACE] Improved after %d step\n", i);
//		}
//
//		Real Cnorm = this->m_vC.norm();
//		Real Gnorm = this->m_vG.norm();
//
//		logSimu("[TRACE] LS Merit function: %.9f\n", L);
//		logSimu("[TRACE] Constraint norm: %.9f\n", Cnorm);
//		logSimu("[TRACE] Gradient norm: %.9f\n", Gnorm);
//
//		logSimu("[TRACE] Regularization: %.9f\n", this->m_smooth);
//	}
//
//	logSimu("-------------------------------------------------------\n");
//
//	return true;
//}
//
//void SQPOptimizerEQ::getStep(VectorXd& vq)
//{
//	updateHessian();
//
//	// Update theta parameter for acceptance condition
//	this->m_theta = 1e-8*this->m_mWs.cwiseAbs().sum();
//
//	// N. parameters + N. constraints
//	int N = this->m_np + this->m_nc;
//
//	tVector vA;
//	SparseMatrixXd mAs(N, N);
//	VectorXd vb(N);
//
//	// Matrix assembly
//
//	vA.insert(vA.end(), this->m_vWs.begin(), this->m_vWs.end());
//
//	for (int i = 0; i < (int) this->m_vAs.size(); ++i)
//	{
//		const Triplet<Real>& t = this->m_vAs[i];
//		vA.push_back(Triplet<Real>(this->m_np + t.row(), t.col(), t.value())); // Assemble A
//		vA.push_back(Triplet<Real>(t.col(), this->m_np + t.row(), t.value()));	// Assemble A^T
//	}
//
//	mAs.setFromTriplets(vA.begin(), vA.end());
//	mAs.makeCompressed(); // Compress structure
//
//	// Right-hand side
//
//	for (int i = 0; i < this->m_np; ++i)
//		vb(i) = -this->m_vG(i); // DLDp
//
//	for (int i = 0; i < this->m_nc; ++i)
//		vb(this->m_np + i) = -this->m_vC(i);
//
//	writeToFile(this->m_mWs, "QPHessianMatrix.csv");
//	writeToFile(this->m_mAs, "QPConstraintsJ.csv");
//	writeToFile(mAs, "QPSystemMatrix.csv");
//	writeToFile(vb, "QPSystemRHS.csv");
//
//	// Solve s.t. regularization
//	for (int i = 0; i < 50; ++i)
//	{
//		if (i != 0)
//		{
//			logSimu("[ERROR] In %s: Regularizing system, iteration %d\n", __FUNCTION__, i);
//
//			// Regularize diagonal
//			for (int ii = 0; ii < this->m_np; ++ii)
//				mAs.coeffRef(ii, ii) += 0.0001*pow(10.0, i);
//		}
//
//		mAs.makeCompressed();
//
//		SparseLU<SparseMatrixXd> solver;
//
//		solver.analyzePattern(mAs);
//		solver.factorize(mAs);
//		vq = solver.solve(vb);
//		if (solver.info() != Success)
//			continue; // Iterate again
//
//		///////////////////////// TEST ///////////////////////
//		//VectorXd vbTest = mAs.selfadjointView<Lower>()*vq;
//		//double error = (vb - vbTest).squaredNorm();
//		//if (error > 1e-9) // Iterate again
//		//	continue;
//		///////////////////////// TEST ///////////////////////
//
//		VectorXd vd; // Get parameters step
//		getSubvector(0, this->m_np, vq, vd);
//
//		Real CnormTest = this->m_vC.norm();
//		Real GnormTest = this->m_vG.norm();
//		Real objectiveTest = this->computeObjective();
//		Real modelOBJReductionTest = this->computeModelReduction_OBJ(vd);
//		Real modelCONReductionTest = this->computeModelReduction_CON(vd);
//		//assert(isApprox(modelCONReductionTest, CnormTest, 1e-9));
//
//		Real dWd05 = 0.5*vd.transpose()*this->m_mWs*vd;
//		Real tBound = this->computeTangentialBound(vd);
//		Real nBound = this->computeOrthogonalBound(vd);
//		Real Cnorm = this->m_vC.norm();
//
//		Real modelOBJBound = max(dWd05, this->m_theta*tBound);
//		Real modelCONBound = this->m_sigma*this->m_mu*Cnorm;
//		Real modelReduction = this->computeModelReduction(vd);
//
//		//Real muTrial = (this->m_vg.dot(vd) + modelOBJBound) / ((1.0 - this->m_tau)*Cnorm);
//		////if (this->m_mu < muTrial)
//		////{
//		////	this->m_mu = muTrial + 1e-4;
//		////}
//		//break;
//
//		// Termination Test 1
//
//		if (modelReduction >= modelOBJBound + modelCONBound)
//		{
//			logSimu("[TRACE] T1 Passed. Step accepted. Current mu: %f\n", this->m_mu);
//			break;
//		}
//
//		// Termination Test 2
//
//		if (dWd05 >= this->m_theta*tBound || this->m_psi*nBound >= tBound)
//		{
//			logSimu("[TRACE] T2 Passed. Step accepted. Checking mu...\n");
//
//			Real muTrial = (this->m_vg.dot(vd) + modelOBJBound) / ((1.0 - this->m_tau)*Cnorm);
//
//			if (this->m_mu < muTrial)
//			{
//				this->m_mu = muTrial + 1e-4;
//
//				logSimu("[TRACE] Mu updated: %f\n", this->m_mu);
//
//				// DEBUG
//
//				modelReduction = this->computeModelReduction(vd);
//				modelOBJBound = max(dWd05, this->m_theta*tBound);
//				modelCONBound = this->m_sigma*this->m_mu*Cnorm;
//
//				//assert(modelReduction >= modelOBJBound + modelCONBound);
//			}
//			else
//			{
//				logSimu("[TRACE] Mu keeped: %f\n", this->m_mu);
//			}
//
//			break;
//		}
//	}
//}
//
//Real SQPOptimizerEQ::computeObjective() const
//{
//	//VectorXd vxD = this->m_pModel->getPositions() - this->m_vxT;
//	//return 0.5 * vxD.dot(vxD); // Position error deformed|target
//
//	Real error = 0.0;
//	int numObjs = (int) this->m_vobjSet.size();
//	for (int i = 0; i < numObjs; ++i) // Add error
//		error += this->m_vobjSet[i]->getError();
//
//	// Add simple smoothness regularization
//	error += this->m_smooth*this->m_vp.norm();
//
//	return error;
//}
//
//Real SQPOptimizerEQ::computeLagrangian() const
//{
//	return  this->computeObjective() + this->m_vl.dot(this->m_vC) ;
//}
//
//Real SQPOptimizerEQ::computeMeritError() const
//{
//	return this->computeObjective() + this->m_mu*this->m_vC.norm();
//}
//
//Real SQPOptimizerEQ::computeModelReduction_OBJ(const VectorXd& vd) const
//{
//	return -this->m_vg.dot(vd);
//}
//
//Real SQPOptimizerEQ::computeModelReduction_CON(const VectorXd& vd) const
//{
//	return (this->m_vC.norm() - (this->m_vC + this->m_mAs*vd).norm());
//}
//
//Real SQPOptimizerEQ::computeModelReduction(const VectorXd& vd) const
//{
//	return computeModelReduction_OBJ(vd) + this->m_mu*computeModelReduction_CON(vd);
//}
//
//Real SQPOptimizerEQ::computeTangentialBound(const VectorXd& vd) const
//{
//	return vd.squaredNorm() - this->computeOrthogonalBound(vd);
//}
//
//Real SQPOptimizerEQ::computeOrthogonalBound(const VectorXd& vd) const
//{
//	return (this->m_mAs*vd).squaredNorm()/this->m_mAs.squaredNorm();;
//}
//
//void SQPOptimizerEQ::restartConstraints()
//{
//	this->m_vC.resize(this->m_nc);
//	this->m_vC.setZero(); // Init.
//
//	this->m_mAs.resize(this->m_nc, this->m_np);
//	this->m_mAs.setZero(); // Empty Jacobian
//
//	this->m_vAs.clear();
//
//	logSimu("[TRACE] Constraints value restarted. Norm: %.9f\n", this->m_vC.norm());
//	logSimu("[TRACE] Constraints Jacobian restarted. Norm: %.9f\n", this->m_mAs.norm());
//}
//
//void SQPOptimizerEQ::updateConstraints()
//{
//	// Constraint vector
//
//	VectorXd vx = this->m_pModel->getPositions_x();
//	VectorXd vv = this->m_pModel->getVelocities_x();
//
//	this->m_vC.resize(this->m_nc);
//	this->m_vC.setZero(); // Init.
//
//	this->m_pModel->addForce(this->m_vC, NULL); // Internal
//	this->m_pSolver->addBoundaryForce(vx, vv, this->m_vC);
//
//	this->fixConstraintVector(this->m_vC); // Set fixed
//
//	// Constraint Jacobian
//
//	this->updateJacobianA();
//	//this->updateJacobianFD();
//
//	//logSimu("[TRACE] Constraints value updated. Norm: %.9f\n", this->m_vC.norm());
//}
//
//void SQPOptimizerEQ::updateJacobianA()
//{
//	this->m_vAs.clear();
//
//	// Derivative w.r.t. parameters
//
//	for (int i = 0; i < this->m_nparamSet; ++i)
//	{
//		tVector vDFxDpi;
//		this->m_vparamSet[i]->get_DfDp(vDFxDpi);
//		for (int j = 0; j < (int)vDFxDpi.size(); ++j)
//			this->m_vAs.push_back(Triplet<Real>(vDFxDpi[j].row(), vDFxDpi[j].col() + this->m_vparamOff[i], vDFxDpi[j].value()));
//	}
//
//	this->fixConstraintJacobian(this->m_vAs); // Set fixed points
//	this->m_mAs.setFromTriplets(this->m_vAs.begin(), this->m_vAs.end());
//	this->m_mAs.makeCompressed(); // Compress structure (non-symmetric)
//
//	//logSimu("[TRACE] Constraints Jacobian updated. Norm: %.9f\n", this->m_mAs.norm());
//}
//
//void SQPOptimizerEQ::updateJacobianFD()
//{
//	double EPS = 1e-6;
//	MatrixXd mAFD(this->m_nx, this->m_np);
//	mAFD.setZero();
//	for (int j = 0; j < this->m_np; ++j)
//	{
//		// +
//		this->m_vp(j) += EPS;
//		VectorXd vfp(this->m_nx);
//		this->pushParameters();
//		vfp.setZero();
//		this->m_pModel->addForce(vfp);
//
//		// -
//		this->m_vp(j) -= 2 * EPS;
//		VectorXd vfm(this->m_nx);
//		this->pushParameters();
//		vfm.setZero();
//		this->m_pModel->addForce(vfm);
//
//		// Estimate
//		VectorXd vfFD = (vfp - vfm) / (2 * EPS);
//		for (int i = 0; i < this->m_nx; ++i)
//			mAFD(i, j) = vfFD(i);
//
//		this->m_vp(j) += EPS;
//	}
//
//	this->m_vAs.clear();
//	for (int i = 0; i < this->m_nx; ++i)
//		for (int j = 0; j < this->m_np; ++j) // Get all entries
//			this->m_vAs.push_back(Triplet<Real>(i, j, mAFD(i, j)));
//
//	this->fixConstraintJacobian(this->m_vAs);
//	this->m_mAs.setFromTriplets(this->m_vAs.begin(), this->m_vAs.end());
//	this->m_mAs.makeCompressed(); // Compress sparse structure (symmetric)
//
//	// Recover original value
//	this->pushParameters();
//}
//
//void SQPOptimizerEQ::testJacobian()
//{
//	this->updateJacobianFD(); // Copy dense
//	MatrixXd mAFD = this->m_mAs.toDense();
//
//	this->updateJacobianA(); // Copy dense
//	MatrixXd mAA = this->m_mAs.toDense();
//
//	MatrixXd mADiff = mAA - mAFD;
//	Real mADiffNorm = mADiff.norm();
//	Real mAFDNorm = mAFD.norm();
//	if (mAFDNorm > 1e-9)
//	{
//		Real relativeError = mADiffNorm / mAFDNorm;
//		if (relativeError >= 1e-9) // Significative
//		{
//			writeToFile(mAA, "ConstraintJacobianA.csv");
//			writeToFile(mAFD, "ConstraintJacobianFD.csv");
//			writeToFile(mADiff, "ConstraintJacobianDiff.csv");
//
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//			logSimu("[ERROR] Jacobian test. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", mAFDNorm, mADiffNorm, relativeError);
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//		}
//	}
//}
//
//void SQPOptimizerEQ::restartGradient()
//{
//	// Objective gradient
//	this->m_vg.resize(this->m_np);
//	this->m_vg.setZero(); // Init
//
//	// Lagrangian gradient
//	this->m_vG.resize(this->m_np);
//	this->m_vG.setZero(); // Init.
//
//	logSimu("[TRACE] Objective gradient restarted. Norm: %.9f\n", this->m_vg.norm());
//	logSimu("[TRACE] Lagrangian gradient restarted. Norm: %.9f\n", this->m_vG.norm());
//}
//
//void SQPOptimizerEQ::updateGradient()
//{
//	updateGradientA();
//	//updateGradientFD();
//}
//
//void SQPOptimizerEQ::updateHessian()
//{
//	//updateHessianA();
//	updateHessianFD();
//	//updateHessianBFGS();
//	//restartHessian();
//}
//
//void SQPOptimizerEQ::updateGradientA()
//{
//	this->m_vg.setZero();
//	this->m_vG.setZero();
//
//	VectorXd vg(this->m_nx);
//	vg.setZero(); // Init.
//
//	int numObjs = (int) this->m_vobjSet.size();
//	for (int i = 0; i < numObjs; ++i) // Add
//		this->m_vobjSet[i]->add_DEDx(vg);
//
//	setSubvector(0, this->m_nx, vg, this->m_vg);
//	setSubvector(0, this->m_nx, vg, this->m_vG);
//
//	// Add regularizer to both objective and Lagrangian gradients
//	this->m_vg += this->m_smooth*(this->m_vp/this->m_vp.norm());
//	this->m_vG += this->m_smooth*(this->m_vp/this->m_vp.norm());
//
//	this->m_vG += this->m_vl.transpose()*this->m_mAs;
//
//	// Add smoothness regularization gradient
//
//	this->fixLagrangianVector(this->m_vg);
//	this->fixLagrangianVector(this->m_vG);
//
//	//logSimu("[TRACE] Objective gradient updated (A). Norm: %.9f\n", this->m_vg.norm());
//	//logSimu("[TRACE] Lagrangian gradient updated (A). Norm: %.9f\n", this->m_vG.norm());
//}
//
//void SQPOptimizerEQ::updateGradientFD()
//{
//	Real EPS = 1e-6;
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lp = this->computeLagrangian();
//		Real fp = this->computeObjective();
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		Real Lm = this->computeLagrangian();
//		Real fm = this->computeObjective();
//
//		// Estimate
//		this->m_vg[i] = (fp - fm) / (2 * EPS);
//		this->m_vG[i] = (Lp - Lm) / (2 * EPS);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//
//	this->fixLagrangianVector(this->m_vg);
//	this->fixLagrangianVector(this->m_vG);
//
//	//logSimu("[TRACE] Objective gradient updated (FD). Norm: %.9f\n", this->m_vg.norm());
//	//logSimu("[TRACE] Lagrangian gradient updated (FD). Norm: %.9f\n", this->m_vG.norm());
//}
//
//void SQPOptimizerEQ::restartHessian()
//{
//	this->m_mW.resize(this->m_np, this->m_np);
//	this->m_mW.setIdentity(); // Init. from I
//
//	m_modifier1.resize(this->m_np, this->m_np);
//	m_modifier2.resize(this->m_np, this->m_np);
//	m_tempMatrix.resize(this->m_np, this->m_np);
//
//	m_sk.resize(this->m_np);
//	m_yk.resize(this->m_np);
//	m_vp_P = this->m_vp;
//	m_vg_P = this->m_vG;
//
//	this->m_mWs.resize(this->m_np, this->m_np);
//	this->m_mWs.setIdentity(); // Empty Hessian
//	
//	for (int i = 0; i < this->m_np; ++i) // Add diagonal
//		this->m_vWs.push_back(Triplet<Real>(i, i, 1.0));
//
//	logSimu("[TRACE] Lagrangian Hessian restarted. Identity\n");
//}
//
//void SQPOptimizerEQ::updateHessianA()
//{
//	// TODO: Reserve space here
//
//	this->m_vWs.clear();
//
//	// D2{f(p)}/Dp2 = I
//
//	int numObjs = (int) this->m_vobjSet.size();
//	for (int i = 0; i < numObjs; ++i) // Add Hessian
//		this->m_vobjSet[i]->add_D2EDx2(this->m_vWs);
//
//	// TODO: Implement second derivatives of regularization
//
//	SparseMatrixXd mWtest = this->m_mWs;
//	mWtest.setFromTriplets(this->m_vWs.begin(), this->m_vWs.end());
//	mWtest.makeCompressed();
//
//	// D2{L^T*C(p)}/Dp2 = Sum_i [ L_i * D2{F_i(p)}/Dp2 ]
//
//	// TODO: Implemente second derivative of constraints
//
//	this->fixLagrangianHessian(this->m_vWs);
//	this->m_mWs.setFromTriplets(this->m_vWs.begin(), this->m_vWs.end());
//	this->m_mWs.makeCompressed(); // Compress sparse structure (symmetric)
//
//	logSimu("[TRACE] Lagrangian Hessian updated (A)\n");
//}
//
//void SQPOptimizerEQ::updateHessianFD()
//{
//	Real EPS = 1e-6;
//
//	for (int i = 0; i < this->m_np; ++i)
//	{
//		// Plus
//		this->m_vp[i] += EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		this->updateGradient();
//
//		VectorXd vgp = this->m_vG;
//
//		// Minus
//		this->m_vp[i] -= 2 * EPS;
//		this->pushParameters();
//		this->updateConstraints();
//		this->updateGradient();
//
//		VectorXd vgm = this->m_vG;
//
//		// Estimate
//		VectorXd Wcol = (vgp - vgm) / (2 * EPS);
//		for (int j = 0; j < this->m_np; ++j)
//			this->m_mW(j, i) = Wcol(j);
//
//		// Recover
//		this->m_vp[i] += EPS;
//	}
//
//	this->pushParameters();
//	this->updateConstraints();
//	this->updateGradient();
//
//	// TODO: Reserve space here
//
//	this->m_vWs.clear();
//	for (int i = 0; i < this->m_np; ++i)
//		for (int j = 0; j < this->m_np; ++j) // Get entries (all entries)
//			this->m_vWs.push_back(Triplet<Real>(i, j, this->m_mW(i, j)));
//
//	this->fixLagrangianHessian(this->m_vWs);
//	this->m_mWs.setFromTriplets(this->m_vWs.begin(), this->m_vWs.end());
//	this->m_mWs.makeCompressed(); // Compress sparse structure (symmetric)
//
//	logSimu("[TRACE] Lagrangian Hessian updated (FD)\n");
//}
//
//void SQPOptimizerEQ::updateHessianBFGS()
//{
//	m_sk = this->m_vp - this->m_vp_P;
//	m_yk = this->m_vG - this->m_vg_P;
//
//	this->m_vp_P = this->m_vp;
//	this->m_vg_P = this->m_vG;
//
//	double D = m_yk.dot(m_sk);
//
//	if (fabs(D) > 1e-6) // ?
//	{
//		m_rho = 1.0 / D; // For too small discriminant, the update is invalid
//
//		// Update the Hessian matrix (direct one) (Numerical Optimization, Nocedal, Wright, page 25)
//		// Hk+1 = (I - rho * m_sk * m_yk^T) * Hk * (I - rho * m_yk * m_sk^T) + rho * m_yk * m_yk^T
//		for (int i = 0; i < this->m_np; i++)
//		{
//			for (int j = 0; j <= i; j++)
//			{
//				if (i == j)
//				{
//					m_modifier1(i, i) = 1.0 - m_rho * m_yk[i] * m_sk[i];
//				}
//				else
//				{
//					m_modifier1(i, j) = -m_rho * m_sk[i] * m_yk[j];
//					m_modifier1(j, i) = -m_rho * m_sk[j] * m_yk[i];
//				}
//
//				m_modifier2(i, j) = m_rho * m_yk[i] * m_yk[j];
//				m_modifier2(j, i) = m_rho * m_yk[j] * m_yk[i];
//			}
//		}
//
//		this->m_mW = m_modifier1*this->m_mW*m_modifier1.transpose();
//		this->m_mW = this->m_mW + m_modifier2; // Add Hessian modifier
//
//		// TODO: Reserve space here
//
//		this->m_vWs.clear();
//		for (int i = 0; i < this->m_np; ++i)
//			for (int j = 0; j < this->m_np; ++j) // Get entries (all entries)
//				this->m_vWs.push_back(Triplet<Real>(i, j, this->m_mW(i, j)));
//
//		this->fixLagrangianHessian(this->m_vWs);
//		this->m_mWs.setFromTriplets(this->m_vWs.begin(), this->m_vWs.end());
//		this->m_mWs.makeCompressed(); // Compress sparse structure (symmetric)
//
//		logSimu("[TRACE] Lagrangian Hessian updated (BFGS)\n");
//	}
//	else
//	{
//		// Initialize to FD Hess
//		//this->updateHessianFD();
//		this->restartHessian();
//	}
//}
//
//void SQPOptimizerEQ::testGradient()
//{
//	this->updateGradientFD();
//	VectorXd vgFD = this->m_vG;
//
//	this->updateGradientA();
//	VectorXd vgA = this->m_vG;
//
//	VectorXd vgDiff = vgA - vgFD;
//	Real vgDiffNorm = vgDiff.norm();
//	Real vgFDNorm = vgFD.norm();
//	if (vgFDNorm > 1e-9)
//	{
//		Real relativeError = vgDiffNorm / vgFDNorm;
//		if (relativeError > 1e-9) // Significative
//		{
//			writeToFile(vgA, "LagrangianGradientA.csv");
//			writeToFile(vgFD, "LagrangianGradientFD.csv");
//			writeToFile(vgDiff, "LagrangianGradientDiff.csv");
//
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//			logSimu("[ERROR] Gradient test. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", vgFDNorm, vgDiffNorm, relativeError);
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//		}
//	}
//}
//
//void SQPOptimizerEQ::testHessian()
//{
//	this->updateHessianFD(); // Copy dense
//	MatrixXd mHFD = this->m_mWs.toDense();
//
//	this->updateHessianA(); // Copy dense
//	MatrixXd mHA = this->m_mWs.toDense();
//
//	MatrixXd mHDiff = mHA - mHFD;
//	Real mHDiffNorm = mHDiff.norm();
//	Real mHFDNorm = mHFD.norm();
//	if (mHFDNorm > 1e-9)
//	{
//		Real relativeError = mHDiffNorm / mHFDNorm;
//		if (relativeError >= 1e-9) // Significative
//		{
//			writeToFile(mHA, "LagrangianHessianA.csv");
//			writeToFile(mHFD, "LagrangianHessianFD.csv");
//			writeToFile(mHDiff, "LagrangianHessianDiff.csv");
//
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//			logSimu("[ERROR] Hessian test. Norm FD: %.9f, Norm Diff.: %.9f, Rel. Error: %.9f\n", mHFDNorm, mHDiffNorm, relativeError);
//			logSimu("|||||||||||||||||||||||||||||||||| WARNING ||||||||||||||||||||||||||||||||||\n");
//		}
//	}
//}
//
//void SQPOptimizerEQ::pullParameters()
//{
//	this->m_vp.resize(this->m_np);
//	this->m_vp.setZero(); // Init.
//
//	for (int i = 0; i < this->m_nparamSet; ++i)
//	{
//		VectorXd vpi;
//		this->m_vparamSet[i]->getParameters(vpi);
//		int npi = this->m_vparamSet[i]->getNumParameter();
//		setSubvector(this->m_vparamOff[i], npi, vpi, this->m_vp);
//	}
//}
//
//void SQPOptimizerEQ::pushParameters()
//{
//	for (int i = 0; i < this->m_nparamSet; ++i)
//	{
//		VectorXd vpi;
//		int npi = this->m_vparamSet[i]->getNumParameter();
//		getSubvector(this->m_vparamOff[i], npi, this->m_vp, vpi);
//		this->m_vparamSet[i]->setParameters(vpi);
//	}
//}
//
//void SQPOptimizerEQ::fixLagrangianVector(VectorXd& vL) const
//{ 
//	// Fixed DOF specified in configuration space: transform
//	const iVector& vsimDOF = this->m_pModel->getRawToSim_x();
//
//	for (int i = 0; i < this->m_nparamSet; ++i)
//	{
//		const iVector& vidxFix = this->m_vparamSet[i]->getFixedParameters();
//
//		// Zero value on fixed indices
//		int nfix = (int)vidxFix.size();
//		for (int j = 0; j < nfix; ++j)
//		{
//			if (i == 0)
//			{
//				if (vsimDOF[vidxFix[j]] == -1)
//					continue; // Not simulated
//
//				// Simulation parameters, raw
//				vL(vsimDOF[vidxFix[j]]) = 0.0;
//			}
//			else
//			{
//				// Consider parameters offset in the vector
//				vL(vidxFix[j] + this->m_vparamOff[i]) = 0.0;
//			}
//		}
//	}
//}
//
//void SQPOptimizerEQ::fixConstraintVector(VectorXd& vC) const
//{
//	// The first parameter set is assumed to be the material coordinates
//	const iVector& vidxFix = this->m_vparamSet[0]->getFixedParameters();
//
//	// Fixed DOF specified in configuration space: transform
//	const iVector& vsimDOF = this->m_pModel->getRawToSim_x();
//
//	// Zero constraint on fixed index
//	int nfix = (int)vidxFix.size();
//	for (int i = 0; i < nfix; ++i)
//	{
//		if (vsimDOF[vidxFix[i]] == -1)
//			continue; // Not simulated
//
//		vC(vsimDOF[vidxFix[i]]) = 0.0;
//	}
//}
//
//void SQPOptimizerEQ::fixLagrangianHessian(tVector& vW) const
//{
//	// Fixed DOF specified in configuration space: transform
//	const iVector& vsimDOF = this->m_pModel->getRawToSim_x();
//
//	// Create the fixed stencil to filter
//	bVector vfixedStencil(this->m_np, false);
//
//	iVector vidxFix;
//
//	for (int i = 0; i < this->m_nparamSet; ++i)
//	{
//		const iVector& vidxFixi = this->m_vparamSet[i]->getFixedParameters();
//
//		int offset = this->m_vparamOff[i];
//		int nfix = (int)vidxFixi.size();
//		for (int j = 0; j < nfix; ++j)
//		{
//			if (i == 0) 
//			{
//				if (vsimDOF[vidxFixi[j]] == -1)
//					continue; // Not simulated
//
//				// Simulation parameters, configuration
//				vidxFix.push_back(vsimDOF[vidxFixi[j]]);
//			}
//			else
//			{
//				// Consider parameters offset in vector
//				vidxFix.push_back(vidxFixi[j] + offset);
//			}
//
//			vfixedStencil[vidxFix.back()] = true; 
//		}
//	}
//
//	// Traverse entries and make zero those in the row or 
//	// the column of a fixed parameter. Then, add ones to
//	// the diagonal where is needed to guarantee the non
//	// singularity.
//
//	int nEntries = (int)vW.size();
//	for (int i = 0; i < nEntries; ++i)
//	{
//		Triplet<Real>& t = vW[i];
//		int r = t.row();
//		int c = t.col();
//		if (vfixedStencil[r] || vfixedStencil[c])
//			vW[i] = Triplet<Real>(r, c, 0.0);
//	}
//
//	
//	int nfix = (int) vidxFix.size();
//	for (int i = 0; i < nfix; ++i) // Add ones to the diagonal
//		vW.push_back(Triplet<Real>(vidxFix[i], vidxFix[i], 1.0));
//}
//
//void SQPOptimizerEQ::fixConstraintJacobian(tVector& vA) const
//{
//	// Fixed DOF specified in configuration space: transform
//	const iVector& vsimDOF = this->m_pModel->getRawToSim_x();
//
//	// Create the fixed stencil to filter
//	bVector vfixedStencil(this->m_np, false);
//
//	iVector vidxFix;
//
//	for (int i = 0; i < this->m_nparamSet; ++i)
//	{
//		const iVector& vidxFixi = this->m_vparamSet[i]->getFixedParameters();
//
//		int offset = this->m_vparamOff[i];
//		int nfix = (int)vidxFixi.size();
//		for (int j = 0; j < nfix; ++j)
//		{
//			if (i == 0)
//			{
//				if (vsimDOF[vidxFixi[j]] == -1)
//					continue; // Not simulated
//
//				// Simulation parameters, configuration
//				vidxFix.push_back(vsimDOF[vidxFixi[j]]);
//			}
//			else
//			{
//				// Consider parameters offset in vector
//				vidxFix.push_back(vidxFixi[j] + offset);
//			}
//
//			vfixedStencil[vidxFix.back()] = true;
//		}
//	}
//
//	// Traverse entries and make zero those in the row or 
//	// the column of a fixed parameter. Then, add ones to
//	// the diagonal where is needed to guarantee
//	// non-singularity.
//
//	int nEntries = (int)vA.size();
//	for (int i = 0; i < nEntries; ++i)
//	{
//		Triplet<Real>& t = vA[i];
//		int r = t.row();
//		int c = t.col();
//		if (vfixedStencil[r] || vfixedStencil[c])
//			vA[i] = Triplet<Real>(r, c, 0.0);
//	}
//
//	// The first parameter set is assumed to be the material coordinates
//	const iVector& vidxFixx = this->m_vparamSet[0]->getFixedParameters();
//
//	// Add ones to the diagonal
//	int nfix = (int)vidxFixx.size();
//	for (int i = 0; i < nfix; ++i) 
//	{
//		if (vsimDOF[vidxFixx[i]] == -1)
//			continue; // Not simulated
//
//		vA.push_back(Triplet<Real>(vsimDOF[vidxFixx[i]], vsimDOF[vidxFixx[i]], 1.0));
//	}
//}
//
//void SQPOptimizerEQ::testThirdOrderTensor() const
//{
//	// TODO
//
//	//// TODO: Reserve space here
//	//vector<tVector> vD2fxDxx_t(this->m_nx);
//	//vector<tVector> vD2fxDxX_t(this->m_nX);
//	//vector<tVector> vD2fxDXX_t(this->m_nX);
//	////vector<MatrixXd> vD2fxDxx_m(this->m_nx);
//	////vector<MatrixXd> vD2fxDxX_m(this->m_nX);
//	////vector<MatrixXd> vD2fxDXX_m(this->m_nX);
//	//this->m_pModel->add_D2fxDxx(vD2fxDxx_t);
//	//this->m_pModel->add_D2fxDx0(vD2fxDxX_t);
//	//this->m_pModel->add_D2fxD00(vD2fxDXX_t);
//	//for (int i = 0; i < this->m_nx; ++i)
//	//{
//	//	ostringstream name; // Output name
//	//	name << "D2fxDxx_" << i << ".csv";
//
//	//	SparseMatrixXd test(this->m_nx, this->m_nx);
//	//	test.setFromTriplets(vD2fxDxx_t[i].begin(), vD2fxDxx_t[i].end());
//	//	test.makeCompressed();
//
//	//	writeToFile(test, name.str().c_str());
//	//}
//	//for (int i = 0; i < this->m_nX; ++i)
//	//{
//	//	ostringstream name; // Output name
//	//	name << "D2fxDx0_" << i << ".csv";
//
//	//	SparseMatrixXd test(this->m_nx, this->m_nx);
//	//	test.setFromTriplets(vD2fxDxX_t[i].begin(), vD2fxDxX_t[i].end());
//	//	test.makeCompressed();
//
//	//	writeToFile(test, name.str().c_str());
//	//}
//	//for (int i = 0; i < this->m_nX; ++i)
//	//{
//	//	ostringstream name; // Output name
//	//	name << "D2fxD00_" << i << ".csv";
//
//	//	SparseMatrixXd test(this->m_nx, this->m_nX);
//	//	test.setFromTriplets(vD2fxDXX_t[i].begin(), vD2fxDXX_t[i].end());
//	//	test.makeCompressed();
//
//	//	writeToFile(test, name.str().c_str());
//	//}
//}