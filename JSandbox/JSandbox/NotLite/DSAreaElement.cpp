/*=====================================================================================*/
/*!
\file		DSAreaElement.cpp
\author		jesusprod
\brief		Implementation of DSAreaElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/DSAreaElement.h>

DSAreaElement::DSAreaElement(int dim0) : NodalSolidElement(3, 3, dim0)
{
	assert(dim0 == 2 || dim0 == 3);

	// This carries mass
	this->m_addMass = true;
}

DSAreaElement::~DSAreaElement()
{
	// Nothing to do here
}

void DSAreaElement::update_Rest(const VectorXd& vX)
{
	if (this->m_numDim_0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
		Vector3d x2 = getBlock3x1(this->m_vnodeIdx0[2], vX);
		this->m_A0 = 0.5*(x2 - x0).cross(x1 - x0).norm();
		assert(this->m_A0 >= 0.0);
	}
	else if (this->m_numDim_0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
		Vector2d x2 = getBlock2x1(this->m_vnodeIdx0[2], vX);
		Vector3d x03D(x0.x(), 0.0, x0.y());
		Vector3d x13D(x1.x(), 0.0, x1.y());
		Vector3d x23D(x2.x(), 0.0, x2.y());
		this->m_A0 = 0.5*(x23D - x03D).cross(x13D - x03D).norm();
		assert(this->m_A0 >= 0.0);
	}
	else assert(false);
}

void DSAreaElement::update_Mass(const VectorXd& vX)
{
	if (!this->m_addMass)
		return; // Ignore

	double A0 = -1;

	if (this->m_numDim_0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdx0[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdx0[1], vX);
		Vector3d x2 = getBlock3x1(this->m_vnodeIdx0[2], vX);
		A0 = 0.5*(x2 - x0).cross(x1 - x0).norm();
		assert(A0 >= 0.0);
	}
	else if (this->m_numDim_0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdx0[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdx0[1], vX);
		Vector2d x2 = getBlock2x1(this->m_vnodeIdx0[2], vX);
		Vector3d x03D(x0.x(), 0.0, x0.y());
		Vector3d x13D(x1.x(), 0.0, x1.y());
		Vector3d x23D(x2.x(), 0.0, x2.y());
		A0 = 0.5*(x23D - x03D).cross(x13D - x03D).norm();
		assert(A0 >= 0.0);
	}
	else assert(false);

	Real nodeMass = (1.0/3.0)*A0*this->m_vpmat[0]->getDensity();
	for (int i = 0; i < this->m_numDOFx; ++i)
		this->m_vmx[i] = nodeMass;
}

void DSAreaElement::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}
	double A0 = this->m_A0*this->m_preStrain;
	double kA = m_vpmat[0]->getAreaK();

#include "../../Maple/Code/DSAreaEnergy.mcg"

	this->m_energy = t137;
}

void DSAreaElement::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}
	double A0 = this->m_A0*this->m_preStrain;
	double kA = m_vpmat[0]->getAreaK();

	double vfx[9];

#include "../../Maple/Code/DSAreaForce.mcg"

	for (int i = 0; i < 9; ++i)
		if (!isfinite(vfx[i]))
			logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 9; ++i)
		this->m_vfVal(i) = vfx[i];
}

void DSAreaElement::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}
	double A0 = this->m_A0*this->m_preStrain;
	double kA = m_vpmat[0]->getAreaK();
	double mJx[9][9];

#include "../../Maple/Code/DSAreaJacobian.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}