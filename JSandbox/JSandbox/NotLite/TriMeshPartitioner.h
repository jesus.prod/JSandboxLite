/*=====================================================================================*/
/*!
\file		TriMeshPartitioner.h
\author		jesusprod
\brief		TriMesh partion maker (to parametrize).
*/
/*=====================================================================================*/

#ifndef TRIMESH_PARTITION_MAKER_H
#define TRIMESH_PARTITION_MAKER_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Geometry/RodMesh.h>
#include <JSandbox/Geometry/TriMesh.h>
#include <JSandbox/Geometry/SurfaceRemesherCutter.h>
#include <JSandbox/Geometry/SurfaceRemesherContour.h>

typedef struct PartitionOP
{
	bool m_isOK;
	TriMesh* m_pTriMesh;
	RodMesh* m_pRodMesh;
	vector<iVector> m_vmapSplitToWhole;
	vector<iVector> m_vmapWholeToSplit;
	map<int, int> m_mapBoundaryEdges;
	vector<RemeshOP> m_vremOPs;
} PartitionOP;

class JSANDBOX_EXPORT TriMeshPartitioner
{

public:
	TriMeshPartitioner(void);
	virtual ~TriMeshPartitioner(void);

	virtual Real getEdgeMoveFactor() const { return this->m_edgeMoveFactor; }
	virtual Real getFaceMoveFactor() const { return this->m_faceMoveFactor; }
	virtual Real getIntersecEpsilon() const { return this->m_intersecEpsi; }
	virtual Real getBoundaryEpsilon() const { return this->m_boundaryEpsi; }

	virtual void setEdgeMoveFactor(Real f) { this->m_edgeMoveFactor = f; }
	virtual void setFaceMoveFactor(Real f) { this->m_faceMoveFactor = f; }
	virtual void setIntersecEpsilon(Real e) { this->m_intersecEpsi = e; }
	virtual void setBoundaryEpsilon(Real e) { this->m_boundaryEpsi = e; }

	virtual bool makePartition(const TriMesh& triMesh, const RodMesh& rodMesh);

	virtual const PartitionOP& getResult() const { return this->m_resultOP; }

protected:

	virtual bool prepareRodMesh(const RodMesh& meshIn, RodMesh& meshOut);

	PartitionOP m_resultOP;

	Real m_edgeMoveFactor;
	Real m_faceMoveFactor;
	Real m_intersecEpsi;
	Real m_boundaryEpsi;

};

#endif