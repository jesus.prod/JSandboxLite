/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_EdgeLength.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_EdgeLength.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_EdgeLength.h>

ParameterSet_Tensile_EdgeLength::ParameterSet_Tensile_EdgeLength(int midx) : ParameterSet("TENEDGELENGTH")
{
	assert(midx >= 0);
	this->m_midx = midx;
}

ParameterSet_Tensile_EdgeLength::~ParameterSet_Tensile_EdgeLength()
{
	// Nothing to do here
}

void ParameterSet_Tensile_EdgeLength::getParameters(VectorXd& vp) const
{
	this->m_pTSModel->getRodMeshModel(this->m_midx)->getParam_LengthEdge(vp);

	assert((int) vp.size() == this->m_np);
}

void ParameterSet_Tensile_EdgeLength::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	VectorXd vpB = vp;
	this->limitParameters(vpB);
	this->m_pTSModel->getRodMeshModel(this->m_midx)->setParam_LengthEdge(vpB);
}

bool ParameterSet_Tensile_EdgeLength::isReady_fp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_fle();
}

bool ParameterSet_Tensile_EdgeLength::isReady_DfDp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_DfxDle();
}

void ParameterSet_Tensile_EdgeLength::get_fp(VectorXd& vfp) const
{
	this->m_pTSModel->getRodMeshModel(this->m_midx)->add_fle(vfp);
}

void ParameterSet_Tensile_EdgeLength::get_DfDp(tVector& vDfDp_t) const
{
	tVector vDfDp_i;
	vDfDp_i.reserve(this->m_pTSModel->getRodMeshModel(this->m_midx)->getNumNonZeros_DfxDle());
	vDfDp_t.reserve(this->m_pTSModel->getRodMeshModel(this->m_midx)->getNumNonZeros_DfxDle());
	this->m_pTSModel->getRodMeshModel(this->m_midx)->add_DfxDle(vDfDp_i);
	int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);
	this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_i, vDfDp_t);
}

void ParameterSet_Tensile_EdgeLength::setup()
{
	assert(this->m_pModel != NULL);
	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	this->m_np = (int) this->m_pTSModel->getRodMeshModel(this->m_midx)->getSimMesh_0().getNumEdge();

	this->updateBoundsVector();
}