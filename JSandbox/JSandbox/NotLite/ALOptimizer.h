///*=====================================================================================*/
///*!
//\file		ALOptimizer.h
//\author		jesusprod
//\brief		Basic class for Aumented Lagrangian optimization.
//*/
///*=====================================================================================*/
//
//#ifndef AL_OPTIMIZER_H
//#define AL_OPTIMIZER_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//
//#include <JSandbox/Model/SolidModel.h>
//#include <JSandbox/Solver/PhysSolver.h>
//#include <JSandbox/CustomTimer.h>
//
//class JSANDBOX_EXPORT ALOptimizer
//{
//public:
//	ALOptimizer(const string& ID);
//
//	virtual ~ALOptimizer();
//	
//	virtual void setModel(SolidModel* pModel);
//	virtual void setSolver(PhysSolver* pSolver);
//
//	virtual void setTarget(const VectorXd& vt);
//	virtual void getTarget(VectorXd& vt) const;
//
//	virtual const VectorXd& getGradient() const { return this->m_vg; }
//	virtual const VectorXd& getConstraint() const { return this->m_vC; }
//	virtual const VectorXd& getParameters() const { return this->m_vp;  }
//
//	virtual Real computePositionError() const;
//	virtual Real computeMultiplierError() const;
//	virtual Real computePenaltyError() const;
//	
//	virtual VectorXd computePositionGradient() const;
//	virtual VectorXd computeMultiplierGradient() const;
//	virtual VectorXd computePenaltyGradient() const;
//
//	virtual VectorXd computePositionGradientFD();
//	virtual VectorXd computeMultiplierGradientFD();
//	virtual VectorXd computePenaltyGradientFD();
//
//	virtual void setFixedParameters(const iVector& vidx);
//	virtual const iVector& getFixedParameters() const;
//
//	virtual void setup();
//
//	virtual bool init();
//	virtual bool step();
//
//protected:
//
//	virtual void getStep(VectorXd& vdq);
//
//	virtual Real computeMeritError() const;
//
//	virtual void restartConstraints();
//	virtual void updateConstraints();
//	
//	virtual void restartGradient();
//	virtual void restartHessian();
//	virtual void updateGradient();
//	virtual void updateHessian();
//
//	virtual void testGradient();
//	virtual void updateGradientA();
//	virtual void updateGradientFD();
//
//	virtual void testHessian();
//	virtual void updateHessianA();
//	virtual void updateHessianFD();
//	virtual void updateHessian2ND();
//	virtual void updateHessianBFGS();
//
//	virtual void updateLambdas();
//	virtual void updatePenalty();
//
//	virtual void pushParameters();
//	virtual void pullParameters();
//
//	virtual void fixParameterVector(VectorXd& vp) const;
//	virtual void fixConstraintVector(VectorXd& vc) const;
//	virtual void fixConstraintJacobian(tVector& vJ) const;
//
//protected:
//
//	int m_nX;
//	int m_nx;
//	int m_np;
//
//	Real m_mu;						// Current mu
//	VectorXd m_vg;					// Current gradient
//	VectorXd m_vC;					// Current constraints
//	VectorXd m_vp;					// Current solution
//	VectorXd m_vl;					// Current lambdas
//
//	SparseMatrixXd m_mDCDp;			// Constraint Jacobian
//	
//	MatrixXd m_mH;					// Current Hessian
//
//	// BFGS variables
//	VectorXd m_vg_P;				// Previous gradient
//	VectorXd m_vp_P;				// Previous solution
//	VectorXd m_sk;
//	VectorXd m_yk;
//	MatrixXd m_modifier1;
//	MatrixXd m_modifier2;
//	MatrixXd m_tempMatrix;
//	Real m_rho;
//
//	Real m_curGradi;
//	Real m_curConst;
//	Real m_maxGradi;
//	Real m_maxConst;
//
//	Real m_tau;
//	Real m_gamma;
//	Real m_alpha;
//
//	Real m_betaConst;				// betaconst > 0 && betaConst < min(1, betaMerit)
//	Real m_alphaConst;				// alphaConst > 0 && alphaConst < min(1, alphaMerit)
//	
//	Real m_betaGradi;				// betaMerit > 0
//	Real m_alphaGradi;				// alphaMerit > 0
//
//	SolidModel* m_pModel;
//	PhysSolver* m_pSolver;
//
//	string m_ID;
//
//	iVector m_vfixIdx;
//
//	VectorXd m_vxT;
//
//	int m_outerIt;
//	int m_innerIt;
//	bool m_solved;
//
//};
//
//#endif