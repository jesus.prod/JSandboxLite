/*=====================================================================================*/
/*!
\file		SurfaceRemesher.cpp
\author		jesusprod
\brief		Implementation of SurfaceRemesher.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/SurfaceRemesher.h>

RemeshOP SurfaceRemesher::splitFace(const MeshTraits::ID& fid)
{
	RemeshOP out = this->createOperation();

	TriMesh::FaceHandle fh = this->m_pMesh->getFaceHandle(fid);

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getFaceVertices(fh, vvh);

	vector<TriMesh::EdgeHandle> vfeh;
	m_pMesh->getFaceEdges(fh, vfeh);

	// Remove face and edges if needed

	m_pMesh->delete_face(fh, false);
	out.m_vDelFace.push_back(fid);

	for (int j = 0; j < (int)vfeh.size(); ++j)
		if (this->m_pMesh->status(vfeh[j]).deleted())
			out.m_vDelEdge.push_back(m_pMesh->getEdgeID(vfeh[j]));

	// Introduce new vertex in the midpoint

	TriMesh::Point newX = (m_pMesh->point(vvh[0]) +
						   m_pMesh->point(vvh[1]) +
						   m_pMesh->point(vvh[2])*(1/3));
	TriMesh::VertexHandle vh = m_pMesh->add_vertex(newX);
	MeshTraits::ID vid = TriMesh::CreateIdentifier();
	m_pMesh->setVertID(vh, vid);
	out.m_vAddVert.push_back(vid);

	// Introduce new faces and edges

	TriMesh::FaceHandle fh0 = m_pMesh->add_face(vh, vvh[0], vvh[1]);
	TriMesh::FaceHandle fh1 = m_pMesh->add_face(vh, vvh[1], vvh[2]);
	TriMesh::FaceHandle fh2 = m_pMesh->add_face(vh, vvh[2], vvh[0]);
	MeshTraits::ID fid0 = TriMesh::CreateIdentifier();
	MeshTraits::ID fid1 = TriMesh::CreateIdentifier();
	MeshTraits::ID fid2 = TriMesh::CreateIdentifier();
	m_pMesh->setFaceID(fh0, fid0);
	m_pMesh->setFaceID(fh1, fid1);
	m_pMesh->setFaceID(fh2, fid2);
	out.m_vAddFace.push_back(fid0);
	out.m_vAddFace.push_back(fid1);
	out.m_vAddFace.push_back(fid2);

	vector<TriMesh::FaceHandle> vfhNew;
	vfhNew.push_back(fh0);
	vfhNew.push_back(fh1);
	vfhNew.push_back(fh2);
	for (int i = 0; i < (int)vfhNew.size(); ++i)
	{
		const TriMesh::FaceHandle& fh = vfhNew[i];
		vector<TriMesh::EdgeHandle> vehNew;
		m_pMesh->getFaceEdges(fh, vehNew);
		for (int j = 0; j < 3; ++j)
		{
			if (m_pMesh->getEdgeID(vehNew[j]) == 0)
			{
				MeshTraits::ID eid = TriMesh::CreateIdentifier();
				m_pMesh->setEdgeID(vehNew[j], eid);
				out.m_vAddEdge.push_back(eid);
			}
		}
	}

	// Operation OK
	out.m_OK = true;

	return out;
}


RemeshOP SurfaceRemesher::splitEdge(const MeshTraits::ID& eid)
{
	RemeshOP out = this->createOperation();

	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getHingeVertices(eh, vvh);

	vector<TriMesh::FaceHandle> vfh;
	m_pMesh->getEdgeFaces(eh, vfh);

	vector<vector<TriMesh::EdgeHandle>> vfehs;	
	for (int i = 0; i < (int)vfh.size(); ++i)
	{
		vfehs.push_back(vector<TriMesh::EdgeHandle>());
		m_pMesh->getFaceEdges(vfh[i], vfehs.back());
	}

	// Remove edge and faces ikj, jli

	m_pMesh->delete_edge(eh, false);
	out.m_vDelEdge.push_back(eid);

	for (int i = 0; i < (int)vfh.size(); ++i) 
	{
		for (int j = 0; j < (int) vfehs[i].size(); ++j)
			if (this->m_pMesh->status(vfehs[i][j]).deleted())
				out.m_vDelEdge.push_back(m_pMesh->getEdgeID(vfehs[i][j]));

		out.m_vDelFace.push_back(m_pMesh->getFaceID(vfh[i]));
	}

	// Introduce new vertex in the midpoint

	TriMesh::Point newX = (m_pMesh->point(vvh[0]) +
						   m_pMesh->point(vvh[1]))*0.5;
	TriMesh::VertexHandle vh = m_pMesh->add_vertex(newX);
	MeshTraits::ID vid = TriMesh::CreateIdentifier();
	m_pMesh->setVertID(vh, vid);
	out.m_vAddVert.push_back(vid);

	// Introduce new faces and edges

	vector<TriMesh::FaceHandle> vfhNew;

	Vector3d v = m_pMesh->getPoint(vh);
	Vector3d v0 = m_pMesh->getPoint(vvh[0]);
	Vector3d v1 = m_pMesh->getPoint(vvh[1]);
	Vector3d v2 = m_pMesh->getPoint(vvh[2]);

	TriMesh::FaceHandle fh0;
	TriMesh::FaceHandle fh1;
	fh0 = m_pMesh->add_face(vh, vvh[0], vvh[2]);
	fh1 = m_pMesh->add_face(vh, vvh[2], vvh[1]);
	MeshTraits::ID fid0 = TriMesh::CreateIdentifier();
	MeshTraits::ID fid1 = TriMesh::CreateIdentifier();
	m_pMesh->setFaceID(fh0, fid0);
	m_pMesh->setFaceID(fh1, fid1);
	out.m_vAddFace.push_back(fid0);
	out.m_vAddFace.push_back(fid1);
	vfhNew.push_back(fh0);
	vfhNew.push_back(fh1);

	if ((int) vvh.size() == 4)
	{
		Vector3d v3 = m_pMesh->getPoint(vvh[3]);
		TriMesh::FaceHandle fh2;
		TriMesh::FaceHandle fh3;
		fh2 = m_pMesh->add_face(vh, vvh[1], vvh[3]);
		fh3 = m_pMesh->add_face(vh, vvh[3], vvh[0]);
		MeshTraits::ID fid2 = TriMesh::CreateIdentifier();
		MeshTraits::ID fid3 = TriMesh::CreateIdentifier();
		m_pMesh->setFaceID(fh2, fid2);
		m_pMesh->setFaceID(fh3, fid3);
		out.m_vAddFace.push_back(fid2);
		out.m_vAddFace.push_back(fid3);
		vfhNew.push_back(fh2);
		vfhNew.push_back(fh3);
	}

	for (int i = 0; i < (int)vfhNew.size(); ++i)
	{
		const TriMesh::FaceHandle& fh = vfhNew[i];
		vector<TriMesh::EdgeHandle> vehNew;
		m_pMesh->getFaceEdges(fh, vehNew);
		for (int j = 0; j < 3; ++j)
		{
			if (m_pMesh->getEdgeID(vehNew[j]) == 0)
			{
				MeshTraits::ID eid = TriMesh::CreateIdentifier();
				m_pMesh->setEdgeID(vehNew[j], eid);
				out.m_vAddEdge.push_back(eid);
			}
		}
	}

	// Operation OK
	out.m_OK = true;

	return out;
}

RemeshOP SurfaceRemesher::flipEdge(const MeshTraits::ID& eid)
{
	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	assert(this->m_pMesh->is_boundary(eh));

	RemeshOP out = this->createOperation();

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getHingeVertices(eh, vvh);

	vector<TriMesh::FaceHandle> vfh;
	m_pMesh->getEdgeFaces(eh, vfh);

	// Remove edge and faces ikj, jli

	m_pMesh->delete_edge(eh, false);
	out.m_vDelEdge.push_back(eid);

	for (int i = 0; i < (int)vfh.size(); ++i)
		out.m_vDelFace.push_back(m_pMesh->getFaceID(vfh[i]));

	// Introduce new faces and edges

	TriMesh::FaceHandle fh0 = m_pMesh->add_face(vvh[0], vvh[2], vvh[3]);
	TriMesh::FaceHandle fh1 = m_pMesh->add_face(vvh[2], vvh[3], vvh[2]);
	MeshTraits::ID fid0 = TriMesh::CreateIdentifier();
	MeshTraits::ID fid1 = TriMesh::CreateIdentifier();
	m_pMesh->setFaceID(fh0, fid0);
	m_pMesh->setFaceID(fh1, fid1);
	out.m_vAddFace.push_back(fid0);
	out.m_vAddFace.push_back(fid1);
	for (TriMesh::FaceEdgeIter fe_it = m_pMesh->fe_begin(fh0); fe_it != m_pMesh->fe_end(fh0); fe_it++)
	{
		// Look for the new added edges
		TriMesh::EdgeHandle feh = *fe_it;

		vector<TriMesh::VertexHandle> vevh;
		m_pMesh->getEdgeVertices(feh, vevh);
		if ((vevh[0] == vvh[2] && vevh[1] == vvh[3]) ||
			(vevh[0] == vvh[2] && vevh[1] == vvh[3]))
		{
			MeshTraits::ID eid = TriMesh::CreateIdentifier();
			m_pMesh->setEdgeID(feh, eid);
			out.m_vAddEdge.push_back(eid);
			break;
		}
	}

	// Operation OK
	out.m_OK = true;

	return out;
}

bool SurfaceRemesher::filterIndependent(const vector<MeshTraits::ID>& veidIn, vector<MeshTraits::ID>& veidOut) const
{
	veidOut.clear();

	for (int i = 0; i < (int)veidIn.size(); ++i)
		if (this->isIndependentEdge(veidIn[i], veidIn))
			veidOut.push_back(veidIn[i]);

	return !veidOut.empty();
}

bool SurfaceRemesher::isIndependentEdge(const MeshTraits::ID& eid, const vector<MeshTraits::ID>& veid) const
{
	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	vector<TriMesh::VertexHandle> vvh0;
	m_pMesh->getEdgeVertices(eh, vvh0);

	for (int i = 0; i < (int)veid.size(); ++i)
	{
		TriMesh::EdgeHandle ehi = this->m_pMesh->getEdgeHandle(veid[i]);

		vector<TriMesh::VertexHandle> vvh1;
		m_pMesh->getEdgeVertices(ehi, vvh1);
		if (vvh0[0] == vvh1[0] || vvh0[0] == vvh1[1] ||
			vvh0[1] == vvh1[0] || vvh0[1] == vvh1[1])
			return false;
	}

	return true;
}

RemeshOP SurfaceRemesher::appendOperations(const RemeshOP& op0, const RemeshOP& op1) const
{
	RemeshOP out = op0;
	out.m_OK &= op1.m_OK;

	for (int i = 0; i < (int) op1.m_vAddEdge.size(); ++i) out.m_vAddEdge.push_back(op1.m_vAddEdge[i]);
	for (int i = 0; i < (int) op1.m_vAddFace.size(); ++i) out.m_vAddFace.push_back(op1.m_vAddFace[i]);
	for (int i = 0; i < (int) op1.m_vAddVert.size(); ++i) out.m_vAddVert.push_back(op1.m_vAddVert[i]);
	for (int i = 0; i < (int) op1.m_vDelEdge.size(); ++i) out.m_vDelEdge.push_back(op1.m_vDelEdge[i]);
	for (int i = 0; i < (int) op1.m_vDelFace.size(); ++i) out.m_vDelFace.push_back(op1.m_vDelFace[i]);
	for (int i = 0; i < (int) op1.m_vDelVert.size(); ++i) out.m_vDelVert.push_back(op1.m_vDelVert[i]); 

	std::sort(out.m_vAddEdge.begin(), out.m_vAddEdge.end()), out.m_vAddEdge.end();
	std::sort(out.m_vAddFace.begin(), out.m_vAddFace.end()), out.m_vAddFace.end();
	std::sort(out.m_vAddVert.begin(), out.m_vAddVert.end()), out.m_vAddVert.end();
	std::sort(out.m_vDelEdge.begin(), out.m_vDelEdge.end()), out.m_vDelEdge.end();
	std::sort(out.m_vDelFace.begin(), out.m_vDelFace.end()), out.m_vDelFace.end();
	std::sort(out.m_vDelVert.begin(), out.m_vDelVert.end()), out.m_vDelVert.end();
	out.m_vAddEdge.erase(unique(out.m_vAddEdge.begin(), out.m_vAddEdge.end()), out.m_vAddEdge.end());
	out.m_vAddFace.erase(unique(out.m_vAddFace.begin(), out.m_vAddFace.end()), out.m_vAddFace.end());
	out.m_vAddVert.erase(unique(out.m_vAddVert.begin(), out.m_vAddVert.end()), out.m_vAddVert.end());
	out.m_vDelEdge.erase(unique(out.m_vDelEdge.begin(), out.m_vDelEdge.end()), out.m_vDelEdge.end());
	out.m_vDelFace.erase(unique(out.m_vDelFace.begin(), out.m_vDelFace.end()), out.m_vDelFace.end());
	out.m_vDelVert.erase(unique(out.m_vDelVert.begin(), out.m_vDelVert.end()), out.m_vDelVert.end());

	// Remove deleted elements from the list of added

	for (int i = 0; i < (int)out.m_vDelEdge.size(); ++i)
	{
		vector<MeshTraits::ID>::iterator it = std::find(out.m_vAddEdge.begin(), out.m_vAddEdge.end(), out.m_vDelEdge[i]);
		if (it != out.m_vAddEdge.end())
			out.m_vAddEdge.erase(it);
	}

	for (int i = 0; i < (int)out.m_vDelFace.size(); ++i)
	{
		vector<MeshTraits::ID>::iterator it = std::find(out.m_vAddFace.begin(), out.m_vAddFace.end(), out.m_vDelFace[i]);
		if (it != out.m_vAddFace.end())
			out.m_vAddFace.erase(it);
	}

	for (int i = 0; i < (int)out.m_vDelVert.size(); ++i)
	{
		vector<MeshTraits::ID>::iterator it = std::find(out.m_vAddVert.begin(), out.m_vAddVert.end(), out.m_vDelVert[i]);
		if (it != out.m_vAddVert.end())
			out.m_vAddVert.erase(it);
	}

	return out;
}

void SurfaceRemesher::updateActiveSet(vector<MeshTraits::ID>& vacset, const RemeshOP& op) const
{
	for (int i = 0; i < (int)op.m_vDelEdge.size(); ++i) // Remove deleted edges before adding new
	{
		vector<MeshTraits::ID>::iterator it = std::find(vacset.begin(), vacset.end(), op.m_vDelEdge[i]);
		if (it != vacset.end())
			vacset.erase(it);
	}

	for (int i = 0; i < (int)op.m_vAddEdge.size(); ++i)
		vacset.push_back(op.m_vAddEdge[i]); // Add new
}

RemeshOP SurfaceRemesher::createOperation() const
{
	RemeshOP out;
	out.m_OK = true;
	out.m_pMesh = this->m_pMesh;
	out.m_pEdgeTree = this->m_pEdgeTree;
	out.m_pFaceTree = this->m_pFaceTree;
	return out;
}

void SurfaceRemesher::freeMemory()
{
	if (!this->m_remeshOP.m_OK) // Invalid
		delete this->m_remeshOP.m_pMesh;
}
