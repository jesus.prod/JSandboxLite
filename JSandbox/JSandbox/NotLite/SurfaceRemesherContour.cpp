/*=====================================================================================*/
/*!
\file		SurfaceRemesherContour.cpp
\author		jesusprod
\brief		Implementation of SurfaceRemesherContour.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/SurfaceRemesherContour.h>

SurfaceRemesherContour::SurfaceRemesherContour()
{
	this->m_intersecEpsi = EPS_INTERS;
	this->m_boundaryEpsi = EPS_INTERS;
	this->m_edgeMovePara = 1.0/2.0;
	this->m_faceMovePara = 1.0/3.0;
}

SurfaceRemesherContour::~SurfaceRemesherContour()
{
	this->freeMemory();
}

const RemeshOP& SurfaceRemesherContour::remesh(const TriMesh* pMesh, const BVHTree* pEdgeTree, const BVHTree* pFaceTree)
{
	assert(pMesh != NULL);

	// Copy mesh to remesh and build tree

	this->m_pMesh = new TriMesh(*pMesh);

	this->m_pEdgeTree = NULL;
	this->m_pFaceTree = NULL;

	//// Prepare edge tree

	//if (pEdgeTree != NULL)
	//	this->m_pEdgeTree = new BVHTree(*pEdgeTree);
	//else
	//{
	//	BVHTree* aEdgeTree = new BVHTree(0);
	//	aEdgeTree->initializeMeshEdge(m_pMesh);
	//	this->m_pEdgeTree = aEdgeTree;
	//}

	//// Prepare face tree

	//if (pFaceTree != NULL)
	//	this->m_pFaceTree = new BVHTree(*pFaceTree);
	//else
	//{
	//	BVHTree* aFaceTree = new BVHTree(0);
	//	aFaceTree->initializeMeshFace(m_pMesh);
	//	this->m_pFaceTree = aFaceTree;
	//}

	// Set intersection epsilon depending on the size

	dVector bbRanges = this->m_pMesh->getAABBRanges();
	double max = -HUGE_VAL;
	for (int i = 0; i < 3; ++i)
		if (bbRanges[i] > max)
			max = bbRanges[i];

	this->m_boundaryEpsiScaled = this->m_boundaryEpsi*max;
	this->m_intersecEpsiScaled = this->m_intersecEpsi*max;

	// Create remesh operation

	this->m_remeshOP = this->createOperation();

	// Project curves over surface
	
	int nl = (int) this->m_vrawContours.size();

	this->m_vEdges.resize(nl); // Output edges
	this->m_vVerts.resize(nl); // Output verts

	for (int i = 0; i < nl; ++i)
	{
		this->m_vconflictEdge.clear();
		this->m_vconflictNode.clear();

		// Intialize conflict node list with added vertices
		this->m_vconflictNode = this->m_remeshOP.m_vAddVert;

		RemeshOP contourOP = this->createOperation();

		// Contour nodes

		if (this->m_contNodes)
		{
			delete this->m_pFaceTree;
			this->m_pFaceTree = new BVHTree(0);
			this->m_pFaceTree->initializeMeshFace(this->m_pMesh);
			contourOP = this->appendOperations(contourOP, this->contourNodes(i));
		}
		else if (this->m_contExtremes)
			contourOP = this->appendOperations(contourOP, this->contourExtremes(i));

		if (!contourOP.m_OK)
			break; // Invalid

		// Garbage collection before continuing 

		this->m_pMesh->garbage_collection();
		this->m_pMesh->updateIdentifiersMap();

		// Contour edges

		if (this->m_contEdges)
		{
			delete this->m_pEdgeTree;
			this->m_pEdgeTree = new BVHTree(0);
			this->m_pEdgeTree->initializeMeshEdge(this->m_pMesh);
			contourOP = this->appendOperations(contourOP, this->contourEdges(i));
		}

		if (!contourOP.m_OK)
		break; // Invalid

		// Garbage collection before continuing 

		this->m_pMesh->garbage_collection();
		this->m_pMesh->updateIdentifiersMap();

		// Extract contour verts and edges

		this->m_vVerts[i] = contourOP.m_vAddVert;
		this->m_vEdges[i] = contourOP.m_vAddEdge;

		m_remeshOP = this->appendOperations(m_remeshOP, contourOP);
	}

	delete this->m_pEdgeTree;
	delete this->m_pFaceTree;

	for (int i = 0; i < nl; ++i)
	{
		vector<MeshTraits::ID> vfilteredEdges;
		computeContourEdges(i, vfilteredEdges);
		this->m_vEdges[i] = vfilteredEdges;
	}

	if (!this->m_remeshOP.m_OK)
	{
		this->freeMemory(); // Invalid
	}

	return this->m_remeshOP;
}

RemeshOP SurfaceRemesherContour::contourNodes(int i)
{
	RemeshOP nodesOP = this->createOperation();

	const Curve& curve = this->m_vrawContours[i];
	int nn = curve.getNumNode();
	vector<Vector3d> vcurveCloud(nn);
	for (int j = 0; j < nn; ++j) // Get cloud
		vcurveCloud[j] = curve.getPosition(j);

	bVector vdoneStencil(nn, false);

	BVHTree* pContourTree = new BVHTree(i + 1);
	pContourTree->initializeCloud(vcurveCloud);

	// Find potential segment-edge collisions

	vector<BVHClosePrimitivePair> vcpps; // Close pairs meshedge-segment
	BVHTree::testRecursive(this->m_pFaceTree, pContourTree, 0, 0, vcpps);

	for (int j = 0; j < (int)vcpps.size(); ++j)
	{
		if (vdoneStencil[vcpps[j].m_prim1.m_vpidx[0]])
			continue; // This node has been projected

		TriMesh::FaceHandle fh = this->m_pMesh->face_handle(vcpps[j].m_prim0.m_index);

		if (this->m_pMesh->status(fh).deleted())
			continue; // Has been deleted before!

		Vector3d p = curve.getPosition(vcpps[j].m_prim1.m_vpidx[0]);

		// TODO: Remove this check
		vector<TriMesh::VertexHandle> vfvh;
		m_pMesh->getFaceVertices(fh, vfvh);
		assert(vfvh[0].idx() == vcpps[j].m_prim0.m_vpidx[0]);
		assert(vfvh[1].idx() == vcpps[j].m_prim0.m_vpidx[1]);
		assert(vfvh[2].idx() == vcpps[j].m_prim0.m_vpidx[2]);

		MeshTraits::ID fid = this->m_pMesh->getFaceID(fh);
		RemeshOP nodeOP = this->projectPointToFace(p, fid);
		if (!nodeOP.m_OK)
			break;

		if ((int)nodeOP.m_vAddVert.size() != 0) // Added?
			vdoneStencil[vcpps[j].m_prim1.m_vpidx[0]] = true;

		for (int k = 0; k < (int)nodeOP.m_vDelFace.size(); ++k)
		{
			int delidx = this->m_pMesh->getFaceHandle(nodeOP.m_vDelFace[k]).idx();

			for (int p = 0; p < (int)vcpps.size(); ++p)
			{
				if (vcpps[p].m_prim0.m_index != delidx)
					continue; // Only deleted primitives

				for (int f = 0; f < (int)nodeOP.m_vAddFace.size(); ++f)
				{
					TriMesh::FaceHandle newfh = this->m_pMesh->getFaceHandle(nodeOP.m_vAddFace[f]);

					vector<TriMesh::VertexHandle> vhvh;
					m_pMesh->getFaceVertices(newfh, vhvh);

					// Create and add new close primitive pair 
					BVHClosePrimitivePair newcpps = vcpps[p];
					newcpps.m_prim0.m_index = newfh.idx();
					for (int v = 0; v < 3; ++v)
						newcpps.m_prim0.m_vpidx[v] = vhvh[v].idx();

					vcpps.push_back(newcpps);
				}
			}
		}

		for (int k = 0; k < (int)nodeOP.m_vAddVert.size(); ++k)
		{
			TriMesh::VertexHandle vh = this->m_pMesh->getVertHandle(nodeOP.m_vAddVert[k]);

			//vector<TriMesh::VertexHandle> vvh;
			//this->m_pMesh->getVertexVertices(vh, vvh);
			//for (int l = 0; l < (int) vvh.size(); ++l)
			//	this->m_vconflictNode.push_back(this->m_pMesh->getVertID(vvh[l]));

			m_vconflictNode.push_back(nodeOP.m_vAddVert[k]);
		}

		for (int k = 0; k < (int) nodeOP.m_vAddEdge.size(); ++k)
			m_vconflictEdge.push_back(nodeOP.m_vAddEdge[k]);

		nodesOP = this->appendOperations(nodesOP, nodeOP);
	}

	// Free contour BVH
	delete pContourTree;

	return nodesOP;
}

RemeshOP SurfaceRemesherContour::contourEdges(int i)
{
	RemeshOP edgesOP = this->createOperation();

	const Curve& curve = this->m_vrawContours[i];
	BVHTree* pContourTree = new BVHTree(i + 1);
	pContourTree->initializeCurve(curve);

	// Find potential segment-edge collisions

	vector<BVHClosePrimitivePair> vcpps; // Close pairs meshedge-segment
	BVHTree::testRecursive(this->m_pEdgeTree, pContourTree, 0, 0, vcpps);

	for (int j = 0; j < (int)vcpps.size(); ++j)
	{
		TriMesh::EdgeHandle eh = this->m_pMesh->edge_handle(vcpps[j].m_prim0.m_index);

		if (this->m_pMesh->status(eh).deleted())
			continue; // Has been deleted before!

		MeshTraits::ID eid = this->m_pMesh->getEdgeID(eh);
		if (find(this->m_vconflictEdge.begin(), m_vconflictEdge.end(), eid) != m_vconflictEdge.end())
			continue; // Has been already added what means it is a new edge or it has been visited

		Vector3d p0 = curve.getPosition(vcpps[j].m_prim1.m_vpidx[0]);
		Vector3d p1 = curve.getPosition(vcpps[j].m_prim1.m_vpidx[1]);

		// TODO: Remove this check
		vector<TriMesh::VertexHandle> vevh;
		m_pMesh->getEdgeVertices(eh, vevh);
		assert(vevh[0].idx() == vcpps[j].m_prim0.m_vpidx[0]);
		assert(vevh[1].idx() == vcpps[j].m_prim0.m_vpidx[1]);

		RemeshOP edgeOP = this->projectSegmentToEdge(p0, p1, eid);
		if (!edgeOP.m_OK)
			break;

		for (int k = 0; k < (int)edgeOP.m_vAddVert.size(); ++k)
		{
			TriMesh::VertexHandle vh = this->m_pMesh->getVertHandle(edgeOP.m_vAddVert[k]);

			//vector<TriMesh::VertexHandle> vvh;
			//this->m_pMesh->getVertexVertices(vh, vvh);
			//for (int l = 0; l < (int)vvh.size(); ++l)
			//	this->m_vconflictNode.push_back(this->m_pMesh->getVertID(vvh[l]));

			m_vconflictNode.push_back(edgeOP.m_vAddVert[k]);
		}

		for (int k = 0; k < (int)edgeOP.m_vAddEdge.size(); ++k)
			m_vconflictEdge.push_back(edgeOP.m_vAddEdge[k]);

		edgesOP = this->appendOperations(edgesOP, edgeOP);
	}

	// Free contour BVH
	delete pContourTree;

	return edgesOP;
}

RemeshOP SurfaceRemesherContour::contourExtremes(int i)
{
	RemeshOP extremesOP = this->createOperation();

	const Curve& curve = this->m_vrawContours[i];

	// Are there extremes?
	if (curve.getIsClosed())
		return extremesOP;

	int nn = curve.getNumNode();

	vector<Vector3d> vpoints;
	vpoints.push_back(curve.getPosition(0));
	vpoints.push_back(curve.getPosition(nn-1));
	
	// TODO: Use here the BVH for mesh faces here

	for (int j = 0; j < 2; ++j)
	{
		for (TriMesh::FaceIter fit = this->m_pMesh->faces_begin(); fit != this->m_pMesh->faces_end(); fit++)
		{
			TriMesh::FaceHandle fh = *fit;

			// Check if the face still exists
			if (this->m_pMesh->status(fh).deleted())
				continue; // Has been deleted before!

			MeshTraits::ID fid = this->m_pMesh->getFaceID(fh);
				
			RemeshOP extremeOP = this->projectPointToFace(vpoints[j], fid);
			if (!extremeOP.m_OK)
				continue;

			for (int k = 0; k < (int) extremeOP.m_vAddVert.size(); ++k)
			{
				TriMesh::VertexHandle vh = this->m_pMesh->getVertHandle(extremeOP.m_vAddVert[k]);

				vector<TriMesh::VertexHandle> vvh;
				this->m_pMesh->getVertexVertices(vh, vvh);
				for (int l = 0; l < (int)vvh.size(); ++l)
					this->m_vconflictNode.push_back(this->m_pMesh->getVertID(vvh[l]));

				m_vconflictNode.push_back(extremeOP.m_vAddVert[k]);
			}

			for (int k = 0; k < (int) extremeOP.m_vAddEdge.size(); ++k)
				m_vconflictEdge.push_back(extremeOP.m_vAddEdge[k]);
				
			if (extremeOP.m_vAddVert.size() == 1)
			{
				extremesOP = this->appendOperations(extremesOP, extremeOP);
				break; // Extreme already added to the contour, break out 
			}
		}
	}

	return extremesOP;
}

RemeshOP SurfaceRemesherContour::projectPointToFace(const Vector3d& p, const MeshTraits::ID& fid)
{
	RemeshOP remeshOP;

	TriMesh::FaceHandle fh = this->m_pMesh->getFaceHandle(fid);

	vector<TriMesh::VertexHandle> vvh;
	this->m_pMesh->getFaceVertices(fh, vvh);
	Vector3d y0 = this->m_pMesh->getPoint(vvh[0]);
	Vector3d y1 = this->m_pMesh->getPoint(vvh[1]);
	Vector3d y2 = this->m_pMesh->getPoint(vvh[2]);

	bool remeshed = false;

	double l0, l1;
	double distance = sqrDistanceVertexTriangle(p, y0, y1, y2, &l0, &l1);
	if (distance >= 0 && distance < this->m_intersecEpsiScaled) // Face
	{
		double b0 = 1 - l0 - l1;
		double b1 = l0;
		double b2 = l1;

		MeshTraits::ID vid0 = this->m_pMesh->getVertID(vvh[0]);
		MeshTraits::ID vid1 = this->m_pMesh->getVertID(vvh[1]);
		MeshTraits::ID vid2 = this->m_pMesh->getVertID(vvh[2]);
		bool conflict0 = find(this->m_vconflictNode.begin(), this->m_vconflictNode.end(), vid0) != this->m_vconflictNode.end();
		bool conflict1 = find(this->m_vconflictNode.begin(), this->m_vconflictNode.end(), vid1) != this->m_vconflictNode.end();
		bool conflict2 = find(this->m_vconflictNode.begin(), this->m_vconflictNode.end(), vid2) != this->m_vconflictNode.end();

		// On vertex

		if (!remeshed && isApprox(b0, 1.0, this->m_faceMovePara)) // Almost y0
		{
			if (isApprox(b0, 1.0, EPS_POS))
			{
				remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[0]), p);
				remeshed = true;
			}
			else if (!conflict0)
			{
				if (!this->m_pMesh->is_boundary(vvh[0]))
					remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[0]), p);
				else remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[0]), y0);
				remeshed = true;
			}
		}

		if (!remeshed && isApprox(b1, 1.0, this->m_faceMovePara)) // Almost y1
		{
			if (isApprox(b1, 1.0, EPS_POS))
			{
				remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[1]), p);
				remeshed = true;
			}
			else if (!conflict1)
			{
				if (!this->m_pMesh->is_boundary(vvh[1]))
					remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[1]), p);
				else remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[1]), y1);
				remeshed = true;
			}
		}

		if (!remeshed && isApprox(b2, 1.0, this->m_faceMovePara))  // Almost y2
		{
			if (isApprox(b2, 1.0, EPS_POS))
			{
				remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[2]), p);
				remeshed = true;
			}
			else if (!conflict2)
			{
				if (!this->m_pMesh->is_boundary(vvh[2]))
					remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[2]), p);
				else remeshOP = moveMeshVertex(this->m_pMesh->getVertID(vvh[2]), y2);
				remeshed = true;
			}
		}

		// On edge

		vector<TriMesh::EdgeHandle> veh;
		this->m_pMesh->getFaceEdges(fh, veh);
		vector<TriMesh::VertexHandle> vhPair(2);
		vector<Vector3d> vpPair(2);
		vector<double> vbPair(2);

		Real edgePara = -1.0;
		bool isOnEdge = false;
		
		if (!remeshed && !isOnEdge && isApprox(b0, 0.0, this->m_faceMovePara) && b0 < b1 && b0 < b2) // Almost no y0, split [y1, y2]
		{
			vhPair[0] = vvh[1];
			vhPair[1] = vvh[2];
			vpPair[0] = y1;
			vpPair[1] = y2;
			vbPair[0] = b1;
			vbPair[1] = b2;
			isOnEdge = true;
			edgePara = b0;
		}

		if (!remeshed && !isOnEdge && isApprox(b1, 0.0, this->m_faceMovePara) && b1 < b0 && b1 < b2) // Almost no y1, split [y0, y2]
		{
			vhPair.resize(2);
			vhPair[0] = vvh[0];
			vhPair[1] = vvh[2];
			vpPair[0] = y0;
			vpPair[1] = y2;
			vbPair[0] = b0;
			vbPair[1] = b2;
			isOnEdge = true;
			edgePara = b1;
		}

		if (!remeshed && !isOnEdge && isApprox(b2, 0.0, this->m_faceMovePara) && b2 < b0 && b2 < b1) // Almost no y2, split [y0, y1]
		{
			vhPair.resize(2);
			vhPair[0] = vvh[0];
			vhPair[1] = vvh[1];
			vpPair[0] = y0;
			vpPair[1] = y1;
			vbPair[0] = b0;
			vbPair[1] = b1;
			isOnEdge = true;
			edgePara = b2;
		}

		if (!remeshed && isOnEdge)
		{
			TriMesh::EdgeHandle eh;

			// Look for the corresponding edge
			vector<TriMesh::VertexHandle> vevh;
			for (int i = 0; i < 3; ++i)
			{
				this->m_pMesh->getEdgeVertices(veh[i], vevh);

				if ((vevh[0] == vhPair[0] && vevh[1] == vhPair[1]) ||
					(vevh[0] == vhPair[1] && vevh[1] == vhPair[0]))
				{
					// Edge found
					eh = veh[i];
					break;
				}
			}

			//MeshTraits::ID eid = this->m_pMesh->getEdgeID(eh);

			// Check if there is no conflict with previously added edges, from this or previous contoured curves
			// find(this->m_vconflictEdge.begin(), this->m_vconflictEdge.end(), eid) == this->m_vconflictEdge.end()
			if (true)
			{
				bool isBoundary = this->m_pMesh->is_boundary(eh); // Cache status
				RemeshOP splitOP = this->splitEdge(this->m_pMesh->getEdgeID(eh));
				if (splitOP.m_OK)
				{
					// Assert only one vertex has been added
					assert((int)splitOP.m_vAddVert.size() == 1);

					if (!isBoundary)
						remeshOP = this->appendOperations(splitOP, this->moveMeshVertex(splitOP.m_vAddVert[0], p));
					else
					{
						double bsum = vbPair[0] + vbPair[1];
						vbPair[0] = vbPair[0] / bsum;
						vbPair[1] = vbPair[1] / bsum;
						remeshOP = this->appendOperations(splitOP, this->moveMeshVertex(splitOP.m_vAddVert[0], vbPair[0] * vpPair[0] + vbPair[1] * vpPair[1]));
					}

					remeshed = true;
				}
			}
		}

		// On face

		if (!remeshed)
		{
			RemeshOP splitOP = this->splitFace(fid);
			if (splitOP.m_OK)
			{
				// Assert only one vertex has been added
				assert((int)splitOP.m_vAddVert.size() == 1);

				remeshOP = this->appendOperations(splitOP, this->moveMeshVertex(splitOP.m_vAddVert[0], b0*y0 + b1*y1 + b2*y2));

				remeshed = true;
			}
		}
	}

	if (remeshed)
		return remeshOP;
	else return this->createOperation();
}

RemeshOP SurfaceRemesherContour::projectSegmentToEdge(const Vector3d& x0, const Vector3d& x1, const MeshTraits::ID& eid)
{
	RemeshOP remeshOP;

	TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);

	vector<TriMesh::VertexHandle> vvh;
	this->m_pMesh->getEdgeVertices(eh, vvh);
	Vector3d y0 = this->m_pMesh->getPoint(vvh[0]);
	Vector3d y1 = this->m_pMesh->getPoint(vvh[1]);

	dVector a0(3); a0[0] = x0(0); a0[1] = x0(1); a0[2] = x0(2);
	dVector a1(3); a1[0] = x1(0); a1[1] = x1(1); a1[2] = x1(2);
	dVector b0(3); b0[0] = y0(0); b0[1] = y0(1); b0[2] = y0(2);
	dVector b1(3); b1[0] = y1(0); b1[1] = y1(1); b1[2] = y1(2);

	bool remeshed = false;

	double mua;
	double mub;
	if (lineLineIntersect(a0, a1, b0, b1, mua, mub, this->m_intersecEpsiScaled))
	{
		// If the intersection is close enough to the 
		// extreme of the edge don't split the edge, 
		// just move the point
		
		MeshTraits::ID vid0 = this->m_pMesh->getVertID(vvh[0]);
		MeshTraits::ID vid1 = this->m_pMesh->getVertID(vvh[1]);

		if (!remeshed && mub <= this->m_edgeMovePara)
		{
			if (!this->m_pMesh->is_boundary(vvh[0]) || this->m_pMesh->is_boundary(eh) || mub < this->m_boundaryEpsiScaled)
			{
				if (find(this->m_vconflictNode.begin(), this->m_vconflictNode.end(), vid0) == this->m_vconflictNode.end())
				{
					remeshOP = this->moveMeshVertex(this->m_pMesh->getVertID(vvh[0]), x0*(1 - mua) + x1*mua);
					remeshed = remeshOP.m_OK;
				}
			}
		}

		if (mub >= 1.0 - this->m_edgeMovePara)
		{
			if (!this->m_pMesh->is_boundary(vvh[1]) || this->m_pMesh->is_boundary(eh) || mub > 1.0 - this->m_boundaryEpsiScaled)
			{
				if (find(this->m_vconflictNode.begin(), this->m_vconflictNode.end(), vid1) == this->m_vconflictNode.end())
				{
					remeshOP = this->moveMeshVertex(vid1, x0*(1 - mua) + x1*mua);
					remeshed = remeshOP.m_OK;
				}
			}
		}

		if (!remeshed)
		{
			// Change topology to account for splitted edge, then 
			// move the point to its expectes position on the line
			// segment

			RemeshOP splitOP = this->splitEdge(eid);
			if (splitOP.m_OK)
			{
				// Assert only one vertex has been added
				assert((int)splitOP.m_vAddVert.size() == 1);

				remeshOP = this->appendOperations(splitOP, this->moveMeshVertex(splitOP.m_vAddVert[0], x0*(1 - mua) + x1*mua));

				remeshed = remeshOP.m_OK;
			}
		}
	}

	if (remeshed)
		return remeshOP;
	else return this->createOperation();
}

RemeshOP SurfaceRemesherContour::moveMeshVertex(const MeshTraits::ID& vid, const Vector3d& p)
{
	RemeshOP OP = this->createOperation();

	TriMesh::VertexHandle vh = this->m_pMesh->getVertHandle(vid);

	//const Vector3d& v = this->m_pMesh->getPoint(vh);

	//vector<TriMesh::VertexHandle> vvvh;
	//m_pMesh->getVertexVertices(vh, vvvh);
	//int conflict = 0;
	//for (int i = 0; i < (int)vvvh.size(); ++i)
	//{
	//	MeshTraits::ID vvid = this->m_pMesh->getVertID(vvvh[i]); // Check if there are conflictive verts
	//	if (find(this->m_vconflict.begin(), this->m_vconflict.end(), vvid) != this->m_vconflict.end())
	//		conflict++;
	//}

	//if (conflict >= 2)
	//{
	//	// Do not move, maybe OK if point distance is short
	//	OP.m_OK = (v - p).norm() < this->m_intersecEpsiScaled;
	//}
	//else
	//{
		// Move point for each property
		this->m_pMesh->setPoint(vh, p);
		const vector<string>& vps = m_pMesh->getProperties();
		for (int i = 0; i < this->m_pMesh->getNumProp(); ++i)
			this->m_pMesh->setPoint(vh, p, vps[i]); // For each

		OP.m_OK = true;
	//}

	//  Create OP

	if (OP.m_OK)
	{
		OP.m_vAddVert.push_back(vid);

		vector<TriMesh::EdgeHandle> veh;
		m_pMesh->getVertexEdges(vh, veh);
		for (int i = 0; i < (int)veh.size(); ++i)
		{
			if (!m_pMesh->status(veh[i]).deleted()) // Not deleted
				OP.m_vAddEdge.push_back(m_pMesh->getEdgeID(veh[i]));
		}
	}

	return OP;
}

int SurfaceRemesherContour::getContourNumber() const
{
	return (int) this->m_vrawContours.size();
}

void SurfaceRemesherContour::addContourCurve(const Curve& curve)
{
	this->m_vrawContours.push_back(curve);
}

const vector<MeshTraits::ID>& SurfaceRemesherContour::getContourVerts(int i) const
{
	assert(i >= 0 && i < (int) this->m_vVerts.size());

	return this->m_vVerts[i];
}

const vector<MeshTraits::ID>& SurfaceRemesherContour::getContourEdges(int i) const
{
	assert(i >= 0 && i < (int) this->m_vEdges.size());

	return this->m_vEdges[i];
}

void SurfaceRemesherContour::computeContourEdges(int i, vector<MeshTraits::ID>& vcontour) const
{
	assert(i >= 0 && i < (int)m_vrawContours.size());

	const vector<MeshTraits::ID>& vvertsIn = this->m_vVerts[i];
	const vector<MeshTraits::ID>& vedgesIn = this->m_remeshOP.m_vAddEdge;

	vcontour.clear();

	// Extract contour edges from the list of added edges

	int ne = (int)vedgesIn.size();
	for (int i = 0; i < ne; ++i)
	{
		const TriMesh::EdgeHandle& eh = m_pMesh->getEdgeHandle(vedgesIn[i]);

		vector<TriMesh::VertexHandle> vevh;
		this->m_pMesh->getEdgeVertices(eh, vevh);
		MeshTraits::ID veid0 = this->m_pMesh->getVertID(vevh[0]);
		MeshTraits::ID veid1 = this->m_pMesh->getVertID(vevh[1]);
		if (std::find(vvertsIn.begin(), vvertsIn.end(), veid0) != vvertsIn.end() &&
			std::find(vvertsIn.begin(), vvertsIn.end(), veid1) != vvertsIn.end())
			vcontour.push_back(vedgesIn[i]);
	}

	// Remove triangles

	map<int, iVector> mcontourFaces;

	int nc = (int)vcontour.size();
	for (int i = 0; i < nc; ++i)
	{
		TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(vcontour[i]);

		vector<TriMesh::FaceHandle> vfh;
		this->m_pMesh->getEdgeFaces(eh, vfh);

		int nf = (int)vfh.size();
		for (int f = 0; f < nf; ++f)
		{
			int fidx = vfh[f].idx(); // One or two faces per edge
			if (mcontourFaces.find(fidx) != mcontourFaces.end())
			{
				iVector& vedges = mcontourFaces[fidx];
				vedges.push_back(eh.idx()); // Add one
			}
			else
			{
				iVector vedges;
				vedges.push_back(eh.idx());
				mcontourFaces[fidx] = vedges;
			}
		}
	}

	for (map<int, iVector>::iterator f_it = mcontourFaces.begin(); f_it != mcontourFaces.end(); ++f_it)
	{
		iVector& vedges = f_it->second;

		sort(vedges.begin(), vedges.end()); // Remove the repeated edges
		vedges.erase(unique(vedges.begin(), vedges.end()), vedges.end());

		int ne = (int)vedges.size();

		if (ne > 1)
		{
			assert(ne == 3); // Full triangle

			int maxIndex = -1; // Longest
			double maxLength = -HUGE_VAL;
			for (int i = 0; i <ne; ++i)
			{
				TriMesh::EdgeHandle eh = this->m_pMesh->edge_handle(vedges[i]);

				vector<TriMesh::VertexHandle> vvh;
				this->m_pMesh->getEdgeVertices(eh, vvh);
				Vector3d a = this->m_pMesh->getPoint(vvh[0]);
				Vector3d b = this->m_pMesh->getPoint(vvh[1]);
				double norm = (a - b).norm();
				if (norm > maxLength)
				{
					maxLength = norm;
					maxIndex = vedges[i];
				}
			}

			// Remove others but bigger
			for (int i = 0; i < 3; ++i)
			{
				if (vedges[i] == maxIndex)
				{
					TriMesh::EdgeHandle eh = this->m_pMesh->edge_handle(vedges[i]); // Remove the longest
					vector<MeshTraits::ID>::iterator it = find(vcontour.begin(), vcontour.end(), this->m_pMesh->getEdgeID(eh));
					if (it == vcontour.end())
						logSimu("ERROR");
					else vcontour.erase(it);
					break;
				}
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////// DEPRECATED //////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////// 


//const vector<MeshTraits::ID>& SurfaceRemesherContour::getSortedContourVerts(int i) const
//{
//	assert(i >= 0 && i < (int) this->m_vSortVerts.size());
//
//	return this->m_vSortVerts[i];
//}
//
//const vector<MeshTraits::ID>& SurfaceRemesherContour::getSortedContourEdges(int i) const
//{
//	assert(i >= 0 && i < (int) this->m_vSortEdges.size());
//
//	return this->m_vSortEdges[i];
//}
//
//void SurfaceRemesherContour::getSortedContour(int i, const Curve& curveIn, vector<MeshTraits::ID>& vvertsOut, vector<MeshTraits::ID>& vedgesOut) const
//{
//	assert(i >= 0 && i < (int) this->m_vrawContours.size());
//
//	const vector<MeshTraits::ID>& vedges = this->m_vEdges[i];
//	const vector<MeshTraits::ID>& vverts = this->m_vVerts[i];
//
//	typedef struct Trait
//	{
//		MeshTraits::ID m_id;
//		double m_parameter;
//	} 
//	ComparableVert;
//
//	typedef struct LT_Trait
//	{
//		bool operator()(Trait t0, Trait t1)
//		{
//			return t0.m_parameter < t1.m_parameter;
//		}
//	} vert_LE;
//
//	int nv = (int)vverts.size();
//	int ne = (int)vedges.size();
//
//	// Sort contour vertices
//	vector<Trait> vvertsSorted;
//
//	for (int i = 0; i < nv; ++i)
//	{
//		Vector3d v = this->m_pMesh->getPoint(this->m_pMesh->getVertHandle(vverts[i]));
//	
//		Real t;
//		Vector3d p;
//		curveIn.projectPoint(v, p, t);
//	
//		Trait trait;
//		trait.m_parameter = t;
//		trait.m_id = vverts[i];
//
//		vvertsSorted.push_back(trait);
//	}
//
//	sort(vvertsSorted.begin(), vvertsSorted.end(), LT_Trait());
//
//	for (int i = 0; i < nv; ++i) // Copy vertices
//		vvertsOut.push_back(vvertsSorted[i].m_id);
//
//	// Extract sorted contour edges
//	for (int i = 0; i < nv - 1; ++i)
//	{
//		MeshTraits::ID id0 = vvertsOut[i];
//		MeshTraits::ID id1 = vvertsOut[i+1];
//		TriMesh::VertexHandle vh0 = this->m_pMesh->getVertHandle(id0);
//		TriMesh::VertexHandle vh1 = this->m_pMesh->getVertHandle(id1);
//		
//		// Look for edge
//		for (int j = 0; j < ne; ++j)
//		{
//			TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(vedges[j]);
//
//			vector<TriMesh::VertexHandle> vvh;
//			this->m_pMesh->getEdgeVertices(eh, vvh);
//			if ((vh0 == vvh[0] && vh1 == vvh[1]) ||
//				(vh1 == vvh[0] && vh0 == vvh[1]))
//			{
//				vedgesOut.push_back(vedges[j]);
//				break; // Found the missing edge
//			}
//		}
//	}
//}
//
//void SurfaceRemesherContour::computeSortedContour(const vector<MeshTraits::ID>& vvertsIn, const vector<MeshTraits::ID>& vedgesIn, vector<MeshTraits::ID>& vcontVerts, vector<MeshTraits::ID>& vcontEdges) const
//{
//	vcontVerts.clear();
//	vcontEdges.clear();
//
//	int iniEdge = -1;
//	for (int i = 0; i < (int)vedgesIn.size(); ++i)
//	{
//		const MeshTraits::ID& eid = vedgesIn[i];
//		TriMesh::EdgeHandle eh = this->m_pMesh->getEdgeHandle(eid);
//
//		vector<TriMesh::VertexHandle> vevh;
//		this->m_pMesh->getEdgeVertices(eh, vevh);
//		if (find(vvertsIn.begin(), vvertsIn.end(), this->m_pMesh->getVertID(vevh[0])) != vvertsIn.end() &&
//			find(vvertsIn.begin(), vvertsIn.end(), this->m_pMesh->getVertID(vevh[1])) != vvertsIn.end())
//		{
//			// Found initial
//			iniEdge = i;
//			break;
//		}
//	}
//
//	if(iniEdge == -1)
//	{
//		// Weeeeird
//		assert(false);
//	}
//
//	set<MeshTraits::ID> setEdgeAdded;
//	set<MeshTraits::ID> setVertAdded;
//
//	TriMesh::EdgeHandle inieh = this->m_pMesh->getEdgeHandle(vedgesIn[iniEdge]);
//
//	vector<TriMesh::VertexHandle> inivevh;
//	m_pMesh->getEdgeVertices(inieh, inivevh);
//	
//	MeshTraits::ID inieid = this->m_pMesh->getEdgeID(inieh);
//	const MeshTraits::ID& inivid0 = this->m_pMesh->getVertID(inivevh[0]);
//	const MeshTraits::ID& inivid1 = this->m_pMesh->getVertID(inivevh[1]);
//
//	vcontVerts.push_back(inivid0);
//	vcontVerts.push_back(inivid1);
//	vcontEdges.push_back(inieid);
//
//	setVertAdded.insert(inivid0);
//	setVertAdded.insert(inivid1);
//	setEdgeAdded.insert(inieid);
//
//	bool goFro = true;
//	bool goEnd = true;
//
//	while (goFro || goEnd)
//	{
//		// Grow front
//
//		if (goFro)
//		{
//			const MeshTraits::ID& vid0 = vcontVerts.front(); // Grow front extreme
//			const TriMesh::VertexHandle& vh0 = this->m_pMesh->getVertHandle(vid0);
//			vector<TriMesh::EdgeHandle> veh;
//			m_pMesh->getVertexEdges(vh0, veh);
//
//			goFro = false;
//			TriMesh::EdgeHandle nextEdge;
//			TriMesh::VertexHandle nextVert;
//
//			int ne = (int)veh.size();
//			for (int e = 0; e < ne; ++e)
//			{
//				const MeshTraits::ID& eid = this->m_pMesh->getEdgeID(veh[e]);
//				if (setEdgeAdded.find(eid) != setEdgeAdded.end()) // Added
//					continue; // Ignore
//
//				vector<TriMesh::VertexHandle> vevh;
//				m_pMesh->getEdgeVertices(veh[e], vevh);
//
//				TriMesh::VertexHandle vh1;
//				if (vevh[0] == vh0) vh1 = vevh[1];
//				else if (vevh[1] == vh0) vh1 = vevh[0];
//				else assert(false);
//				if (find(vvertsIn.begin(), vvertsIn.end(), this->m_pMesh->getVertID(vh1)) != vvertsIn.end())
//				{
//					goFro = true;
//					nextVert = vh1;
//					nextEdge = veh[e];
//				}
//			}
//
//			if (goFro)
//			{
//				MeshTraits::ID vid = this->m_pMesh->getVertID(nextVert);
//				MeshTraits::ID eid = this->m_pMesh->getEdgeID(nextEdge);
//
//				if (setVertAdded.find(vid) == setVertAdded.end())
//				{
//					vcontVerts.insert(vcontVerts.begin(), vid);
//					setVertAdded.insert(vid); // Add just once
//				}
//
//				if (setEdgeAdded.find(eid) == setEdgeAdded.end())
//				{
//					vcontEdges.insert(vcontEdges.begin(), eid);
//					setEdgeAdded.insert(eid); // Add just once
//				}
//			}
//		}
//
//		// Grow end
//
//		if (goEnd)
//		{
//			const MeshTraits::ID& vid0 = vcontVerts.back(); // Grow front extreme
//			const TriMesh::VertexHandle& vh0 = this->m_pMesh->getVertHandle(vid0);
//			vector<TriMesh::EdgeHandle> veh;
//			m_pMesh->getVertexEdges(vh0, veh);
//
//			goEnd = false;
//			TriMesh::EdgeHandle nextEdge;
//			TriMesh::VertexHandle nextVert;
//
//			int ne = (int)veh.size();
//			for (int e = 0; e < ne; ++e)
//			{
//				const MeshTraits::ID& eid = this->m_pMesh->getEdgeID(veh[e]);
//				if (setEdgeAdded.find(eid) != setEdgeAdded.end()) // Added
//					continue; // Ignore
//
//				vector<TriMesh::VertexHandle> vevh;
//				m_pMesh->getEdgeVertices(veh[e], vevh);
//
//				TriMesh::VertexHandle vh1;
//				if (vevh[0] == vh0) vh1 = vevh[1];
//				else if (vevh[1] == vh0) vh1 = vevh[0];
//				else assert(false);
//
//				const MeshTraits::ID& vid1 = this->m_pMesh->getVertID(vh1);
//				if (find(vvertsIn.begin(), vvertsIn.end(), vid1) != vvertsIn.end())
//				{
//					goEnd = true;
//					nextVert = vh1;
//					nextEdge = veh[e];
//				}
//			}
//
//			if (goEnd)
//			{
//				MeshTraits::ID vid = this->m_pMesh->getVertID(nextVert);
//				MeshTraits::ID eid = this->m_pMesh->getEdgeID(nextEdge);
//
//				if (setVertAdded.find(vid) == setVertAdded.end())
//				{
//					vcontVerts.insert(vcontVerts.end(), vid);
//					setVertAdded.insert(vid); // Add just once
//				}
//
//				if (setEdgeAdded.find(eid) == setEdgeAdded.end())
//				{
//					vcontEdges.insert(vcontEdges.end(), eid);
//					setEdgeAdded.insert(eid); // Add just once
//				}
//			}
//		}
//	}
//}

//const RemeshOP& SurfaceRemesherContour::remesh(const TriMesh* pMesh, const BVHTree* pTree)
//{
//	assert(pMesh != NULL);
//
//	// Copy mesh to remesh and build tree
//
//	this->m_pMesh = new TriMesh(*pMesh); 
//
//	if (pTree != NULL)
//		this->m_pTree = new BVHTree(*pTree);
//	else
//	{
//		BVHTree* aTree = new BVHTree(0);
//		aTree->initializeMeshEdge(m_pMesh);
//		this->m_pTree = aTree; 
//	}
//
//	// Set intersection epsilon depending on the size
//
//	dVector bbRanges = this->m_pMesh->getAABBRanges();
//	double max = -HUGE_VAL;
//	for (int i = 0; i < 3; ++i)
//		if (bbRanges[i] > max)
//			max = bbRanges[i];
//	this->m_interEPS = 1e-6*max;
//
//	// Create remesh operation
//
//	this->m_remeshOP = this->createOperation();
//
//	// Project curves over surface
//
//	int nl = (int) this->m_vrawContours.size();
//	this->m_vedges.resize(nl); // Output edges
//	this->m_vSortEdges.resize(nl); // Sorted output edges
//	this->m_vSortVerts.resize(nl); // Sorted output verts
//
//	vector<vector<MeshTraits::ID>> vlineNewVerts(nl);
//
//	for (int i = 0; i < nl; ++i)
//	{
//		this->m_vconflict.clear(); // Restart list
//
//		RemeshOP lineOP = this->createOperation();
//
//		int np = (int) this->m_vrawContours[i].size();
//
//		if (np < 2) continue; // Invalid contour
//
//		//// Extremes
//
//		//int minIdx;
//		//double minNorm;
//		//Vector3d p0 = m_vrawContours[i].front();
//		//Vector3d p1 = m_vrawContours[i].back();
//		//if ((p1 - p0).norm() > this->m_interEPS)
//		//{
//		//	minIdx = -1;
//		//	minNorm = HUGE_VAL;
//
//		//	for (int v = 0; v < this->m_pMesh->n_vertices(); ++v)
//		//	{
//		//		Vector3d mp = this->m_pMesh->getPoint(this->m_pMesh->vertex_handle(v));
//		//		double distance = (p0 - mp).norm(); // Get closest point to the extremes
//		//		if (distance < minNorm)
//		//		{
//		//			minNorm = distance;
//		//			minIdx = v; // Close
//		//		}
//		//	}
//
//		//	TriMesh::VertexHandle vh0 = this->m_pMesh->vertex_handle(minIdx);
//		//	this->m_pMesh->setPoint(vh0, p0);
//		//	vector<TriMesh::EdgeHandle> veh0;
//		//	this->m_pMesh->getVertexEdges(vh0, veh0);
//		//	for (int e = 0; e < (int)veh0.size(); ++e)
//		//		lineOP.m_vAddEdge.push_back(this->m_pMesh->getEdgeID(veh0[e]));
//		//	lineOP.m_vAddVert.push_back(this->m_pMesh->getVertID(vh0)); // Trace
//
//		//	minIdx = -1;
//		//	minNorm = HUGE_VAL;
//
//		//	for (int v = 0; v < this->m_pMesh->n_vertices(); ++v)
//		//	{
//		//		Vector3d mp = this->m_pMesh->getPoint(this->m_pMesh->vertex_handle(v));
//		//		double distance = (p1 - mp).norm();
//		//		if (distance < minNorm)
//		//		{
//		//			minNorm = distance;
//		//			minIdx = v; // Close
//		//		}
//		//	}
//
//		//	TriMesh::VertexHandle vh1 = this->m_pMesh->vertex_handle(minIdx);
//		//	this->m_pMesh->setPoint(vh1, p1);
//		//	vector<TriMesh::EdgeHandle> veh1;
//		//	this->m_pMesh->getVertexEdges(vh1, veh1);
//		//	for (int e = 0; e < (int)veh1.size(); ++e)
//		//		lineOP.m_vAddEdge.push_back(this->m_pMesh->getEdgeID(veh1[e]));
//		//	lineOP.m_vAddVert.push_back(this->m_pMesh->getVertID(vh1)); // Trace
//		//}
//
//		BVHTree* pLineTree = new BVHTree(i + 1);
//		pLineTree->initializeCurve(m_vrawContours[i]);
//
//		// Find potential edge-edge collisions
//
//		vector<BVHClosePrimitivePair> vcpps; // Close primitive pairs
//		BVHTree::testRecursive(this->m_pTree, pLineTree, 0, 0, vcpps);
//
//		for (int j = 0; j < (int) vcpps.size(); ++j)
//		{
//			// Mesh edge
//
//			TriMesh::EdgeHandle eh = this->m_pMesh->edge_handle(vcpps[j].m_prim0.m_index);
//
//			if (this->m_pMesh->status(eh).deleted())
//				continue; // Has been deleted before!
//
//			MeshTraits::ID eid = this->m_pMesh->getEdgeID(eh);
//
//			vector<TriMesh::VertexHandle> vevh;
//			m_pMesh->getEdgeVertices(eh, vevh);
//			assert(vevh[0].idx() == vcpps[j].m_prim0.m_vpidx[0]);
//			assert(vevh[1].idx() == vcpps[j].m_prim0.m_vpidx[1]); 
//
//			if (std::find(lineOP.m_vAddEdge.begin(), lineOP.m_vAddEdge.end(), eid) != lineOP.m_vAddEdge.end())
//				continue;
//
//			// Line segment
//			 
//			Vector3d p0 = m_vrawContours[i][vcpps[j].m_prim1.m_vpidx[0]];
//			Vector3d p1 = m_vrawContours[i][vcpps[j].m_prim1.m_vpidx[1]];
//
//			RemeshOP pairOP = this->projectSegmentToEdge(eid, p0, p1);
//			if (!pairOP.m_OK)
//				break;
//
//			lineOP = this->appendOperations(lineOP, pairOP);
//		}
//
//		if (!lineOP.m_OK)
//			break;
//
//		// Garbage collection before continuing 
//
//		this->m_pMesh->garbage_collection();
//		this->m_pMesh->updateIdentifiersMap();
//		
//		vlineNewVerts[i] = lineOP.m_vAddVert; // Store vertices
//		m_remeshOP = this->appendOperations(m_remeshOP, lineOP);
//
//		this->getSortedContour(lineOP.m_vAddVert, this->m_vSortVerts[i], this->m_vSortEdges[i]);
//
//		// Reconstruct mesh BVH tree after the changes 
//
//		delete this->m_pTree;
//		this->m_pTree = new BVHTree(0);
//		this->m_pTree->initializeMeshEdge(this->m_pMesh);
//	}
//
//	//for (int i = 0; i < nl; ++i) // Extract contour edges from the remeshing operation and line verts
//	//	this->getContourEdges(vlineNewVerts[i], this->m_remeshOP.m_vAddEdge, this->m_vedges[i]);
//
//	if (!this->m_remeshOP.m_OK)
//	{  
//		this->freeMemory(); // Invalid
//	}
//	else
//	{ 
//		// Prepare output
//	}
//
//	return this->m_remeshOP;
//}

//void SurfaceRemesherContour::computeSortedContour(const vector<MeshTraits::ID>& vvertsIn, vector<MeshTraits::ID>& vcontVerts, vector<MeshTraits::ID>& vcontEdges) const
//{
//	vcontVerts.clear();
//	vcontEdges.clear();
//
//	set<MeshTraits::ID> setEdgeAdded;
//	set<MeshTraits::ID> setVertAdded;
//
//	int nv = (int)vvertsIn.size();
//	vcontVerts.push_back(vvertsIn[nv / 2]);
//
//	bool goFro = true;
//	bool goEnd = true;
//
//	while (goFro || goEnd)
//	{
//		// Grow front
//
//		if (goFro)
//		{
//			const MeshTraits::ID& vid0 = vcontVerts.front(); // Grow front extreme
//			const TriMesh::VertexHandle& vh0 = this->m_pMesh->getVertHandle(vid0);
//			vector<TriMesh::EdgeHandle> veh;
//			m_pMesh->getVertexEdges(vh0, veh);
//
//			goFro = false;
//			double minLength = HUGE_VAL;
//			TriMesh::EdgeHandle nextEdge;
//			TriMesh::VertexHandle nextVert;
//
//			int ne = (int)veh.size();
//			for (int e = 0; e < ne; ++e)
//			{
//				const MeshTraits::ID& eid = this->m_pMesh->getEdgeID(veh[e]);
//				if (setEdgeAdded.find(eid) != setEdgeAdded.end()) // Added
//					continue; // Ignore
//
//				vector<TriMesh::VertexHandle> vevh;
//				m_pMesh->getEdgeVertices(veh[e], vevh);
//
//				TriMesh::VertexHandle vh1;
//				if (vevh[0] == vh0) vh1 = vevh[1];
//				else if (vevh[1] == vh0) vh1 = vevh[0];
//				else assert(false);
//
//				const MeshTraits::ID& vid1 = this->m_pMesh->getVertID(vh1);
//				if (find(vvertsIn.begin(), vvertsIn.end(), vid1) != vvertsIn.end())
//				{
//					goFro = true;
//					Vector3d p0 = this->m_pMesh->getPoint(vh0);
//					Vector3d p1 = this->m_pMesh->getPoint(vh1);
//					double dist = (p0 - p1).norm();
//					if (dist < minLength)
//					{
//						nextVert = vh1;
//						nextEdge = veh[e];
//						minLength = dist;
//					}
//				}
//			}
//
//			if (goFro)
//			{
//				MeshTraits::ID vid = this->m_pMesh->getVertID(nextVert);
//				MeshTraits::ID eid = this->m_pMesh->getEdgeID(nextEdge);
//
//				if (setVertAdded.find(vid) == setVertAdded.end())
//				{
//					vcontVerts.insert(vcontVerts.begin(), vid);
//					setVertAdded.insert(vid); // Add just once
//				}
//
//				if (setEdgeAdded.find(eid) == setEdgeAdded.end())
//				{
//					vcontEdges.insert(vcontEdges.begin(), eid);
//					setEdgeAdded.insert(eid); // Add just once
//				}
//			}
//		}
//
//		// Grow end
//
//		if (goEnd)
//		{
//			const MeshTraits::ID& vid0 = vcontVerts.back(); // Grow front extreme
//			const TriMesh::VertexHandle& vh0 = this->m_pMesh->getVertHandle(vid0);
//			vector<TriMesh::EdgeHandle> veh;
//			m_pMesh->getVertexEdges(vh0, veh);
//
//			goEnd = false;
//			double minLength = HUGE_VAL;
//			TriMesh::EdgeHandle nextEdge;
//			TriMesh::VertexHandle nextVert;
//
//			int ne = (int)veh.size();
//			for (int e = 0; e < ne; ++e)
//			{
//				const MeshTraits::ID& eid = this->m_pMesh->getEdgeID(veh[e]);
//				if (setEdgeAdded.find(eid) != setEdgeAdded.end()) // Added
//					continue; // Ignore
//
//				vector<TriMesh::VertexHandle> vevh;
//				m_pMesh->getEdgeVertices(veh[e], vevh);
//
//				TriMesh::VertexHandle vh1;
//				if (vevh[0] == vh0) vh1 = vevh[1];
//				else if (vevh[1] == vh0) vh1 = vevh[0];
//				else assert(false);
//
//				const MeshTraits::ID& vid1 = this->m_pMesh->getVertID(vh1);
//				if (find(vvertsIn.begin(), vvertsIn.end(), vid1) != vvertsIn.end())
//				{
//					goEnd = true;
//					Vector3d p0 = this->m_pMesh->getPoint(vh0);
//					Vector3d p1 = this->m_pMesh->getPoint(vh1);
//					double dist = (p0 - p1).norm();
//					if (dist < minLength)
//					{
//						nextVert = vh1;
//						nextEdge = veh[e];
//						minLength = dist;
//					}
//				}
//			}
//
//			if (goEnd)
//			{
//				MeshTraits::ID vid = this->m_pMesh->getVertID(nextVert);
//				MeshTraits::ID eid = this->m_pMesh->getEdgeID(nextEdge);
//
//				if (setVertAdded.find(vid) == setVertAdded.end())
//				{
//					vcontVerts.insert(vcontVerts.end(), vid);
//					setVertAdded.insert(vid); // Add just once
//				}
//
//				if (setEdgeAdded.find(eid) == setEdgeAdded.end())
//				{
//					vcontEdges.insert(vcontEdges.end(), eid);
//					setEdgeAdded.insert(eid); // Add just once
//				}
//			}
//		}
//	}
////}
//
//void SurfaceRemesherContour::computeContourVerts(const vector<MeshTraits::ID>& vvertsIn,
//	const vector<MeshTraits::ID>& vedgesIn,
//	vector<MeshTraits::ID>& vcontour) const
//{
//	//vcontour = vvertsIn;
//}
