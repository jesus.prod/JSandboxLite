/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_RodMeshRest.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_EdgeLength.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_RodMeshRest.h>

ParameterSet_Tensile_RodMeshRest::ParameterSet_Tensile_RodMeshRest(int midx) : ParameterSet("TENRODMESHREST")
{
	assert(midx >= 0);
	this->m_midx = midx;
	this->m_is2D = false;
}

ParameterSet_Tensile_RodMeshRest::~ParameterSet_Tensile_RodMeshRest()
{
	// Nothing to do here
}

void ParameterSet_Tensile_RodMeshRest::getParameters(VectorXd& vp) const
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	VectorXd vpSplit;
	pModel->getParam_VertexRest(vpSplit);
	const RodMesh& rodMesh = pModel->getSimMesh_0();
	const vector<iVector>& vmapWholeToSplit = rodMesh.getWholeToSplitMap();
	int nvWhole = (int)vmapWholeToSplit.size();

	if (!this->m_is2D)
	{
		vp.resize(3 * nvWhole);
		for (int i = 0; i < nvWhole; ++i)
		{
			int s = vmapWholeToSplit[i][0];
			int offsetWhole = 3 * i;
			int offsetSplit = 3 * s;
			vp(offsetWhole + 0) = vpSplit(offsetSplit + 0);
			vp(offsetWhole + 1) = vpSplit(offsetSplit + 1);
			vp(offsetWhole + 2) = vpSplit(offsetSplit + 2);
		}
	}
	else
	{
		vp.resize(2 * nvWhole);
		for (int i = 0; i < nvWhole; ++i)
		{
			int s = vmapWholeToSplit[i][0];
			int offsetWhole = 2 * i;
			int offsetSplit = 3 * s;
			vp(offsetWhole + 0) = vpSplit(offsetSplit + 0);
			vp(offsetWhole + 1) = vpSplit(offsetSplit + 2);
		}
	}

	assert((int)vp.size() == this->m_np);
}

void ParameterSet_Tensile_RodMeshRest::setParameters(const VectorXd& vp)
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	assert((int)vp.size() == this->m_np);

	VectorXd vpWhole = vp;
	this->limitParameters(vpWhole);
	const RodMesh& rodMesh = pModel->getSimMesh_0();
	const vector<iVector>& vmapSplitToWhole = rodMesh.getSplitToWholeMap();
	int nvSplit = (int) vmapSplitToWhole.size();

	VectorXd vpSplit;

	if (!this->m_is2D)
	{
		vpSplit.resize(3 * nvSplit);
		for (int i = 0; i < nvSplit; ++i)
		{
			int w = vmapSplitToWhole[i][0];
			int offsetWhole = 3 * w;
			int offsetSplit = 3 * i;
			vpSplit(offsetSplit + 0) = vp(offsetWhole + 0);
			vpSplit(offsetSplit + 1) = vp(offsetWhole + 1);
			vpSplit(offsetSplit + 2) = vp(offsetWhole + 2);
		}
	}
	else
	{
		vpSplit.resize(3 * nvSplit);
		for (int i = 0; i < nvSplit; ++i)
		{
			int w = vmapSplitToWhole[i][0];
			int offsetWhole = 2 * w;
			int offsetSplit = 3 * i;
			vpSplit(offsetSplit + 0) = vp(offsetWhole + 0);
			vpSplit(offsetSplit + 2) = vp(offsetWhole + 1);
			vpSplit(offsetSplit + 1) = 0;
		}
	}

	pModel->setParam_VertexRest(vpSplit);
}

bool ParameterSet_Tensile_RodMeshRest::isReady_fp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_f0v();
}

bool ParameterSet_Tensile_RodMeshRest::isReady_DfDp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_DfxD0v();
}

void ParameterSet_Tensile_RodMeshRest::get_fp(VectorXd& vfv0) const
{
	throw new exception("Not implemented yet...");

	//vfv0.resize(this->m_np);
	//vfv0.setZero(); // Init.

	//this->m_pTSModel->getRodMeshModel(this->m_midx)->add_fv0(vfv0);

	//// TODO

	//throw new exception("Not implemented yet");
}

void ParameterSet_Tensile_RodMeshRest::get_DfDp(tVector& vDfxDv0_t) const
{
	RodMeshModel* pModel = this->m_pTSModel->getRodMeshModel(this->m_midx);

	const vector<iVector>& vmapSplit2Whole = pModel->getSimMesh_x().getSplitToWholeMap();

	if (!this->m_is2D)
	{
		int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);

		int nCoef = pModel->getNumNonZeros_DfxD0v();

		tVector vDfxDv0_split;
		tVector vDfxDv0_whole;
		vDfxDv0_split.reserve(nCoef);
		vDfxDv0_whole.reserve(nCoef);
		pModel->add_DfxD0v(vDfxDv0_split);

		for (int i = 0; i < nCoef; ++i)
		{
			const Triplet<Real>& t = vDfxDv0_split[i];
			
			int colSpl = t.col();
			int col = 3 * vmapSplit2Whole[colSpl / 3][0] + (colSpl % 3);
			vDfxDv0_whole.push_back(Triplet<Real>(t.row(), col, t.value()));
		}

		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfxDv0_whole, vDfxDv0_t);
	}
	else
	{
		int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);

		int nCoef = pModel->getNumNonZeros_DfxD0v();

		tVector vDfxDv0_3D;
		tVector vDfxDv0_2D;
		vDfxDv0_3D.reserve(nCoef);
		vDfxDv0_2D.reserve(nCoef);
		pModel->add_DfxD0v(vDfxDv0_3D);

		for (int i = 0; i < nCoef; ++i)
		{
			const Triplet<Real>& t = vDfxDv0_3D[i];
			int colSpl = t.col();
			int mod = colSpl % 3;
			int div = vmapSplit2Whole[colSpl / 3][0];
			if (mod == 0) vDfxDv0_2D.push_back(Triplet<Real>(t.row(), 2 * div + 0, t.value()));
			if (mod == 2) vDfxDv0_2D.push_back(Triplet<Real>(t.row(), 2 * div + 1, t.value()));
		}

		this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfxDv0_2D, vDfxDv0_t);
	}
}

void ParameterSet_Tensile_RodMeshRest::setup()
{
	assert(this->m_pModel != NULL);

	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);

	if (!this->m_is2D)
		this->m_np = 3 * (int) this->m_pTSModel->getRodMeshModel(this->m_midx)->getSimMesh_0().getWholeToSplitMap().size();
	else this->m_np = 2 * (int) this->m_pTSModel->getRodMeshModel(this->m_midx)->getSimMesh_0().getWholeToSplitMap().size();

	this->updateBoundsVector();
}