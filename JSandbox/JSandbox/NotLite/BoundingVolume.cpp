/*=====================================================================================*/
/*!
\file		BoundingVolume.cpp
\author		jesusprod,bthomasz
\brief		Implementation of BoundingVolume.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/BoundingVolume.h>

BoundingVolume::BoundingVolume(void)
{
	m_empty = true;
}

BoundingVolume::~BoundingVolume(void)
{
	// Nothing to do here
}
