/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_SurfaceRest.h
\author		jesusprod
\brief		Parameter set for the surface rest state stretch in a tensile structure.
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_TENSILE_SURFACESTRETCH_H
#define PARAMETER_SET_TENSILE_SURFACESTRETCH_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/Model/TensileStructureModel.h>

class JSANDBOX_EXPORT ParameterSet_Tensile_SurfaceStretch : public ParameterSet
{
public:
	ParameterSet_Tensile_SurfaceStretch(int midx);

	virtual ~ParameterSet_Tensile_SurfaceStretch();

	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

protected:

	int m_midx;

	SurfaceModel* m_pSurfModel;

	TensileStructureModel* m_pTSModel;

};

#endif