/*=====================================================================================*/
/*!
\file		CSTElementNH.h
\author		jesusprod
\brief		Constant strain triangle element with Neo-Hookean constitutive model.
*/
/*=====================================================================================*/

#ifndef CST_ELEMENT_NH_H
#define CST_ELEMENT_NH_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Element/CSTElement.h>

class CSTElementNH : public CSTElement
{
public:
	CSTElementNH(int dim0);
	virtual ~CSTElementNH();

	virtual void update_Energy(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Force(const VectorXd& vx, const VectorXd& vv);
	virtual void update_Jacobian(const VectorXd& vx, const VectorXd& vv);

	virtual void update_DfxDx(const VectorXd& vx, const VectorXd& vX);
	virtual void update_DfxD0(const VectorXd& vx, const VectorXd& vX);

};

#endif