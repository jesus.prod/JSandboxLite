/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_RodsLength.cpp
\author		jesusprod
\brief		Implementation of ParameterSet_Tensile_RodsLength.h
*/
/*=====================================================================================*/

#include <JSandbox/Optim/ParameterSet_Tensile_RodsLength.h>

ParameterSet_Tensile_RodsLength::ParameterSet_Tensile_RodsLength(int midx) : ParameterSet("TENRODSLENGTH")
{
	assert(midx >= 0);
	this->m_midx = midx;
}

ParameterSet_Tensile_RodsLength::~ParameterSet_Tensile_RodsLength()
{
	// Nothing to do here
}

void ParameterSet_Tensile_RodsLength::getParameters(VectorXd& vp) const
{
	this->m_pTSModel->getRodMeshModel(this->m_midx)->getParam_LengthRod(vp);

	assert((int)vp.size() == this->m_np);
}

void ParameterSet_Tensile_RodsLength::setParameters(const VectorXd& vp)
{
	assert((int)vp.size() == this->m_np);

	VectorXd vpB = vp;
	this->limitParameters(vpB);
	this->m_pTSModel->getRodMeshModel(this->m_midx)->setParam_LengthRod(vpB);
}

bool ParameterSet_Tensile_RodsLength::isReady_fp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_flr();
}

bool ParameterSet_Tensile_RodsLength::isReady_DfDp() const
{
	return this->m_pTSModel->getRodMeshModel(this->m_midx)->isReady_DfxDlr();
}

void ParameterSet_Tensile_RodsLength::get_fp(VectorXd& vfp) const
{
	this->m_pTSModel->getRodMeshModel(this->m_midx)->add_flr(vfp);
}

void ParameterSet_Tensile_RodsLength::get_DfDp(tVector& vDfDp_t) const
{
	tVector vDfDp_i;
	vDfDp_i.reserve(this->m_pTSModel->getRodMeshModel(this->m_midx)->getNumNonZeros_DfxDlr());
	vDfDp_t.reserve(this->m_pTSModel->getRodMeshModel(this->m_midx)->getNumNonZeros_DfxDlr());
	this->m_pTSModel->getRodMeshModel(this->m_midx)->add_DfxDlr(vDfDp_i);
	int modelIdx = this->m_pTSModel->getRodMeshModelOffset(this->m_midx);
	this->m_pTSModel->addLocal2Global_Row(modelIdx, vDfDp_i, vDfDp_t);
}

void ParameterSet_Tensile_RodsLength::setup()
{
	assert(this->m_pModel != NULL);
	this->m_pTSModel = dynamic_cast<TensileStructureModel*>(this->m_pModel);
	this->m_np = (int) this->m_pTSModel->getRodMeshModel(this->m_midx)->getSimMesh_0().getNumRod();

	this->updateBoundsVector();
}