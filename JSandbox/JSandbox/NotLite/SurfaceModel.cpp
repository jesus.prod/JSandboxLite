/*=====================================================================================*/
/*!
\file		SurfaceModel.cpp
\author		jesusprod
\brief		Implementation of SurfaceModel.h
*/
/*=====================================================================================*/

#include <JSandbox/Model/SurfaceModel.h>

#include <JSandbox/Element/DSAreaElement.h>
#include <JSandbox/Element/DSEdgeElement.h>
#include <JSandbox/Element/DSHingeElement.h>
#include <JSandbox/Element/CSTElementStVKTrueOrthotropic.h>
#include <JSandbox/Element/CSTElementStVKOrthotropic.h>
#include <JSandbox/Element/CSTElementStVKIsotropic.h>
#include <JSandbox/Element/CSTElementNH.h>
#include <JSandbox/Element/CSTElementMR.h>

#include <JSandbox/Optim/AlgLib_Sparse_BoxC_QP.h>
#include <JSandbox/Optim/AlgLib_Sparse_EQIN_QP.h>
#include <JSandbox/Optim/Mosek_Sparse_EQIN_QP.h>

#include <omp.h> // MP support

SurfaceModel::SurfaceModel() : NodalSolidModel("DS")
{
	this->m_pSimMesh = NULL;

	this->m_isDummy = false;

	this->deprecateConfiguration();
}

SurfaceModel::~SurfaceModel()
{
	freeMesh();

	logSimu("[TRACE] Destroying SurfaceModel: %d", this);
}

void SurfaceModel::configureModel(SurfaceModel::Model DM)
{
	this->m_DM = DM;

	this->deprecateConfiguration();
}

void SurfaceModel::configureMesh(const TriMesh& pMesh)
{
	// There is a 3D position of the vertices and
	// a 3D position for the mesh initial state
	assert(pMesh.hasProperty(VProperty::POSE));
	assert(pMesh.hasProperty(VProperty::POS0));

	// Perform a deep clone of the given 
	// mesh. Maybe this is not the best
	// idea but it feels safer by now.

	this->freeMesh(); // Reinitialization
	this->m_pSimMesh = new TriMesh(pMesh);

	this->deprecateConfiguration();
}

void SurfaceModel::setup()
{
	int numv = (int) this->m_pSimMesh->getNumNode();

	// DOF

	this->m_numDim_x = 3;
	this->m_numDim_0 = 3;

	this->m_numDimRaw = 3;

	this->m_numNodeSim = numv;
	this->m_numNodeRaw = numv;

	this->m_nsimDOF_x = this->m_nsimDOF_0 = this->m_nrawDOF = 3 * numv;
	
	dVector vXSTL(this->m_nrawDOF);
	dVector vxSTL(this->m_nrawDOF);
	this->m_pSimMesh->getPoints(vXSTL, VProperty::POS0);
	this->m_pSimMesh->getPoints(vxSTL, VProperty::POSE);
	toEigen(vxSTL, this->m_state_x->m_vx);
	toEigen(vXSTL, this->m_state_0->m_vx);

	this->m_state_x->m_vv.resize(this->m_nrawDOF);
	this->m_state_0->m_vv.resize(this->m_nrawDOF);
	this->m_state_0->m_vv.setZero();
	this->m_state_x->m_vv.setZero();

	// Initialize elements

	this->freeElements();

	// Initialize stretch elements

	switch (this->m_DM)
	{
	case Model::QuadArea: 
		this->initializeElements_QuadArea(); 
		break;
	case Model::MS_QuadArea: 
		//this->initializeElements_QuadArea();
		this->initializeElements_MassSpring();
		break;
	case Model::DS_MS_QuadArea:
		//this->initializeElements_QuadArea();
		this->initializeElements_MassSpring();
		this->initializeElements_Bending();
		break;
	case Model::DS_CSTStretch:
		this->initializeElements_CSTStretch();
		this->initializeElements_Bending();
		break;
	}

	this->initializeMaterials();

	this->m_isReady_Setup = true;
	this->deprecateDiscretization();

	// Update simulation 
	this->update_SimDOF();

	logSimu("[TRACE] Initialized SurfaceModel. Raw: %d, Sim: %d", this->m_nrawDOF, this->m_nsimDOF_x);
}

void SurfaceModel::initializeElements_QuadArea()
{
	// Initialize area preserving elements

	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		NodalSolidElement* pEle = new DSAreaElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds.push_back((*fv_it).idx());
			++fv_it;
		}
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);

		// Add to elements vector
		this->m_vpEles.push_back(pEle);
		this->m_vpStretchEles.push_back(pEle);
	}
}

void SurfaceModel::initializeElements_MassSpring()
{
	// Initialize stretch mass-spring elements

	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());
	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		NodalSolidElement* pEle = new DSEdgeElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(2);
		TriMesh::EdgeHandle eh = *e_it;
		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(2);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);

		vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[0]).idx());
		vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[1]).idx());
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);

		// Add to elements vector
		this->m_vpEles.push_back(pEle);
		this->m_vpStretchEles.push_back(pEle);
	}
}

void SurfaceModel::initializeElements_CSTStretch()
{
	TriMesh::FaceIter f_it, f_end(m_pSimMesh->faces_end());
	for (f_it = m_pSimMesh->faces_begin(); f_it != f_end; ++f_it)
	{
		//CSTElement* pEle = new CSTElementStVKTrueOrthotropic(3);
		CSTElement* pEle = new CSTElementStVKOrthotropic(3);
		//CSTElement* pEle = new CSTElementStVKIsotropic(3);
		//CSTElement* pEle = new CSTElementNH(3);
		//CSTElement* pEle = new CSTElementMR(3);

		// Set indices
		vector<int> vinds0(3);
		TriMesh::FaceVertexIter fv_it = m_pSimMesh->fv_iter(*f_it);
		for (int v = 0; v < 3; ++v)
		{
			vinds0[v] = (*fv_it).idx();
			++fv_it;
		}
		pEle->setNodeIndicesx(vinds0);
		pEle->setNodeIndices0(vinds0);

		// Add element
		this->m_vpEles.push_back(pEle);
		this->m_vpStretchEles.push_back(pEle);
	}
}

void SurfaceModel::initializeElements_Bending()
{
	// Initialize discrete-shells bending elements

	TriMesh::EdgeIter e_it, e_end(m_pSimMesh->edges_end());
	for (e_it = m_pSimMesh->edges_begin(); e_it != e_end; ++e_it)
	{
		NodalSolidElement* pEle = new DSHingeElement(3);

		// Set indices
		vector<int> vinds;
		vinds.reserve(4);
		TriMesh::EdgeHandle eh = *e_it;
		if (m_pSimMesh->is_boundary(eh))
			continue;

		std::vector<TriMesh::HalfedgeHandle> vHalfEdges(4);
		vHalfEdges[0] = m_pSimMesh->halfedge_handle(eh, 1);
		vHalfEdges[1] = m_pSimMesh->halfedge_handle(eh, 0);
		vHalfEdges[2] = m_pSimMesh->next_halfedge_handle(vHalfEdges[1]);
		vHalfEdges[3] = m_pSimMesh->next_halfedge_handle(vHalfEdges[0]);

		for (int i = 0; i < 4; i++)
			vinds.push_back(m_pSimMesh->to_vertex_handle(vHalfEdges[i]).idx());
		pEle->setNodeIndicesx(vinds);
		pEle->setNodeIndices0(vinds);

		// Add to elements vector
		this->m_vpEles.push_back(pEle);
		this->m_vpBendingEles.push_back(pEle);
	}
}

void SurfaceModel::update_StateFromSimMesh()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.
	
	dVector vxSTL;
	dVector vXSTL;
	this->m_pSimMesh->getPoints(vxSTL, VProperty::POSE);
	this->m_pSimMesh->getPoints(vXSTL, VProperty::POS0);

	toEigen(vxSTL, this->m_state_x->m_vx);
	toEigen(vXSTL, this->m_state_0->m_vx);

	this->m_isReady_Mesh = true;
	this->m_isReady_Rest = false;
	this->deprecateDeformation();
}

void SurfaceModel::update_SimMeshFromState()
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (this->m_isReady_Mesh)
		return; // Already

	this->m_pSimMesh->setPoints(toSTL(this->m_state_x->m_vx), VProperty::POSE);
	this->m_pSimMesh->setPoints(toSTL(this->m_state_0->m_vx), VProperty::POS0);

	this->m_isReady_Mesh = true;
}

void SurfaceModel::freeElements()
{
	SolidModel::freeElements();

	this->m_vpStretchEles.clear();
	this->m_vpBendingEles.clear();
}

void SurfaceModel::freeMesh()
{
	if (this->m_pSimMesh != NULL)
		delete this->m_pSimMesh;
	this->m_pSimMesh = NULL;
}

void SurfaceModel::onDiscretizationChanged(const iVector& vnewIdxRaw)
{
	// Default implementation: deprecate all
	// precomputed information related with
	// configuration and setup again.

	this->deprecateConfiguration();

	// Notify about the changes in the topology indices

	__raise this->discretizationChanged(this, vnewIdxRaw);
}

void SurfaceModel::configureHarmonicRest(bool useIt, const iVector& vcoupling)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (!this->m_isReady_Mesh)
		this->update_SimMeshFromState();

	this->m_vboundaryList.clear();
	this->m_vcouplingList.clear();
	this->m_vinternalList.clear();

	this->m_vfull2BoundaryMap.clear();
	this->m_vfull2CouplingMap.clear();
	this->m_vfull2InternalMap.clear();

	this->m_nNZ_DfxDb0 = -1;
	this->m_nNZ_DfxDc0 = -1;

	this->m_mD0Db0.setZero();
	this->m_mD0Dc0.setZero();

	this->m_vfull2BoundaryMap.resize(this->m_numNodeRaw, -1);
	this->m_vfull2CouplingMap.resize(this->m_numNodeRaw, -1);
	this->m_vfull2InternalMap.resize(this->m_numNodeRaw, -1);

	this->m_pSimMesh->getSortedBoundaryIndices(this->m_vboundaryList, this->m_vinternalList);

	int numB = (int) this->m_vboundaryList.size();
	int numI = (int) this->m_vinternalList.size();

	// Create boundary map

	int boundaryCount = 0;
	for (int i = 0; i < numB; ++i)
		this->m_vfull2BoundaryMap[this->m_vboundaryList[i]] = boundaryCount++;

	// Create internal map

	int internalCount = 0;
	for (int i = 0; i < numI; ++i)
		this->m_vfull2InternalMap[this->m_vinternalList[i]] = internalCount++;

	// Create coupling list

	this->m_vcouplingList.clear();
	int numC = (int)vcoupling.size();

	for (int i = 0; i < numC; ++i)
		if (this->m_vfull2BoundaryMap[vcoupling[i]] == -1)
			this->m_vcouplingList.push_back(vcoupling[i]);

	numC = (int) this->m_vcouplingList.size();

	// Create coupling map

	int couplingCount = 0;
	for (int i = 0; i < numC; ++i)
		this->m_vfull2CouplingMap[this->m_vcouplingList[i]] = couplingCount++;

	// Format mapping vectors

	vector<iVector> vfull2BoundaryFormatted(this->m_numNodeRaw);
	vector<iVector> vfull2InternalFormatted(this->m_numNodeRaw);
	vector<iVector> vfull2CouplingFormatted(this->m_numNodeRaw);
	for (int i = 0; i < this->m_numNodeRaw; ++i)
	{
		if (this->m_vfull2BoundaryMap[i] != -1) vfull2BoundaryFormatted[i].push_back(this->m_vfull2BoundaryMap[i]);
		if (this->m_vfull2InternalMap[i] != -1) vfull2InternalFormatted[i].push_back(this->m_vfull2InternalMap[i]);
		if (this->m_vfull2CouplingMap[i] != -1) vfull2CouplingFormatted[i].push_back(this->m_vfull2CouplingMap[i]);
	}

	// Create selection matrices

	SparseMatrixXd mSbNodes;
	SparseMatrixXd mSiNodes;
	SparseMatrixXd mScNodes;
	buildSelectionMatrix(vfull2BoundaryFormatted, mSbNodes, true);
	buildSelectionMatrix(vfull2InternalFormatted, mSiNodes, true);
	buildSelectionMatrix(vfull2CouplingFormatted, mScNodes, true);
	buildExtendedMatrix(mSbNodes, this->m_mSb, 2);
	buildExtendedMatrix(mSiNodes, this->m_mSi, 2);
	buildExtendedMatrix(mScNodes, this->m_mSc, 2);

	// Compute Laplacian matrices

	this->initializeHarmonicBoundary();
	this->initializeHarmonicCoupling();

	// Set Jacobian w.r.t. couplings

	this->m_mD0Dc0 = this->m_mBTemp;

	// Set Jacobian w.r.t. boundary

	this->m_mD0Db0 = this->m_mATemp;

	if (numC != 0) // Add also coupling points matrix contribution
		this->m_mD0Db0 -= this->m_mBTemp*this->m_mSc*this->m_mATemp;
}

void SurfaceModel::deprecateDeformation()
{
	NodalSolidModel::deprecateDeformation();
	
	this->m_isReady_DfxDs = false;

	this->m_isReady_fs = false;
}

void SurfaceModel::deprecateDiscretization()
{
	NodalSolidModel::deprecateDiscretization();

	this->m_nNZ_DfxDb0 = -1;
	this->m_nNZ_DfxDc0 = -1;

	this->m_nNZ_DfxDs = -1;
}

void SurfaceModel::initializeMaterials()
{
	for (int i = 0; i < (int) this->m_vpEles.size(); ++i)
	{
		if (this->m_isDummy)
		{
			this->m_vpEles[i]->setMaterial(&this->m_dummyMat);
		}
		else
		{
			this->m_vpEles[i]->setMaterial(&this->m_material);
		}
	}
}

void SurfaceModel::initializeHarmonicBoundary()
{
	this->m_pSimMesh->computeBoundaryToMinimalMap_LaplacianRegular(this->m_mATemp, VProperty::POS0, 2);
}

//void SurfaceModel::initializeHarmonicCoupling()
//{
//	TriMesh* pMesh = this->m_pSimMesh;
//
//	int numD = this->m_numDim_0;
//	int numV = (int) pMesh->getNumNode();
//	int numC = (int) this->m_vcouplingList.size();
//	int numB = (int) this->m_vboundaryList.size();
//	int numF = numC + numB; // All fixed values
//
//	dVector vedgeWeights;
//
//	// Compute edge Laplacian harmonic weights considering cotangents at undeformed
//
//	this->m_pSimMesh->computeLaplacianWeights_Cotangent(vedgeWeights, VProperty::POS0);
//
//	// Assemble Laplacian stiffness matrix
//
//	tVector vL;
//
//	dVector vm(numV, 0.0);
//
//	for (TriMesh::ConstEdgeIter e_it = pMesh->edges_begin(); e_it != pMesh->edges_end(); e_it++)
//	{
//		vector<TriMesh::VertexHandle> vvh;
//		pMesh->getEdgeVertices(*e_it, vvh);
//
//		double w = vedgeWeights[(*e_it).idx()];
//		int idx0 = vvh[0].idx();
//		int idx1 = vvh[1].idx();
//
//		assert(idx0 != idx1);
//
//		vL.push_back(Triplet<Real>(idx0, idx0, w));
//		vL.push_back(Triplet<Real>(idx1, idx1, w));
//		vL.push_back(Triplet<Real>(idx0, idx1, -w));
//		vL.push_back(Triplet<Real>(idx1, idx0, -w));
//
//		// Add Voronoi
//		vm[idx0] += w;
//		vm[idx1] += w;
//	}
//
//	// Assemble inverse lumped mass matrixx
//
//	tVector vM;
//
//	vM.reserve(numV);
//	for (int i = 0; i < numV; ++i) // Invert diagonal
//		vM.push_back(Triplet<Real>(i, i, 1.0 / vm[i]));
//
//	// Compute QP quadratic matrix
//
//	SparseMatrixXd mM(numV, numV);
//	SparseMatrixXd mL(numV, numV);
//	mM.setFromTriplets(vM.begin(), vM.end());
//	mL.setFromTriplets(vL.begin(), vL.end());
//	mM.makeCompressed();
//	mL.makeCompressed();
//
//	SparseMatrixXd mG = mL*mM*mL;
//
//	// Prepare solver parameters
//
//	tVector vG;
//	toTriplets(mG, vG);
//
//	VectorXd vx(numV);
//	vx.setZero();
//
//	VectorXd vc(numV);
//	vc.setZero();
//
//	iVector vCT(numF, 0);
//
//	VectorXd vb(numF);
//	vb.setZero();
//
//	VectorXd vl(numV);
//	VectorXd vu(numV);
//	vl.setZero();
//	vu.setOnes();
//
//	tVector vA;
//	vA.reserve(numC + numB);
//	for (int i = 0; i < numC; ++i)
//		vA.push_back(Triplet<Real>(i, this->m_vcouplingList[i], 1.0));
//	for (int i = 0; i < numB; ++i) 
//		vA.push_back(Triplet<Real>(numC + i, this->m_vboundaryList[i], 1.0));
//
//	// Compute binding weights
//
//	MatrixXd mBTemp(numV, numC);
//	for (int i = 0; i < numC; ++i)
//	{
//		// i-th master 1.0, rest 0.0
//
//		vb.setZero();
//		vb(i) = 1.0;
//
//		string error;
//
//		VectorXd vxo(numV);
//
//		Mosek_Sparse_EQIN_QP solver;
//		if (!solver.solve(vG, vc, vA, vb, vCT, vx, vl, vu, vxo, error))
//			throw new exception("[ERROR] Impossible to solve binding");
//
//		// Copy computed harmonic weights
//		for (int j = 0; j < numV; ++j)
//			mBTemp(j, i) = vxo(j);
//
//		logSimu("[TRACE] Finished harmonic binding for handle %d", i);
//	}
//
//	// Normalize weigths
//
//	MatrixXd mBTempRaw = mBTemp;
//
//	//for (int i = 0; i < numV; ++i)
//	//{
//	//	Real weiSum = 0;
//
//	//	for (int j = 0; j < numC; ++j)
//	//		weiSum += mBTempRaw(i, j);
//
//	//	if (!isApprox(weiSum, 0.0, EPS_APPROX))
//	//	{
//	//		for (int j = 0; j < numC; ++j)
//	//			mBTemp(i, j) /= weiSum;
//	//	}
//	//}
//
//	MatrixXd mBTempNor = mBTemp;
//
//	// Compare raw with normalized weights
//
//	logSimu("[TRACE] Harmonic binding normalization difference: %f", (mBTempRaw - mBTempNor).norm() / mBTempRaw.norm());
//
//	// Initialize temporal B matrix
//
//	tVector vBTemp;
//
//	for (int i = 0; i < numV; ++i)
//	{
//		for (int j = 0; j < numC; ++j)
//		{
//			int offsetV = i*numD;
//			int offsetC = j*numD;
//
//			for (int d = 0; d < numD; ++d) // Add diagonal blocks to temporal B matrix
//				vBTemp.push_back(Triplet<Real>(offsetV + d, offsetC + d, mBTemp(i, j)));
//		}
//	}
//
//	this->m_mBTemp.resize(numD*numV, numD*numC);
//	this->m_mBTemp.setFromTriplets(vBTemp.begin(), vBTemp.end());
//	this->m_mBTemp.makeCompressed(); // Ensure assymetric, sparse
//}

void SurfaceModel::getParam_BoundaryRest(VectorXd& vb0) const
{
	if (this->m_vboundaryList.empty())
		vb0.resize(0); // Empty...

	vb0 = this->m_mSb*this->m_state_0->m_vx;
}

void SurfaceModel::getParam_CouplingRest(VectorXd& vc0) const
{
	if (this->m_vcouplingList.empty())
		vc0.resize(0); // Empty...

	vc0 = this->m_mSc*this->m_state_0->m_vx;
}

void SurfaceModel::getParam_Stretch(VectorXd& vs) const
{
	vs.resize(1);

	vs(0) = this->m_vpStretchEles[0]->getPreStrain();
}

void SurfaceModel::setParam_BoundaryRest(const VectorXd& vb0)
{
	if (this->m_vboundaryList.empty())
		return; // No boundary: out

	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int numB = (int) this->m_vboundaryList.size();
	assert((int)vb0.size() == this->m_numDim_0*numB);


	VectorXd vc0; // Get couplings
	this->getParam_CouplingRest(vc0);

	// Compute rest from boundary and coupling transform
	this->m_state_0->m_vx = this->m_mD0Db0*vb0 + this->m_mD0Dc0*vc0;

	this->m_isReady_Rest = false;
	this->m_isReady_Mass = false;
	this->deprecateDeformation();
}

void SurfaceModel::setParam_CouplingRest(const VectorXd& vc0)
{
	if (this->m_vcouplingList.empty())
		return; // No couplings: out

	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	int numC = (int) this->m_vcouplingList.size();
	assert((int)vc0.size() == this->m_numDim_0*numC);

	VectorXd vb0; // Get boundary
	this->getParam_BoundaryRest(vb0);

	// Compute rest from boundary and coupling transform
	this->m_state_0->m_vx = this->m_mD0Db0*vb0 + this->m_mD0Dc0*vc0;

	this->m_isReady_Rest = false;
	this->m_isReady_Mass = false;
	this->deprecateDeformation();
}

void SurfaceModel::setParam_Stretch(const VectorXd& vs)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	assert(vs.size() == 1);

	for (int i = 0; i < (int) this->m_vpStretchEles.size(); ++i)
		this->m_vpStretchEles[i]->setPreStrain(vs(0)); // Set

	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i)
		this->m_vpBendingEles[i]->setPreStrain(vs(0)); // Set

	this->m_isReady_Rest = false;
	this->m_isReady_Mass = false;
	this->deprecateDeformation();
}

bool SurfaceModel::getParam_Dummy() const
{
	return this->m_isDummy;
}

void SurfaceModel::setParam_Dummy(bool s)
{
	if (!this->m_isReady_Setup)
		this->setup(); // Init.

	if (s == this->m_isDummy)
		return; // No changes

	// Toggle dummy state
	this->m_isDummy = s;

	this->initializeMaterials();

	this->deprecateUndeformed();
}

void SurfaceModel::add_fb0(VectorXd& vfb0)
{
	int numB = (int) this->m_vboundaryList.size();
	vfb0.resize(this->m_numDim_0*numB); // Boundary

	VectorXd vf0(this->m_nsimDOF_0);
	this->add_f0(vf0); // Get global

	// Transform from full space to boundary
	vfb0 = this->m_mD0Db0.transpose() * vf0;
}

void SurfaceModel::add_fc0(VectorXd& vfc0)
{
	int numC = (int) this->m_vcouplingList.size();
	vfc0.resize(this->m_numDim_0*numC); // Coupling

	VectorXd vf0(this->m_nsimDOF_0);
	this->add_f0(vf0); // Get global

	// Transform from full space to coupling
	vfc0 = this->m_mD0Dc0.transpose() * vf0;
}

void SurfaceModel::add_DfxDb0(tVector& vDfxDb0)
{
	//this->testDfxD0Global();

	tVector vDfxD0;
	vDfxD0.reserve(this->getNumNonZeros_DfxD0());
	this->add_DfxD0(vDfxD0, NULL); // Get global

	SparseMatrixXd mDfxD0(m_nsimDOF_x, m_nsimDOF_0);
	mDfxD0.setFromTriplets(vDfxD0.begin(), vDfxD0.end());
	mDfxD0.makeCompressed(); // Assymetric and sparse

	// Transform from full space to boundary space
	SparseMatrixXd mDfxDb0 = mDfxD0*this->m_mD0Db0;

	// Convert to triplet vector
	toTriplets(mDfxDb0, vDfxDb0);
}

void SurfaceModel::add_DfxDc0(tVector& vDfxDc0)
{
	//this->testDfxD0Global();

	tVector vDfxD0;
	vDfxD0.reserve(this->getNumNonZeros_DfxD0());
	this->add_DfxD0(vDfxD0, NULL); // Get global

	SparseMatrixXd mDfxD0(m_nsimDOF_x, m_nsimDOF_0);
	mDfxD0.setFromTriplets(vDfxD0.begin(), vDfxD0.end());
	mDfxD0.makeCompressed(); // Assymetric and sparse

	// Transform from full space to coupling space
	SparseMatrixXd mDfxDc0 = mDfxD0*this->m_mD0Dc0;

	// Convert to triplet vector
	toTriplets(mDfxDc0, vDfxDc0);
}

Real SurfaceModel::get_Energy_Bending()
{
	if (!this->m_isReady_Energy)
		this->update_Energy();

	Real energy = 0;

	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed
		energy += static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->getEnergyIntegral();

	return energy;
}

void SurfaceModel::add_Force_Bending(VectorXd& vfx)
{
	if (!this->m_isReady_Force)
		this->update_Energy();

	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed
		static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->addForce(vfx);

	return;
}

void SurfaceModel::add_DfxDx_Bending(tVector& vDfxDx)
{
	if (!this->m_isReady_Force)
		this->update_Energy();

	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed
		static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->add_DfxDx(vDfxDx);

	return;
}

void SurfaceModel::add_fs_Bending(VectorXd& vfs)
{
	throw new exception("Not implemented yet...");
}

void SurfaceModel::add_DfxDs_Bending(tVector& vDfxDs)
{
	if (!this->m_isReady_DfxDs)
		this->update_DfxDs();

	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed
		static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->add_DfxDs(vDfxDs);
}

void SurfaceModel::add_DfsDs_Bending(tVector& vDfxDs)
{
	throw new exception("Not implemented yet...");

	if (!this->m_isReady_DfsDs)
		this->update_DfsDs();

	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed
		static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->add_DfsDs(vDfxDs);
}

void SurfaceModel::add_fs(VectorXd& vfs)
{
	throw new exception("Not implemented yet...");
}

void SurfaceModel::add_DfxDs(tVector& vDfxDs)
{
	if (!this->m_isReady_DfxDs)
		this->update_DfxDs();

	for (int i = 0; i < (int) this->m_vpStretchEles.size(); ++i) // Cast needed
		static_cast<CSTElement*>(this->m_vpStretchEles[i])->add_DfxDs(vDfxDs);

	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed
		static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->add_DfxDs(vDfxDs);

	// Add gravity contribution
	if (this->m_vg.norm() > 1e-6)
	{
		VectorXd vg(this->m_nsimDOF_x);
		vg.setZero(); // Gravity vector

		this->add_vgravity(vg);

		for (int i = 0; i < (int) this->m_vpStretchEles.size(); ++i) // Add
		{
			tVector vDmxDs; vDmxDs.reserve(9);

			static_cast<CSTElement*>(this->m_vpStretchEles[i])->add_DmxDs(vDmxDs);

			int nC = (int)vDmxDs.size();
			for (int j = 0; j < nC; ++j)
			{
				const Triplet<Real>& t = vDmxDs[j]; // Multiply by gravity times identity
				vDfxDs.push_back(Triplet<Real>(t.row(), t.col(), t.value()*vg(t.row())));
			}
		}
	}
}

void SurfaceModel::add_DfsDs(tVector& vDfxDs)
{
	throw new exception("Not implemented yet...");

	if (!this->m_isReady_DfsDs)
		this->update_DfsDs();

	//for (int i = 0; i < (int) this->m_vpStretchEles.size(); ++i) // Cast needed
	//	static_cast<CSTElement*>(this->m_vpStretchEles[i])->add_DfsDs(vDfxDs);

	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed
		static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->add_DfsDs(vDfxDs);
}

void SurfaceModel::update_fs()
{
	throw new exception("Not implemented yet..");
}

void SurfaceModel::update_DfxDs()
{
	if (this->m_isReady_DfxDs)
		return; // Already done

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int) this->m_vpStretchEles.size(); ++i) // Cast needed, then update matrix
		static_cast<CSTElement*>(this->m_vpStretchEles[i])->update_DfxDs(this->m_state_x->m_vx, this->m_state_0->m_vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed, then update matrix
		static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->update_DfxDs(this->m_state_x->m_vx, this->m_state_0->m_vx);

	// Update also mass derivative

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int) this->m_vpStretchEles.size(); ++i)  // Cast needed, then update matrix
		static_cast<CSTElement*>(this->m_vpStretchEles[i])->update_DmxDs(this->m_state_x->m_vx, this->m_state_0->m_vx);
	
	this->m_isReady_DfxDs = true;
}

void SurfaceModel::update_DfsDs()
{
	throw new exception("Not implemented yet...");

	if (this->m_isReady_DfsDs)
		return; // Already done

//#ifdef OMP_ENFORJAC
//#pragma omp parallel for
//#endif
//	for (int i = 0; i < (int) this->m_vpStretchEles.size(); ++i) // Cast needed, then update matrix
//		static_cast<CSTElement*>(this->m_vpStretchEles[i])->update_DfsDs(this->m_state_x->m_vx, this->m_state_0->m_vx);

#ifdef OMP_ENFORJAC
#pragma omp parallel for
#endif
	for (int i = 0; i < (int) this->m_vpBendingEles.size(); ++i) // Cast needed, then update matrix
		static_cast<DSHingeElement*>(this->m_vpBendingEles[i])->update_DfsDs(this->m_state_x->m_vx, this->m_state_0->m_vx);

	this->m_isReady_DfsDs = true;
}

void SurfaceModel::precompute_DfxDb0()
{
	tVector vDfxDb0;
	this->add_DfxDb0(vDfxDb0);
	this->m_nNZ_DfxDb0 = (int)vDfxDb0.size();
}

void SurfaceModel::precompute_DfxDc0()
{
	tVector vDfxDc0;
	this->add_DfxDc0(vDfxDc0);
	this->m_nNZ_DfxDc0 = (int)vDfxDc0.size();
}

void SurfaceModel::precompute_DfxDs()
{
	tVector vDfxDs;
	this->add_DfxDs(vDfxDs);
	this->m_nNZ_DfxDs = (int)vDfxDs.size();
}

void SurfaceModel::precompute_DfsDs()
{
	tVector vDfsDs;
	this->add_DfsDs(vDfsDs);
	this->m_nNZ_DfsDs = (int)vDfsDs.size();
}

// DEPRECATED -----------------------------------------------------------------------------------------

//void SurfaceModel::configureHarmonicRest(const iVector& vmaster)
//{
//	if (!this->m_isReady_Setup)
//		this->setup(); // Init.
//
//	if (!this->m_isReady_Mesh)
//		this->update_SimMeshFromState();
//
//	this->m_vharmonicMasList.clear();
//	this->m_vharmonicSlaList.clear();
//	this->m_vfull2MasMap.clear();
//	this->m_vfull2SlaMap.clear();
//
//	// Copy the same master node list
//	this->m_vharmonicMasList = vmaster;
//
//	// Create additional structures
//
//	int numMas = (int) this->m_vharmonicMasList.size();
//	int numSla = this->m_numNodeRaw - numMas; // Comp.
//
//	this->m_vfull2MasMap.resize(this->m_numNodeRaw, -1);
//	this->m_vfull2SlaMap.resize(this->m_numNodeRaw, -1);
//	this->m_vmas2FullMap.resize(numMas, -1);
//	this->m_vsla2FullMap.resize(numSla, -1);
//
//	int masCount = 0;
//	for (int i = 0; i < numMas; ++i)
//	{
//		this->m_vmas2FullMap[masCount] = this->m_vharmonicMasList[i];
//		this->m_vfull2MasMap[this->m_vharmonicMasList[i]] = masCount;
//		masCount++;
//	}
//
//	for (int i = 0; i < this->m_numNodeRaw; ++i)
//		if (this->m_vfull2MasMap[i] == -1)
//			this->m_vharmonicSlaList.push_back(i);
//
//	int slaCount = 0;
//	for (int i = 0; i < numSla; ++i)
//	{
//		this->m_vsla2FullMap[slaCount] = this->m_vharmonicSlaList[i];
//		this->m_vfull2SlaMap[this->m_vharmonicSlaList[i]] = slaCount;
//		slaCount++;
//	}
//
//	// Precompute harmonic matrices
//	this->initializeHarmonicRest();
//}
//
//void SurfaceModel::add_fh0(VectorXd& vfh0)
//{
//	int numMas = (int) this->m_vharmonicMasList.size();
//	vfh0.resize(this->m_numDim_0*numMas); // Only master
//
//	VectorXd vf0(this->m_nsimDOF_0);
//	this->add_f0(vf0); // Get global
//
//	// Transform from full space to master
//	vfh0 = this->m_mD0Dh0.transpose() * vf0;
//}
//
//void SurfaceModel::add_DfxDh0(tVector& vDfxDh0)
//{
//	tVector vDfxD0;
//	vDfxD0.reserve(this->getNumNonZeros_DfxD0());
//	this->add_DfxD0(vDfxD0, NULL); // Get global
//
//	SparseMatrixXd mDfxD0(m_nsimDOF_x, m_nsimDOF_0);
//	mDfxD0.setFromTriplets(vDfxD0.begin(), vDfxD0.end());
//	mDfxD0.makeCompressed(); // Assymetric and sparse
//
//	SparseMatrixXd mDfxDh0 = mDfxD0*this->m_mD0Dh0;
//
//	// Convert to triplet vector
//	toTriplets(mDfxDh0, vDfxDh0);
//}
//
//void SurfaceModel::precompute_DfxDh0()
//{
//	tVector vDfxDh0;
//	this->add_DfxDh0(vDfxDh0);
//	this->m_nNZ_DfxDh0 = (int)vDfxDh0.size();
//}
//
//void SurfaceModel::initializeHarmonicRest()
//{
//	TriMesh* pMesh = this->m_pSimMesh;
//
//	int numVtx = (int)pMesh->getNumNode();
//	int numMas = (int) this->m_vharmonicMasList.size();
//	int numSla = (int) this->m_vharmonicSlaList.size();
//
//	dVector vedgeWeights;
//
//	// Compute edge harmonic weights considering cotangents at undeformed
//	this->m_pSimMesh->computeLaplacianWeights_Cotangent(vedgeWeights, VProperty::POS0);
//
//	// Assemble Laplacian stiffness matrix
//
//	tVector vL;
//
//	dVector vm(numVtx, 0.0);
//
//	for (TriMesh::ConstEdgeIter e_it = pMesh->edges_begin(); e_it != pMesh->edges_end(); e_it++)
//	{
//		vector<TriMesh::VertexHandle> vvh;
//		pMesh->getEdgeVertices(*e_it, vvh);
//
//		double w = vedgeWeights[(*e_it).idx()];
//		int idx0 = vvh[0].idx();
//		int idx1 = vvh[1].idx();
//
//		assert(idx0 != idx1);
//
//		vL.push_back(Triplet<Real>(idx0, idx0, w));
//		vL.push_back(Triplet<Real>(idx1, idx1, w));
//		vL.push_back(Triplet<Real>(idx0, idx1, -w));
//		vL.push_back(Triplet<Real>(idx1, idx0, -w));
//
//		// Add Voronoi
//		vm[idx0] += w;
//		vm[idx1] += w;
//	}
//
//	// Assemble inverse lumped mass matrixx
//
//	tVector vM; // Diag.
//	vM.reserve(numVtx);
//
//	for (int i = 0; i < numVtx; ++i) // Invert diagonal
//		vM.push_back(Triplet<Real>(i, i, 1.0 / vm[i]));
//
//	// Compute QP quadratic matrix
//
//	SparseMatrixXd mM(numVtx, numVtx);
//	SparseMatrixXd mL(numVtx, numVtx);
//	mM.setFromTriplets(vM.begin(), vM.end());
//	mL.setFromTriplets(vL.begin(), vL.end());
//	mM.makeCompressed();
//	mL.makeCompressed();
//
//	SparseMatrixXd mG = mL*mM*mL;
//
//	// Prepare solver parameters
//
//	iVector vCT(numMas, 0);
//
//	tVector vG;
//	toTriplets(mG, vG);
//
//	VectorXd vx(numVtx);
//	vx.setZero();
//
//	VectorXd vc(numVtx);
//	vc.setZero();
//
//	VectorXd vb(numMas);
//	vb.setZero();
//
//	VectorXd vl(numVtx);
//	VectorXd vu(numVtx);
//	vl.setZero();
//	vu.setOnes();
//
//	MatrixXd mA(numMas, numVtx);
//	mA.setZero();
//	for (int i = 0; i < numMas; ++i)
//		mA(i, this->m_vmas2FullMap[i]) = 1.0;
//
//	// Compute binding weights
//
//	MatrixXd mD0Dh0(numVtx, numMas);
//	for (int i = 0; i < numMas; ++i)
//	{
//		// i-th master 1.0, rest 0.0
//
//		vb.setZero();
//		vb(i) = 1.0;
//
//		string error;
//
//		VectorXd vxo(numVtx);
//
//		AlgLib_Sparse_EQIN_QP solver;
//		if (!solver.solve(vG, vc, mA, vb, vCT, vx, vl, vu, vxo, error))
//			throw new exception("[ERROR] Impossible to solve binding");
//
//		// Copy computed harmonic weights
//		for (int j = 0; j < numVtx; ++j)
//			mD0Dh0(j, i) = vxo(j);
//
//		logSimu("[TRACE] Finished harmonic binding for handle %d", i);
//	}
//
//	// Normalize weigths
//
//	MatrixXd mD0Dh0Raw = mD0Dh0;
//
//	for (int i = 0; i < numVtx; ++i)
//	{
//		Real weiSum = 0;
//
//		for (int j = 0; j < numMas; ++j)
//			weiSum += mD0Dh0(i, j);
//
//		for (int j = 0; j < numMas; ++j)
//			mD0Dh0(i, j) /= weiSum;
//	}
//
//	MatrixXd mD0Dh0Nor = mD0Dh0;
//
//	// Compare raw with normalized weights
//
//	logSimu("[TRACE] Harmonic binding normalization difference: %f", (mD0Dh0Raw - mD0Dh0Nor).norm() / mD0Dh0Raw.norm());
//
//	// Initialize D0Dh0
//
//	tVector vD0Dh0;
//
//	for (int i = 0; i < numVtx; ++i)
//	{
//		for (int j = 0; j < numMas; ++j)
//		{
//			int offsetV = i*this->m_numDim_0;
//			int offsetM = j*this->m_numDim_0;
//
//			for (int d = 0; d < this->m_numDim_0; ++d) // Add diagonal blocks to matrix
//				vD0Dh0.push_back(Triplet<Real>(offsetV + d, offsetM + d, mD0Dh0(i, j)));
//		}
//	}
//
//	this->m_mD0Dh0.resize(this->m_numDim_0*numVtx, this->m_numDim_0*numMas);
//
//	this->m_mD0Dh0.setFromTriplets(vD0Dh0.begin(), vD0Dh0.end());
//	this->m_mD0Dh0.makeCompressed(); // Ensure assymetric, sparse
//
//	// Store rest coordinates at binding time
//
//	this->m_vh0.resize(numMas*this->m_numDim_0);
//
//	this->m_vx0 = this->m_state_0->m_vx;
//
//	for (int i = 0; i < numMas; ++i)
//	{
//		for (int d = 0; d < this->m_numDim_0; ++d)
//			this->m_vh0(this->m_numDim_0*i + d) = this->m_state_0->m_vx(this->m_numDim_0*this->m_vmas2FullMap[i] + d);
//	}
//}
//
//void SurfaceModel::update_fh0()
//{
//	this->update_f0();
//}
//
//void SurfaceModel::update_DfxDh0()
//{
//	this->update_DfxD0();
//}
//
//void SurfaceModel::getParam_HarmonicRest(VectorXd& vhX) const
//{
//	int numMas = (int) this->m_vharmonicMasList.size();
//	vhX.resize(this->m_numDim_0 * numMas); // Master X
//
//	for (int i = 0; i < numMas; ++i)
//	{
//		for (int d = 0; d < this->m_numDim_0; ++d) // Get master coordinates from global rest X0
//			vhX(this->m_numDim_0*i + d) = this->m_state_0->m_vx(this->m_numDim_0*this->m_vmas2FullMap[i] + d);
//	}
//}
//
//void SurfaceModel::setParam_HarmonicRest(const VectorXd& vh0)
//{
//	int numMas = (int) this->m_vharmonicMasList.size();
//	assert((int)vh0.size() == this->m_numDim_0*numMas);
//
//	//// Compute positions from master
//	//this->m_state_0->m_vx = this->m_mD0Dh0*vh0;
//
//	// Compute positions from the transform of master displacement
//	this->m_state_0->m_vx = this->m_mD0Dh0*(vh0 - this->m_vh0) + this->m_vx0;
//
//	this->m_isReady_Rest = false;
//	this->m_isReady_Mass = false;
//	this->deprecateDeformation();
//}

//const iVector& SurfaceModel::getHarmonicMasterNodes() const
//{
//	return this->m_vharmonicMasList;
//}
//
//const iVector& SurfaceModel::getHarmonicSlaveNodes() const
//{
//	return this->m_vharmonicSlaList;
//}
