/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_EdgeLength.h
\author		jesusprod
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_TENSILE_EDGELENGTH_H
#define PARAMETER_SET_TENSILE_EDGELENGTH_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/Model/TensileStructureModel.h>

class JSANDBOX_EXPORT ParameterSet_Tensile_EdgeLength : public ParameterSet
{
public:
	ParameterSet_Tensile_EdgeLength(int midx);
	virtual ~ParameterSet_Tensile_EdgeLength();

	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

protected:

	TensileStructureModel* m_pTSModel;

	int m_midx;

};

#endif