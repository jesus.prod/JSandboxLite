///*=====================================================================================*/
///*!
//\file		Mosek_Sparse_EQIN_QP.h
//\author		jesusprod
//\brief		Wrapper for Mosek solver for linearly constrained QP problems.
//*/
///*=====================================================================================*/
//
//#ifndef MOSEK_SPARSE_EQIN_QP_H
//#define MOSEK_SPARSE_EQIN_QP_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//
//#include <mosek.h>
//
//class JSANDBOX_EXPORT Mosek_Sparse_EQIN_QP
//{
//public:
//	Mosek_Sparse_EQIN_QP()
//	{
//		// Nothing to do here...
//	}
//
//	virtual ~Mosek_Sparse_EQIN_QP()
//	{
//		// Nothing to do here...
//	}
//
//	/*! This method solves problems of the kind:
//	*	min q(x) = 0.5*x^T*Q*x + x^T*c + D
//	*		s.t. Ax >=< b
//	*			 l <= x
//	*			 x <= u
//	*/
//	virtual bool solve(const tVector& vQ,		 // The quadratic coeffients matrix
//					   const VectorXd& vc,		 // The linear coefficients vector
//					   const tVector& mA,		 // The constraints coefficients matrix
//					   const VectorXd& vb,		 // The constraints independent vector
//					   const iVector& vCT,		 // The type of constraints { -, +, 0}
//					   const VectorXd& vx,		 // The initial solution vector
//					   const VectorXd& vl,		 // The lower bounds: -Inf if not bounded
//					   const VectorXd& vu,		 // The uppder bounds: Inf if not bounded
//					   VectorXd& vxo,			 // The solution of the QP problem
//					   string& error);
//
//};
//
//#endif