/*=====================================================================================*/
/*!
\file		SurfaceRemesherNew.cpp
\author		jesusprod
\brief		Implementation of SurfaceRemesherNew.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/SurfaceRemesherNew.h>

bool SurfaceRemesherNew::splitFace(TriMesh::FaceHandle fh)
{
	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getFaceVertices(fh, vvh);

	// Remove face and edges if needed

	m_pMesh->delete_face(fh, false);

	// Introduce new vertex in the midpoint

	TriMesh::Point newX = (
		m_pMesh->point(vvh[0]) +
		m_pMesh->point(vvh[1]) +
		m_pMesh->point(vvh[2])*(1 / 3));
	TriMesh::VertexHandle vh = m_pMesh->add_vertex(newX);

	// Introduce new faces and edges

	TriMesh::FaceHandle fh0 = m_pMesh->add_face(vh, vvh[0], vvh[1]);
	TriMesh::FaceHandle fh1 = m_pMesh->add_face(vh, vvh[1], vvh[2]);
	TriMesh::FaceHandle fh2 = m_pMesh->add_face(vh, vvh[2], vvh[0]);

	return true;
}

bool SurfaceRemesherNew::splitEdge(TriMesh::EdgeHandle eh)
{
	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getHingeVertices(eh, vvh);

	// Remove edge and faces ikj, jli

	m_pMesh->delete_edge(eh, false);

	// Introduce new vertex in the midpoint

	TriMesh::Point newX = (
		m_pMesh->point(vvh[0]) +
		m_pMesh->point(vvh[1]))*0.5;
	TriMesh::VertexHandle vh = m_pMesh->add_vertex(newX);

	// Introduce new faces and edges

	vector<TriMesh::FaceHandle> vfhNew;

	Vector3d v = m_pMesh->getPoint(vh);
	Vector3d v0 = m_pMesh->getPoint(vvh[0]);
	Vector3d v1 = m_pMesh->getPoint(vvh[1]);
	Vector3d v2 = m_pMesh->getPoint(vvh[2]);

	TriMesh::FaceHandle fh0;
	TriMesh::FaceHandle fh1;
	fh0 = m_pMesh->add_face(vh, vvh[0], vvh[2]);
	fh1 = m_pMesh->add_face(vh, vvh[2], vvh[1]);

	if ((int)vvh.size() == 4)
	{
		TriMesh::FaceHandle fh2;
		TriMesh::FaceHandle fh3;
		fh2 = m_pMesh->add_face(vh, vvh[1], vvh[3]);
		fh3 = m_pMesh->add_face(vh, vvh[3], vvh[0]);
	}

	return true;
}

bool SurfaceRemesherNew::flipEdge(TriMesh::EdgeHandle eh)
{
	assert(this->m_pMesh->is_boundary(eh));

	vector<TriMesh::VertexHandle> vvh;
	m_pMesh->getHingeVertices(eh, vvh);

	// Remove edge and faces ikj, jli

	m_pMesh->delete_edge(eh, false);

	// Introduce new faces and edges

	TriMesh::FaceHandle fh0 = m_pMesh->add_face(vvh[0], vvh[2], vvh[3]);
	TriMesh::FaceHandle fh1 = m_pMesh->add_face(vvh[2], vvh[3], vvh[2]);

	return true;
}