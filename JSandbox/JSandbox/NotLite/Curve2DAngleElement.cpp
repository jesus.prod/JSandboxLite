/*=====================================================================================*/
/*!
\file		Curve2DAngleElement.cpp
\author		jesusprod
\brief		Implementation of Curve2DAngleElement.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/Curve2DAngleElement.h>

Curve2DAngleElement::Curve2DAngleElement(int dim0) : NodalSolidElement(3, 2, dim0)
{
	this->m_dim0 = dim0;
}

Curve2DAngleElement::~Curve2DAngleElement()
{
	// Nothing to do here
}

void Curve2DAngleElement::update_Rest(const VectorXd& vX)
{
	if (this->m_dim0 == 2)
	{
		Vector2d x0 = getBlock2x1(this->m_vnodeIdxx[0], vX);
		Vector2d x1 = getBlock2x1(this->m_vnodeIdxx[1], vX);
		Vector2d x2 = getBlock2x1(this->m_vnodeIdxx[2], vX);
		Vector2d x01 = x1 - x0;
		Vector2d x12 = x2 - x1;
		Real x01norm = x01.norm();
		Real x12norm = x12.norm();
		x01 /= x01norm;
		x12 /= x12norm;
		double cosa = x01.dot(x12);

		this->m_vL0 = 0.5*(x01norm + x12norm);

		if (isApprox(abs(cosa), 1.0, 1.0e-9)) // Co-linear
		{
			this->m_phi0 = 0;
			return; // Special
		}

		{
			Real C = (((x1[0] - x0[0])*(x2[1] - x1[1]) - (x1[1] - x0[1])*(x2[0] - x1[0])) < 0.0) ? -1.0 : 1.0; // Convexity

#include "../../Maple/Code/Curve2DAnglePhi.mcg"
			this->m_phi0 = C*t30;
		}
	}

	if (this->m_dim0 == 3)
	{
		Vector3d x0 = getBlock3x1(this->m_vnodeIdxx[0], vX);
		Vector3d x1 = getBlock3x1(this->m_vnodeIdxx[1], vX);
		Vector3d x2 = getBlock3x1(this->m_vnodeIdxx[2], vX);
		Vector3d x01 = x1 - x0;
		Vector3d x12 = x2 - x1;
		Real x01norm = x01.norm();
		Real x12norm = x12.norm();
		x01 /= x01norm;
		x12 /= x12norm;
		this->m_vL0 = 0.5*(x01norm + x12norm);

		Vector3d x01p = x01 - x01.dot(m_ref)*m_ref;
		Vector3d x12p = x12 - x12.dot(m_ref)*m_ref;
		x01p.normalize();
		x12p.normalize();

		double cosa = x01p.dot(x12p); // Col?
		if (isApprox(abs(cosa), 1.0, 1.0e-9))
		{
			this->m_phi0 = 0;
			return; // Special
		}

		Real C = ((x01.cross(x12)).dot(m_ref) < 0) ? 1 : -1;

		this->m_phi0 = C*acos(cosa);
	}
}

void Curve2DAngleElement::update_Mass(const VectorXd& vX)
{
	// Does not carry mass
	this->m_vmx.setZero();
}

void Curve2DAngleElement::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	Vector2d x0 = getBlock2x1(this->m_vnodeIdxx[0], vx);
	Vector2d x1 = getBlock2x1(this->m_vnodeIdxx[1], vx);
	Vector2d x2 = getBlock2x1(this->m_vnodeIdxx[2], vx);
	Vector2d x01 = x1 - x0;
	Vector2d x12 = x2 - x1;
	double cosa = x01.normalized().dot(x12.normalized());

	//if (isApprox(abs(cosa), 1.0, 1.0e-9)) // Co-linear
	//{
	//	this->m_energy = 0;
	//	return; // Special case
	//}

	// --------------------------------------------------

	Real C = (((x1[0] - x0[0])*(x2[1] - x1[1]) - (x1[1] - x0[1])*(x2[0] - x1[0])) < 0.0) ? -1.0 : 1.0; // Convexity

	double vL0 = this->m_vL0;
	double phi0 = this->m_phi0;

	double kB = m_vpmat[0]->getBendingK();

//	{
//#include "../../Maple/Code/Curve2DAngleEnergy.mcg"
//
//		this->m_energy = t36;
//	}
	
	{
	double t9 = x1[0] * x1[0];
	double t12 = x0[0] * x0[0];
	double t13 = x1[1] * x1[1];
	double t16 = x0[1] * x0[1];
	double t18 = sqrt(t9 - 2.0*x1[0] * x0[0] + t12 + t13 - 2.0*x1[1] * x0[1] + t16);
	double t21 = x2[0] * x2[0];
	double t24 = x2[1] * x2[1];
	double t28 = sqrt(t21 - 2.0*x2[0] * x1[0] + t9 + t24 - 2.0*x2[1] * x1[1] + t13);
	Real cosa = ((x1[0] - x0[0])*(x2[0] - x1[0]) + (x1[1] - x0[1])*(x2[1] - x1[1])) / t18 / t28;
	cosa = max(cosa, -1.0);
	cosa = min(cosa, +1.0);
	double t31 = acos(cosa);
	double t34 = pow(C*t31 - phi0, 2.0);
	double t36 = 0.5*kB*vL0*t34;

	if (isfinite(t36))
		this->m_energy = t36;
	else this->m_energy = 0.0;
	}
}

void Curve2DAngleElement::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	Vector2d x0p = getBlock2x1(this->m_vnodeIdxx[0], vx);
	Vector2d x1p = getBlock2x1(this->m_vnodeIdxx[1], vx);
	Vector2d x2p = getBlock2x1(this->m_vnodeIdxx[2], vx);
	Vector2d x01 = x1p - x0p;
	Vector2d x12 = x2p - x1p;
	double cosa = x01.normalized().dot(x12.normalized());

	//if (isApprox(abs(cosa), 1.0, 1.0e-9)) // Co-linear
	//{
	//	this->m_vfVal.setZero();
	//	return; // Special case
	//}

	// --------------------------------------------------

	vector<double> x0(2);
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x0[i] = vx(this->m_vidxx[0 + i]);
		x1[i] = vx(this->m_vidxx[2 + i]);
		x2[i] = vx(this->m_vidxx[4 + i]);
	}

	Real C = (((x1[0] - x0[0])*(x2[1] - x1[1]) - (x1[1] - x0[1])*(x2[0] - x1[0])) < 0.0) ? -1.0 : 1.0; // Convexity

	double vL0 = this->m_vL0;
	double phi0 = this->m_phi0;

	double kB = m_vpmat[0]->getBendingK();

	double vfx[6];

//	{
//#include "../../Maple/Code/Curve2DAngleForce.mcg"
//	}

	{
		double t2 = x1[0] - x0[0];
		double t3 = x2[0] - x1[0];
		double t5 = x1[1] - x0[1];
		double t6 = x2[1] - x1[1];
		double t8 = t2*t3 + t5*t6;
		double t9 = x1[0] * x1[0];
		double t12 = x0[0] * x0[0];
		double t13 = x1[1] * x1[1];
		double t16 = x0[1] * x0[1];
		double t17 = t9 - 2.0*x1[0] * x0[0] + t12 + t13 - 2.0*x1[1] * x0[1] + t16;
		double t18 = sqrt(t17);
		double t19 = 1 / t18;
		double t20 = t8*t19;
		double t21 = x2[0] * x2[0];
		double t24 = x2[1] * x2[1];
		double t27 = t21 - 2.0*x2[0] * x1[0] + t9 + t24 - 2.0*x2[1] * x1[1] + t13;
		double t28 = sqrt(t27);
		double t29 = 1 / t28;
		Real cosa = t20*t29;
		cosa = max(cosa, -1.0);
		cosa = min(cosa, +1.0);
		double t31 = acos(cosa);
		double t34 = kB*vL0*(C*t31 - phi0);
		double t39 = t8 / t18 / t17;
		double t45 = t8*t8;
		double t51 = sqrt(1.0 - t45 / t17 / t27);
		double t52 = 1 / t51;
		double t74 = 1 / t28 / t27;
		vfx[0] = 0.1E1*t34*C*(-t3*t19*t29 + t39*t29*t2)*t52;
		vfx[1] = 0.1E1*t34*C*(-t6*t19*t29 + t39*t29*t5)*t52;
		vfx[2] = 0.1E1*t34*C*((x2[0] - 2.0*x1[0] + x0[0])*t19*t29 - t39*t29*t2 + t20*t74*
			t3)*t52;
		vfx[3] = 0.1E1*t34*C*((x2[1] - 2.0*x1[1] + x0[1])*t19*t29 - t39*t29*t5 + t20*t74*
			t6)*t52;
		vfx[4] = 0.1E1*t34*C*(t2*t19*t29 - t20*t74*t3)*t52;
		vfx[5] = 0.1E1*t34*C*(t5*t19*t29 - t20*t74*t6)*t52;
	}

	for (int i = 0; i < 6; ++i)
	if (!isfinite(vfx[i]))
	{
		//logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);
		vfx[i] = 0.0;
	}

	// Store force vector

	for (int i = 0; i < 6; ++i)
		this->m_vfVal(i) = vfx[i];
}

void Curve2DAngleElement::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	Vector2d x0p = getBlock2x1(this->m_vnodeIdxx[0], vx);
	Vector2d x1p = getBlock2x1(this->m_vnodeIdxx[1], vx);
	Vector2d x2p = getBlock2x1(this->m_vnodeIdxx[2], vx);
	Vector2d x01 = x1p - x0p;
	Vector2d x12 = x2p - x1p;
	double cosa = x01.normalized().dot(x12.normalized());

	//if (isApprox(abs(cosa), 1.0, 1.0e-9)) // Co-linear
	//{
	//	this->m_vJVal.setZero();
	//	return; // Special case
	//}

	// --------------------------------------------------

	vector<double> x0(2);
	vector<double> x1(2);
	vector<double> x2(2);
	for (int i = 0; i < 2; ++i)
	{
		x0[i] = vx(this->m_vidxx[0 + i]);
		x1[i] = vx(this->m_vidxx[2 + i]);
		x2[i] = vx(this->m_vidxx[4 + i]);
	}

	Real C = (((x1[0] - x0[0])*(x2[1] - x1[1]) - (x1[1] - x0[1])*(x2[0] - x1[0])) < 0.0) ? -1.0 : 1.0; // Convexity

	double vL0 = this->m_vL0;
	double phi0 = this->m_phi0;

	double kB = m_vpmat[0]->getBendingK();

	double mJx[6][6];

	//#include "../../Maple/Code/Curve2DAngleJacobian.mcg"

	{
		double t1 = kB*vL0;
		double t2 = C*C;
		double t3 = -x2[0] + x1[0];
		double t4 = x1[0] * x1[0];
		double t7 = x0[0] * x0[0];
		double t8 = x1[1] * x1[1];
		double t11 = x0[1] * x0[1];
		double t12 = t4 - 2.0*x1[0] * x0[0] + t7 + t8 - 2.0*x1[1] * x0[1] + t11;
		double t13 = sqrt(t12);
		double t14 = 1 / t13;
		double t15 = t3*t14;
		double t16 = x2[0] * x2[0];
		double t19 = x2[1] * x2[1];
		double t22 = t16 - 2.0*x2[0] * x1[0] + t4 + t19 - 2.0*x2[1] * x1[1] + t8;
		double t23 = sqrt(t22);
		double t24 = 1 / t23;
		double t26 = x1[0] - x0[0];
		double t28 = x1[1] - x0[1];
		double t29 = x2[1] - x1[1];
		double t31 = -t26*t3 + t28*t29;
		double t33 = 1 / t13 / t12;
		double t34 = t31*t33;
		double t35 = -2.0*t24*t26;
		double t38 = t15*t24 - t34*t35 / 2.0;
		double t39 = t38*t38;
		double t41 = t31*t31;
		double t42 = 1 / t12;
		double t43 = t41*t42;
		double t44 = 1 / t22;
		double t46 = 1.0 - t43*t44;
		double t47 = 1 / t46;
		double t51 = t31*t14;
		Real cosa = t51*t24;
		cosa = max(cosa, -1.0);
		cosa = min(cosa, +1.0);
		double t53 = acos(cosa);
		double t56 = t1*(C*t53 - phi0);
		double t57 = t3*t33;
		double t59 = t12*t12;
		double t62 = t31 / t13 / t59;
		double t66 = 3.0*t62*t24*t26*t26;
		double t67 = t34*t24;
		double t70 = sqrt(t46);
		double t71 = 1 / t70;
		double t75 = C*t38;
		double t77 = 1 / t70 / t46;
		double t78 = t31*t42;
		double t83 = t41 / t59;
		double t87 = t77*(-2.0*t78*t44*t3 - 2.0*t83*t44*t26);
		double t92 = t1*t2;
		double t93 = -t29*t14;
		double t95 = -2.0*t24*t28;
		double t98 = t93*t24 - t34*t95 / 2.0;
		double t102 = 0.1E1*t92*t98*t47*t38;
		double t105 = -t29*t33;
		double t115 = 0.1E1*t56*C*(-t57*t95 / 2.0 - t105*t35 / 2.0 - 3.0 / 2.0*t62*t35*t28)*t71;
		double t122 = t77*(2.0*t78*t44*t29 - 2.0*t83*t44*t28);
		double t128 = x2[0] - 2.0*x1[0] + x0[0];
		double t129 = t128*t14;
		double t131 = 2.0*t24*t26;
		double t135 = 1 / t23 / t22;
		double t136 = 2.0*t135*t3;
		double t139 = t129*t24 - t34*t131 / 2.0 - t51*t136 / 2.0;
		double t140 = t139*t47;
		double t143 = 0.1E1*t92*t140*t38;
		double t144 = t14*t24;
		double t149 = t128*t33;
		double t155 = -2.0*t135*t26;
		double t163 = 0.1E1*t56*C*(t144 - t57*t131 / 2.0 - t15*t136 / 2.0 - t149*t35 / 2.0 + 3.0 / 2.0*
			t62*t35*t26 + t34*t155*t3 / 2.0 + t67)*t71;
		double t169 = t22*t22;
		double t170 = 1 / t169;
		double t174 = t77*(-2.0*t78*t44*t128 + 2.0*t83*t44*t26 + 2.0*t43*t170*t3);
		double t180 = x2[1] - 2.0*x1[1] + x0[1];
		double t181 = t180*t14;
		double t183 = 2.0*t24*t28;
		double t186 = -2.0*t135*t29;
		double t189 = t181*t24 - t34*t183 / 2.0 - t51*t186 / 2.0;
		double t190 = t189*t47;
		double t193 = 0.1E1*t92*t190*t38;
		double t198 = t180*t33;
		double t211 = 0.1E1*t56*C*(-t57*t183 / 2.0 - t15*t186 / 2.0 - t198*t35 / 2.0 + 3.0 / 2.0*t62*
			t35*t28 - t34*t155*t29 / 2.0)*t71;
		double t220 = t77*(-2.0*t78*t44*t180 + 2.0*t83*t44*t28 - 2.0*t43*t170*t29);
		double t225 = t26*t14;
		double t227 = -2.0*t135*t3;
		double t230 = t225*t24 - t51*t227 / 2.0;
		double t231 = t230*t47;
		double t234 = 0.1E1*t92*t231*t38;
		double t237 = t26*t33;
		double t247 = 0.1E1*t56*C*(-t144 - t15*t227 / 2.0 - t237*t35 / 2.0 - t34*t155*t3 / 2.0)*t71;
		double t254 = t77*(-2.0*t78*t44*t26 - 2.0*t43*t170*t3);
		double t259 = t28*t14;
		double t261 = 2.0*t135*t29;
		double t264 = t259*t24 - t51*t261 / 2.0;
		double t265 = t264*t47;
		double t268 = 0.1E1*t92*t265*t38;
		double t271 = t28*t33;
		double t281 = 0.1E1*t56*C*(-t15*t261 / 2.0 - t271*t35 / 2.0 + t34*t155*t29 / 2.0)*t71;
		double t288 = t77*(-2.0*t78*t44*t28 + 2.0*t43*t170*t29);
		double t293 = C*t98;
		double t298 = t98*t98;
		double t307 = 3.0*t62*t24*t28*t28;
		double t319 = 0.1E1*t92*t140*t98;
		double t329 = -2.0*t135*t28;
		double t337 = 0.1E1*t56*C*(-t105*t131 / 2.0 - t93*t136 / 2.0 - t149*t95 / 2.0 + 3.0 / 2.0*t62*
			t95*t26 + t34*t329*t3 / 2.0)*t71;
		double t344 = 0.1E1*t92*t190*t98;
		double t361 = 0.1E1*t56*C*(t144 - t105*t183 / 2.0 - t93*t186 / 2.0 - t198*t95 / 2.0 + 3.0 / 2.0*
			t62*t95*t28 - t34*t329*t29 / 2.0 + t67)*t71;
		double t368 = 0.1E1*t92*t231*t98;
		double t380 = 0.1E1*t56*C*(-t93*t227 / 2.0 - t237*t95 / 2.0 - t34*t329*t3 / 2.0)*t71;
		double t387 = 0.1E1*t92*t265*t98;
		double t399 = 0.1E1*t56*C*(-t144 - t93*t261 / 2.0 - t271*t95 / 2.0 + t34*t329*t29 / 2.0)*t71
			;
		double t404 = C*t139;
		double t413 = t139*t139;
		double t418 = 2.0*t144;
		double t421 = 2.0*t135*t26;
		double t426 = 1 / t23 / t169;
		double t430 = 3.0*t51*t426*t3*t3;
		double t431 = t51*t135;
		double t443 = 0.1E1*t92*t190*t139;
		double t461 = 2.0*t426*t3;
		double t469 = 0.1E1*t56*C*(-t149*t183 / 2.0 - t129*t186 / 2.0 - t198*t131 / 2.0 + 3.0 / 2.0*
			t62*t131*t28 - t34*t421*t29 / 2.0 - t181*t136 / 2.0 + t34*t136*t28 / 2.0 - 3.0 / 2.0*t51*t461*
			t29)*t71;
		double t476 = 0.1E1*t92*t231*t139;
		double t493 = 0.1E1*t56*C*(t144 - t129*t227 / 2.0 - t237*t131 / 2.0 - t34*t421*t3 / 2.0 - t225
			*t136 / 2.0 - 3.0 / 2.0*t51*t461*t3 + t431)*t71;
		double t500 = 0.1E1*t92*t265*t139;
		double t517 = 0.1E1*t56*C*(-t129*t261 / 2.0 - t271*t131 / 2.0 + t34*t421*t29 / 2.0 - t259*
			t136 / 2.0 + 3.0 / 2.0*t51*t461*t29)*t71;
		double t522 = C*t189;
		double t535 = t189*t189;
		double t542 = 2.0*t135*t28;
		double t549 = 3.0*t51*t426*t29*t29;
		double t561 = 0.1E1*t92*t231*t189;
		double t571 = -2.0*t426*t29;
		double t579 = 0.1E1*t56*C*(-t181*t227 / 2.0 - t237*t183 / 2.0 - t34*t542*t3 / 2.0 - t225*
			t186 / 2.0 - 3.0 / 2.0*t51*t571*t3)*t71;
		double t586 = 0.1E1*t92*t265*t189;
		double t603 = 0.1E1*t56*C*(t144 - t181*t261 / 2.0 - t271*t183 / 2.0 + t34*t542*t29 / 2.0 -
			t259*t186 / 2.0 + 3.0 / 2.0*t51*t571*t29 + t431)*t71;
		double t608 = C*t230;
		double t625 = t230*t230;
		double t642 = 0.1E1*t92*t265*t230;
		double t655 = 0.1E1*t56*C*(-t225*t261 / 2.0 - t259*t227 / 2.0 - 3.0*t51*t426*t3*t29)*t71
			;
		double t660 = C*t264;
		double t681 = t264*t264;
		mJx[0][0] = -0.1E1*t1*t2*t39*t47 + 0.1E1*t56*C*(-t57*t35 + t66 - t67)*t71 - 0.5*
			t56*t75*t87;
		mJx[0][1] = -t102 + t115 - 0.5*t56*t75*t122;
		mJx[0][2] = -t143 + t163 - 0.5*t56*t75*t174;
		mJx[0][3] = -t193 + t211 - 0.5*t56*t75*t220;
		mJx[0][4] = -t234 + t247 - 0.5*t56*t75*t254;
		mJx[0][5] = -t268 + t281 - 0.5*t56*t75*t288;
		mJx[1][0] = -t102 + t115 - 0.5*t56*t293*t87;
		mJx[1][1] = -0.1E1*t1*t2*t298*t47 + 0.1E1*t56*C*(-t105*t95 + t307 - t67)*t71
			- 0.5*t56*t293*t122;
		mJx[1][2] = -t319 + t337 - 0.5*t56*t293*t174;
		mJx[1][3] = -t344 + t361 - 0.5*t56*t293*t220;
		mJx[1][4] = -t368 + t380 - 0.5*t56*t293*t254;
		mJx[1][5] = -t387 + t399 - 0.5*t56*t293*t288;
		mJx[2][0] = -t143 + t163 - 0.5*t56*t404*t87;
		mJx[2][1] = -t319 + t337 - 0.5*t56*t404*t122;
		mJx[2][2] = -0.1E1*t1*t2*t413*t47 + 0.1E1*t56*C*(-t418 - t149*t131 - t129*t136 +
			t66 + t34*t421*t3 - t67 + t430 - t431)*t71 - 0.5*t56*t404*t174;
		mJx[2][3] = -t443 + t469 - 0.5*t56*t404*t220;
		mJx[2][4] = -t476 + t493 - 0.5*t56*t404*t254;
		mJx[2][5] = -t500 + t517 - 0.5*t56*t404*t288;
		mJx[3][0] = -t193 + t211 - 0.5*t56*t522*t87;
		mJx[3][1] = -t344 + t361 - 0.5*t56*t522*t122;
		mJx[3][2] = -t443 + t469 - 0.5*t56*t522*t174;
		mJx[3][3] = -0.1E1*t1*t2*t535*t47 + 0.1E1*t56*C*(-t418 - t198*t183 - t181*t186 +
			t307 - t34*t542*t29 - t67 + t549 - t431)*t71 - 0.5*t56*t522*t220;
		mJx[3][4] = -t561 + t579 - 0.5*t56*t522*t254;
		mJx[3][5] = -t586 + t603 - 0.5*t56*t522*t288;
		mJx[4][0] = -t234 + t247 - 0.5*t56*t608*t87;
		mJx[4][1] = -t368 + t380 - 0.5*t56*t608*t122;
		mJx[4][2] = -t476 + t493 - 0.5*t56*t608*t174;
		mJx[4][3] = -t561 + t579 - 0.5*t56*t608*t220;
		mJx[4][4] = -0.1E1*t1*t2*t625*t47 + 0.1E1*t56*C*(-t225*t227 + t430 - t431)*t71
			- 0.5*t56*t608*t254;
		mJx[4][5] = -t642 + t655 - 0.5*t56*t608*t288;
		mJx[5][0] = -t268 + t281 - 0.5*t56*t660*t87;
		mJx[5][1] = -t387 + t399 - 0.5*t56*t660*t122;
		mJx[5][2] = -t500 + t517 - 0.5*t56*t660*t174;
		mJx[5][3] = -t586 + t603 - 0.5*t56*t660*t220;
		mJx[5][4] = -t642 + t655 - 0.5*t56*t660*t254;
		mJx[5][5] = -0.1E1*t1*t2*t681*t47 + 0.1E1*t56*C*(-t259*t261 + t549 - t431)*t71
			- 0.5*t56*t660*t288;
	}

	for (int i = 0; i < 6; ++i)
		for (int j = 0; j < 6; ++j)
		if (!isfinite(mJx[i][j]))
		{
			//logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
			mJx[i][j] = 0;
		}

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 6; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}