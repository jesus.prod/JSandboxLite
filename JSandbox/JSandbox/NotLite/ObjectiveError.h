///*=====================================================================================*/
///*!
//\file		ObjectiveError.h
//\author		jesusprod
//\brief		Base abstract class for optimization objectives.
//*/
///*=====================================================================================*/
//
//#ifndef OBJECTIVE_ERROR_H
//#define OBJECTIVE_ERROR_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//
//#include <JSandbox/Optim/SQPOptimizerEQ.h>
//#include <JSandbox/Solver/PhysSolver.h>
//#include <JSandbox/Model/SolidModel.h>
//
//class SQPOptimizerEQ;
//
//class JSANDBOX_EXPORT ObjectiveError
//{
//public:
//	ObjectiveError(const string& ID);
//
//	virtual ~ObjectiveError();
//
//	virtual void setOptimizer(SQPOptimizerEQ* pOptim);
//	virtual const SQPOptimizerEQ* getOptimizer() const;
//
//	virtual const string& getID() const;
//
//	virtual void setup() = 0;
//
//	virtual Real getError() const = 0;
//	virtual void add_DEDx(VectorXd& vDEDx) const = 0;
//	virtual void add_D2EDx2(tVector& vD2EDx2) const = 0;
//
//protected:
//
//	string m_ID;
//
//	SolidModel* m_pModel;
//	PhysSolver* m_pSolver;
//	SQPOptimizerEQ* m_pOptim;
//
//};
//
//#endif