/*=====================================================================================*/
/*!
\file		TriMeshPartitioner.cpp
\author		jesusprod
\brief		Implementation of TriMeshPartitioner.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/TriMeshPartitioner.h>


TriMeshPartitioner::TriMeshPartitioner()
{
	
}

TriMeshPartitioner::~TriMeshPartitioner()
{
	// Nothing to do here...
}

bool TriMeshPartitioner::makePartition(const TriMesh& triMesh, const RodMesh& rodMesh)
{
	this->m_resultOP.m_isOK = false;
	this->m_resultOP.m_mapBoundaryEdges.clear();
	this->m_resultOP.m_vmapSplitToWhole.clear();
	this->m_resultOP.m_vmapWholeToSplit.clear();
	this->m_resultOP.m_pRodMesh = NULL;
	this->m_resultOP.m_pTriMesh = NULL;
	this->m_resultOP.m_vremOPs.clear();

	this->m_resultOP.m_pTriMesh = new TriMesh(triMesh);

	// Prepare rod mesh: split rod meshes whenever a pair
	// of rods intersect so that in the end intersections
	// are only placed at extremes. That improves the
	// contouring
	
	this->m_resultOP.m_pRodMesh = new RodMesh(rodMesh);
	this->prepareRodMesh(rodMesh, *m_resultOP.m_pRodMesh);

	// Use a Contour Surface Remesher to create a sequence
	// of edges in the triangle mesh that follow a set of
	// curves.

	SurfaceRemesherContour contourRemesher;

	contourRemesher.setContourEdges(true);
	contourRemesher.setContourNodes(false);
	contourRemesher.setContourExtremes(true);
	contourRemesher.setEdgeMoveParameter(this->m_edgeMoveFactor);
	contourRemesher.setFaceMoveParameter(this->m_faceMoveFactor);
	contourRemesher.setInsersectEPS(this->m_intersecEpsi);
	contourRemesher.setBoundaryEPS(this->m_boundaryEpsi);

	const RodMesh& contours = *this->m_resultOP.m_pRodMesh;
	int nr = contours.getNumRod(); // Add mesh contours
	for (int i = 0; i < nr; ++i)
		contourRemesher.addContourCurve(contours.getRod(i).getCurve());

	const RemeshOP& contourOP = contourRemesher.remesh(this->m_resultOP.m_pTriMesh);
	if (!contourOP.m_OK)
		return false; // Error in contouring, return false
	else
	{
		this->m_resultOP.m_vremOPs.push_back(contourOP);

		delete this->m_resultOP.m_pTriMesh; // Exchange
		this->m_resultOP.m_pTriMesh = contourOP.m_pMesh;

		this->m_resultOP.m_pTriMesh->testOverlappedVertices(1e-6);
		this->m_resultOP.m_pTriMesh->testInvertedTriangles();
		this->m_resultOP.m_pTriMesh->testIsolatedVertices();
	}
	
	// Use a Cutter Surface Remesher to cut the triangle mesh along the
	// lines previously contoured by the contour remesher. Partitioning.

	// Get seam curves edges

	vector<MeshTraits::ID> vcontoursEdges;
	for (int r = 0; r < nr; ++r)
	{
		const vector<MeshTraits::ID>& vcontourEdges = contourRemesher.getContourEdges(r);
		vcontoursEdges.insert(vcontoursEdges.end(), vcontourEdges.begin(), vcontourEdges.end());
	}

	std::sort(vcontoursEdges.begin(), vcontoursEdges.end()); // Remove duplicated edges from edges vector
	vcontoursEdges.erase(std::unique(vcontoursEdges.begin(), vcontoursEdges.end()), vcontoursEdges.end());

	// Check all edges are contained in theb mesh
	for (int i = 0; i < (int)vcontoursEdges.size(); ++i)
		this->m_resultOP.m_pTriMesh->getEdgeHandle(vcontoursEdges[i]);

	if ((int) vcontoursEdges.size() > 0)
	{
		SurfaceRemesherCutter cutterRemesher;
		cutterRemesher.setCutEdges(vcontoursEdges);

		RemeshOP cutterOP = cutterRemesher.remesh(this->m_resultOP.m_pTriMesh);
		
		// Error in cutting?
		if (!cutterOP.m_OK)
			return false;

		delete this->m_resultOP.m_pTriMesh; // Exchange
		this->m_resultOP.m_pTriMesh = cutterOP.m_pMesh;

		this->m_resultOP.m_pTriMesh->getSplittedMeshMaps(this->m_resultOP.m_vmapSplitToWhole,
														 this->m_resultOP.m_vmapWholeToSplit,
														 this->m_resultOP.m_mapBoundaryEdges);

		dVector vpos;
		this->m_resultOP.m_pTriMesh->getPoints(vpos);
		this->m_resultOP.m_pTriMesh->setPoints(vpos, VPropGeo::POS3D);
		this->m_resultOP.m_pTriMesh->setPoints(vpos, VPropGeo::PAR2D);
	}

	this->m_resultOP.m_isOK = true;

	return true;
}

bool TriMeshPartitioner::prepareRodMesh(const RodMesh& meshIn, RodMesh& meshOut)
{
	// TODO: Made fast, to many unnecessary copies
	// of rods and curves here. Optimize this part
	// of the code.

	int nr = meshIn.getNumRod();

	vector<Curve> vcurves;
	for (int r = 0; r < nr; ++r) // Copy curves to split
		vcurves.push_back(meshIn.getRod(r).getCurve());

	vector<Curve> vsplittedCurves;

	// Split curves at intersection
	vector<vector<SplitPoint>> vpoints;
	Curve::Intersect(vcurves, vpoints);
	for (int r = 0; r < nr; r++)
	{
		if (!vpoints[r].empty())
		{
			vector<Curve> rodCurves; // Vector of splitted curve
			Curve::Split(vcurves[r], vpoints[r], rodCurves);
			for (int c = 0; c < (int)rodCurves.size(); c++)
				vsplittedCurves.push_back(rodCurves[c]);
		}
		else vsplittedCurves.push_back(vcurves[r]); // Whole curve
	}
	
	int nsc = (int)vsplittedCurves.size();

	vector<Rod> vsplittedRods(nsc);
	for (int c = 0; c < nsc; ++c) // Add splitted rod
		vsplittedRods[c].initialize(vsplittedCurves[c]);

	meshOut.init(vsplittedRods);

	return true;
}
