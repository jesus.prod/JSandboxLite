/*=====================================================================================*/
/*!
\file		ParameterSet_Tensile_SurfaceCoupling.h
\author		jesusprod
\brief		Parameter set for a surface coupling in a tensile structure.
*/
/*=====================================================================================*/

#ifndef PARAMETER_SET_TENSILE_SURFACECOUPLING_H
#define PARAMETER_SET_TENSILE_SURFACECOUPLING_H

#if _MSC_VER > 1000
#pragma once
#endif

#include <JSandbox/JSandboxPCH.h>
#include <JSandbox/MathUtils.h>

#include <JSandbox/Optim/ParameterSet.h>

#include <JSandbox/Model/TensileStructureModel.h>

class JSANDBOX_EXPORT ParameterSet_Tensile_SurfaceCoupling : public ParameterSet
{
public:
	ParameterSet_Tensile_SurfaceCoupling();

	virtual ~ParameterSet_Tensile_SurfaceCoupling();

	virtual void getParameters(VectorXd& vp) const;
	virtual void setParameters(const VectorXd& vp);

	virtual void setup();
	virtual bool isReady_fp() const;
	virtual bool isReady_DfDp() const;
	virtual void get_fp(VectorXd& vfp) const;
	virtual void get_DfDp(tVector& vDfDp) const;

protected:

	TensileStructureModel* m_pTSModel;

};

#endif