/*=====================================================================================*/
/*!
\file		TriMeshParametrizer.cpp
\author		mskouras,jesusprod
\brief		Implementation of TriMeshParametrizer.h
*/
/*=====================================================================================*/

#include <JSandbox/Geometry/TriMeshParametrizer.h>

#include <JSandbox/Model/Curve2DModel.h>
#include <JSandbox/Solver/BConditionFixed.h>
#include <JSandbox/Solver/SolverGenericDynSta.h>

TriMeshParametrizer::TriMeshParametrizer()
{
	this->m_curveAngleK = 1.0;
	this->m_curveLengthK = 1.0;
	this->m_correctArea = true;

	this->m_curveSolverError = 1.0e-6;
}

TriMeshParametrizer::~TriMeshParametrizer()
{
	// Nothing to do here...
}

bool TriMeshParametrizer::parametrize(TriMesh* pMesh, dVector& valluv) const
{
	assert(pMesh != NULL);

	vector<vector<TriMesh::FaceHandle>> vpatsFH;
	vector<vector<TriMesh::VertexHandle>> vpatsVH;

	pMesh->getPartitionConnectedComponents(vpatsFH, vpatsVH);

	int nv = (int) pMesh->n_vertices();
	int npatchs = (int) vpatsFH.size();
	
	valluv.resize(2*nv);
	Real shiftXSum = 0.0;

	// Parametrize individual patches

	for (int i = 0; i < npatchs; ++i)
	{
		int patvCount = 0;
		map<int, int> mapGlobalToLocal;
		map<int, int> mapLocalToGlobal;
		dVector vpatPos;
		iVector vpatIdx;

		int patchnv = (int)vpatsVH[i].size();
		int patchnf = (int)vpatsFH[i].size();
		for (int f = 0; f < patchnf; ++f)
		{
			vector<TriMesh::VertexHandle> vvh;
			pMesh->getFaceVertices(vpatsFH[i][f], vvh);
			for (int v = 0; v < 3; ++v) // Face vertices
			{
				int glovidx = vvh[v].idx();

				if (mapGlobalToLocal.find(glovidx) == mapGlobalToLocal.end())
				{
					mapGlobalToLocal[glovidx] = patvCount;
					mapLocalToGlobal[patvCount] = glovidx;  
					Vector3d p = pMesh->getPoint(vvh[v]);
					vpatPos.push_back(p(0));
					vpatPos.push_back(p(1));
					vpatPos.push_back(p(2));
					patvCount++;
				}

				vpatIdx.push_back(mapGlobalToLocal[glovidx]);
			}
		}

		assert((int)vpatPos.size() / 3 == patchnv);
		assert((int)vpatIdx.size() / 3 == patchnf);
	
		dVector vpatuv; // Patch UV

		TriMesh patMesh;
		patMesh.init(vpatPos, vpatIdx);
		if (!this->parametrizePatch(&patMesh, vpatuv))
		{
			logSimu("[ERROR] Imposible to parametrize patch %d\n", i);
			vpatuv.resize((int) vpatPos.size() * 2); // Invalid values
			return false;
		}

		// Compute bounding box

		Real minX = HUGE_VAL, maxX = -HUGE_VAL,
			 minY = HUGE_VAL, maxY = -HUGE_VAL;
		for (int v = 0; v < patchnv; ++v)
		{
			int offset = 2 * v;
			Real x = vpatuv[offset + 0];
			Real y = vpatuv[offset + 1];
			if (x < minX) minX = x;
			if (x > maxX) maxX = x;
			if (y < minY) minY = y;
			if (y > maxY) maxY = y;
		}

		// Centering shifting patterns

		Real rangeX = (maxX - minX);
		Real shiftY = -(maxY + minY)*0.5;
		Real shiftX = -(maxX + minX)*0.5; 
		shiftX += shiftXSum + (rangeX/2)*1.25;
		shiftXSum = maxX + shiftX; // Offset

		for (int v = 0; v < patchnv; ++v)
		{
			int offset = 2 * v;
			vpatuv[offset + 0] = vpatuv[offset + 0] + shiftX;
			vpatuv[offset + 1] = vpatuv[offset + 1] + shiftY;
		}

		// Fill global vector

		for (int v = 0; v < patchnv; ++v)
		{
			int offsetLoc = 2 * v;
			int offsetGlo = 2 * mapLocalToGlobal[v];
			valluv[offsetGlo + 0] = vpatuv[offsetLoc + 0];
			valluv[offsetGlo + 1] = vpatuv[offsetLoc + 1];
		}
	}

	// Compute bounding box to center patches

	Real minX = HUGE_VAL, maxX = -HUGE_VAL;

	for (int v = 0; v < nv; ++v) 
	{
		Real x = valluv[2*v];
		if (x < minX) minX = x;
		if (x > maxX) maxX = x;
	}

	Real shiftX = -(maxX + minX)*0.5;
	for (int v = 0; v < nv; ++v)
		valluv[2*v] += shiftX;
	
	return true;
}

bool TriMeshParametrizer::parametrizePatch(TriMesh* pMesh, dVector& vpatuv) const
{
	// Get boundary loops

	vector<vector<TriMesh::EdgeHandle>> vehs;
	vector<vector<TriMesh::VertexHandle>> vvhs;
	pMesh->getBoundaryComponents_Sorted(vehs, vvhs);

	int nloops = (int)vvhs.size();
	if (nloops < 1)
	{
		logSimu("[ERROR] Mesh patch has no boundary loops\n");
		return false;
	}
	if (nloops > 1)
	{
		logSimu("[ERROR] Mesh patch has many boundary loops\n");
		return false;
	}

	int nv = (int) pMesh->n_vertices();
	vpatuv.resize(2*nv); // Out coords.

	// Parametrize boundary loops

	for (int i = 0; i < nloops; ++i)
	{
		// Get positions

		int loopnv = (int)vvhs[i].size();
		dVector vlen(loopnv);
		dVector vang(loopnv);
		dVector vcurPosIn(3*loopnv);
		for (int v = 0; v < loopnv; ++v)
		{
			int offset = 3 * v;
			Vector3d p = pMesh->getPoint(vvhs[i][v]);
			for (int ii = 0; ii < 3; ++ii) // Fill
				vcurPosIn[offset + ii] = p(ii);
		}

		for (int v = 0; v < loopnv; ++v)
		{
			// Target length

			int i0 = v;
			int i1 = (v + 1) % loopnv;
			Vector3d p0 = get3D(i0, vcurPosIn);
			Vector3d p1 = get3D(i1, vcurPosIn);
			vlen[i0] = (p1 - p0).norm();

			// Target angle

			vector<TriMesh::VertexHandle> vvh; // Ring
			pMesh->getVertexVertices(vvhs[i][v], vvh);
			int ringnv = (int) vvh.size();
			double angleSum = 0.0;
			for (int rv = 0; rv < ringnv - 1; ++rv)
			{
				Vector3d o0 = pMesh->getPoint(vvh[rv + 0]);
				Vector3d o1 = pMesh->getPoint(vvh[rv + 1]);
				Vector3d v0 = o0 - p0;
				Vector3d v1 = o1 - p0;
				angleSum += acos(v0.normalized().dot(v1.normalized()));
			}

			vang[i0] = M_PI - angleSum;
		}

		// Parametrize loop curve

		dVector vcuruv;
		if (this->parametrizeCurve(vcurPosIn, vlen, vang, vcuruv))
		{
			for (int v = 0; v < loopnv; ++v) // Copy resulting UV
			{ 
				int offsetl = 2 * v;
				int offsetg = 2 * vvhs[i][v].idx();
				vpatuv[offsetg + 0] = vcuruv[offsetl + 0];
				vpatuv[offsetg + 1] = vcuruv[offsetl + 1];
			}
		}
		else 
		{
			logSimu("[ERROR] Imposible to parametrize curve %d\n", i);
			return false;
		}
	}

	// Parametrize the patch

	tVector vA;

	dVector vedgeWeights; // Edge harmonic weights
	this->computeHarmonicWeights(pMesh, vedgeWeights);

	for (TriMesh::ConstEdgeIter e_it = pMesh->edges_begin(); e_it != pMesh->edges_end(); e_it++)
	{
		if (pMesh->is_boundary(*e_it))
			continue; // Ignore bounds

		vector<TriMesh::VertexHandle> vvh;
		pMesh->getEdgeVertices(*e_it, vvh);

		if (pMesh->is_boundary(vvh[0]) &&
			pMesh->is_boundary(vvh[1]))
			continue; // Fixed

		double w = vedgeWeights[(*e_it).idx()];
		int idx0 = vvh[0].idx();
		int idx1 = vvh[1].idx();

		vA.push_back(Triplet<Real>(idx0, idx0, w));
		vA.push_back(Triplet<Real>(idx1, idx1, w));

		// Lower triangular

		if (idx0 > idx1) 
		{
			vA.push_back(Triplet<Real>(idx0, idx1, -w));
		}
		else
		{
			vA.push_back(Triplet<Real>(idx1, idx0, -w));
		}
	}

	// Solve for each UV coordinate separately

	for (int c = 0; c < 2; ++c)
	{
		tVector vAc = vA;

		dVector vb(nv, 0.0);
		for (int i = 0; i < nloops; ++i)
		{
			int loopnv = (int)vvhs[i].size();
			for (int v = 0; v < loopnv; ++v)
			{
				int idx = vvhs[i][v].idx();
				vb[idx] += vpatuv[2*idx + c];

				// Remove entries
				int ne = (int) vAc.size();
				for (int e = 0; e < ne; ++e)
				{
					const Triplet<Real>& t = vAc[e];
					int row = t.row();
					int col = t.col();

					if (row == idx && col != idx)
					{
						vb[col] -= t.value()*vb[idx]; // RHS
						vAc[e] = Triplet<Real>(row, col, 0.0);
					}
					if (col == idx && row != idx)
					{
						vb[row] -= t.value()*vb[idx]; // RHS
						vAc[e] = Triplet<Real>(row, col, 0.0);
					}
					if (col == idx && row == idx)
					{
						vAc[e] = Triplet<Real>(row, col, 0.0);
					}
				}

				vAc.push_back(Triplet<Real>(idx, idx, 1.0));
			}
		}

		VectorXd vbSolve = toEigen(vb);

		SparseMatrixXd mA(nv, nv);
		mA.setFromTriplets(vAc.begin(), vAc.end());
		mA.makeCompressed(); // Compress symmetric

		SimplicialLDLT<SparseMatrixXd> solver;
		solver.compute(mA); // Prefactorization

		VectorXd vxSolve = solver.solve(vbSolve);

		if (solver.info() != Success)
		{
			logSimu("[ERROR] Imposible to solve linear system for parametrization\n");
			return false;
		}

		logSimu("[GOOD] In %s: Linear system solved using Cholesky\n", __FUNCTION__);

		dVector vx = toSTL(vxSolve);

		// Check 

		for (int i = 0; i < nloops; ++i)
		{
			int loopnv = (int)vvhs[i].size();
			for (int v = 0; v < loopnv; ++v)
			{ 
				int idx = vvhs[i][v].idx(); // Check fixed boundary
				if (!isApprox(vx[idx], vpatuv[2*idx + c], 1e-6))
				{
					logSimu("[ERROR] Boundary vertices not fixed\n");
					return false;
				}
			}
		}

		// Copy coordinate to result 
		for (int i = 0; i < nv; ++i)
			vpatuv[2*i + c] = vx[i];
	}

	if (this->m_correctArea)
	{
		int nf = (int) pMesh->n_faces();

		dVector vx0;
		dVector vx1;
		pMesh->getPoints(vx0);
		to3D(vpatuv, vx1, 2);

		Real area0 = 0.0;
		Real area1 = 0.0;

		for (int i = 0; i < nf; ++i) // Sum all faces areas
			area0 += pMesh->getFaceArea(pMesh->face_handle(i));

		// Test new points
		pMesh->setPoints(vx1);

		for (int i = 0; i < nf; ++i) // Sum all faces areas
			area1 += pMesh->getFaceArea(pMesh->face_handle(i));

		// Recover previous
		pMesh->setPoints(vx0);

		Real alpha = sqrt(area0 / area1);

		// Correct scale

		int numEle = 2 * nv;
		for (int i = 0; i < numEle; ++i)
			vpatuv[i] = vpatuv[i] * alpha;
	}

	return true;
}

bool TriMeshParametrizer::parametrizeCurve(const dVector& vposIn, const dVector& vlen, const dVector& vang, dVector& vposOut) const
{
	initializeCurve(vposIn, vlen, vang, vposOut);

	// Create model

	Curve2DModel* pModel = new Curve2DModel();

	dVector vtarLen = vlen;
	dVector vtarAng = vang;
	SolidMaterial mat;
	mat.setDensity(1.0);
	mat.setBendingK(1.0);
	mat.setStretchK(10.0);
	pModel->configureMaterial(mat);
	pModel->configureCurve(vposOut);
	pModel->setParam_Lengths(&vtarLen);
	pModel->setParam_Angles(&vtarAng);
	pModel->setup();

	// Create minimizer

	SolverGenericDynSta* pSolver = new SolverGenericDynSta();
	pSolver->setModel(pModel);
	pSolver->setIsStatic(true);
	pSolver->setIsNonLin(true); 
	pSolver->setSolverError(1e-3); 
	pSolver->setLinearError(1e-6);
	pSolver->setSolverMaxIters(500);
	pSolver->setLinearMaxIters(500); 
	pSolver->setRegularizeFactor(1e-3);
	pSolver->setLineSearchFactor(0.50); 
	pSolver->setRegularizeMaxIters(10);
	pSolver->setLineSearchMaxIters(10);
	pSolver->setLinearSolver(PhysSolver::Cholesky);
	BConditionFixed* pBCFixed = new BConditionFixed();
	iVector vfix;
	vfix.push_back(0);
	vfix.push_back(1); 
	pBCFixed->initializeNodal(2, vfix);
	pSolver->addBCondition(pBCFixed);
	pBCFixed->updateStencil(pModel);

	pSolver->setup(); 

	if (pSolver->solve())
	{
		vposOut = toSTL(pModel->getPositions_x()); 

		delete pSolver;
		delete pModel;
		 
		return true;
	}
	else
	{
		vposOut = toSTL(pModel->getPositions_x());

		delete pSolver;
		delete pModel;

		return false;
	}

	return true;
}

void TriMeshParametrizer::initializeCurve(const dVector& vposIn, const dVector& vlen, const dVector& vang, dVector& vposOut) const
{
	assert((int)vposIn.size() % 3 == 0 && (int)vposIn.size() >= 6);

	int nv = (int)vposIn.size() / 3;

	double lenT = 0.0; // Total
	for (int i = 0; i < nv; ++i)
		lenT += vlen[i];

	double R = lenT / (2 * M_PI);

	vposOut.clear();
	double angle = 0;
	for (int i = 0; i < nv; ++i)
	{
		vposOut.push_back(R*cos(angle));
		vposOut.push_back(R*sin(angle));
		angle += vlen[i] / R;
	}
}

void TriMeshParametrizer::computeHarmonicWeights(const TriMesh* pMesh, dVector& vedgeWeights) const
{
	vedgeWeights.resize(pMesh->n_edges());

	TriMesh::HalfedgeHandle h0, h1, h2;
	TriMesh::VertexHandle v0, v1, v2;
	Vector3d p0, p1, p2, d0, d1;

	for (TriMesh::ConstEdgeIter e_it = pMesh->edges_begin(); e_it != pMesh->edges_end(); ++e_it)
	{
		double w = 0.0;

		if (pMesh->is_boundary(*e_it))
			w = 1.0; // Ignore boundary
		else
		{
			h0 = pMesh->halfedge_handle(*e_it, 0);
			v0 = pMesh->to_vertex_handle(h0);
			p0 = pMesh->getPoint(v0);

			h1 = pMesh->halfedge_handle(*e_it, 1);
			v1 = pMesh->to_vertex_handle(h1);
			p1 = pMesh->getPoint(v1);

			h2 = pMesh->next_halfedge_handle(h0);
			v2 = pMesh->to_vertex_handle(h2);
			p2 = pMesh->getPoint(v2);

			d0 = (p0 - p2).normalized();
			d1 = (p1 - p2).normalized();
			w += 1.0 / tan(acos(std::min(0.99, std::max(-0.99, (d0.dot(d1))))));

			h2 = pMesh->next_halfedge_handle(h1);
			v2 = pMesh->to_vertex_handle(h2);
			p2 = pMesh->getPoint(v2);
	
			d0 = (p0 - p2).normalized();
			d1 = (p1 - p2).normalized();
			w += 1.0 / tan(acos(std::min(0.99, std::max(-0.99, (d0.dot(d1))))));

			w = std::max(w, 0.0);
		}

		vedgeWeights[(*e_it).idx()] = w;
	}
}