///*=====================================================================================*/
///*!
//\file		SolverParameterized.h
//\author		jesusprod
//\brief		TODO
//*/
///*=====================================================================================*/
//
//#ifndef SOLVER_PARAMETERIZED_H
//#define SOLVER_PARAMETERIZED_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//
//#include <JSandbox\Model\ModelParameterization.h>
//
//#include <JSandbox/Optim/ParameterSet.h>
//
//#include <JSandbox/CustomTimer.h>
//
//class ParameterSet;
//
//class JSANDBOX_EXPORT SolverParameterized
//{
//public:
//	SolverParameterized(const string& ID);
//
//	virtual ~SolverParameterized();
//
//	virtual void setup();
//
//	virtual ModelParameterization* getParameterization() const;
//	virtual void setParameterization(ModelParameterization* pP);
//
//	virtual bool solve();
//	virtual Real getPotential(const VectorXd& vx, const VectorXd& vp);
//	virtual void getResidual(const VectorXd& vx, const VectorXd& vp, VectorXd& vR);
//	virtual void getMatrix(const VectorXd& vx, const VectorXd& vp, SparseMatrixXd& mA);
//
//	//virtual void getMatrix(const VectorXd& vx, 
//	//					   const VectorXd& vp, 
//	//					   SparseMatrixXd& mAx, 
//	//					   SparseMatrixXd& mAp);
//
//	virtual bool hasMaximumStepSize() const { return this->m_hasMaxStep; }
//	virtual void hasMaximumStepSize(bool has) { this->m_hasMaxStep = has; }
//
//	virtual Real getMaximumStepSize() const { return this->m_maxStep; }
//	virtual void setMaximumStepSize(Real ss) { this->m_maxStep = ss; }
//
//	virtual Real getSolverError() const { return this->m_solverMaxError; }
//	virtual void setSolverError(Real e) { this->m_solverMaxError = e; }
//
//	virtual Real getLinearError() const { return this->m_linearMaxError; }
//	virtual void setLinearError(Real lE) { this->m_linearMaxError = lE; }
//
//	virtual bool getUseRegularize() const { return this->m_useRegularize; }
//	virtual void setUseRegularize(bool reg) { this->m_useRegularize = reg; }
//
//	virtual bool getUseLineSearch() const { return this->m_useLineSearch; }
//	virtual void setUseLineSearch(bool bLS) { this->m_useLineSearch = bLS; }
//
//	virtual int getLineSearchMaxIters() const { return this->m_lsMaxIters; }
//	virtual void setLineSearchMaxIters(int maxit) { this->m_lsMaxIters = maxit; }
//
//	virtual int getRegularizeMaxIters() const { return this->m_regMaxIters; }
//	virtual void setRegularizeMaxIters(int maxit) { this->m_regMaxIters = maxit; }
//
//	virtual int getSolverMaxIters() const { return this->m_solverMaxIters; }
//	virtual void setSolverMaxIters(int maxit) { this->m_solverMaxIters = maxit; }
//
//	virtual int getLinearMaxIters() const { return this->m_linearMaxIters; }
//	virtual void setLinearMaxIters(int maxit) { this->m_linearMaxIters = maxit; }
//
//	virtual Real getRegularizeFactor() const { return this->m_rFactor; }
//	virtual void setRegularizeFactor(Real rC) { this->m_rFactor = rC; }
//
//	virtual Real getLineSearchFactor() const { return this->m_lsBeta; }
//	virtual void setLineSearchFactor(Real lsB) { this->m_lsBeta = lsB; }
//
//protected:
//
//	virtual void plotSearchDirection(int Nx, int Np, const VectorXd& vxi, const VectorXd& vpi, const VectorXd& vdt);
//
//	virtual bool solveLinearSystem_Mosek(SparseMatrixXd& mA, VectorXd& vb, VectorXd& x, double wx = 1.0, double wp = 1.0, bool bounded = false, double regDef = 1e-3);
//
//	virtual bool solveLinearSystem_AlgLibBLEIC(SparseMatrixXd& mA, VectorXd& vb, VectorXd& x, double wx = 1.0, double wp = 1.0, bool bounded = false, double regDef = 1e-3);
//	virtual bool solveLinearSystem_AlgLibQuick(SparseMatrixXd& mA, VectorXd& vb, VectorXd& x, double wx = 1.0, double wp = 1.0, bool bounded = false, double regDef = 1e-3);
//	virtual bool solveLinearSystem_CustomFullQP(SparseMatrixXd& mA, VectorXd& vb, VectorXd& x, double wx = 1.0, double wp = 1.0, bool bounded = false, double regDef = 1e-3);
//	virtual bool solveLinearSystem_CustomFactQP(SparseMatrixXd& mA, VectorXd& vb, VectorXd& x, double wx = 1.0, double wp = 1.0, bool bounded = false, double regDef = 1e-3);
//
//	virtual bool solveLinearSystem_FactorBound(SparseMatrixXd& mA, VectorXd& vb, VectorXd& x, double wx = 1.0, double wp = 1.0, bool bounded = false, double regDef = 1e-3);
//
//	//virtual bool solveLinearSystem(const SparseMatrixXd& mAx, 
//	//							     const SparseMatrixXd& mAp, 
//	//							     const VectorXd& vbx, 
//	//							     VectorXd& x, 
//	//							     VectorXd& p);
//
//	string m_ID;
//
//	SolidModel* m_pModel;
//	PhysSolver* m_pSolver;
//
//	ModelParameterization* m_pParam;
//
//	Real m_rFactor;					// Factor for regularization
//	Real m_lsBeta;					// Factor for the line-search
//	Real m_solverMaxError;			// Physics solver convergence error
//	Real m_linearMaxError;			// Linear solver convergence error
//
//	int m_lsMaxIters;				// Maximum line-search bisections
//	int m_regMaxIters;				// Maximum matrix regularizations
//	int m_solverMaxIters;			// Maximum physics solver iterations
//	int m_linearMaxIters;			// Maximum linear solver iterations
//
//	bool m_hasMaxStep;
//	double m_maxStep;
//
//	bool m_useRegularize;
//	bool m_useLineSearch;
//
//
//	VectorXd m_vp;
//	VectorXd m_vx;
//
//	VectorXd m_vfp;
//
//};
//
//#endif