/*=====================================================================================*/
/*!
\file		CSTElementNH.cpp
\author		jesusprod
\brief		Implementation of CSTElementNH.h
*/
/*=====================================================================================*/

#include <JSandbox/Element/CSTElementNH.h>

CSTElementNH::CSTElementNH(int d0) : CSTElement(d0)
{
	// Nothing to do here
}

CSTElementNH::~CSTElementNH()
{
	// Nothing to do here
}

void CSTElementNH::update_Energy(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	double kc = m_vpmat[0]->getShear() / 2.0;
	double kd = m_vpmat[0]->getBulk() / 2.0;

	const double& A0 = this->m_A0;
	const DNDx& dNdx = this->m_dNdx;

#include "../../Maple/Code/CSTNHCompressibleEnergy.mcg"

	this->m_energy = t614;
}

void CSTElementNH::update_Force(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	double kc = m_vpmat[0]->getShear() / 2.0;
	double kd = m_vpmat[0]->getBulk() / 2.0;

	const double& A0 = this->m_A0;
	const DNDx& dNdx = this->m_dNdx;

	double vfx[9];

#include "../../Maple/Code/CSTNHCompressibleForce.mcg"

	for (int i = 0; i < 9; ++i)
		if (!isfinite(vfx[i]))
			logSimu("In %s: Force value is NAN\n", __FUNCDNAME__);

	// Store force vector

	for (int i = 0; i < 9; ++i)
		this->m_vfVal(i) = vfx[i];
}

void CSTElementNH::update_Jacobian(const VectorXd& vx, const VectorXd& vv)
{
	vector<double> x1(3);
	vector<double> x2(3);
	vector<double> x3(3);
	for (int i = 0; i < 3; ++i)
	{
		x1[i] = vx(this->m_vidxx[0 + i]);
		x2[i] = vx(this->m_vidxx[3 + i]);
		x3[i] = vx(this->m_vidxx[6 + i]);
	}

	double kc = m_vpmat[0]->getShear() / 2.0;
	double kd = m_vpmat[0]->getBulk() / 2.0;

	const double& A0 = this->m_A0;
	const DNDx& dNdx = this->m_dNdx;

	double mJx[9][9];

#include "../../Maple/Code/CSTNHCompressibleJacobian.mcg"

	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 9; ++j)
			if (!isfinite(mJx[i][j]))
				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);

	// Store triangular lower matrix

	int count = 0;
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j <= i; ++j)
			this->m_vJVal(count++) = mJx[i][j];
}

void CSTElementNH::update_DfxDx(const VectorXd& vx, const VectorXd& vX)
{
//	double lame1 = m_vpmat[0]->getLameFirst();
//	double lame2 = m_vpmat[0]->getLameSecond();
//	double pS = this->m_preStrain;
//
//	// Get material points
//
//	vector<double> x1(3);
//	vector<double> x2(3);
//	vector<double> x3(3);
//	for (int i = 0; i < 3; ++i)
//	{
//		x1[i] = vx(this->m_vidxx[0 + i]);
//		x2[i] = vx(this->m_vidxx[3 + i]);
//		x3[i] = vx(this->m_vidxx[6 + i]);
//	}
//
//	// Get rest points
//
//	vector<double> X1(3);
//	vector<double> X2(3);
//	vector<double> X3(3);
//	if (this->m_numDim_0 == 2)
//	{
//		for (int i = 0; i < 2; ++i)
//		{
//			X1[i] = vX(this->m_vidx0[0 + i]);
//			X2[i] = vX(this->m_vidx0[2 + i]);
//			X3[i] = vX(this->m_vidx0[4 + i]);
//		}
//
//		// Keep at plane
//		X1[2] = 0.0;
//		X2[2] = 0.0;
//		X3[2] = 0.0;
//	}
//	else if (this->m_numDim_0 == 3)
//	{
//		for (int i = 0; i < 3; ++i)
//		{
//			X1[i] = vX(this->m_vidx0[0 + i]);
//			X2[i] = vX(this->m_vidx0[3 + i]);
//			X3[i] = vX(this->m_vidx0[6 + i]);
//		}
//	}
//	else assert(false); // WTF?
//
//	double mJx[9][9];
//
//#include "../../Maple/Code/CSTStVKIsotropicRest_DfxDx.mcg"
//
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			if (!isfinite(mJx[i][j]))
//				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			this->m_mDfxDx(i, j) = mJx[i][j];
}

void CSTElementNH::update_DfxD0(const VectorXd& vx, const VectorXd& vX)
{
//	double lame1 = m_vpmat[0]->getLameFirst();
//	double lame2 = m_vpmat[0]->getLameSecond();
//	double pS = this->m_preStrain;
//
//	// Get material points
//
//	vector<double> x1(3);
//	vector<double> x2(3);
//	vector<double> x3(3);
//	for (int i = 0; i < 3; ++i)
//	{
//		x1[i] = vx(this->m_vidxx[0 + i]);
//		x2[i] = vx(this->m_vidxx[3 + i]);
//		x3[i] = vx(this->m_vidxx[6 + i]);
//	}
//
//	// Get rest points
//
//	vector<double> X1(3);
//	vector<double> X2(3);
//	vector<double> X3(3);
//	if (this->m_numDim_0 == 2)
//	{
//		for (int i = 0; i < 2; ++i)
//		{
//			X1[i] = vX(this->m_vidx0[0 + i]);
//			X2[i] = vX(this->m_vidx0[2 + i]);
//			X3[i] = vX(this->m_vidx0[4 + i]);
//		}
//
//		// Keep at plane
//		X1[2] = 0.0;
//		X2[2] = 0.0;
//		X3[2] = 0.0;
//	}
//	else if (this->m_numDim_0 == 3)
//	{
//		for (int i = 0; i < 3; ++i)
//		{
//			X1[i] = vX(this->m_vidx0[0 + i]);
//			X2[i] = vX(this->m_vidx0[3 + i]);
//			X3[i] = vX(this->m_vidx0[6 + i]);
//		}
//	}
//	else assert(false); // WTF?
//
//	double mJX[9][9];
//
//#include "../../Maple/Code/CSTStVKIsotropicRest_DfxD0.mcg"
//
//	for (int i = 0; i < 9; ++i)
//		for (int j = 0; j < 9; ++j)
//			if (!isfinite(mJX[i][j]))
//				logSimu("In %s: Jacobian value is NAN\n", __FUNCDNAME__);
//
//	for (int i = 0; i < this->m_numDOFx; ++i)
//	{
//		for (int j = 0; j < this->m_numNode; ++j)
//		{
//			for (int k = 0; k < this->m_numDim_0; ++k)
//			{
//				this->m_mDfxD0(i, this->m_numDim_0*j + k) = mJX[i][this->m_numDim_x*j + k];
//			}
//		}
//	}
}
