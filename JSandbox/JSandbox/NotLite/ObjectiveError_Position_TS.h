///*=====================================================================================*/
///*!
//\file		ObjectiveError_TensilePosition.h
//\author		jesusprod
//\brief		Position based objective for tensile structures.
//*/
///*=====================================================================================*/
//
//#ifndef OBJECTIVE_ERROR_POSITION_TS_H
//#define OBJECTIVE_ERROR_POSITION_TS_H
//
//#if _MSC_VER > 1000
//#pragma once
//#endif
//
//#include <JSandbox/JSandboxPCH.h>
//#include <JSandbox/MathUtils.h>
//
//#include <JSandbox/Optim/ObjectiveError.h>
//
//#include <JSandbox/Model/TensileStructureModel.h>
//
//class ModelParameterization;
//
//class JSANDBOX_EXPORT ObjectiveError_Position_TS : public ObjectiveError
//{
//public:
//	ObjectiveError_Position_TS();
//
//	virtual ~ObjectiveError_Position_TS();
//
//	virtual const VectorXd& getTargetPosition() const;
//	virtual void setTargetPosition(const VectorXd& vt);
//
//	virtual void setup();
//
//	virtual Real getError() const;
//	virtual void add_DEDx(VectorXd& vDEDx) const;
//	virtual void add_D2EDx2(tVector& vD2EDx2) const;
//
//protected:
//
//	VectorXd m_vxt;
//
//	SurfaceModel* m_pSurfModel;
//
//	TensileStructureModel* m_pTSModel;
//
//};
//
//#endif