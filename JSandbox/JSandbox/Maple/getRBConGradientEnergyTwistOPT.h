#include <math.h>

void getRBConGradientEnergyTwistOPT (
  double young,
  double shear,
  double radw,
  double radh,
  double hL0,
  const double *iniMatTanRB,
  const double *iniMatNorRB,
  const double *iniRefTanRO,
  const double *iniRefNorRO,
  double rtx,
  const double *rotationAnglesRB,
  double *ex,
  double thetax,
  double *energyGradient)
{
  double t10;
  double t100;
  double t1010;
  double t102;
  double t1021;
  double t103;
  double t106;
  double t107;
  double t108;
  double t109;
  double t11;
  double t111;
  double t113;
  double t116;
  double t118;
  double t12;
  double t122;
  double t125;
  double t13;
  double t130;
  double t138;
  double t139;
  double t14;
  double t142;
  double t144;
  double t149;
  double t15;
  double t159;
  double t16;
  double t163;
  double t165;
  double t17;
  double t175;
  double t177;
  double t18;
  double t181;
  double t182;
  double t183;
  double t185;
  double t187;
  double t188;
  double t189;
  double t19;
  double t191;
  double t192;
  double t193;
  double t195;
  double t198;
  double t20;
  double t201;
  double t202;
  double t203;
  double t205;
  double t206;
  double t207;
  double t209;
  double t21;
  double t210;
  double t214;
  double t216;
  double t217;
  double t22;
  double t220;
  double t223;
  double t224;
  double t227;
  double t229;
  double t23;
  double t230;
  double t235;
  double t237;
  double t239;
  double t243;
  double t245;
  double t247;
  double t25;
  double t255;
  double t258;
  double t26;
  double t260;
  double t262;
  double t268;
  double t27;
  double t271;
  double t273;
  double t277;
  double t28;
  double t281;
  double t282;
  double t285;
  double t289;
  double t29;
  double t295;
  double t296;
  double t298;
  double t3;
  double t303;
  double t305;
  double t309;
  double t31;
  double t311;
  double t317;
  double t32;
  double t326;
  double t327;
  double t33;
  double t332;
  double t341;
  double t342;
  double t345;
  double t35;
  double t354;
  double t355;
  double t358;
  double t36;
  double t360;
  double t361;
  double t362;
  double t37;
  double t370;
  double t373;
  double t378;
  double t38;
  double t388;
  double t39;
  double t390;
  double t391;
  double t394;
  double t395;
  double t399;
  double t4;
  double t402;
  double t405;
  double t410;
  double t415;
  double t42;
  double t426;
  double t431;
  double t439;
  double t44;
  double t45;
  double t452;
  double t456;
  double t459;
  double t463;
  double t469;
  double t47;
  double t48;
  double t480;
  double t487;
  double t5;
  double t50;
  double t503;
  double t504;
  double t51;
  double t52;
  double t522;
  double t529;
  double t53;
  double t533;
  double t535;
  double t539;
  double t54;
  double t544;
  double t549;
  double t558;
  double t57;
  double t570;
  double t584;
  double t587;
  double t59;
  double t6;
  double t60;
  double t606;
  double t62;
  double t621;
  double t63;
  double t639;
  double t64;
  double t645;
  double t651;
  double t654;
  double t658;
  double t66;
  double t664;
  double t668;
  double t67;
  double t674;
  double t678;
  double t68;
  double t684;
  double t69;
  double t692;
  double t697;
  double t7;
  double t709;
  double t71;
  double t72;
  double t722;
  double t73;
  double t734;
  double t737;
  double t748;
  double t759;
  double t76;
  double t77;
  double t775;
  double t777;
  double t780;
  double t787;
  double t791;
  double t793;
  double t796;
  double t799;
  double t8;
  double t80;
  double t802;
  double t804;
  double t806;
  double t812;
  double t817;
  double t82;
  double t823;
  double t828;
  double t834;
  double t842;
  double t847;
  double t859;
  double t86;
  double t872;
  double t875;
  double t884;
  double t887;
  double t89;
  double t898;
  double t9;
  double t909;
  double t92;
  double t927;
  double t930;
  double t934;
  double t936;
  double t941;
  double t943;
  double t947;
  double t95;
  double t952;
  double t958;
  double t96;
  double t970;
  double t98;
  double t984;
  double t996;
  double t999;
  t3 = shear * radw * radh * M_PI;
  t4 = radw * radw;
  t5 = radh * radh;
  t6 = t4 + t5;
  t19 = ex[0];
  t7 = t19 * t19;
  t20 = ex[1];
  t8 = t20 * t20;
  t23 = ex[2];
  t9 = t23 * t23;
  t10 = t7 + t8 + t9;
  t11 = sqrt(t10);
  t12 = 0.1e1 / t11;
  t13 = t12 * t20;
  t14 = pow(iniRefTanRO[0], 0.2e1);
  t15 = pow(iniRefTanRO[1], 0.2e1);
  t16 = pow(iniRefTanRO[2], 0.2e1);
  t17 = t14 + t15 + t16;
  t37 = iniRefNorRO[2];
  t18 = t17 * t37;
  t21 = t12 * t23;
  t39 = iniRefNorRO[1];
  t22 = t17 * t39;
  t25 = 0.1e1 * t13 * t18 - 0.1e1 * t21 * t22;
  t26 = cos(rtx);
  t51 = rotationAnglesRB[2];
  t27 = cos(t51);
  t52 = rotationAnglesRB[1];
  t28 = cos(t52);
  t29 = t27 * t28;
  t31 = sin(t52);
  t32 = t27 * t31;
  t54 = rotationAnglesRB[0];
  t33 = sin(t54);
  t35 = sin(t51);
  t36 = cos(t54);
  t38 = t32 * t33 - t35 * t36;
  t42 = t32 * t36 + t35 * t33;
  t63 = iniMatTanRB[0];
  t67 = iniMatTanRB[1];
  t73 = iniMatTanRB[2];
  t44 = t29 * t63 + t38 * t67 + t42 * t73;
  t45 = t44 * t12;
  t47 = 0.1e1 * t45 * t19;
  t48 = t35 * t28;
  t50 = t35 * t31;
  t53 = t50 * t33 + t27 * t36;
  t57 = t50 * t36 - t27 * t33;
  t59 = t48 * t63 + t53 * t67 + t57 * t73;
  t60 = t59 * t12;
  t62 = 0.1e1 * t60 * t20;
  t64 = t28 * t33;
  t66 = t28 * t36;
  t68 = -t31 * t63 + t64 * t67 + t66 * t73;
  t69 = t68 * t12;
  t71 = 0.1e1 * t69 * t23;
  t72 = t47 + t62 + t71;
  t95 = iniMatNorRB[0];
  t98 = iniMatNorRB[1];
  t100 = iniMatNorRB[2];
  t76 = t29 * t95 + t38 * t98 + t42 * t100;
  t77 = t72 * t76;
  t80 = t45 * t23;
  t82 = 0.1e1 * t69 * t19 - 0.1e1 * t80;
  t86 = -t31 * t95 + t64 * t98 + t66 * t100;
  t89 = 0.1e1 * t45 * t20;
  t92 = t89 - 0.1e1 * t60 * t19;
  t96 = t48 * t95 + t53 * t98 + t57 * t100;
  t102 = 0.1e1 * t60 * t23 - 0.1e1 * t69 * t20;
  t103 = t102 * t76;
  t106 = t103 + t82 * t96 + t86 * t92;
  t107 = 0.1e1 + t47 + t62 + t71;
  t108 = 0.1e1 / t107;
  t109 = t106 * t108;
  t111 = t77 + t82 * t86 - t92 * t96 + t109 * t102;
  t113 = sin(rtx);
  t116 = t82 * t76;
  t118 = t72 * t86 + t102 * t96 - t116 + t109 * t92;
  t122 = t92 * t76;
  t125 = t72 * t96 + t122 - t86 * t102 + t109 * t82;
  t130 = t12 * t19;
  t138 = 0.1e1 - t26;
  t139 = (0.1e1 * t130 * t111 + 0.1e1 * t13 * t125 + 0.1e1 * t21 * t118) * t138;
  t142 = t26 * t111 + t113 * (0.1e1 * t13 * t118 - 0.1e1 * t21 * t125) + 0.1e1 * t139 * t130;
  t163 = iniRefNorRO[0];
  t144 = t163 * t17;
  t149 = 0.1e1 * t144 * t21 - 0.1e1 * t130 * t18;
  t159 = t26 * t125 + t113 * (0.1e1 * t21 * t111 - 0.1e1 * t130 * t118) + 0.1e1 * t139 * t13;
  t165 = 0.1e1 * t130 * t22 - 0.1e1 * t13 * t144;
  t175 = t26 * t118 + t113 * (0.1e1 * t130 * t125 - 0.1e1 * t13 * t111) + 0.1e1 * t21 * t139;
  t177 = t25 * t142 + t149 * t159 + t165 * t175;
  t181 = t144 * t142 + t159 * t22 + t18 * t175;
  t182 = atan2(t177, t181);
  t183 = thetax + rtx - t182;
  t185 = 0.1e1 / hL0;
  t187 = t12 / t10;
  t188 = t187 * t20;
  t189 = t18 * t19;
  t191 = 0.1e1 * t188 * t189;
  t192 = t187 * t23;
  t193 = t22 * t19;
  t195 = 0.1e1 * t192 * t193;
  t198 = t44 * t187;
  t201 = 0.1e1 * t45;
  t202 = t59 * t187;
  t203 = t20 * t19;
  t205 = 0.1e1 * t202 * t203;
  t206 = t68 * t187;
  t207 = t23 * t19;
  t209 = 0.1e1 * t206 * t207;
  t210 = -0.1e1 * t198 * t7 + t201 - t205 - t209;
  t214 = 0.1e1 * t69;
  t216 = 0.1e1 * t198 * t207;
  t217 = -0.1e1 * t206 * t7 + t214 + t216;
  t220 = 0.1e1 * t198 * t203;
  t223 = 0.1e1 * t60;
  t224 = -t220 + 0.1e1 * t202 * t7 - t223;
  t227 = 0.1e1 * t202 * t207;
  t229 = 0.1e1 * t206 * t203;
  t230 = -t227 + t229;
  t235 = (t230 * t76 + t217 * t96 + t224 * t86) * t108;
  t237 = t107 * t107;
  t239 = t106 / t237;
  t262 = t239 * t102;
  t243 = t210 * t76 + t217 * t86 - t224 * t96 + t235 * t102 - t210 * t262 + t109 * t230;
  t245 = t118 * t19;
  t247 = 0.1e1 * t188 * t245;
  t271 = t239 * t92;
  t255 = t210 * t86 + t230 * t96 - t217 * t76 + t235 * t92 - t271 * t210 + t109 * t224;
  t258 = t125 * t19;
  t260 = 0.1e1 * t192 * t258;
  t282 = t239 * t82;
  t268 = t210 * t96 + t224 * t76 - t230 * t86 + t235 * t82 - t282 * t210 + t109 * t217;
  t273 = t187 * t7;
  t277 = 0.1e1 * t12 * t111;
  t281 = 0.1e1 * t188 * t258;
  t285 = 0.1e1 * t192 * t245;
  t289 = (-0.1e1 * t273 * t111 + t277 + 0.1e1 * t130 * t243 - t281 + 0.1e1 * t13 * t268 - t285 + 0.1e1 * t21 * t255) * t138;
  t295 = 0.1e1 * t139 * t12;
  t296 = t26 * t243 + t113 * (-t247 + 0.1e1 * t13 * t255 + t260 - 0.1e1 * t21 * t268) + 0.1e1 * t289 * t130 - 0.1e1 * t139 * t273 + t295;
  t298 = t144 * t19;
  t303 = t17 * t12;
  t305 = 0.1e1 * t303 * t37;
  t309 = t111 * t19;
  t311 = 0.1e1 * t192 * t309;
  t317 = 0.1e1 * t12 * t118;
  t326 = 0.1e1 * t139 * t188 * t19;
  t327 = t26 * t268 + t113 * (-t311 + 0.1e1 * t21 * t243 + 0.1e1 * t273 * t118 - t317 - 0.1e1 * t130 * t255) + 0.1e1 * t289 * t13 - t326;
  t332 = 0.1e1 * t303 * t39;
  t341 = 0.1e1 * t12 * t125;
  t345 = 0.1e1 * t188 * t309;
  t342 = t139 * t192;
  t354 = 0.1e1 * t342 * t19;
  t355 = t26 * t255 + t113 * (-0.1e1 * t273 * t125 + t341 + 0.1e1 * t130 * t268 + t345 - 0.1e1 * t13 * t243) + 0.1e1 * t289 * t21 - t354;
  t358 = 0.1e1 / t181;
  t360 = t181 * t181;
  t361 = 0.1e1 / t360;
  t362 = t177 * t361;
  t370 = t177 * t177;
  t373 = 0.1e1 / (0.1e1 + t370 * t361);
  t378 = t187 * t8;
  t388 = t23 * t20;
  t390 = 0.1e1 * t206 * t388;
  t391 = -t220 - 0.1e1 * t202 * t8 + t223 - t390;
  t394 = 0.1e1 * t198 * t388;
  t395 = -t229 + t394;
  t399 = -0.1e1 * t198 * t8 + t201 + t205;
  t402 = 0.1e1 * t202 * t388;
  t405 = -t402 + 0.1e1 * t206 * t8 - t214;
  t410 = (t405 * t76 + t395 * t96 + t399 * t86) * t108;
  t415 = t391 * t76 + t395 * t86 - t399 * t96 + t410 * t102 - t262 * t391 + t109 * t405;
  t426 = t391 * t86 + t405 * t96 - t395 * t76 + t410 * t92 - t271 * t391 + t109 * t399;
  t431 = 0.1e1 * t192 * t125 * t20;
  t439 = t391 * t96 + t399 * t76 - t405 * t86 + t410 * t82 - t282 * t391 + t109 * t395;
  t452 = 0.1e1 * t192 * t118 * t20;
  t456 = (-t345 + 0.1e1 * t130 * t415 - 0.1e1 * t378 * t125 + t341 + 0.1e1 * t13 * t439 - t452 + 0.1e1 * t21 * t426) * t138;
  t459 = t26 * t415 + t113 * (-0.1e1 * t378 * t118 + t317 + 0.1e1 * t13 * t426 + t431 - 0.1e1 * t21 * t439) + 0.1e1 * t456 * t130 - t326;
  t463 = 0.1e1 * t192 * t144 * t20;
  t469 = 0.1e1 * t192 * t111 * t20;
  t480 = t26 * t439 + t113 * (-t469 + 0.1e1 * t21 * t415 + t247 - 0.1e1 * t130 * t426) + 0.1e1 * t456 * t13 - 0.1e1 * t139 * t378 + t295;
  t487 = 0.1e1 * t303 * t163;
  t503 = 0.1e1 * t342 * t20;
  t504 = t26 * t426 + t113 * (-t281 + 0.1e1 * t130 * t439 + 0.1e1 * t378 * t111 - t277 - 0.1e1 * t13 * t415) + 0.1e1 * t456 * t21 - t503;
  t522 = t187 * t9;
  t529 = -t216 - t402 - 0.1e1 * t206 * t9 + t214;
  t533 = -t209 + 0.1e1 * t198 * t9 - t201;
  t535 = -t394 + t227;
  t539 = -0.1e1 * t202 * t9 + t223 + t390;
  t544 = (t539 * t76 + t533 * t96 + t535 * t86) * t108;
  t549 = t529 * t76 + t533 * t86 - t535 * t96 + t544 * t102 - t262 * t529 + t109 * t539;
  t558 = t529 * t86 + t539 * t96 - t533 * t76 + t544 * t92 - t271 * t529 + t109 * t535;
  t570 = t529 * t96 + t535 * t76 - t539 * t86 + t544 * t82 - t282 * t529 + t109 * t533;
  t584 = (-t311 + 0.1e1 * t130 * t549 - t431 + 0.1e1 * t13 * t570 - 0.1e1 * t522 * t118 + t317 + 0.1e1 * t21 * t558) * t138;
  t587 = t26 * t549 + t113 * (-t452 + 0.1e1 * t13 * t558 + 0.1e1 * t522 * t125 - t341 - 0.1e1 * t21 * t570) + 0.1e1 * t584 * t130 - t354;
  t606 = t26 * t570 + t113 * (-0.1e1 * t522 * t111 + t277 + 0.1e1 * t21 * t549 + t285 - 0.1e1 * t130 * t558) + 0.1e1 * t584 * t13 - t503;
  t621 = t26 * t558 + t113 * (-t260 + 0.1e1 * t130 * t570 + t469 - 0.1e1 * t13 * t549) + 0.1e1 * t584 * t21 - 0.1e1 * t139 * t522 + t295;
  t639 = (t42 * t67 - t38 * t73) * t12;
  t645 = (t57 * t67 - t53 * t73) * t12;
  t651 = (t66 * t67 - t64 * t73) * t12;
  t654 = 0.1e1 * t639 * t19 + 0.1e1 * t645 * t20 + 0.1e1 * t651 * t23;
  t658 = t42 * t98 - t38 * t100;
  t664 = 0.1e1 * t651 * t19 - 0.1e1 * t639 * t23;
  t668 = t66 * t98 - t64 * t100;
  t674 = 0.1e1 * t639 * t20 - 0.1e1 * t645 * t19;
  t678 = t57 * t98 - t53 * t100;
  t684 = 0.1e1 * t645 * t23 - 0.1e1 * t651 * t20;
  t692 = (t684 * t76 + t102 * t658 + t664 * t96 + t82 * t678 + t674 * t86 + t92 * t668) * t108;
  t697 = t654 * t76 + t72 * t658 + t664 * t86 + t82 * t668 - t674 * t96 - t92 * t678 + t692 * t102 - t262 * t654 + t109 * t684;
  t709 = t654 * t86 + t72 * t668 + t684 * t96 + t102 * t678 - t664 * t76 - t82 * t658 + t692 * t92 - t271 * t654 + t109 * t674;
  t722 = t654 * t96 + t72 * t678 + t674 * t76 + t92 * t658 - t86 * t684 - t102 * t668 + t692 * t82 - t282 * t654 + t109 * t664;
  t734 = (0.1e1 * t130 * t697 + 0.1e1 * t13 * t722 + 0.1e1 * t21 * t709) * t138;
  t737 = t26 * t697 + t113 * (0.1e1 * t13 * t709 - 0.1e1 * t21 * t722) + 0.1e1 * t734 * t130;
  t748 = t26 * t722 + t113 * (0.1e1 * t21 * t697 - 0.1e1 * t130 * t709) + 0.1e1 * t734 * t13;
  t759 = t26 * t709 + t113 * (0.1e1 * t130 * t722 - 0.1e1 * t13 * t697) + 0.1e1 * t21 * t734;
  t775 = t33 * t67;
  t777 = t36 * t73;
  t780 = (-t32 * t63 + t29 * t775 + t29 * t777) * t12;
  t787 = (-t50 * t63 + t48 * t775 + t48 * t777) * t12;
  t791 = t31 * t33;
  t793 = t31 * t36;
  t796 = (-t28 * t63 - t791 * t67 - t793 * t73) * t12;
  t799 = 0.1e1 * t780 * t19 + 0.1e1 * t787 * t20 + 0.1e1 * t796 * t23;
  t802 = t33 * t98;
  t804 = t36 * t100;
  t806 = -t32 * t95 + t29 * t802 + t29 * t804;
  t812 = 0.1e1 * t796 * t19 - 0.1e1 * t780 * t23;
  t817 = -t28 * t95 - t791 * t98 - t793 * t100;
  t823 = 0.1e1 * t780 * t20 - 0.1e1 * t787 * t19;
  t828 = -t50 * t95 + t48 * t802 + t48 * t804;
  t834 = 0.1e1 * t787 * t23 - 0.1e1 * t796 * t20;
  t842 = (t834 * t76 + t102 * t806 + t812 * t96 + t82 * t828 + t823 * t86 + t92 * t817) * t108;
  t847 = t799 * t76 + t72 * t806 + t812 * t86 + t82 * t817 - t823 * t96 - t92 * t828 + t842 * t102 - t262 * t799 + t109 * t834;
  t859 = t799 * t86 + t72 * t817 + t834 * t96 + t102 * t828 - t812 * t76 - t82 * t806 + t842 * t92 - t271 * t799 + t109 * t823;
  t872 = t799 * t96 + t72 * t828 + t823 * t76 + t92 * t806 - t834 * t86 - t102 * t817 + t842 * t82 - t282 * t799 + t109 * t812;
  t884 = (0.1e1 * t130 * t847 + 0.1e1 * t13 * t872 + 0.1e1 * t21 * t859) * t138;
  t887 = t26 * t847 + t113 * (0.1e1 * t13 * t859 - 0.1e1 * t21 * t872) + 0.1e1 * t884 * t130;
  t898 = t26 * t872 + t113 * (0.1e1 * t21 * t847 - 0.1e1 * t130 * t859) + 0.1e1 * t884 * t13;
  t909 = t26 * t859 + t113 * (0.1e1 * t130 * t872 - 0.1e1 * t13 * t847) + 0.1e1 * t884 * t21;
  t927 = -t59 * t12;
  t930 = 0.1e1 * t927 * t19 + t89;
  t934 = -t96;
  t936 = t23 * t86;
  t941 = 0.1e1 * t927 * t20 - t47;
  t943 = t23 * t76;
  t947 = t23 * t96;
  t952 = (0.1e1 * t45 * t943 + t102 * t934 - 0.1e1 * t927 * t947 + t116 + t941 * t86) * t108;
  t958 = t930 * t76 + t72 * t934 - 0.1e1 * t927 * t936 - t941 * t96 - t122 + t952 * t102 - t262 * t930 + 0.1e1 * t109 * t80;
  t970 = t930 * t86 + 0.1e1 * t45 * t947 + t103 + 0.1e1 * t927 * t943 - t82 * t934 + t952 * t92 - t271 * t930 + t109 * t941;
  t984 = t930 * t96 + t77 + t941 * t76 + t92 * t934 - 0.1e1 * t45 * t936 + t952 * t82 - t282 * t930 - 0.1e1 * t109 * t927 * t23;
  t996 = (0.1e1 * t130 * t958 + 0.1e1 * t13 * t984 + 0.1e1 * t21 * t970) * t138;
  t999 = t26 * t958 + t113 * (0.1e1 * t13 * t970 - 0.1e1 * t21 * t984) + 0.1e1 * t130 * t996;
  t1010 = t26 * t984 + t113 * (0.1e1 * t21 * t958 - 0.1e1 * t130 * t970) + 0.1e1 * t996 * t13;
  t1021 = t26 * t970 + t113 * (0.1e1 * t130 * t984 - 0.1e1 * t13 * t958) + 0.1e1 * t996 * t21;
  t875 = t3 * t6 * t183;
  energyGradient[0] = -0.2500000000e0 * t875 * t185 * (((-t191 + t195) * t142 + t25 * t296 + (-0.1e1 * t192 * t298 + 0.1e1 * t273 * t18 - t305) * t159 + t149 * t327 + (-0.1e1 * t273 * t22 + t332 + 0.1e1 * t188 * t298) * t175 + t165 * t355) * t358 - t362 * (t144 * t296 + t22 * t327 + t18 * t355)) * t373;
  energyGradient[1] = -0.2500000000e0 * t875 * t185 * (((-0.1e1 * t378 * t18 + t305 + 0.1e1 * t192 * t22 * t20) * t142 + t25 * t459 + (-t463 + t191) * t159 + t149 * t480 + (-0.1e1 * t188 * t193 + 0.1e1 * t378 * t144 - t487) * t175 + t165 * t504) * t358 - t362 * (t144 * t459 + t22 * t480 + t18 * t504)) * t373;
  energyGradient[2] = -0.2500000000e0 * t875 * t185 * (((-0.1e1 * t192 * t18 * t20 + 0.1e1 * t522 * t22 - t332) * t142 + t25 * t587 + (-0.1e1 * t522 * t144 + t487 + 0.1e1 * t192 * t189) * t159 + t149 * t606 + (-t195 + t463) * t175 + t165 * t621) * t358 - t362 * (t144 * t587 + t22 * t606 + t18 * t621)) * t373;
  energyGradient[3] = -0.2500000000e0 * t875 * t185 * ((t25 * t737 + t149 * t748 + t165 * t759) * t358 - t362 * (t144 * t737 + t22 * t748 + t18 * t759)) * t373;
  energyGradient[4] = -0.2500000000e0 * t875 * t185 * ((t25 * t887 + t149 * t898 + t165 * t909) * t358 - t362 * (t144 * t887 + t22 * t898 + t18 * t909)) * t373;
  energyGradient[5] = -0.2500000000e0 * t875 * t185 * ((t25 * t999 + t149 * t1010 + t165 * t1021) * t358 - t362 * (t144 * t999 + t22 * t1010 + t18 * t1021)) * t373;
  energyGradient[6] = 0.2500000000e0 * t3 * t6 * t183 * t185;
}
