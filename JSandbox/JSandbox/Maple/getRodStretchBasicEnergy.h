#include <math.h>

double getRodStretchBasicEnergy (
  double sK,
  double L0,
  double *x1,
  double *x2)
{
  double t13;
  double t15;
  double t19;
  double t5;
  double t9;
  t5 = pow(x2[0] - x1[0], 0.2e1);
  t9 = pow(x2[1] - x1[1], 0.2e1);
  t13 = pow(x2[2] - x1[2], 0.2e1);
  t15 = sqrt(t5 + t9 + t13);
  t19 = pow(t15 / L0 - 0.1e1, 0.2e1);
  return(0.5e0 * sK * L0 * t19);
}
