#include <math.h>

double getRBConEnergyTwistOPT (
  double young,
  double shear,
  double radw,
  double radh,
  double hL0,
  const double *iniMatTanRB,
  const double *iniMatNorRB,
  const double *iniRefTanRO,
  const double *iniRefNorRO,
  double rtx,
  const double *rotationAnglesRB,
  double *ex,
  double thetax)
{
  double t10;
  double t103;
  double t109;
  double t11;
  double t113;
  double t119;
  double t12;
  double t126;
  double t128;
  double t130;
  double t135;
  double t14;
  double t142;
  double t147;
  double t15;
  double t156;
  double t159;
  double t16;
  double t162;
  double t177;
  double t18;
  double t193;
  double t20;
  double t200;
  double t202;
  double t22;
  double t23;
  double t25;
  double t28;
  double t3;
  double t30;
  double t34;
  double t35;
  double t36;
  double t37;
  double t38;
  double t39;
  double t4;
  double t40;
  double t42;
  double t43;
  double t44;
  double t45;
  double t47;
  double t48;
  double t50;
  double t51;
  double t55;
  double t56;
  double t59;
  double t61;
  double t62;
  double t64;
  double t67;
  double t7;
  double t71;
  double t74;
  double t76;
  double t78;
  double t8;
  double t80;
  double t83;
  double t85;
  double t86;
  double t87;
  double t89;
  double t9;
  double t91;
  double t93;
  double t99;
  t3 = radw * radw;
  t4 = radh * radh;
  t7 = ex[0];
  t8 = t7 * t7;
  t9 = ex[1];
  t10 = t9 * t9;
  t11 = ex[2];
  t12 = t11 * t11;
  t14 = sqrt(t8 + t10 + t12);
  t15 = 0.1e1 / t14;
  t16 = t15 * t9;
  t18 = pow(iniRefTanRO[0], 0.2e1);
  t20 = pow(iniRefTanRO[1], 0.2e1);
  t22 = pow(iniRefTanRO[2], 0.2e1);
  t23 = t18 + t20 + t22;
  t25 = t23 * iniRefNorRO[2];
  t28 = t15 * t11;
  t30 = t23 * iniRefNorRO[1];
  t34 = cos(rtx);
  t35 = rotationAnglesRB[2];
  t36 = cos(t35);
  t37 = rotationAnglesRB[1];
  t38 = cos(t37);
  t39 = t36 * t38;
  t40 = iniMatTanRB[0];
  t42 = sin(t37);
  t43 = t36 * t42;
  t44 = rotationAnglesRB[0];
  t45 = sin(t44);
  t47 = sin(t35);
  t48 = cos(t44);
  t50 = t43 * t45 - t47 * t48;
  t51 = iniMatTanRB[1];
  t55 = t43 * t48 + t47 * t45;
  t56 = iniMatTanRB[2];
  t59 = (t39 * t40 + t50 * t51 + t55 * t56) * t15;
  t61 = 0.1e1 * t59 * t7;
  t62 = t47 * t38;
  t64 = t47 * t42;
  t67 = t64 * t45 + t48 * t36;
  t71 = t64 * t48 - t36 * t45;
  t74 = (t62 * t40 + t67 * t51 + t71 * t56) * t15;
  t76 = 0.1e1 * t74 * t9;
  t78 = t38 * t45;
  t80 = t38 * t48;
  t83 = (-t42 * t40 + t78 * t51 + t80 * t56) * t15;
  t85 = 0.1e1 * t83 * t11;
  t86 = t61 + t76 + t85;
  t87 = iniMatNorRB[0];
  t89 = iniMatNorRB[1];
  t91 = iniMatNorRB[2];
  t93 = t39 * t87 + t50 * t89 + t55 * t91;
  t99 = 0.1e1 * t83 * t7 - 0.1e1 * t59 * t11;
  t103 = -t42 * t87 + t78 * t89 + t80 * t91;
  t109 = 0.1e1 * t59 * t9 - 0.1e1 * t74 * t7;
  t113 = t62 * t87 + t67 * t89 + t71 * t91;
  t119 = 0.1e1 * t74 * t11 - 0.1e1 * t83 * t9;
  t126 = (t119 * t93 + t99 * t113 + t103 * t109) / (0.1e1 + t61 + t76 + t85);
  t128 = t86 * t93 + t99 * t103 - t109 * t113 + t126 * t119;
  t130 = sin(rtx);
  t135 = t86 * t103 + t119 * t113 - t99 * t93 + t126 * t109;
  t142 = t86 * t113 + t109 * t93 - t119 * t103 + t126 * t99;
  t147 = t15 * t7;
  t156 = (0.1e1 * t147 * t128 + 0.1e1 * t16 * t142 + 0.1e1 * t28 * t135) * (0.1e1 - t34);
  t159 = t34 * t128 + t130 * (0.1e1 * t16 * t135 - 0.1e1 * t28 * t142) + 0.1e1 * t156 * t147;
  t162 = t23 * iniRefNorRO[0];
  t177 = t34 * t142 + t130 * (0.1e1 * t28 * t128 - 0.1e1 * t135 * t147) + 0.1e1 * t156 * t16;
  t193 = t34 * t135 + t130 * (0.1e1 * t147 * t142 - 0.1e1 * t16 * t128) + 0.1e1 * t156 * t28;
  t200 = atan2((0.1e1 * t25 * t16 - 0.1e1 * t28 * t30) * t159 + (0.1e1 * t28 * t162 - 0.1e1 * t147 * t25) * t177 + (0.1e1 * t147 * t30 - 0.1e1 * t162 * t16) * t193, t159 * t162 + t30 * t177 + t25 * t193);
  t202 = pow(thetax + rtx - t200, 0.2e1);
  return(0.1250000000e0 * shear * radw * radh * M_PI * (t3 + t4) * t202 / hL0);
}
