#include <math.h>

double getRBConEnergyOPTMetaTest (
  double young,
  double shear,
  double radw,
  double radh,
  double hL0,
  const double *defRefTanRB,
  const double *defRefNorRB,
  const double *iniRefTanRO,
  const double *iniRefNorRO,
  double rtx,
  const double *ex,
  double thetax)
{
  double t10;
  double t105;
  double t11;
  double t111;
  double t112;
  double t114;
  double t116;
  double t12;
  double t121;
  double t128;
  double t133;
  double t14;
  double t142;
  double t145;
  double t15;
  double t151;
  double t156;
  double t16;
  double t166;
  double t172;
  double t18;
  double t182;
  double t189;
  double t191;
  double t192;
  double t20;
  double t200;
  double t201;
  double t203;
  double t213;
  double t216;
  double t22;
  double t223;
  double t229;
  double t234;
  double t24;
  double t242;
  double t251;
  double t26;
  double t265;
  double t28;
  double t29;
  double t3;
  double t30;
  double t36;
  double t37;
  double t4;
  double t43;
  double t44;
  double t52;
  double t57;
  double t59;
  double t62;
  double t67;
  double t7;
  double t70;
  double t71;
  double t73;
  double t75;
  double t77;
  double t79;
  double t8;
  double t81;
  double t83;
  double t84;
  double t85;
  double t9;
  double t91;
  double t92;
  double t98;
  double t99;
  t3 = radw * radw;
  t4 = radh * radh;
  t7 = ex[0];
  t8 = t7 * t7;
  t9 = ex[1];
  t10 = t9 * t9;
  t11 = ex[2];
  t12 = t11 * t11;
  t14 = sqrt(t8 + t10 + t12);
  t15 = 0.1e1 / t14;
  t16 = t15 * t9;
  t18 = iniRefTanRO[0] * t15;
  t20 = 0.1e1 * t18 * t7;
  t22 = iniRefTanRO[1] * t15;
  t24 = 0.1e1 * t22 * t9;
  t26 = iniRefTanRO[2] * t15;
  t28 = 0.1e1 * t26 * t11;
  t29 = t20 + t24 + t28;
  t30 = iniRefNorRO[2];
  t36 = 0.1e1 * t22 * t11 - 0.1e1 * t26 * t9;
  t37 = iniRefNorRO[1];
  t43 = 0.1e1 * t26 * t7 - 0.1e1 * t18 * t11;
  t44 = iniRefNorRO[0];
  t52 = 0.1e1 * t18 * t9 - 0.1e1 * t22 * t7;
  t57 = (t36 * t44 + t43 * t37 + t52 * t30) / (0.1e1 + t20 + t24 + t28);
  t59 = t29 * t30 + t36 * t37 - t43 * t44 + t57 * t52;
  t62 = t15 * t11;
  t67 = t29 * t37 + t52 * t44 - t36 * t30 + t57 * t43;
  t70 = 0.1e1 * t16 * t59 - 0.1e1 * t62 * t67;
  t71 = cos(rtx);
  t73 = defRefTanRB[0] * t15;
  t75 = 0.1e1 * t73 * t7;
  t77 = defRefTanRB[1] * t15;
  t79 = 0.1e1 * t77 * t9;
  t81 = defRefTanRB[2] * t15;
  t83 = 0.1e1 * t81 * t11;
  t84 = t75 + t79 + t83;
  t85 = defRefNorRB[0];
  t91 = 0.1e1 * t81 * t7 - 0.1e1 * t73 * t11;
  t92 = defRefNorRB[2];
  t98 = 0.1e1 * t73 * t9 - 0.1e1 * t77 * t7;
  t99 = defRefNorRB[1];
  t105 = 0.1e1 * t77 * t11 - 0.1e1 * t81 * t9;
  t111 = 0.1e1 / (0.1e1 + t75 + t79 + t83);
  t112 = (t105 * t85 + t91 * t99 + t98 * t92) * t111;
  t114 = t84 * t85 + t91 * t92 - t98 * t99 + t112 * t105;
  t116 = sin(rtx);
  t121 = t84 * t92 + t105 * t99 - t91 * t85 + t112 * t98;
  t128 = t84 * t99 + t98 * t85 - t105 * t92 + t112 * t91;
  t133 = t15 * t7;
  t142 = (0.1e1 * t133 * t114 + 0.1e1 * t16 * t128 + 0.1e1 * t62 * t121) * (0.1e1 - t71);
  t145 = t71 * t114 + t116 * (0.1e1 * t16 * t121 - 0.1e1 * t62 * t128) + 0.1e1 * t142 * t133;
  t151 = t29 * t44 + t43 * t30 - t52 * t37 + t57 * t36;
  t156 = 0.1e1 * t62 * t151 - 0.1e1 * t133 * t59;
  t166 = t71 * t128 + t116 * (0.1e1 * t62 * t114 - 0.1e1 * t133 * t121) + 0.1e1 * t142 * t16;
  t172 = 0.1e1 * t133 * t67 - 0.1e1 * t16 * t151;
  t182 = t71 * t121 + t116 * (0.1e1 * t133 * t128 - 0.1e1 * t16 * t114) + 0.1e1 * t142 * t62;
  t189 = atan2(t70 * t145 + t156 * t166 + t172 * t182, t151 * t145 + t67 * t166 + t59 * t182);
  t191 = pow(thetax + rtx - t189, 0.2e1);
  t192 = 0.1e1 / hL0;
  t200 = t111 * t105;
  t201 = cos(thetax);
  t203 = sin(thetax);
  t213 = (0.1e1 * t133 * t151 + 0.1e1 * t16 * t67 + 0.1e1 * t62 * t59) * (0.1e1 - t201);
  t216 = t201 * t59 + t203 * t172 + 0.1e1 * t213 * t62;
  t223 = t201 * t67 + t203 * t156 + 0.1e1 * t213 * t16;
  t229 = t111 * t91;
  t234 = t201 * t151 + t203 * t70 + 0.1e1 * t213 * t133;
  t242 = t111 * t98;
  t251 = pow(0.2e1 * t200 * (0.1e1 * t16 * t216 - 0.1e1 * t62 * t223) + 0.2e1 * t229 * (0.1e1 * t62 * t234 - 0.1e1 * t133 * t216) + 0.2e1 * t242 * (0.1e1 * t133 * t223 - 0.1e1 * t16 * t234), 0.2e1);
  t265 = pow(-0.2e1 * t200 * t234 - 0.2e1 * t229 * t223 - 0.2e1 * t242 * t216, 0.2e1);
  return(0.1250000000e0 * shear * radw * radh * M_PI * (t3 + t4) * t191 * t192 + (0.25e0 * young * radw * t4 * radh * M_PI * t251 + 0.25e0 * young * t3 * radw * radh * M_PI * t265) * t192 / 0.2e1);
}
