#include <math.h>

double getRodConBasicEnergyBending (
  double bKh,
  double bKw,
  double tK,
  double L0,
  double *iniMatTanRB,
  double iniMatNorRB,
  double *iniRefTanRO,
  double *iniRefNorRO,
  double rtx,
  double *rotationAnglesRB,
  double *x1,
  double *x2,
  double thetax)
{
  double t1;
  double t10;
  double t100;
  double t106;
  double t107;
  double t11;
  double t115;
  double t120;
  double t122;
  double t124;
  double t125;
  double t13;
  double t130;
  double t137;
  double t14;
  double t146;
  double t151;
  double t154;
  double t166;
  double t17;
  double t177;
  double t189;
  double t2;
  double t200;
  double t209;
  double t218;
  double t22;
  double t28;
  double t29;
  double t3;
  double t33;
  double t34;
  double t38;
  double t39;
  double t4;
  double t41;
  double t42;
  double t43;
  double t48;
  double t58;
  double t6;
  double t67;
  double t71;
  double t77;
  double t78;
  double t79;
  double t8;
  double t81;
  double t83;
  double t85;
  double t87;
  double t89;
  double t9;
  double t91;
  double t92;
  double t93;
  double t99;
  t1 = rotationAnglesRB[2];
  t2 = cos(t1);
  t3 = rotationAnglesRB[1];
  t4 = cos(t3);
  t6 = iniMatTanRB[0];
  t8 = sin(t3);
  t9 = t2 * t8;
  t10 = rotationAnglesRB[0];
  t11 = sin(t10);
  t13 = sin(t1);
  t14 = cos(t10);
  t17 = iniMatTanRB[1];
  t22 = iniMatTanRB[2];
  t28 = x2[0] - 0.1e1 * x1[0];
  t29 = t28 * t28;
  t33 = x2[1] - 0.1e1 * x1[1];
  t34 = t33 * t33;
  t38 = x2[2] - 0.1e1 * x1[2];
  t39 = t38 * t38;
  t41 = sqrt(t29 + t34 + t39);
  t42 = 0.1e1 / t41;
  t43 = (t2 * t4 * t6 + (t11 * t9 - t13 * t14) * t17 + (t11 * t13 + t14 * t9) * t22) * t42;
  t48 = t13 * t8;
  t58 = (t13 * t4 * t6 + (t11 * t48 + t14 * t2) * t17 + (-t11 * t2 + t14 * t48) * t22) * t42;
  t67 = (t11 * t17 * t4 + t14 * t22 * t4 - t6 * t8) * t42;
  t71 = 0.1e1 / (0.1e1 + 0.1e1 * t43 * t28 + 0.1e1 * t58 * t33 + 0.1e1 * t67 * t38);
  t77 = t71 * (0.1e1 * t58 * t38 - 0.1e1 * t67 * t33);
  t78 = t42 * t33;
  t79 = cos(thetax);
  t81 = iniRefTanRO[0] * t42;
  t83 = 0.1e1 * t81 * t28;
  t85 = iniRefTanRO[1] * t42;
  t87 = 0.1e1 * t85 * t33;
  t89 = iniRefTanRO[2] * t42;
  t91 = 0.1e1 * t89 * t38;
  t92 = t83 + t87 + t91;
  t93 = iniRefNorRO[2];
  t99 = 0.1e1 * t85 * t38 - 0.1e1 * t89 * t33;
  t100 = iniRefNorRO[1];
  t106 = -0.1e1 * t81 * t38 + 0.1e1 * t89 * t28;
  t107 = iniRefNorRO[0];
  t115 = 0.1e1 * t81 * t33 - 0.1e1 * t85 * t28;
  t120 = (t100 * t106 + t107 * t99 + t115 * t93) / (0.1e1 + t83 + t87 + t91);
  t122 = t100 * t99 - t106 * t107 + t115 * t120 + t92 * t93;
  t124 = sin(thetax);
  t125 = t42 * t28;
  t130 = t100 * t92 + t106 * t120 + t107 * t115 - t93 * t99;
  t137 = -t100 * t115 + t106 * t93 + t107 * t92 + t120 * t99;
  t146 = t42 * t38;
  t151 = (0.1e1 * t125 * t137 + 0.1e1 * t78 * t130 + 0.1e1 * t146 * t122) * (0.1e1 - t79);
  t154 = t79 * t122 + t124 * (0.1e1 * t125 * t130 - 0.1e1 * t78 * t137) + 0.1e1 * t151 * t146;
  t166 = t79 * t130 + t124 * (-0.1e1 * t125 * t122 + 0.1e1 * t146 * t137) + 0.1e1 * t151 * t78;
  t177 = t71 * (-0.1e1 * t43 * t38 + 0.1e1 * t67 * t28);
  t189 = t79 * t137 + t124 * (0.1e1 * t78 * t122 - 0.1e1 * t146 * t130) + 0.1e1 * t151 * t125;
  t200 = t71 * (0.1e1 * t43 * t33 - 0.1e1 * t58 * t28);
  t209 = pow(0.2e1 * t77 * (0.1e1 * t78 * t154 - 0.1e1 * t146 * t166) + 0.2e1 * t177 * (-0.1e1 * t125 * t154 + 0.1e1 * t146 * t189) + 0.2e1 * t200 * (0.1e1 * t125 * t166 - 0.1e1 * t78 * t189), 0.2e1);
  t218 = pow(-0.2e1 * t77 * t189 - 0.2e1 * t177 * t166 - 0.2e1 * t200 * t154, 0.2e1);
  return((bKh * t209 + bKw * t218) / L0 / 0.2e1);
}
