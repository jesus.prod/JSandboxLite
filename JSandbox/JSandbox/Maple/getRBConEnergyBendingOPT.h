#include <math.h>

double getRBConEnergyBendingOPT (
  double young,
  double shear,
  double radw,
  double radh,
  double hL0,
  const double *iniMatTanRB,
  const double iniMatNorRB,
  const double *iniRefTanRO,
  const double *iniRefNorRO,
  double rtx,
  const double *rotationAnglesRB,
  double *ex,
  double thetax)
{
  double t10;
  double t101;
  double t102;
  double t107;
  double t110;
  double t12;
  double t122;
  double t13;
  double t133;
  double t14;
  double t143;
  double t15;
  double t156;
  double t165;
  double t169;
  double t17;
  double t18;
  double t180;
  double t2;
  double t21;
  double t26;
  double t29;
  double t30;
  double t31;
  double t32;
  double t33;
  double t34;
  double t36;
  double t37;
  double t38;
  double t43;
  double t5;
  double t53;
  double t6;
  double t62;
  double t66;
  double t7;
  double t72;
  double t73;
  double t74;
  double t76;
  double t78;
  double t8;
  double t80;
  double t81;
  double t82;
  double t83;
  double t85;
  double t86;
  double t87;
  double t88;
  double t91;
  double t92;
  t2 = radh * radh;
  t5 = rotationAnglesRB[2];
  t6 = cos(t5);
  t7 = rotationAnglesRB[1];
  t8 = cos(t7);
  t10 = iniMatTanRB[0];
  t12 = sin(t7);
  t13 = t6 * t12;
  t14 = rotationAnglesRB[0];
  t15 = sin(t14);
  t17 = sin(t5);
  t18 = cos(t14);
  t21 = iniMatTanRB[1];
  t26 = iniMatTanRB[2];
  t29 = ex[0];
  t30 = t29 * t29;
  t31 = ex[1];
  t32 = t31 * t31;
  t33 = ex[2];
  t34 = t33 * t33;
  t36 = sqrt(t30 + t32 + t34);
  t37 = 0.1e1 / t36;
  t38 = (t6 * t8 * t10 + (t13 * t15 - t17 * t18) * t21 + (t13 * t18 + t17 * t15) * t26) * t37;
  t43 = t17 * t12;
  t53 = (t17 * t8 * t10 + (t43 * t15 + t6 * t18) * t21 + (t43 * t18 - t6 * t15) * t26) * t37;
  t62 = (-t12 * t10 + t8 * t15 * t21 + t8 * t18 * t26) * t37;
  t66 = 0.1e1 / (0.1e1 + 0.1e1 * t38 * t29 + 0.1e1 * t53 * t31 + 0.1e1 * t62 * t33);
  t72 = t66 * (0.1e1 * t53 * t33 - 0.1e1 * t62 * t31);
  t73 = t37 * t31;
  t74 = cos(thetax);
  t76 = pow(iniRefTanRO[0], 0.2e1);
  t78 = pow(iniRefTanRO[1], 0.2e1);
  t80 = pow(iniRefTanRO[2], 0.2e1);
  t81 = t76 + t78 + t80;
  t82 = t74 * t81;
  t83 = iniRefNorRO[2];
  t85 = sin(thetax);
  t86 = t29 * t37;
  t87 = iniRefNorRO[1];
  t88 = t81 * t87;
  t91 = iniRefNorRO[0];
  t92 = t81 * t91;
  t101 = t37 * t33;
  t102 = t81 * t83;
  t107 = (0.1e1 * t86 * t92 + 0.1e1 * t73 * t88 + 0.1e1 * t101 * t102) * (0.1e1 - t74);
  t110 = t82 * t83 + t85 * (0.1e1 * t86 * t88 - 0.1e1 * t92 * t73) + 0.1e1 * t107 * t101;
  t122 = t82 * t87 + t85 * (0.1e1 * t101 * t92 - 0.1e1 * t86 * t102) + 0.1e1 * t73 * t107;
  t133 = t66 * (0.1e1 * t62 * t29 - 0.1e1 * t38 * t33);
  t143 = t82 * t91 + t85 * (0.1e1 * t73 * t102 - 0.1e1 * t101 * t88) + 0.1e1 * t86 * t107;
  t156 = t66 * (0.1e1 * t38 * t31 - 0.1e1 * t53 * t29);
  t165 = pow(0.2e1 * t72 * (0.1e1 * t73 * t110 - 0.1e1 * t101 * t122) + 0.2e1 * t133 * (0.1e1 * t101 * t143 - 0.1e1 * t86 * t110) + 0.2e1 * t156 * (0.1e1 * t86 * t122 - 0.1e1 * t73 * t143), 0.2e1);
  t169 = radw * radw;
  t180 = pow(-0.2e1 * t72 * t143 - 0.2e1 * t122 * t133 - 0.2e1 * t156 * t110, 0.2e1);
  return((0.25e0 * young * radw * t2 * radh * M_PI * t165 + 0.25e0 * young * t169 * radw * radh * M_PI * t180) / hL0 / 0.2e1);
}
