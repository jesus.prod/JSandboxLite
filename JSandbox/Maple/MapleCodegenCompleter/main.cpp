#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cctype>


void main(int argc, char* argv[])
{
	if(argc==1)
	{
		std::cout<<"Need file name to work on\n";
		return;
	}
	char* sInFile = argv[1];

	bool removeZeroAssignments = false;
	if(argc >=3)
		removeZeroAssignments = (strcmp(argv[2], "true")==0);

	int zeroAssignments = 0;
	std::ifstream inFile(sInFile);
	if(!inFile.good())
	{
		std::cout<<"Could not open input file\n";
		return;
	}

	std::string sLineIn;
	std::string sOutString;
	char buf[10000];
	while(inFile.getline(buf,sizeof(buf)))
	{
		sLineIn = std::string(buf);
		if(sLineIn.find(" = (0.0/0.0)")!=std::string::npos)
			continue;

		if(removeZeroAssignments)
			if(sLineIn.find(" = 0.0")!=std::string::npos)
			{
				zeroAssignments++;
				continue;
			}

		int nSpaceCnt = 0;
		while(isspace(sLineIn[nSpaceCnt])) nSpaceCnt++;

		if((nSpaceCnt>5)&&(sLineIn[nSpaceCnt]=='t')&&(isdigit(sLineIn[nSpaceCnt+1])))
		{
			sLineIn.erase(sLineIn.begin(),sLineIn.begin()+nSpaceCnt);
			sOutString.append("\t\tdouble ");
			sOutString.append(sLineIn);
		}
		else sOutString.append(sLineIn);
		sOutString.append("\n");
	}
	inFile.close();
	std::ofstream out(sInFile);
	out.clear();
	out<<sOutString;
	if(removeZeroAssignments)
		std::cout<<"Removed "<<zeroAssignments<<" assigments to zero\n";
}