#include <math.h>

void getRBParamLengthJacobianOPT (
  double young,
  double shear,
  double radw,
  double radh,
  double hL0,
  double *iniMatTanRB,
  double *iniMatNorRB,
  double *iniRefTanRO,
  double *iniRefNorRO,
  double rtx,
  double *rotationAnglesRB,
  double *ex,
  double thetax,
  double *lengthJacobian)
{
  double t10;
  double t100;
  double t101;
  double t1010;
  double t1014;
  double t1018;
  double t102;
  double t1022;
  double t1025;
  double t1030;
  double t1033;
  double t104;
  double t1043;
  double t1046;
  double t105;
  double t1050;
  double t1054;
  double t1056;
  double t106;
  double t1060;
  double t1065;
  double t107;
  double t1070;
  double t1079;
  double t108;
  double t1091;
  double t11;
  double t110;
  double t1105;
  double t1108;
  double t1119;
  double t112;
  double t1124;
  double t113;
  double t1137;
  double t1143;
  double t1156;
  double t116;
  double t1177;
  double t118;
  double t1180;
  double t1194;
  double t1199;
  double t12;
  double t1208;
  double t1217;
  double t122;
  double t1226;
  double t1239;
  double t125;
  double t128;
  double t1284;
  double t1285;
  double t1291;
  double t1297;
  double t13;
  double t1300;
  double t1304;
  double t1310;
  double t1312;
  double t1313;
  double t1314;
  double t132;
  double t1320;
  double t1324;
  double t1328;
  double t133;
  double t1330;
  double t1338;
  double t1343;
  double t1347;
  double t135;
  double t1355;
  double t1367;
  double t1368;
  double t1369;
  double t137;
  double t1376;
  double t138;
  double t1380;
  double t1383;
  double t1385;
  double t139;
  double t1394;
  double t14;
  double t1405;
  double t142;
  double t1423;
  double t1429;
  double t143;
  double t1435;
  double t144;
  double t145;
  double t1466;
  double t1468;
  double t147;
  double t1471;
  double t1478;
  double t1482;
  double t1484;
  double t1487;
  double t149;
  double t1490;
  double t1493;
  double t1495;
  double t1497;
  double t15;
  double t1503;
  double t1508;
  double t1514;
  double t1519;
  double t152;
  double t1525;
  double t1533;
  double t1538;
  double t154;
  double t1550;
  double t1563;
  double t1575;
  double t1578;
  double t158;
  double t1589;
  double t16;
  double t1600;
  double t161;
  double t1618;
  double t1624;
  double t1630;
  double t166;
  double t1662;
  double t1663;
  double t1666;
  double t1670;
  double t1672;
  double t1677;
  double t1679;
  double t1683;
  double t1688;
  double t1694;
  double t17;
  double t1706;
  double t1720;
  double t1732;
  double t1735;
  double t174;
  double t1746;
  double t175;
  double t1757;
  double t1775;
  double t178;
  double t1782;
  double t1789;
  double t18;
  double t1827;
  double t1830;
  double t1837;
  double t184;
  double t1847;
  double t189;
  double t19;
  double t199;
  double t20;
  double t205;
  double t21;
  double t215;
  double t217;
  double t22;
  double t221;
  double t222;
  double t223;
  double t225;
  double t226;
  double t228;
  double t229;
  double t23;
  double t230;
  double t232;
  double t233;
  double t236;
  double t237;
  double t238;
  double t24;
  double t240;
  double t241;
  double t242;
  double t244;
  double t245;
  double t248;
  double t250;
  double t251;
  double t255;
  double t257;
  double t258;
  double t26;
  double t263;
  double t266;
  double t267;
  double t270;
  double t272;
  double t274;
  double t278;
  double t28;
  double t281;
  double t282;
  double t284;
  double t29;
  double t292;
  double t295;
  double t297;
  double t3;
  double t300;
  double t301;
  double t302;
  double t303;
  double t304;
  double t306;
  double t307;
  double t311;
  double t313;
  double t314;
  double t317;
  double t318;
  double t320;
  double t321;
  double t324;
  double t326;
  double t327;
  double t332;
  double t334;
  double t335;
  double t336;
  double t340;
  double t342;
  double t344;
  double t35;
  double t352;
  double t355;
  double t357;
  double t360;
  double t365;
  double t369;
  double t370;
  double t374;
  double t378;
  double t380;
  double t382;
  double t386;
  double t392;
  double t393;
  double t395;
  double t397;
  double t4;
  double t405;
  double t411;
  double t414;
  double t417;
  double t419;
  double t423;
  double t425;
  double t43;
  double t432;
  double t434;
  double t435;
  double t440;
  double t444;
  double t447;
  double t45;
  double t453;
  double t457;
  double t46;
  double t464;
  double t466;
  double t467;
  double t47;
  double t470;
  double t472;
  double t473;
  double t474;
  double t48;
  double t485;
  double t488;
  double t49;
  double t496;
  double t497;
  double t499;
  double t5;
  double t50;
  double t507;
  double t508;
  double t509;
  double t512;
  double t519;
  double t52;
  double t520;
  double t522;
  double t525;
  double t53;
  double t530;
  double t531;
  double t535;
  double t538;
  double t543;
  double t548;
  double t55;
  double t552;
  double t557;
  double t563;
  double t567;
  double t571;
  double t575;
  double t579;
  double t58;
  double t580;
  double t585;
  double t591;
  double t592;
  double t598;
  double t6;
  double t602;
  double t605;
  double t61;
  double t615;
  double t616;
  double t62;
  double t622;
  double t628;
  double t63;
  double t632;
  double t638;
  double t64;
  double t65;
  double t67;
  double t68;
  double t69;
  double t692;
  double t697;
  double t699;
  double t7;
  double t700;
  double t703;
  double t706;
  double t709;
  double t71;
  double t710;
  double t716;
  double t719;
  double t72;
  double t724;
  double t729;
  double t737;
  double t74;
  double t740;
  double t745;
  double t746;
  double t749;
  double t750;
  double t754;
  double t757;
  double t760;
  double t765;
  double t770;
  double t78;
  double t781;
  double t786;
  double t794;
  double t8;
  double t80;
  double t807;
  double t81;
  double t811;
  double t814;
  double t818;
  double t826;
  double t83;
  double t831;
  double t836;
  double t84;
  double t847;
  double t855;
  double t86;
  double t868;
  double t870;
  double t871;
  double t88;
  double t89;
  double t892;
  double t9;
  double t90;
  double t907;
  double t91;
  double t911;
  double t915;
  double t916;
  double t919;
  double t928;
  double t93;
  double t937;
  double t942;
  double t947;
  double t95;
  double t958;
  double t96;
  double t961;
  double t969;
  double t98;
  t3 = shear * radw * radh * M_PI;
  t4 = radw * radw;
  t5 = radh * radh;
  t6 = t4 + t5;
  t15 = ex[0];
  t7 = t15 * t15;
  t18 = ex[1];
  t8 = t18 * t18;
  t21 = ex[2];
  t9 = t21 * t21;
  t10 = t7 + t8 + t9;
  t11 = sqrt(t10);
  t12 = 0.1e1 / t11;
  t13 = t12 * t18;
  t24 = iniRefTanRO[0];
  t14 = t24 * t12;
  t16 = 0.1e1 * t14 * t15;
  t26 = iniRefTanRO[1];
  t17 = t26 * t12;
  t19 = 0.1e1 * t17 * t18;
  t28 = iniRefTanRO[2];
  t20 = t28 * t12;
  t22 = 0.1e1 * t20 * t21;
  t23 = t16 + t19 + t22;
  t29 = 0.1e1 * t17 * t21 - 0.1e1 * t20 * t18;
  t35 = 0.1e1 * t20 * t15 - 0.1e1 * t14 * t21;
  t43 = 0.1e1 * t14 * t18 - 0.1e1 * t17 * t15;
  t49 = iniRefNorRO[0];
  t52 = iniRefNorRO[1];
  t55 = iniRefNorRO[2];
  t45 = t29 * t49 + t35 * t52 + t43 * t55;
  t46 = 0.1e1 + t16 + t19 + t22;
  t47 = 0.1e1 / t46;
  t48 = t45 * t47;
  t50 = t23 * t55 + t29 * t52 - t35 * t49 + t48 * t43;
  t53 = t12 * t21;
  t58 = t23 * t52 + t43 * t49 - t29 * t55 + t48 * t35;
  t61 = 0.1e1 * t13 * t50 - 0.1e1 * t53 * t58;
  t62 = cos(rtx);
  t88 = rotationAnglesRB[2];
  t63 = cos(t88);
  t90 = rotationAnglesRB[1];
  t64 = cos(t90);
  t65 = t63 * t64;
  t67 = sin(t90);
  t68 = t63 * t67;
  t91 = rotationAnglesRB[0];
  t69 = sin(t91);
  t71 = sin(t88);
  t72 = cos(t91);
  t74 = t68 * t69 - t71 * t72;
  t78 = t68 * t72 + t71 * t69;
  t101 = iniMatTanRB[0];
  t106 = iniMatTanRB[1];
  t110 = iniMatTanRB[2];
  t80 = t65 * t101 + t74 * t106 + t78 * t110;
  t81 = t80 * t12;
  t83 = 0.1e1 * t81 * t15;
  t84 = t71 * t64;
  t86 = t71 * t67;
  t89 = t86 * t69 + t63 * t72;
  t93 = t86 * t72 - t63 * t69;
  t95 = t84 * t101 + t89 * t106 + t93 * t110;
  t96 = t95 * t12;
  t98 = 0.1e1 * t96 * t18;
  t100 = t64 * t69;
  t102 = t64 * t72;
  t104 = -t67 * t101 + t100 * t106 + t102 * t110;
  t105 = t104 * t12;
  t107 = 0.1e1 * t105 * t21;
  t108 = t83 + t98 + t107;
  t133 = iniMatNorRB[0];
  t135 = iniMatNorRB[1];
  t137 = iniMatNorRB[2];
  t112 = t65 * t133 + t74 * t135 + t78 * t137;
  t113 = t108 * t112;
  t116 = t81 * t21;
  t118 = 0.1e1 * t105 * t15 - 0.1e1 * t116;
  t122 = -t67 * t133 + t100 * t135 + t102 * t137;
  t125 = 0.1e1 * t81 * t18;
  t128 = t125 - 0.1e1 * t96 * t15;
  t132 = t84 * t133 + t89 * t135 + t93 * t137;
  t138 = 0.1e1 * t96 * t21 - 0.1e1 * t105 * t18;
  t139 = t138 * t112;
  t142 = t139 + t118 * t132 + t128 * t122;
  t143 = 0.1e1 + t83 + t98 + t107;
  t144 = 0.1e1 / t143;
  t145 = t142 * t144;
  t147 = t113 + t118 * t122 - t128 * t132 + t145 * t138;
  t149 = sin(rtx);
  t152 = t118 * t112;
  t154 = t108 * t122 + t138 * t132 - t152 + t145 * t128;
  t158 = t128 * t112;
  t161 = t108 * t132 + t158 - t122 * t138 + t145 * t118;
  t166 = t12 * t15;
  t174 = 0.1e1 - t62;
  t175 = (0.1e1 * t166 * t147 + 0.1e1 * t13 * t161 + 0.1e1 * t53 * t154) * t174;
  t178 = t62 * t147 + t149 * (0.1e1 * t13 * t154 - 0.1e1 * t53 * t161) + 0.1e1 * t175 * t166;
  t184 = t23 * t49 + t35 * t55 - t43 * t52 + t48 * t29;
  t189 = 0.1e1 * t53 * t184 - 0.1e1 * t166 * t50;
  t199 = t62 * t161 + t149 * (0.1e1 * t53 * t147 - 0.1e1 * t166 * t154) + 0.1e1 * t175 * t13;
  t205 = 0.1e1 * t166 * t58 - 0.1e1 * t13 * t184;
  t215 = t62 * t154 + t149 * (0.1e1 * t166 * t161 - 0.1e1 * t13 * t147) + 0.1e1 * t175 * t53;
  t217 = t61 * t178 + t189 * t199 + t205 * t215;
  t221 = t184 * t178 + t58 * t199 + t50 * t215;
  t222 = atan2(t217, t221);
  t223 = thetax + rtx - t222;
  t225 = hL0 * hL0;
  t226 = 0.1e1 / t225;
  t228 = 0.1e1 / t10 * t12;
  t229 = t228 * t18;
  t230 = t50 * t15;
  t232 = 0.1e1 * t229 * t230;
  t233 = t24 * t228;
  t236 = 0.1e1 * t14;
  t237 = t26 * t228;
  t238 = t18 * t15;
  t240 = 0.1e1 * t237 * t238;
  t241 = t28 * t228;
  t242 = t21 * t15;
  t244 = 0.1e1 * t241 * t242;
  t245 = -0.1e1 * t233 * t7 + t236 - t240 - t244;
  t248 = 0.1e1 * t237 * t242;
  t250 = 0.1e1 * t241 * t238;
  t251 = -t248 + t250;
  t255 = 0.1e1 * t20;
  t257 = 0.1e1 * t233 * t242;
  t258 = -0.1e1 * t241 * t7 + t255 + t257;
  t263 = 0.1e1 * t238 * t233;
  t266 = 0.1e1 * t17;
  t267 = -t263 + 0.1e1 * t237 * t7 - t266;
  t270 = (t251 * t49 + t258 * t52 + t267 * t55) * t47;
  t272 = t46 * t46;
  t274 = t45 / t272;
  t302 = t274 * t43;
  t278 = t245 * t55 + t251 * t52 - t258 * t49 + t270 * t43 - t302 * t245 + t267 * t48;
  t281 = t228 * t21;
  t282 = t58 * t15;
  t284 = 0.1e1 * t281 * t282;
  t318 = t274 * t35;
  t292 = t245 * t52 + t267 * t49 - t251 * t55 + t270 * t35 - t318 * t245 + t48 * t258;
  t295 = -t232 + 0.1e1 * t13 * t278 + t284 - 0.1e1 * t53 * t292;
  t297 = t80 * t228;
  t300 = 0.1e1 * t81;
  t301 = t95 * t228;
  t303 = 0.1e1 * t301 * t238;
  t304 = t104 * t228;
  t306 = 0.1e1 * t304 * t242;
  t307 = -0.1e1 * t297 * t7 + t300 - t303 - t306;
  t311 = 0.1e1 * t105;
  t313 = 0.1e1 * t297 * t242;
  t314 = -0.1e1 * t304 * t7 + t311 + t313;
  t317 = 0.1e1 * t297 * t238;
  t320 = 0.1e1 * t96;
  t321 = -t317 + 0.1e1 * t301 * t7 - t320;
  t324 = 0.1e1 * t301 * t242;
  t326 = 0.1e1 * t238 * t304;
  t327 = -t324 + t326;
  t332 = (t327 * t112 + t314 * t132 + t321 * t122) * t144;
  t334 = t143 * t143;
  t335 = 0.1e1 / t334;
  t336 = t142 * t335;
  t360 = t336 * t138;
  t340 = t307 * t112 + t314 * t122 - t321 * t132 + t332 * t138 - t360 * t307 + t145 * t327;
  t342 = t154 * t15;
  t344 = 0.1e1 * t229 * t342;
  t369 = t336 * t128;
  t352 = t307 * t122 + t327 * t132 - t314 * t112 + t332 * t128 - t369 * t307 + t145 * t321;
  t355 = t161 * t15;
  t357 = 0.1e1 * t281 * t355;
  t380 = t336 * t118;
  t365 = t307 * t132 + t321 * t112 - t327 * t122 + t332 * t118 - t380 * t307 + t145 * t314;
  t370 = t228 * t7;
  t374 = 0.1e1 * t12 * t147;
  t378 = 0.1e1 * t229 * t355;
  t382 = 0.1e1 * t281 * t342;
  t386 = (-0.1e1 * t370 * t147 + t374 + 0.1e1 * t166 * t340 - t378 + 0.1e1 * t13 * t365 - t382 + 0.1e1 * t53 * t352) * t174;
  t392 = 0.1e1 * t175 * t12;
  t393 = t62 * t340 + t149 * (-t344 + 0.1e1 * t13 * t352 + t357 - 0.1e1 * t53 * t365) + 0.1e1 * t386 * t166 - 0.1e1 * t175 * t370 + t392;
  t395 = t184 * t15;
  t397 = 0.1e1 * t281 * t395;
  t423 = t274 * t29;
  t405 = t245 * t49 + t258 * t55 - t267 * t52 + t270 * t29 - t423 * t245 + t48 * t251;
  t411 = 0.1e1 * t12 * t50;
  t414 = -t397 + 0.1e1 * t53 * t405 + 0.1e1 * t370 * t50 - t411 - 0.1e1 * t166 * t278;
  t417 = t147 * t15;
  t419 = 0.1e1 * t281 * t417;
  t425 = 0.1e1 * t12 * t154;
  t432 = t229 * t15;
  t434 = 0.1e1 * t175 * t432;
  t435 = t62 * t365 + t149 * (-t419 + 0.1e1 * t53 * t340 + 0.1e1 * t370 * t154 - t425 - 0.1e1 * t166 * t352) + 0.1e1 * t386 * t13 - t434;
  t440 = 0.1e1 * t12 * t58;
  t444 = 0.1e1 * t229 * t395;
  t447 = -0.1e1 * t370 * t58 + t440 + 0.1e1 * t166 * t292 + t444 - 0.1e1 * t13 * t405;
  t453 = 0.1e1 * t12 * t161;
  t457 = 0.1e1 * t229 * t417;
  t464 = t281 * t15;
  t466 = 0.1e1 * t175 * t464;
  t467 = t62 * t352 + t149 * (-0.1e1 * t370 * t161 + t453 + 0.1e1 * t166 * t365 + t457 - 0.1e1 * t13 * t340) + 0.1e1 * t386 * t53 - t466;
  t470 = 0.1e1 / t221;
  t472 = t221 * t221;
  t473 = 0.1e1 / t472;
  t474 = t217 * t473;
  t485 = t217 * t217;
  t488 = 0.1e1 / (0.1e1 + t485 * t473);
  t496 = t144 * t138;
  t497 = cos(thetax);
  t499 = sin(thetax);
  t507 = 0.1e1 * t166 * t184 + 0.1e1 * t13 * t58 + 0.1e1 * t53 * t50;
  t508 = 0.1e1 - t497;
  t509 = t507 * t508;
  t512 = t497 * t50 + t499 * t205 + 0.1e1 * t509 * t53;
  t519 = t497 * t58 + t499 * t189 + 0.1e1 * t509 * t13;
  t520 = t53 * t519;
  t522 = 0.1e1 * t13 * t512 - 0.1e1 * t520;
  t525 = t144 * t118;
  t530 = t497 * t184 + t499 * t61 + 0.1e1 * t509 * t166;
  t531 = t53 * t530;
  t535 = 0.1e1 * t531 - 0.1e1 * t166 * t512;
  t538 = t144 * t128;
  t543 = 0.1e1 * t166 * t519 - 0.1e1 * t13 * t530;
  t548 = t335 * t138;
  t552 = t144 * t327;
  t557 = 0.1e1 * t229 * t512 * t15;
  t563 = 0.1e1 * t12 * t184;
  t567 = 0.1e1 * t229 * t282;
  t571 = 0.1e1 * t281 * t230;
  t575 = (-0.1e1 * t370 * t184 + t563 + 0.1e1 * t166 * t405 - t567 + 0.1e1 * t13 * t292 - t571 + 0.1e1 * t53 * t278) * t508;
  t579 = 0.1e1 * t509 * t464;
  t580 = t497 * t278 + t499 * t447 + 0.1e1 * t575 * t53 - t579;
  t585 = 0.1e1 * t281 * t519 * t15;
  t591 = 0.1e1 * t509 * t432;
  t592 = t497 * t292 + t499 * t414 + 0.1e1 * t575 * t13 - t591;
  t598 = t335 * t118;
  t602 = t144 * t314;
  t605 = t530 * t15;
  t615 = 0.1e1 * t509 * t12;
  t616 = t497 * t405 + t499 * t295 + 0.1e1 * t575 * t166 - 0.1e1 * t509 * t370 + t615;
  t622 = 0.1e1 * t12 * t512;
  t628 = t335 * t128;
  t632 = t144 * t321;
  t638 = 0.1e1 * t12 * t519;
  t692 = t228 * t8;
  t697 = t21 * t18;
  t699 = 0.1e1 * t241 * t697;
  t700 = -t263 - 0.1e1 * t237 * t8 + t266 - t699;
  t703 = 0.1e1 * t237 * t697;
  t706 = -t703 + 0.1e1 * t241 * t8 - t255;
  t709 = 0.1e1 * t233 * t697;
  t710 = -t250 + t709;
  t716 = -0.1e1 * t233 * t8 + t236 + t240;
  t719 = (t706 * t49 + t710 * t52 + t716 * t55) * t47;
  t724 = t700 * t55 + t706 * t52 - t710 * t49 + t719 * t43 - t302 * t700 + t48 * t716;
  t729 = 0.1e1 * t281 * t58 * t18;
  t737 = t700 * t52 + t716 * t49 - t706 * t55 + t719 * t35 - t318 * t700 + t48 * t710;
  t740 = -0.1e1 * t692 * t50 + t411 + 0.1e1 * t13 * t724 + t729 - 0.1e1 * t53 * t737;
  t745 = 0.1e1 * t304 * t697;
  t746 = -t317 - 0.1e1 * t301 * t8 + t320 - t745;
  t749 = 0.1e1 * t297 * t697;
  t750 = -t326 + t749;
  t754 = -0.1e1 * t297 * t8 + t300 + t303;
  t757 = 0.1e1 * t301 * t697;
  t760 = -t757 + 0.1e1 * t304 * t8 - t311;
  t765 = (t760 * t112 + t750 * t132 + t754 * t122) * t144;
  t770 = t746 * t112 + t750 * t122 - t754 * t132 + t765 * t138 - t360 * t746 + t145 * t760;
  t781 = t746 * t122 + t760 * t132 - t750 * t112 + t765 * t128 - t369 * t746 + t145 * t754;
  t786 = 0.1e1 * t281 * t161 * t18;
  t794 = t746 * t132 + t754 * t112 - t760 * t122 + t765 * t118 - t380 * t746 + t145 * t750;
  t807 = 0.1e1 * t281 * t154 * t18;
  t811 = (-t457 + 0.1e1 * t166 * t770 - 0.1e1 * t692 * t161 + t453 + 0.1e1 * t13 * t794 - t807 + 0.1e1 * t53 * t781) * t174;
  t814 = t62 * t770 + t149 * (-0.1e1 * t692 * t154 + t425 + 0.1e1 * t13 * t781 + t786 - 0.1e1 * t53 * t794) + 0.1e1 * t811 * t166 - t434;
  t818 = 0.1e1 * t281 * t184 * t18;
  t826 = t700 * t49 + t710 * t55 - t716 * t52 + t719 * t29 - t423 * t700 + t48 * t706;
  t831 = -t818 + 0.1e1 * t53 * t826 + t232 - 0.1e1 * t166 * t724;
  t836 = 0.1e1 * t281 * t147 * t18;
  t847 = t62 * t794 + t149 * (-t836 + 0.1e1 * t53 * t770 + t344 - 0.1e1 * t166 * t781) + 0.1e1 * t811 * t13 - 0.1e1 * t175 * t692 + t392;
  t855 = -t567 + 0.1e1 * t166 * t737 + 0.1e1 * t692 * t184 - t563 - 0.1e1 * t13 * t826;
  t868 = t281 * t18;
  t870 = 0.1e1 * t175 * t868;
  t871 = t62 * t781 + t149 * (-t378 + 0.1e1 * t166 * t794 + 0.1e1 * t692 * t147 - t374 - 0.1e1 * t13 * t770) + 0.1e1 * t811 * t53 - t870;
  t892 = t144 * t760;
  t907 = 0.1e1 * t281 * t50 * t18;
  t911 = (-t444 + 0.1e1 * t166 * t826 - 0.1e1 * t692 * t58 + t440 + 0.1e1 * t13 * t737 - t907 + 0.1e1 * t53 * t724) * t508;
  t915 = 0.1e1 * t509 * t868;
  t916 = t497 * t724 + t499 * t855 + 0.1e1 * t911 * t53 - t915;
  t919 = t519 * t18;
  t928 = t497 * t737 + t499 * t831 + 0.1e1 * t911 * t13 - 0.1e1 * t509 * t692 + t615;
  t937 = t144 * t750;
  t942 = 0.1e1 * t281 * t530 * t18;
  t947 = t497 * t826 + t499 * t740 + 0.1e1 * t911 * t166 - t591;
  t958 = t144 * t754;
  t961 = t228 * t15;
  t969 = 0.1e1 * t12 * t530;
  t1010 = -t257 - t703 - 0.1e1 * t241 * t9 + t255;
  t1014 = -0.1e1 * t237 * t9 + t266 + t699;
  t1018 = -t244 + 0.1e1 * t233 * t9 - t236;
  t1022 = -t709 + t248;
  t1025 = (t1014 * t49 + t1018 * t52 + t1022 * t55) * t47;
  t1030 = t1010 * t55 + t1014 * t52 - t1018 * t49 + t1025 * t43 - t302 * t1010 + t48 * t1022;
  t1033 = t228 * t9;
  t1043 = t1010 * t52 + t1022 * t49 - t1014 * t55 + t1025 * t35 - t318 * t1010 + t48 * t1018;
  t1046 = -t907 + 0.1e1 * t13 * t1030 + 0.1e1 * t1033 * t58 - t440 - 0.1e1 * t53 * t1043;
  t1050 = -t313 - t757 - 0.1e1 * t304 * t9 + t311;
  t1054 = -t306 + 0.1e1 * t297 * t9 - t300;
  t1056 = -t749 + t324;
  t1060 = -0.1e1 * t301 * t9 + t320 + t745;
  t1065 = (t1060 * t112 + t1054 * t132 + t1056 * t122) * t144;
  t1070 = t1050 * t112 + t1054 * t122 - t1056 * t132 + t1065 * t138 - t360 * t1050 + t145 * t1060;
  t1079 = t1050 * t122 + t1060 * t132 - t1054 * t112 + t1065 * t128 - t369 * t1050 + t145 * t1056;
  t1091 = t1050 * t132 + t1056 * t112 - t1060 * t122 + t1065 * t118 - t380 * t1050 + t145 * t1054;
  t1105 = (-t419 + 0.1e1 * t166 * t1070 - t786 + 0.1e1 * t13 * t1091 - 0.1e1 * t1033 * t154 + t425 + 0.1e1 * t53 * t1079) * t174;
  t1108 = t62 * t1070 + t149 * (-t807 + 0.1e1 * t1079 * t13 + 0.1e1 * t1033 * t161 - t453 - 0.1e1 * t53 * t1091) + 0.1e1 * t1105 * t166 - t466;
  t1119 = t1010 * t49 + t1018 * t55 - t1022 * t52 + t1025 * t29 - t423 * t1010 + t48 * t1014;
  t1124 = -0.1e1 * t1033 * t184 + t563 + 0.1e1 * t53 * t1119 + t571 - 0.1e1 * t166 * t1030;
  t1137 = t62 * t1091 + t149 * (-0.1e1 * t1033 * t147 + t374 + 0.1e1 * t53 * t1070 + t382 - 0.1e1 * t166 * t1079) + 0.1e1 * t1105 * t13 - t870;
  t1143 = -t284 + 0.1e1 * t166 * t1043 + t818 - 0.1e1 * t13 * t1119;
  t1156 = t62 * t1079 + t149 * (-t357 + 0.1e1 * t166 * t1091 + t836 - 0.1e1 * t13 * t1070) + 0.1e1 * t1105 * t53 - 0.1e1 * t175 * t1033 + t392;
  t1177 = t1060 * t144;
  t1180 = t512 * t21;
  t1194 = (-t397 + 0.1e1 * t166 * t1119 - t729 + 0.1e1 * t13 * t1043 - 0.1e1 * t1033 * t50 + t411 + 0.1e1 * t53 * t1030) * t508;
  t1199 = t497 * t1030 + t499 * t1143 + 0.1e1 * t1194 * t53 - 0.1e1 * t509 * t1033 + t615;
  t1208 = t497 * t1043 + t499 * t1124 + 0.1e1 * t1194 * t13 - t915;
  t1217 = t144 * t1054;
  t1226 = t497 * t1119 + t499 * t1046 + 0.1e1 * t1194 * t166 - t579;
  t1239 = t144 * t1056;
  t1285 = (t78 * t106 - t74 * t110) * t12;
  t1291 = (t93 * t106 - t89 * t110) * t12;
  t1297 = (t102 * t106 - t100 * t110) * t12;
  t1300 = 0.1e1 * t1285 * t15 + 0.1e1 * t1291 * t18 + 0.1e1 * t1297 * t21;
  t1304 = t78 * t135 - t74 * t137;
  t1310 = 0.1e1 * t1297 * t15 - 0.1e1 * t1285 * t21;
  t1314 = t102 * t135 - t100 * t137;
  t1320 = 0.1e1 * t1285 * t18 - 0.1e1 * t1291 * t15;
  t1324 = t93 * t135 - t89 * t137;
  t1330 = 0.1e1 * t1291 * t21 - 0.1e1 * t1297 * t18;
  t1338 = (t1330 * t112 + t1304 * t138 + t1310 * t132 + t118 * t1324 + t1320 * t122 + t128 * t1314) * t144;
  t1343 = t1300 * t112 + t108 * t1304 + t1310 * t122 + t118 * t1314 - t1320 * t132 - t128 * t1324 + t1338 * t138 - t360 * t1300 + t1330 * t145;
  t1355 = t1300 * t122 + t108 * t1314 + t132 * t1330 + t138 * t1324 - t1310 * t112 - t118 * t1304 + t1338 * t128 - t369 * t1300 + t145 * t1320;
  t1368 = t1300 * t132 + t108 * t1324 + t1320 * t112 + t128 * t1304 - t122 * t1330 - t138 * t1314 + t1338 * t118 - t380 * t1300 + t145 * t1310;
  t1380 = (0.1e1 * t166 * t1343 + 0.1e1 * t13 * t1368 + 0.1e1 * t53 * t1355) * t174;
  t1383 = t62 * t1343 + t149 * (0.1e1 * t13 * t1355 - 0.1e1 * t53 * t1368) + 0.1e1 * t1380 * t166;
  t1394 = t62 * t1368 + t149 * (0.1e1 * t53 * t1343 - 0.1e1 * t166 * t1355) + 0.1e1 * t1380 * t13;
  t1405 = t62 * t1355 + t149 * (0.1e1 * t166 * t1368 - 0.1e1 * t13 * t1343) + 0.1e1 * t1380 * t53;
  t1423 = t144 * t1330;
  t1429 = t144 * t1310;
  t1435 = t144 * t1320;
  t1466 = t69 * t106;
  t1468 = t72 * t110;
  t1471 = (-t68 * t101 + t65 * t1466 + t65 * t1468) * t12;
  t1478 = (-t86 * t101 + t84 * t1466 + t84 * t1468) * t12;
  t1482 = t67 * t69;
  t1484 = t67 * t72;
  t1487 = (-t64 * t101 - t106 * t1482 - t1484 * t110) * t12;
  t1490 = 0.1e1 * t1471 * t15 + 0.1e1 * t1478 * t18 + 0.1e1 * t1487 * t21;
  t1493 = t69 * t135;
  t1495 = t72 * t137;
  t1497 = -t68 * t133 + t65 * t1493 + t65 * t1495;
  t1503 = 0.1e1 * t1487 * t15 - 0.1e1 * t1471 * t21;
  t1508 = -t64 * t133 - t1482 * t135 - t1484 * t137;
  t1514 = 0.1e1 * t1471 * t18 - 0.1e1 * t1478 * t15;
  t1519 = -t86 * t133 + t84 * t1493 + t84 * t1495;
  t1525 = 0.1e1 * t1478 * t21 - 0.1e1 * t1487 * t18;
  t1533 = (t1525 * t112 + t138 * t1497 + t1503 * t132 + t118 * t1519 + t1514 * t122 + t128 * t1508) * t144;
  t1538 = t1490 * t112 + t108 * t1497 + t1503 * t122 + t118 * t1508 - t1514 * t132 - t128 * t1519 + t1533 * t138 - t360 * t1490 + t145 * t1525;
  t1550 = t1490 * t122 + t108 * t1508 + t1525 * t132 + t138 * t1519 - t1503 * t112 - t118 * t1497 + t1533 * t128 - t369 * t1490 + t145 * t1514;
  t1563 = t1490 * t132 + t108 * t1519 + t1514 * t112 + t128 * t1497 - t1525 * t122 - t138 * t1508 + t1533 * t118 - t380 * t1490 + t145 * t1503;
  t1575 = (0.1e1 * t166 * t1538 + 0.1e1 * t13 * t1563 + 0.1e1 * t53 * t1550) * t174;
  t1578 = t1538 * t62 + t149 * (0.1e1 * t13 * t1550 - 0.1e1 * t53 * t1563) + 0.1e1 * t1575 * t166;
  t1589 = t62 * t1563 + t149 * (0.1e1 * t53 * t1538 - 0.1e1 * t166 * t1550) + 0.1e1 * t1575 * t13;
  t1600 = t62 * t1550 + t149 * (0.1e1 * t166 * t1563 - 0.1e1 * t13 * t1538) + 0.1e1 * t1575 * t53;
  t1618 = t144 * t1525;
  t1624 = t144 * t1503;
  t1630 = t144 * t1514;
  t1662 = -t95;
  t1663 = t1662 * t12;
  t1666 = 0.1e1 * t1663 * t15 + t125;
  t1670 = -t132;
  t1672 = t21 * t122;
  t1677 = 0.1e1 * t1663 * t18 - t83;
  t1679 = t21 * t112;
  t1683 = t21 * t132;
  t1688 = (0.1e1 * t81 * t1679 + t138 * t1670 - 0.1e1 * t1663 * t1683 + t152 + t1677 * t122) * t144;
  t1694 = t1666 * t112 + t108 * t1670 - 0.1e1 * t1663 * t1672 - t1677 * t132 - t158 + t1688 * t138 - t360 * t1666 + 0.1e1 * t116 * t145;
  t1706 = t1666 * t122 + 0.1e1 * t81 * t1683 + t139 + 0.1e1 * t1663 * t1679 - t118 * t1670 + t1688 * t128 - t369 * t1666 + t145 * t1677;
  t1720 = t1666 * t132 + t113 + t1677 * t112 + t128 * t1670 - 0.1e1 * t81 * t1672 + t1688 * t118 - t380 * t1666 - 0.1e1 * t145 * t1663 * t21;
  t1732 = (0.1e1 * t166 * t1694 + 0.1e1 * t13 * t1720 + 0.1e1 * t53 * t1706) * t174;
  t1735 = t62 * t1694 + t149 * (0.1e1 * t13 * t1706 - 0.1e1 * t53 * t1720) + 0.1e1 * t1732 * t166;
  t1746 = t62 * t1720 + t149 * (0.1e1 * t53 * t1694 - 0.1e1 * t166 * t1706) + 0.1e1 * t1732 * t13;
  t1757 = t62 * t1706 + t149 * (0.1e1 * t166 * t1720 - 0.1e1 * t13 * t1694) + 0.1e1 * t1732 * t53;
  t1775 = t144 * t80;
  t1782 = t144 * t1662;
  t1789 = t144 * t1677;
  t1827 = t507 * t499;
  t1830 = -t499 * t50 + t497 * t205 + 0.1e1 * t1827 * t53;
  t1837 = -t499 * t58 + t497 * t189 + 0.1e1 * t1827 * t13;
  t1847 = -t499 * t184 + t497 * t61 + 0.1e1 * t1827 * t166;
  t1284 = t3 * t6 * t223;
  t1312 = young * radw * t5 * radh * M_PI * (0.2e1 * t496 * t522 + 0.2e1 * t525 * t535 + 0.2e1 * t538 * t543);
  t1313 = t548 * t522;
  t1328 = t598 * t535;
  t1347 = t628 * t543;
  t1367 = young * t4 * radw * radh * M_PI * (-0.2e1 * t496 * t530 - 0.2e1 * t525 * t519 - 0.2e1 * t538 * t512);
  t1369 = t548 * t530;
  t1376 = t598 * t519;
  t1385 = t628 * t512;
  lengthJacobian[0] = 0.2500000000e0 * t1284 * t226 * ((t295 * t178 + t61 * t393 + t414 * t199 + t189 * t435 + t447 * t215 + t205 * t467) * t470 - t474 * (t178 * t405 + t184 * t393 + t292 * t199 + t58 * t435 + t278 * t215 + t50 * t467)) * t488 - (0.50e0 * t1312 * (-0.2e1 * t1313 * t307 + 0.2e1 * t552 * t522 + 0.2e1 * t496 * (-t557 + 0.1e1 * t13 * t580 + t585 - 0.1e1 * t53 * t592) - 0.2e1 * t1328 * t307 + 0.2e1 * t602 * t535 + 0.2e1 * t525 * (-0.1e1 * t281 * t605 + 0.1e1 * t53 * t616 + 0.1e1 * t370 * t512 - t622 - 0.1e1 * t166 * t580) - 0.2e1 * t1347 * t307 + 0.2e1 * t632 * t543 + 0.2e1 * t538 * (-0.1e1 * t370 * t519 + t638 + 0.1e1 * t166 * t592 + 0.1e1 * t229 * t605 - 0.1e1 * t13 * t616)) + 0.50e0 * t1367 * (0.2e1 * t1369 * t307 - 0.2e1 * t552 * t530 - 0.2e1 * t496 * t616 + 0.2e1 * t1376 * t307 - 0.2e1 * t602 * t519 - 0.2e1 * t525 * t592 + 0.2e1 * t1385 * t307 - 0.2e1 * t632 * t512 - 0.2e1 * t538 * t580)) * t226 / 0.2e1;
  lengthJacobian[1] = 0.2500000000e0 * t1284 * t226 * ((t740 * t178 + t61 * t814 + t831 * t199 + t189 * t847 + t855 * t215 + t205 * t871) * t470 - t474 * (t826 * t178 + t184 * t814 + t737 * t199 + t58 * t847 + t724 * t215 + t50 * t871)) * t488 - (0.50e0 * t1312 * (-0.2e1 * t1313 * t746 + 0.2e1 * t892 * t522 + 0.2e1 * t496 * (-0.1e1 * t692 * t512 + t622 + 0.1e1 * t13 * t916 + 0.1e1 * t281 * t919 - 0.1e1 * t53 * t928) - 0.2e1 * t1328 * t746 + 0.2e1 * t937 * t535 + 0.2e1 * t525 * (-t942 + 0.1e1 * t53 * t947 + t557 - 0.1e1 * t166 * t916) - 0.2e1 * t1347 * t746 + 0.2e1 * t958 * t543 + 0.2e1 * t538 * (-0.1e1 * t961 * t919 + 0.1e1 * t166 * t928 + 0.1e1 * t692 * t530 - t969 - 0.1e1 * t13 * t947)) + 0.50e0 * t1367 * (0.2e1 * t1369 * t746 - 0.2e1 * t892 * t530 - 0.2e1 * t496 * t947 + 0.2e1 * t1376 * t746 - 0.2e1 * t937 * t519 - 0.2e1 * t525 * t928 + 0.2e1 * t746 * t1385 - 0.2e1 * t958 * t512 - 0.2e1 * t538 * t916)) * t226 / 0.2e1;
  lengthJacobian[2] = 0.2500000000e0 * t1284 * t226 * ((t1046 * t178 + t61 * t1108 + t1124 * t199 + t189 * t1137 + t1143 * t215 + t205 * t1156) * t470 - t474 * (t1119 * t178 + t184 * t1108 + t1043 * t199 + t58 * t1137 + t1030 * t215 + t50 * t1156)) * t488 - (0.50e0 * t1312 * (-0.2e1 * t1313 * t1050 + 0.2e1 * t1177 * t522 + 0.2e1 * t496 * (-0.1e1 * t229 * t1180 + 0.1e1 * t13 * t1199 + 0.1e1 * t1033 * t519 - t638 - 0.1e1 * t53 * t1208) - 0.2e1 * t1328 * t1050 + 0.2e1 * t1217 * t535 + 0.2e1 * t525 * (-0.1e1 * t1033 * t530 + t969 + 0.1e1 * t53 * t1226 + 0.1e1 * t961 * t1180 - 0.1e1 * t166 * t1199) - 0.2e1 * t1347 * t1050 + 0.2e1 * t1239 * t543 + 0.2e1 * t538 * (-t585 + 0.1e1 * t166 * t1208 + t942 - 0.1e1 * t13 * t1226)) + 0.50e0 * t1367 * (0.2e1 * t1369 * t1050 - 0.2e1 * t1177 * t530 - 0.2e1 * t496 * t1226 + 0.2e1 * t1376 * t1050 - 0.2e1 * t1217 * t519 - 0.2e1 * t525 * t1208 + 0.2e1 * t1385 * t1050 - 0.2e1 * t1239 * t512 - 0.2e1 * t538 * t1199)) * t226 / 0.2e1;
  lengthJacobian[3] = 0.2500000000e0 * t1284 * t226 * ((t61 * t1383 + t189 * t1394 + t205 * t1405) * t470 - t474 * (t184 * t1383 + t58 * t1394 + t50 * t1405)) * t488 - (0.50e0 * t1312 * (-0.2e1 * t1313 * t1300 + 0.2e1 * t1423 * t522 - 0.2e1 * t1328 * t1300 + 0.2e1 * t1429 * t535 - 0.2e1 * t1347 * t1300 + 0.2e1 * t1435 * t543) + 0.50e0 * t1367 * (0.2e1 * t1369 * t1300 - 0.2e1 * t1423 * t530 + 0.2e1 * t1376 * t1300 - 0.2e1 * t1429 * t519 + 0.2e1 * t1385 * t1300 - 0.2e1 * t1435 * t512)) * t226 / 0.2e1;
  lengthJacobian[4] = 0.2500000000e0 * t1284 * t226 * ((t61 * t1578 + t189 * t1589 + t205 * t1600) * t470 - t474 * (t184 * t1578 + t58 * t1589 + t50 * t1600)) * t488 - (0.50e0 * t1312 * (-0.2e1 * t1313 * t1490 + 0.2e1 * t1618 * t522 - 0.2e1 * t1328 * t1490 + 0.2e1 * t1624 * t535 - 0.2e1 * t1347 * t1490 + 0.2e1 * t1630 * t543) + 0.50e0 * t1367 * (0.2e1 * t1369 * t1490 - 0.2e1 * t1618 * t530 + 0.2e1 * t1376 * t1490 - 0.2e1 * t1624 * t519 + 0.2e1 * t1385 * t1490 - 0.2e1 * t1630 * t512)) * t226 / 0.2e1;
  lengthJacobian[5] = 0.2500000000e0 * t1284 * t226 * ((t61 * t1735 + t189 * t1746 + t205 * t1757) * t470 - t474 * (t184 * t1735 + t58 * t1746 + t50 * t1757)) * t488 - (0.50e0 * t1312 * (-0.2e1 * t1313 * t1666 + 0.2e1 * t1775 * t53 * t522 - 0.2e1 * t1328 * t1666 - 0.2e1 * t1782 * t53 * t535 - 0.2e1 * t1347 * t1666 + 0.2e1 * t1789 * t543) + 0.50e0 * t1367 * (0.2e1 * t1369 * t1666 - 0.2e1 * t1775 * t531 + 0.2e1 * t1376 * t1666 + 0.2e1 * t1782 * t520 + 0.2e1 * t1385 * t1666 - 0.2e1 * t1789 * t512)) * t226 / 0.2e1;
  lengthJacobian[6] = -0.2500000000e0 * t3 * t6 * t223 * t226 - (0.50e0 * t1312 * (0.2e1 * t496 * (0.1e1 * t13 * t1830 - 0.1e1 * t53 * t1837) + 0.2e1 * t525 * (0.1e1 * t53 * t1847 - 0.1e1 * t166 * t1830) + 0.2e1 * t538 * (0.1e1 * t166 * t1837 - 0.1e1 * t13 * t1847)) + 0.50e0 * t1367 * (-0.2e1 * t496 * t1847 - 0.2e1 * t525 * t1837 - 0.2e1 * t538 * t1830)) * t226 / 0.2e1;
}
