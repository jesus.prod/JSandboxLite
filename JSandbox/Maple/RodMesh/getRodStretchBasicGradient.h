#include <math.h>

void getRodStretchBasicGradient (
  double sK,
  double L0,
  double *x1,
  double *x2,
  double *energyGradient)
{
  double t1;
  double t2;
  double t20;
  double t22;
  double t24;
  double t26;
  double t3;
  double t4;
  double t5;
  double t6;
  double t8;
  t1 = x2[0] - x1[0];
  t2 = t1 * t1;
  t3 = x2[1] - x1[1];
  t4 = t3 * t3;
  t5 = x2[2] - x1[2];
  t6 = t5 * t5;
  t8 = sqrt(t2 + t4 + t6);
  t20 = sK * (t8 / L0 - 0.1e1) / t8;
  t22 = 0.10e1 * t20 * t1;
  energyGradient[0] = -t22;
  t24 = 0.10e1 * t20 * t3;
  energyGradient[1] = -t24;
  t26 = 0.10e1 * t20 * t5;
  energyGradient[2] = -t26;
  energyGradient[3] = t22;
  energyGradient[4] = t24;
  energyGradient[5] = t26;
}
