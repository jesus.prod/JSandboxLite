#include <math.h>

double getRBConEnergyBendingOPT (
  double young,
  double shear,
  double radw,
  double radh,
  double hL0,
  double *iniMatTanRB,
  double iniMatNorRB,
  double *iniRefTanRO,
  double *iniRefNorRO,
  double rtx,
  double *rotationAnglesRB,
  double *ex,
  double thetax)
{
  double t10;
  double t101;
  double t102;
  double t110;
  double t115;
  double t117;
  double t119;
  double t12;
  double t120;
  double t125;
  double t13;
  double t132;
  double t14;
  double t141;
  double t146;
  double t149;
  double t15;
  double t161;
  double t17;
  double t172;
  double t18;
  double t182;
  double t195;
  double t2;
  double t204;
  double t208;
  double t21;
  double t219;
  double t26;
  double t29;
  double t30;
  double t31;
  double t32;
  double t33;
  double t34;
  double t36;
  double t37;
  double t38;
  double t43;
  double t5;
  double t53;
  double t6;
  double t62;
  double t66;
  double t7;
  double t72;
  double t73;
  double t74;
  double t76;
  double t78;
  double t8;
  double t80;
  double t82;
  double t84;
  double t86;
  double t87;
  double t88;
  double t94;
  double t95;
  t2 = radh * radh;
  t5 = rotationAnglesRB[2];
  t6 = cos(t5);
  t7 = rotationAnglesRB[1];
  t8 = cos(t7);
  t10 = iniMatTanRB[0];
  t12 = sin(t7);
  t13 = t6 * t12;
  t14 = rotationAnglesRB[0];
  t15 = sin(t14);
  t17 = sin(t5);
  t18 = cos(t14);
  t21 = iniMatTanRB[1];
  t26 = iniMatTanRB[2];
  t29 = ex[0];
  t30 = t29 * t29;
  t31 = ex[1];
  t32 = t31 * t31;
  t33 = ex[2];
  t34 = t33 * t33;
  t36 = sqrt(t30 + t32 + t34);
  t37 = 0.1e1 / t36;
  t38 = (t6 * t8 * t10 + (t13 * t15 - t17 * t18) * t21 + (t13 * t18 + t17 * t15) * t26) * t37;
  t43 = t17 * t12;
  t53 = (t17 * t8 * t10 + (t43 * t15 + t6 * t18) * t21 + (t43 * t18 - t6 * t15) * t26) * t37;
  t62 = (-t12 * t10 + t8 * t15 * t21 + t8 * t18 * t26) * t37;
  t66 = 0.1e1 / (0.1e1 + 0.1e1 * t38 * t29 + 0.1e1 * t53 * t31 + 0.1e1 * t62 * t33);
  t72 = t66 * (0.1e1 * t53 * t33 - 0.1e1 * t62 * t31);
  t73 = t37 * t31;
  t74 = cos(thetax);
  t76 = iniRefTanRO[0] * t37;
  t78 = 0.1e1 * t76 * t29;
  t80 = iniRefTanRO[1] * t37;
  t82 = 0.1e1 * t80 * t31;
  t84 = iniRefTanRO[2] * t37;
  t86 = 0.1e1 * t84 * t33;
  t87 = t78 + t82 + t86;
  t88 = iniRefNorRO[2];
  t94 = 0.1e1 * t80 * t33 - 0.1e1 * t84 * t31;
  t95 = iniRefNorRO[1];
  t101 = 0.1e1 * t84 * t29 - 0.1e1 * t76 * t33;
  t102 = iniRefNorRO[0];
  t110 = 0.1e1 * t76 * t31 - 0.1e1 * t80 * t29;
  t115 = (t94 * t102 + t101 * t95 + t110 * t88) / (0.1e1 + t78 + t82 + t86);
  t117 = t87 * t88 + t94 * t95 - t101 * t102 + t115 * t110;
  t119 = sin(thetax);
  t120 = t29 * t37;
  t125 = t87 * t95 + t110 * t102 - t94 * t88 + t115 * t101;
  t132 = t87 * t102 + t101 * t88 - t110 * t95 + t115 * t94;
  t141 = t37 * t33;
  t146 = (0.1e1 * t120 * t132 + 0.1e1 * t73 * t125 + 0.1e1 * t141 * t117) * (0.1e1 - t74);
  t149 = t74 * t117 + t119 * (0.1e1 * t125 * t120 - 0.1e1 * t73 * t132) + 0.1e1 * t146 * t141;
  t161 = t74 * t125 + t119 * (0.1e1 * t141 * t132 - 0.1e1 * t120 * t117) + 0.1e1 * t146 * t73;
  t172 = t66 * (0.1e1 * t62 * t29 - 0.1e1 * t38 * t33);
  t182 = t74 * t132 + t119 * (0.1e1 * t73 * t117 - 0.1e1 * t141 * t125) + 0.1e1 * t146 * t120;
  t195 = t66 * (0.1e1 * t38 * t31 - 0.1e1 * t53 * t29);
  t204 = pow(0.2e1 * t72 * (0.1e1 * t73 * t149 - 0.1e1 * t141 * t161) + 0.2e1 * t172 * (0.1e1 * t141 * t182 - 0.1e1 * t120 * t149) + 0.2e1 * t195 * (0.1e1 * t120 * t161 - 0.1e1 * t73 * t182), 0.2e1);
  t208 = radw * radw;
  t219 = pow(-0.2e1 * t72 * t182 - 0.2e1 * t161 * t172 - 0.2e1 * t195 * t149, 0.2e1);
  return((0.25e0 * young * radw * t2 * radh * M_PI * t204 + 0.25e0 * young * t208 * radw * radh * M_PI * t219) / hL0 / 0.2e1);
}
