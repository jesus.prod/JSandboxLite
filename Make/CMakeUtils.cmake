###########################################################################
###########################################################################
###########################################################################
# Adds the source files 'files' to the list 'varName' and creates
# a project group named 'groupName' in the folder 'Source Files'
# and 'Header Files', respectively.
#
# Example:
#	file(GLOB sourcesTmp
#		${CMAKE_CURRENT_SOURCE_DIR}/SparseLinearAlgebra/*.cpp
#		${CMAKE_CURRENT_SOURCE_DIR}/SparseLinearAlgebra/*.h
#	)
#	ADD_GROUPED_SOURCE(sources "${sourcesTmp}" "SparseLinearAlgebra")
##
function(ADD_GROUPED_SOURCE varName files groupName)
	foreach(file ${files})
		get_filename_component(ext ${file} EXT)

		if(ext STREQUAL ".h")
			set(filesH ${filesH} ${file})
		else()
			set(filesC ${filesC} ${file})
		endif()
	endforeach()

	if(filesC)
		source_group("Source Files\\${groupName}" FILES ${filesC}) 
	endif()

	if(filesH)
		source_group("Header Files\\${groupName}" FILES ${filesH}) 
	endif()

	set(${varName} ${${varName}} ${files} PARENT_SCOPE)	
endfunction()


###########################################################################
###########################################################################
###########################################################################
# Creates .cmake associated to a specific project so that it can be
# easily imported by other projects as an external library into their 
# build-trees.
##
set(PLUGINS_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/../Modules")

list(APPEND CMAKE_MODULE_PATH "${PLUGINS_MODULE_PATH}")
list(APPEND CMAKE_PREFIX_PATH "${PLUGINS_MODULE_PATH}")

set(MAKE_SCRIPT_DIR ${CMAKE_CURRENT_LIST_DIR})

function(plugin_export name version)
	set(dependencies ${ARGN})

	# Ensure the directory exists
	file(MAKE_DIRECTORY "${PLUGINS_MODULE_PATH}")

	# Add all targets to the build-tree export set
	file(REMOVE "${PLUGINS_MODULE_PATH}/${name}Targets.cmake")
	export(TARGETS ${name} APPEND FILE "${PLUGINS_MODULE_PATH}/${name}Targets.cmake")

	# Export the package for use from the build-tree (this 
	# registers the build-tree with a global CMake-registry)
	export(PACKAGE ${name})

	set(plugin ${name})
	string(TOUPPER "${plugin}" PLUGIN)
	set(PLUGIN_VERSION ${version})

	get_filename_component(PLUGIN_CMAKE_DIR_NAME "${CMAKE_CURRENT_LIST_DIR}" NAME)

	# Most of the plugins have no separation of public and private headers,
	# ideally only the public include directory would be listed here
	set(REL_INCLUDE_DIR "../${PLUGIN_CMAKE_DIR_NAME}")
	set(PLUGIN_INCLUDE_DIR
		"\${${PLUGIN}_CMAKE_DIR}/${REL_INCLUDE_DIR}/include"
		"\${${PLUGIN}_CMAKE_DIR}/${REL_INCLUDE_DIR}"
	)

	set(PLUGIN_DEPENDENCIES ${dependencies})

	set(PLUGIN_DEFINITIONS)

	list(APPEND PLUGIN_DEFINITIONS -D${PLUGIN}_STATIC)

	# Create the ${name}Config.cmake and ${name}ConfigVersion files
	configure_file("${MAKE_SCRIPT_DIR}/PluginConfig.cmake.in"
		"${PLUGINS_MODULE_PATH}/${name}Config.cmake" @ONLY)
	configure_file("${MAKE_SCRIPT_DIR}/PluginConfigVersion.cmake.in"
		"${PLUGINS_MODULE_PATH}/${name}ConfigVersion.cmake" @ONLY)
endfunction(plugin_export)

###########################################################################
###########################################################################
###########################################################################
# Sets the variable 'varName' to the cpp file created
# for the precompiled header file 'PCHFile'
#
# Example:
#          GET_PRECOMPILED_HEADER_CPP_NAME(PRECOMPILED_HEADER_H_CPP "src/MyLibraryPCH.h")
##
function(GET_PRECOMPILED_HEADER_CPP_NAME varName PCHFile)
	string(REPLACE ".." "__" fixedDots "${PCHFile}")
	set(${varName} "${fixedDots}.cpp" PARENT_SCOPE)	
endfunction()

# Enables precompiled headers for target '<targetName>'.
# The target must contain the auto-generated source file '<PCHFile>.cpp'.
# The precompiled header file is '<PCHFile>' and the
# file '<PCHFile>.cpp' will be created automatically.
#
# Example:
#          SET_PRECOMPILED_HEADER(${MODULE_NAME} "src/MyLibraryPCH.h")
#
macro(SET_PRECOMPILED_HEADER targetName PCHFile)
	get_filename_component(PCHName ${PCHFile} NAME)
	
	GET_PRECOMPILED_HEADER_CPP_NAME(PCHCPPName ${PCHFile})

	get_filename_component(PCHCPPDir ${PCHCPPName} PATH)

	if(${CMAKE_GENERATOR} MATCHES "NMake Makefiles" OR ${CMAKE_GENERATOR} MATCHES "NMake Makefiles JOM")
		# TODO: implement this for NMake!
	elseif(MSVC)
		set(_FC_MSVC_FORCEINCLUDE1 "/Yc\"${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}\" /FI\"$(NOINHERIT)\" /FI\"${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}\"")
		set_target_properties(${targetName} PROPERTIES COMPILE_FLAGS "/Yu\"${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}\" /FI\"${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}\"")
		set_source_files_properties(${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPName} PROPERTIES COMPILE_FLAGS ${_FC_MSVC_FORCEINCLUDE1})
	elseif(${CMAKE_GENERATOR} MATCHES "Xcode")
		set_target_properties(${targetName} PROPERTIES XCODE_ATTRIBUTE_GCC_PREFIX_HEADER "${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}")
		set_target_properties(${targetName} PROPERTIES XCODE_ATTRIBUTE_GCC_PRECOMPILE_PREFIX_HEADER "YES")
	else()
		# TODO (Linux)
		set_target_properties(${targetName} PROPERTIES COMPILE_FLAGS "-include \"${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}\"")
		# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -include ${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}")
	endif()

	# Need different escaping for Win and OS X:
	if(${CMAKE_GENERATOR} MATCHES "NMake Makefiles" OR ${CMAKE_GENERATOR} MATCHES "NMake Makefiles JOM")
		add_custom_command(
			OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPName}
			COMMAND IF NOT EXIST "\"${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPDir}\"" mkdir "\"${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPDir}\""
			COMMAND echo \#include ^<${PCHFile}^> > ${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPName}
			DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}
		)
	elseif(MSVC)
		add_custom_command(
			OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPName}
			COMMAND echo \#include ^<${PCHFile}^> > ${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPName}
			DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}
		)
	else()
		add_custom_command(
			OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPName}
			COMMAND mkdir -p ${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPDir}
			COMMAND echo \\\#include \\<${PCHFile}\\> > ${CMAKE_CURRENT_BINARY_DIR}/${PCHCPPName}
			DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${PCHFile}
		)
	endif()

	# Make sure the header file can be found from the generated cpp file:
	include_directories(${CMAKE_CURRENT_SOURCE_DIR})
endmacro(SET_PRECOMPILED_HEADER)
